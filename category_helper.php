<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function CountProductinCategory($id) {
    $CI = & get_instance();
    $CI->load->model('Frontmodel');
    $details = $CI->Frontmodel->getNumberOfCategory($id);
    return $details;
}

function GetLoginDetailsArr() {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->GetLoginDetailsArr();
    return $details;
}

function GetLoginUserIdAndRole() {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->GetLoginUserID();
    return $details;
}

function time_elapsed_string($ptime) {
    $etime = time() - $ptime;

    if ($etime < 1) {
        return '0 seconds';
    }

    $a = array(365 * 24 * 60 * 60 => 'year',
        30 * 24 * 60 * 60 => 'month',
        24 * 60 * 60 => 'day',
        60 * 60 => 'hour',
        60 => 'minute',
        1 => 'second'
    );
    $a_plural = array('year' => 'years',
        'month' => 'months',
        'day' => 'days',
        'hour' => 'hours',
        'minute' => 'minutes',
        'second' => 'seconds'
    );

    foreach ($a as $secs => $str) {
        $d = $etime / $secs;
        if ($d >= 1) {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
        }
    }
}

function time_ago($date) {
    if (empty($date)) {
        return "No date provided";
    }
    $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
    $lengths = array("60", "60", "24", "7", "4.35", "12", "10");
    $now = time();
    $unix_date = strtotime($date);
// check validity of date
    if (empty($unix_date)) {
        return "Bad date";
    }
// is it future date or past date
    if ($now > $unix_date) {
        $difference = $now - $unix_date;
        $tense = "ago";
    } else {
        $difference = $unix_date - $now;
        $tense = "from now";
    }
    for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
        $difference /= $lengths[$j];
    }
    $difference = round($difference);
    if ($difference != 1) {
        $periods[$j] .= "s";
    }
    return "$difference $periods[$j] {$tense}";
}

//Get Project ID Ash 20-Dec..
function getprojectid_tstobd($bdProjId) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $return = $CI->Front_model->getprojectid_tstobd($bdProjId);
    return $return;
}

function getsingleyearmonthAtten($bdProjId, $empId, $month, $year) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $return = $CI->Front_model->getsingleyearmonthAtten($bdProjId, $empId, $month, $year);
    return ($return) ? $return : "0";
}

//Return Month
function getDateDiffMonth($from_ymd, $to_ymd) {
    $datetime1 = new DateTime($from_ymd);
    $datetime2 = new DateTime($to_ymd);
    $interval = $datetime2->diff($datetime1);
    return (($interval->format('%y') * 12) + $interval->format('%m'));
}

//Get Sub designation ...
function getTotMMBymmyy($userID, $month, $year) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $return = $CI->Front_model->getTotMMBymmyy($userID, $month, $year);
    return ($return) ? $return : "";
}

//Get Record From monthwise_invc_saveupd_afterlock..
function getRecordInvcmm($BDprojID,$empId){
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $return = $CI->Front_model->getRecordInvcmm($BDprojID, $empId);
    return ($return) ? $return : "";
}


?>
