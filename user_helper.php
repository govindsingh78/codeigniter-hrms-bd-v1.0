<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//code by durgesh for get company id(17-01-2020)
function get_companyIdByname($compname) {
    $CI = & get_instance();
    $CI->db->select('a.fld_id');
    $CI->db->from('main_company as a');
    $CI->db->where(array('a.company_name' => $compname, 'a.status' => '1'));
    $result = $CI->db->get()->row();
    return ($result) ? $result : '';
}


//code by durgesh for base competitor(20-12-2019)
function get_basecomp($projid, $comid) {
    $CI = & get_instance();
    $CI->db->select('a.base_comp_id');
    $CI->db->from('jv_cegexp as a');
    $CI->db->where(array('a.project_id' => $projid, 'a.status' => '1', 'a.base_comp_id' => $comid));
    $CI->db->group_by('a.project_id');
    $result = $CI->db->get()->row();
    return ($result) ? $result : '';
}

//code by durgesh for lead company(20-12-2019)
function get_leadcomp($projid, $comid) {
    $CI = & get_instance();
    $CI->db->select('a.lead_comp_id');
    $CI->db->from('jv_cegexp as a');
    $CI->db->where(array('a.project_id' => $projid, 'a.status' => '1', 'a.base_comp_id' => $comid));
    $CI->db->where("(find_in_set(74, a.lead_comp_id))", NULL, FALSE);
    $CI->db->group_by('a.project_id');
    $result = $CI->db->get()->row();
    return ($result) ? $result : '';
}

//code by durgesh for jv company(20-12-2019)
function get_jvcomp($projid, $comid) {
    $CI = & get_instance();
    $CI->db->select('a.joint_venture');
    $CI->db->from('jv_cegexp as a');
    $CI->db->where(array('a.project_id' => $projid, 'a.status' => '1', 'a.base_comp_id' => $comid));
    $CI->db->where("(find_in_set(74, a.joint_venture))", NULL, FALSE);
    $CI->db->group_by('a.project_id');
    $result = $CI->db->get()->row();
    return ($result) ? $result : '';
}

//code by durgesh for assoc(20-12-2019)
function get_assoccomp($projid, $comid) {
    $CI = & get_instance();
    $CI->db->select('a.asso_comp');
    $CI->db->from('jv_cegexp as a');
    $CI->db->where(array('a.project_id' => $projid, 'a.status' => '1', 'a.base_comp_id' => $comid));
    $CI->db->where("(find_in_set(74, a.asso_comp))", NULL, FALSE);
    $CI->db->group_by('a.project_id');
    $result = $CI->db->get()->row();
    return ($result) ? $result : '';
}

//code by durgesh for LeadJvAssoc record()
function get_LeadjvAssocById($projid) {
    $CI = & get_instance();
    $CI->db->select('a.project_id,a.lead_comp_id,a.joint_venture,a.asso_comp,b.financial_detail');
    $CI->db->from('jv_cegexp as a');
    $CI->db->join('proj_financial_details as b', 'b.bd_projid=a.project_id', 'left');
    $CI->db->where(array('a.project_id' => $projid, 'a.status' => '1'));
    $CI->db->where("(a.base_comp_id='74' OR a.lead_comp_id='74' OR find_in_set(74, a.joint_venture) OR find_in_set(74, a.asso_comp) )", NULL, FALSE);
    $result = $CI->db->get()->result();
    return ($result) ? $result : '';
}

//code by durgesh for comapany name()
function get_companyname($companyid) {
    $CI = & get_instance();
    $CI->db->select('company_name');
    $CI->db->from('main_company');
    $CI->db->where('status', '1');
    $CI->db->where_in('fld_id', $companyid);
    $result = $CI->db->get()->result();
    return ($result) ? $result : '';
}

// code by durgesh
function get_account_generate_invice_id($id) {
    $CI = & get_instance();
    $CI->db->select('*');
    $CI->db->from('accountinfo');
    $CI->db->where(array('project_numberid' => $id));
    $result = $CI->db->get()->row();
    return ($result) ? $result : '';
}

// code by durgesh
function max_date($projid) {
    $CI = & get_instance();
    $CI->db->select_max('invoice_date');
    $CI->db->from("invoicedetail as a");
    $CI->db->join("invoice_after_xlsdownload as b", "a.id= b.invoice_id", 'inner');
    $CI->db->where(array('b.project_id' => $projid, 'b.srpos_no' => 'sub_total'));
    $result = $CI->db->get()->row_array();
    return ($result) ? $result : '';
}

//code by durgesh for difference the two date
function datediffgap($date1, $date2) {
//$date2 = date("Y-m-d");
    $diff = abs(strtotime($date2) - strtotime($date1));
    $years = floor($diff / (365 * 60 * 60 * 24));
    $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
    $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
    $dumArr = array("0" => $years, "1" => $months, "2" => $days);
    return $dumArr;
}

//code by durgesh for monthly report of acount dashbaoard
function Get_key_completition_date($projid) {
    $CI = & get_instance();
    $CI->db->select('date_stipulatedcompletion');
    $CI->db->from("proj_keydate_details");
    $CI->db->where('bd_projid', $projid);
    $result = $CI->db->get()->row_array();
    if ($result):
        return ($result) ? $result : '';
    endif;
}

//code by durgesh for monthly report of acount dashbaoard
function monthly_report_project($projid, $yearmonth) {
    // $final = date("Y-m", strtotime("-1 month", $yearmonth));
    //$effectiveDate = strtotime(date("Y-m", strtotime($yearmonth)) . "-1 months");
    $CI = & get_instance();
    $CI->db->select('a.invoice_date,b.current_month_bill');
    $CI->db->from("invoicedetail as a");
    $CI->db->join("invoice_after_xlsdownload as b", "a.id= b.invoice_id", 'left');
    $CI->db->like('a.invoice_date', $yearmonth);
    $CI->db->where(array('b.project_id' => $projid, 'b.srpos_no' => 'sub_total'));
    $result = $CI->db->get()->row_array();
    return ($result) ? $result : '';
}

//code by durgesh for monthly report of acount dashbaoard
function total_monthly_report($projid, $date) {
    $CI = & get_instance();
    $CI->db->select('a.current_month_bill,b.invoice_date');
    $CI->db->from("invoice_after_xlsdownload as a");
    $CI->db->join('invoicedetail as b', 'b.id=a.invoice_id', 'inner');
    $CI->db->where(array('a.project_id' => $projid, 'a.srpos_no' => 'sub_total'));
    $CI->db->like('b.invoice_date', $date);
    $result = $CI->db->get()->result();
    return ($result) ? $result : '';
}

//code by durgesh for value convert in word
function numberconvert1($number) {
    $no = round($number);
    $point = round($number - $no, 2) * 100;
    $hundred = null;
    $digits_1 = strlen($no);
    $i = 0;
    $str = array();
    $words = array('0' => '', '1' => 'one', '2' => 'two',
        '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
        '7' => 'seven', '8' => 'eight', '9' => 'nine',
        '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
        '13' => 'thirteen', '14' => 'fourteen',
        '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
        '18' => 'eighteen', '19' => 'nineteen', '20' => 'twenty',
        '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
        '60' => 'sixty', '70' => 'seventy',
        '80' => 'eighty', '90' => 'ninety');
    $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
    while ($i < $digits_1) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += ($divider == 10) ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] .
                    " " . $digits[$counter] . $plural . " " . $hundred :
                    $words[floor($number / 10) * 10]
                    . " " . $words[$number % 10] . " "
                    . $digits[$counter] . $plural . " " . $hundred;
        } else
            $str[] = null;
    }
    $str = array_reverse($str);
    $result = implode('', $str);
    $points = ($point) ?
            "." . $words[$point / 10] . " " .
            $words[$point = $point % 10] : '';
    $recReturn = "( " . $result . "Rupees Only )";
    return ucwords($recReturn);
}

//Login User Name
function loginUserName() {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $CI->load->model('mastermodel');
    $CI->load->model('SecondDB_model');

    $details = $CI->SecondDB_model->loginUserName();
    return ($details) ? $details : '';
}

//Tender Scope..
function tendersope($key) {
    $resArr = array('New_project' => 'New',
        'Active_project' => 'Active',
        'InActive_project' => '<span style="color:red">In-Active</span>',
        'Important_project' => 'Important',
        'Not_Important_project' => '<span style="color:red">Not Important</span>',
        'In_Review_project' => 'In Review',
        'To_Go_project' => 'To Go',
        'No_Go_project' => '<span style="color:red">No Go</span>',
        'Bid_project' => 'Sumitted',
        'No_Submit_project' => '<span style="color:red">No Submitted</span>');
    return ($resArr[$key]) ? $resArr[$key] : 'Undefine';
}

//Get Team Req Row By ID..
function teamreq_RowById($I) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->Team_Req_RowById($I);
    return ($details) ? $details : '';
}

//Login User Dept
function loginUserDept() {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->loginUserDept();
    return ($details) ? $details : '';
}

//User Name By Id..
function UserNameById($userId) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    //$details = $CI->Front_model->UserNameById($userId);
    $details = $CI->SecondDB_model->getUserByID($userId);
    return $details;
}

//Other Emp Name By Id..
function UserOtherNameById($userId) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->UserOtherNameById($userId);
    return $details;
}

//Designation Name By Id..
function DesignationNameById($userId) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->DesignationNameById($userId);
    return $details;
}

//current_url
function thisurl() {
    $actual_link = (isset($_SERVER['HTTP']) ? "http" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    return $actual_link;
}

//Get Login User Role..
function loginUserRole() {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->loginUserRole();
    return ($details) ? $details : '';
}

//All User Record..
function GetAllUserRec() {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->GetAllBdUserRec();
    return ($details) ? $details : '';
}

//All Other Employee
function GetAllOtherUserRec() {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->team_req_otherempAll();
    return ($details) ? $details : '';
}

//Get Tender Details By Id..
//All User Record..
function GetProjectNameById($tndrid) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->GettenderNameById($tndrid);
    return ($details) ? $details : '';
}

//All Prefix Master..
function GetAllPrefixRec() {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->GetAllBdPrefixRec();
    return ($details) ? $details : '';
}

//Get All Notified UserID..
function NotifiedUserIDs($userId) {
    $CI = & get_instance();
    $UserIdArr = array('296' => '296', '287' => '287', '190' => '190', '260' => '260', '175' => '175', '346' => '346', '257' => '257', '256' => '256', '191' => '191', '259' => '259', '211' => '211');
    unset($UserIdArr[$userId]);
    return $UserIdArr;
}

//My Notification..
function GetAllNotification() {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $notifArrRec = $CI->Front_model->GetAllNotification();
    $returnRowArr = array();
    if ($notifArrRec) {
        foreach ($notifArrRec as $notifRow) {
            $notifRow['tendername'] = $CI->Front_model->GetProjectNameById($notifRow['event_id_project']);
            $notifRow['eventby'] = UserNameById($notifRow['user_who_fired_event']);
            array_push($returnRowArr, $notifRow);
        }
    }
    return $returnRowArr;
}

//Get Project Name By ID..
function ProjNameById($pid) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $RecNM = $CI->Front_model->GetProjectNameById($pid);
    return ($RecNM) ? $RecNM : '';
}

//Total No of Notification..
function CountAllNotification() {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $notifArrRec = $CI->Front_model->CountAllNotification();
    return $notifArrRec;
}

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if (getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if (getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if (getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if (getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if (getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

//Bid Project By ProjectId..
function BidProjectById($projectid) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->BidProjectById($projectid);
    return $details;
}

function bd_rolecheck() {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->bd_rolecheck();
    return ($details) ? $details : '';
}

//Get No of Company for jv..
function getnocompjv($base_compid) {
    $CI = & get_instance();
    $norec = $CI->mastermodel->count_allByCond('jv_cegexp', array('base_comp_id' => $base_compid, 'status' => '1'));
    return ($norec) ? $norec : '0';
}

//Code By Asheesh For Get Rfp Enterted By Proposal Manager..
function GetRfpByPid_desId($pid, $designID) {
    $CI = & get_instance();
    $respon = $CI->mastermodel->GetRfpByPm($pid, $designID);
    return ($respon) ? $respon : false;
}

//Mail Lebraray
function sendMail($to, $subject, $msgDetails) {
    //return true;
    $CI = & get_instance();
    $CI->load->library('email');
    $CI->email->initialize(array(
        'protocol' => 'smtp',
        'smtp_host' => 'mail.cegindia.com',
        'smtp_user' => 'marketing@cegindia.com',
        'smtp_pass' => 'MARK-2015ceg',
        'smtp_port' => 587,
        'crlf' => "\r\n",
        'newline' => "\r\n",
        'mailtype' => 'html',
        'charset' => 'iso-8859-1',
        'wordwrap' => TRUE
    ));
    $CI->email->from('tender@cegindia.com', 'Do_not_reply_Mail');
    $CI->email->to($to);
    $CI->email->cc('marketing@cegindia.com');
    $CI->email->bcc('marketing@cegindia.com');
    $CI->email->subject($subject);
    $CI->email->message($msgDetails);
    $resp = $CI->email->send();
    return ($resp) ? $resp : $CI->email->print_debugger();
}

//Mail Lebraray


function sendcomptitorMail($to, $subject, $msgDetails) {
    //return true;
    $CI = & get_instance();
    $CI->load->library('email');
    $CI->email->initialize(array(
        'protocol' => 'smtp',
        'smtp_host' => 'mail.cegindia.com',
        'smtp_user' => 'marketing@cegindia.com',
        'smtp_pass' => 'MARK-2015ceg',
        'smtp_port' => 587,
        'crlf' => "\r\n",
        'newline' => "\r\n",
        'mailtype' => 'html',
        'charset' => 'iso-8859-1',
        'wordwrap' => TRUE
    ));

    $CI->email->from('tender@cegindia.com', 'Do_not_reply');
    //$CI->email->from('durgeshgarg32@gmail.com', 'Do_not_reply');
    $CI->email->to($to);
    $cc = array('marketing@cegindia.com', 'jobs@cegindia.com', 'gmcru@cegindia.com');
    //$cc = array('marketing@cegindia.com');
    $CI->email->cc($cc);
    $CI->email->bcc('marketing@cegindia.com');
    $CI->email->subject($subject);
    $CI->email->message($msgDetails);
    $resp = $CI->email->send();
    return ($resp) ? $resp : $CI->email->print_debugger();
}

function sendcomptitorProjectMail($to, $subject, $msgDetails) {
    //return true;
    $CI = & get_instance();
    $CI->load->library('email');
    $CI->email->initialize(array(
        'protocol' => 'smtp',
        'smtp_host' => 'mail.cegindia.com',
        'smtp_user' => 'marketing@cegindia.com',
        'smtp_pass' => 'MARK-2015ceg',
        'smtp_port' => 587,
        'crlf' => "\r\n",
        'newline' => "\r\n",
        'mailtype' => 'html',
        'charset' => 'iso-8859-1',
        'wordwrap' => TRUE
    ));

    $CI->email->from('tender@cegindia.com', 'Do_not_reply');
    //$CI->email->from('durgeshgarg32@gmail.com', 'Do_not_reply');
    $CI->email->to($to);
    //$cc = array('marketing@cegindia.com');
    $cc = array('ceg@cegindia.com');
    $CI->email->cc($cc);
    $CI->email->bcc('marketing@cegindia.com');
    //$CI->email->bcc('jitendra00752@gmail.com');
    $CI->email->subject($subject);
    $CI->email->message($msgDetails);
    $resp = $CI->email->send();
    return ($resp) ? $resp : $CI->email->print_debugger();
}

function getCegScore() {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->bd_rolecheck();
    return ($details) ? $details : '';
}

function getProjectDetail($id) {
    $CI = & get_instance();
    $CI->db->select('*');
    $CI->db->from('project_detail');
    $CI->db->where('is_active', '1');
    $CI->db->where('cegexp_id', $id);
    $Rec = $CI->db->get()->result();
    if ($Rec) {
        return $Rec;
    } else {
        return false;
    }
}

function getsideMenubyrole($roleid) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->getsideMenubyrole($roleid);
    return ($details) ? $details : '';
}

function getsidesubmenu($menuid, $roleid) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->getsidesubmenu($menuid, $roleid);
    return ($details) ? $details : '';
}

function gettopMenubyrole($roleid) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->gettopMenubyrole($roleid);
    return ($details) ? $details : '';
}

function gettopsubmenu($menuid, $roleid) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->gettopsubmenu($menuid, $roleid);
    return ($details) ? $details : '';
}

function getsubMenu($menuid) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->getsubMenu($menuid);
    return ($details) ? $details : '';
}

function getuserDetail($projectID, $userID) {
    $CI = & get_instance();
    $CI->db->select("*");
    $CI->db->from("finalteamdata");
    $CI->db->where("project_id", $projectID);
    $CI->db->where("emp_id", $userID);
    $result = $CI->db->get()->result_object();
    return isset($result) ? $result : false;
}

function getProjectBYId($id) {
    $CI = & get_instance();
    $details = $CI->SecondDB_model->getProjectBYId($id);
    return ($details) ? $details : '';
}

function getcountryById($id) {
    $CI = & get_instance();
    $CI->db->select("country_name");
    $CI->db->from("countries");
    $CI->db->where("country_id", $id);
    $result = $CI->db->get()->result_object();
    return isset($result) ? $result : false;
}

function getstateById($id) {
    $CI = & get_instance();
    $CI->db->select("state_name");
    $CI->db->from("states");
    $CI->db->where("state_id", $id);
    $result = $CI->db->get()->result_object();
    return isset($result) ? $result : false;
}

function getcityById($id) {
    $CI = & get_instance();
    $CI->db->select("city_name");
    $CI->db->from("cities");
    $CI->db->where("city_id", $id);
    $result = $CI->db->get()->result_object();
    return isset($result) ? $result : false;
}

function getinvoicedata($projectid, $invoiceno) {
    $CI = & get_instance();
    $CI->db->select("*");
    $CI->db->from("invoicedetail");
    $CI->db->where("project_numberid", $projectid);
    $CI->db->where("invoice_no", $invoiceno);
    $result = $CI->db->get()->result_object();
    return isset($result) ? $result : false;
}

function geteotbasic($projectid) {
    $CI = & get_instance();
    $CI->db->select("*");
    $CI->db->from("acceot_date");
    $CI->db->where("project_id", $projectid);
    $CI->db->where("statusid", 0);
    $result = $CI->db->get()->result_object();
    return isset($result) ? $result : false;
}

function gettransportation($projectid, $type) {
    $CI = & get_instance();
    $CI->db->select("*");
    $CI->db->from("acc_msterdetail");
    $CI->db->where("project_id", $projectid);
    $CI->db->where("type_id", $type);
    $CI->db->where("status", 1);
    $result = $CI->db->get()->result_object();
    return isset($result) ? $result : false;
}

function getAssignuserinproject($projectID, $key_id) {
    $CI = & get_instance();
    $details = $CI->Front_model->getAssignuserinproject($projectID, $key_id);
    return ($details) ? $details : '';
}

function geteotemployee($projectID, $eotid, $key_id) {
    $CI = & get_instance();
    $details = $CI->Front_model->geteotemployee($projectID, $eotid, $key_id);
    return ($details) ? $details : '';
}

function getcurrenteotemployee($projectID, $eotid, $designation_id) {
    $CI = & get_instance();
    $details = $CI->Front_model->getcurrenteotemployee($projectID, $eotid, $designation_id);
    return ($details) ? $details : '';
}

function getreplacemendata($id) {
    $CI = & get_instance();
    $details = $CI->Front_model->getreplacemendata($id);
    return ($details) ? $details : '';
}

function getpreviousmonth($projectID, $invoiceID, $key_id) {
    $CI = & get_instance();
    $details = $CI->Front_model->getpreviousmonth($projectID, $invoiceID, $key_id);
    return ($details) ? $details : '';
}

function getcurrentmonth($projectID, $invoiceID, $key_id) {
    $CI = & get_instance();
    $details = $CI->Front_model->getcurrentmonth($projectID, $invoiceID, $key_id);
    return ($details) ? $details : '';
}

function getotherpreviousmonth($projectID, $invoiceID, $masterdetail_id, $invoicedate) {
    $CI = & get_instance();
    $details = $CI->Front_model->getotherpreviousmonth($projectID, $invoiceID, $masterdetail_id, $invoicedate);
    return ($details) ? $details : '';
}

function getothercurrentmonth($projectID, $invoiceID, $masterdetail_id, $invoicedate) {
    $CI = & get_instance();
    $details = $CI->Front_model->getothercurrentmonth($projectID, $invoiceID, $masterdetail_id, $invoicedate);
    return ($details) ? $details : '';
}

function eotdetail($projectID, $eot_id, $masterdetail_id) {
    $CI = & get_instance();
    $CI->db->select("*");
    $CI->db->from("eot_detail");
    $CI->db->where("project_id", $projectID);
    $CI->db->where("eot_id", $eot_id);
    $CI->db->where("masterdetail_id", $masterdetail_id);
    $result = $CI->db->get()->result_object();
    return isset($result) ? $result : false;
}

function othermonthdetail($month_id, $masterdetail_id) {
    $CI = & get_instance();
    $CI->db->select("*");
    $CI->db->from("acc_othermonthdetail");
    $CI->db->where("month_id", $month_id);
    $CI->db->where("masterdetail_id", $masterdetail_id);
    $result = $CI->db->get()->result_object();
    return isset($result) ? $result : false;
}

function eotteamdetail($projectID, $eot_id, $assign_id) {
    $CI = & get_instance();
    $CI->db->select("*");
    $CI->db->from("saveeotteam");
    $CI->db->where("project_id", $projectID);
    $CI->db->where("eot_id", $eot_id);
    $CI->db->where("assign_id", $assign_id);
    $result = $CI->db->get()->result_object();
    return isset($result) ? $result : false;
}

function geteotdetail($projectid) {
    $CI = & get_instance();
    $CI->db->select("*");
    $CI->db->from("acceot_date");
    $CI->db->where("project_id", $projectid);
    $CI->db->where("statusid", 1);
    $CI->db->order_by("id", 'desc');

    $result = $CI->db->get()->result_object();
    return isset($result) ? $result : false;
}

function geteotdetailLastID($projectid) {
    $CI = & get_instance();
    $CI->db->select("*");
    $CI->db->from("acceot_date");
    $CI->db->where("project_id", $projectid);
    $CI->db->where("statusid", 1);
    $CI->db->order_by("id", 'desc');
    $CI->db->limit(1);

    $result = $CI->db->get()->result_object();
    return isset($result) ? $result : false;
}

function monthdetail($month_id, $assign_id) {
    $CI = & get_instance();
    $CI->db->select("*");
    $CI->db->from("acc_monthdetail");
    $CI->db->where("month_id", $month_id);
    $CI->db->where("assign_id", $assign_id);
    $result = $CI->db->get()->result_object();
    return isset($result) ? $result : false;
}

function getuseattd($project_id, $empid, $designation_id, $month, $year) {
    $CI = & get_instance();
    $details = $CI->Front_model->getuseattd($project_id, $empid, $designation_id, $month, $year);
    return ($details) ? $details : '';
}

function getmonthattdence($month, $year, $project_id, $empid, $designation_id) {
    $CI = & get_instance();
    $details = $CI->Front_model->getmonthattdence($month, $year, $project_id, $empid, $designation_id);
    return ($details) ? $details : '';
}

function getemptimesheet($emp_id, $designation_id, $project_id, $selectdate) {
    $CI = & get_instance();
    $details = $CI->Front_model->getemptimesheet($emp_id, $designation_id, $project_id, $selectdate);
    return ($details) ? $details : '';
}

function checktaxID($projectid, $invoiceid, $taxID) {
    $CI = & get_instance();
    $details = $CI->Front_model->checktaxID($projectid, $invoiceid, $taxID);
    return ($details) ? $details : '';

    // $CI->db->select('*');
    // $CI->db->from('project_tax');
    // $CI->db->where('project_id',$projectid);
    // $CI->db->where('invoice_id',$invoiceid);
    // $CI->db->where('tax_id',$taxID);
    // $result = $CI->db->get()->result_object();
    // return isset($result) ? $result : false;
}

function getCumulativemm($projectID, $designation_id) {
    $CI = & get_instance();
    $details = $CI->Front_model->getCumulativemm($projectID, $designation_id);
    return ($details) ? $details : '';
}

function getintermittent($projectID, $empID) {
    $CI = & get_instance();
    $details = $CI->Front_model->getintermittent($projectID, $empID);
    return ($details) ? $details : '';
}

function getintermittentattd($project_id, $empid, $designation_id, $month, $year) {
    $CI = & get_instance();
    $details = $CI->Front_model->getintermittentattd($project_id, $empid, $designation_id, $month, $year);
    return ($details) ? $details : '';
}

function date_sort($a, $b) {
    return strtotime($a) - strtotime($b);
}

function getemptimesheetattd($emp_id, $designation_id, $project_id, $selectdate) {
    $CI = & get_instance();
    $details = $CI->Front_model->getemptimesheetattd($emp_id, $designation_id, $project_id, $selectdate);
    return ($details) ? $details : '';
}

function checkinvoiceexists($invoice_no, $project_id) {
    $CI = & get_instance();
    $details = $CI->Front_model->checkinvoiceexists($invoice_no, $project_id);
    return ($details) ? $details : '';
}

function checpercentage($projectid, $invoiceid, $typeid) {
    $CI = & get_instance();
    $details = $CI->Front_model->checpercentage($projectid, $invoiceid, $typeid);
    return ($details) ? $details : '';
}

function getProjNameByID($pid) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $RecNM = $CI->Front_model->getProjNameByID($pid);
    return ($RecNM) ? $RecNM : '';
}

//Get All Intermittent Project List Employee Ids Wise..
function GetAllIntermittentProjectListEmpIdWise($Empid) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $RecNM = $CI->Front_model->GetAllIntermittentProjectListEmpIdWise($Empid);
    return ($RecNM) ? $RecNM : '';
}

function GetColorCodeWithCond($AT_project_id, $AT_designation_id, $AT_emp_id = "", $AT_year, $AT_month, $fullDate) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $RecNM = $CI->Front_model->GetColorCodeWithCond($AT_project_id, $AT_designation_id, $AT_emp_id, $AT_year, $AT_month, $fullDate);
    return ($RecNM) ? $RecNM : 'black';
}

function GetCompAlldetails($projId, $baseCompId, $typS) {
    $CI = & get_instance();
    $CI->db->select("base_comp_id,lead_comp_id,joint_venture,asso_comp");
    $CI->db->from("jv_cegexp");
    $CI->db->where(array("project_id" => $projId, "base_comp_id" => $baseCompId, "status" => "1"));
    $CI->db->order_by("fld_id", 'DESC');
    $CI->db->limit(1);
    $restRow = $CI->db->get()->row();
    if ($restRow) {
        $arrayLeadsID = explode(",", $restRow->lead_comp_id);
        $arrayjoint_ventureID = explode(",", $restRow->joint_venture);
        $arrayasso_compID = explode(",", $restRow->asso_comp);
        //01..
        if ((count($arrayLeadsID) > 0) and ( $typS == 1)):
            $CI->db->SELECT("company_name");
            $CI->db->FROM("main_company");
            $CI->db->where_in("fld_id", $arrayLeadsID);
            $restArrRow1 = $CI->db->get()->result();
            $D1 = '';
            if ($restArrRow1) {
                foreach ($restArrRow1 as $rEcord1) {
                    $D1 .= $rEcord1->company_name . " , ";
                }
                return $D1 . "<b>LEAD</b>";
            }
        endif;
        //02..
        if ((count($arrayjoint_ventureID) > 0) and ( $typS == 2)):
            $CI->db->SELECT("company_name");
            $CI->db->FROM("main_company");
            $CI->db->where_in("fld_id", $arrayjoint_ventureID);
            $restArrRow2 = $CI->db->get()->result();
            $D2 = '';
            if ($restArrRow2) {
                foreach ($restArrRow2 as $rEcord2) {
                    $D2 .= $rEcord2->company_name . " , ";
                }
                return $D2 . "<b>JV</b>";
            }
        endif;
        //03..
        if ((count($arrayasso_compID) > 0) and ( $typS == 3)):
            $CI->db->SELECT("company_name");
            $CI->db->FROM("main_company");
            $CI->db->where_in("fld_id", $arrayasso_compID);
            $restArrRow3 = $CI->db->get()->result();
            $D3 = '';
            if ($restArrRow3) {
                foreach ($restArrRow3 as $rEcord3) {
                    $D3 .= $rEcord3->company_name . " , ";
                }
                return $D3 . "<b>ASSOCIATE</b>";
            }
        endif;
    }
}

function GetAllSundyDateList($month, $year) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $DayArrList = $CI->Front_model->GetAllSundayList($month, $year);
    return $DayArrList;
}

function GetCompAlldetailsnewash($projId, $typS) {
    $CI = & get_instance();
    $CI->db->select("base_comp_id,lead_comp_id,joint_venture,asso_comp");
    $CI->db->from("jv_cegexp");
    $CI->db->where(array("project_id" => $projId, "status" => "1"));
    $CI->db->where("(base_comp_id='74' OR lead_comp_id='74' OR find_in_set(74, joint_venture) OR find_in_set(74, asso_comp) )", NULL, FALSE);
    $CI->db->order_by("fld_id", 'DESC');
    $CI->db->limit(1);
    $restRow = $CI->db->get()->row();
    if ($restRow) {
        $arrayLeadsID = explode(",", $restRow->lead_comp_id);
        $arrayjoint_ventureID = explode(",", $restRow->joint_venture);
        $arrayasso_compID = explode(",", $restRow->asso_comp);
        //01..
        if ((count($arrayLeadsID) > 0) and ( $typS == 1)):
            $CI->db->SELECT("company_name");
            $CI->db->FROM("main_company");
            $CI->db->where_in("fld_id", $arrayLeadsID);
            $restArrRow1 = $CI->db->get()->result();
            $D1 = '';
            if ($restArrRow1) {
                foreach ($restArrRow1 as $rEcord1) {
                    $D1 .= $rEcord1->company_name . " , ";
                }
                return $D1 . "<b>LEAD</b>";
            }
        endif;
        //02..
        if ((count($arrayjoint_ventureID) > 0) and ( $typS == 2)):
            $CI->db->SELECT("company_name");
            $CI->db->FROM("main_company");
            $CI->db->where_in("fld_id", $arrayjoint_ventureID);
            $restArrRow2 = $CI->db->get()->result();
            $D2 = '';
            if ($restArrRow2) {
                foreach ($restArrRow2 as $rEcord2) {
                    $D2 .= $rEcord2->company_name . " , ";
                }
                return $D2 . "<b>JV</b>";
            }
        endif;
        //03..
        if ((count($arrayasso_compID) > 0) and ( $typS == 3)):
            $CI->db->SELECT("company_name");
            $CI->db->FROM("main_company");
            $CI->db->where_in("fld_id", $arrayasso_compID);
            $restArrRow3 = $CI->db->get()->result();
            $D3 = '';
            if ($restArrRow3) {
                foreach ($restArrRow3 as $rEcord3) {
                    $D3 .= $rEcord3->company_name . " , ";
                }
                return $D3 . "<b>ASSOCIATE</b>";
            }
        endif;
    }
}

function GetKeyTeamList($projId, $keyID) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    $RecArrList = $CI->Accountdept_model->GetKeyTeamList($projId, $keyID);
    return ($RecArrList) ? $RecArrList : null;
}

//function GetKey_SubTeamList($projId) {
//    $CI = & get_instance();
//    $CI->load->model('Accountdept_model');
//    $RecArrList = $CI->Accountdept_model->GetKey_SubTeamList($projId);
//    return ($RecArrList) ? $RecArrList : null;
//}
//Reimb
function GetReimbursableList($projId, $keyID) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    $RecArrList = $CI->Accountdept_model->GetReimbursableList($projId, $keyID);
    return ($RecArrList) ? $RecArrList : null;
}

function numberToRomanRepresentation($number) {
    $CI = & get_instance();
    $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
    $returnValue = '';
    while ($number > 0) {
        foreach ($map as $roman => $int) {
            if ($number >= $int) {
                $number -= $int;
                $returnValue .= $roman;
                break;
            }
        }
    }
    return $returnValue;
}

function getUserfullnameHrms($userId) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    //$details = $CI->Front_model->UserNameById($userId);
    $details = $CI->SecondDB_model->getUserfullnameHrms($userId);
    return $details;
}

function GetStateNameById($state_id) {
    $CI = & get_instance();
    $CI->db->SELECT('state_name');
    $CI->db->FROM('states');
    $CI->db->WHERE(array('state_id' => $state_id));
    $StateDetailArr = $CI->db->get()->row();
    return ($StateDetailArr) ? $StateDetailArr->state_name : null;
}

function GetRembursableTotAmntDetail($project_id, $typeID) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    //static Code For Jind Proj Transpor
    if ($project_id == "89169" and $typeID == "1"):
        $details = $CI->Accountdept_model->GetRembursableTotAmntDetail_new($project_id, $typeID);
    else:
        $details = $CI->Accountdept_model->GetRembursableTotAmntDetail($project_id, $typeID);
    endif;
    return $details;
}

//Get Reimbursable Cumulative up to Previous Bill..
function ReimbursableCumulativePreviousBill($bdprojId, $typerId) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    $details = $CI->Accountdept_model->ReimbursableCumulativePreviousBill($bdprojId, $typerId);
    return $details;
}

//Get Reimbursable Cumulative up to Previous Bill..
function ReimbursableCumulativeCurrentBill($bdprojId, $typerId) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    $details = $CI->Accountdept_model->ReimbursableCumulativeCurrentBill($bdprojId, $typerId);
    return $details;
}

function SingleReimbursableCumulativeCurrentBill($bdprojId, $typerId, $masterdetail_id) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    // $details = $CI->Accountdept_model->SingleReimbursableCumulativeCurrentBill($bdprojId, $typerId, $masterdetail_id);
    $details = $CI->Accountdept_model->SingleReimbursableCumulativeCurrentBill($bdprojId, $masterdetail_id);
    return $details;
}

//Get Holidays Sunday Count ..
function getSunHolidaysArr($month, $year, $projID) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    $details = $CI->Accountdept_model->getSunHolidaysArr($month, $year, $projID);
    return $details;
}

//Get Holidays Sunday Count..
function getHolidaysArr($month, $year, $projID) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    $details = $CI->Accountdept_model->getHolidaysArr($month, $year, $projID);
    return $details;
}

//Get Present Days..
function gettimesheetdetail($project_id, $designation_id, $empid, $month, $year) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->gettimesheetdetail($project_id, $designation_id, $empid, $month, $year);
    return $details;
}

//Cumulative up to Previous Bill.. Single Employee Wise..
function Emp_CumulativuptoPrevAmnt($Project_id, $invoice_no, $DesignationID, $emplID) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    $details = $CI->Accountdept_model->Emp_CumulativuptoPrevAmnt($Project_id, $invoice_no, $DesignationID, $emplID);
    return $details;
}

// Get Total in Reimbursable Current Months..
function GetTotReimbursableCurrentMM($bdProjID, $reim_type_id) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    $details = $CI->Accountdept_model->GetTotReimbursableCurrentMM($bdProjID, $reim_type_id);
    return $details;
}

//Get EOT Single Row..
function GetEOTRecRow($project_id, $designationID, $emplID) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    $details = $CI->Accountdept_model->GetEOTRecArr($project_id, $designationID, $emplID);
    return $details;
}

function GetEOTRecReimburRow($project_id, $Reimburid) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    $Reimbursdetail = $CI->Accountdept_model->GetEOTRecReimburRow($project_id, $Reimburid);
    return ($Reimbursdetail) ? $Reimbursdetail : null;
}

//Get Total Reimbursable Value EOT-Total And REVISED CONTRACT VALUE As Per...
function GetToReimbursableEOT_RvContVal($project_id, $typeID) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    $ReimbursAmountArr = $CI->Accountdept_model->GetToReimbursableEOT_RvContVal($project_id, $typeID);
    return ($ReimbursAmountArr) ? $ReimbursAmountArr : null;
}

function numberconvert($number) {
    $CI = & get_instance();
    $no = round($number);
    $point = round($number - $no, 2) * 100;
    $hundred = null;
    $digits_1 = strlen($no);
    $i = 0;
    $str = array();
    $words = array('0' => '', '1' => 'one', '2' => 'two',
        '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
        '7' => 'seven', '8' => 'eight', '9' => 'nine',
        '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
        '13' => 'thirteen', '14' => 'fourteen',
        '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
        '18' => 'eighteen', '19' => 'nineteen', '20' => 'twenty',
        '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
        '60' => 'sixty', '70' => 'seventy',
        '80' => 'eighty', '90' => 'ninety');
    $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
    while ($i < $digits_1) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += ($divider == 10) ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] .
                    " " . $digits[$counter] . $plural . " " . $hundred :
                    $words[floor($number / 10) * 10]
                    . " " . $words[$number % 10] . " "
                    . $digits[$counter] . $plural . " " . $hundred;
        } else
            $str[] = null;
    }
    $str = array_reverse($str);
    $result = implode('', $str);
    $points = ($point) ?
            "." . $words[$point / 10] . " " .
            $words[$point = $point % 10] : '';
    $recReturn = "( " . $result . "Rupees Only )";
    return ucwords($recReturn);
}

//code by durgesh for comapany name()
function CompanyNameByID($companyid) {
    $CI = & get_instance();
    $CI->db->select('company_name');
    $CI->db->from('main_company');
    $CI->db->where('status', '1');
    $CI->db->where_in('fld_id', $companyid);
    $result = $CI->db->get()->row();
    return ($result) ? $result->company_name : '';
}

function GetAllDesignListByDesigCategID($desigCategId) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    $ListRecArr = $CI->Accountdept_model->GetAllDesignListByDesigCategID($desigCategId);
    return ($ListRecArr) ? $ListRecArr : null;
}


function SingleReimbursableCumulativePrev_Bill($bdprojId, $invoiceId, $masterdetail_id) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    $details = $CI->Accountdept_model->SingleReimbursableCumulativePrev_Bill($bdprojId, $invoiceId, $masterdetail_id);
    return $details;
}

function SingleReimbursableCumulativeCurrent_Bill($bdprojId, $invoiceId, $masterdetail_id) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    $details = $CI->Accountdept_model->SingleReimbursableCumulativeCurrent_Bill($bdprojId, $invoiceId, $masterdetail_id);
    return $details;
}


//Get Reimbursable Cumulative up to Previous Bill..
function ReimbursableCumulative_PrevBill($bdprojId, $invoiceID, $typerId) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    $details = $CI->Accountdept_model->ReimbursableCumulative_PrevBill($bdprojId, $invoiceID, $typerId);
    return $details;
}


//Get Reimbursable Cumulative up to Current Total Bill..
function ReimbursableCumulative_CurrentBill($bdprojId, $invoiceID, $typerId) {
    $CI = & get_instance();
    $CI->load->model('Accountdept_model');
    $details = $CI->Accountdept_model->ReimbursableCumulative_CurrentBill($bdprojId, $invoiceID, $typerId);
    return $details;
}


?>
