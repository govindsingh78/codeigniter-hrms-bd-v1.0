<html lang="en">
    <?php include('include/innerhead.php'); ?>
    <?php error_reporting(0); ?>
    <body>
        <!-- topbar starts -->
        <?php include('include/tabbar.php'); ?>
        <!-- topbar ends -->

        <script src="<?= FRONTASSETS; ?>ckeditor/ckeditor.js"></script>
        <link rel="stylesheet" href="<?= FRONTASSETS; ?>style/format.css">

        <div class="wrapper">
			<?php include('include/sidebar.php') ?>
                        
                <div id="content" class="col-lg-10 col-sm-10">
                    <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Success!</strong>  Tender Added Success !
                        </div>
                    <?php } ?>
                    <div>
                        <ul class="breadcrumb">
                            <li> <a href="<?= base_url(); ?>">Home</a></li>
                            <li>
                                <a href="#"><?= $title;?> </a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">
                        <form action="<?= base_url('tender/tender_add'); ?>" method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data mvc">
                            <div class="box col-md-12">

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tender-name">Tender Type <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <select name="tndrType" data-placeholder="Tender/Project  Type" id="selectError2" data-rel="chosen">
                                            <option value="New_project">New Tender </option>
                                            <option value="Active_project">Active Project</option>
                                            <option value="Important_project">Important Project</option>
                                            <option value="In_Review_project">In Review Project</option>
                                            <!--<option value="To_Go_project">To Go Project</option>
                                            <option value="Bid_project">Bid Project</option>  -->
                                        </select>
                                    </div>
                                </div>      

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tender-name">Tender Phase <span class="required">*</span>
                                    </label>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <input type="checkbox" name="tndrphase[]" value="Design"> Design &nbsp;&nbsp;
                                        <input type="checkbox" name="tndrphase[]" value="Construction"> Construction &nbsp;&nbsp;
                                        <input type="checkbox" name="tndrphase[]" value="Electrical"> Electrical &nbsp;&nbsp;
                                        <input type="checkbox" name="tndrphase[]" value="Maintenance"> Maintenance &nbsp;&nbsp;
                                        <input type="checkbox" name="tndrphase[]" value="Other"> Other &nbsp;&nbsp;
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tender-name">Tender Sector <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="tendersector[]" id="selectError1" multiple="multiple" class="form-control" data-rel="chosen">
                                            <?php
                                            if ($activesectArr) {
                                                foreach ($activesectArr as $rowsect):
                                                    ?>
                                                    <option value="<?php echo $rowsect->fld_id; ?>"><?php echo $rowsect->sectName; ?></option>
                                                    <?php
                                                endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tender-name">Tender Name <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <input type="text" id="tendername" name="tendername" required="required" class="form-control col-md-4 col-xs-12 required">
                                    </div>
                                </div>

                                <div class="form-group control-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="deadline">Deadline Date<span class="required">*</span>
                                    </label>
                                    <div class="controls">
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <input type="date" id="single_cal1" name="deadline" required="required" class="form-control col-md-4 col-xs-12 required">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="link-url">Link URL<span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <input type="text" id="linkurl" name="linkurl" required="required" class="form-control col-md-4 col-xs-12 required url">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tender-value">Tender Value </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <input type="text" id="tendervalue" name="tendervalue" class="form-control col-md-4 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="location">Location</label>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <input type="text" id="location" name="location" class="form-control col-md-4 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="location">Located</label>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <select id="national_intern" name="national_intern" class="form-control">
                                            <option value="1">National</option>
                                            <option value="2">International</option>
                                            <option value="3">Other</option> 
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="organisation">Organisation</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" id="organisation" name="organisation" class="form-control col-md-4 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Tender Details">Tender Details</label>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <textarea cols="80" id="edi" name="editor1" rows="10"></textarea>
                                        <script>CKEDITOR.replace('edi');</script>
                                    </div>
                                </div>

                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <input class="btn btn-primary" name="cancel" id="cancel" value="Cancel" type="reset"/>
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            

            <div class="row">
                <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
                </div>
            </div>
            <hr>
            <!-- BootStrap Model For Comments.. -->
            <?php include('include/commenthtml.php'); ?>
            <!-- close Model -->
           
        </div>
		 <?php include('include/footer.php'); ?>
		<?php $this->load->view('include/datatablejs'); ?>
        <!--<script src="<?= FRONTASSETS; ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?= FRONTASSETS; ?>js/jquery.cookie.js"></script>
        <script src='<?= FRONTASSETS; ?>bower_components/moment/min/moment.min.js'></script>
        <script src='<?= FRONTASSETS; ?>bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
        <script src='<?= FRONTASSETS; ?>js/jquery.dataTables.min.js'></script>
        <script src="<?= FRONTASSETS; ?>bower_components/chosen/chosen.jquery.min.js"></script>
        <script src="<?= FRONTASSETS; ?>bower_components/colorbox/jquery.colorbox-min.js"></script>
        <script src="<?= FRONTASSETS; ?>js/jquery.noty.js"></script>
        <script src="<?= FRONTASSETS; ?>bower_components/responsive-tables/responsive-tables.js"></script>
        <script src="<?= FRONTASSETS; ?>bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
        <script src="<?= FRONTASSETS; ?>js/jquery.raty.min.js"></script>
        <script src="<?= FRONTASSETS; ?>js/jquery.iphone.toggle.js"></script>
        <script src="<?= FRONTASSETS; ?>js/jquery.autogrow-textarea.js"></script>
        <script src="<?= FRONTASSETS; ?>js/jquery.uploadify-3.1.min.js"></script>
        <script src="<?= FRONTASSETS; ?>js/jquery.history.js"></script>-->
    </body>
</html>

<?php
//include('include/customjs_last.php');
?>
<style>
    span.required {
        color: red;
    }
</style>
