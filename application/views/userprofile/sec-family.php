<div class="row clearfix">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Sr.No.</th>
                <th>Family Name</th>
                <th>Family Relation</th>
                <th>Family DOB</th>
                <th>Aadhar Number</th>
                <th>Dependent or Not</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($FamilyDetailsRecArr) {
                foreach ($FamilyDetailsRecArr as $kEy => $recD) {
                    ?>
                    <tr>
                        <td><?= $kEy + 1; ?></td>
                        <td><?= ($recD->dependent_name) ? $recD->dependent_name : ""; ?></td>
                        <td><?= ($recD->dependent_relation) ? $recD->dependent_relation : ""; ?></td>
                        <td><?= ($recD->dependent_dob) ? date("d-m-Y", strtotime($recD->dependent_dob)) : ""; ?></td>
                        <td><?= ($recD->aadhar_no) ? $recD->aadhar_no : ""; ?></td>
                        <td><?= ($recD->dependent_select) ? $recD->dependent_select : ""; ?></td>
                        <td>-</td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td style="color:red;text-align:center;" colspan="7"><b>Record Not Found.</b></td>
                </tr>
            <?php } ?>

        </tbody>
    </table>
</div>