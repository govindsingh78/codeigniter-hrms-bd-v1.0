<div class="row clearfix">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Sr.No.</th>
                <th>Education Level</th>
                <th>Institution Name</th>
                <th>Course</th>
                <th>Location</th>
                <th>Specialization</th>
                <th>Passing Year</th>
                <th>Percentage</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($EduDetailsRecArr) {
                foreach ($EduDetailsRecArr as $kEy => $recD) {
                    ?>
                    <tr>
                        <td><?= $kEy + 1; ?></td>
                        <td><?= ($recD->educationlevelcode) ? $recD->educationlevelcode : ""; ?></td>
                        <td><?= ($recD->institution_name) ? $recD->institution_name : ""; ?></td>
                        <td><?= ($recD->course) ? $recD->course : ""; ?></td>
                        <td><?= ($recD->spc_location) ? $recD->spc_location : ""; ?></td>
                        <td><?= ($recD->specialization) ? $recD->specialization : ""; ?></td>
                        <td><?= ($recD->from_date) ? date("Y", strtotime($recD->from_date)) : ""; ?></td>
                        <td><?= ($recD->percentage) ? $recD->percentage : ""; ?></td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td style="color:red" colspan="8"> Record Not Found. </td>
                </tr>
            <?php } ?>

        </tbody>
    </table>
</div>