<div class="row clearfix">
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Mother Name : </label> <br>
            <input type="text" value="<?= ($PersonalDetailRecArr->mother_nm) ? @$PersonalDetailRecArr->mother_nm : "-"; ?>" name="mother_nm" id="mother_nm" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Father Name : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->father_nm) ? @$PersonalDetailRecArr->father_nm : "-"; ?>" name="father_nm" id="father_nm" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Gender : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->genderid) ? @$genderidArr[@$PersonalDetailRecArr->genderid] : "-"; ?>" name="genderid" id="genderid" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Marital Status : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->maritalstatusname) ? @$PersonalDetailRecArr->maritalstatusname : "-"; ?>" name="maritalstatusname" id="maritalstatusname" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Nationality : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->nationalitycode) ? @$PersonalDetailRecArr->nationalitycode : "-"; ?>" name="nationalitycode" id="nationalitycode" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Mother Tongue : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->languagename) ? @$PersonalDetailRecArr->languagename : "-"; ?>" name="languagename" id="languagename" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Date of Birth : </label> <br>
            <input type="text" value="<?= ($PersonalDetailRecArr->dob) ? date("d-m-Y", strtotime(@$PersonalDetailRecArr->dob)) : "-"; ?>" name="dob" id="dob" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Blood Group : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->bloodgroup) ? @$PersonalDetailRecArr->bloodgroup : "-"; ?>" name="bloodgroup" id="bloodgroup" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Aadhar No /Enrolment : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->aadhar_no_enrolment) ? @$PersonalDetailRecArr->aadhar_no_enrolment : "-"; ?>" name="aadhar_no_enrolment" id="aadhar_no_enrolment" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">SkyPee : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->skypee_id) ? @$PersonalDetailRecArr->skypee_id . " " . $ofcRecArr->userfullname_ro : "-"; ?>" name="skypee_id" id="skypee_id" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
</div>