<div class="row clearfix">
    <table class="table table-striped" border="1">
        <tbody>
            <tr>
                <th style="color:#49c5b6" colspan="4"> PERMANENT ADDRESS </th>
            </tr>
            <tr>
                <th>Street Address : </th>
                <td colspan="3"><?= (@$ContactDetailsRecArr->perm_streetaddress) ? @$ContactDetailsRecArr->perm_streetaddress : ""; ?></td>
            </tr>
            <tr>
                <th>Country : </th>
                <td><?= (@$ContactDetailsRecArr->per_country_name) ? @$ContactDetailsRecArr->per_country_name : ""; ?></td>
                <th>State : </th>
                <td><?= (@$ContactDetailsRecArr->perm_state_name) ? @$ContactDetailsRecArr->perm_state_name : ""; ?></td>
            </tr>
            <tr>
                <th>City : </th>
                <td><?= (@$ContactDetailsRecArr->perm_city_name) ? @$ContactDetailsRecArr->perm_city_name : ""; ?></td>
                <th>Pin Code : </th>
                <td><?= (@$ContactDetailsRecArr->perm_pincode) ? @$ContactDetailsRecArr->perm_pincode : ""; ?></td> 
            </tr>
        </tbody>
    </table>

    <table class="table table-striped" border="1">
        <tbody>
            <tr>
                <th style="color:#49c5b6" colspan="4"> CURRENT ADDRESS </th>
            </tr>
            <tr>
                <th>Street Address : </th>
                <td colspan="3"><?= (@$ContactDetailsRecArr->current_streetaddress) ? @$ContactDetailsRecArr->current_streetaddress : ""; ?></td>
            </tr>
            <tr>
                <th>Country : </th>
                <td><?= (@$ContactDetailsRecArr->current_country_name) ? @$ContactDetailsRecArr->current_country_name : ""; ?></td>
                <th>State : </th>
                <td><?= (@$ContactDetailsRecArr->current_state_name) ? @$ContactDetailsRecArr->current_state_name : ""; ?></td>
            </tr>
            <tr>
                <th>City : </th>
                <td><?= (@$ContactDetailsRecArr->current_city_name) ? @$ContactDetailsRecArr->current_city_name : ""; ?></td>
                <th>Pin Code : </th>
                <td><?= (@$ContactDetailsRecArr->current_pincode) ? @$ContactDetailsRecArr->current_pincode : ""; ?></td> 
            </tr>
        </tbody>
    </table>

    <table class="table table-striped" border="1">
        <tbody>
            <tr>
                <th style="color:#49c5b6" colspan="4"> EMERGENCY DETAILS </th>
            </tr>
            <tr>
                <th>Name : </th>
                <td><?= (@$ContactDetailsRecArr->emergency_name) ? @$ContactDetailsRecArr->emergency_name : ""; ?></td>
                <th>Relation : </th>
                <td><?= (@$ContactDetailsRecArr->emergency_namerelation) ? @$ContactDetailsRecArr->emergency_namerelation : ""; ?></td>
            </tr>
            <tr>
                <th>Email : </th>
                <td><?= (@$ContactDetailsRecArr->emergency_email) ? @$ContactDetailsRecArr->emergency_email : ""; ?></td>
                <th>Contact No : </th>
                <td><?= (@$ContactDetailsRecArr->emergency_number) ? @$ContactDetailsRecArr->emergency_number : ""; ?></td>
            </tr>
        </tbody>
    </table>

</div>