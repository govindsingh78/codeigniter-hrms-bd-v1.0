<div class="row clearfix">
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Employee Code : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->employeeId) ? @$ofcRecArr->employeeId : "-"; ?>" name="employeeId" id="employeeId" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Prefix : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->prefix_name) ? @$ofcRecArr->prefix_name : "-"; ?>" name="prefix_name" id="prefix_name" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">First Name : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->firstname) ? @$ofcRecArr->firstname : "-"; ?>" name="firstname" id="firstname" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Last Name : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->lastname) ? @$ofcRecArr->lastname : "-"; ?>" name="lastname" id="lastname" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Official Email : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->emailaddress) ? @$ofcRecArr->emailaddress : "-"; ?>" name="emailaddress" id="emailaddress" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Business Unit : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->businessunit_name) ? @$ofcRecArr->businessunit_name : "-"; ?>" name="businessunit_name" id="businessunit_name" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Reporting Manager (IO) : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->reporting_manager_name) ? @$ofcRecArr->reporting_manager_name : "-"; ?>" name="reporting_manager_name" id="reporting_manager_name" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Reporting Manager (RO) : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->userfullname_ro) ? $ofcRecArr->userfullname_ro : "-"; ?>" name="userfullname_ro" id="userfullname_ro" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Role : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->emprole_name) ? @$ofcRecArr->emprole_name : "-"; ?>" name="emprole_name" id="emprole_name" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Job Group : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->jobtitle_name) ? @$ofcRecArr->jobtitle_name : "-"; ?>" name="jobtitle_name" id="jobtitle_name" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Designation : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->position_name) ? @$ofcRecArr->position_name : "-"; ?>" name="position_name" id="position_name" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Employment Status : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->emp_status_name) ? @$ofcRecArr->emp_status_name : "-"; ?>" name="emp_status_name" id="emp_status_name" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Department : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->department_name) ? @$ofcRecArr->department_name : "-"; ?>" name="department_name" id="department_name" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Sub Department : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->subdepartment) ? @$ofcRecArr->subdepartment : "-"; ?>" name="subdepartment" id="subdepartment" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Date of Joining : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->selecteddate) ? date("d-m-Y", strtotime($ofcRecArr->selecteddate)) : "-"; ?>" name="selecteddate" id="selecteddate" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Company Name : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->company_name) ? @$ofcRecArr->company_name : "-"; ?>" name="company_name" id="company_name" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Years of Experience : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->years_exp) ? @$ofcRecArr->years_exp : "-"; ?>" name="years_exp" id="years_exp" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Mobile Number : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->office_number) ? @$ofcRecArr->office_number : "-"; ?>" name="office_number" id="office_number" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Extension : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->extension_number) ? @$ofcRecArr->extension_number : "-"; ?>" name="extension_number" id="extension_number" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Probation Period : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->probation_period_no) ? @$ofcRecArr->probation_period_no : "-"; ?>" name="probation_period_no" id="probation_period_no" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Notice Period : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->noticeperiod) ? @$ofcRecArr->noticeperiod : "-"; ?>" name="noticeperiod" id="noticeperiod" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Date of Leaving : </label> <br>
            <input type="text" value="<?= ($ofcRecArr->date_of_leaving) ? @$ofcRecArr->date_of_leaving : "-"; ?>" name="date_of_leaving" id="date_of_leaving" <?= $disAbl; ?> class="form-control">
        </div>
    </div>
</div>