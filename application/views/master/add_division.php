<?php $this->load->view('admin/includes/head'); ?>
<?php $this->load->view('admin/includes/header'); ?>
<?php $this->load->view('admin/includes/sidebar'); ?>
<?php $this->load->view('admin/includes/footer'); ?>
<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-8 col-sm-12">                        
                    <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href=""><i class="icon-home"></i></a></li>                            
                        <li class="breadcrumb-item">Home</li>
                        <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                    </ul>
                </div>            
                <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                    <div class="inlineblock text-center m-r-15 m-l-15 hidden-sm">
                        <div class="sparkline text-left" data-type="line" data-width="8em" data-height="20px" data-line-Width="1" data-line-Color="#00c5dc"
                             data-fill-Color="transparent">3,5,1,6,5,4,8,3</div>
                        <span>Visitors</span>
                    </div>
                    <div class="inlineblock text-center m-r-15 m-l-15 hidden-sm">
                        <div class="sparkline text-left" data-type="line" data-width="8em" data-height="20px" data-line-Width="1" data-line-Color="#f4516c"
                             data-fill-Color="transparent">4,6,3,2,5,6,5,4</div>
                        <span>Visits</span>
                    </div>
                </div>
            </div>
        </div>


        <?php if ($this->session->flashdata('success_msg')): ?>
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Success ! </strong> <?= $this->session->flashdata('success_msg'); ?>
            </div>
        <?php endif; ?>

        <?php if ($this->session->flashdata('error_msg')): ?>
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Error ! </strong> <?= $this->session->flashdata('error_msg'); ?>
            </div>
        <?php endif; ?>

        <?php if (validation_errors()): ?>
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Error ! </strong> <?= validation_errors(); ?>
            </div>
        <?php endif; ?>


        <div class="row clearfix">
            <div class="col-md-12">  
                <div class="card">
                    <div class="header">
                        <h2>Add New State Name</h2>
                    </div>
                    <div class="body">
                        <form action="<?= base_url('division_management'); ?>" id="basic-form" method="post">
                            <div class="row">
                                <div class="col-md-6"> 
                                    <div class="form-group">
                                        <label> Division Name : <span id="reqd"> * </span></label>
                                        <input autocomplete="off" type="text" class="form-control" name="division_name" value="<?= set_value('division_name'); ?>">
                                    </div>
                                </div>
                                <div class="col-md-2"> 
                                    <div class="form-group">
                                        <label> &nbsp; </label> <br>
                                        <button type="submit" class="btn btn-primary"> Add </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>   
                </div>



                <div class="card">
                    <div class="row">
                        <div class="col-md-12"> 
                            <table id="table" class="table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Sr. NO.</th>
                                        <th>Division Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Sr. NO.</th>
                                        <th>Division Name</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


<link href="<?= FRONTASSETS; ?>datatables/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="<?= FRONTASSETS; ?>datatables/css/buttons.dataTables.min.css" rel="stylesheet">
<script src="<?= FRONTASSETS; ?>datatables/js/jquery.dataTables.min.js" ></script>
<script src="<?= FRONTASSETS; ?>datatables/js/dataTables.bootstrap.min.js" ></script>
<script src="<?= FRONTASSETS; ?>datatables/js/dataTables.colVis.js?>" ></script>
<script src="<?= FRONTASSETS; ?>datatables/js/dataTables.buttons.min.js" ></script>
<script src="<?= FRONTASSETS; ?>datatables/js/buttons.flash.min.js" ></script>
<script src="<?= FRONTASSETS; ?>datatables/js/pdfmake.min.js" ></script>
<script src="<?= FRONTASSETS; ?>datatables/js/jszip.min.js" ></script>
<script src="<?= FRONTASSETS; ?>datatables/js/vfs_fonts.js" ></script>
<script src="<?= FRONTASSETS; ?>datatables/js/buttons.html5.min.js"></script>
<script src="<?= FRONTASSETS; ?>datatables/js/buttons.print.min.js"></script> 
<script src="<?= FRONTASSETS; ?>datatables/js/jquery.cookie.min.js"></script> 

<script>
    var table;
    $(document).ready(function () {
        table = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?= base_url('division_data_ajax') ?>",
                "type": "POST",
                "data": function (data) {
                },
            },
            "dom": 'lBfrtip',
            "buttons": [{extend: 'collection', text: 'Export', buttons: ['copy', 'excel', 'csv', 'pdf', 'print']}],
            "columnDefs": [{"targets": [0], "orderable": false,
                },
            ],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });
        var colvis = new $.fn.dataTable.ColVis(table);
        $('#colvis').html(colvis.button());
        $('#btn-filter').click(function () {
            table.ajax.reload();
        });
        $('#btn-reset').click(function () {
            $('#form-filter')[0].reset();
            table.ajax.reload();
        });
    });
</script>
