<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$this->load->view('admin/includes/sidebar');
$this->load->view('admin/includes/footer');
?>
<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-8 col-sm-12">                        
                    <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href=""><i class="icon-home"></i></a></li>                            
                        <li class="breadcrumb-item">Home</li>
                        <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                    </ul>
                </div>            
                <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                    <div class="inlineblock text-center m-r-15 m-l-15 hidden-sm">
                        <div class="sparkline text-left" data-type="line" data-width="8em" data-height="20px" data-line-Width="1" data-line-Color="#00c5dc"
                             data-fill-Color="transparent">3,5,1,6,5,4,8,3</div>
                        <span>Visitors</span>
                    </div>
                    <div class="inlineblock text-center m-r-15 m-l-15 hidden-sm">
                        <div class="sparkline text-left" data-type="line" data-width="8em" data-height="20px" data-line-Width="1" data-line-Color="#f4516c"
                             data-fill-Color="transparent">4,6,3,2,5,6,5,4</div>
                        <span>Visits</span>
                    </div>
                </div>
            </div>
        </div>


        <?php if ($this->session->flashdata('success_msg')): ?>
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Success ! </strong> <?= $this->session->flashdata('success_msg'); ?>
            </div>
        <?php endif; ?>

        <?php if ($this->session->flashdata('error_msg')): ?>
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Error ! </strong> <?= $this->session->flashdata('error_msg'); ?>
            </div>
        <?php endif; ?>

        <?php if (validation_errors()): ?>
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Error ! </strong> <?= validation_errors(); ?>
            </div>
        <?php endif; ?>


        <div class="row clearfix">
            <div class="col-md-12">  
                <div class="card">
                    <div class="header">
                        <h2>Edit / Update MP Constituency Name</h2>
                    </div>
                    <div class="body">
                        <form action="<?= base_url('edit_mpconstituency/' . $SingleRow->fld_id); ?>" id="basic-form" method="post">
                            <div class="row">
                                <div class="col-md-6"> 
                                    <div class="form-group">
                                        <label class="email">MP Constituency Name : <span id="reqd"> * </span></label>
                                        <input autocomplete="off" type="text" class="form-control" name="mpconstituency_name" value="<?= set_value('mpconstituency_name', $SingleRow->mpconstituency_name); ?>">
                                    </div>
                                </div>
                                <div class="col-md-1"> 
                                    <div class="form-group">
                                        <label> &nbsp; </label> <br>
                                        <button type="submit" class="btn btn-primary"> Update </button>
                                    </div>
                                </div>

                                <div class="col-md-1"> 
                                    <div class="form-group">
                                        <label> &nbsp; </label> <br>
                                        <a href="<?= base_url("mpconstituency_management"); ?>"><button type="button" class="btn btn-primary"> Back </button></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>   
                </div>
            </div>
        </div>
    </div>
</div>
