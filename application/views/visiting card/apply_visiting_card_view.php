<?php


$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');

?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div> 
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="accordion" id="accordion">
                                    <div>
									<form method="post" action="<?= base_url('applyvisitingcard'); ?>" id="visitingcard_form">
                                        <div class="card-header" id="headingOne">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    Apply Visiting Card
                                                </button>
                                            </h5>
                                        </div>                                
                                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                            <!--<div class="header">
                                                <ul class="header-dropdown">
                                                    <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="javascript:void(0);">View Holidays</a></li>
                                                            <li><a href="javascript:void(0);">Apply Tour</a></li>
                                                            <li><a href="javascript:void(0);">Employee Leave</a></li>
                                                            <li><a href="javascript:void(0);">Employee Tour</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="remove">
                                                        <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                                                    </li>
                                                </ul>
                                            </div>-->
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-3 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Date of Initiation </b>
															<input readonly="readonly" name="date_of_initiation" type="text" value="<?php echo date("d-m-Y");?>" id="array-select" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Name</b>
                                                            <input readonly="readonly" name="userfullname" type="text" value="<?= (@$visitingDetailArr->userfullname) ? $visitingDetailArr->userfullname : ""; ?>" id="array-select" class="form-control">
                                                            <input type="hidden" name="approved_by" type="text" value="<?= (@$visitingDetailArr->reporting_manager) ? $visitingDetailArr->reporting_manager : ""; ?>" id="array-select" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Employee No.</b>
                                                            <input readonly="readonly" type="text" step="any" value="<?= (@$visitingDetailArr->employeeId) ? $visitingDetailArr->employeeId : ""; ?>" id="array-select" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Designation</b>
                                                            <input readonly="readonly" name="position_name" type="text" step="any" value="<?= (@$visitingDetailArr->position_name) ? $visitingDetailArr->position_name : ""; ?>" id="array-select" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-6">
                                                        <b>Email ID</b>
                                                        <div class="input-group mb-3">
                                                            <input readonly="readonly" name="emailaddress" type="text" step="any" value="<?= (@$visitingDetailArr->emailaddress) ? $visitingDetailArr->emailaddress : ""; ?>" id="array-select" class="form-control">
                                                        </div>
                                                    </div>
													 <div class="col-lg-3 col-md-6">
                                                        <b>Contact No.</b>
                                                        <div class="input-group mb-3">
                                                            <input readonly="readonly" name="contactnumber" type="text" step="any" value="<?= (@$visitingDetailArr->contactnumber) ? $visitingDetailArr->contactnumber : ""; ?>" id="array-select" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-6">
                                                        <b>Extension No.</b>
                                                        <div class="input-group mb-3">
                                                            <input readonly="readonly" name="extension_number" type="text" step="any" value="<?php if($visitingDetailArr->extension_number == ''){echo "";}else { echo $visitingDetailArr->extension_number;} ?>" id="array-select" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Fax No.</b>
                                                            <input readonly="readonly" name="fax_no" type="text" step="any" value="+911412751806" id="array-select" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-6">
                                                        <div class="mb-2">
                                                            <b></b>
                                                            <input type="button" value="Preview" data-toggle="modal" data-target="#myModal" name="submit" class="btn btn-round btn-primary">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										</form>
                                    </div>
                                </div>
                            </div>
                        </div>
						<?php //echo "<pre>"; print_r($appliedvisitingDetailArr2);?>
						<div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                        <thead>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Name</th>
                                                <th>Employee No.</th>
                                                <th>Designation</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($appliedvisitingDetailArr1) {
                                                foreach ($appliedvisitingDetailArr1 as $kEy => $data) {
													$id = $this->session->userdata('loginid');
												if($id == $data->approved_by || $id == $data->user_id) {?>
                                                    <tr style="">
                                                        <td><?= $kEy + 1; ?></td>
                                                        <td><?= ($data->userfullname) ? $data->userfullname : ""; ?></td>
                                                        <td><?= ($data->employeeId) ? $data->employeeId : ""; ?></td>
                                                        <td><?= ($data->position_name) ? $data->position_name : ""; ?></td>
														<?php if($data->hod == $data->approved_by) {?>
														
                                                        <td><?php if($data->status == 0){?>
														<span onclick="approvevisiting('<?= @$data->user_id; ?>')" style="cursor:pointer;color:green"> Approve</span> / <span onclick="rejectvisiting('<?= @$data->user_id; ?>')" style="cursor:pointer;color:red">Reject</span></td>
														<?php } elseif($data->status == 3){ ?>
															<span style="color:red;">Rejected</span>
														<?php } elseif($data->status == 1){ ?>
															<span style="color:green;">Approved</span>
														<?php } else { ?>
                                                        <td><?php if($data->status == 0){ echo "<span style='color:red'>Pending</span>";} elseif($data->status == 1){ echo "<span style='color:green'>Approved By HOD</span>";}elseif($data->status == 2){ echo "<span style='color:green'>Approved By HRD</span>";}elseif($data->status == 3){ echo "<span style='color:red'>Rejected</span>";} ?></td>
														<?php } }?>
													</tr>
                                                   <?php
                                                }
												}
											}
											elseif($appliedvisitingDetailArr) {
												foreach ($appliedvisitingDetailArr as $kEy => $data) {
													$id = $this->session->userdata('loginid');
													if($id == $data->department_id){
														echo "test"; die;
													}
												if($id == $data->user_id) {?>
                                                    <tr style="">
                                                        <td><?= $kEy + 1; ?></td>
                                                        <td><?= ($data->userfullname) ? $data->userfullname : ""; ?></td>
                                                        <td><?= ($data->employeeId) ? $data->employeeId : ""; ?></td>
                                                        <td><?= ($data->position_name) ? $data->position_name : ""; ?></td>
														<?php //if($data->hod == $data->approved_by) {?>
														
                                                        <!--<td><span onclick="approvevisiting('<?= @$data->user_id; ?>')" style="cursor:pointer;color:green"> Approve</span> / <span onclick="rejectvisiting('<?= @$data->user_id; ?>')" style="cursor:pointer;color:red">Reject</span></td>-->
														<?php //} else { ?>
                                                        <td><?php if($data->status == 0){ echo "<span style='color:red'>Pending</span>";} elseif($data->status == 1){ echo "<span style='color:green'>Approved By HOD</span>";}elseif($data->status == 2){ echo "<span style='color:green'>Approved By HRD</span>";}elseif($data->status == 3){ echo "<span style='color:red'>Rejected</span>";} ?></td>
														<?php //} ?>
													</tr>
                                                   <?php
                                                }
												}
                                            }
											elseif($appliedvisitingDetailArr2) {
												$id = $this->session->userdata('loginid');
												$dptid = get_emp_dpt_data($id);
												// print_r($dptid[0]->department_id); die;
												
												// echo "<pre>";print_r($appliedvisitingDetailArr2);die;
												foreach ($appliedvisitingDetailArr2 as $kEy => $data) {
													$id = $this->session->userdata('loginid');
													if($dptid[0]->department_id == '11'){?>
                                                    <tr style="">
                                                        <td><?= $kEy + 1; ?></td>
                                                        <td><?= ($data->userfullname) ? $data->userfullname : ""; ?></td>
                                                        <td><?= ($data->employeeId) ? $data->employeeId : ""; ?></td>
                                                        <td><?= ($data->position_name) ? $data->position_name : ""; ?></td>
														<?php //if($data->hod == $data->approved_by) {?>
														<?php if($data->status == 1){?>
                                                        <td>
														<span onclick="approvevisitinghr('<?= @$data->user_id; ?>')" style="cursor:pointer;color:green"> Approve</span> / <span onclick="rejectvisitinghr('<?= @$data->user_id; ?>')" style="cursor:pointer;color:red">Reject</span></td>
														<?php } elseif($data->status == 2) { ?>
														<td><span style="color:green">Approved</span></td>
														<?php } elseif($data->status == 3) { ?>
															<td><span style="color:red">Rejected</span></td>	
														<?php } ?>
                                                        <!--<td><?php if($data->status == 0){ echo "<span style='color:red'>Pending</span>";} elseif($data->status == 1){ echo "<span style='color:green'>Approved By HOD</span>";}elseif($data->status == 2){ echo "<span style='color:green'>Approved By HRD</span>";}elseif($data->status == 3){ echo "<span style='color:red'>Rejected</span>";} ?></td>-->
														<?php //} ?>
													</tr>
                                                   <?php
                                                }
												}
                                            } 
											else {  ?>
                                                <tr>
                                                    <td style="color:red;text-align:center;" colspan="9"> Record Not Found. </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                        
                                        <tfoot>
                                            <tr>
                                               <th>Sr.No.</th>
                                                <th>Name</th>
                                                <th>Employee No.</th>
                                                <th>Designation</th>
                                                <th>Status</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
	
<script>
    function approvevisiting(approveid) {
		// alert(approveid)
            window.location = "<?= base_url("update_visiting_card_rqst"); ?>/" + approveid;
    }
	 function rejectvisiting(rejectid) {
		// alert(rejectid)
            window.location = "<?= base_url("update_visiting_card_rqst_reject"); ?>/" + rejectid;
    }
	function approvevisitinghr(approveid) {
		// alert(approveid)
            window.location = "<?= base_url("update_visiting_card_rqst_hr"); ?>/" + approveid;
    }
	 function rejectvisitinghr(rejectid) {
		// alert(rejectid)
            window.location = "<?= base_url("update_visiting_card_rqst_reject"); ?>/" + rejectid;
    }
	
	
</script>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	  <h4 class="modal-title">Preview Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
		
      </div>
      <div class="modal-body">
       <form method="post" action="<?= base_url('applyvisitingcard'); ?>" id="visitingcard_form"> 
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Date of Initiation </b>
                                                            <input readonly="readonly" type="text" class="form-control" name="date_of_initiation" value="<?php echo date("d-m-Y");?>" >    
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Name</b>
                                                            <input readonly="readonly" name="userfullname" type="text" value="<?= (@$visitingDetailArr->userfullname) ? $visitingDetailArr->userfullname : ""; ?>" id="array-select" class="form-control">
															<input readonly="readonly" name="approved_by" type="hidden" value="<?= (@$visitingDetailArr->reporting_manager) ? $visitingDetailArr->reporting_manager : ""; ?>" id="array-select" class="form-control">
														</div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Employee No.</b>
                                                            <input readonly="readonly" name="employeeId" type="text" step="any" value="<?= (@$visitingDetailArr->employeeId) ? $visitingDetailArr->employeeId : ""; ?>" id="array-select" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Designation</b>
                                                            <input readonly="readonly" name="position_name" type="text" step="any" value="<?= (@$visitingDetailArr->position_name) ? $visitingDetailArr->position_name : ""; ?>" id="array-select" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <b>Email ID</b>
                                                        <div class="input-group mb-3">
                                                            <input readonly="readonly" name="emailaddress" type="text" step="any" value="<?= (@$visitingDetailArr->emailaddress) ? $visitingDetailArr->emailaddress : ""; ?>" id="array-select" class="form-control">
                                                        </div>
                                                    </div>
													 <div class="col-lg-4 col-md-6">
                                                        <b>Contact No.</b>
                                                        <div class="input-group mb-3">
                                                            <input readonly="readonly" name="contactnumber" type="text" step="any" value="<?= (@$visitingDetailArr->contactnumber) ? $visitingDetailArr->contactnumber : ""; ?>" id="array-select" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <b>Extension No.</b>
                                                        <div class="input-group mb-3">
                                                            <input readonly="readonly" name="extension_number" type="text" step="any" value="<?php if($visitingDetailArr->extension_number == ''){echo "";}else {echo $visitingDetailArr->extension_number;} ?>" id="array-select" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Fax No.</b>
                                                            <input readonly="readonly" name="fax_no" type="text" step="any" value="+911412751806" id="array-select" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        
                                                    </div>
													<div class="col-lg-4 col-md-6">
                                                        <div class="mb-2">
                                                            <b></b>
                                                            <input type="submit" name="submit" class="btn btn-round btn-primary">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
										</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</body>
