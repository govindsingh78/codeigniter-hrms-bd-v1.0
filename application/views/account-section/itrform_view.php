<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<body class="theme-cyan">
    <div id="wrapper">
      <?php
        $this->load->view('admin/includes/sidebar');
      ?>
        <div id="main-content" class="profilepage_1">
            <div class="container-fluid">
                <div class="block-header">

                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                        
                        
                        
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">

                            <div class="tab-pane" id="documentsdetails">
                                <div class="body">
                                    <div class="row clearfix">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Sr.No.</th>
                                                    <th>Doc/ Attachment </th>
                                                    <th>Name/Year</th>
                                                    <th>Pan No.</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if ($ItrFormDetailsRecArr) {
                                                    foreach ($ItrFormDetailsRecArr as $kEy => $dataRow) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $kEy + 1; ?></td>
                                                            <td>
                                                                <a target="_blank" href="<?= ITRFILE_IMAGES . $dataRow->finyear . "/" . $dataRow->folder_name . "/" . $dataRow->file_path; ?>">view/Save <i class=" fa fa-file-pdf-o"></i></a>
                                                            </td>
                                                            <td><?= ($dataRow->finyear) ? $dataRow->finyear : ""; ?></td>
                                                            <td><?= ($dataRow->pancardno) ? $dataRow->pancardno : ""; ?></td>
                                                            <td></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <tr>
                                                        <td style="color:red" colspan="5"> Record Not Found. </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>