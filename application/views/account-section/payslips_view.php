<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php $this->load->view('admin/includes/sidebar'); ?>
        <div id="main-content" class="profilepage_1">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li><li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li> </ul>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <form method="post" action="<?= base_url("mypayslip"); ?>" id="myform">
                                <div class="col-lg-6">
                                    <div class="body">
                                        <div class="input-group">
                                            <select class="form-control" name="selectedyear" id="inputGroupSelect04">
                                                <option <?= (@$selyear == "2024") ? "Selected" : ""; ?> value="2024">2024</option>
                                                <option <?= (@$selyear == "2023") ? "Selected" : ""; ?> value="2023">2023</option>
                                                <option <?= (@$selyear == "2022") ? "Selected" : ""; ?> value="2022">2022</option>
                                                <option <?= (@$selyear == "2021") ? "Selected" : ""; ?> value="2021">2021</option>
                                                <option <?= (@$selyear == "2020") ? "Selected" : ""; ?> value="2020">2020</option>
                                                <option <?= (@$selyear == "2019") ? "Selected" : ""; ?> value="2019">2019</option>
                                                <option <?= (@$selyear == "2018") ? "Selected" : ""; ?> value="2018">2018</option>
                                                <option <?= (@$selyear == "2017") ? "Selected" : ""; ?> value="2017">2017</option>
                                                <option <?= (@$selyear == "2016") ? "Selected" : ""; ?> value="2016">2016</option>
                                            </select> &nbsp;
                                            <div class="input-group-append">  <button class="btn btn-outline-primary" type="submit"> Filter Year</button> </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="tab-pane" id="documentsdetails">
                                <div class="body">
                                    <div class="row clearfix">
                                        <table class="table table-striped">
                                            <thead>  <tr> <th>Sr.No.</th> <th>Doc/Payslip Name</th> <th>view/Download</th> <th>PDF Password</th> <th>Send on Mail</th> </tr> </thead>
                                            <tbody>
                                                <?php
                                                if ($empPayslipDetailArr) {
                                                    foreach ($empPayslipDetailArr as $kEy => $recD) {
                                                        ?>
                                                        <tr> <td><?= $kEy + 1; ?></td>
                                                            <td><?= (@$recD->filename) ? @$recD->filename : ""; ?></td>
                                                            <td> <a target="_blank" href="<?= PAYSLIPBASEPATH . $recD->fullpath; ?>">view/Save <i class=" fa fa-file-pdf-o"></i></a> </td>
                                                            <td><a href="#"> ***** &nbsp;<i class="fa fa-eye"></i></a></td>
                                                            <td><a href="#">Send <i class="fa fa-send-o" style="font-size:17px"></i></a></td> </tr>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <tr><td style="color:red" colspan="4"> Record Not Found. </td> </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>