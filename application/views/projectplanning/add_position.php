<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        <div class="wrapper">
			<?php $this->load->view('include/sidebar'); ?>
			<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
			<link href="<?= FRONTASSETS; ?>css/jquery.multiselect.css" rel="stylesheet"></link>
		
			
            <div id="content" class="col-lg-10 col-sm-10">
                    <!-- content starts -->
                    <div>
                        <ul class="breadcrumb">
                            <li> <a href="<?= base_url(); ?>">Home</a></li>
                            <li><a href="#"><?= $title; ?></a></li>
                        </ul>
                    </div>
                    <div class="row">
                        <?php if ($this->session->flashdata('msg')) { ?>
                            <div class="alert alert-success alert-dismissable" >
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                            </div>
                        <?php } ?>
                        


                        <!-- open Phase Section-->
                        <div class="box-inner">
							
							<div class="box-content">
                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">
									<form name="frm1" method="post" action="<?= base_url('projectplanning/viewposition'); ?>">
                                        <div class="row">
											<div class="col-sm-5">
                                                <label class="col-sm-10 control-label" for="selectError2">Designation:</label>
												<br/>
                                                <div class="col-sm-10">
                                                    <select onchange="this.form.submit()" required name="project_id" id="project_id"  class="form-control selectpicker" data-live-search="true">
                                                        <option Selected value="">--All--</option>
                                                        <?php
                                                        if ($designation_master):
                                                            foreach ($designation_master as $value) {
                                                                ?>
                                                                <option <?= ($id == $value->fld_id) ? 'selected' : ''; ?> value="<?= $value->fld_id; ?>"><?= $value->designation_name; ?></option>
                                                            <?php } endif; ?>
                                                    </select>
                                                </div>
                                            </div>
											
											<div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <div class="col-sm-12">
													<select onchange="this.form.submit()" id="sector" name="sector" class="form-control col-md-4 col-xs-12">
														<!--<option value=""> -- Select Designation -- </option>-->
														<option Selected value="">--All--</option>
														<?php
														if ($sector):
															foreach ($sector as $rowr) {
																?>
																<option <?= ($scid == $rowr->fld_id) ? 'selected' : ''; ?> value="<?= $rowr->fld_id; ?>"> <?= $rowr->sectName; ?></option>
															<?php } endif; ?>
													</select>
                                                </div>
                                            </div>
											
											<div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <div class="col-sm-12">
													<select id="designation_id" name="designation_id[]" multiple class="form-control col-md-4 col-xs-12">
														<!--<option value=""> -- Select Designation -- </option>-->
														<?php
														if ($designation):
															foreach ($designation as $rowr) {
																?>
																<option value="<?= $rowr->fld_id; ?>"> <?= $rowr->designation_name; ?></option>
															<?php } endif; ?>
													</select>
                                                </div>
                                            </div>
											
										</div>
										<div class="form-group has-success col-md-2"> 
											<button type="submit" class="btn-info">Submit</button>
										</div>
									</form>
									<br/><br/>
									<div class="form-group has-success col-md-2"> 
										<button type="button" class="btn-info" data-toggle="modal" data-target="#Designation">Add More Designation</button>
									</div>
									
										<br/><br/>
										
										<?php 
											if(!empty($designation_master1)){
											?>
											<table>
												<tr>
													<th>Designation</th>
												</tr>
  

											
											<?php //echo '<pre>'; print_r($designation_master1);
												foreach($designation_master1 as $valdesg){
											?>
												<tr>
													<td>
														<input type="text" value="<?= ($valdesg->designation_name) ? $valdesg->designation_name : '' ?>" readonly class="form-control"/>
													</td>
													
												</tr>
											
										<?php 
													
												}
										?>
										</table>
										<br/><br/>
										
										
										<?php 
											} 
										?>
										
                                    </div>
                                </div>
                            </div>
							
                        </div>
                    </div>
                    <!--Close phase Section-->

                </div>
          
        </div>	
        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>

        <hr>

        
		<div class="modal fade" id="Designation" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Designation</h4>
                    </div>
                    <div class="modal-body">
                        <form name="form1" method="post" id="designations">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-1" ></div>
                                    <div class="col-sm-8">
                                        <div id="msg"></div>
                                    </div>
                                </div>
                                <div class="controls col-md-12">
                                    <div class="col-sm-9">
                                        <input type="text" required name="designation" id="adddesignation" placeholder = "Designation"  class="form-control input-sm">
                                    </div>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn-primary submitdesig">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        



        <!-- BootStrap Model For Comments.. -->
        <?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->
        <?php $this->load->view('include/footer'); ?>
		<?php $this->load->view('include/datatablejs'); ?>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="<?= FRONTASSETS; ?>js/jquery.multiselect.js"></script>
		 
		
		<style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            #chatbox {padding: 15px;overflow: scroll;height: 300px;}
			
			table {
				font-family: arial, sans-serif;
				border-collapse: collapse;
				width: 100%;
			}

			td, th {
				border: 1px solid #dddddd;
				text-align: left;
				padding: 8px;
			}

			/* tr:nth-child(even) {
				background-color: #dddddd;
			} */
			.ms-options >ul >li{list-style-type:none;}
        </style>
		<script>
			$('#designation_id').multiselect({
				columns: 1,
				placeholder: 'Select Designation',
				search: true
			});
			
			$('document').ready(function(){
				$('#emp_id').on("change",function(){
					var userID = $(this).val();
					$.ajax({
						"url": "<?php echo site_url('projectplanning/getuserdetailByID') ?>",
                        "type": "POST",
						"data":{'id':userID},
						success:function(res){
							var result = JSON.parse(res);
							console.log(result[0]);
							$('#email_id').val(result[0].emailaddress);
							$('#contact_no').val(result[0].contactnumber);
						}
					});
				});
			});
			
			
			 $(document).ready(function () {
				$(".submitdesig").click(function (event) {
					event.preventDefault();
					$("#msg").html("");
					var designation = $("#adddesignation").val();
					if (designation == '') {
						$("#msg").html('Please enter designation');
					} else {
						$.ajax({
							type: 'POST',
							url: "<?= base_url('projectplanning/addDesignationmaster'); ?>",
							data: {'designation': designation},
							success: function (response) {
								location.reload(1);
								//console.log(response);
							}
						});
					}
				});
				});
			
		</script>
    </div>
</body>
</html>
