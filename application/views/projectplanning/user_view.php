<?php //echo '<pre>'; print_r($data);                     ?>
<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        <div class="wrapper">
            <?php $this->load->view('include/sidebar'); ?>
            <link href="<?= FRONTASSETS; ?>user/css/font-awesome.min.css" rel="stylesheet" type="text/css">
            <link href="<?= FRONTASSETS; ?>user/css/theme.min.css" rel="stylesheet" type="text/css">
            <link href="<?= FRONTASSETS; ?>user/css/theme-responsive.min.css" rel="stylesheet" type="text/css">

            <div id="content" class="full-layout  nav-right-hide nav-right-start-hide  nav-top-fixed      responsive    clearfix" data-active="pages "  data-smooth-scrolling="1">
                <div class="vd_body">
                    <div class="content">
                        <div class="container">
                            <!-- Middle Content Start -->
                            <div class="vd_content-wrapper">
                                <div class="vd_container" style="margin-left:0;">
                                    <div class="vd_content clearfix">
                                        <div class="vd_title-section clearfix">
                                            <div class="vd_panel-header no-subtitle">
                                                <h1>User Profile Page</h1>
                                            </div>
                                        </div>
                                        <div class="vd_content-section clearfix">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="panel widget light-widget panel-bd-top">
                                                        <div class="panel-heading no-title"> </div>
                                                        <div class="panel-body">
                                                            <div class="text-center vd_info-parent"> 
                                                                <?php
                                                                if ($data['userdetail'][0]->profileimg) {
                                                                    ?>	
                                                                    <img alt="example image" src="http://172.16.1.30:8087/public/uploads/profile/<?= $data['userdetail'][0]->profileimg ?>"> 
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <img alt="example image" src="<?= FRONTASSETS; ?>user/img/avatar/profile.jpg"> 		
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <h2 class="font-semibold mgbt-xs-5" style="color:black;"><?= isset($data['userdetail'][0]->userfullname) ? $data['userdetail'][0]->userfullname : ''; ?></h2>
                                                            <h4><?= isset($data['userdetail'][0]->position_name) ? $data['userdetail'][0]->position_name : ''; ?></h4>
                                                            <p><?= isset($data['userdetail'][0]->businessunit_name) ? $data['userdetail'][0]->businessunit_name : ''; ?></p>
                                                            <div class="mgtp-20">
                                                                <table class="table table-striped table-hover">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="width:60%;">Status</td>
                                                                            <td>
                                                                                <?php
                                                                                if ($data['userdetail'][0]->isactive == 1) {
                                                                                    ?>	
                                                                                    <span class="label label-success">Active</span>
                                                                                    <?php
                                                                                } else {
                                                                                    ?>
                                                                                    <span class="label label-danger">InActive</span>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>DOJ</td>
                                                                            <td> <?= isset($data['userdetail'][0]->date_of_joining) ? date('M d,Y', strtotime($data['userdetail'][0]->date_of_joining)) : ''; ?></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- panel widget -->
                                                    <!--
                                                    <div class="panel widget light-widget">
                                                      <div class="panel-body-list">
                                                        <h3 class="pd-20 mgbt-xs-0"><i class="fa fa-users mgr-10"></i>Friends</h3>
                                                        <div class="content-grid column-xs-2 column-sm-3 height-xs-auto mgbt-xs-20">
                                                          <div>
                                                            <ul class="list-wrapper">
                                                              <li> <a href="#"> <span class="menu-icon"><img alt="example image" src="<?= FRONTASSETS; ?>user/img/avatar/avatar.jpg"></span> </a> </li>
                                                              <li class="warning"> <a href="#"> <span class="menu-icon"><img alt="example image" src="<?= FRONTASSETS; ?>user/img/avatar/avatar-2.jpg"></span> </a> </li>
                                                              <li> <a href="#"> <span class="menu-icon"><img alt="example image" src="<?= FRONTASSETS; ?>user/img/avatar/avatar-3.jpg"></span> </a> </li>
                                                              <li> <a href="#"> <span class="menu-icon"><img alt="example image" src="<?= FRONTASSETS; ?>user/img/avatar/avatar-4.jpg"></span> </a> </li>
                                                              <li> <a href="#"> <span class="menu-icon"><img alt="example image" src="<?= FRONTASSETS; ?>user/img/avatar/avatar-5.jpg"></span> </a> </li>
                                                              <li> <a href="#"> <span class="menu-icon"><img alt="example image" src="<?= FRONTASSETS; ?>user/img/avatar/avatar-6.jpg"></span> </a> </li>
                                                              <li> <a href="#"> <span class="menu-icon"><img alt="example image" src="<?= FRONTASSETS; ?>user/img/avatar/avatar-7.jpg"></span> </a> </li>
                                                              <li> <a href="#"> <span class="menu-icon"><img alt="example image" src="<?= FRONTASSETS; ?>user/img/avatar/avatar-8.jpg"></span> </a> </li>
                                                            </ul>
                                                          </div>
                                                        </div>
                                                        <div class="closing text-center" style=""> <a href="#">See All Friends<i class="fa fa-angle-double-right prepend-icon"></i></a> </div>
                                                      </div>
                                                    </div>-->
                                                    <!-- panel widget --> 
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="tabs widget">
                                                        <ul class="nav nav-tabs widget">
                                                            <li class="active"> <a data-toggle="tab" href="#profile-tab"> Profile <span class="menu-active"><i class="fa fa-caret-up"></i></span> </a></li>
                                                            <li> <a data-toggle="tab" href="#projects-tab"> Projects <span class="menu-active"><i class="fa fa-caret-up"></i></span> </a></li>    
                                                            <li> <a data-toggle="tab" href="#value-tab"> Values <span class="menu-active"><i class="fa fa-caret-up"></i></span> </a></li> 
                                                            <li> <a data-toggle="tab" href="#coord-project-tab">Project Coordinator<span class="menu-active"><i class="fa fa-caret-up"></i></span> </a></li>    															
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div id="profile-tab" class="tab-pane active">
                                                                <div class="pd-20">

                                                                    <h3 class="mgbt-xs-15 mgtp-10 font-semibold"><i class="fa fa-user"></i> ABOUT</h3>
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <div class="row mgbt-xs-0">
                                                                                <label class="col-xs-5 control-label">First Name:</label>
                                                                                <div class="col-xs-7 controls"><?= isset($data['userdetail'][0]->firstname) ? $data['userdetail'][0]->firstname : ''; ?></div>
                                                                                <!-- col-sm-10 --> 
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="row mgbt-xs-0">
                                                                                <label class="col-xs-5 control-label">Last Name:</label>
                                                                                <div class="col-xs-7 controls"><?= isset($data['userdetail'][0]->lastname) ? $data['userdetail'][0]->lastname : ''; ?></div>
                                                                                <!-- col-sm-10 --> 
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="row mgbt-xs-0">
                                                                                <label class="col-xs-5 control-label">User Name:</label>
                                                                                <div class="col-xs-7 controls"><?= isset($data['userdetail'][0]->userfullname) ? $data['userdetail'][0]->userfullname : ''; ?></div>
                                                                                <!-- col-sm-10 --> 
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="row mgbt-xs-0">
                                                                                <label class="col-xs-5 control-label">Email:</label>
                                                                                <div class="col-xs-7 controls"><?= isset($data['userdetail'][0]->emailaddress) ? $data['userdetail'][0]->emailaddress : ''; ?></div>
                                                                                <!-- col-sm-10 --> 
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="row mgbt-xs-0">
                                                                                <label class="col-xs-5 control-label">City:</label>
                                                                                <div class="col-xs-7 controls"><?= isset($data['userdetail'][0]->city_name) ? $data['userdetail'][0]->city_name : ''; ?></div>
                                                                                <!-- col-sm-10 --> 
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="row mgbt-xs-0">
                                                                                <label class="col-xs-5 control-label">State:</label>
                                                                                <div class="col-xs-7 controls"><?= isset($data['userdetail'][0]->state_name) ? $data['userdetail'][0]->state_name : ''; ?></div>
                                                                                <!-- col-sm-10 --> 
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="row mgbt-xs-0">
                                                                                <label class="col-xs-5 control-label">Country:</label>
                                                                                <div class="col-xs-7 controls"><?= isset($data['userdetail'][0]->country_name) ? $data['userdetail'][0]->country_name : ''; ?></div>
                                                                                <!-- col-sm-10 --> 
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="row mgbt-xs-0">
                                                                                <label class="col-xs-5 control-label">Birthday:</label>
                                                                                <div class="col-xs-7 controls"><?= isset($data['userdetail'][0]->dob) ? date('M d,Y', strtotime($data['userdetail'][0]->dob)) : ''; ?></div>
                                                                                <!-- col-sm-10 --> 
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="row mgbt-xs-0">
                                                                                <label class="col-xs-5 control-label">Phone:</label>
                                                                                <div class="col-xs-7 controls"><?= isset($data['userdetail'][0]->office_number) ? $data['userdetail'][0]->office_number : ''; ?></div>
                                                                                <!-- col-sm-10 --> 
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <hr class="pd-10"  />
                                                                    <div class="row">
                                                                        <div class="col-sm-7 mgbt-xs-20">
                                                                            <h3 class="mgbt-xs-15 font-semibold"><i class="fa fa-file-text-o mgr-10 profile-icon"></i> EXPERIENCE</h3>
                                                                            <div class="content-list content-menu">
                                                                                <ul class="list-wrapper">
                                                                                    <?php
                                                                                    if ($data['experience']) {
                                                                                        foreach ($data['experience'] as $valuedata) {
                                                                                            ?>
                                                                                            <li class="mgbt-xs-10"> 
                                                                                                <span class="menu-icon vd_grey">
                                                                                                    <i class=" fa  fa-circle-o"></i>
                                                                                                </span> 
                                                                                                <span class="menu-text"> 
                                                                                                    <?= isset($valuedata->designation) ? $valuedata->designation : ''; ?> at <?= isset($valuedata->comp_name) ? $valuedata->comp_name : ''; ?>
                                                                                                    <span class="menu-info">
                                                                                                        <span class="menu-date"> 
                                                                                                            <?= isset($valuedata->from_date) ? date('F Y', strtotime($valuedata->from_date)) : ''; ?>   <?= isset($valuedata->to_date) ? ' ~ ' . date('F Y', strtotime($valuedata->to_date)) : ''; ?>
                                                                                                        </span>
                                                                                                    </span>
                                                                                                </span> 
                                                                                            </li>
                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-5">
                                                                            <h3 class="mgbt-xs-15 font-semibold"><i class="fa fa-trophy mgr-10 profile-icon"></i> EDUCATION</h3>
                                                                            <div class="content-list content-menu">
                                                                                <ul class="list-wrapper">
                                                                                    <?php
                                                                                    if ($data['education']) {
                                                                                        foreach ($data['education'] as $valueedu) {
                                                                                            ?>
                                                                                            <li class="mgbt-xs-10"> 
                                                                                                <span class="menu-icon vd_green">
                                                                                                    <i class="fa  fa-circle-o"></i>
                                                                                                </span>
                                                                                                <span class="menu-text"> 
                                                                                                    <?= isset($valueedu->course) ? $valueedu->course : ''; ?> , <?= isset($valueedu->specialization) ? $valueedu->specialization : ''; ?>  at 
                                                                                                    <?= isset($valueedu->institution_name) ? $valueedu->institution_name : ''; ?>
                                                                                                    <?= isset($valueedu->spc_location) ? $valueedu->spc_location : ''; ?>
                                                                                                    <span class="menu-info">
                                                                                                        <span class="menu-date"> 
                                                                                                            <?= isset($valueedu->from_date) ? $valueedu->from_date : ''; ?>   <?= isset($valueedu->to_date) ? ' ~ ' . $valueedu->to_date : ''; ?>
                                                                                                        </span>
                                                                                                    </span> 
                                                                                                </span>
                                                                                            </li>
                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </ul>
                                                                            </div>            
                                                                        </div>
                                                                    </div>
                                                                    <!-- row -->
                                                                    <hr class="pd-10"  />
                                                                    <div class="row">
                                                                        <div class="col-sm-7">
                                                                            <h3 class="mgbt-xs-15 font-semibold"><i class="fa fa-globe mgr-10 profile-icon"></i> COMMENT</h3>
                                                                            <div class="">
                                                                                <div class="content-list">
                                                                                    <div data-rel="scroll" style="overflow:scroll;">
                                                                                        <ul  class="list-wrapper">
                                                                                            <?php
                                                                                            if ($data['comment']) {
                                                                                                foreach ($data['comment'] as $commenval) {
                                                                                                    ?>
                                                                                                    <li>  
                                                                                                        <span class="menu-icon vd_green">
                                                                                                            <?php
                                                                                                            if ($commenval->profileimg) {
                                                                                                                ?>	
                                                                                                                <img alt="example image" src="http://172.16.1.30:8087/public/uploads/profile/<?= $commenval->profileimg ?>"> 
                                                                                                                <?php
                                                                                                            } else {
                                                                                                                ?>
                                                                                                                <img alt="example image" src="<?= FRONTASSETS; ?>user/img/avatar/profile.jpg"> 		
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>
                                                                                                        </span> 
                                                                                                        <span class="menu-text"> 
                                                                                                            <?= isset($commenval->comment) ? $commenval->comment : ''; ?>
                                                                                                            <span class="menu-info">
                                                                                                                <span class="menu-date">
                                                                                                                    <?php
                                                                                                                    $OldDate = strtotime($commenval->created);
                                                                                                                    $NewDate = date('M j, Y', $OldDate);
                                                                                                                    $diff = date_diff(date_create($NewDate), date_create(date("M j, Y")));
                                                                                                                    $daysdiff = $diff->format('%R%a days');
                                                                                                                    ?>
                                                                                                                    ~ <?= isset($daysdiff) ? $daysdiff : ''; ?> Ago By <?= isset($commenval->userfullname) ? $commenval->userfullname : ''; ?>
                                                                                                                </span>
                                                                                                            </span>
                                                                                                        </span>
                                                                                                    </li>
                                                                                                    <?php
                                                                                                }
                                                                                            }
                                                                                            ?>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <form method="post" action="<?php /* base_url('/projectplanning/usercomment/') */ ?>" id="submit_cmnt">
                                                                                        <div class="closing" style=""> 
                                                                                            <textarea name="user_comment" id="user_comment" cols="58" rows="4"> </textarea> 
                                                                                            <input type="hidden" name="comment_id" id="comment_id" value="<?= $data['userdetail'][0]->user_id; ?>" /> 
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- col-sm-7 --> 
                                                                        <div class="col-sm-5">
                                                                            <h3 class="mgbt-xs-15 font-semibold"><i class="fa fa-flask mgr-10 profile-icon"></i> ON PROJECT </h3>
                                                                            <?php
                                                                            if ($data['onproject']) {
                                                                                foreach ($data['onproject'] as $valuepro) {
                                                                                    ?>
                                                                                    <div class="skill-list">
                                                                                        <div class="skill-name"> <?= isset($valuepro->project_name) ? $valuepro->project_name : ''; ?> </div>
                                                                                        <div class="progress  progress-sm"></div>
                                                                                    </div>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>

                                                                        </div>
                                                                        <!-- col-sm-7 -->           
                                                                    </div>
                                                                    <!-- row --> 
                                                                </div>
                                                                <!-- pd-20 --> 
                                                            </div>
                                                            <!-- home-tab -->

                                                            <div id="projects-tab" class="tab-pane">
                                                                <div class="pd-20">
                                                                    <h3 class="mgbt-xs-15 mgtp-10 font-semibold"><i class="fa fa-bolt mgr-10 profile-icon"></i> PROJECTS</h3>        
                                                                    <table class="table table-striped table-hover">
                                                                        <thead>
                                                                            <tr>

                                                                                <th>Total</th>                                  
                                                                                <th>Awaiting</th>
                                                                                <th>Won</th>
                                                                                <th>Lost</th>
                                                                                <th>Cancel</th>
                                                                            </tr>
                                                                        </thead>   
                                                                        <tbody>
                                                                            <?php
                                                                            if ($data['bdproject']) {
                                                                                $total = count($data['bdproject']);
                                                                                $await = 0;
                                                                                $won = 0;
                                                                                $loose = 0;
                                                                                $cancel = 0;
                                                                                foreach ($data['bdproject'] as $val) {
                                                                                    if ($val->project_status == 0) {
                                                                                        $await = $await + 1;
                                                                                    }
                                                                                    if ($val->project_status == 1) {
                                                                                        $won = $won + 1;
                                                                                    }
                                                                                    if ($val->project_status == 2) {
                                                                                        $loose = $loose + 1;
                                                                                    }
                                                                                    if ($val->project_status == 3) {
                                                                                        $cancel = $cancel + 1;
                                                                                    }
                                                                                }
                                                                            }
                                                                            ?>	
                                                                            <tr>
                                                                                <td><?= isset($total) ? $total : ''; ?></td>                    
                                                                                <td><?= isset($await) ? $await : ''; ?></td>                    
                                                                                <td><?= isset($won) ? $won : ''; ?></td>                    
                                                                                <td><?= isset($loose) ? $loose : ''; ?></td>                    
                                                                                <td><?= isset($cancel) ? $cancel : ''; ?></td>                    
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <br/><br/>
                                                                    <table class="table table-striped table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Sr.No.</th>
                                                                                <th>Project Name</th>                                  
                                                                                <th>Designation</th>
                                                                                <th>Project Status</th>
                                                                                <th>Total MM</th>
                                                                                <th>Consumed MM</th>
                                                                                <th>Balanced MM</th>

                                                                            </tr>
                                                                        </thead>   
                                                                        <tbody>
                                                                            <?php
                                                                            $currentdate = date('Y-m-d', strtotime($invoice[0]->invoice_date . "-1 month"));
                                                                            $cruyears = date('Y', strtotime($currentdate));
                                                                            $crumonths = date('n', strtotime($currentdate));

                                                                            if ($data['bdproject']) {
                                                                                $no = 1;
                                                                                $balancemm = '';
                                                                                $totalcumulativemm = '';

                                                                                foreach ($data['bdproject'] as $val) {

                                                                                    $consumedmm = $val->cumulative_pre_mm + $val->invtotalbalmm;
                                                                                    ?>
                                                                                    <tr>
                                                                                        <td><?= $no; ?></td>
                                                                                        <td><?= isset($val->TenderDetails) ? $val->TenderDetails : ''; ?><?= $val->fld_id; ?></td>                    
                                                                                        <td><?= isset($val->designation_name) ? $val->designation_name : ''; ?></td>                    
                                                                                        <td>
                                                                                            <?php
                                                                                            $status = '';
                                                                                            if ($val->visible_scope == 'Bid_project') {
                                                                                                if ($val->project_status == 0) {
                                                                                                    $status = 'Bid(Awaiting)';
                                                                                                } else if ($val->project_status == 1) {
                                                                                                    $status = '<span style="color:green">Bid(Won)</span>';
                                                                                                } else if ($val->project_status == 2) {
                                                                                                    $status = 'Bid(Loose)';
                                                                                                } else if ($val->project_status == 3) {
                                                                                                    $status = 'Bid(Cancel)';
                                                                                                }
                                                                                            } elseif ($val->visible_scope == 'No_Submit_project') {
                                                                                                $status = 'No Submit';
                                                                                            } elseif ($val->visible_scope == 'To_Go_project') {
                                                                                                $status = 'To Go';
                                                                                            }
                                                                                            echo $status;
                                                                                            ?>
                                                                                        </td>

                                                                                        <td>
                                                                                            <?php
                                                                                            $responRec = getRecordInvcmm($val->fld_id, $val->empname);
                                                                                            if ($responRec) {
                                                                                                echo round($responRec->total_mm, 3);
                                                                                            }
                                                                                            //isset($val->fld_id) ? $val->fld_id."--".$val->empname : ''; 
                                                                                            ?>
                                                                                        </td>                    
                                                                                        <td>
                                                                                            <?php
                                                                                            if ($responRec) {
                                                                                                echo round(($responRec->total_mm - $responRec->balance_mm), 3);
                                                                                            }
                                                                                            ?>
                                                                                        </td>                    
                                                                                        <td>
                                                                                            <?php
                                                                                            if ($responRec) {
                                                                                                echo round($responRec->balance_mm, 3);
                                                                                            }
                                                                                            ?>
                                                                                        </td>

                                                                                    </tr>
                                                                                    <?php
                                                                                    $no++;
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </tbody>
                                                                    </table>
                                                                    <div class="">
                                                                    </div>        
                                                                </div>
                                                            </div>

                                                            <div id="value-tab" class="tab-pane">
                                                                <div class="pd-20">
                                                                    <h3 class="mgbt-xs-15 mgtp-10 font-semibold"><i class="fa fa-bolt mgr-10 profile-icon"></i> Total </h3>

                                                                    <table id="usertable">
                                                                        <tr>
                                                                            <th>Year</th>
                                                                            <th>Jan</th>
                                                                            <th>Feb</th>
                                                                            <th>Mar</th>
                                                                            <th>Apr</th>
                                                                            <th>May</th>
                                                                            <th>June</th>
                                                                            <th>Jul</th>
                                                                            <th>Aug</th>
                                                                            <th>Sept</th>
                                                                            <th>Oct</th>
                                                                            <th>Nov</th>
                                                                            <th>Dec</th>
                                                                        </tr>

                                                                        <?php
                                                                        $projID = $valueproj->project_id;
                                                                        $userID = $data['userdetail'][0]->user_id;
                                                                        ?>

                                                                        <?php
                                                                        for ($year = 2018; $year <= date('Y'); $year++) {
                                                                            ?>	
                                                                            <tr>
                                                                                <td><?= $year; ?></td>
                                                                                <td>
                                                                                    <?php
                                                                                    $mmRec1 = getTotMMBymmyy($userID, '1', $year);
                                                                                    echo $mmRec1;
                                                                                    ?>	
                                                                                </td>
                                                                                <td>
                                                                                    <?php
                                                                                    $mmRec2 = getTotMMBymmyy($userID, '2', $year);
                                                                                    echo $mmRec2;
                                                                                    ?>	
                                                                                </td>
                                                                                <td>
                                                                                    <?php
                                                                                    $mmRec3 = getTotMMBymmyy($userID, '3', $year);
                                                                                    echo $mmRec3;
                                                                                    ?>	
                                                                                </td>
                                                                                <td>
                                                                                    <?php
                                                                                    $mmRec4 = getTotMMBymmyy($userID, '4', $year);
                                                                                    echo $mmRec4;
                                                                                    ?>	
                                                                                </td>
                                                                                <td>
                                                                                    <?php
                                                                                    $mmRec5 = getTotMMBymmyy($userID, '5', $year);
                                                                                    echo $mmRec5;
                                                                                    ?>	
                                                                                </td>
                                                                                <td>
                                                                                    <?php
                                                                                    $mmRec6 = getTotMMBymmyy($userID, '6', $year);
                                                                                    echo $mmRec6;
                                                                                    ?>	
                                                                                </td>
                                                                                <td>
                                                                                    <?php
                                                                                    $mmRec7 = getTotMMBymmyy($userID, '7', $year);
                                                                                    echo $mmRec7;
                                                                                    ?>	
                                                                                </td>
                                                                                <td>
                                                                                    <?php
                                                                                    $mmRec8 = getTotMMBymmyy($userID, '8', $year);
                                                                                    echo $mmRec8;
                                                                                    ?>	
                                                                                </td>
                                                                                <td>
                                                                                    <?php
                                                                                    $mmRec9 = getTotMMBymmyy($userID, '9', $year);
                                                                                    echo $mmRec9;
                                                                                    ?>	
                                                                                </td>
                                                                                <td>
                                                                                    <?php
                                                                                    $mmRec10 = getTotMMBymmyy($userID, '10', $year);
                                                                                    echo $mmRec10;
                                                                                    ?>	
                                                                                </td>
                                                                                <td>
                                                                                    <?php
                                                                                    $mmRec11 = getTotMMBymmyy($userID, '11', $year);
                                                                                    echo $mmRec11;
                                                                                    ?>	
                                                                                </td>
                                                                                <td>
                                                                                    <?php
                                                                                    $mmRec12 = getTotMMBymmyy($userID, '12', $year);
                                                                                    echo $mmRec12;
                                                                                    ?>	
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </table>

                                                                    <h3 class="mgbt-xs-15 mgtp-10 font-semibold"><i class="fa fa-bolt mgr-10 profile-icon"></i> PROJECTS </h3>

                                                                    <?php
                                                                    if ($data['onproject']) {
                                                                        foreach ($data['onproject'] as $valueproj) { //echo '<pre>'; print_r($valueproj->project_id);
                                                                            ?>
                                                                            <button class="accordion1"><?= $valueproj->project_name; ?></button>
                                                                            <div class="panel1">

                                                                                <?php
                                                                                $projID = $valueproj->project_id;
                                                                                $userID = $data['userdetail'][0]->user_id;
                                                                                ?>

                                                                                <table id="usertable">
                                                                                    <tr>
                                                                                        <th>Year</th>
                                                                                        <th>Jan</th>
                                                                                        <th>Feb</th>
                                                                                        <th>Mar</th>
                                                                                        <th>Apr</th>
                                                                                        <th>May</th>
                                                                                        <th>June</th>
                                                                                        <th>Jul</th>
                                                                                        <th>Aug</th>
                                                                                        <th>Sept</th>
                                                                                        <th>Oct</th>
                                                                                        <th>Nov</th>
                                                                                        <th>Dec</th>
                                                                                    </tr>
                                                                                    <?php
                                                                                    $cons = 0;
                                                                                    for ($year = 2018; $year <= date('Y'); $year++) {
                                                                                        ?>	
                                                                                        <tr>
                                                                                            <td><?= $year; ?></td>
                                                                                            <td>
                                                                                                <?php
                                                                                                $Recmonth1 = getsingleyearmonthAtten($projID, $userID, '1', $year);
                                                                                                echo ($Recmonth1) ? round($Recmonth1->current_mm, 2) : '';
                                                                                                ?>	
                                                                                            </td>
                                                                                            <td>
                                                                                                <?php
                                                                                                $Recmonth2 = getsingleyearmonthAtten($projID, $userID, '2', $year);
                                                                                                echo ($Recmonth2) ? round($Recmonth2->current_mm, 2) : '';
                                                                                                ?>	
                                                                                            </td>
                                                                                            <td>
                                                                                                <?php
                                                                                                $Recmonth3 = getsingleyearmonthAtten($projID, $userID, '3', $year);
                                                                                                echo ($Recmonth3) ? round($Recmonth3->current_mm, 2) : '';
                                                                                                ?>
                                                                                            </td>
                                                                                            <td>
                                                                                                <?php
                                                                                                $Recmonth4 = getsingleyearmonthAtten($projID, $userID, '4', $year);
                                                                                                echo ($Recmonth4) ? round($Recmonth4->current_mm, 2) : '';
                                                                                                ?>
                                                                                            </td>
                                                                                            <td>
                                                                                                <?php
                                                                                                $Recmonth5 = getsingleyearmonthAtten($projID, $userID, '5', $year);
                                                                                                echo ($Recmonth5) ? round($Recmonth5->current_mm, 2) : '';
                                                                                                ?>
                                                                                            </td>
                                                                                            <td>
                                                                                                <?php
                                                                                                $Recmonth6 = getsingleyearmonthAtten($projID, $userID, '6', $year);
                                                                                                echo ($Recmonth6) ? round($Recmonth6->current_mm, 2) : '';
                                                                                                ?>
                                                                                            </td>
                                                                                            <td>
                                                                                                <?php
                                                                                                $Recmonth7 = getsingleyearmonthAtten($projID, $userID, '7', $year);
                                                                                                echo ($Recmonth7) ? round($Recmonth7->current_mm, 2) : '';
                                                                                                ?>
                                                                                            </td>
                                                                                            <td>
                                                                                                <?php
                                                                                                $Recmonth8 = getsingleyearmonthAtten($projID, $userID, '8', $year);
                                                                                                echo ($Recmonth8) ? round($Recmonth8->current_mm, 2) : '';
                                                                                                ?>
                                                                                            </td>
                                                                                            <td>
                                                                                                <?php
                                                                                                $Recmonth9 = getsingleyearmonthAtten($projID, $userID, '9', $year);
                                                                                                echo ($Recmonth9) ? round($Recmonth9->current_mm, 2) : '';
                                                                                                ?>
                                                                                            </td>
                                                                                            <td>
                                                                                                <?php
                                                                                                $Recmonth10 = getsingleyearmonthAtten($projID, $userID, '10', $year);
                                                                                                echo ($Recmonth10) ? round($Recmonth10->current_mm, 2) : '';
                                                                                                ?>
                                                                                            </td>
                                                                                            <td>
                                                                                                <?php
                                                                                                $Recmonth11 = getsingleyearmonthAtten($projID, $userID, '11', $year);
                                                                                                echo ($Recmonth11) ? round($Recmonth11->current_mm, 2) : '';
                                                                                                ?>
                                                                                            </td>
                                                                                            <td>
                                                                                                <?php
                                                                                                $Recmonth12 = getsingleyearmonthAtten($projID, $userID, '12', $year);
                                                                                                echo ($Recmonth12) ? round($Recmonth12->current_mm, 2) : '';
                                                                                                ?>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <?php
                                                                                    }
                                                                                    ?>

                                                                                </table>
                                                                            </div>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div id="coord-project-tab" class="tab-pane">
                                                              <div class="pd-20">
                                                                 <h3 class="mgbt-xs-15 mgtp-10 font-semibold"><i class="fa fa-bolt mgr-10 profile-icon"></i> Project list</h3>   
														        <table class="table table-bordered">
																	      <thead><tr><th>S No.</th><th>Project Name</th></tr></thead>
																		  <tbody>
																<?php if(@$prj_coor){
																	$sno=0;
																   foreach($prj_coor as $rrow){ 
																         $sno++;
																         $proj =  getProjectBYId($rrow->hrms_projid);
																		 
																		 ?>
																		  <tr><td><?= $sno;?></td><td><?= $proj->project_name;?></td></tr>
																	  
																   
																   <?php } } ?>
															   </tbody></table>
															  </div>
															</div>
														</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>	
        <style>		
		#sidebar {
           background: #1c4e7f!important;
          }
		  .tabs .nav-tabs {
            background-color: #1c4e7f!important;
          }
            #usertable {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
                font-size: 12px;
            }
            #usertable td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }
            #usertable tr:nth-child(even) {
                background-color: #dddddd;
            }
            .accordion1 {
                background-color: #eee;
                color: #444;
                cursor: pointer;
                padding: 18px;
                width: 100%;
                border: none;
                text-align: left;
                outline: none;
                font-size: 15px;
                transition: 0.4s;
                border: 1px solid #fff;
            }
            .panel1 {
                padding: 10 18px;
                display: none;
                background-color: white;
                overflow: scroll;
            }
        </style>		
        <script type="text/javascript" src="<?= FRONTASSETS; ?>user/js/jquery.js"></script> 
        <script type="text/javascript" src="<?= FRONTASSETS; ?>user/js/bootstrap.min.js"></script> 
        <script type="text/javascript">
            var acc = document.getElementsByClassName("accordion1");
            var i;

            for (i = 0; i < acc.length; i++) {
                acc[i].addEventListener("click", function () {
                    this.classList.toggle("active");
                    var panel = this.nextElementSibling;
                    if (panel.style.display === "block") {
                        panel.style.display = "none";
                    } else {
                        panel.style.display = "block";
                    }
                });
            }

            $(document).ready(function () {
                "use strict";
                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    $container.isotope('layout');
                });
                // bind filter button click
                $('#filters').on('click', 'a', function () {
                    var filterValue = $(this).attr('data-filter');
                    $('#filters li').removeClass('active');
                    $(this).parent().addClass('active');
                    $container.isotope({filter: filterValue});
                });
            });

            $(document).ready(function () {
                $('#user_comment').keypress(function (e) {
                    var key = e.which;
                    if (key == 13) {
                        var user_comment = $("#user_comment").val();
                        var comment_id = $("#comment_id").val();
                        //Comment Submit..
                        $.ajax({
                            type: 'POST',
                            dataType: "text",
                            url: "<?= base_url('projectplanning/usercomment/'); ?>",
                            data: {'user_comment': user_comment, 'comment_id': comment_id},
                            success: function (responData) {
                                location.reload(1);
                            },
                        });
                    }
                });
            });
        </script>
    </body>
</html>
