<?php //echo '<pre>'; print_r($data);                           ?>
<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        <div class="wrapper">
            <?php $this->load->view('include/sidebar'); ?>
            <link href="<?= FRONTASSETS; ?>user/css/font-awesome.min.css" rel="stylesheet" type="text/css">
            <link href="<?= FRONTASSETS; ?>user/css/theme.min.css" rel="stylesheet" type="text/css">
            <link href="<?= FRONTASSETS; ?>user/css/theme-responsive.min.css" rel="stylesheet" type="text/css">

            <div id="content" class="full-layout  nav-right-hide nav-right-start-hide  nav-top-fixed      responsive    clearfix" data-active="pages "  data-smooth-scrolling="1">


                <div class="vd_body">

                    <div class="content">
                        <div class="container">

                            <!-- Middle Content Start -->

                            <div class="vd_content-wrapper">
                                <div class="vd_container" style="margin-left:0;">
                                    <div class="vd_content clearfix">

                                        <div class="vd_title-section clearfix">
                                            <div class="vd_panel-header no-subtitle">
                                                <h1>Intermittent Report</h1>
                                            </div>
                                        </div>
                                        <div class="vd_content-section clearfix">
                                            <div class="row">

                                                <div class="col-sm-12">
                                                    <div class="tabs widget">
                                                        <ul class="nav nav-tabs widget">  
                                                            <li> <a data-toggle="tab" href="#value-tab"> List View <span class="menu-active"><i class="fa fa-caret-up"></i></span> </a></li>    
                                                        </ul>
                                                        <div class="tab-content">

                                                            <!-- home-tab -->
                                                            <div id="value-tab" class="tab-pane">
                                                                <div class="pd-20">
                                                                    <h3 class="mgbt-xs-15 mgtp-10 font-semibold"><i class="fa fa-bolt mgr-10 profile-icon"></i> Intermittent Employee </h3>        
                                                                    <?php
                                                                    if ($empRecArr) {
                                                                        foreach ($empRecArr as $valueproj) { //echo '<pre>'; print_r($valueproj->project_id);
                                                                            ?>
                                                                            <button class="accordion1"><?= $valueproj->userfullname; ?> ( <b title="Department" style="color:green"> <?= $valueproj->department_name; ?> </b> ) - <b title="Designation" style="color:#7386d5"><?= $valueproj->position_name; ?></b> </button>
                                                                            <div class="panel1">
                                                                                <?php
                                                                                $userID = $valueproj->emp_id;
                                                                                $reCpRojarr = GetAllIntermittentProjectListEmpIdWise($userID);

                                                                                $Rcolm1 = 0;
                                                                                $Rcolm2 = 0;
                                                                                $Rcolm3 = 0;
                                                                                $Rcolm4 = 0;
                                                                                $Rcolm5 = 0;
                                                                                $Rcolm6 = 0;
                                                                                $Rcolm7 = 0;
                                                                                $Rcolm8 = 0;
                                                                                $Rcolm9 = 0;
                                                                                $Rcolm10 = 0;
                                                                                $Rcolm11 = 0;
                                                                                $Rcolm12 = 0;

                                                                                if ($reCpRojarr) {
                                                                                    foreach ($reCpRojarr as $kkYY => $RoWrEc) {
                                                                                        $projID = getprojectid_tstobd($RoWrEc->project_id);
                                                                                        $cons = 0;

                                                                                        $colm1 = 0;
                                                                                        $colm2 = 0;
                                                                                        $colm3 = 0;
                                                                                        $colm4 = 0;
                                                                                        $colm5 = 0;
                                                                                        $colm6 = 0;
                                                                                        $colm7 = 0;
                                                                                        $colm8 = 0;
                                                                                        $colm9 = 0;
                                                                                        $colm10 = 0;
                                                                                        $colm11 = 0;
                                                                                        $colm12 = 0;

                                                                                        for ($year = 2018; $year <= 2020; $year++) {

                                                                                            $Recmonth1 = getsingleyearmonthAtten($projID, $userID, '1', $year);
                                                                                            ($Recmonth1) ? $Recmonth1->current_mm : '';
                                                                                            $colm1 += $Recmonth1->current_mm;
                                                                                            //2
                                                                                            $Recmonth2 = getsingleyearmonthAtten($projID, $userID, '2', $year);
                                                                                            ($Recmonth2) ? $Recmonth2->current_mm : '';
                                                                                            $colm2 += $Recmonth2->current_mm;
                                                                                            //3
                                                                                            $Recmonth3 = getsingleyearmonthAtten($projID, $userID, '3', $year);
                                                                                            ($Recmonth3) ? $Recmonth3->current_mm : '';
                                                                                            $colm3 += $Recmonth3->current_mm;
                                                                                            //4
                                                                                            $Recmonth4 = getsingleyearmonthAtten($projID, $userID, '4', $year);
                                                                                            ($Recmonth4) ? $Recmonth4->current_mm : '';
                                                                                            $colm4 += $Recmonth4->current_mm;
                                                                                            //5
                                                                                            $Recmonth5 = getsingleyearmonthAtten($projID, $userID, '5', $year);
                                                                                            ($Recmonth5) ? $Recmonth5->current_mm : '';
                                                                                            $colm5 += $Recmonth5->current_mm;
                                                                                            //6
                                                                                            $Recmonth6 = getsingleyearmonthAtten($projID, $userID, '6', $year);
                                                                                            ($Recmonth6) ? $Recmonth6->current_mm : '';
                                                                                            $colm6 += $Recmonth6->current_mm;
                                                                                            //7
                                                                                            $Recmonth7 = getsingleyearmonthAtten($projID, $userID, '7', $year);
                                                                                            ($Recmonth7) ? $Recmonth7->current_mm : '';
                                                                                            $colm7 += $Recmonth7->current_mm;
                                                                                            //8
                                                                                            $Recmonth8 = getsingleyearmonthAtten($projID, $userID, '8', $year);
                                                                                            ($Recmonth8) ? $Recmonth8->current_mm : '';
                                                                                            $colm8 += $Recmonth8->current_mm;
                                                                                            //9
                                                                                            $Recmonth9 = getsingleyearmonthAtten($projID, $userID, '9', $year);
                                                                                            ($Recmonth9) ? $Recmonth9->current_mm : '';
                                                                                            $colm9 += $Recmonth9->current_mm;
                                                                                            //10
                                                                                            $Recmonth10 = getsingleyearmonthAtten($projID, $userID, '10', $year);
                                                                                            ($Recmonth10) ? $Recmonth10->current_mm : '';
                                                                                            $colm10 += $Recmonth10->current_mm;
                                                                                            //11
                                                                                            $Recmonth11 = getsingleyearmonthAtten($projID, $userID, '11', $year);
                                                                                            ($Recmonth11) ? $Recmonth11->current_mm : '';
                                                                                            $colm11 += $Recmonth11->current_mm;
                                                                                            //12
                                                                                            $Recmonth12 = getsingleyearmonthAtten($projID, $userID, '12', $year);
                                                                                            ($Recmonth12) ? $Recmonth12->current_mm : '';
                                                                                            $colm12 += $Recmonth12->current_mm;
                                                                                        }

                                                                                        //Calculate..
                                                                                        $Rcolm1 += $colm1;
                                                                                        $Rcolm2 += $colm2;
                                                                                        $Rcolm3 += $colm3;
                                                                                        $Rcolm4 += $colm4;
                                                                                        $Rcolm5 += $colm5;
                                                                                        $Rcolm6 += $colm6;
                                                                                        $Rcolm7 += $colm7;
                                                                                        $Rcolm8 += $colm8;
                                                                                        $Rcolm9 += $colm9;
                                                                                        $Rcolm10 += $colm10;
                                                                                        $Rcolm11 += $colm11;
                                                                                        $Rcolm12 += $colm12;
                                                                                    }
                                                                                }
                                                                                ?>

                                                                                <table id="usertable">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <th>Jan</th>
                                                                                            <th>Feb</th>
                                                                                            <th>Mar</th>
                                                                                            <th>Apr</th>
                                                                                            <th>May</th>
                                                                                            <th>June</th>
                                                                                            <th>Jul</th>
                                                                                            <th>Aug</th>
                                                                                            <th>Sept</th>
                                                                                            <th>Oct</th>
                                                                                            <th>Nov</th>
                                                                                            <th>Dec</th>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><?= $Rcolm1; ?></td>
                                                                                            <td><?= $Rcolm2; ?></td>
                                                                                            <td><?= $Rcolm3; ?></td>
                                                                                            <td><?= $Rcolm4; ?></td>
                                                                                            <td><?= $Rcolm5; ?></td>
                                                                                            <td><?= $Rcolm6; ?></td>
                                                                                            <td><?= $Rcolm7; ?></td>
                                                                                            <td><?= $Rcolm8; ?></td>
                                                                                            <td><?= $Rcolm9; ?></td>
                                                                                            <td><?= $Rcolm10; ?></td>
                                                                                            <td><?= $Rcolm11; ?></td>
                                                                                            <td><?= $Rcolm12; ?></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>

                                                                            </div>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <!-- tab-content --> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- .vd_container --> 
                            </div>
                            

                        </div>
                        <!-- .container --> 
                    </div>


                </div>

               
            </div>

        </div>	
        <style>
            #usertable {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
                font-size: 12px;
            }

            #usertable td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            #usertable tr:nth-child(even) {
                background-color: #dddddd;
            }
            .accordion1 {
                background-color: #eee;
                color: #444;
                cursor: pointer;
                padding: 18px;
                width: 100%;
                border: none;
                text-align: left;
                outline: none;
                font-size: 15px;
                transition: 0.4s;
                border: 1px solid #fff;
            }



            .panel1 {
                padding: 10 18px;
                display: none;
                background-color: white;
                overflow: scroll;
            }
        </style>		
        <script type="text/javascript" src="<?= FRONTASSETS; ?>user/js/jquery.js"></script> 
        <script type="text/javascript" src="<?= FRONTASSETS; ?>user/js/bootstrap.min.js"></script> 
        <script type="text/javascript">
            var acc = document.getElementsByClassName("accordion1");
            var i;

            for (i = 0; i < acc.length; i++) {
                acc[i].addEventListener("click", function () {
                    this.classList.toggle("active");
                    var panel = this.nextElementSibling;
                    if (panel.style.display === "block") {
                        panel.style.display = "none";
                    } else {
                        panel.style.display = "block";
                    }
                });
            }

            $(document).ready(function () {
                "use strict";
                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    $container.isotope('layout');
                });

                // bind filter button click
                $('#filters').on('click', 'a', function () {
                    var filterValue = $(this).attr('data-filter');
                    $('#filters li').removeClass('active');
                    $(this).parent().addClass('active');
                    $container.isotope({filter: filterValue});
                });


                /* $('#user_comment').keydown(function(){
                 var message = $("#user_comment").val();
                 alert(message);
                 if (event.keyCode == 13) {
                 if (message == "") {
                 alert("Enter Some Text In Textarea");
                 } else {
                 $('#submit_cmnt').submit();
                 alert("Succesfully submitted:- " + message);
                 }
                 $("textarea").val('');
                 return false;
                 }
                 });  */



            });

            $(document).ready(function () {
                $('#user_comment').keypress(function (e) {
                    var key = e.which;
                    if (key == 13) {
                        var user_comment = $("#user_comment").val();
                        var comment_id = $("#comment_id").val();
                        //Comment Submit..
                        $.ajax({
                            type: 'POST',
                            dataType: "text",
                            url: "<?= base_url('projectplanning/usercomment/'); ?>",
                            data: {'user_comment': user_comment, 'comment_id': comment_id},
                            success: function (responData) {
                                location.reload(1);
                            },
                        });
                    }
                });
            });


        </script>
    </body>
</html>
