<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<style>
#table_length {
    position: absolute;
    margin-left: 100px !important;
}
.table tbody tr td, .table tbody th td {
    vertical-align: middle;
    white-space: normal !important;
}
</style>
 
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row clearfix">
                    <div class="col-lg-12">

                        <?php if ($this->session->flashdata('msg')) { ?>

                            <!-- <script>
                            toastr.success(<?//= //$this->session->flashdata('msg'); ?> , 'Message', {timeOut: 5000});
                            </script> -->
                            <div class="alert alert-success alert-dismissable" style="">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>
                                    <div id="msgs"> Success ! 
                                </strong> <?= $this->session->flashdata('msg'); ?></div>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('error_msg')) { ?>
                            <div class="alert alert-danger alert-dismissable" style="">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>
                                    <div id="msgs"> Error !
                                </strong> <?= $this->session->flashdata('error_msg'); ?> </div>
                            </div>
                        <?php } ?>


                        <div class="card">
                            <div class="header">
                                <form id="form-filter" class="form-horizontal">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label class="email"> Select Employee </label>
                                            <select id="emp_id" name="emp_id" class="form-control  chosen-select" >
                                                <option value=""> -- Select Employee -- </option>
                                                <?php
                                                if ($employee):
                                                    foreach ($employee as $value) {
                                                        ?>
                                                        <option value="<?= $value->id; ?>"><?= $value->userfullname; ?></option>
                                                    <?php } endif; ?>
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Operator </label>
                                            <select id="emp_cond" class="form-control">
                                                <option value="0">None</option>
                                                <option value="1">AND</option>
                                                <option value="2">OR</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Email </label>
                                            <input placeholder="Email" type="text" id="email_id" name="email_id" value="" class="form-control"/>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Operator </label>
                                            <select id="email_cond" class="form-control">
                                                <option value="0">None</option>
                                                <option value="1">AND</option>
                                                <option value="2">OR</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Select Designation </label>
                                            <select id="designation_id" name="designation_id[]" multiple class="form-control col-md-4 col-xs-12">
                                                <!--<option value=""> -- Select Designation -- </option>-->
                                                <?php
                                                if ($designation):
                                                    foreach ($designation as $rowr) {
                                                        ?>
                                                        <option value="<?= $rowr->fld_id; ?>"> <?= $rowr->designation_name; ?></option>
                                                    <?php } endif; ?>
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Operator </label>
                                            <select id="designation_cond" class="form-control">
                                                <option value="0">None</option>
                                                <option value="1">AND</option>
                                                <option value="2">OR</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Mobile No. </label>
                                            <input placeholder="Mobile" type="text" id="contact_no" name="contact_no" value="" class="form-control"/>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Operator </label>
                                            <select id="contact_cond" class="form-control">
                                                <option value="0">None</option>
                                                <option value="1">AND</option>
                                                <option value="2">OR</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Total MM </label>
                                            <input placeholder="Total MM" type="text" class="form-control" name="max_mm" id="max_mm" />
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Operator </label>
                                            <select id="balance_cond" class="form-control">
                                                <option value="0">None</option>
                                                <option value="1">AND</option>
                                                <option value="2">OR</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Balance MM </label>
                                            <input placeholder="Balance MM" type="text" id="balance_mm" name="balance_mm" value="" class="form-control"/>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Operator </label>
                                            <select id="project_cond" class="form-control">
                                                <option value="0">None</option>
                                                <option value="1">AND</option>
                                                <option value="2">OR</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Project </label>
                                            <select name="projectinput" id="projectinput"  class="form-control">
                                                <option value="">--select Project--</option>
                                                <?php
                                                if ($tmProject):
                                                    foreach ($tmProject as $value) {
                                                        ?>
                                                        <option value="<?= $value->id; ?>"><?= $value->project_name; ?></option>
                                                    <?php } endif; ?>
                                            </select>
                                        </div>


                                        <div class="col-sm-3">
                                            <label class="email"> &nbsp; </label> <br>
                                            <button type="button" id="btn-filter" class="btn btn-primary pull-right ml-2"> Filter </button> &nbsp;&nbsp;&nbsp;
                                            <button type="button" id="btn-reset" class="btn btn-default pull-right"> Reset </button>
                                        </div>
                                    </div>
                                </form>


                            </div>



                            <div class="row">
                                
                            </div>


                            <div class="row clearfix">
                                <div class="col-lg-12">
                    <div class="">
                        
                        <div class="body">
						
                        <div class="table-responsive"> 
                                   
                        <div class="col-md-12">
                                    <div id="colvis"></div>
                                    <div class="form-group has-success pull-right"> 
                                        
                                    </div>
                                </div>
                                    
                               
                                    <table id="table" class="table table-striped display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                            <th>Sr. No</th>
                                            <th>Project Name</th>
                                            <th>Employee Name</th>
                                            <th>Email ID</th>
                                            <th>Mobile Number</th>
                                            <th>Designation</th>
                                            <th>Marks</th>
                                            <th>Firm</th>
                                            <th>Total MM</th>
                                            <th>Balance MM</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>

                                <tfoot>
                                    <tr>
                                            <th>Sr. No</th>
                                            <th>Project Name</th>
                                            <th>Employee Name</th>
                                            <th>Email ID</th>
                                            <th>Mobile Number</th>
                                            <th>Designation</th>
                                            <th>Marks</th>
                                            <th>Firm</th>
                                            <th>Total MM</th>
                                            <th>Balance MM</th>
                                    </tr>
                                </tfoot>
                                <hr>
                            </table>
							</div>
                        </div>
                    </div>
                </div>
            </div>




                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view('admin/includes/footer'); ?>
       
    </div>
   <style>
        #table_length {
            margin-left: 20px;
        }

        #table_filter {
            margin-right: 2%;
        }

        .designation_detail {
            display: none;
        }
    </style>

    <script type="text/javascript">
         $(document).ready(function () {
            $("li#submitted_search").addClass('active');
            $("li#bidprojects").addClass('active');
        });
        function assignpm(projid) {
            $("#assigntenderid").val(projid);
            $("#projectstatus").val(projid);
        }

        //$("#emp_id").chosen();
        // $("#projectinput").chosen();
        $('#designation_id').multiselect({
            columns: 1,
            placeholder: 'Select Designation',
            search: true
        });

        var table;
        $(document).ready(function() {
            //datatables
            table = $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "order": [],
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url('projectplanning/projectreportData') ?>",
                    "type": "POST",
                    "data": function(data) {
                        data.projectinput = $('#projectinput').val();
                        data.max_mm = $('#max_mm').val();
                        data.balance_mm = $('#balance_mm').val();
                        data.emp_id = $('#emp_id').val();
                        data.emp_cond = $('#emp_cond').val();
                        data.email_id = $('#email_id').val();
                        data.email_cond = $('#email_cond').val();
                        data.designation_id = $('#designation_id').val();
                        data.designation_cond = $('#designation_cond').val();
                        data.contact_no = $('#contact_no').val();
                        data.contact_cond = $('#contact_cond').val();
                        data.balance_cond = $('#balance_cond').val();
                        data.project_cond = $('#project_cond').val();
                    },
                },
                "dom": 'lBfrtip',
                "buttons": [{
                    extend: 'collection',
                    text: 'Export',
                    buttons: ['copy', 'excel', 'csv', 'pdf', 'print']
                }],
                //Set column definition initialisation properties.
                "columnDefs": [{
                    "targets": [0],
                    "orderable": false,
                }, ],
                "aLengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
            });

            var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
            $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
            $('#btn-filter').click(function() { //button filter event click
                table.ajax.reload(); //just reload table
            });
            $('#btn-reset').click(function() { //button reset event click
                $('#form-filter')[0].reset();
                table.ajax.reload(); //just reload table
            });
        });

        
    </script>
</body>