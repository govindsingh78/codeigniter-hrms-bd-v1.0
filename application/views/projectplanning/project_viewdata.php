<html lang="en">
    <?php
    error_reporting(E_ALL); //error_reporting(0);
    $statusArr = array('won' => '1', 'loose' => '2', 'awaiting' => '0', 'cancel' => '3');
    ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        <div class="wrapper">
            <?php $this->load->view('include/sidebar'); ?>
            <div id="content" class="col-lg-10 col-sm-10">
                <div>
                    <ul class="breadcrumb">
                        <li> <a href="<?= base_url(); ?>">Home</a></li>
                        <li><a href="#">Project User Detail </a></li>
                    </ul>
                </div>
                <div class="row">
                    <?php if ($this->session->flashdata('msg')) { ?>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                        </div>
                    <?php } ?>
                    <div class="box col-md-12">
                        <div class="panel panel-default">
                            <?php
                            if (!empty($data)) {
                                $empID = isset($data[0]->emp_id) ? $data[0]->emp_id : '';
                                $empname = UserNameById($empID);
                                ?>
                                <h4 style="padding: 1%;color: black;"><?= isset($tenderdetails[0]->TenderDetails) ? $tenderdetails[0]->TenderDetails : ''; ?></h4>
                                <a target="_blank" href="<?= base_url('projectplanning/userprofile/' . $empID); ?>"><h5 style="text-align:center;color: #7386D5;font-size: 20px;"><?= isset($empname->userfullname) ? $empname->userfullname : ''; ?></h5></a>


                                <div class="panel-body">
                                    <div class="row">

                                        <div class="box">

                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <!--Project View Section start -->
                                                <table>
                                                    <tr>
                                                        <th>Year</th>
                                                        <th>Jan</th>
                                                        <th>Feb</th>
                                                        <th>Mar</th>
                                                        <th>Apr</th>
                                                        <th>May</th>
                                                        <th>June</th>
                                                        <th>Jul</th>
                                                        <th>Aug</th>
                                                        <th>Sept</th>
                                                        <th>Oct</th>
                                                        <th>Nov</th>
                                                        <th>Dec</th>

                                                    </tr>


                                                    <?php
                                                    if ($yearsRecArr) :
                                                        foreach ($yearsRecArr as $recRW) {
                                                            ?>
                                                            <tr>
                                                                <td><?= $recRW->year; ?></td>
                                                                <td>
                                                                    <?php
                                                                    $Recmonth1 = getsingleyearmonthAtten($recRW->bd_projId, $recRW->emp_id, '1', $recRW->year);
                                                                    echo ($Recmonth1) ? $Recmonth1->current_mm : '';
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $Recmonth2 = getsingleyearmonthAtten($recRW->bd_projId, $recRW->emp_id, '2', $recRW->year);
                                                                    echo ($Recmonth2) ? $Recmonth2->current_mm : '';
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $Recmonth3 = getsingleyearmonthAtten($recRW->bd_projId, $recRW->emp_id, '3', $recRW->year);
                                                                    echo ($Recmonth3) ? $Recmonth3->current_mm : '';
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $Recmonth4 = getsingleyearmonthAtten($recRW->bd_projId, $recRW->emp_id, '4', $recRW->year);
                                                                    echo ($Recmonth4) ? $Recmonth4->current_mm : '';
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $Recmonth5 = getsingleyearmonthAtten($recRW->bd_projId, $recRW->emp_id, '5', $recRW->year);
                                                                    echo ($Recmonth5) ? $Recmonth5->current_mm : '';
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $Recmonth6 = getsingleyearmonthAtten($recRW->bd_projId, $recRW->emp_id, '6', $recRW->year);
                                                                    echo ($Recmonth6) ? $Recmonth6->current_mm : '';
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $Recmonth7 = getsingleyearmonthAtten($recRW->bd_projId, $recRW->emp_id, '7', $recRW->year);
                                                                    echo ($Recmonth7) ? $Recmonth7->current_mm : '';
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $Recmonth8 = getsingleyearmonthAtten($recRW->bd_projId, $recRW->emp_id, '8', $recRW->year);
                                                                    echo ($Recmonth8) ? $Recmonth8->current_mm : '';
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $Recmonth9 = getsingleyearmonthAtten($recRW->bd_projId, $recRW->emp_id, '9', $recRW->year);
                                                                    echo ($Recmonth9) ? $Recmonth9->current_mm : '';
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $Recmonth10 = getsingleyearmonthAtten($recRW->bd_projId, $recRW->emp_id, '10', $recRW->year);
                                                                    echo ($Recmonth10) ? $Recmonth10->current_mm : '';
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $Recmonth11 = getsingleyearmonthAtten($recRW->bd_projId, $recRW->emp_id, '11', $recRW->year);
                                                                    echo ($Recmonth11) ? $Recmonth11->current_mm : '';
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $Recmonth12 = getsingleyearmonthAtten($recRW->bd_projId, $recRW->emp_id, '12', $recRW->year);
                                                                    echo ($Recmonth12) ? $Recmonth12->current_mm : '';
                                                                    ?>
                                                                </td>


                                                            </tr>
                                                        <?php } endif; ?>

                                                </table>




                                            </div>
                                            <!-- /.box-footer -->
                                        </div>
                                    </div>

                                </div>
                            <?php } else { ?>	
                                <div class="panel-body"><p align="center">Detail Not Found</p> </div>
                            <?php } ?>
                        </div>
                    </div>

                    <!-- open Phase Section-->

                </div>
                <!--Close phase Section-->
            </div>

        </div>	



        <!-- BootStrap Model For Comments.. -->
        <?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->
        <?php $this->load->view('include/footer'); ?>
        <?php $this->load->view('include/datatablejs'); ?>
        <!-- Code For Multi Select By Asheesh -->
        <link href="<?= FRONTASSETS; ?>fm.selectator.jquery.css" rel="stylesheet" type="text/css">
        <script src="<?= FRONTASSETS; ?>fm.selectator.jquery.js"></script> 
        <script src="<?= FRONTASSETS; ?>ckeditor/ckeditor.js"></script> 
        <script src="<?= FRONTASSETS; ?>js/jquery.multiselect.js"></script>
    </body>
</html>

<style>
    table{
        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
        font-size: 14px;
    }
</style>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>