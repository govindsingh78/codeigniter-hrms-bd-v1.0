<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        <div class="wrapper">
			<?php $this->load->view('include/sidebar'); ?>
            <div id="content" class="col-lg-10 col-sm-10">
                    <!-- content starts -->
                    <div>
                        <ul class="breadcrumb">
                            <li> <a href="<?= base_url(); ?>">Home</a></li>
                            <li><a href="#"><?= $title; ?></a></li>
                        </ul>
                    </div>
                    <div class="row">
                        <?php if ($this->session->flashdata('msg')) { ?>
                            <div class="alert alert-success alert-dismissable" >
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                            </div>
                        <?php } ?>
                        <div class="box col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <form id="form-filter" class="form-horizontal">
                                        <div class="row">
                                            <div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <label class="col-sm-2 control-label" for="selectError2">Project:</label>
                                                <div class="col-sm-10">
                                                    <select name="projectinput" id="projectinput"  class="form-control">
                                                        <option Selected value="">--All--</option>
                                                        <?php
                                                        if ($tmProject):
                                                            foreach ($tmProject as $value) {
                                                                ?>
                                                                <option value="<?= $value->id; ?>"><?= $value->project_name; ?></option>
                                                            <?php } endif; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                                <label class="col-sm-4 control-label">Max MM</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="max_mm" id="max_mm" />
                                                </div>
                                            </div>
											
											
                                        </div>
                                        <div class="col-sm-4" style="margin: 1%;">
                                            <div class="col-sm-12">
                                                <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                                                <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                        <!-- open Phase Section-->
                        <div class="box-inner">
                            <div class="box-content">
                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="colvis"></div>
                                            </div>
                                        </div>
                                        <table id="table" class="display" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Sr. No</th>
                                                    <th>Project Name</th>
                                                    <th>Client Name</th>
                                                    <th>Start Date</th>
                                                    <th>End Date</th>
                                                    <th>Service</th>
                                                    <th>Total MM </th>
                                                    <th>Actions </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot>
                                                <tr>
													<th>Sr. No</th>
                                                    <th>Project Name</th>
                                                    <th>Client Name</th>
                                                    <th>Start Date</th>
                                                    <th>End Date</th>
													<th>Service</th>
                                                    <th>Total MM </th>
                                                    <th>Actions </th>
                                                </tr>
                                            </tfoot>
                                            <hr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Close phase Section-->

                </div>
          
        </div>	
        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>

        <hr>

        


        



        <!-- BootStrap Model For Comments.. -->
        <?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->
        <?php $this->load->view('include/footer'); ?>
		<?php $this->load->view('include/datatablejs'); ?>
		<style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            #chatbox {padding: 15px;overflow: scroll;height: 300px;}
        </style>
        <script type="text/javascript">
            $(document).ready(function () {
				$("li#submitted_search").addClass('active');
                $("li#bidprojects").addClass('active');
            });
            function assignpm(projid) {
                $("#assigntenderid").val(projid);
                $("#projectstatus").val(projid);
            }
           
            var table;
            $(document).ready(function () {
                //datatables
                table = $('#table').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "order": [],
                    "ajax": {
                        "url": "<?php echo site_url('projectplanning/AllprojectData') ?>",
                        "type": "POST",
                        "data": function (data) {
                            data.projectinput = $('#projectinput').val();
                            data.max_mm = $('#max_mm').val();
						},
                    },
                    "dom": 'lBfrtip',
                    "buttons": [{extend: 'collection', text: 'Export', buttons: ['copy', 'excel', 'csv', 'pdf', 'print']}
                    ],
                    "columnDefs": [{"targets": [0], "orderable": false, }, ],
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                });
                var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
                $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
                $('#btn-filter').click(function () { //button filter event click
                    table.ajax.reload();  //just reload table
                });
                $('#btn-reset').click(function () { //button reset event click
                    $('#form-filter')[0].reset();
                    table.ajax.reload();  //just reload table
                });
            });

            
        </script>

    </div>
</body>
</html>
