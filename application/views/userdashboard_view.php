<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?> </h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>                            
                                <li class="breadcrumb-item active">Home / <?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>   
                        
                        
                        
                    </div>
                </div>


                <div class="row clearfix">
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body">
                                <div class="icon text-info"><i class="fa fa-user"></i> </div>
                                <div class="content">
                                    <div class="text"><?= date("M d", strtotime($recInOutArr->FirstIn)); ?><sup><b> th</b></sup> &nbsp(Out Time)</div>
                                    <h5 class="number"><?= date("h:i A", strtotime($recInOutArr->LastOut)); ?></h5>
                                </div>
                                <hr>
                                <div class="icon text-warning"><i class="fa fa-user-circle"></i> </div>
                                <div class="content">
                                    <div class="text"><?= date("M d"); ?><sup><b> th</b></sup> &nbsp(In Time)</div>
                                    <h5 class="number"><?= date("h:i A", strtotime($recInOutArr->FirstIn)); ?></h5>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body">
                                <div class="icon text-warning"><i class="fa fa-tags"></i> </div>
                                <div class="content">
                                    <div class="text">Total Delay</div>
                                    <h5 class="number">20 min.</h5>
                                </div>
                                <hr>
                                <div class="icon"><i class="fa fa-graduation-cap"></i> </div>
                                <div class="content">
                                    <div class="text">Left Minutes</div>
                                    <h5 class="number">28 min.</h5>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body">
                                <div class="icon text-danger"><i class="fa fa-credit-card"></i> </div>
                                <div class="content">
                                    <div class="text">Qtr Day Leave Availed</div>
                                    <h5 class="number">05</h5>
                                </div>
                                <hr>
                                <div class="icon text-success"><i class="fa fa-university"></i> </div>
                                <div class="content">
                                    <div class="text">Qtr Day Leave Balance</div>
                                    <h5 class="number">03</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body">
                                <div style="margin: 0px 0px 0 75px;" class="icon"><i class="fa fa-map-pin"></i> </div>
                                <!--<div class="content">
                                    
                                </div>
                                <hr>
                                <div class="icon text-success"><i class="fa fa-smile-o"></i> </div>-->
                                <div class="content" style="height:72px;text-align: center;">
                                    <div class="text" style="margin: 60px 0px 0px 0px;">During the month, my availability for work</div>
                                    <h5 class="number">97%</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="header">
                                <p style="margin:0 0 0 0;" >You have exceeded allowable limit on <span style="color:red;">07/01/2020 </span>by <span style="color:red;">22 min.</span></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="header">
                                <h2>University Survey</h2>
                                <ul class="header-dropdown">
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="javascript:void(0);">Action</a></li>
                                            <li><a href="javascript:void(0);">Another Action</a></li>
                                            <li><a href="javascript:void(0);">Something else</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row text-center">
                                    <div class="col-sm-3 col-6">
                                        <h4 class="margin-0">$231</h4>
                                        <p class="text-muted margin-0"> Today's</p>
                                    </div>
                                    <div class="col-sm-3 col-6">
                                        <h4 class="margin-0">$1,254</h4>
                                        <p class="text-muted margin-0">This Week's</p>
                                    </div>
                                    <div class="col-sm-3 col-6">
                                        <h4 class="margin-0">$3,298</h4>
                                        <p class="text-muted margin-0">This Month's</p>
                                    </div>
                                    <div class="col-sm-3 col-6">
                                        <h4 class="margin-0">$9,208</h4>
                                        <p class="text-muted margin-0">This Year's</p>
                                    </div>
                                </div>
                                <div id="m_bar_chart" class="graph m-t-20" style="position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg height="345" version="1.1" width="1289" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative; top: -0.25px;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Rapha�l 2.2.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text x="36.203125" y="309" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan></text><path fill="none" stroke="#eef0f2" d="M48.703125,309H1264" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="36.203125" y="238" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">22.5</tspan></text><path fill="none" stroke="#eef0f2" d="M48.703125,238H1264" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="36.203125" y="167" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">45</tspan></text><path fill="none" stroke="#eef0f2" d="M48.703125,167H1264" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="36.203125" y="96" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">67.5</tspan></text><path fill="none" stroke="#eef0f2" d="M48.703125,96H1264" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="36.203125" y="25" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">90</tspan></text><path fill="none" stroke="#eef0f2" d="M48.703125,25H1264" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="1177.193080357143" y="321.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2017</tspan></text><text x="1003.5792410714286" y="321.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2016</tspan></text><text x="829.9654017857143" y="321.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2015</tspan></text><text x="656.3515625" y="321.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2014</tspan></text><text x="482.7377232142857" y="321.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013</tspan></text><text x="309.12388392857144" y="321.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2012</tspan></text><text x="135.51004464285714" y="321.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2011</tspan></text><rect x="70.40485491071428" y="56.55555555555557" width="41.40345982142858" height="252.44444444444443" rx="0" ry="0" fill="#4978b1" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="114.80831473214286" y="132.2888888888889" width="41.40345982142858" height="176.7111111111111" rx="0" ry="0" fill="#7e9bc8" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="159.21177455357144" y="28.155555555555566" width="41.40345982142858" height="280.84444444444443" rx="0" ry="0" fill="#b6c3dc" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="244.01869419642856" y="72.33333333333334" width="41.40345982142858" height="236.66666666666666" rx="0" ry="0" fill="#4978b1" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="288.4221540178571" y="103.88888888888889" width="41.40345982142858" height="205.11111111111111" rx="0" ry="0" fill="#7e9bc8" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="332.8256138392857" y="189.0888888888889" width="41.40345982142858" height="119.9111111111111" rx="0" ry="0" fill="#b6c3dc" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="417.63253348214283" y="122.82222222222222" width="41.40345982142858" height="186.17777777777778" rx="0" ry="0" fill="#4978b1" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="462.0359933035714" y="214.33333333333334" width="41.40345982142858" height="94.66666666666666" rx="0" ry="0" fill="#7e9bc8" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="506.439453125" y="192.24444444444447" width="41.40345982142858" height="116.75555555555553" rx="0" ry="0" fill="#b6c3dc" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="591.2463727678571" y="72.33333333333334" width="41.40345982142858" height="236.66666666666666" rx="0" ry="0" fill="#4978b1" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="635.6498325892857" y="103.88888888888889" width="41.40345982142858" height="205.11111111111111" rx="0" ry="0" fill="#7e9bc8" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="680.0532924107142" y="182.77777777777777" width="41.40345982142858" height="126.22222222222223" rx="0" ry="0" fill="#b6c3dc" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="764.8602120535713" y="135.44444444444446" width="41.40345982142858" height="173.55555555555554" rx="0" ry="0" fill="#4978b1" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="809.2636718749999" y="182.77777777777777" width="41.40345982142858" height="126.22222222222223" rx="0" ry="0" fill="#7e9bc8" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="853.6671316964284" y="167" width="41.40345982142858" height="142" rx="0" ry="0" fill="#b6c3dc" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="938.4740513392856" y="72.33333333333334" width="41.40345982142858" height="236.66666666666666" rx="0" ry="0" fill="#4978b1" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="982.8775111607141" y="103.88888888888889" width="41.40345982142858" height="205.11111111111111" rx="0" ry="0" fill="#7e9bc8" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="1027.2809709821427" y="182.77777777777777" width="41.40345982142858" height="126.22222222222223" rx="0" ry="0" fill="#b6c3dc" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="1112.087890625" y="34.4666666666667" width="41.40345982142858" height="274.5333333333333" rx="0" ry="0" fill="#4978b1" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="1156.4913504464287" y="31.31111111111113" width="41.40345982142858" height="277.68888888888887" rx="0" ry="0" fill="#7e9bc8" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="1200.894810267857" y="195.4" width="41.40345982142858" height="113.6" rx="0" ry="0" fill="#b6c3dc" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect></svg><div class="morris-hover morris-default-style" style="left: 0px; top: 172px; display: none;"><div class="morris-hover-row-label">2014</div><div class="morris-hover-point" style="color: #4978b1">
                                            A:
                                            75
                                        </div><div class="morris-hover-point" style="color: #7e9bc8">
                                            B:
                                            65
                                        </div><div class="morris-hover-point" style="color: #b6c3dc">
                                            C:
                                            40
                                        </div></div></div>                            
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>

