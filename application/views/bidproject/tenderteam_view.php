<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        <link href="<?= FRONTASSETS; ?>Customselect/css/jquery-customselect-1.9.1.css?>" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">


        <div class="ch-container">
            <div class="row">
                <!-- left menu starts -->
                <div class="col-sm-2 col-lg-2">
                    <div class="sidebar-nav">
                        <div class="nav-canvas">
                            <div class="nav-sm nav nav-stacked">
                            </div>
                            <?php $this->load->view('include/sidebar'); ?>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div id="content" class="col-lg-10 col-sm-10">
                    <!-- content starts -->
                    <div>
                        <ul class="breadcrumb">
                            <li> <a href="<?= base_url(); ?>">Home</a></li>
                            <li>
                                <a href="#">Tender Team Details </a>
                            </li>
                        </ul>
                    </div>

                    <div class="row">
                        

                        <!-- open Phase Section-->
                        <div class="box-inner">
                            <div class="box-content">
                                <div class="tab-content">

                                    <div id="home" class="tab-pane fade in active">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span class="col-md-12" style="
                                                      color: green;margin: 0,auto;text-align: center;font-size: 20px;"><?php echo $resulstArr[0]['generated_tenderid']; ?></span>
                                                <div id="colvis" style="float:right">
 
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php echo $resulstArr[0]['TenderDetails']; ?>

                                            </div>
                                            <div class="col-md-12">
                                                <div style="overflow-x: scroll;">
                                                    <form name="req_form" action="<?= base_url('cru/saveteamdata'); ?>" method="post" >
                                                        <table id="table" class="display" cellspacing="0" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 22%;padding: 1% 0%;">Designation</th>
                                                                    <th>Personal Details</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Marks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Firm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TotalMM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                                    <th>Construction Man Month</th>
                                                                    <th>Development Man Month</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O&M &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AgeLimit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Certifcate&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Undertaking&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Salary&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Remarks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>



                                                                </tr>
                                                            </thead>
                                                            <hr/>

                                                            <?php
                                                            // echo '<pre>';
                                                            // foreach($resulstArr as $rows){ 
                                                            // print_r($rows);
                                                            // }
                                                            $n = 0;
                                                            foreach ($resulstArr as $rows) {
                                                                ?>
                                                                <tbody>
                                                                <input type="hidden" id="team_id<?= $n; ?>" name="team_id[]" value="<?php echo $rows['id']; ?>">
                                                                <td><?php echo $rows['designation_name']; ?></td>
                                                                <td>
                                                                    <div class="controls">
                                                                            <!--<select data-placeholder="Select Sector" id="selectuser<?= $n; ?>"  name="sectorinput" data-rel="chosen">-->
                                                                        <select  id="selectuser<?= $n; ?>" class='custom-select' name="username[]" class="standard" disabled>
                                                                            <option value="">Please select Name </option>
                                                                            <?php
                                                                            if ($userData):
                                                                                foreach ($userData as $row) {
                                                                                    ?>
                                                                                    <option <?= isset($rows['user_id']) ? "selected='selected'" : ''; ?> value="<?= $row->fld_id; ?>"><?= $row->userfullname; ?></option>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                                <option <?= (isset($rows['user_id']) && $rows['user_id'] == 0) ? "selected='selected'" : ''; ?> value="0">other</option>				
                                                                                <?php
                                                                            endif;
                                                                            ?>
                                                                        </select>
                                                                        <br/>
                                                                        <br/>
                                                                        <?php if (!empty($rows['otheruser_name'])) { ?>
                                                                            <input disabled type='text' value="<?php echo $rows['otheruser_name']; ?>" class="form-control"/>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                        <input type='text' placeholder ="Name" name="other[]" id="other<?= $n; ?>" value="" class="form-control hidden1" disabled/>
                                                                        <input type="email" disabled name="emailaddress[]" value="<?= isset($rows['emailaddress']) ? $rows['emailaddress'] : ''; ?>" maxlength="50" value="" class="form-control" id="emailaddress<?= $n; ?>" placeholder ="Email Address">
                                                                        <input maxlength="10" disabled pattern="[0-9]{10}" value="<?= isset($rows['contactnumber']) ? $rows['contactnumber'] : ''; ?>" name="contactnumber[]" class="form-control" id="contactnumber<?= $n; ?>"  placeholder ="Contact Number">

                                                                    </div>	

                                                                </td>
                                                                <td><input disabled type="number" value="<?= isset($rows['marks']) ? $rows['marks'] : ''; ?>" id="marks<?= $n; ?>" name="marks[]" class="form-control"></td>
                                                                <td><input disabled type="text" value="<?= isset($rows['firm']) ? $rows['firm'] : ''; ?>" id="firm<?= $n; ?>" name="firm[]" class="form-control"></td>
                                                                <td><input disabled type="number" id="man_months<?= $n; ?>" name="man_months[]" value="<?php echo $rows['man_months']; ?>" class="form-control"></td>
                                                                <td><input disabled type="number" id="construction_months<?= $n; ?>" name="construction_months[]" value="<?php echo $rows['construction_months']; ?>" class="form-control"></td>
                                                                <td><input disabled type="number" id="development_months<?= $n; ?>" name="development_months[]" value="<?php echo $rows['development_months']; ?>" class="form-control"></td>
                                                                <td><input disabled type="number" id="om_months<?= $n; ?>" name="om_months[]" value="<?php echo $rows['om_months']; ?>" class="form-control"></td>
                                                                <td><input disabled type="number" id="age_limit<?= $n; ?>" name="age_limit[]" value="<?php echo $rows['age_limit']; ?>" class="form-control"></td>
                                                                <td>
                                                                    <select name="certificate[]" class="form-control" id="certificate<?= $n; ?>" disabled>
                                                                        <option >Select</option>
                                                                        <option <?= (isset($rows['certificate']) && $rows['certificate'] == '1') ? "selected='selected'" : ''; ?> value="1"> Yes </option>
                                                                        <option <?= (isset($rows['certificate']) && $rows['certificate'] == '2') ? "selected='selected'" : ''; ?> value="2"> No </option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select name="undertaking[]" class="form-control" id="undertaking<?= $n; ?>" disabled>
                                                                        <option>Select</option>
                                                                        <option <?= (isset($rows['undertaking']) && $rows['undertaking'] == '1') ? "selected='selected'" : ''; ?>  value="1"> Yes </option>
                                                                        <option <?= (isset($rows['undertaking']) && $rows['undertaking'] == '2') ? "selected='selected'" : ''; ?> value="2"> No </option>
                                                                    </select>
                                                                </td>
                                                                <td><input disabled type="number" id="salary<?= $n; ?>" name="salary[]" value="<?php echo $rows['salary']; ?>" class="form-control"></td>
                                                                <td> <textarea disabled name="remarks[]" id="remarks<?= $n; ?>" class="form-control"><?php echo $rows['remarks']; ?></textarea></td>


                                                                </tbody>
                                                                <?php
                                                                $n++;
                                                            }
                                                            ?>


                                                        </table>


                                                    </form>
                                                </div>

                                            </div>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Close phase Section-->

                </div>

            </div>
        </div>	

        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>

        <hr>




        <!-- BootStrap Model For Comments.. -->
        <?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->

        <?php $this->load->view('include/footer'); ?>

        <script src="<?= FRONTASSETS; ?>jquery/jquery-2.2.3.min.js?>"></script>
        <script src="<?= FRONTASSETS; ?>js/bootstrap.min.js?>" ></script>
        <script src="<?= FRONTASSETS; ?>Customselect/js/jquery-customselect-1.9.1.js"></script>
        <script src="<?= FRONTASSETS; ?>Customselect/js/jquery-customselect-1.9.1.min.js"></script>
        <style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            .designation_detail{display:none;}
            #selectuser_chosen{width:251px!important;}
            .hidden1{display:none;}

        </style>
        <script type="text/javascript">
            $(function () {
                $(".standard").customselect();
            });
            //$('[data-rel="chosen"],[rel="chosen"]').chosen();
            $(document).ready(function () {
                $("li#teamrequisition").addClass('active');
            });

            function lock_data(ids) {
                $.ajax({
                    type: 'post',
                    url: "<?= base_url('cru/locadata'); ?>",
                    data: {id: ids},
                    success: function (response) {
                        //console.log(response);
                        location.reload(1);
                    }
                });
            }
        </script>

    </div>
</body>
</html>
