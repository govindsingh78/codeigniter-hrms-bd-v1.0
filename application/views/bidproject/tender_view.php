<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<style>
    #table_length,
    #table1_length,
    #table2_length,
    #table3_length {
        position: absolute;
        margin-left: 100px !important;
    }

    .table tbody tr td,
    .table tbody th td {
        vertical-align: middle;
        white-space: normal !important;
    }

    ul.nav.nav-tabs-new {
        float: right !important;
        margin: auto;
    }
</style>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>


             
        <?php
//Financial Year Condition Code By Asheesh.. 14-02-2019.
        $year = '2016';
        $YearPluse1 = (date('Y') + 1);
        $finCdateStart = "01-04-" . date('Y');
// $finCdateEnd = "31-03-" . $YearPluse1;
        $gEtCuurentDate = date("d-m-Y");
        $gEtCuurentDateCONV = strtotime($gEtCuurentDate);
        $finCdateStartCONV = strtotime($finCdateStart);
        if ($finCdateStartCONV >= $gEtCuurentDateCONV):
            $currentyear = date('Y') - 1;
        else:
            $currentyear = date('Y');
        endif;
        ?>
      
                    <?php if ($this->session->flashdata('msg')) { ?>
                        <div class="alert alert-success alert-dismissable" >
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong><div id="msgs">Success!</strong> <?= $this->session->flashdata('msg'); ?></div>
                        </div>
                    <?php } ?>
                    
                    







                    

                   				
                   
  



                        <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="header">
                                <form id="form-filter">
                                    <!--<div class="card-header" id="headingOne">-->
                                    <div class="row clearfix">
                                        <!-- <div class="col-lg-3 col-md-6">
                                        <label class="col-sm-3 control-label" for="selectError2">Sector:</label>
                                            <div class="col-sm-8"> -->

                                          
                                        <div class="col-lg-2 col-md-6">
                                            <label>Sector :</label>
                                            <div class="input-group mb-3">
                                                <select name="sectorinput" id="sectorinput"  class="form-control">
                                                    <option <?= ($secId == "") ? 'Selected' : ''; ?> value="">--All--</option>
                                                    <?php
                                                    if ($sectorArr):
                                                        foreach ($sectorArr as $row) {
                                                            ?>
                                                            <option <?= ($secId == $row->fld_id) ? 'Selected' : ''; ?> value="<?= $row->fld_id; ?>"><?= $row->sectName; ?></option>
                                                        <?php } endif; ?>
                                                </select>
                                            </div>
                                        
                                        
                                        </div> 
                                            <div class="col-lg-3 col-md-6">
                                            <label>Filter :</label>
                                            <div class="input-group mb-3">
                                       
                                                <select id="company_name" class="form-control">
                                                    <option value="" selected="selected">--All--</option>
                                                    <option value="E">EOI</option>
                                                    <option value="P">RFP</option>
                                                    <option value="FQ">FQ</option>
                                                </select>
                                            </div>
                                        
                                        
                                        </div>

                                       
                                        <div class="col-lg-3 col-md-6">
                                            <label>Financial Year :</label>
                                            <div class="input-group mb-3">  
                                         
                                                <select id="financial_year" class="form-control">
                                                    <!--<option value="" selected="selected">--Financial Year--</option>-->
                                                    <option <?php echo ($currentyear == '') ? 'selected' : ''; ?> value="">--Select All--</option>
													<?php
                                                    for ($i = $year; $i <= 2022; $i++) {
                                                        ?>
                                                        <option <?php echo ($currentyear == $i) ? 'selected' : ''; ?> value=<?= $i; ?>><?= $i; ?>-<?= $i + 1; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>    
                                        
                                        
                                        </div>


                                        <div class="col-lg-2 col-md-6">
                                            <label>Status :</label>
                                            <div class="input-group mb-3">  
                                         

                                                <select id="financial_status" class="form-control">
                                                    <option value="" selected="selected">--Select--</option>
                                                    <option value="0">Awaiting</option>
                                                    <option value="1">Won</option>
                                                    <option value="2">Loose</option>
                                                    <option value="3">Cancel</option>
                                                </select>

                                            </div>
                                        
                                        
                                        </div>



                                        

                                        <div class="col-lg-2 col-md-12 mt-4">

                                           
                                        
                                        
                                            <button type="button" id="btn-filter" class="btn btn-info  ml-2 pull-right">Filter</button> &nbsp;
                                            <button type="button" id="btn-reset" class="btn btn-default ml-2 pull-right">Reset</button>


 
                                        </div>



                                        </div>

                                    </div>
                                    <!--</div> -->

                                </form>


 
                            <div class="body">


                                <div class="col-lg-12 col-md-12">

                                

                                    <ul class="nav nav-tabs-new">
                                        <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#all-new">Report 1</a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Home-new">Report 2</a></li>
                                     


                                    </ul>
                                </div>

                                 


                                <div class="tab-content">

                                <div class="tab-pane show active" id="all-new">
                                        <div class="table-responsive">
 
                                            <div id="colvis"></div>
                                        
                                    <table id="table"  class="table table-striped display" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr. No </th>
                                                <th style="width:10%">Exp-Date</th>
                                                <th>Tender Details</th>
                                                <th>Location</th>
                                                <th>Client</th>
                                                <th >Actions </th>
                                                <th style="width:20%">Reminder</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Exp-Date</th>
                                                <th>Tender Details</th>
                                                <th>Location</th>
                                                <th>Client</th>
                                                <th>Actions </th>
                                                <th>Reminder</th>
                                            </tr>
                                        </tfoot>
                                        <hr>
                                    </table>
                                    </div>
                                </div>







                                <div class="tab-pane" id="Home-new">
                                    <div class="table-responsive">
                            
                                
                                        <div id="colvis1"></div>
                                  
                                <table id="table1" class="table table-striped display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Sr. No. </th>
                                            <th>Exp-Date</th>
                                            <th style="width:30%;">Tender Details</th>
                                            <th>Country</th>
                                            <th>State</th>
                                            <th>Sector</th>
                                            <th>Service</th>
                                            <th style="width:10%;"> Consortium </th>
											<th style="width:10%;"> Competitor </th>
                                            <th>Client</th>
                                            <th>Client Name</th>
											<th>Client</th>
                                            <!--<th>Client Contact</th>-->
                                            <!--<th>Client Position</th>-->
                                            <th>Remark</th>
                                            <th>Actions </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Sr. No</th>
                                            <th>Exp-Date</th>
                                            <th style="width:30%;">Tender Details</th>
                                            <th>Country</th>
                                            <th>State</th>
                                            <th>Sector</th>
                                            <th>Service</th>
                                            
                                            <th style="width:10%;">Consortium</th>
											<th style="width:10%;"> Competitor </th>
											<th>Client</th>
                                            <th>Client Name</th>
											<th>Client</th>
                                            <!--<th>Client Contact</th>-->
                                            <!--<th>Client Position</th>-->
                                            <th>Remark</th>
                                            <th>Actions </th>
                                        </tr>
                                    </tfoot>
                                    <hr>
                                </table>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

        <!-- Change Status Modal -->
        <div class="modal fade" id="changestatus" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 id="tender_title" class="modal-title">Proposal Status</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <div class="modal-body">
                        <form action="<?= base_url('dashboard/changeprojstatus'); ?>" method="post">
                            <div class="box-content">
                                <div class="controls">
                                    <label>Select Proposal Status</label>
                                    <select required="required" class="form-control"  name="project_status">
                                        <option value="">--select--</option>
                                        <option value="0">Awaiting</option>
                                        <option value="1">Won</option>
                                        <option value="2">Loose</option>
                                        <option value="3">Cancel</option>
                                    </select>
                                </div>
                            </div>
                            <div class="box-content">
                                <input type="hidden" id="projectstatus" name="project_id" value="">
                                
                            </div>
                            <div class="modal-footer">
                            <input type="submit"  value="Submit" class="btn btn-info pull-right" >
                            <button class="btn btn-default  pull-right" data-dismiss="modal">X</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- close Modal -->


        <div class="modal fade" id="changedate" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 id="tender_title" class="modal-title">Project Expiry</h4>    
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <div class="modal-body">
                        <form action="<?= base_url('bidproject/changeprojdate'); ?>" method="post">
                            <div class="box-content">
                                <div class="controls">
                                    <label>Expiry Date</label>
                                    <input type="date" class="form-control" id="upd_date" name="upd_date">
                                </div>
                            </div>
                            <div class="box-content">
                                <input type="hidden" id="updproject_id" name="updproject_id" value="">
                                <input type="submit"  value="Update" class="btn btn-info pull-right" >
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
        </div>



        <div class="modal fade" id="myProjedit" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 id="tender_title" class="modal-title"></h4>
                    </div>
                    <div class="modal-body">

                        <form action="<?= base_url('dashboard/Updateproject'); ?>" method="post">
                            <div class="form-group required col-md-12">
                                <label class="control-label" for="usr">Project Name </label>
                                <textarea rows="3" name="project_name" id="project_nameUPD" required cols="27" class="form-control"></textarea>
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">Service </label>
                                <input type="text" name="service" id="serviceUPD" class="form-control" >
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">Client </label>
                                <input type="text" name="client" id="clientUPD" class="form-control" >
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">Start Year </label>
                                <input type="text" name="service" id="serviceUPD" class="form-control" >
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">Start Date </label>
                                <input type="date" name="start_date" id="start_dateUPD" class="form-control" >
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">End Date </label>
                                <input type="date" name="end_date" id="end_date" class="form-control" >
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">Funding </label>
                                <input type="text" name="end_date" id="end_date" class="form-control" >
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">Sector </label>
                                <select name="country" id="country" class="form-control" >
                                    <option value=""> Select Country </option>
                                </select>
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">Client </label>
                                <input type="text" name="end_date" id="end_date" class="form-control">
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">Tender Amount </label>
                                <input type="text" name="end_date" id="end_date" class="form-control" >
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">Tender Code </label>
                                <input type="number" step="any" name="end_date" id="end_date" class="form-control" >
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">Tender No </label>
                                <input type="text" name="end_date" id="end_date" class="form-control" >
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">Lane </label>
                                <input type="text" name="end_date" id="end_date" class="form-control" >
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">Length </label>
                                <input type="text" name="end_date" id="end_date" class="form-control" >
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">Country </label>
                                <select name="country" id="country" class="form-control" >
                                    <option value=""> Select Country </option>
                                    <?php
                                    if ($CountryrecArr):
                                        foreach ($CountryrecArr as $countr) {
                                            ?>
                                            <option value="<?= $countr->country_id; ?>" <?= ($countr->country_id == '100') ? 'selected' : ''; ?> ><?= $countr->country_name; ?></option>
                                            <?php
                                        }
                                    endif;
                                    ?>
                                </select>
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">State </label>
                                <select name="country" id="country" class="form-control" > 
                                    <option value=""> Select State </option>
                                    <?php
                                    if ($stateDetails):
                                        foreach ($stateDetails as $statr) {
                                            ?>
                                            <option value="<?= $statr->state_id; ?>"><?= $statr->state_name; ?></option>
                                        <?php } endif; ?>
                                </select>
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">City </label>
                                <select name="country" id="country" class="form-control" >
                                    <option value=""> Select Country </option>
                                </select>
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr"> &nbsp; </label>
                                <br>
                                <input type="hidden" id="projectstatus" name="project_id" value="">
                                <input type="submit"  value="Submit" class="btn btn-success glyphicon glyphicon-user" >
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-default red" data-dismiss="modal">X</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="clientmodel" role="dialog">
            <div class="modal-dialog" style="width:60%;">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title">Add Reminder</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>

                    <div class="modal-body">
                        <form name="frmm" id="frmm" method="post" action="<?= base_url('bidproject/reminderinfo'); ?>" enctype="">
                            <div class="row">
                                <div class="form-group required col-md-6">  
                                    <label> Reminder Date</label>
                                    <input required type="date" class="form-control" id="reminder_date" name="reminder_date" value="">
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> Subject</label>
                                    <input required type="text" class="form-control" id="reminder_subject" name="reminder_subject" value="">
                                </div>
                                <div class="form-group required col-md-12">  
                                    <label> Message</label>
                                    <textarea required name="reminder_message" class="form-control"> </textarea>
                                </div>
                                <div class="form-group required col-md-12 mt-4">
                                    <input type="hidden" name="project_id" id="project_iddata" class="btn green" >
                                    <input type="submit" class="btn btn-info pull-right" value="Submit">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group required col-md-12"> 
                                    <table border="1" id="clientadd" style="width:100%"></table>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>


        <?php $this->load->view('admin/includes/footer'); ?>
        <style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            #chatbox {padding: 15px;overflow: scroll;height: 300px;}
            #table1_wrapper {
                width: 94em;
                /*overflow-x: auto;*/
                white-space: nowrap;
            }
        </style>
        <script type="text/javascript">
            function editclientmodel(projid) {
                if (projid) {
                    $('#project_iddata').val(projid);
                    $('#clientadd').html('');
                    $.ajax({
                        type: 'POST',
                        url: "<?= base_url('bidproject/ajax_reminderinfo'); ?>",
                        data: {'project_id': projid},
                        success: function (responData) {
                            var parsed = jQuery.parseJSON(responData);
                            if (parsed) {
                                $('#clientadd').append('<tr><th>Date</th><th>Subject</th><th>Message</th><th>Action</th></tr>');
                                $.each(parsed, function (index, val) {
                                    //console.log(val.client_name);
                                    $('#clientadd').append('<tr><td>' + val.reminder_date + '</td><td>' + val.reminder_subject + '</td><td>' + val.reminder_message + '</td><td><a href="<?= base_url('bidproject/ajax_reminderdel'); ?>/' + val.id + '" class="clientinfodel">Delete</a></td></tr>');
                                });
                            }

                        },
                    });
                }
            }
            $(document).ready(function () {
                $("li#submitted_search").addClass('active');
                $("li#bidprojects").addClass('active');
            });
            function assignpm(projid) {
                $("#assigntenderid").val(projid);
                $("#projectstatus").val(projid);
            }
            function gocomment(tndrid) {
                //Put ProjId And Secot Id..
                $("#projId").val(tndrid);
                $.ajax({
                    type: 'POST',
                    dataType: "text",
                    data: {'tndrid': tndrid},
                    url: "<?= base_url('bidproject/commentset'); ?>",
                    success: function (responData) {
                        $("#chatbox").html(responData);
                    },
                });
            }

            //Comment Submitted...
            $(document).ready(function () {
                $('#commentmsg').keypress(function (e) {
                    var key = e.which;
                    if (key == 13) {
                        var comnt = $("#commentmsg").val();
                        var projId = $("#projId").val();
                        //Comment Submit..
                        $.ajax({
                            type: 'POST',
                            dataType: "text",
                            url: "<?= base_url('bidproject/commentsubmit'); ?>",
                            data: {'projId': projId, 'comnt': comnt},
                            success: function (responData) {
                                $("#commentmsg").val('');
                                return gocomment(projId);
                            },
                        });
                    }
                });
            });

            //Project No Go..
            function  nosubmitproj(tndrid, sectid) {
                if (confirm("Are You Sure No Submitted This.") == true) {
                    $.ajax({
                        type: 'POST',
                        url: "<?= base_url('bidproject/projnosubmit'); ?>",
                        data: {'actid': tndrid},
                        success: function (responData) {
                            console.log(responData);
                            $('.alert').css('display', 'block');
                            $('#msgs').html('Tender No Submitted.');
                            setTimeout(function () {
                                location.reload(1);
                            }, 1000);
                            location.reload(1);
                        },
                    });
                }
            }

            function bidproject(actprojid, sectid) {
                if (confirm("Are You Sure Bid This.") == true) {
                    $.ajax({
                        type: 'POST',
                        url: "<?= base_url('bidproject/bidprojectbyuser'); ?>",
                        data: {'projid': actprojid},
                        success: function (responData) {
                            console.log(responData);
                            $('.alert').css('display', 'block');
                            $('#msgs').html('Tender Bid.');
                            setTimeout(function () {
                                location.reload(1);
                            }, 1000);
                            //location.reload(1);
                        },
                    });
                }
            }
 
            function  nogoprojs(tndrid, sectid) {
                if (confirm("Are You Sure No Go This.") == true) {
                    $.ajax({
                        type: 'POST',
                        url: "<?= base_url('bidproject/nogosubmit'); ?>",
                        data: {'actid': tndrid},
                        success: function (responData) {
                            console.log(responData);
                            $('.alert').css('display', 'block');
                            $('#msgs').html('Tender No Go.');
                            setTimeout(function () {
                                location.reload(1);
                            }, 1000);
                        },
                    });
                }
            }

            var table;
            $(document).ready(function () {
                //datatables
                table = $('#table').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "order": [],
                    "ajax": {
                        "url": "<?php echo base_url('bidproject/newProjectAll') ?>",
                        "type": "POST",
                        "data": function (data) {
                            data.project_name = $('#company_name').val();
                            data.sectorinput = $('#sectorinput').val();
                            data.financial_status = $('#financial_status').val();
                            data.financial_year = $('#financial_year').val();
                        },
                    },
                    "dom": 'lBfrtip',
                    "buttons": [{extend: 'collection', text: 'Export', buttons: ['copy', 'excel', 'csv', 'pdf', 'print']}
                    ],
                    "columnDefs": [{"targets": [0], "orderable": false, }, ],
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                });
                var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
                $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
                $('#btn-filter').click(function () { //button filter event click
                    table.ajax.reload();  //just reload table
                });
                $('#btn-reset').click(function () { //button reset event click
                    $('#form-filter')[0].reset();
                    table.ajax.reload();  //just reload table
                });
            });


            // ###############################################//
            /***************code by durgesh (13-09-2019)*******************/
// ###############################################//

            var table1;
            $(document).ready(function () {
                //datatables
                table1 = $('#table1').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "order": [],
                    "ajax": {
                        "url": "<?php echo base_url('bidproject/newProjectnewAll') ?>",
                        "type": "POST",
                        "data": function (data) {
                            data.sectorinput = $('#sectorinput').val();
                            data.financial_year = $('#financial_year').val();
                            data.financial_status = $('#financial_status').val();
                        },
                    },
                    "dom": 'lBfrtip',
                    "buttons": [{extend: 'collection', text: 'Export', buttons: ['copy', 'excel', 'csv', 'pdf', 'print']}
                    ],
                    "columnDefs": [{"targets": [7, 8, 9,10,11, 12], "orderable": false, "visible": false}],
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                });
                var colvis1 = new $.fn.dataTable.ColVis(table1); //initial colvis
                $('#colvis1').html(colvis1.button()); //add colvis button to div with id="colvis"
                $('#btn-filter').click(function () { //button filter event click
                    table1.ajax.reload();  //just reload table
                });
                $('#btn-reset').click(function () { //button reset event click
                    $('#form-filter')[0].reset();
                    table1.ajax.reload();  //just reload table
                });
            });

// ###############################################//
            /***************code by durgesh (13-09-2019)*******************/
// ###############################################//

            //Edit Project for CEg Exp Moto..
            function projedit(prjid) {
                $.ajax({
                    type: 'POST',
                    url: "<?= base_url('bidproject/ajax_proj_detailsById'); ?>",
                    data: {'projId': prjid},
                    success: function (responData) {
                        var parsed = JSON.parse(responData);
                        var arr = [];
                        for (var x in parsed) {
                            arr.push(parsed[x]);
                        }
                        $('#project_nameUPD').val(arr[3]);
                    },
                });
            }


            //Edit Date of Project..
            function datechangeproj(prjid, dater) {
                $("#updproject_id").val(prjid);
                $("#upd_date").val(dater);
            }

        </script>
        <script>

            // ###############################################//
            /***************code by durgesh (13-09-2019)*******************/
            // ###############################################//

            // function openCity(evt, cityName) {
            //     var i, tabcontent, tablinks;
            //     tabcontent = document.getElementsByClassName("tabcontent");
            //     for (i = 0; i < tabcontent.length; i++) {
            //         tabcontent[i].style.display = "none";
            //     }
            //     tablinks = document.getElementsByClassName("tablinks");
            //     for (i = 0; i < tablinks.length; i++) {
            //         tablinks[i].className = tablinks[i].className.replace(" active", "");
            //     }
            //     document.getElementById(cityName).style.display = "block";
            //     evt.currentTarget.className += " active";
            // }
            // Get the element with id="defaultOpen" and click on it
            // document.getElementById("defaultOpen").click();

            // ###############################################//
            /***************code by durgesh (13-09-2019)*******************/
            // ###############################################//
        </script>

        <?php
        // ###############################################//
        /*         * *************code by durgesh (13-09-2019)****************** */
// ###############################################//
        ?>
        <style>

            .tablespace {
                padding: 7px 7px !important;
                text-align: left;
            }

            .tab {
                overflow: hidden;
                border: 1px solid #ccc;
                background-color: #f1f1f1;
            }
            /* Style the buttons inside the tab */
            .tab button {
                background-color: inherit;
                float: left;
                border: none;
                outline: none;
                cursor: pointer;
                padding: 14px 16px;
                transition: 0.3s;
                font-size: 17px;
            }
            /* Change background color of buttons on hover */
            .tab button:hover {
                background-color: #ddd;
            }
            /* Create an active/current tablink class */
            .tab button.active {
                background-color: #ccc;
            }
            /* Style the tab content */
            .tabcontent {
                display: none;
                padding: 6px 12px;
                border: 1px solid #ccc;
                border-top: none;
            }
            /* Style the close button */
            .topright {
                float: right;
                cursor: pointer;
                font-size: 28px;
            }
            .topright:hover {color: red;}
            /*        .dt-buttons, .dataTables_filter {
            display: none;
            }*/
            .panel-title a[aria-expanded="true"] {
                background: #1c4e7f!important;
                padding: 10px 30px 10px 10px!important;
                border-radius: 5px!important;
                margin: -15px!important;
            }
            .panel-title a[aria-expanded="true"]:active {
                background: #1c4e7f!important;
                padding: 10px 30px 10px 10px!important;
                border-radius: 5px!important;
                margin: -15px!important;
            }
            .panel-title a[data-toggle="collapse"] {
                padding: 10px 30px 10px 10px!important;
                margin: -15px!important;
            }

            .chosen-container{width:100%!important;}

        </style>
        <?php
        // ###############################################//
        /*         * *************code by durgesh (13-09-2019)****************** */
// ###############################################//
        ?>
    </div>
</body>
</html>
