<ul class="chat" style="padding: 0;">
    <?php 
    if ($allcomments) {
        foreach ($allcomments as $cmntRow) {
			
            $userId = $cmntRow->user_id; 
            $loginId = $this->session->userdata('uid');
            if ($userId == $loginId) {
                ?>
                <li class="left clearfix" style="list-style: none;">
                    <!--<span class="chat-img pull-right">
                        <img src="<?= FRONTASSETS."user.png"; ?>" alt="User Avatar" class="img-circle">
                    </span>-->
                    <div class="chat-body clearfix" style="border-bottom: 1px dashed;">
                        <div class="header" style="margin-top: 5px;margin-bottom: 5px;">
                            <strong class="primary-font"> &nbsp; <?php  $user = UserNameById($cmntRow->user_id); echo $user->userfullname; ?> 
                            </strong> <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"> </span> 
                            <?= date('d-m-Y H:i:s', strtotime($cmntRow->entrydate)); ?></small>
                        </div> 
                        <p style="padding-left: 2%;">
                            <?= $cmntRow->commentText; ?>
                        </p>
                    </div>

                </li>

        <?php } else { ?>

                <li class="right clearfix" style="list-style: none;">
                    <span class="chat-img pull-left">
                        <i class="img-circle glyphicon glyphicon-user icon-white"></i> 
                    </span>

                    <div class="chat-body clearfix">
                        <div class="header">
                            <strong class="primary-font"> &nbsp; <?php  $user = UserNameById($cmntRow->user_id); echo $user->userfullname; ?>
                            </strong> <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"> </span> 
									<?= date('d-m-Y H:i:s', strtotime($cmntRow->entrydate)); ?></small>
                        </div> &nbsp;
                        <p>
							<?= $cmntRow->commentText; ?>
                        </p>
                    </div>
                </li>


                <?php
            }
        }
    }
    ?>


</ul>