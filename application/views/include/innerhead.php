<?php
error_reporting(0);
?>
<head>
<meta charset="utf-8">
<title><?php echo $title; ?> | C-Link </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">  
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link id="bs-css" href="<?= FRONTASSETS; ?>css/bootstrap-cerulean.min.css" rel="stylesheet">
<link href="<?= FRONTASSETS; ?>css/charisma-app.css" rel="stylesheet">
<!--<link href='<?= FRONTASSETS; ?>bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
<link href='<?= FRONTASSETS; ?>bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>-->
<link href='<?= FRONTASSETS; ?>bower_components/chosen/chosen.min.css' rel='stylesheet'>
<link href='<?= FRONTASSETS; ?>bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
<link href='<?= FRONTASSETS; ?>bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
<link href='<?= FRONTASSETS; ?>bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>

<!-- jQuery -->
<script src="<?= FRONTASSETS; ?>bower_components/jquery/jquery.min.js"></script>
<link rel="shortcut icon" href="<?= FRONTASSETS; ?>img/favicon.ico">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">
</head>