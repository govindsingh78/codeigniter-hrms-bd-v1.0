<head>
        <meta charset="utf-8">
        <title>C-Link</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
        <meta name="author" content="">
        <!-- The styles -->
        <link id="bs-css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link id="bs-css" href="<?= FRONTASSETS; ?>css/bootstrap-cerulean.min.css" rel="stylesheet">
        <link href="<?= FRONTASSETS; ?>css/charisma-app.css" rel="stylesheet">
        <link href='<?= FRONTASSETS; ?>bower_components/chosen/chosen.min.css' rel='stylesheet'>
        <link href='<?= FRONTASSETS; ?>bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
        <link href='<?= FRONTASSETS; ?>bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
        <link href='<?= FRONTASSETS; ?>css/jquery.noty.css' rel='stylesheet'>
        <link href='<?= FRONTASSETS; ?>css/noty_theme_default.css' rel='stylesheet'>
        <link href='<?= FRONTASSETS; ?>css/elfinder.min.css' rel='stylesheet'>
        <link href='<?= FRONTASSETS; ?>css/elfinder.theme.css' rel='stylesheet'>
        <link href='<?= FRONTASSETS; ?>css/jquery.iphone.toggle.css' rel='stylesheet'>
        <script src="<?= FRONTASSETS; ?>bower_components/jquery/jquery.min.js"></script>
        <!-- The fav icon -->
         <link rel="shortcut icon" href="http://hrms.cegindia.com/public/favicon.ico">
    </head>

    