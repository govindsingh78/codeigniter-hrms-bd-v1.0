<?php
$user_id = $this->session->userdata('uid');
//echo $user_id;
$uRil = end($this->uri->segment_array());
?>
<nav id="sidebar">

    <ul class="list-unstyled components">
        <!-- LIST 1 -->
        <li class="<?= ($uRil == "hrms") ? "active" : ""; ?>">
            <a href="http://172.16.1.30:8087/hrms"  aria-expanded="false" class="<?= ($uRil == "hrms") ? "active" : "dropdown-toggle"; ?>" target="_blank">
                <i class="glyphicon glyphicon-user"></i>HRMS</a>
        </li>
        <li class="<?= (($uRil == 'financial_yearwisereport') or ( $uRil == 'newproject') or ( $uRil == 'activeproject') or ( $uRil == 'importantproject') or ( $uRil == 'reviewproject') or ( $uRil == "dashboard") or ( $uRil == "teamrequisition") or ( $uRil == "cru")) ? "active" : ""; ?>">
            <a href="#homeSubmenu1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="glyphicon glyphicon-home"></i>
                Business Development
            </a>

            <ul class="<?= (($uRil == 'financial_yearwisereport') or ( $uRil == "dashboard") or ( $uRil == "tender") or ( $uRil == "tndrinfocsv") or ( $uRil == "tndrtwentyfourcsv") or ( $uRil == 'newproject') or ( $uRil == 'activeproject') or ( $uRil == 'importantproject') or ( $uRil == 'reviewproject') or ( $uRil == 'ongoingfqproject') or ( $uRil == 'ongoingrfpproject') or ( $uRil == 'ongoingeoiproject') or ( $uRil == 'togoproject') or ( $uRil == 'submittedeoiproject') or ( $uRil == 'submittedrfpproject') or ( $uRil == 'submittedfqproject') or ( $uRil == 'bidproject') or ( $uRil == "teamrequisition") or ( $uRil == "cru") or ( $uRil == 'inactiveproject') or ( $uRil == 'notimportantproject') or ( $uRil == 'nogoproject') or ( $uRil == 'nosubmitproject') or ( $uRil == 'Searchprojectcru') or ( $uRil == 'educationalreport') or ( $uRil == 'projectreport') or ( $uRil == 'crureport') or ( $uRil == 'cegexp') or ( $uRil == 'bidnewreport') or ( $uRil == 'financialreport') or ( $uRil == 'technicalreport') or ( $uRil == 'marksreport') or ( $uRil == 'pdfsortdata') or ( $uRil == 'cegeoiproject') or ( $uRil == 'cegrfpproject') or ( $uRil == 'cegfqproject') or ( $uRil == 'completeproject')) ? "list-unstyled group-list" : "collapse list-unstyled group-list"; ?>">
                <li style="margin-left:40px;" aria-expanded="false" class="<?= (($uRil == 'dashboard')) ? 'active' : 'dropdown-toggle'; ?>">
                    <a href="<?= base_url('dashboard'); ?>"><i class="glyphicon glyphicon-share"></i> Dashboard </a>
                </li>
                <li style="margin-left:40px;" aria-expanded="false" class="<?= (($uRil == 'tender')) ? 'active' : 'dropdown-toggle'; ?>">
                    <a href="#" data-toggle="collapse" aria-expanded="false"><i class="glyphicon glyphicon-plus"></i>Add</a>
                    <ul class="<?= (($uRil == 'tender') or ( $uRil == 'tndrinfocsv') or ( $uRil == 'tndrtwentyfourcsv')) ? 'list-unstyled sub-group-list' : 'collapse list-unstyled sub-group-list'; ?>">
                        <li class="<?= (($uRil == 'tender')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('tender'); ?>"><i class="glyphicon glyphicon-home"></i>New Tender</a></li>
                        <li class="<?= (($uRil == 'tndrinfocsv')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('tender/tndrinfocsv'); ?>"><i class="glyphicon glyphicon-home"></i>CSV Tender Info </a></li>
                        <li class="<?= (($uRil == 'tndrtwentyfourcsv')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('tender/tndrtwentyfourcsv'); ?>"><i class="glyphicon glyphicon-home"></i>CSV Tender24 </a></li>
                    </ul>
                </li>
                <li style="margin-left:40px;" aria-expanded="false" class="<?= (($uRil == 'newproject') or ( $uRil == 'activeproject') or ( $uRil == 'importantproject') or ( $uRil == 'reviewproject')) ? 'active' : 'dropdown-toggle'; ?>">
                    <a href="#" data-toggle="collapse" aria-expanded="false"><i class="glyphicon glyphicon-home"></i>Tender Search</a>
                    <ul class="<?= (($uRil == 'newproject') or ( $uRil == 'activeproject') or ( $uRil == 'importantproject') or ( $uRil == 'reviewproject')) ? 'list-unstyled sub-group-list' : 'collapse list-unstyled sub-group-list'; ?>">
                        <li class="<?= (($uRil == 'newproject')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('newproject'); ?>"><i class="glyphicon glyphicon-eye-open"></i>New Project</a></li>
                        <li class="<?= (($uRil == 'activeproject')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('activeproject'); ?>"><i class="glyphicon glyphicon-ok"></i>Active Project </a></li>
                        <li class="<?= (($uRil == 'importantproject')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('importantproject'); ?>"><i class="glyphicon glyphicon-star"></i>Important Project</a></li>
                        <li class="<?= (($uRil == 'reviewproject')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('reviewproject'); ?>"><i class="glyphicon glyphicon-sunglasses"></i>In Review Project</a></li>
                    </ul>
                </li>

                <li style="margin-left:40px;" aria-expanded="false" class="<?= (($uRil == 'ongoingfqproject') or ( $uRil == 'ongoingrfpproject') or ( $uRil == 'ongoingeoiproject') or ( $uRil == 'togoproject')) ? 'active' : 'dropdown-toggle'; ?>">
                    <a href="#" data-toggle="collapse" aria-expanded="false"><i class="glyphicon glyphicon-level-up"></i>Ongoing Biding</a>
                    <ul class="<?= (($uRil == 'ongoingfqproject') or ( $uRil == 'ongoingrfpproject') or ( $uRil == 'ongoingeoiproject') or ( $uRil == 'togoproject')) ? 'list-unstyled sub-group-list' : 'collapse list-unstyled sub-group-list'; ?>">
                        <li class="<?= (($uRil == 'ongoingeoiproject')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('togoproject/ongoingeoiproject'); ?>"><i class="glyphicon glyphicon-eye-open"></i> EOI </a></li>
                        <li class="<?= (($uRil == 'ongoingrfpproject')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('togoproject/ongoingrfpproject'); ?>"><i class="glyphicon glyphicon-ok"></i> RFP </a></li>
                        <li class="<?= (($uRil == 'ongoingfqproject')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('togoproject/ongoingfqproject'); ?>"><i class="glyphicon glyphicon-star"></i> FQ </a></li>
                        <li class="<?= (($uRil == 'togoproject')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('togoproject'); ?>"><i class="glyphicon glyphicon-sunglasses"></i> All </a></li>
                    </ul>
                </li>
                <li style="margin-left:40px;" aria-expanded="false" class="<?= (($uRil == 'submittedeoiproject') or ( $uRil == 'submittedrfpproject') or ( $uRil == 'submittedfqproject') or ( $uRil == 'bidproject')) ? 'active' : 'dropdown-toggle'; ?>">
                    <a href="#" data-toggle="collapse" aria-expanded="false"><i class="glyphicon glyphicon-level-up"></i>Submitted</a>

                    <ul class="<?= (($uRil == 'submittedeoiproject') or ( $uRil == 'submittedrfpproject') or ( $uRil == 'submittedfqproject') or ( $uRil == 'bidproject')) ? 'list-unstyled sub-group-list' : 'collapse list-unstyled sub-group-list'; ?>">
                        <li class="<?= (($uRil == 'submittedeoiproject')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('bidproject/submittedeoiproject'); ?>"><i class="glyphicon glyphicon-eye-open"></i> EOI </a></li>
                        <li class="<?= (($uRil == 'submittedrfpproject')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('bidproject/submittedrfpproject'); ?>"><i class="glyphicon glyphicon-ok"></i> RFP </a></li>
                        <li class="<?= (($uRil == 'submittedfqproject')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('bidproject/submittedfqproject'); ?>"><i class="glyphicon glyphicon-star"></i> FQ </a></li>
                        <li class="<?= (($uRil == 'bidproject')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('bidproject'); ?>"><i class="glyphicon glyphicon-sunglasses"></i> All </a></li>
                    </ul>
                </li>


                <li style="margin-left:40px;" aria-expanded="false" class="<?= (($uRil == 'teamrequisition')) ? 'active' : 'dropdown-toggle'; ?>">
                    <a href="<?= base_url('teamrequisition'); ?>"><i class="glyphicon glyphicon-share"></i> Team Requisition </a>
                </li>

                <li style="margin-left:40px;" aria-expanded="false" class="<?= (($uRil == 'cru')) ? 'active' : 'dropdown-toggle'; ?>">
                    <a href="<?= base_url('cru'); ?>"><i class="glyphicon glyphicon-share"></i> CRU Team </a>
                </li>

                <li style="margin-left:40px;" class="<?= (($uRil == 'financial_yearwisereport') or ( $uRil == 'financial_yearwisereport') or ( $uRil == 'Searchprojectcru') or ( $uRil == 'educationalreport') or ( $uRil == 'projectreport') or ( $uRil == 'crureport') or ( $uRil == 'cegexp')) ? 'active' : 'dropdown-toggle'; ?>" >
                    <a href="#" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-list"></i>
                        Reports
                    </a>


                    <ul class="<?= ( ($uRil == 'financial_yearwisereport') or ( $uRil == 'Searchprojectcru') or ( $uRil == 'educationalreport') or ( $uRil == 'projectreport') or ( $uRil == 'crureport') or ( $uRil == 'cegexp') or ( $uRil == 'bidnewreport') or ( $uRil == 'financialreport') or ( $uRil == 'technicalreport') or ( $uRil == 'marksreport') or ( $uRil == 'pdfsortdata') or ( $uRil == 'cegeoiproject') or ( $uRil == 'cegrfpproject') or ( $uRil == 'cegfqproject') or ( $uRil == 'completeproject')) ? "list-unstyled sub-group-list" : "collapse list-unstyled sub-group-list"; ?>">
                        <li style="margin-left:20px;" class="<?= (($uRil == 'financial_yearwisereport')) ? 'active' : ''; ?>"><a href="<?= base_url('report/financial_yearwisereport'); ?>"><i class="glyphicon glyphicon-star"></i>   Order Booking   </a></li>
                        <li style="margin-left:20px;" aria-expanded="false" class="<?= (($uRil == 'Searchprojectcru') or ( $uRil == 'educationalreport') or ( $uRil == 'projectreport') or ( $uRil == 'crureport')) ? 'active' : 'dropdown-toggle'; ?>">
                            <a href="#" data-toggle="collapse" aria-expanded="false"><i class="glyphicon glyphicon-eye-open"></i> Search </a>
                            <ul class="<?= (($uRil == 'Searchprojectcru') or ( $uRil == 'educationalreport') or ( $uRil == 'projectreport') or ( $uRil == 'crureport')) ? 'list-unstyled sub-group-list' : 'collapse list-unstyled sub-group-list'; ?>">
                                <li style="margin-left:20px;" class="<?= (($uRil == 'Searchprojectcru')) ? 'active' : ''; ?>"><a href="<?= base_url('Searchprojectcru'); ?>"><i class="glyphicon glyphicon-eye-open"></i> By Project  </a></li>
                                <li style="margin-left:20px;" class="<?= (($uRil == 'educationalreport')) ? 'active' : ''; ?>"><a href="<?= base_url('projectplanning/educationalreport'); ?>"><i class="glyphicon glyphicon-ok"></i> Educational Report  </a></li>
                                <li style="margin-left:20px;" class="<?= (($uRil == 'projectreport')) ? 'active' : ''; ?>"><a href="<?= base_url('projectplanning/projectreport'); ?>"><i class="glyphicon glyphicon-star"></i> By MM  </a></li>
                                <li style="margin-left:20px;" class="<?= (($uRil == 'crureport')) ? 'active' : ''; ?>"><a href="<?= base_url('report/crureport'); ?>"><i class="glyphicon glyphicon-sunglasses"></i> Position Count  </a></li>
                            </ul>
                        </li>

                        <li style="margin-left:20px;" class="<?= (($uRil == 'bidnewreport') or ( $uRil == 'financialreport') or ( $uRil == 'technicalreport') or ( $uRil == 'marksreport') or ( $uRil == 'pdfsortdata')) ? 'active' : 'dropdown-toggle'; ?>">
                            <a href="#" data-toggle="collapse" aria-expanded="false"><i class="glyphicon glyphicon-ok"></i> 
                                Reports 
                            </a>
                            <ul  class="<?= (($uRil == 'bidnewreport') or ( $uRil == 'financialreport') or ( $uRil == 'technicalreport') or ( $uRil == 'marksreport') or ( $uRil == 'pdfsortdata')) ? 'list-unstyled sub-group-list' : 'collapse list-unstyled sub-group-list'; ?>">
                                <li style="margin-left:20px;" class="<?= (($uRil == 'bidnewreport')) ? 'active' : ''; ?>"><a href="<?= base_url('bidproject/bidnewreport'); ?>"><i class="glyphicon glyphicon-eye-open"></i>  Submitted Report  </a></li>
                                <li style="margin-left:20px;" class="<?= (($uRil == 'financialreport')) ? 'active' : ''; ?>"><a href="<?= base_url('report/financialreport'); ?>"><i class="glyphicon glyphicon-ok"></i>  Financial Marks  </a></li>
                                <li style="margin-left:20px;" class="<?= (($uRil == 'technicalreport')) ? 'active' : ''; ?>"><a href="<?= base_url('report/technicalreport'); ?>"><i class="glyphicon glyphicon-star"></i>  Technical Marks  </a></li>
                                <li style="margin-left:20px;" class="<?= (($uRil == 'marksreport')) ? 'active' : ''; ?>"><a href="<?= base_url('report/marksreport'); ?>"><i class="glyphicon glyphicon-sunglasses"></i>  Total Marks  </a></li>
                                <li style="margin-left:20px;" class="<?= (($uRil == 'pdfsortdata')) ? 'active' : ''; ?>"><a href="<?= base_url('pdfsort/pdfsortdata'); ?>"><i class="glyphicon glyphicon-ok"></i>   PDS   </a></li>
                            </ul>
                        </li>

                        <li style="margin-left:20px;" aria-expanded="false" class="<?= (($uRil == 'cegeoiproject') or ( $uRil == 'cegrfpproject') or ( $uRil == 'cegfqproject') or ( $uRil == 'completeproject')) ? 'active' : 'dropdown-toggle'; ?>">
                            <a href="#" data-toggle="collapse" aria-expanded="false"><i class="glyphicon glyphicon-star"></i> MIS </a>
                            <ul class="<?= (($uRil == 'cegeoiproject') or ( $uRil == 'cegrfpproject') or ( $uRil == 'cegfqproject') or ( $uRil == 'completeproject')) ? 'list-unstyled sub-group-list' : 'collapse list-unstyled sub-group-list'; ?>">

                                <li style="margin-left:20px;" class="<?= (($uRil == 'cegeoiproject')) ? 'active' : ''; ?>"><a href="<?= base_url('completeproject/cegeoiproject'); ?>"><i class="glyphicon glyphicon-eye-open"></i> EOI </a></li>
                                <li style="margin-left:20px;" class="<?= (($uRil == 'cegrfpproject')) ? 'active' : ''; ?>" ><a href="<?= base_url('completeproject/cegrfpproject'); ?>"><i class="glyphicon glyphicon-ok"></i> RFP </a></li>
                                <li style="margin-left:20px;" class="<?= (($uRil == 'cegfqproject')) ? 'active' : ''; ?>"><a href="<?= base_url('completeproject/cegfqproject'); ?>"><i class="glyphicon glyphicon-star"></i> FQ </a></li>
                                <li style="margin-left:20px;" class="<?= (($uRil == 'completeproject')) ? 'active' : ''; ?>"><a href="<?= base_url('completeproject'); ?>"><i class="glyphicon glyphicon-sunglasses"></i> All </a></li>
                            </ul>
                        </li>
                        <li style="margin-left:20px;" class="<?= (($uRil == 'cegexp')) ? 'active' : ''; ?>"><a href="<?= base_url('company/cegexp'); ?>">
                                <i class="glyphicon glyphicon-sunglasses"></i> 
                                CEG Exp.
                            </a>
                        </li>
                    </ul>
                </li>


                <li style="margin-left:40px;" class="<?= (($uRil == 'inactiveproject') or ( $uRil == 'notimportantproject') or ( $uRil == 'nogoproject') or ( $uRil == 'nosubmitproject')) ? 'active' : 'dropdown-toggle'; ?>">
                    <a href="#" data-toggle="collapse" aria-expanded="false"><i class="glyphicon glyphicon-trash"></i>Trash</a>
                    <ul class="<?= (($uRil == 'inactiveproject') or ( $uRil == 'notimportantproject') or ( $uRil == 'nogoproject') or ( $uRil == 'nosubmitproject')) ? 'list-unstyled sub-group-list' : 'collapse list-unstyled sub-group-list'; ?>">
                        <li class="<?= (($uRil == 'inactiveproject')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('inactiveproject'); ?>"><i class="glyphicon glyphicon-eye-open"></i> InActive Project  </a></li>
                        <li class="<?= (($uRil == 'notimportantproject')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('notimportantproject'); ?>"><i class="glyphicon glyphicon-ok"></i> Not Important Project  </a></li>
                        <li class="<?= (($uRil == 'nogoproject')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('nogoproject'); ?>"><i class="glyphicon glyphicon-star"></i> No Go Project  </a></li>
                        <li class="<?= (($uRil == 'nosubmitproject')) ? 'active' : ''; ?>" style="margin-left:20px;"><a href="<?= base_url('dashboard/nosubmitproject'); ?>"><i class="glyphicon glyphicon-sunglasses"></i> Not Submitted  </a></li>
                    </ul>
                </li>
            </ul>
        </li>




        <li class="<?= (($uRil == "accountsummaryreport") or ( $uRil == "acdashboard") or ( $uRil == "viewaccountinfo") or ( $uRil == "itmt_report")) ? "active" : ""; ?>">
            <a href="#homeSubmenu1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="glyphicon glyphicon-book"></i>
                Accounts
            </a>
            <ul class="<?= (($uRil == "accountsummaryreport") or ( $uRil == "acdashboard") or ( $uRil == "viewaccountinfo") or ( $uRil == "bg_report")) ? "list-unstyled group-list" : "collapse list-unstyled group-list"; ?>">
                <li class="<?= ($uRil == "acdashboard") ? "active" : ""; ?>" style="margin-left:40px;"><a href="<?= base_url('dashboard/acdashboard'); ?>"><i class="glyphicon glyphicon-ok"></i> Dashboard  </a></li>
                <li class="<?= ($uRil == "accountsummaryreport") ? "active" : ""; ?>" style="margin-left:40px;"><a href="<?= base_url('accountsummaryreport'); ?>"><i class="glyphicon glyphicon-eye-open"></i> Account Summary  </a></li>
                <li class="<?= ($uRil == "viewaccountinfo") ? "active" : ""; ?>" style="margin-left:40px;"><a href="<?= base_url('account/viewaccountinfo'); ?>"><i class="glyphicon glyphicon-star"></i> Generate Invoice  </a></li>
                <li class="<?= ($uRil == "bg_report") ? "active" : ""; ?>" style="margin-left:40px;"><a href="<?= base_url('bgreport/bg_report'); ?>"><i class="glyphicon glyphicon-sunglasses"></i> BG Report </a></li>

                           <!--<li class="<? //= ($uRil == "itmt_report") ? "active" : "";    ?>" style="margin-left:40px;"><a href="<? //= base_url('projectplanning/itmt_report');    ?>"><i class="glyphicon glyphicon-sunglasses"></i> Intermittent Report  </a></li>-->
            </ul>
        </li>


        <li class="<?= ($uRil == "project_vacant") ? "active" : ""; ?>">
            <a href="<?= base_url('crutraker/project_vacant'); ?>"  aria-expanded="false" class="<?= ($uRil == "project_vacant") ? "active" : "dropdown-toggle"; ?>">
                <i class="glyphicon glyphicon-search"></i>
                Cv Tracker
            </a>
        </li>


        <li class="<?= (($uRil == "add") or ( $uRil == 'showlist')) ? 'active' : ''; ?>">
            <a href="#" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="glyphicon glyphicon-envelope"></i>
                Company Contact
            </a>
            <ul class="<?= (($uRil == "add") or ( $uRil == 'showlist')) ? "list-unstyled group-list" : "collapse list-unstyled group-list"; ?>">
                <li class="<?= (($uRil == 'add')) ? 'active' : ''; ?>" style="margin-left:40px;"><a href="<?= base_url('company/add'); ?>"><i class="glyphicon glyphicon-plus"></i> New Company </a></li>
                <li class="<?= (($uRil == 'showlist')) ? 'active' : ''; ?>" style="margin-left:40px;"><a href="<?= base_url('company/showlist'); ?>"><i class="glyphicon glyphicon-envelope"></i>Company list </a></li>
            </ul>
        </li>


        <li class="<?= ($uRil == "websites") ? "active" : ""; ?>">
            <a href="<?= base_url('company/websites'); ?>"  aria-expanded="false" class="<?= ($uRil == "websites") ? "active" : "dropdown-toggle"; ?>">
                <i class="glyphicon glyphicon-search"></i>Websites</a>
        </li>
        <?php
        if ($user_id == '296' or $user_id == '1886' or $user_id == '414') {
            ?>
            <li class="<?= (($uRil == 'tender_add_manaul')) ? 'active' : ''; ?>">
                <a href="#homeSubmenu1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="glyphicon glyphicon-file"></i>
                    Tender manual
                </a>
                <ul class="<?= (($uRil == "tender_add_manaul")) ? "list-unstyled group-list" : "collapse list-unstyled group-list"; ?>">
                    <li class="<?= (($uRil == 'tender_add_manaul')) ? 'active' : ''; ?>" style="margin-left:40px;"><a href="<?= base_url('tender/tender_add_manaul'); ?>"><i class="glyphicon glyphicon-plus"></i>Add Tender</a></li>
                </ul>
            </li>  
        <?php } ?>   

        <?php
        if ($user_id == '1615' or $user_id == '629') {
            ?>
            <li class="<?= (($uRil == 'viewaccountinfo') or ( $uRil == 'viewattdence')) ? 'active' : ''; ?>">
                <a href="#homeSubmenu1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="glyphicon glyphicon-trash"></i>
                    Other
                </a>
                <ul class="<?= (($uRil == "viewaccountinfo") or ( $uRil == "viewattdence")) ? "list-unstyled group-list" : "collapse list-unstyled group-list"; ?>">
                    <li class="<?= (($uRil == 'viewaccountinfo')) ? 'active' : ''; ?>" style="margin-left:40px;"><a href="<?= base_url('account/viewaccountinfo'); ?>"><i class="glyphicon glyphicon-star"></i> Account  </a></li>
                    <li class="<?= (($uRil == 'viewattdence')) ? 'active' : ''; ?>" style="margin-left:40px;"><a href="<?= base_url('account/viewattdence'); ?>"><i class="glyphicon glyphicon-ok"></i> Siteoffice Attendance   </a></li>
                </ul>
            </li>

            <li class="<?= (($uRil == 'designcateg_assignonproj') or ( $uRil == 'design_ondesigncategproj') or ( $uRil == "reimbursable_assign")) ? 'active' : ''; ?>">
                <a href="#homeSubmenu1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="glyphicon glyphicon-file"></i>
                    Invoice Config
                </a>
                <ul class="<?= (($uRil == "designcateg_assignonproj") or ( $uRil == "design_ondesigncategproj") or ( $uRil == "reimbursable_assign")) ? "list-unstyled group-list" : "collapse list-unstyled group-list"; ?>">
                    <li class="<?= (($uRil == 'designcateg_assignonproj')) ? 'active' : ''; ?>" style="margin-left:40px;"><a href="<?= base_url('accountdept/designcateg_assignonproj'); ?>"><i class="glyphicon glyphicon-star"></i>Designation Category </a></li>
                    <li class="<?= (($uRil == 'design_ondesigncategproj')) ? 'active' : ''; ?>" style="margin-left:40px;"><a href="<?= base_url('accountdept/design_ondesigncategproj'); ?>"><i class="glyphicon glyphicon-ok"></i>Add Designation </a></li>
                    <li class="<?= (($uRil == 'reimbursable_assign')) ? 'active' : ''; ?>" style="margin-left:40px;"><a href="<?= base_url('accountdept/reimbursable_assign'); ?>"><i class="glyphicon glyphicon-star"></i>Assign Reimbursable </a></li>
                    <li class="<?= (($uRil == 'accdept_attendance')) ? 'active' : ''; ?>" style="margin-left:40px;"><a href="<?= base_url('accountdept/accdept_attendance'); ?>"><i class="glyphicon glyphicon-calendar"></i> Attendance </a></li>
                </ul>
            </li>

        <?php } ?>

        <?php
        $user_login_id = $this->session->userdata('uid');
        $proj_coor = array('271', '272');
        if (in_array($user_login_id, $proj_coor)) {
        ?>
            <li>
                <a href="#homeSubmenu1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="glyphicon glyphicon-file"></i>
                    Project Coordinator 
                </a>
                <ul class="<?= (( $uRil == "doctrol_project_coord")) ? "list-unstyled group-list" : "collapse list-unstyled group-list"; ?>">
                    <li class="<?= (($uRil == 'doctrol_project_coord')) ? 'active' : ''; ?>" style="margin-left:40px;"><a href="<?= base_url('doctrol/doctrol_project_coord'); ?>"><i class="glyphicon glyphicon-star"></i>Dashboard</a></li>
                </ul>
            </li> 
        <?php } ?>

        <?php
        $proj_mgmt = array('296', '190','1615');

        if (in_array($user_login_id, $proj_mgmt)) {
            ?>
            <li>
                <a href="#homeSubmenu1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="glyphicon glyphicon-file"></i>
                    Project Management
                </a>
                <ul class="<?= (( $uRil == "doctrol_project_mgmt")) ? "list-unstyled group-list" : "collapse list-unstyled group-list"; ?>">
                    <li class="<?= (($uRil == 'doctrol_project_mgmt')) ? 'active' : ''; ?>" style="margin-left:40px;"><a href="<?= base_url('doctrolcontroller/doctrol_project_mgmt'); ?>"><i class="glyphicon glyphicon-star"></i>Dashboard</a></li>
                </ul>
            </li>
<?php } ?>
    </ul> 

</nav>
<style>

    a, a:hover, a:focus {
        color: inherit;
        text-decoration: none;
        transition: all 0.3s;
    }
    .line {
        width: 100%;
        height: 1px;
        border-bottom: 1px dashed #ddd;
        margin: 40px 0;
    }

    i, span {
        display: inline-block;
    }

    /* ---------------------------------------------------
        SIDEBAR STYLE
    ----------------------------------------------------- */
    .wrapper {
        display: flex;
        align-items: stretch;
    }

    #sidebar {
        min-width: 250px;
        max-width: 250px;
        background: #7386D5;
        color: #fff;
        transition: all 0.3s;
    }

    #sidebar.active {
        min-width: 80px;
        max-width: 80px;
        text-align: center;
    }

    #sidebar.active .sidebar-header h3, #sidebar.active .CTAs {
        display: none;
    }

    #sidebar.active .sidebar-header strong {
        display: block;
    }

    #sidebar ul li a {
        text-align: left;
    }

    #sidebar.active ul li a {
        padding: 20px 10px;
        text-align: center;
        font-size: 0.85em;
    }

    #sidebar.active ul li a i {
        margin-right:  0;
        display: block;
        font-size: 1.8em;
        margin-bottom: 5px;
    }

    #sidebar.active ul ul a {
        padding: 10px !important;
    }

    #sidebar.active a[aria-expanded="false"]::before, #sidebar.active a[aria-expanded="true"]::before {
        top: auto;
        bottom: 5px;
        right: 50%;
        -webkit-transform: translateX(50%);
        -ms-transform: translateX(50%);
        transform: translateX(50%);
    }

    #sidebar .sidebar-header {
        padding: 20px;
        background: #6d7fcc;
    }

    #sidebar .sidebar-header strong {
        display: none;
        font-size: 1.8em;
    }

    #sidebar ul.components {
        padding: 20px 0;
        border-bottom: 1px solid #47748b;
    }

    #sidebar ul li a {
        padding: 10px;
        font-size: 1.1em;
        display: block;
    }
    #sidebar ul li a:hover {
        color: #7386D5;
        background: #fff;
    }
    #sidebar ul li a i {
        margin-right: 10px;
    }

    #sidebar ul li.active > a, a[aria-expanded="true"] {
        color: #6d7fcc;
        background: #fff;
    }


    a[data-toggle="collapse"] {
        position: relative;
    }

    a[aria-expanded="false"]::before, a[aria-expanded="true"]::before {
        content: '\e259';
        display: block;
        position: absolute;
        right: 20px;
        font-family: 'Glyphicons Halflings';
        font-size: 0.6em;
    }
    a[aria-expanded="true"]::before {
        content: '\e260';
    }


    /* ul ul a {
        font-size: 0.9em !important;
        padding-left: 30px !important;
        background: #6d7fcc;
    } */

    ul.CTAs {
        padding: 20px;
    }

    ul.CTAs a {
        text-align: center;
        font-size: 0.9em !important;
        display: block;
        border-radius: 5px;
        margin-bottom: 5px;
    }

    a.download {
        background: #fff;
        color: #7386D5;
    }

    a.article, a.article:hover {
        background: #6d7fcc !important;
        color: #fff !important;
    }



    /* ---------------------------------------------------
        CONTENT STYLE
    ----------------------------------------------------- */
    #content {
        padding: 20px;
        min-height: 100vh;
        transition: all 0.3s;
        width:100%;
    }


    /* ---------------------------------------------------
        MEDIAQUERIES
    ----------------------------------------------------- */
    @media (max-width: 768px) {
        #sidebar {
            min-width: 80px;
            max-width: 80px;
            text-align: center;
            margin-left: -80px !important ;
        }
        a[aria-expanded="false"]::before, a[aria-expanded="true"]::before {
            top: auto;
            bottom: 5px;
            right: 50%;
            -webkit-transform: translateX(50%);
            -ms-transform: translateX(50%);
            transform: translateX(50%);
        }
        #sidebar.active {
            margin-left: 0 !important;
        }

        #sidebar .sidebar-header h3, #sidebar .CTAs {
            display: none;
        }

        #sidebar .sidebar-header strong {
            display: block;
        }

        #sidebar ul li a {
            padding: 20px 10px;
        }

        #sidebar ul li a span {
            font-size: 0.85em;
        }
        #sidebar ul li a i {
            margin-right:  0;
            display: block;
        }

        #sidebar ul ul a {
            padding: 10px !important;
        }

        #sidebar ul li a i {
            font-size: 1.3em;
        }
        #sidebar {
            margin-left: 0;
        }
        #sidebarCollapse span {
            display: none;
        }
    }

</style>

<script>
    $(function () {
        $('.components li a').on('click', function () {
            //e.preventDefault();
            var $this = $(this);
            var mainList = $this.closest('ul');
            var subLists = mainList.children('li').children('ul');

            mainList.children('li').removeClass('active');
            subLists.addClass('collapse');

            $this.parent().addClass('active');
            var selectedSublist = $this.closest('li').children('ul')
            if (selectedSublist.hasClass('collapse'))
                selectedSublist.removeClass('collapse');
            else
                selectedSublist.addClass('collapse');
        });
    });
</script>