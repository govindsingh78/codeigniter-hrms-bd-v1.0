
<div class="navbar navbar-default" role="navigation">
    <div class="navbar-inner">
        <button type="button" class="navbar-toggle pull-left animated flip">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?= base_url('dashboard'); ?>"> 
            <img alt="Project Planning" src="<?= FRONTASSETS; ?>img1/logo20.png" class="hidden-xs"/>
<!--            <span>CEG</span>-->
        
        </a>

        <!-- user dropdown starts -->
        <div class="btn-group pull-right">
            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> <?php echo loginUserName(); ?> </span>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#">Profile</a></li>
                <li class="divider"></li>
                <li><a href="<?= base_url('welcome/logout'); ?>">Logout</a></li>
            </ul>
        </div>
        <!-- user dropdown ends -->

        <style>

            #nav{list-style:none;margin: 0px;
                 padding: 0px;}
            #nav li {
                float: left;
                margin-right: 20px;
                font-size: 14px;
                font-weight:bold;
            }
            #nav li a{color:#333333;text-decoration:none}
            #nav li a:hover{color:#006699;text-decoration:none}
            #notification_li{position:relative}
            #notificationContainer {
                background-color: #fff;
                border: 1px solid rgba(100, 100, 100, .4);
                -webkit-box-shadow: 0 3px 8px rgba(0, 0, 0, .25);
                overflow: visible;
                position: absolute;
                top: 45px;
                margin-left: -170px;
                width: 400px;
                z-index: 1;
                display: none;
            }
            #notificationContainer:before {
                content: '';
                display: block;
                position: absolute;
                width: 0;
                height: 0;
                color: transparent;
                border: 10px solid black;
                border-color: transparent transparent white;
                margin-top: -20px;
                margin-left: 188px;
            }



            #notification_count {
                padding: 2px 2px 2px 2px;
                background: #cc0000;
                color: #ffffff;
                font-weight: bold;
                margin-left: 25px;
                /* border-radius: 31px; */
                position: absolute;
                margin-top: 13px;
                font-size: 11px;
                z-index: 10001;
                line-height: 7px;
            }
        </style>

<!--
        <ul class="collapse navbar-collapse nav navbar-nav top-menu" style="margin-left:65px;">
            <li id="tenderdetailsmenu" >
                <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i> Tender <span
                        class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="<?= base_url('tender') ?>">Add New </a></li>
                    <li><a href="<?= base_url('tender/tndrinfocsv') ?>">Upload CSV Tender Info </a></li>
                    <li><a href="<?= base_url('tender/tndrtwentyfourcsv') ?>">Upload CSV Tender24 </a></li>
                    <li><a target="_blank" href="<?= HOSTNAME."expdate.php"; ?>">Project Filter </a></li>
                </ul>
            </li>



            <div class="dropdown" id="notification_li">
                <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="#" >
                    <span id="notification_count"><?= CountAllNotification(); ?></span>
                    <i style="font-size:20px;" class="glyphicon glyphicon-globe"></i>
                </a>

                <ul class="dropdown-menu notifications" role="menu" aria-labelledby="dLabel">
                    <div class="notification-heading"><h4 class="menu-title">Notifications</h4><h4 class="menu-title pull-right">View all<i class="glyphicon glyphicon-circle-arrow-right"></i></h4>
                    </div>
                    <li class="divider"></li>
                    <div class="notifications-wrapper">

                        <?php
                        $notifArr = GetAllNotification();
                        foreach ($notifArr as $rowNotif) {
                            ?>
                            <a class="content" href="#"> 
                                <div class="notification-item"> 
                                    <p title="<?= $rowNotif['tendername']; ?>" class="item-info"><?= $rowNotif['notification_data']; ?></p>
                                    <h4 class="item-title">By :-  <?= $rowNotif['eventby']; ?> , 
                                        <i class="glyphicon glyphicon-time"></i>
                                        <?= date("d-m-Y H:i:s", strtotime($rowNotif['entrydate'])); ?></h4>
                                </div>   
                            </a>
                        <?php } ?>

                    </div>

                    <li class="divider"></li>
                    <div class="notification-footer"><h4 class="menu-title">View all<i class="glyphicon glyphicon-circle-arrow-right"></i></h4></div>
                </ul>
            </div>

        </ul>-->

    </div>
</div>


<style type="text/css">
    /* CSS used here will be applied after bootstrap.css */

    .dropdown {
        display:inline-block;
        margin-left:20px;
        padding:10px;
    }
    .glyphicon-bell {

        font-size:1.5rem;
    }
    .notifications {
        min-width:420px; 
    }
    .notifications-wrapper {
        overflow:auto;
        max-height:250px;
    }
    .menu-title {
        color:#ff7788;
        font-size:1.5rem;
        display:inline-block;
    }
    .glyphicon-circle-arrow-right {
        margin-left:10px;     
    }
    .notification-heading, .notification-footer  {
        padding:2px 10px;
    }
    .dropdown-menu.divider {
        margin:5px 0;          
    }
    .item-title {
        font-size:1.3rem;
        color:#000;
    }
    .notifications a.content {
        text-decoration:none;
        background:#ccc;
    }
    .notification-item {
        padding:10px;
        margin:5px;
        background:rgba(109, 109, 109, 0.05);
        border-radius:4px;
    }
    a#dLabel {
        color: white;
    }
</style>
<style>
body{background-color:#ccd6da;font-family:Arial, Helvetica, sans-serif;padding:0;margin:0;font-size:12px;color:#000;}
.clear{clear:both;}
a{color:#0291d4; text-decoration:none;}
a#closebt{ position:absolute; top:130px; left:208px; outline:none; z-index:999;}
a#openbt{ position:absolute; top:130px; left:0px; outline:none; display:none;z-index:999;}
a.button{-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px;font-family:arial;font-size:12px;font-weight: bold;padding:8px 12px 8px 12px;text-align: center;cursor:pointer; margin:10px 10px 0 0; float:left;}
.red{color:#fff;background: #eb8484;text-shadow:1px 1px #cc5959;}
.green{color:#fff;background: #7dc44e;text-shadow:1px 1px #4c9021;}
.red:hover{ background-color:#d96060;}
.green:hover{ background-color:#69b736;}
#panelwrap{width:90%; margin:20px auto; background-color:#FFFFFF;-moz-border-radius:10px;-webkit-border-radius:10px;-khtml-border-radius:10px; border-radius:10px;border:6px #c0cdd2 solid;}
#loginpanelwrap{width:500px; margin:120px auto auto auto; background-color:#FFFFFF;-moz-border-radius:10px;-webkit-border-radius:10px;-khtml-border-radius:10px; border-radius:10px;border:6px #c0cdd2 solid;}
#right_wrap{float: left;width: 100%;}
#right_content{margin:10px 10px 10px 10px;}
.form_sub_buttons{ padding:0 0 20px 0; float:left;}
#right_content h2{ background-color:#bad7e6;margin:0px; clear:both;
-moz-border-radius-topleft:6px;-webkit-border-top-left-radius:6px;-khtml-border-top-left-radius:6px;border-top-left-radius:6px;
-moz-border-radius-topright:6px;-webkit-border-top-right-radius:6px;-khtml-border-top-right-radius:6px;border-top-right-radius:6px;
color:#22425e; font-size:14px; font-weight:bold; padding:10px 0 10px 15px; text-shadow:1px 1px #DCEEF7;
border-bottom:1px #ABC7D6 solid;
}
.gvtbl th {
    padding: 8px;
    font-weight: bold;
    font-size: 12px;
    color: #535E66;
    background: #dde8f0;
    text-shadow: 1px 1px #F2F8FC;
}
</style>
<script>
    $(document).ready(function () {
        $("#dLabel").click(function () {
            truevar = true;
            $.ajax({type: 'POST',
                dataType: "text",
                url: "<?= base_url('dashboard/notificationseen'); ?>",
                data: {'projId': truevar},
                success: function (responData) {
                    $("#notification_count").html('0');
                    
                },
            });

        });
    });

</script>