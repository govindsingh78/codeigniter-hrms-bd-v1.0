<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php
    $this->load->view('include/innerhead');
    $statusArr = array('1' => '<span style="color:green">Enable</span>', '0' => '<span style="color:red">Disable</span>');
    ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        
        <div class="wrapper">
            <?php $this->load->view('include/sidebar'); ?>
			<div id="content" class="col-lg-10 col-sm-10">
                    <!-- content starts -->
                    <div>
                        <ul class="breadcrumb">
                            <li> <a href="<?= base_url(); ?>">Home</a></li>
                            <li><a href="<?= thisurl(); ?>"><?= $title ?></a></li>
                        </ul>
                    </div>
                    <div class="row">

                        <?php if ($this->session->flashdata('msg')) { ?>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                            </div>
                        <?php } ?>

                        <?php if ($this->session->flashdata('errormsg')) { ?>
                            <div class="alert danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><div id="msgs">Error ! </div></strong> <?= $this->session->flashdata('errormsg'); ?>
                            </div>
                        <?php } ?>

                        <div class="box col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                       
                                        <div class="box">
                                            <div class="box-header with-border"></div>
                                            <br>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <?php if (@$res[0]): ?>
                                                    <hr>

                                                    <div class="container">
														<form method="post" action="<?= base_url('company/savewebsite'); ?>" >
                                                                <div class="form-group col-md-4" >
                                                                    <label for="sel1">Website Url : <span class="required">*</span></label>
                                                                    <input required type="text" value="<?= @$res[0]->website_url; ?>" class="form-control" name="website_url" id="website_url">   
                                                                </div>
                                                                <div class="form-group col-md-4" >
                                                                    <label for="sel1">Department : <span class="required">*</span></label>
                                                                    <input required type="text"  class="form-control" name="department" id="department" value="<?= @$res[0]->department; ?>" >   
                                                                </div>

                                                                <div class="form-group col-md-4" >
                                                                    <label for="sel1"> Eproc  : </label>
                                                                    <input type="text" class="form-control" name="eproc" id="eproc" value="<?= @$res[0]->eproc; ?>">   
                                                                </div>

                                                                <div class="form-group col-md-4" >
                                                                    <label for="sel1">User ID : </label>
                                                                    <input  type="text"  class="form-control" name="user_name" id="user_name" value="<?= @$res[0]->user_name; ?>">   
                                                                </div>

                                                                <div class="form-group col-md-4" >
                                                                    <label for="sel1">Password : </label>
                                                                    <input  type="text"  class="form-control" name="password_encode" id="password_encode" value="<?= @$res[0]->password_encode; ?>"> 
                                                                </div>

                                                                <div class="form-group col-md-4" >
                                                                    <label for="sel1">Valid To :</label>
																	<input type="date" class="form-control" name="valid_to" id="valid_to" value="<?= @$res[0]->valid_to; ?>"> 
                                                                </div>
                                                                <div class="form-group col-md-4" >
                                                                    <label for="sel1">Valid Form : </label>
                                                                    <input type="date" class="form-control" name="valid_from" id="valid_from" value="<?= @$res[0]->valid_from; ?>">   
                                                                </div>
                                                                <div class="form-group col-md-4" >
                                                                    <label for="sel1">Other Details : </label>
																	<textarea name="other_details"><?= @$res[0]->other_details; ?></textarea>	
                                                                </div>
                                                                <div class="form-group col-md-12" >
																	<input type="hidden" name="company_id" value="<?= @$res[0]->fld_id; ?>" />
                                                                    <input type="submit" class="btn btn-primary">   
                                                                </div>
                                                            </form>
                                                        
                                                    </div>
                                                <?php endif; ?>

                                                <hr>

                                               
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        </div>	


        <!-- BootStrap Model For Comments.. -->
        <?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->
        <?php $this->load->view('include/footer'); ?>
		<?php $this->load->view('include/datatablejs'); ?>
        <!-- Code For Multi Select By Asheesh -->
		<style>
			.comany_font{color:black; padding-left: 4%;}
		</style>
        <link href="<?= FRONTASSETS; ?>fm.selectator.jquery.css" rel="stylesheet" type="text/css">
        <script src="<?= FRONTASSETS; ?>fm.selectator.jquery.js"></script> 
    </body>
</html>
<style>
    table{
        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
        font-size: 14px;
    }
</style>
