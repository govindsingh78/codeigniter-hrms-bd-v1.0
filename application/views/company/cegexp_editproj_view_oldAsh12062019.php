<html lang="en">
    <?php
    error_reporting(E_ALL); //error_reporting(0);
    $statusArr = array('won' => '1', 'loose' => '2', 'awaiting' => '0', 'cancel' => '3');

    // $projId = '174617';
    // $baseCompId = '1004';
    //$typS = '1';
    //  $rrEccT = GetCompAlldetails($projId, $baseCompId, '3');
    // print_r($rrEccT);
    ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        <div class="wrapper">
            <?php $this->load->view('include/sidebar'); ?>
            <div id="content" class="col-lg-10 col-sm-10">
                <div>
                    <ul class="breadcrumb">
                        <li> <a href="<?= base_url(); ?>">Home</a></li>
                        <li><a href="#">Edit Awarded Project </a></li>
                    </ul>
                </div>
                <div class="row">
                    <?php if ($this->session->flashdata('msg')) { ?>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                        </div>
                    <?php } ?>
                    <div class="box col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form id="form-filter" class="form-horizontal">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="d-flex w-100 justify-content-between"> 
                                                <?PHP /* if (!($consortiumArr)) { ?>
                                                  <a data-toggle="modal" data-target="#myModalcons" style="cursor:pointer" class="btn btn-primary pull-left">Consortium</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                  <?PHP } */ ?>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a data-toggle="modal" data-target="#myModalprojstatus" style="cursor:pointer" class="btn btn-primary pull"> Project Status</a>
                                                <a href="<?= base_url('company/cegexp'); ?>" class="btn btn-primary pull-right">View All Projects</a>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                    <!--<a href="<?= base_url('company/save_download/' . $cegexpRec->fld_id); ?>" class="btn btn-primary pull-right">PDS Download</a>-->
                                                <a data-toggle="modal" data-target="#mypsdModel" style="cursor:pointer" class="btn btn-primary pull-left">PDS View</a>
                                            </div>
                                        </div>
                                        <div class="box">
                                            <div class="box-header with-border">
                                                <br>
                                            </div>
                                            <br>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <!--Project View Section start -->
                                                <div class="container">
                                                    <div class="panel-heading">
                                                        <h3 style="cursor: pointer; color: green" data-toggle="modal" data-target="#myModalbasic" class="panel-title pull-right"> Edit </h3>
                                                        <br>
                                                    </div>
                                                    <table class="table table-bordered">

                                                        <thead>
                                                            <tr>
                                                                <th>Project Name </th>
                                                                <td colspan="3"><?= $bddetail->TenderDetails; ?></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Service </th>
                                                                <td><?= $service->service_name; ?></td>
                                                                <th>Client </th>
                                                                <td><?= isset($expres->client) ? $expres->client : $bddetail->Organization; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Country </th>
                                                                <td><?= $this->mastermodel->countNameById($projdetail->country_id); ?></td>
                                                                <th>State </th>
                                                                <td><?= $this->mastermodel->GetStateNameById($projdetail->state_id); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Start Year </th>
                                                                <td><?= $expres->start_year; ?></td>
                                                                <th>Funding </th>
                                                                <td><?= $expres->funding; ?> </td>
                                                            </tr>
                                                            <tr>
                                                                <th>Bid Security Type </th>
                                                                <td><?= !empty($projdetail->bid_securitytype) ? $securitytype->security_type : '' ?></td>
                                                                <th>Bid Security Amount </th>
                                                                <td><?= !empty($projdetail->amount) ? $projdetail->amount : ''; ?> </td>
                                                            </tr>
                                                            <tr>
                                                                <th>Cost Of RFP</th>
                                                                <td><?= !empty($projdetail->rfp_cost) ? $projdetail->rfp_cost : ''; ?></td>
                                                                <th>&nbsp;</th>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>   
                                                    </table>
                                                </div>


                                                <div class="container">
                                                    <div class="panel-heading">
                                                        <h3 style="cursor: pointer; color: green" data-toggle="modal" data-target="#myModalsummery" class="panel-title pull-right"> Edit </h3>
                                                        <br>
                                                    </div>
                                                    <table class="table table-bordered">                                                           
                                                        <tbody>
                                                            <tr>
                                                                <th>Project Code : </th>
                                                                <td><?= @$expres->generated_tenderid; ?></td>
                                                                <th>Project Number : </th>
                                                                <td><?= @$projectname->project_name; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Sector :</th>
                                                                <td><?= @$sectorname->sectName; ?></td>
                                                                <th>Start Date :</th>
                                                                <td>
                                                                    <?php
                                                                    if ($expsummery->start_date != '0000-00-00'):
                                                                        echo date('d-m-Y', strtotime(@$expsummery->start_date));
                                                                    endif;
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>End Date :</th>
                                                                <td><?php
                                                                    if ($expsummery->end_date != '0000-00-00'):
                                                                        echo date('d-m-Y', strtotime(@$expsummery->end_date));
                                                                    endif;
                                                                    ?>
                                                                </td>
                                                                <th>Project Cost :</th>
                                                                <td><?= number_format($expsummery->project_cost, 2); ?> </td>
                                                            </tr>
                                                            <tr>
                                                                <th>Revised Con. Value RCV :</th>
                                                                <td><?= number_format($expsummery->project_rcv, 2); ?></td>
                                                                <th>CEG Share :</th>
                                                                <td><?= number_format($expsummery->cegshare_val, 2); ?> </td>
                                                            </tr>
                                                            <tr>
                                                                <th>Consultancy Fee :</th>
                                                                <td><?= number_format($expsummery->consultancy_fee, 2); ?></td>
                                                                <!--<th>Lane :</th>
                                                                <td><?= @$expsummery->lane; ?> </td>-->
                                                                <td  colspan="2">
                                                                    <table class="table table-bordered">                                                           
                                                                        <tbody>
                                                                            <tr>
                                                                                <th>Lane</th>
                                                                                <th>Length</th>
                                                                                <th>Remove</th>
                                                                            </tr>
                                                                            <?php
                                                                            if (!empty($ceglane)) {
                                                                                foreach ($ceglane as $val) {
                                                                                    ?>
                                                                                    <tr>
                                                                                        <td><?= $val->lane; ?></td>
                                                                                        <td><?= $val->lane_length; ?></td>
                                                                                        <td><a data-laneid = "<?= $val->id; ?>" class="lane_id btn btn-danger remove"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>
                                                                                    </tr>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>

                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th> Length :</th>
                                                                <td><?= @$expsummery->length; ?></td>
                                                                <th>PDS File :</th>
                                                                <td>
                                                                    <?php if ($expsummery->pds_file): ?>
                                                                        <a href="<?= HOSTNAME . "uploads/pdsfile/" . @$expsummery->pds_file; ?>" target="_blank"> <i class="glyphicon glyphicon-download-alt"></i> Download </a>
                                                                    <?php endif; ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th> Certificate  :</th>
                                                                <td>
                                                                    <?php if ($expsummery->certificate_file): ?>
                                                                        <a href="<?= HOSTNAME . "uploads/certificate/" . @$expsummery->certificate_file; ?>" target="_blank"> <i class="glyphicon glyphicon-download-alt"></i> Download </a> 
                                                                    <?php endif; ?>
                                                                </td>
                                                                <th>Contract Agreement :</th>
                                                                <td>
                                                                    <?php if ($expsummery->contract_agreement_file): ?>
                                                                        <a href="<?= HOSTNAME . "uploads/contract/" . @$expsummery->contract_agreement_file; ?>" target="_blank"> <i class="glyphicon glyphicon-download-alt"></i> Download </a>  
                                                                    <?php endif; ?>
                                                                </td>
                                                            </tr>

                                                            <?php
//if Have u consortium 
                                                            if ($consortiumArr) {
                                                                $leadComp = $consortiumArr->lead_companes;
                                                                $associate_company = $consortiumArr->associate_company;
                                                                $jv_company = $consortiumArr->joint_ventures;
                                                                ?>
                                                                <tr>
                                                                    <th>Lead Company :</th>
                                                                    <td>
                                                                        <?php
                                                                        if ($leadComp):
                                                                            $leadComp = explode(",", $leadComp);
                                                                            foreach ($leadComp as $compRow) {
                                                                                $compName .= $this->mastermodel->GetCompNameById($compRow) . " , ";
                                                                            }
                                                                            echo @$compName;
                                                                        endif;
                                                                        ?>
                                                                    </td>
                                                                    <th>Associate Company :</th>
                                                                    <td>
                                                                        <?php
                                                                        if ($associate_company):
                                                                            $assoComp = explode(",", $associate_company);
                                                                            foreach ($assoComp as $compRow) {
                                                                                $compName2 .= $this->mastermodel->GetCompNameById($compRow) . " , ";
                                                                            }
                                                                            echo @$compName2;
                                                                        endif;
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Joint Venture :</th>
                                                                    <td colspan="3">
                                                                        <?php
                                                                        if ($jv_company):
                                                                            $jvComp = explode(",", $jv_company);
                                                                            foreach ($jvComp as $compRow) {
                                                                                $compName3 .= $this->mastermodel->GetCompNameById($compRow) . " , ";
                                                                            }
                                                                            echo @$compName3;
                                                                        endif;
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>   
                                                    </table>
                                                </div>
                                                <?php // echo '<pre>'; print_r($cegcompcomp);    ?>

                                                <div class="container">
                                                    <div class="panel-heading">
                                                        <span style="cursor: pointer; color: green" data-toggle="modal" data-target="#Modalweightage" class="panel-title"> Add Weightage </span>
                                                        <span style="cursor: pointer; color: green" data-toggle="modal" data-target="#myModalcompt" class="panel-title pull-right"> Add Comptitor </span>
                                                        <br>
                                                    </div>
                                                    <table id="example" class="display">                                                         
                                                        <thead>
                                                            <tr>
                                                                <th>Sr </th>
                                                                <th>Company </th>
                                                                <th>Technical</th>
                                                                <th>Marks &nbsp;
                                                                    <a href="<?= base_url('company/reloadscore_tech/' . $bddetail->fld_id); ?>">
                                                                        <li style="cursor:pointer" title="Generate" class="glyphicon glyphicon-refresh"></li>
                                                                    </a>
                                                                </th>
                                                                <th>Financial</th>
                                                                <th>
                                                                    Marks &nbsp; 
                                                                    <a href="<?= base_url('company/reloadscore_finan/' . $bddetail->fld_id); ?>">
                                                                        <li style="cursor:pointer" title="Generate" class="glyphicon glyphicon-refresh"></li>
                                                                    </a>
                                                                </th>
                                                                <th>Total</th>
                                                                <th>Rank &nbsp;&nbsp; 

                                                                </th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                            <?php
//start Rank Code
                                                            $sortArr = array();
                                                            $temrArr = array();
                                                            foreach ($cegcompcomp as $comptrow) {
                                                                $totRec7 = ($comptrow->technical_marks + $comptrow->financial_marks);
                                                                $temrArr[$comptrow->fld_id] = number_format($totRec7, 2);
                                                            }
                                                            arsort($temrArr);
                                                            $finalRank = array();
                                                            $nnmms = 1;
                                                            foreach ($temrArr as $kky => $rwrank):
                                                                $finalRank[$kky] = $nnmms;
                                                                $nnmms++;
                                                            endforeach;
//Close Rank Code
                                                            $ssnn = 0;
                                                            if ($cegcompcomp):
                                                                foreach ($cegcompcomp as $comptrow) {
                                                                    ?>
                                                                    <tr>
                                                                        <td><?= $ssnn = $ssnn + 1; ?></td>
                                                                        <td><?PHP //if (!in_array('74', $jvComp)) {       ?>
                                                                            <small data-toggle="modal" onclick="jvforcompsetid('<?= $comptrow->compt_comp_id ?>')" data-target="#myModaljv" title="Add Join joint Venture" style="color:green; cursor: pointer; border: solid 1px black; padding: 2px"> JV </small>
                                                                            <?php // }    ?>
                                                                            &nbsp; <?= $this->mastermodel->GetCompNameById($comptrow->compt_comp_id); ?>
                                                                            <!--<small data-toggle="modal" onclick="jvforcompedit('')" data-target="#myModaljvedit" title="Edit joint Venture" style="color:green; cursor: pointer; border: solid 1px black; padding: 2px" ><i class="fa fa-eye"></i></small>-->

                                                                            &nbsp;&nbsp;

                                                                            <?php //if ($comptrow->compt_comp_id != '74') {    ?>
                                                                            <small onclick="delcomptcom('<?= $comptrow->fld_id ?>')" title="Delete" style="color:green; cursor: pointer; border: solid 1px black; padding:2px">
                                                                                <i class="fa fa-trash">Del</i>
                                                                            </small>

                                                                           

                                                                            <?php
                                                                            $rrEccT1 = '';
                                                                            $rrEccT2 = '';
                                                                            $rrEccT3 = '';

                                                                            $rrEccT1 = GetCompAlldetails($bddetail->fld_id, $comptrow->compt_comp_id, '1');
                                                                            $rrEccT2 = GetCompAlldetails($bddetail->fld_id, $comptrow->compt_comp_id, '2');
                                                                            $rrEccT3 = GetCompAlldetails($bddetail->fld_id, $comptrow->compt_comp_id, '3');
                                                                            ?>

                                                                            <?php if ($rrEccT1 or $rrEccT2 or $rrEccT3) { ?>
                                                                            <table style="margin: 10px; border:#ccc solid 1px">
                                                                                    <?php if ($rrEccT1) : ?>
                                                                                        <tr>
                                                                                            <td><?= $rrEccT1; ?></td>
                                                                                        </tr>
                                                                                    <?php endif; ?>
                                                                                    <?php if ($rrEccT2) : ?>
                                                                                        <tr>
                                                                                            <td><?= $rrEccT2; ?></td>
                                                                                        </tr>
                                                                                    <?php endif; ?>
                                                                                    <?php if ($rrEccT3) : ?>
                                                                                        <tr>
                                                                                            <td><?= $rrEccT3; ?></td>
                                                                                        </tr>
                                                                                    <?php endif; ?>
                                                                                </table>

                                                                            <?php } ?>


                                                                        </td>
                                                                        <td><?= number_format($comptrow->technical_score, 2); ?></td>
                                                                        <td><?= number_format($comptrow->technical_marks, 2); ?></td>
                                                                        <!--<td>
                                                                        <?php
                                                                        if (($comptrow->techn_weightage) and ( $comptrow->technical_score)) {
                                                                            $reeMulScore = ($comptrow->technical_score * $comptrow->techn_weightage);
                                                                        }
                                                                        echo isset($reeMulScore) ? $reeMulScore : '0.00';
                                                                        ?>
                                                                                                                                                    </td>-->
                                                                        <td><?= number_format($comptrow->financial_score, 2); ?></td>
                                                                        <td><?= number_format($comptrow->financial_marks, 2); ?></td>
                                                                        <td>
                                                                            <?php
                                                                            $totRec = ($comptrow->technical_marks + $comptrow->financial_marks);
                                                                            echo number_format($totRec, 2);
                                                                            ?>
                                                                        </td>
                                                                        <td>
                                                                            <?= $finalRank[$comptrow->fld_id]; ?> &nbsp;&nbsp; <li style="cursor:pointer" onclick="setupdid('<?= $comptrow->fld_id; ?>')" data-toggle="modal" data-target="#myModalscore" title="Generate Rank" class="glyphicon glyphicon-edit"></li>
                                                                </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        endif;
                                                        ?>
                                                        </tbody>

                                                    </table>
                                                </div>

                                                <span>&nbsp;&nbsp;&nbsp;<button type="submit" name="mailsender" value="Send_Mail" class="btn glyphicon-envelope btn-primary"> Send Mail Competitor</button></span>
                                                <span>&nbsp;&nbsp;&nbsp;<button type="submit" name="mailsendercom" value="mailsendercom" class="btn glyphicon-envelope btn-primary"> Send Mail 8020</button></span>

                                            </div>
                                            <!-- /.box-footer -->
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- open Phase Section-->
                    <div class="box-inner">
                        <div class="box-content">
                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="colvis"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Close phase Section-->
            </div>

        </div>	

        <hr>


        <div class="modal fade" id="Modalweightage" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add / Update Weightage</h4>
                    </div>
                    <div class="modal-body">
                        <form name="frmm" id="frmm" method="post" action="<?= base_url('company/updatefixweightage'); ?>" enctype="">
                            <div class="row">
                                <div class="form-group required col-md-6">
                                    <label> Technical Weightage </label>
                                    <input type="number" min="0" step="any" name="techn_weightage" class="form-control" value="<?= number_format($cegcompcomp[0]->techn_weightage, 2); ?>" >
                                </div>
                                <div class="form-group required col-md-6">
                                    <label> Financial Weightage </label>
                                    <input type="number" min="0" step="any" name="finan_weightage" class="form-control" value="<?= number_format($cegcompcomp[0]->finan_weightage, 2); ?>">
                                </div>   
                                <div class="form-group required col-md-6">
                                    <input type="hidden" name="project_id" class="btn green" value="<?= $bddetail->fld_id; ?>">
                                    <input type="submit" class="btn green" value="Update">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="myModalscore" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Comptitor</h4>
                    </div>
                    <div class="modal-body">
                        <form name="frmm" id="frmm" method="post" action="<?= base_url('company/updateweightage'); ?>" enctype="">
                            <div class="row">
                                <div class="form-group required col-md-6">
                                    <label> Technical Weightage </label>
                                    <input type="number" readonly min="0" step="any" name="techn_weightage" class="form-control" id="techn_weightage">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label> Financial Weightage </label>
                                    <input type="number" readonly min="0" step="any" name="finan_weightage" class="form-control" id="finan_weightage">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label> Technical </label>
                                    <input type="number" min="0" step="any" name="technical_score" class="form-control" id="technical_score">
                                </div>

                                <div class="form-group required col-md-6">
                                    <label> Financial  </label>
                                    <input type="number" step="any" name="financial_score" class="form-control" id="financial_score">
                                </div>

                                <div class="form-group required col-md-6">
                                    <input type="hidden" name="project_id" class="btn green" value="<?= $bddetail->fld_id; ?>">
                                    <input type="hidden" name="actid" id="actionid" >
                                    <input type="submit" class="btn green" value="Update">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



        <!-- Modal For Add Comptitor Company Details -->
        <div class="modal fade" id="myModalcompt" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Comptitor</h4>
                    </div>
                    <div class="modal-body">
                        <form name="frmm" id="frmm" method="post" action="<?= base_url('company/addcomptcompn'); ?>" enctype="">
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <label>Add Company </label>
                                    <select id="multiple" name="leadcompany[]" style="width:100%" multiple>    
                                        <?php foreach ($compnameArr as $rowrec) { ?>
                                            <option value="<?= $rowrec->fld_id; ?>">
                                                <?= ucfirst($rowrec->company_name); ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group required col-md-6">
                                    <input type="hidden" name="project_id" class="btn green" value="<?= $bddetail->fld_id; ?>">
                                    <input type="submit" class="btn green" value="Add">
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>


        <!--//Add Joint Venture Code Bty Asheesh -->
        <div class="modal fade" id="myModaljv" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Consortium </h4>
                        <!--<h5 class="modal-title" align="center"> Sole : <input type="checkbox" checked id="soleornot1"> </h5>-->
                    </div> 
                    <form name="frmm" id="frmm" method="post" action="<?= base_url('company/addjointventure'); ?>" enctype="">
                        <div class="modal-body" id="jvsection1">
                            <div class="controls">
                                <label>Lead Company </label>
                                <select class="leaddataid" id="multiple" name="leadcompany[]" style="width:100%" multiple>    
                                    <?php foreach ($compnameArr as $rowrec) {
                                        ?>
                                        <option value="<?= $rowrec->fld_id; ?>">
                                            <?= ucfirst($rowrec->company_name); ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="controls">   
                                <label> JV (<small> joint venture </small>)</label>
                                <select id="multiple1" name="joint_venture[]" style="width:100%" multiple>    
                                    <?php foreach ($compnameArr as $rowrec) { ?>
                                        <option value="<?= $rowrec->fld_id; ?>">
                                            <?= ucfirst($rowrec->company_name); ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="controls">
                                <label> Associate company..</label>
                                <select id="multiple2" name="assoc[]" style="width:100%" multiple>    
                                    <?php foreach ($compnameArr as $rowrec) { ?>
                                        <option value="<?= $rowrec->fld_id; ?>">
                                            <?= ucfirst($rowrec->company_name); ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <br>
                            <div class="controls">
                                <input type="hidden" name="project_id" value="<?= $bddetail->fld_id; ?>" id="project_id">
                                <input type="hidden" name="leadcompanyid" value="" id="leadcompanyid">
                                <input type="submit" class="btn" name="submit" value="Add / Save">
                            </div>
                        </div>
                    </form>  
                </div>
            </div>
        </div>




        <!--<div id="myModalcons" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <form action="<?= base_url('company/savejvdata'); ?>" method="post">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"> Consortium </h4>
                            <h5 class="modal-title" align="center"> Sole : <input type="checkbox" checked id="soleornot2"> </h5>
                        </div>
                        <div class="modal-body" id="jvsection2" style="display:none">
                            <div class="controls">
                                <label>Lead Company </label>
                                <select id="multiple" name="leadcompany[]" style="width:100%" multiple>    
        <?php foreach ($compnameArr as $rowrec) {
            ?>
                                                                    <option <?= $selected ?> value="<?= $rowrec->fld_id; ?>">
            <?= ucfirst($rowrec->company_name); ?>
                                                                    </option>
        <?php } ?>
                                </select>
                            </div>
                            <div class="controls">   
                                <label> JV (<small> joint venture </small>)</label>
                                <select id="multiple1" name="joint_venture[]" style="width:100%" multiple>    
        <?php foreach ($compnameArr as $rowrec) { ?>
                                                                    <option value="<?= $rowrec->fld_id; ?>">
            <?= ucfirst($rowrec->company_name); ?>
                                                                    </option>
        <?php } ?>
                                </select>
                            </div>
                            <div class="controls">
                                <label> Associate company..</label>
                                <select id="multiple2" name="assoc[]" style="width:100%" multiple>    
        <?php foreach ($compnameArr as $rowrec) { ?>
                                                                    <option value="<?= $rowrec->fld_id; ?>">
            <?= ucfirst($rowrec->company_name); ?>
                                                                    </option>
        <?php } ?>
                                </select>
                            </div>
                            <br>
                            <div class="controls">
                                <input type="hidden" name="cegexp_id" value="<?= $cegexpRec->fld_id; ?>" id="cegexp_id">
                                <input type="submit" class="btn" name="submit" value="Add / Save">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>-->


        <div class="modal fade" id="myModaljvedit" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Coming Soon </h4>
                    </div>
                    <!--       <div class="modal-body">
                                            <form name="frmm" id="frmm" method="post" action="<?= base_url('company/updatejointventure'); ?>" enctype="">
                                                <div class="row">
                                                    <div class="form-group required col-md-12">
                                                        <label>Add Company </label>
                                                        <select id="multiplejvUPD" name="leadcompanyUPD[]" style="width:100%" multiple>    
                    <?php //foreach ($compnameArr as $rowrec) {      ?>
                                                                <option value="<?php // $rowrec->fld_id;                ?>">
                    <?php // ucfirst($rowrec->company_name);      ?>
                                                                </option>
                    <?php // }      ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group required col-md-6">
                                                        <input type="hidden" name="projid" value="<?= $cegexpRec->fld_id; ?>">
                                                        <input type="hidden" id="base_company" value="">
                                                        <input type="submit" class="btn green" value="Update">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>-->
                </div>
            </div>
        </div>



        <!-- Modal For Project Status Code By Asheesh.. content--> 
        <div class="modal fade" id="myModalprojstatus" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 id="tender_title" class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <form action="<?= base_url('company/changeprojstatus'); ?>" method="post">
                            <div class="box-content">
                                <div class="controls">
                                    <label>Select Project Status..</label>
                                    <select required="required" class="form-control"  name="project_status">
                                        <option <?= ($project_status->project_status == '') ? 'selected' : ''; ?> value="">--select--</option>
                                        <option <?= ($project_status->project_status == '0') ? 'selected' : ''; ?> value="0">Awaiting</option>
                                        <option <?= ($project_status->project_status == '1') ? 'selected' : ''; ?> value="1">Won</option>
                                        <option <?= ($project_status->project_status == '2') ? 'selected' : ''; ?> value="2">Loose</option>
                                        <option <?= ($project_status->project_status == '3') ? 'selected' : ''; ?> value="3">Cancel</option>
                                    </select>
                                </div>
                            </div>
                            <div class="box-content">
                                <input type="hidden" id="project_id" name="project_id" value="<?= $bddetail->fld_id; ?>">
                                <input type="submit"  value="Update" class="btn btn-success glyphicon glyphicon-user" >
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>



        <script>
            //add
            function jvforcompset(compid) {
                $("#base_company").val(compid);
            }
            //Edit..
            function jvforcompedit(compid) {
                // alert('rtyhrt' + compid);
                // $("#base_companyUPD").val(compid);

//                 var valArr = [74, 75];
//                 i = 0, size = valArr.length;
//                for (i; i < size; i++) {
//                $("#multiplejvUPD").multiselect("widget").find(":checkbox[value='" + valArr[i] + "']").attr("checked", "checked");
//                 $("#multiplejvUPD option[value='" + valArr[i] + "']").attr("selected", 1);
//                 $("#multiplejvUPD").multiselect("refresh");
            }

            /* $(document).ready(function () {
             $("#soleornot1").click(function () {
             if ($(this).prop('checked') == true) {
             $("#jvsection1").hide();
             } else {
             $("#jvsection1").show();
             }
             });
             }); */

            $(document).ready(function () {
                $("#soleornot2").click(function () {
                    if ($(this).prop('checked') == true) {
                        $("#jvsection2").hide();
                    } else {
                        $("#jvsection2").show();
                    }
                });
            });

        </script>


        <!-- Modal For Edit Basic Details -->
        <div class="modal fade" id="myModalbasic" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Basic Project Details</h4>
                    </div>

                    <div class="modal-body">
                        <form name="frmm" id="frmm" method="post" action="<?= base_url('company/updatecegexp'); ?>" enctype="">
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <label class="control-label" for="usr">Project Name </label>
                                    <textarea rows="3" name="project_name" required cols="27" class="form-control"><?= $bddetail->TenderDetails; ?></textarea>
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> Submission Date</label>
                                    <input type="date" class="form-control" id="submission_date" name="submission_date"  value="<?= $projdetail->submission_date; ?>">
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> Bid Validity Date</label>
                                    <input type="date" class="form-control" id="tender_openingdate" name="tender_openingdate"  value="<?= $projdetail->tender_openingdate; ?>">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Service </label>
                                    <!--<input type="text" value="<?= $expres->service; ?>" name="service" class="form-control" id="service">-->
                                    <select id="service" name="service_id" class="form-control col-md-4 col-xs-12">
                                        <option value=""> -- Select Service -- </option>
                                        <?php
                                        if ($serviceArr):
                                            foreach ($serviceArr as $rowr) {
                                                ?>
                                                <option <?= (@$projdetail->service_id == $rowr->id) ? 'selected' : ''; ?> value="<?= $rowr->id; ?>"> <?= $rowr->service_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>  
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Client </label>
                                    <input type="text" value="<?= isset($expres->client) ? $expres->client : $bddetail->Organization; ?>" required class="form-control" name="client" id="client">
                                </div>

                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Country </label>
                                    <select id="country" name="countries_id" onchange="setstatebycid()" class="form-control col-md-4 col-xs-12">
                                        <option value=""> -- Select Country -- </option>
                                        <?php
                                        if ($country_Arr):
                                            foreach ($country_Arr as $rowr) {
                                                ?>
                                                <option <?= (@$projdetail->country_id == $rowr->country_id) ? 'selected' : ''; ?> value="<?= $rowr->country_id; ?>"> <?= $rowr->country_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>
                                <?php //echo '<pre>'; print_r($stateArr);   ?>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">State </label>
                                    <select id="state" name="state_id" class="form-control col-md-4 col-xs-12">
                                        <?php
                                        if ($stateArr):
                                            foreach ($stateArr as $rowrsec) {
                                                ?>
                                                <option <?= (@$projdetail->state_id == $rowrsec->state_id) ? 'selected' : ''; ?> value="<?= $rowrsec->state_id; ?>"> <?= $rowrsec->state_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>

                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Start Year </label>
                                    <input type="text" value="<?= $expres->start_year; ?>" name="start_year" class="form-control" id="start_year">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Funding </label>
                                    <input type="text" value="<?= $expres->funding; ?>"  class="form-control" name="funding" id="funding">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Bid Security</label>
                                    <select required id="bid_security" name="bid_security" class="form-control col-md-4 col-xs-12">
                                        <option value=""> -- Select Type-- </option>
                                        <option value="1" <?= (@$projdetail->bid_security == 1) ? 'selected' : ''; ?>> Yes </option>
                                        <option value="2" <?= (@$projdetail->bid_security == 2) ? 'selected' : ''; ?>> No </option>
                                    </select>
                                </div>
                                <?php
                                if ($projdetail->bid_security == 1) {
                                    $class = 'display:block';
                                } else {
                                    $class = 'display:none';
                                }
                                ?>
                                <div id="security_demnd" style="<?= $class; ?>">
                                    <div class="form-group required col-md-6">
                                        <label class="control-label" for="usr">Bid Security Type </label>
                                        <select id="bid_securitytype" name="bid_securitytype" class="form-control col-md-4 col-xs-12" >
                                            <option value=""> -- Select Type-- </option>
                                            <?php
                                            if ($security_Type):
                                                foreach ($security_Type as $rowrsec) {
                                                    ?>
                                                    <option <?= (@$projdetail->bid_securitytype == $rowrsec->id) ? 'selected' : ''; ?> value="<?= $rowrsec->id; ?>"> <?= $rowrsec->security_type; ?></option>
                                                <?php } endif; ?>
                                        </select>
                                    </div>
                                    <div class="form-group required col-md-6">  
                                        <label>Cost Of Rfp</label>
                                        <input type="text" class="form-control" id="rfp_cost" name="rfp_cost" value="<?= $projdetail->rfp_cost; ?>" >
                                    </div>

                                    <div class="form-group required col-md-6">  
                                        <label> Amount</label>
                                        <input type="text" class="form-control" id="amount" name="amount" value="<?= $projdetail->amount; ?>" >
                                    </div>
                                </div>

                                <div class="form-group required col-md-12">
                                    <input type="hidden" name="projectid" class="btn green" value="<?= $bddetail->fld_id; ?>">
                                    <input type="submit" class="btn green" value="Update">
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <link href="<?= FRONTASSETS; ?>css/jquery.multiselect.css" rel="stylesheet"></link>
        <!-- Modal For Edit Summery Details -->
        <div class="modal fade" id="myModalsummery" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Summery of Project</h4>
                        <h4 id="uploaderror2" class="modal-title" style="color:red"></h4>                  
                    </div>
                    <div class="modal-body">
                        <form method="post" action="<?= base_url('company/updatesummery'); ?>" enctype="multipart/form-data">
                            <div class="row">
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Project Code </label>
                                    <input type="text" value="<?= $expres->generated_tenderid; ?>" disabled class="form-control" id="project_code">
                                </div>
                                <!--<div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Project Number </label>
                                    <input type="text" class="form-control" value="<?= $cegexpSummery->project_number; ?>" name="project_number" id="project_number">
                                </div>-->
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Project Number </label>
                                    <select id="project_number" name="project_number" class="form-control col-md-4 col-xs-12">
                                        <option value="" selected >Select Project </option>
                                        <?php
                                        if ($project):
                                            foreach ($project as $val) {
                                                ?>
                                                <option <?= (@$expsummery->project_numberid == $val->id) ? 'selected' : ''; ?> value="<?= $val->id; ?>"> <?= $val->project_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Sector </label>
                                    <!--<input type="text" class="form-control" value="<?= $cegexpSummery->sector; ?>" name="sector" id="sector">-->
                                    <select id="sector_id" name="sector_id" class="form-control col-md-4 col-xs-12">
                                        <option value=""> -- Select Service -- </option>
                                        <?php
                                        if ($sector):
                                            foreach ($sector as $rowr) {
                                                ?>
                                                <option <?= (@$projdetail->sector_id == $rowr->fld_id) ? 'selected' : ''; ?> value="<?= $rowr->fld_id; ?>"> <?= $rowr->sectName; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>

                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Start Date </label>
                                    <input type="date" name="start_date" class="form-control" value="<?= $expsummery->start_date; ?>" id="start_date">
                                </div>  
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">End Date </label>
                                    <input type="date" class="form-control" name="end_date" value="<?= $expsummery->end_date; ?>" id="end_date">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Project Cost </label>
                                    <input type="number"  min="0" step="any" class="form-control" name="project_cost" value="<?= ($expsummery->project_cost > 0) ? $expsummery->project_cost : ''; ?>" id="project_cost">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Revised Contract Value RCV </label>
                                    <input type="number"  min="0" step="any" class="form-control" name="project_rcv" value="<?= ($expsummery->project_rcv > 0) ? $expsummery->project_rcv : ''; ?>" id="project_rcv">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Consultancy Fee </label>
                                    <input type="number"  min="0" step="any" class="form-control" name="consultancy_fee" value="<?= ($expsummery->consultancy_fee > 0) ? $expsummery->consultancy_fee : ''; ?>" id="consultancy_fee">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">CEG Share</label>
                                    <input type="number"  min="0" step="any" class="form-control" name="cegshare_val" value="<?= ($expsummery->cegshare_val > 0) ? $expsummery->cegshare_val : ''; ?>" id="cegshare_val">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Other Share</label>
                                    <input type="number"  min="0" step="any" class="form-control" name="othershare_val" value="<?= ($expsummery->othershare_val > 0) ? $expsummery->othershare_val : ''; ?>" id="othershare_val">
                                </div>
                                <!-- <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Lane </label>
                                    <input type="text" class="form-control" name="lane" value="<?= $cegexpSummery->lane; ?>" id="lane">
                                </div> --> 
                                <div class="form-group required col-md-12">
                                    <label class="control-label" for="usr">Lane </label>
                                </div>
                                <?php
                                /* if(!empty($ceglane)){
                                  foreach($ceglane as $val){
                                  echo $val->lane;
                                  }
                                  } */
                                ?>
                                <div class="form-group fieldGroup col-md-12">
                                    <div class="col-md-5">

                                        <?php
                                        $sec = '2';
                                        ?>
                                        <select id="lane_chk" name="lane_chk[]" class="form-control">
                                            <option value='' selected>-Lane-</option>
                                            <?php
                                            for ($i = $sec; $i <= 12; $i = $i + 2) {
                                                ?>
                                                <option value=<?= $i; ?>><?= $i; ?></option>

                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" name="lane_length[]" class="form-control"/>
                                    </div>
                                    <div class="col-md-2">		
                                        <a href="javascript:void(0)" class="btn btn-success addMore"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> Add</a>
                                    </div>
                                </div>
                                <div class="form-group required col-md-12">
                                    <label class="control-label" for="usr">Length </label>
                                    <input type="text"  class="form-control" name="length" value="<?= $expsummery->length; ?>" id="length">
                                </div>


                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">PDS File </label>
                                    <input type="file" class="form-control" name="pds_file"  id="pds_file">
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Certificate File </label>
                                    <input type="file" class="form-control" name="certificate_file" id="certificate_file">
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Contract Agreement </label>
                                    <input type="file" class="form-control" name="contract_agreement_file" id="contract_agreement_file">
                                </div>


                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">&nbsp; </label>
                                    <input type="hidden" id="project_id" name="project_id" value="<?= $bddetail->fld_id; ?>">
                                    <input type="submit" required class="btn-primary"  value="Update">
                                    <?php //echo '<pre>'; print_r($cegexpSummery);    ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



        <!-- PDS Edit  -->
        <div class="modal fade" id="mypsdModel" role="dialog">
            <div class="modal-dialog" style="width:80%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">PDS Details</h4>
                    </div>
                    <?php //echo '<pre>'; print_r($cegpds);    ?>
                    <div class="modal-body">
                        <form name="savepds" id="savepds" method="post" action="<?= base_url('company/savepds'); ?>" enctype="">
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <label class="control-label" for="usr">Project Name </label>
                                    <textarea rows="3" disabled required cols="27" class="form-control"><?= $bddetail->TenderDetails; ?></textarea>
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Project Location </label>
                                    <?php
                                    $country = $this->mastermodel->countNameById($expres->countries_id);
                                    $state = $this->mastermodel->GetStateNameById($expres->state_id);
                                    ?>
                                    <input type="text" value="<?= $state; ?> , <?= $country; ?>" readOnly class="form-control">
                                </div>

                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Professional staff provided</label>
                                    <input type="text" value="<?= isset($cegpds->professional_staff_provided) ? $cegpds->professional_staff_provided : ''; ?>" name="professional_staff_provided" class="form-control" id="professional_staff_provided">
                                </div>  
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Client </label>
                                    <input type="text" value="<?= $expres->client; ?>" class="form-control" readOnly>
                                </div>

                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">No. of man-months </label>
                                    <input type="text" value="<?= isset($cegpds->numberof_man_month) ? $cegpds->numberof_man_month : ''; ?>" name="numberof_man_month" class="form-control" id="numberof_man_month">
                                </div>

                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Date of Commencement</label>
                                    <input type="text" value="<?= isset($cegpds->date_of_commencement) ? $cegpds->date_of_commencement : ''; ?>" name="date_of_commencement" class="form-control" id="date_of_commencement">
                                </div>

                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Date of Completion </label>
                                    <input type="text" value="<?= isset($cegpds->stipulated_date_of_completion) ? $cegpds->stipulated_date_of_completion : ''; ?>" name="stipulated_date_of_completion" class="form-control" id="stipulated_date_of_completion">
                                </div>

                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Value of Consultancy Services </label>
                                    <input type="text" value="<?= number_format($expsummery->consultancy_fee, 2); ?>"  class="form-control" readOnly>
                                </div>

                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Name of Association Firm(s), if any </label>
                                    <br/>
                                    <?php if ($associate_company): ?>
                                        <textarea readOnly rows="5" cols="50"><?= @$compName2; ?></textarea>
                                    <?php endif; ?>
                                </div>



                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">No. of man-months of professional staff provided by associated firm (s)</label>
                                    <input type="text" value="<?= isset($cegpds->mmby_associated_firm) ? $cegpds->mmby_associated_firm : ''; ?>" name="mmby_associated_firm" class="form-control" id="mmby_associated_firm">
                                </div>

                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Senior Staff involved and functions performed</label>
                                    <div>
                                        <?php
                                        if ($tqRecArr):
                                            foreach ($tqRecArr as $roRec) {
                                                ?>

                                                <?php
                                                $empNM = '';
                                                $empNM2 = '';
                                                $desiname = DesignationNameById($roRec->designation_id);
                                                if ($roRec->empname):
                                                    $empNM = UserNameById($roRec->empname);
                                                else:
                                                    $empNM2 = UserOtherNameById($roRec->empnameother);
                                                endif;
                                                ?>
                                                <?= $srn = $srn + 1; ?>. <?= $empNM . " " . $empNM2 . '-'; ?> <?= $desiname . ' , '; ?>
                                                <?php
                                            }
                                        endif;
                                        ?>
                                    </div>	
                                </div>

                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Narrative Description of Project</label>
                                    <textarea name="narrative_description" id="narrative_description">
                                        <?= isset($cegpds->narrative_description) ? $cegpds->narrative_description : ''; ?>
                                    </textarea>
                                </div>

                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Details of Major Bridges</label>
                                    <textarea name="major_bridges" id="major_bridges">
                                        <?= isset($cegpds->major_bridges) ? $cegpds->major_bridges : ''; ?>
                                    </textarea>
                                </div>

                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Details of Urban Flyovers</label>
                                    <textarea name="urbon_flyovers" id="urbon_flyovers">
                                        <?= isset($cegpds->urbon_flyovers) ? $cegpds->urbon_flyovers : ''; ?>
                                    </textarea>
                                </div>

                                <div class="form-group required col-md-6">
                                    <input type="hidden" name="project_id" value="<?= $bddetail->fld_id; ?>">
                                    <input type="submit" class="btn green" value="Update">
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>


        <!-- copy of input fields group -->
        <div class="form-group fieldGroupCopy col-md-12" style="display: none;">
            <div class="col-md-5">
                <?php
                $sec = '2';
                ?>
                <select id="lane_chk" name="lane_chk[]" class="form-control">
                    <?php
                    for ($i = $sec; $i <= 12; $i = $i + 2) {
                        ?>
                        <option value=<?= $i; ?>><?= $i; ?></option>

                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-5">
                <input type="text" name="lane_length[]" class="form-control" required />
            </div>
            <div class="col-md-2">		
                <a href="javascript:void(0)" class="btn btn-danger remove"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> Remove</a>
            </div>
        </div>


        <!-- BootStrap Model For Comments.. -->
        <?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->
        <?php $this->load->view('include/footer'); ?>
        <?php $this->load->view('include/datatablejs'); ?>
        <!-- Code For Multi Select By Asheesh -->
        <link href="<?= FRONTASSETS; ?>fm.selectator.jquery.css" rel="stylesheet" type="text/css">
        <script src="<?= FRONTASSETS; ?>fm.selectator.jquery.js"></script> 
        <script src="<?= FRONTASSETS; ?>ckeditor/ckeditor.js"></script> 
        <script src="<?= FRONTASSETS; ?>js/jquery.multiselect.js"></script>
        <script type="text/javascript">
                                        $('#bid_security').on('change', function () {
                                            var id = $(this).val();
                                            if (id == 1) {
                                                $('#security_demnd').css('display', 'block');
                                            } else {
                                                $('#security_demnd').css('display', 'none');
                                            }
                                        });
                                        $('#sector').multiselect({
                                            columns: 1,
                                            placeholder: 'Select Sector',
                                            search: true
                                        });
                                        CKEDITOR.editorConfig = function (config) {
                                            config.language = 'es';
                                            config.uiColor = '#F7B42C';
                                            config.height = 300;
                                            config.toolbarCanCollapse = true;
                                        };
                                        CKEDITOR.replace('narrative_description');
                                        CKEDITOR.replace('major_bridges');
                                        CKEDITOR.replace('urbon_flyovers');

                                        /* CKEDITOR.replace('service_description');
                                         CKEDITOR.instances.service_description.config.readOnly = true; */

                                        $('#multiple,#multiplejv,#multiplejvUPD,#multiple1,#multiple2').selectator({
                                            showAllOptionsOnFocus: true,
                                            searchFields: 'value text subtitle right'
                                        });
                                        $(document).ready(function () {
                                            $("li#cegexp").addClass('active');
                                        });
                                        //Files Upload Validation Code By Asheesh..
                                        $(document).ready(function () {
                                            $("#uploaderror2").hide();
                                            $('INPUT[type="file"]').change(function () {
                                                var ext = this.value.match(/\.(.+)$/)[1];
                                                switch (ext) {
                                                    case 'doc':
                                                    case 'DOC':
                                                    case 'docx':
                                                    case 'jpg':
                                                    case 'JPG':
                                                    case 'jpeg':
                                                    case 'pdf':
                                                    case 'PDF':
                                                        $('#uploadButton').attr('disabled', false);
                                                        $("#uploaderror2").hide();
                                                        break;
                                                    default:
                                                        $("#uploaderror2").show();
                                                        $("#uploaderror2").html('This is not an allowed file type.');
                                                        this.value = '';
                                                }
                                            });
                                            $('INPUT[type="file"]').bind('change', function () {
                                                var imgfilesize = (this.files[0].size / 1001376);
                                                if (imgfilesize > 100) {
                                                    $("#uploaderror2").show();
                                                    $("#uploaderror2").html('Reducing File Size Max Uploads (99 MB). ');
                                                    this.value = '';
                                                } else {
                                                    $("#uploaderror2").hide();
                                                }
                                            });
                                        });

                                        //Validate..
                                        function setupdid(valfield) {
                                            $("#actionid").val(valfield);
                                            $.ajax({
                                                type: 'POST',
                                                dataType: "text",
                                                url: "<?= base_url('company/ajax_score_byid'); ?>",
                                                data: {'editId': valfield},
                                                success: function (responData) {
                                                    var parsed = JSON.parse(responData);
                                                    var arr = [];
                                                    for (var x in parsed) {
                                                        arr.push(parsed[x]);
                                                    }
                                                    $("#techn_weightage").val(arr[4]);
                                                    $("#finan_weightage").val(arr[5]);
                                                    $("#technical_score").val(arr[6]);
                                                    $("#technical_marks").val(arr[7]);
                                                    $("#financial_score").val(arr[8]);
                                                    $("#financial_marks").val(arr[9]);
                                                },
                                            });
                                        }

                                        function setstatebycid() {
                                            var countryid = $("#country").val();
                                            $('#state').html('');
                                            $.ajax({
                                                url: '<?= base_url('company/ajax_state_getbycid') ?>',
                                                method: 'post',
                                                data: {country: countryid},
                                                dataType: 'json',
                                                success: function (response) {
                                                    $.each(response, function (index, data) {
                                                        $('#state').append('<option value="' + data['state_id'] + '">' + data['state_name'] + '</option>');
                                                    });
                                                }
                                            });
                                        }

                                        function jvforcompsetid(jvcompid) {
                                            $('#leadcompanyid').val(jvcompid);
                                            var companyid = jvcompid;
                                            var projectID = <?= $bddetail->fld_id; ?>;
                                            $.ajax({
                                                url: '<?= base_url('company/getcomptdata') ?>',
                                                method: 'post',
                                                data: {'companyid': companyid, 'projectID': projectID},
                                                success: function (response) {
                                                    console.log(response);
                                                    if (response) {
                                                        var data = jQuery.parseJSON(response);
                                                        var numbersString = data.lead_comp_id;
                                                        var numbersArray = numbersString.split(',');
                                                        console.log(numbersArray);
                                                        //console.log(data.lead_comp_id);
                                                        //$(".leaddataid option[value='763']").attr("selected", "selected");
                                                        //$('.id_100 option[value=val2]').attr('selected','selected');
                                                        $.each(numbersArray, function (index, value) {
                                                            //$('.leaddataid').val(value).attr("selected", "selected");
                                                            //jQuery(".leaddataid option:selected").val(value);
                                                            $(".leaddataid option[value='" + value + "']").attr("selected", "selected");
                                                        });

                                                    }
                                                },
                                            });
                                        }
                                        $(document).ready(function () {
                                            $('#service2_name_id').on('change', function () {
                                                var service2_name = $(this).val();
                                                var sector_name = $('#sector_name_id').val();
                                                var service1_name = $('#service1_name_id').val();
                                                CKEDITOR.instances.service_description.setData('');
                                                if (sector_name != '' && service2_name != '' && service1_name != '') {
                                                    var url = '<?= base_url('company/getPDSDesc_ajax') ?>';
                                                    $.ajax({
                                                        type: 'POST',
                                                        url: url,
                                                        data: {'service2_name': service2_name, 'sector_name': sector_name, 'service1_name': service1_name},
                                                        success: function (res) {
                                                            var data = JSON.parse(res);
                                                            //$('#service_description').val(data.desc);
                                                            CKEDITOR.instances['service_description'].setData(data.desc);
                                                        },
                                                    });
                                                } else {
                                                    alert('Please Select All Type');
                                                }

                                            });

                                            $('#sector_name_id').on('change', function () {
                                                var sectID = $(this).val();
                                                $('#service1_name_id').empty();
                                                if (sectID != '') {
                                                    $.ajax({
                                                        type: 'POST',
                                                        url: '<?= base_url('company/getservice1_ajax') ?>',
                                                        data: {'secid': sectID},
                                                        success: function (res) {
                                                            var data = JSON.parse(res);
                                                            $("#service1_name_id").append("<option>--Select--</option>");
                                                            for (var i = 0; i < data.length; i++) {
                                                                $("#service1_name_id").append("<option value=" + data[i].id + ">" + data[i].service_name + "</option>");
                                                            }
                                                        },
                                                    });
                                                } else {
                                                    alert('Please Select Sector');
                                                }
                                            });

                                            $('#service1_name_id').on('change', function () {
                                                var sectID = $('#sector_name_id').val();
                                                var service1_name_id = $(this).val();
                                                $('#service2_name_id').empty();
                                                if (service1_name_id != '' && sectID != '') {
                                                    $.ajax({
                                                        type: 'POST',
                                                        url: '<?= base_url('company/getservice2_ajax') ?>',
                                                        data: {'secid': sectID, 'service1_name_id': service1_name_id},
                                                        success: function (res) {
                                                            var data = JSON.parse(res);
                                                            $("#service2_name_id").append("<option>--Select--</option>");
                                                            for (var i = 0; i < data.length; i++) {
                                                                $("#service2_name_id").append("<option value=" + data[i].id + ">" + data[i].service_name + "</option>");
                                                            }
                                                        },
                                                    });
                                                } else {
                                                    alert('Please Select');
                                                }
                                            });
                                        });


                                        $(document).ready(function () {
                                            //group add limit
                                            var maxGroup = 6;
                                            //add more fields group
                                            $(".addMore").click(function () {
                                                if ($('body').find('.fieldGroup').length < maxGroup) {
                                                    var fieldHTML = '<div class="form-group fieldGroup col-md-12 ">' + $(".fieldGroupCopy").html() + '</div>';
                                                    $('body').find('.fieldGroup:last').after(fieldHTML);
                                                } else {
                                                    alert('Maximum ' + maxGroup + ' Lane are allowed.');
                                                }
                                            });

                                            //remove fields group
                                            $("body").on("click", ".remove", function () {
                                                $(this).parents(".fieldGroup").remove();
                                            });
                                        });
        </script>
    </body>
</html>

<style>
    table{
        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
        font-size: 14px;
    }
    .ms-options >ul >li{list-style-type:none;}
</style>

<script>
    $(document).ready(function () {
        $('#example').dataTable({
            "dom": 'Bfrtip',
            "buttons": {
                "dom": {
                    "button": {
                        "tag": "button",
                        "className": "waves-effect waves-light btn mrm"
                    }
                },
                "buttons": ['copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5']
            }
        });
    });

    function delcomptcom(delid) {
        if (confirm("Are You Sure Delete This ?") == true) {
            window.location = '?delc=' + delid;
        }
    }
    $('.lane_id').click(function () {
        if (confirm("Are you sure you want to remove")) {
            var id = $(this).attr('data-laneid');
            var url = '<?= base_url('company/ceg_laneRemove'); ?>';
            $.ajax({
                type: 'POST',
                url: url,
                data: {id: id},
                success: function (res) {
                    location.reload(1);
                },
            });
        } else {
            return false;
        }
    });

</script>