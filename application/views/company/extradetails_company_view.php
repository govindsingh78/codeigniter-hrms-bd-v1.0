<?php
error_reporting(0);
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<style>
#table_length {
    position: absolute;
    margin-left: 100px !important;
}
.table tbody tr td, .table tbody th td {
    vertical-align: middle;
    white-space: normal !important;
}
</style>

    <?php
    //$this->load->view('include/innerhead');
    $statusArr = array('1' => '<span style="color:green">Enable</span>', '0' => '<span style="color:red">Disable</span>');
    ?>


    <body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>

 


                    <div class="row clearfix">
                    <div class="col-lg-12">
                    <div class="card">

                    <div class="body">

                <!-- CONTENT PART STARTS -->

                     <?php if ($this->session->flashdata('msg')) { ?>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                        </div>
                    <?php } ?>

                    <?php if ($this->session->flashdata('errormsg')) { ?>
                        <div class="alert danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong><div id="msgs">Error ! </div></strong> <?= $this->session->flashdata('errormsg'); ?>
                        </div>
                    <?php } ?>




                       
                     
                        <h5><?= @$comp_single_rec[0]->company_name; ?>     <a href="<?= base_url('company/project_summary_of_company/' . @$comp_single_rec[0]->fld_id); ?>" class="btn btn-danger ml-2 pull-right" target="_blank"><i class="fa fa-eye-open"></i> Project Summary</a><a href="javascript:void(0)" style="cursor: pointer;" data-toggle="modal" data-target="#myModalcompt" class="btn btn-primary pull-right">Manage Extra Details</a>  <a href="javascript:void(0)" style="cursor: pointer;" data-toggle="modal" data-target="#basiccompt_edit" class="btn btn-info mr-2 pull-right"> Edit </a></h5>


                                    <div class="mt-4">

                                            <?php if (@$comp_single_rec[0]): ?>
                                                 
                                                    
                                                     <div class="responsive-table"> 
                                                        
                                                    <table  id="table" class="table table-striped display">
                                                         <tbody>
                                                            <tr>
                                                                <th>Name / Details</th>
                                                                <td ><?= @$comp_single_rec[0]->company_name; ?></td>
                                                           
                                                                <th>Full Address (HO.)</th>
                                                                <td ><?= @$comp_single_rec[0]->company_details; ?></td>
                                                            </tr>
                                                        
                                                        
                                                            <tr>
                                                                <th>Contact No :</th>
                                                                <td><?= @$comp_single_rec[0]->contact_no; ?></td>
                                                                <th>Email :</th>
                                                                <td><?= @$comp_single_rec[0]->email_id; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Ho. Country:</th>
                                                                <td><?= $this->mastermodel->countNameById($comp_single_rec[0]->country); ?>
                                                                </td>
                                                                <th>State:</th>
                                                                <td><?= $this->mastermodel->GetStateNameById($comp_single_rec[0]->state); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Website :</th>
                                                                <td>
                                                                    <?php
                                                                    //" . $comp_single_rec[0]->company_website . "

                                                                    ?>
                                                                    <?= ($comp_single_rec[0]->company_website != null) ? "<a  href='javascript:void(0)'>" . $comp_single_rec[0]->company_website . "</a>" : ''; ?>
                                                                </td>
                                                                <th>Operating Countries:</th>
                                                                <td><?= $this->mastermodel->GetOperatingC($comp_single_rec[0]->operating_countries); ?></td>
                                                            </tr>
                                                            <tr>  
                                                                <th>Sector :</th>
                                                                <td><?= $this->mastermodel->GetSectNameByIdC($comp_single_rec[0]->com_sector);  // echo $this->mastermodel->GetSectNameById($comp_single_rec[0]->com_sector);                                            ?></td>

                                                                <th>Field / Company's Nature :</th>
                                                                <td><?= $this->mastermodel->GetFieldNameC($comp_single_rec[0]->comp_field_id); ?></td>
                                                            </tr>
                                                        </tbody>   
                                                    </table>

                                                </div>
                                                
                                            <?php endif; 




                                            ?>


                                       


                                         <?php if (@$comp_single_rec[0]): ?>
                                                <hr>

                                               
                                                    <form enctype="multipart/form-data" method="post" action="<?= base_url('company/updcompany_brochure'); ?>" id="updformid" >
                                                         
                                                            <div class="col-md-12 p-2 highlightMenu" data-toggle="collapse" data-parent="#accordion" href="#collapse1" >
                                                                <div class="well well-sm green btn">Add Additional Details of Company <i class="fa fa-eye-open pull-right"></i></div>  
                                                            </div>

                                                            <?php if (@$comp_single_rec[0]->brochure == ''): ?>

                                                                 <div class="form-group col-md-12 highlight mt-4" >
                                                                <label for="sel1">Upload Company Brochure : <span class="required">*</span></label>
                                                                  <input type="file" class="form-control well well-sm green btn" title="Company Brochure" id="comp_brochure_file" name="comp_brochure_file"> 
                                                                   
                                                                </div> 
                                                                <div class="col-md-12">
                                                                    <span style="color:red" id="uploaderror2"> </span>
                                                                </div>
                                                            <?php else: ?>
                                                                <div class="col-md-12 highlightMenu highlight mt-2">
                                                                <span>View Company Brochure
                                                                    <a target="_blank" href="<?= HOSTNAME . "uploads/companybrochure/" . $comp_single_rec[0]->brochure; ?>" ><i class="glyphicon glyphicon-download pull-right"></i> </a></span>
                                                                </div>
                                                            <?php endif; ?>
                                                        
                                                        <input type="hidden" name="main_compn_id" value="<?= @$comp_single_rec[0]->fld_id; ?>">
                                                    </form>


                                                    <div id="collapse1" class="panel-collapse collapse highlight mt-4">
                                                        <form method="post" action="<?= base_url('company/savecompany_extra'); ?>" >
                                                            <div class="form-group " >
                                                                <label for="sel1">Name : <span class="required">*</span></label>
                                                                <input type="text" value="<?= @$comp_single_rec[0]->company_name; ?>" class="form-control" name="comp_name" id="comp_name">   
                                                            </div>
                                                            <div class="form-group" >
                                                                <label for="sel1">Contact Person Name : <span class="required">*</span></label>
                                                                <input type="text"  class="form-control" name="cont_person_name" id="cont_person_name">   
                                                            </div>

                                                            <div class="form-group" >
                                                                <label for="sel1"> Designation : </label>
                                                                <input type="text" class="form-control" name="cont_person_designation" id="cont_person_name">   
                                                            </div>
                                                            <div class="form-group" >
                                                                <label for="sel1">Contact No : </label>
                                                                <input type="text"  class="form-control" name="cont_person_no" id="cont_person_no">   
                                                            </div>

                                                            <div class="form-group" >
                                                                <label for="sel1">Email : </label>
                                                                <input type="email"  class="form-control" name="cont_person_email" id="cont_person_email"> 
                                                            </div>

                                                            <div class="form-group" >
                                                                <label for="sel1">Sector : <span class="required">*</span></label>
                                                                <?php
                                                                $parentSect = explode(",", $comp_single_rec[0]->com_sector);
                                                                ?>
                                                                <select required class="form-control" name="sector_name" id="sector_name">
                                                                    <option value="all">All</option>
                                                                    <?php
                                                                    if ($parentSect):
                                                                        foreach ($parentSect as $secrow) {
                                                                            ?>
                                                                            <option value="<?= $secrow; ?>"><?= $this->mastermodel->GetSectNameByIdC($secrow); ?></option>
                                                                        <?php } endif; ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group" >
                                                                <label for="sel1">Department : <span class="required"></span></label>
                                                                <input type="text" class="form-control" name="department_name" id="department_name">   
                                                            </div>
                                                            <div class="form-group" >
                                                                <label for="sel1">Full Address : <span class="required"></span></label>
                                                                <input type="text" class="form-control" name="full_address" id="full_address" value="<?= @$comp_single_rec[0]->company_details; ?>">   
                                                            </div>
                                                            <div class="form-group" >
                                                                <label for="sel1">Ho / Ro : <span class="required">*</span></label>
                                                                <select required class="form-control" name="ho_ro" id="ho_ro">
                                                                    <option value="ho"> Ho </option>
                                                                    <option value="ro"> Ro </option>
                                                                    <option value="other"> Other </option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group" >
                                                                <label for="sel1">Refrance: </label>
                                                                <input type="text" class="form-control" name="refrance" id="comp_name">   
                                                            </div>
                                                            <div class="form-group" >
                                                                <label for="sel1">Description : </label>
                                                                <input type="text" class="form-control" name="detail_desc" id="comp_name">   
                                                            </div>
                                                            <div class="form-group" >
                                                                <label for="sel1"> <br><br> </label>
                                                                <input type="hidden" name="main_comp_id" id="main_comp_id" value="<?= @$comp_single_rec[0]->fld_id; ?>">
                                                                <input type="submit" class="btn btn-info pull-right ">   
                                                            </div>
                                                        </form>
                                                    </div>
                                               
                                            <?php endif; ?>



<div class="modal fade" id="extcompt_edit" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ho Full Address Edit / View </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    
                </div>
                <div class="modal-body">
                    <form method="post" action="" id="editExtraDetail">
                        <div class="row">
                        <div class="form-group col-md-4" >
                            <label for="sel1">Company Name : <span class="required">*</span></label>
                            <input type="text" required class="form-control" value="<?= @$comp_single_rec[0]->company_name; ?>" name="comp_name" id="comp_nameUPD">   
                        </div>
                        <div class="form-group col-md-4" >
                            <label for="sel1">Con. Person Name : <span class="required">*</span></label>
                            <input type="text" required class="form-control" name="cont_person_name" id="cont_person_nameUPD">   
                        </div>

                        <div class="form-group col-md-4" >
                            <label for="sel1">Contact Person No : <span class="required">*</span></label>
                            <input type="text" required class="form-control" name="cont_person_no" id="cont_person_noUPD">   
                        </div>

                        <div class="form-group col-md-4" >
                            <label for="sel1">Email Id : <span></span></label>
                            <input type="email" class="form-control" name="cont_person_email" id="cont_person_emailUPD">   
                        </div>

                        <div class="form-group col-md-4" >
                            <label for="sel1">Designation : <span></span></label>
                            <input type="text" class="form-control" name="cont_person_designation" id="cont_person_designationUPD">   
                        </div>

                        <div class="form-group col-md-4" >
                            <label for="sel1">Sector : <span class="required">*</span></label>
                            <select required class="form-control" name="sector_name" id="sector_nameUPD">
                                <option value="all">All</option>
                                <?php
                                if ($parentSect):
                                    foreach ($parentSect as $secrow) {
                                        ?>
                                        <option value="<?= $secrow; ?>"><?= $this->mastermodel->GetSectNameByIdC($secrow); ?></option>
                                    <?php } endif; ?>
                            </select>
                            </select>
                        </div>
                        <div class="form-group col-md-4" >
                            <label for="sel1">Department : <span class="required"></span></label>
                            <input type="text" class="form-control" name="department_name" id="department_nameUPD">   
                        </div>
                        <div class="form-group col-md-4" >
                            <label for="sel1">Full Address : <span class="required">*</span></label>
                            <input type="text" required class="form-control" name="full_address" id="full_addressUPD">   
                        </div>
                        <div class="form-group col-md-4" >
                            <label for="sel1">Ho / Ro : <span class="required">*</span></label>
                            <select required class="form-control" name="ho_ro" id="ho_roUPD">
                                <option value="ho"> Ho </option>
                                <option value="ro"> Ro </option>
                                <option value="other"> Other </option>
                            </select>
                        </div>
                        <div class="form-group col-md-6" >
                            <label for="sel1">Reference: </label>
                            <input type="text" class="form-control" name="refrance" id="refranceUPD">   
                        </div>
                        <div class="form-group col-md-6" >
                            <label for="sel1">Description : </label>
                            <input type="text" class="form-control" name="detail_desc" id="detail_descUPD">   
                        </div>
                        <div class="form-group col-md-12" >
                            <label for="sel1"> <br> </label>
                            <input type="hidden" name="main_comp_id" id="main_comp_idUPD" value="">
                            <input type="hidden" name="edtid" id="edtidUPD" value="">

                            <a href="javascript:void(0)" class="btn btn-info pull-right" onclick="editExtraDetail()">Save</a>   
                        </div>



                    </div>
                    </form>  

                </div>
            </div>
        </div> 
         </div> 



<div class="modal fade" id="myModalcompt" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Select Company</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form name="frmm" id="frmm" method="post" action="<?= base_url('company/addcompany_extra'); ?>" enctype="">
    <div class="row">
        <div class="form-group required col-md-12">
            <label> Company </label>
            <select id="multiple" name="compnm" class="form-control" style="width:100%" >
                <?php
                if ($ComprecArr):
                    foreach ($ComprecArr as $rwdata) {
                        ?>
                        <option value="<?= $rwdata->fld_id; ?>"><?= $rwdata->company_name; ?></option>
                    <?php } endif; ?>
            </select>
        </div>
        <div class="form-group required col-md-12"> 
            <input type="submit" class="btn btn-info pull-right" value="Set">
        </div>
    </div>
</form>
</div>
</div>
</div>
</div>



                                            <hr>

                                            <?php
                                            if (@$comp_extraRec[0]):
                                               foreach ($comp_extraRec as $extrw) {
                                                    ?>
                                                    
                                                            <div class="row p-2">





                                                                 
                                                             
                                                                            

                                                                            <div class="col-md-6 highlightDashed" title="Company Name"><b>Company Name :</b> <span class="pull-right"><?= !empty($extrw->comp_name) ? $extrw->comp_name : '&nbsp;'; ?> </span></div>

 
                                                                             <div class="col-md-6 highlightDashed" title="Contact person Name"
                                                                            ><b>Contact Person Name :</b><span class="pull-right"> <?= !empty($extrw->cont_person_name) ? $extrw->cont_person_name : '&nbsp'; ?> </span></div>



                                                                           
                                                                             
                                                                             <div class="col-md-6 highlightDashed" title="Contact person No."
                                                                            ><b>Contact person No. :</b> <span class="pull-right"><?= !empty($extrw->cont_person_no) ? $extrw->cont_person_no : '&nbsp;'; ?> </span></div>


                                                                              <div class="col-md-6 highlightDashed" title="Email"
                                                                            ><b>Email :</b>  <span class="pull-right"><?= !empty($extrw->cont_person_email) ? $extrw->cont_person_email : '&nbsp;'; ?> </span></div>




                                                                              <div class="col-md-6 highlightDashed" title="Designation"
                                                                            ><b>Designation :</b>  <span class="pull-right"><?= !empty($extrw->cont_person_designation) ? $extrw->cont_person_designation : '&nbsp;'; ?> </span></div>




                                                                            <div class="col-md-6 highlightDashed" title="Full Address"
                                                                            ><b>Full Address :</b> <span class="pull-right"><?= !empty($extrw->full_address) ? $extrw->full_address : '&nbsp;'; ?> </span></div>
                                                                           

                                                                             <div class="col-md-6 highlightDashed" title="Sector"
                                                                            ><b>Sector :</b> <span class="pull-right"><?= $this->mastermodel->GetSectNameById($extrw->sector_name); ?></span> </div>

                                                                            <div class="col-md-6 highlightDashed" title="Department" 
                                                                            ><b>Department :</b>
                                                                                  
                                                                                <span class="pull-right"><?= !empty($extrw->department_name) ? $extrw->department_name : '&nbsp;'; ?> </span>
                                                                            </div>

                                                                           <div class="col-md-12 highlightDashed" title="Ho / Ro"
                                                                            ><b>Ho / Ro :</b> <span class="pull-right text-center"><?= !empty($extrw->ho_ro) ? $extrw->ho_ro : '&nbsp;'; ?> </span></div>
                                                                           
</div>
                                                                          
                                                                        <div class="commentsec">
                                                                            <small class="text-muted"> 
                                                                                Reference : <span class=" "><?= !empty($extrw->refrance) ? $extrw->refrance : '&nbsp;'; ?> </span> &nbsp;&nbsp;
                                                                                Details : <span class=" "><?= !empty($extrw->detail_desc) ? $extrw->detail_desc : '&nbsp;'; ?></span> &nbsp;&nbsp;
                                                                            <?= !empty($extrw->entry_date) ? $extrw->entry_date : '&nbsp;'; ?> </small>   <a href="javascript:void(0)" onclick="editpopup('<?= $extrw->fld_id; ?>')" class="btn btn-info pull-right" style="cursor: pointer;" data-toggle="modal" data-target="#extcompt_edit"  >
                                                                                <i title="Edit" class="glyphicon glyphicon-edit"></i> Edit
                                                                            </a>
                                                                        
                                                                    </div>
                                                        


                                                <?php } endif; ?>
                                        
                                    
                     






            <!-- Start footer wrapper -->
            
            <!-- End footer wrapper -->
        <style>
            .comany_font{color:black; padding-left: 4%;}
        </style>
        <link href="<?= FRONTASSETS; ?>fm.selectator.jquery.css" rel="stylesheet" type="text/css">
        <script src="<?= FRONTASSETS; ?>fm.selectator.jquery.js"></script> 
        <script type="text/javascript">
                                                                                $('#multiple,#com_sector').selectator({
                                                                                    showAllOptionsOnFocus: true,
                                                                                    searchFields: 'value text subtitle right'
                                                                                });
                                                                                $(document).ready(function () {
                                                                                    $("li#company").addClass('active');
                                                                                });
                                                                                //Validate..
                                                                                function editpopup(valfield) {
                                                                                    $("#actionid").val(valfield);
                                                                                    $.ajax({
                                                                                        type: 'POST',
                                                                                        dataType: "text",
                                                                                        url: "<?= base_url('company/ajax_getextcomp_byid'); ?>",
                                                                                        data: {'editId': valfield},
                                                                                        success: function (responData) {
                                                                                            var parsed = JSON.parse(responData);
                                                                                            var arr = [];
                                                                                            for (var x in parsed) {
                                                                                                arr.push(parsed[x]);
                                                                                            }
                                                                                            $("#comp_nameUPD").val(arr[2]);
                                                                                            $("#cont_person_nameUPD").val(arr[3]);
                                                                                            $("#cont_person_noUPD").val(arr[5]);
                                                                                            $("#cont_person_emailUPD").val(arr[6]);
                                                                                            $("#cont_person_designationUPD").val(arr[4]);
                                                                                            $("#sector_nameUPD").val(arr[7]);
                                                                                            $("#department_nameUPD").val(arr[8]);
                                                                                            $("#full_addressUPD").val(arr[9]);
                                                                                            $("#ho_roUPD").val(arr[10]);
                                                                                            $("#refranceUPD").val(arr[11]);
                                                                                            $("#detail_descUPD").val(arr[12]);
                                                                                            $("#edtidUPD").val(arr[0]);
                                                                                            $("#main_comp_idUPD").val(arr[1]);
                                                                                        },
                                                                                    });
                                                                                }
        </script>
    </body>


    <!-- Edit Section Of Company -->
    <div class="modal fade" id="basiccompt_edit" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title">Edit  / Update Basic Details</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    
                </div>




                <div class="modal-body">
                    <form method="post" action="" id="updateCompanyBasic">
                        <div class="row">
                        <div class="form-group col-md-6" >
                            <label for="sel1">Company Name : <span class="required">*</span></label>
                            <input type="text" required class="form-control" name="company_name" value="<?= @$comp_single_rec[0]->company_name; ?>" >  
                        </div>
                        <div class="form-group col-md-6" >
                            <label for="sel1">Contact No : <span class="required">*</span></label>
                            <input type="text" required class="form-control" name="contact_no" value="<?= @$comp_single_rec[0]->contact_no; ?>" >   
                        </div>

                        <div class="form-group col-md-6" >
                            <label for="sel1">Ho. Country : <span class="required"></span></label>
                            <select id="country" name="country" onchange="setstatebycid()" class="form-control">
                                <option value=""> -- Select Country -- </option>
                                <?php
                                if ($country_Arr):
                                    foreach ($country_Arr as $rowr) {
                                        ?>
                                        <option <?= (@$comp_single_rec[0]->country == $rowr->country_id) ? 'selected' : ''; ?> value="<?= $rowr->country_id; ?>"> <?= $rowr->country_name; ?></option>
                                    <?php } endif; ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="sel1">State : <span class="required">*</span></label>
                            <select id="state" name="state" class="form-control">
                                <?php
                                if ($stateArr):
                                    foreach ($stateArr as $rowrsec) {
                                        ?>
                                        <option <?= (@$comp_single_rec[0]->state == $rowrsec->state_id) ? 'selected' : ''; ?> value="<?= $rowrsec->state_id; ?>"> <?= $rowrsec->state_name; ?></option>
                                    <?php } endif; ?>
                            </select>
                        </div>

                    
                        <div class="form-group col-md-12" >
                            <label for="sel1">Website : <span class="required"></span></label>
                            <input type="url" class="form-control" name="company_website" value="<?= @$comp_single_rec[0]->company_website; ?>" >   
                        </div>

                        <div class="form-group col-md-6" >
                            <label for="sel1">Operating Country : <span class="required"></span></label> <br>
                            <?php
                            $arrOprtCountr = explode(",", @$comp_single_rec[0]->operating_countries);
                            ?>
                            <select id="oprtcountry" name="operating_countries[]" multiple class="form-control chosen-select">
                                <option value=""> -- Select Country -- </option>
                                <?php
                                if ($country_Arr):
                                    foreach ($country_Arr as $rowr) {
                                        ?>
                                        <option <?= (in_array($rowr->country_id, $arrOprtCountr)) ? 'selected' : ''; ?> value="<?= $rowr->country_id; ?>"> <?= $rowr->country_name; ?></option>
                                    <?php } endif; ?>
                            </select>
                        </div>


                        <?php
                        $arrOprtfield = explode(",", @$comp_single_rec[0]->comp_field_id);
                        ?>
                        <div class="form-group col-md-6" >
                            <label for="sel1">Field / Nature of Company : <span class="required">*</span></label>
                            <select multiple id="comp_field_id" name="comp_field_id[]" class="form-control chosen-select">
                                <?php
                                if ($comp_field_Arr):
                                    foreach ($comp_field_Arr as $secrwo) {
                                        ?>
                                        <option <?= (in_array($secrwo->fld_id, $arrOprtfield)) ? 'selected' : ''; ?> value="<?= $secrwo->fld_id; ?>"><?= $secrwo->field_name; ?></option>
                                    <?php } endif; ?>
                            </select>
                        </div>


                        <div class="form-group col-md-6" >
                            <label for="sel1">Full Address (HO.) : <span class="required">*</span></label>
                            <textarea class="form-control" name="company_details" rows="2" cols="5"><?= @$comp_single_rec[0]->company_details; ?></textarea>
                        </div>
                        <div class="form-group col-md-6" >
                            <label for="sel1">Full Address (RO.) : <span class="required">*</span></label>
                            <textarea class="form-control" name="full_address_ro" rows="2" cols="5"><?= @$comp_single_rec[0]->full_address_ro; ?></textarea>
                        </div>

                        <div class="form-group col-md-12" >
                            <label for="sel1">Email : <span class="required">*</span></label>
                            <input type="text" class="form-control" name="email_id" value="<?= @$comp_single_rec[0]->email_id; ?>">   
                        </div>
                        <?php $arrOprtsect = explode(",", @$comp_single_rec[0]->com_sector); ?>
                        <div class="form-group col-md-12" >
                            <label for="sel1">Sector : <span class="required">*</span></label>
                            <select multiple class="form-control chosen-select" name="com_sector[]" id="com_sector" >
                                <?php
                                if ($sectArr_rec):
                                    foreach ($sectArr_rec as $secrwo) {
                                        ?>
                                        <option <?= (in_array($secrwo->id, $arrOprtsect)) ? 'selected' : ''; ?> value="<?= $secrwo->id; ?>"><?= $secrwo->sector_name; ?></option>
                                    <?php } endif; ?>
                            </select>  
                        </div>

                        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen-sprite.png"></script>
                        <link rel="stylesheet" href="<?= ASSETS; ?>assets/docsupport/chosen.css">
                        <script src="<?= ASSETS; ?>assets/docsupport/chosen.jquery.js" type="text/javascript"></script>
                        <script src="<?= ASSETS; ?>assets/docsupport/init.js" type="text/javascript" charset="utf-8"></script>

                        <div class="form-group col-md-12" >
                            <label for="sel1"> <br> </label>
                            <input type="hidden" name="fld_id" value="<?= @$comp_single_rec[0]->fld_id; ?>">
                            <br>
                            <a href="javascript:void(0)" class="btn btn-info pull-right" onclick="updateCompanyBasic()">Save</a>   
                        </div>



                    </form>
                </div>
            </div>
        </div>
    </div>


    
    





  </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            <?php $this->load->view('admin/includes/footer'); ?>

    <script>
                                function setstatebycid() {
                                    var countryid = $("#country").val();
                                    $('#state').html('');
                                    $.ajax({
                                        url: '<?= base_url('company/ajax_state_getbycid') ?>',
                                        method: 'post',
                                        data: {country: countryid},
                                        dataType: 'json',
                                        success: function (response) {
                                            $.each(response, function (index, data) {
                                                $('#state').append('<option value="' + data['state_id'] + '">' + data['state_name'] + '</option>');
                                            });
                                        }
                                    });
                                }

                                $(document).ready(function () {
                                    $("#uploaderror2").hide();
                                    $('INPUT[type="file"]').change(function () {
                                        var ext = this.value.match(/\.(.+)$/)[1];
                                        switch (ext) {
                                            case 'doc':
                                            case 'DOC':
                                            case 'docx':
                                            case 'jpg':
                                            case 'JPG':
                                            case 'jpeg':
                                            case 'pdf':
                                            case 'PDF':
                                                $('#uploadButton').attr('disabled', false);
                                                $("#uploaderror2").hide();
                                                break;
                                            default:
                                                $("#uploaderror2").show();
                                                $("#uploaderror2").html('This is not an allowed file type.');
                                                this.value = '';
                                        }
                                    });
                                    $('INPUT[type="file"]').bind('change', function () {
                                        var imgfilesize = (this.files[0].size / 1001376);
                                        if (imgfilesize > 17700) {
                                            $("#uploaderror2").show();
                                            $("#uploaderror2").html('Reducing File Size Max Uploads (99 MB). ');
                                            this.value = '';
                                        } else {
                                            $("#uploaderror2").hide();
                                            $("#updformid").submit()
                                        }
                                    });
                                });





                               
                                
                                



                                function updateCompanyBasic() {
                                var data = $("#updateCompanyBasic").serialize();
                                $.ajax({
                                type: 'POST',
                                url: "<?= base_url('company/updatecompany_basic'); ?>",
                                data: data,
                                dataType: "json",
                                success: function(responData) {
                                 
                                toastr.success(responData.msg, 'Message', {
                                timeOut: 5000
                                });
                                 
                                $('#basiccompt_edit').modal('toggle');

                                },
                                });
                                } 



                                function editExtraDetail() {
                                var data = $("#editExtraDetail").serialize();
                                $.ajax({
                                type: 'POST',
                                url: "<?= base_url('company/updatecompany_extra'); ?>",
                                data: data,
                                dataType: "json",
                                success: function(responData) {
                                 
                                toastr.success(responData.msg, 'Message', {
                                timeOut: 5000
                                });
                                 
                                $('#extcompt_edit').modal('toggle');

                                },
                                });
                                } 

     



    </script>


    <style>
        table{
            font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
            font-size: 14px;
        }

        .chosen-container, .chosen-container-active {
            width: 100% !important;
        }

    </style>
