<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        <div class="wrapper">
            <?php $this->load->view('include/sidebar'); ?>
			<div id="content" class="col-lg-10 col-sm-10">
                    <!-- content starts -->
                    <div>
                        <ul class="breadcrumb">
                            <li> <a href="<?= base_url(); ?>">Home</a></li>
                            <li>
                                <a href="#"> Websites </a>
                            </li>
							<li>
                                <a href="<?= base_url('company/websiteadd') ?>">
                                    <button type="button" class="btn btn-info">Add Website</button>
                                </a> 
                            </li>
                        </ul>
                    </div>
                    <div class="row">
                        <?php if ($this->session->flashdata('msg')) { ?>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                            </div>
                        <?php } ?>
                        <!-- open Phase Section-->
                        <div class="box-inner">
                            <div class="box-content">

                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="colvis"></div>
                                            </div>
                                        </div>


                                        <table id="table" class="display" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Sr. No</th>
                                                    <th>Website</th>
                                                    <th>Department</th>
                                                    <th>EPROC</th>
                                                    <th>UserID</th>
                                                    <th>Password</th>
                                                    <th>Country</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Sr. No</th>
                                                    <th>Website</th>
                                                    <th>Department</th>
                                                    <th>EPROC</th>
                                                    <th>UserID</th>
                                                    <th>Password</th>
                                                    <th>Country</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                            <hr>
                                        </table>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Close phase Section-->
                </div>
            
        </div>	

        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>

        <hr>
        <!-- BootStrap Model For Comments.. -->
        <?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->
        <?php $this->load->view('include/footer'); ?>
         <?php $this->load->view('include/datatablejs'); ?>
		<style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            #chatbox {padding: 15px;overflow: scroll;height: 300px;}
        </style>
        <script type="text/javascript">

            var table;
            $(document).ready(function () {
                //datatables
                table = $('#table').DataTable({
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.
                    "ajax": {
                        "url": "<?php echo site_url('company/websiteListAll') ?>",
                        "type": "POST",
                        "data": function (data) {
                            data.project_name = $('#company_name').val();
                            data.sectorinput = $('#sectorinput').val();
                        },
                    },
                    "dom": 'lBfrtip',
                    "buttons": [{extend: 'collection', text: 'Export', buttons: ['copy', 'excel', 'csv', 'pdf', 'print']}],
                    "columnDefs": [{"targets": [0], "orderable": false,
                        },
                    ],
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                });
                var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
                $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
                $('#btn-filter').click(function () { //button filter event click
                    table.ajax.reload();  //just reload table
                });
                $('#btn-reset').click(function () { //button reset event click
                    $('#form-filter')[0].reset();
                    table.ajax.reload();  //just reload table
                });
            });

            $(document).ready(function () {
                $("#tenderdetail").addClass('active');
            });

            function showpass(pwdd, dvfldid) {
               // alert(''+pwdd);
               // alert(''+dvfldid);
                $('#dv' + dvfldid).html(pwdd);
            }

        </script>

    </div>
</body>
</html>
