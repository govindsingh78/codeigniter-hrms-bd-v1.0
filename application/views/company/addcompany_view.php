<html lang="en">
    <?php $this->load->view('include/innerhead.php'); ?>
    <?php error_reporting(0); ?>
    <body>
        <?php $this->load->view('include/tabbar.php'); ?>
        <link rel="stylesheet" href="<?= FRONTASSETS; ?>style/format.css">
        <div class="wrapper">
            <?php $this->load->view('include/sidebar.php') ?>
            <div id="content" class="col-lg-10 col-sm-10">
                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success ! </strong> Tender Added Success !
                    </div>
                <?php } ?>
                <div>
                    <ul class="breadcrumb">
                        <li> <a href="<?= base_url(); ?>">Home</a></li>
                        <li class="">
                            <a href="#"><?= $title; ?> </a>
                        </li>
                        <li class="box-title pull-right">
                            <a href="<?= base_url('company/showlist'); ?>"> View All </a>
                        </li>

                    </ul>
                </div>
                <div class="row">
                    <?php if ($this->session->flashdata('success_msg')): ?>
                        <div class="alert alert-block alert-success">
                            <button type="button" class="close" data-dismiss="alert">
                                <i class="ace-icon fa fa-times"></i>
                            </button>
                            <i class="ace-icon fa fa-check green"></i>
                            <?= $this->session->flashdata('success_msg'); ?>
                            <strong class="green"></strong>
                        </div>
                    <?php endif; ?>
                    <?php if ($this->session->flashdata('error_msg')): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Error ! </strong> <?= $this->session->flashdata('error_msg'); ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?= thisurl(); ?>" method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data mvc">
                        <div class="box col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tender-name">Company Name : <span class="required">*</span>
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="text" id="tendername" name="company_name" class="form-control col-md-4 col-xs-12 required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-6" for="tender-name"> </label>
                                <div class="col-md-4 col-sm-4 col-xs-4"><b>HO. Country : </b></div>
                                <div class="col-md-4 col-sm-4 col-xs-4"><b>State : </b></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-6" for="tender-name"> </label>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <select id="country" name="country" onchange="setstatebycid()" class="form-control col-md-4 col-xs-12">
                                        <option value=""> -- Select Country -- </option>
                                        <?php
                                        if ($country_Arr):
                                            foreach ($country_Arr as $rowr) {
                                                ?>
                                                <option value="<?= $rowr->country_id; ?>"> <?= $rowr->country_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <select id="state" name="state" class="form-control col-md-4 col-xs-12">
                                        <option value=""> -- Select State -- </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Tender Details">Full Address (HO.) : </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <textarea cols="117" id="edi" name="company_details" rows="3" required></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-6" for="tender-name"> </label>
                                <div class="col-md-4 col-sm-4 col-xs-4"><b>Contact No : </b></div>
                                <div class="col-md-4 col-sm-4 col-xs-4"><b>Email Id : </b></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-6" for="tender-name"> </label>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <input type="text" title="Multiple Record Separate With Comma" id="contact_no" name="contact_no" class="form-control col-md-4 col-xs-12 ">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <input type="text" title="Multiple Record Separate With Comma" id="email_id" name="email_id" class="form-control col-md-4 col-xs-12 ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-6" for="tender-name"> </label>
                                <div class="col-md-4 col-sm-6 col-xs-6"><b>Fax No : </b></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-3" for="tender-name"> </label>
                                <div class="col-md-8 col-sm-8 col-xs-8">
                                    <input type="text" id="fax_no" name="fax_no" class="form-control col-md-4 col-xs-12 ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-6" for="tender-name"> </label>
                                <div class="col-md-4 col-sm-4 col-xs-4"><b>Company Website : </b></div>
                                <div class="col-md-4 col-sm-4 col-xs-4"><b>Operating Countries : </b></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-6" for="tender-name"> </label>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <input type="url" id="urlv" name="company_website" class="form-control col-md-4 col-xs-12 ">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <select autocomplete="off" id="oprt_countryid" name="operating_countries[]" multiple="" class="form-control col-md-4 col-xs-12">
                                        <option value=""> -- Select Country -- </option>
                                        <?php
                                        if ($country_Arr):
                                            foreach ($country_Arr as $rowr) {
                                                ?>
                                                <option value="<?= $rowr->country_id; ?>"> <?= $rowr->country_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-6" for="tender-name"> </label>
                                <div class="col-md-4 col-sm-4 col-xs-4"><b>Sector : </b> &nbsp;&nbsp;&nbsp; <i data-toggle="modal" data-target="#modeladdsector" style="cursor:pointer; color:green" title="Add New Sector" class="glyphicon glyphicon-plus"></i> </div>
                                <label class="control-label col-md-3 col-sm-3 col-xs-6" for="tender-name"> </label>
                                <div class="col-md-4 col-sm-4 col-xs-4"><b>Field / Nature of Company : </b> &nbsp;&nbsp;&nbsp; <i data-toggle="modal" data-target="#modeladdfield" style="cursor:pointer; color:green" title="Add New Field" class="glyphicon glyphicon-plus"></i> </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-6" for="tender-name"> </label>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <select class="form-control col-md-4 col-xs-12" multiple="multiple" id="com_sector" name="com_sector[]">
                                        <?php
                                        if ($sectArr_rec):
                                            foreach ($sectArr_rec as $secrwo) {
                                                ?>
                                                <option value="<?= $secrwo->id; ?>"><?= $secrwo->sector_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>
                                <!-- Nature of Company -->
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <select class="form-control col-md-4 col-xs-12" multiple="multiple" id="comp_field_id" name="comp_field_id[]">
                                        <?php
                                        if ($comp_field_Arr):
                                            foreach ($comp_field_Arr as $secrwo) {
                                                ?>
                                                <option value="<?= $secrwo->fld_id; ?>"><?= $secrwo->field_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <input class="btn btn-primary" name="cancel" id="cancel" value="Cancel" type="reset"/>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
                </div>
            </div>
            <hr>
            <script>
                function setstatebycid() {
                    var countryid = $("#country").val();
                    $('#state').empty('');
                    $.ajax({
                        url: '<?= base_url('company/ajax_state_getbycid') ?>',
                        method: 'post',
                        data: {country: countryid},
                        dataType: 'json',
                        success: function (response) {
                            $('#state').val();
                            $.each(response, function (index, data) {
                                $('#state').append('<option value="' + data['state_id'] + '">' + data['state_name'] + '</option>');
                            });
                        }
                    });
                }
            </script>
            <!-- Model For Sector Add -->
            <div class="modal fade" id="modeladdsector" role="dialog">
                <div class="modal-dialog">
                    <form name="othrfrmres" id="othrfrmres" method="post" enctype="multipart/form-data" action="<?= base_url('company/addsector'); ?>">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 id="emplnname" class="modal-title">Add New Sector</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="control-label" for="usr">Sector Name </label>
                                    <input type="text" required id="sector_name" name="sector_name" class="form-control" >
                                </div>
                                <div class="form-group required">
                                    <input type="submit"  class="btn-primary" value="Add & Save" >
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Model For Field Add -->
            <div class="modal fade" id="modeladdfield" role="dialog">
                <div class="modal-dialog">
                    <form name="othrfrmres" id="othrfrmres" method="post" enctype="multipart/form-data" action="<?= base_url('company/addfield_name'); ?>">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 id="emplnname" class="modal-title">Add Field of Company</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="control-label" for="usr">Field / Nature of Company </label>
                                    <input type="text" required id="field_name" name="field_name" class="form-control" >
                                </div>
                                <div class="form-group required">
                                    <input type="submit"  class="btn-primary" value="Add & Save" >
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php $this->load->view('include/commenthtml.php'); ?>
        <?php $this->load->view('include/footer.php'); ?>
        <script src="<?= FRONTASSETS; ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?= FRONTASSETS; ?>js/jquery.autogrow-textarea.js"></script>
        <link href="<?= FRONTASSETS; ?>fm.selectator.jquery.css" rel="stylesheet" type="text/css">
        <script src="<?= FRONTASSETS; ?>fm.selectator.jquery.js"></script>
        <script type="text/javascript">
                $('#oprt_countryid,#com_sector,#comp_field_id').selectator({
                    showAllOptionsOnFocus: true,
                    searchFields: 'value text subtitle right'
                });

                $(document).ready(function () {
                    $("tenderdetailsing").addClass('active');
                });
        </script>
    </body>
    <style>
        span.required {
            color: red;
        }
    </style>