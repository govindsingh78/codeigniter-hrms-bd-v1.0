<html lang="en">
<?php error_reporting(0); //error_reporting(0); ?>
<?php $this->load->view('include/innerhead'); ?>
<body>
<!-- topbar starts -->
<?php $this->load->view('include/tabbar'); ?>
<!-- topbar ends -->

<link href="<?= FRONTASSETS; ?>css/jquery.multiselect.css" rel="stylesheet"></link>
<div class="wrapper">
<?php $this->load->view('include/sidebar'); ?>
<div id="content" class="col-lg-10 col-sm-10">
<!-- content starts -->
<div>
<ul class="breadcrumb">
<li>
<a href="<?= base_url(); ?>">Home</a>
</li>
<li>
<a href="#"><?= $title; ?> </a>
</li>
<li>
<a href="<?= base_url('company/addcompany_extra'); ?>">
<button type="button" class="btn btn-info">Company Branch Or Extra</button>
</a>
</li>
<li>
<a href="<?= base_url('company/add'); ?>">
<button type="button" class="btn btn-info">Add Company</button>
</a> 
</li>
</ul>
</div>
<div class="row">
<?php if ($this->session->flashdata('msg')) { ?>
<div class="alert alert-success alert-dismissable">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
</div>
<?php } ?>

<!-- open Phase Section-->
<div class="box-inner">
<div class="box-content">
<div class="tab-content">
<div id="home" class="tab-pane fade in active">
<form id="form-filter" class="form-horizontal">
<div class="row">

<div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
<label class="col-sm-12" for="selectError2">Sector:</label>
<div class="col-sm-12">
<select id="sectorinput" name="sectorinput[]" multiple class="form-control col-md-4 col-xs-12">
<?php
if ($sectArr_rec):
foreach ($sectArr_rec as $rowr) {
?>
<option value="<?= $rowr->id; ?>"> <?= $rowr->sector_name; ?></option>
<?php } endif; ?>
</select>
</div>
</div>

<div class="col-sm-2" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
<label for="Company Nature" class="col-sm-12">Comp. Nature </label>
<div class="col-sm-12">
<select id="company_name_nature" name="company_name_nature" class="form-control">
<option value="" selected="selected">--All--</option>
<?php foreach ($comp_field_Arr as $rowcomp) { ?>
<option value="<?= $rowcomp->fld_id; ?>">
<?= ucfirst($rowcomp->field_name); ?>
</option>
<?php } ?>
</select>
</div>
</div>

<div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
<label for="Country" class="col-sm-12">HO Country</label>
<div class="col-sm-12">
<select name="country_name" id="country_name" class="form-control">
<option value="" selected="selected">--All--</option>
<?php foreach ($country_Arr as $rowcoun) { ?>
<option value="<?= $rowcoun->country_id; ?>">
<?= ucfirst($rowcoun->country_name); ?>
</option>
<?php } ?>
</select>
</div>
</div>

<div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
<label for="Country" class="col-sm-12">Operating Country</label>
<div class="col-sm-12">
<select name="opcountry_name" id="opcountry_name" class="form-control">
<option value="" selected="selected">--All--</option>
<?php foreach ($country_Arr as $rowcoun) { ?>
<option value="<?= $rowcoun->country_id; ?>">
<?= ucfirst($rowcoun->country_name); ?>
</option>
<?php } ?>
</select>
</div>
</div>

</div>
<div class="row">
<div class="col-sm-3" style="margin: 1%;">
<div class="col-sm-12">
<button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
<button type="button" id="btn-reset" class="btn btn-default">Reset</button>
</div>
</div>
</div>
</form>

<div class="row">
<div class="col-md-12">
<a href="<?= base_url('company/addcompany_extra?compnm=74');?>" class="btn btn-info btn-sm">Cegindia</a>
<div id="colvis"></div>
</div>
</div>


<table id="example" class="display" cellspacing="0" width="100%">
<thead>
<tr>
<th>Sr. No</th>
<th>Company Name</th>
<th>Nature of comp.</th>
<th style="width:10px">Website</th>
<th>Sector</th>
<th>HO Country</th>
<th>Email</th>
<th>Contact</th>
<th>Status</th>
</tr>
</thead>
<tbody>
</tbody>
<tfoot>
<tr>
<th>Sr. No</th>
<th>Company Name</th>
<th>Nature of comp.</th>
<th style="width:10px">Website</th>
<th>Sector</th>
<th>HO Country</th>
<th>Email</th>
<th>Contact</th>
<th>Status</th>
</tr>
</tfoot>
<hr>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<div class="row">
<div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
</div>
</div>

<hr>
<!-- BootStrap Model For Comments.. -->
<?php $this->load->view('include/commenthtml'); ?>
<!-- close Model -->
<?php $this->load->view('include/footer'); ?>
<script src="<?= FRONTASSETS; ?>js/jquery.multiselect.js"></script>
<?php $this->load->view('include/datatablejs'); ?>

        
<style>
#table_length{margin-left:20px;}
#table_filter{margin-right:2%;} 
#chatbox {padding: 15px;overflow: scroll;height: 300px;}
.ms-options >ul >li{list-style-type:none;}
</style>

<script type="text/javascript">
var table;
$(document).ready(function () {
//datatables
table = $('#example').DataTable({
"processing": true, //Feature control the processing indicator.
"serverSide": true, //Feature control DataTables' server-side processing mode.
"order": [], //Initial no order.
"ajax": {
"url": "<?php echo site_url('company/companyListAll') ?>",
"type": "POST",
"data": function (data) {
data.sectorinput = $('#sectorinput').val();
data.company_name_nature = $('#company_name_nature').val();
data.country_name = $('#country_name').val();
data.opcountry_name = $('#opcountry_name').val();
// console.log(data);
},
},
"dom": 'lBfrtip',
"buttons": [{extend: 'collection', text: 'Export', buttons: ['copy', 'excel', 'csv', 'pdf', 'print']}],
"columnDefs": [{"targets": [0], "orderable": false,
},
],
"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
});

var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
$('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
$('#btn-filter').click(function () { //button filter event click
table.ajax.reload();  //just reload table
});
$('#btn-reset').click(function () { //button reset event click
$('#form-filter')[0].reset();
table.ajax.reload();  //just reload table
});
});
</script>
<script>
$(document).ready(function () {
$("li#company").addClass('active');
});
$(document).ready(function () {
$("#checkAll").click(function () {
$('input:checkbox').not(this).prop('checked', this.checked);
});
});
$('#sectorinput').multiselect({
columns: 1,
placeholder: 'Select Sector',
search: true
});
</script>
</div>
</body>
</html>
