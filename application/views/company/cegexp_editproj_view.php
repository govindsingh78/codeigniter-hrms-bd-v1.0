<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
//error_reporting(E_ALL);
?>
<link rel="stylesheet" href="<?= FRONTASSETS; ?>css/editable-select.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.18.10/slimselect.min.css" rel="stylesheet">

<style>
    #table_length,
    #table1_length,
    #table2_length,
    #table3_length {
        position: absolute;
        margin-left: 100px !important;
    }

    .table tbody tr td,
    .table tbody th td {
        vertical-align: middle;
        white-space: normal !important;
    }

    ul.nav.nav-tabs-new {
        float: right !important;
        margin: auto;
    }

    .dropdown-box {
        width: 100% !important;
    }

    .dropdown-box>.inputbox {
        width: 98% !important;

    }

    /* .select-editable {
     position:relative;
     background-color:white;
     border:solid grey 1px;
     width:120px;
     height:18px;
 } */

    .select-editable {
        position: relative;
        background-color: white;
        border: 1px solid #ddd;
        width: 100%;
        /* height: 18px; */
        border-radius: 5px;
        /* padding: 24px; */
        height: 40px;
        line-height: 40px;
        padding: 0px;
    }

    /* .select-editable select {
     position:absolute;
     top:0px;
     left:0px;
     font-size:14px;
     border:none;
     width:120px;
     margin:0;
 } */

    .select-editable select {
        position: absolute;
        top: 0px;
        left: 0;
        font-size: 14px;
        border: none;
        width: 98%;
        margin: 0;
        padding: 10px;
    }

    /* .select-editable input {
     position:absolute;
     top:0px;
     left:0px;
     width:100px;
     padding:1px;
     font-size:12px;
     border:none;
 } */

    .select-editable input {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 96%;
        padding: 10px;
        font-size: 12px;
        border: none;
    }

    .select-editable select:focus,
    .select-editable input:focus {
        outline: none;
    }
 
    .card {
    -moz-box-direction: normal;
    -moz-box-orient: vertical;
    background-color: #fff;
    border-radius: 0.25rem;
    display: flex;
    flex-direction: column;
    position: relative;
    margin-bottom:1px;
    border:none;
}
.card-header:first-child {
    border-radius: 0;
}
.card-header {
    background-color: #f7f7f9;
    margin-bottom: 0;
    padding: 10px 1.25rem;
    border:none;
    
}

.card-header h3 {
    margin: 5px 0 5px;
    font-size: 18px;
}
.card-header a i{
    float:left;
    font-size:20px;
    padding:5px 0;
    margin:0 25px 0 0px;
    color:#195C9D;
}
.card-header i{
    float:right;        
    font-size:30px;
    width:1%;
    margin-top:8px;
    margin-right:10px;
}
.card-header a{
    width:97%;
    float:left;
    color:#565656;
}
.card-header p{
    margin:0;
}

 
.card-block {
    -moz-box-flex: 1;
    flex: 1 1 auto;
    padding: 20px;
    color:#232323;
    box-shadow:inset 0px 4px 5px rgba(0,0,0,0.1);
    border-top:1px soild #000;
    border-radius:0;
}
</style>
<?php
     
    $statusArr = array('won' => '1', 'loose' => '2', 'awaiting' => '0', 'cancel' => '3');
    ?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>


             
       
                    
                    







                    

                   				
                   
  



                    <div class="row clearfix">
                    <div class="col-lg-12">
                    <div class="card">          
                             


 
                            <div class="body">
                            <?php if ($this->session->flashdata('msg')) { ?>
                                <div class="alert alert-success alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong><div id="msgs">Success! </strong> <?= $this->session->flashdata('msg'); ?></div>
                                 </div>
                            <?php } ?> 

                                <div class="col-lg-12 col-md-12">

                                <!-- <a href="javacsript:void(0)" data-toggle="modal" data-target="#mypsdModel" class="btn btn-warning pull-left p-2 mr-2"><i class="fa fa-eye" aria-hidden="true"></i> &nbsp;&nbsp;PDS View</a> -->
                                        <a href="javacsript:void(0)" data-toggle="modal" data-target="#myModalprojstatus" class="btn btn-success  pr-3 pl-3 p-2  pull-left mr-2"><i class="fa fa-hourglass-end" aria-hidden="true"></i> &nbsp;&nbsp;Project Status</a>
                                        <!-- <a href="<?= base_url('company/cegexp'); ?>" class="btn btn-danger  pull-left p-2 mr-2"><i class="fa fa-folder" aria-hidden="true"></i>&nbsp;&nbsp; View All Projects</a> -->

                                    <ul class="nav nav-tabs-new">
                                        <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#profile-tab">Details</a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#info-tab">Team</a></li>
                                      

                                    </ul>
                                </div>

                                 


                             
                           

                                <div class="tab-content">

                                <div class="tab-pane show active" id="profile-tab">
                                    
                              
     
                     

                   
                    <div class="bd-example" data-example-id="">
                    <div id="accordion" role="tablist" aria-multiselectable="true">

                    <div class="card pt-3">


                    <!-- Start -->
                    <div class="card-header  mt-2" role="tab" id="projectDetail">
                    <div class="mb-0" style="display: flex">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                   
                    <h3>Project Details </h3>
                    
                    </a>
                    <!-- <a data-toggle="modal" data-target="#myModalbasic" class="collapsed pull-right" title="Add / Edit Project Details"  style="width: auto"> <i class="fa fa-pencil" aria-hidden="true"></i> </a> -->
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseOne" class="collapsed pull-right" style="width: auto">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    </div>
                    </div>


                    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="projectDetail" aria-expanded="false">
                    <div class="card-block">
                     


 <form name="projectBasicsForm" id="projectBasicsForm" method="post" action="" enctype="">
                            <div class="row">

<div class="form-group required col-md-12">
                                    <div class="mt-2">
                                   
                                    <h4 class="modal-title"> Basic Detail  
                                    <!-- <button type="button" class="close pull-right" data-dismiss="modal">&times;</button></h4> -->

                                    <hr style="border: 1px solid" />
                                    </div>

                                    </div>
<!-- Start Section Copied -->
  
                               
                                    <div class="form-group col-md-12">
                                        <label class="control-label" for="inputSuccess1"> Project Name </label>
                                        <textarea rows="3" name="project_name" required cols="27" class="form-control"><?= $bddetail->TenderDetails; ?></textarea>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label> Submission Date</label>
                                        <input type="date" class="form-control" id="subm_date" name="subm_date" value="">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label> Bid Validity Date</label>
                                        <input type="date" class="form-control" id="expr_date" name="expr_date" value="<?= $bddetail->bid_validity_date; ?>">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>Location</label>
                                        <input type="text" class="form-control" id="location" name="location" value="<?= $bddetail->Location; ?>">
                                    </div>


                                           



                                    <div class="form-group col-md-6">
                                        <label>Client</label>

                                        <input type="hidden" id="mTextHid1" name="mTextHid1" value="" />

                                        <div class="select-editable">
                                            <input type="hidden" id="mVal1" name="client_name" value="" class="form-control" />
                                            <select id="list1" onchange="editableFunc(this.value)">
                                                
                                                <?php
                                                if (isset($clientName)) {
                                                    foreach ($clientName as $key => $list) {
                                                ?>
                                                        <option value="<?php echo $list->id; ?>"><?php echo $list->client_name; ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <input type="text" id="mText1" name="client_text" value="" class="form-control" />


                                        </div>


                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Funding</label>
                                        <input type="hidden" id="mTextHid2" name="mTextHid2" value="" />
                                        <div class="select-editable">
                                            <input type="hidden" id="mVal2" name="funding_org" value="" class="form-control" />
                                            <select id="list2" onchange="editableFuncSecond(this.value)">
                                                 
                                                <?php
                                                if (isset($fundingOrg)) {
                                                    foreach ($fundingOrg as $key => $list) {
                                                ?>
                                                        <option value="<?php echo $list->id; ?>"><?php echo $list->funding_org; ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <input type="text" id="mText2" name="funding_org_text" value="" class="form-control" />

                                        </div>
                                        </div>
                                    
                                <!-- End Section Copied -->





                                <!-- <div class="form-group required col-md-12">
                                    <label class="control-label" for="usr">Project Name </label>
                                    <textarea rows="3" name="project_name" required cols="27" class="form-control"><?= $bddetail->TenderDetails; ?></textarea>
                                </div> -->

                                <div class="form-group required col-md-3">
                                    <label class="control-label" for="usr">Sector </label>
                                    <select id="sector_id" name="sector_id" class="form-control">
                                        <option value=""> -- Select Sector -- </option>
                                        <?php
                                        if ($sector):
                                            foreach ($sector as $rowr) {
                                                ?>
                                                <option <?= (@$projdetail->sector_id == $rowr->fld_id) ? 'selected' : ''; ?> value="<?= $rowr->fld_id; ?>"> <?= $rowr->sectName; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>   

                                <div class="form-group required col-md-3">
                                    <label class="control-label" for="usr">Service </label>
                                    <select id="service" name="service_id" class="form-control">
                                        <option value=""> -- Select Service -- </option>
                                        <?php
                                        if ($serviceArr):
                                            foreach ($serviceArr as $rowr) {
                                                ?>
                                                <option <?= (@$projdetail->service_id == $rowr->id) ? 'selected' : ''; ?> value="<?= $rowr->id; ?>"> <?= $rowr->service_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div> 

                                <div class="form-group required col-md-3">
                                    <label class="control-label" for="usr">Country </label>
                                    <select id="country" name="countries_id" onchange="setstatebycid()" class="form-control">
                                        <option value=""> -- Select Country -- </option>
                                        <?php
                                        if ($country_Arr):
                                            foreach ($country_Arr as $rowr) {
                                                ?>
                                                <option <?= (@$projdetail->country_id == $rowr->country_id) ? 'selected' : ''; ?> value="<?= $rowr->country_id; ?>"> <?= $rowr->country_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>
                                <div class="form-group required col-md-3">
                                    <label class="control-label" for="usr">State </label>
                                    <select id="state" name="state_id" class="form-control">
                                        <?php
                                        if ($stateArr):
                                            foreach ($stateArr as $rowrsec) {
                                                ?>
                                                <option <?= (@$projdetail->state_id == $rowrsec->state_id) ? 'selected' : ''; ?> value="<?= $rowrsec->state_id; ?>"> <?= $rowrsec->state_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>

                                <!-- <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Funding </label>
                                    <input type="text" value="<?= $expres->funding; ?>"  class="form-control" name="funding" id="funding">
                                </div>    -->

                                


                                <div class="form-group required col-md-12">
                                    <label class="control-label" for="usr">Lane </label>
                                </div>

                                <?php
                                if (@$ceglane):
                                    foreach ($ceglane as $lane_lenth) :
                                        ?>
                                        <div class="fieldGroup">       
                                            <div class="form-group required col-md-5">
                                                <?php
                                                $sec = '2';
                                                ?>
                                                <select id="lane_chk" name="lane_chk[]" class="form-control">
                                                    <option value='' selected>-Lane-</option>
                                                    <?php
                                                    for ($i = $sec; $i <= 12; $i = $i + 2) {
                                                        ?>
                                                        <option <?= ($lane_lenth->lane == $i) ? "Selected" : ""; ?> value=<?= $i; ?>><?= $i; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="form-group required col-md-5">
                                                <input type="text" name="lane_length[]" 
                                                value="<?= $lane_lenth->lane_length; ?>" class="form-control"/>
                                            </div>

                                            <div class="form-group col-md-2">		
                                                <a href="javascript:void(0)" class="btn btn-success addMore"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> Add</a>
                                            </div>

                                        </div>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                                <div class="form-group required col-md-3">
                                    <label class="control-label" for="usr">Project Cost </label>
                                    <input type="number"  min="0" step="any" class="form-control" name="project_cost" value="<?= ($expsummery->project_cost > 0) ? $expsummery->project_cost : ''; ?>" id="project_cost">
                                </div>
                                <div class="form-group required col-md-3">
                                    <label class="control-label" for="usr">Total Length </label>
                                    <input type="text"  class="form-control" name="length" 
                                    value="<?= $expsummery->length; ?>" id="length">
                                </div>

                                <div class="form-group required col-md-3">
                                    <label class="control-label" for="usr">Project Code </label>
                                    <input type="text" value="<?= $expres->generated_tenderid; ?>" 
                                    disabled class="form-control" id="project_code">
                                </div>
                                <div class="form-group required col-md-3">
                                    <label class="control-label" for="usr">Project Number </label>
                                    <select id="project_number" name="project_number" 
                                    class="form-control">
                                        <option value="" selected >Select Project </option>
                                        <?php
                                        if ($project):
                                            foreach ($project as $val) {
                                                ?>
                                                <option <?= (@$expsummery->project_numberid == $val->id) ? 'selected' : ''; ?> value="<?= $val->id; ?>"> <?= $val->project_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>  

                                <!-- <div class="form-group required col-md-4">
                                <label class="control-label" for="usr">PDS File </label>
                                <input type="file" class="form-control" name="pds_file"  id="pds_file">
                                </div>
                                <div class="form-group required col-md-4">
                                <label class="control-label" for="usr">Certificate File </label>
                                <input type="file" class="form-control" name="certificate_file" id="certificate_file">
                                </div>
                                <div class="form-group required col-md-4">
                                <label class="control-label" for="usr">Contract Agreement </label>
                                <input type="file" class="form-control" name="contract_agreement_file" id="contract_agreement_file">
                                </div>-->



                                            <!-- Consortium starts -->
                                            <div class="form-group required col-md-12">
                                    <div class="mt-2 mb-2">
                                    
                                    <h4 class="modal-title"> Consortium </h4>

                                    <hr style="border: 1px solid" />
                                    </div>


                                    <div class="mt-2 mb-2">

                                    <div class="highlight">

                                    <label class="fancy-checkbox">
                                    <input type="checkbox"  id="soleornot">
                                    <span>Sole</span>
                                    </label>

                                    </div>

                                    </div>

                                     
                           
                                    <div id="jvsection" style="display: block">
                                        <div class="form-group required col-md-12">
                                            <label>Lead Company </label>
                                            <select id="leadCom" name="leadcompany11[]" style="width:100%" multiple>
                                                <?php foreach ($compnameArr as $rowrec) { ?>
                                                    <option value="<?= $rowrec->fld_id; ?>">
                                                        <?= ucfirst($rowrec->company_name); ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group required col-md-12">
                                            <label> JV (<small> joint venture </small>)</label>
                                            <select id="jointCom" name="joint_venture22[]" style="width:100%" multiple>
                                                <?php foreach ($compnameArr as $rowrec) { ?>
                                                    <option value="<?= $rowrec->fld_id; ?>">
                                                        <?= ucfirst($rowrec->company_name); ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group required col-md-12">
                                            <label> Associate company..</label>
                                            <select id="assoCom" name="assoc33[]" style="width:100%" multiple>
                                                <?php foreach ($compnameArr as $rowrec) { ?>
                                                    <option value="<?= $rowrec->fld_id; ?>">
                                                        <?= ucfirst($rowrec->company_name); ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>


                                        
                                    
                                        </div> 
                                    </div>


                    <!-- Consortium Ends -->



                                <div class="form-group required col-md-12">
                                    <input type="hidden" name="projectid" class="btn green" value="<?= $bddetail->fld_id; ?>">
                                    <a href="javascript:void(0)" type="submit" class="btn btn-info pull-right mt-4 mb-2" onclick="saveProjectBasics()">Save</a>
                                </div>
                            </div>
                        </form>


                                            </div>
 





                    </div>
                    </div>
                    </div>
                    <!-- End -->




                      <!-- Start -->
                      <div class="card-header  mt-2" role="tab" id="performance_bd_detail">
                    <div class="mb-0" style="display: flex">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                    
                    <h3>BG Details </h3>
                    
                    </a>
                    <!-- <a data-toggle="modal" data-target="#myModalperformancebd" class="collapsed pull-right" title="Add / Edit BG Details"  style="width: auto"> <i class="fa fa-pencil" aria-hidden="true"></i> </a> -->
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseOne" class="collapsed pull-right" style="width: auto">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    </div>
                    </div>


                    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="performance_bd_detail" aria-expanded="false">
                    <div class="card-block">
                    
                    <form name="bidCostingForm" id="bidCostingForm" method="post" action="" enctype="multipart/form-data">
                            <div class="row">

                               
                                <div class="form-group required col-md-12">
                                        <label class="control-label" for="usr">Bid Security : <small style="color:green"> (Bid Security)</small> </label>
                                        <select required id="bid_security" name="bid_security" class="form-control">
                                            <option value="2"> No </option>
                                            <option value="1"> Yes </option>
                                        </select>
                                    </div>
                                    <div id="security_demnd" class="col-md-12" style="display:none">
                                        <div class="highlight">
                                            <div class="form-group required">
                                                <label class="control-label" for="usr">Bid Security Type : <small style="color:green"> (Bid Security)</small></label>
                                                <select id="bid_securitytype" name="bid_securitytype" class="form-control" onchange="showBankGuaranteeDetail(this.value, 'showSpfBg')">
                                                    <option value=""> -- Select Type-- </option>
                                                    <?php
                                                    if ($security_Type) :
                                                        foreach ($security_Type as $rowrsec) {
                                                    ?>
                                                            <option value="<?= $rowrsec->id; ?>"> <?= $rowrsec->security_type; ?></option>
                                                    <?php }
                                                    endif; ?>
                                                </select>
                                            </div>
                                            <div class="form-group required">
                                                <label> Amount : <small style="color:green"> (Bid Security)</small> </label>
                                                <input type="text" class="form-control" id="amount" name="amount" value="">
                                            </div>
                                            <div class="form-group required">
                                                <label> Validity : <small style="color:green"> (Bid Security)</small> </label>
                                                <input type="date" class="form-control" id="bs_valdity" name="bs_valdity" value="">
                                            </div>


                                            <div id="showSpfBg" class="row" style="display:none">
                                                <div class="form-group required col-md-12">
                                                    <label> Bank Name : </label>
                                                    <input type="text" class="form-control" id="sf_bankname" name="sf_bankname" value="">
                                                </div>


                                                <div class="form-group required col-md-12">
                                                    <label> BG Number : </label>
                                                    <input type="text" class="form-control" id="sf_bgnumber" name="sf_bgnumber" value="">
                                                </div>

                                                <div class="form-group required col-md-12">
                                                    <label> Date of Start : </label>
                                                    <input type="date" class="form-control" id="sf_dateofstart" name="sf_dateofstart" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--                                RFP Section Start-->
                                    <div class="form-group required col-md-12">
                                        <label>Cost Of RFP : <small style="color:#7386D5">(RFP)</small> </label>
                                        <select required id="rfp_cost" name="rfp_cost" class="form-control">
                                            <option value="2"> No </option>
                                            <option value="1"> Yes </option>
                                        </select>
                                    </div>
                                    <div id="security_demnd2" class="col-md-12" style="display:none">

                                        <div class="highlight">
                                            <div class="form-group required">
                                                <label>Amount : <small style="color:#7386D5">(RFP)</small></label>
                                                <input type="text" class="form-control" id="rfp_amount" name="rfp_amount" value="">
                                            </div>
                                            <div class="form-group required">
                                                <label> Type : <small style="color:#7386D5">(RFP)</small></label>
                                                <select id="rfp_type" name="rfp_type" class="form-control" onchange="showBankGuaranteeDetail(this.value, 'showRpfBg')">
                                                    <option value=""> -- Select Type-- </option>
                                                    <?php
                                                    if ($security_Type) :
                                                        foreach ($security_Type as $rowrsec) {
                                                            if ($rowrsec != '5') {
                                                    ?>
                                                                <option value="<?= $rowrsec->id; ?>"> <?= $rowrsec->security_type; ?></option>
                                                    <?php
                                                            }
                                                        }
                                                    endif;
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group required">
                                                <label> Validity : </label>
                                                <input type="date" class="form-control" id="rfp_valdity" name="rfp_valdity" value="">
                                            </div>


                                            <div id="showRpfBg" class="row" style="display:none">
                                                <div class="form-group required col-md-12">
                                                    <label> Bank Name : </label>
                                                    <input type="text" class="form-control" id="rpf_bankname" name="rpf_bankname" value="">
                                                </div>


                                                <div class="form-group required col-md-12">
                                                    <label> BG Number : </label>
                                                    <input type="text" class="form-control" id="rpf_bgnumber" name="rpf_bgnumber" value="">
                                                </div>

                                                <div class="form-group required col-md-12">
                                                    <label> Date of Start : </label>
                                                    <input type="date" class="form-control" id="rpf_dateofstart" name="rpf_dateofstart" value="">
                                                </div>
                                            </div>





                                        </div>
                                    </div>
                                    <!--   RFP Section Close -->

                                    <!-- Proccessing Section Start-->
                                    <div class="form-group required col-md-12">
                                        <label>Processing Fee : <small style="color:#1995dc">(Processing Fee)</small></label>
                                        <select required id="processing_fee" name="processing_fee" class="form-control">
                                            <option value="2"> No </option>
                                            <option value="1"> Yes </option>
                                        </select>
                                    </div>
                                    <div id="security_demnd3" class="col-md-12" style="display:none">

                                        <div class="highlight">
                                            <div class="form-group required">
                                                <label>Amount : <small style="color:#1995dc">(Processing Fee)</small> </label>
                                                <input type="number" step="any" min="1" class="form-control" id="pfee_amount" name="pfee_amount" value="">
                                            </div>
                                            <div class="form-group required">
                                                <label> Type : <small style="color:#1995dc">(Processing Fee)</small></label>
                                                <select id="processing_fee_type" name="processing_fee_type" class="form-control" onchange="showBankGuaranteeDetail(this.value, 'showPfBg')">
                                                    <option value=""> -- Select Type-- </option>
                                                    <?php
                                                    if ($security_Type) :
                                                        foreach ($security_Type as $rowrsec) {
                                                            if ($rowrsec != '5') {
                                                    ?>
                                                                <option value="<?= $rowrsec->id; ?>"> <?= $rowrsec->security_type; ?></option>
                                                    <?php
                                                            }
                                                        }
                                                    endif;
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group required">
                                                <label> Validity : </label>
                                                <input type="date" class="form-control" id="pf_valdity" name="pf_valdity" value="">
                                            </div>




                                            <div id="showPfBg" class="row" style="display:none">
                                                <div class="form-group required col-md-12">
                                                    <label> Bank Name : </label>
                                                    <input type="text" class="form-control" id="pf_bankname" name="pf_bankname" value="">
                                                </div>


                                                <div class="form-group required col-md-12">
                                                    <label> BG Number : </label>
                                                    <input type="text" class="form-control" id="pf_bgnumber" name="pf_bgnumber" value="">
                                                </div>

                                                <div class="form-group required col-md-12">
                                                    <label> Date of Start : </label>
                                                    <input type="date" class="form-control" id="pf_dateofstart" name="pf_dateofstart" value="">
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                     

                              <div class="form-group required col-md-12">
                                    <label for="">PBG: </label>
                                    <select class="form-control" name="pbg_name" id="pbg_name">
                                        <option value="">-Select PBG-</option>
                                        <option <?= ($perf_bd_record->pbg == 'Yes') ? "Selected" : ''; ?> value="Yes">Yes</option>
                                        <option <?= ($perf_bd_record->pbg == 'No') ? "Selected" : ''; ?> value="No">No</option>
                                    </select>
                                </div>
                                <div id="pbg_row" style="width: 100%" class=" m-4 highlight">  
                                  
                                    <div class="form-group required col-md-12">  
                                        <label> PBG Amount : </label>
                                        <input type="text" class="form-control" id="pbg_amount" name="pbg_amount" value="<?= $perf_bd_record->pbg_amount; ?>"></option>
                                        </select>
                                    </div>
                                    <div class="form-group required col-md-12">  
                                        <label> PBG Number: </label>
                                        <input type="text" class="form-control" id="pbg_number" name="pbg_number" value="<?= $perf_bd_record->pbg_number; ?>"/>
                                    </div>
                                    <div class="form-group required col-md-12">  
                                        <label> Bank : </label>
                                        <input type="text" class="form-control" id="pbg_bank" name="pbg_bank" value="<?= $perf_bd_record->pbg_bank; ?>"/>
                                    </div>

                                    <div class="form-group required col-md-12">
                                        <label> PBG Start : </label>
                                        <input type="date" onkeydown="return false" class="form-control" id="pbg_start" name="pbg_start" value="<?= $perf_bd_record->pbg_start; ?>">
                                    </div>

                                    <div class="form-group required col-md-12">  
                                        <label> PBG Validity : </label>
                                        <input type="date" onkeydown="return false" class="form-control" id="pbg_validity" name="pbg_validity" value="<?= $perf_bd_record->pbg_validity; ?>"/>
                                    </div>
                                  
                                    
                                </div>
                                <div class="form-group required col-md-12">
                                    <label for="">ABG: </label>
                                    <select class="form-control" name="abg_name" id="abg_name">
                                        <option value="">-Select ABG-</option>
                                        <option <?= ($perf_bd_record->abg == 'Yes') ? "Selected" : ''; ?> value="Yes">Yes</option>
                                        <option <?= ($perf_bd_record->abg == 'No') ? "Selected" : ''; ?> value="No">No</option>
                                    </select>
                                </div>
                                <div id="abg_row"  style="width: 100%" class=" m-4 highlight">  
                                    <div class="form-group required col-md-12">  
                                        <label> ABG Amount : </label>
                                        <input type="text" class="form-control" id="abg_amount" name="abg_amount" value="<?= $perf_bd_record->abg_amount; ?>"/>
                                    </div>
                                    <div class="form-group required col-md-12">  
                                        <label> ABG Number: </label>
                                        <input type="text" class="form-control" id="abg_number" name="abg_number" value="<?= $perf_bd_record->abg_number; ?>"/>
                                    </div>
                                    <div class="form-group required col-md-12">  
                                        <label> Bank : </label>
                                        <input type="text" class="form-control" id="abg_bank" name="abg_bank" value="<?= $perf_bd_record->abg_bank; ?>"/>
                                    </div>
                                    <div class="form-group required col-md-12">
                                        <label> ABG Start : </label>
                                        <input type="date" onkeydown="return false" class="form-control" id="abg_start" name="abg_start" value="<?= $perf_bd_record->abg_start; ?>">
                                    </div>
                                    <div class="form-group required col-md-12">  
                                        <label> ABG Validity : </label>
                                        <input type="date" onkeydown="return false" class="form-control" id="abg_validity" name="abg_validity" value="<?= $perf_bd_record->abg_validity; ?>"/>
                                    </div>
                                    
                                </div>
                                 
                        
                                <div class="form-group required col-md-12">
                                    <input type="hidden" name="project_id" class="btn green" value="<?= @$bddetail->fld_id; ?>">
                                    <a href="javascript:void(0)"  class="btn btn-info pull-right mb-2 mt-2" onclick="saveBidCosting()">Save</a>
                                </div>


                            </div>
                        </form>


                    </div>
                    </div>
                     
                    <!-- End -->
                  
                    

  <!-- Start -->
  <div class="card-header  mt-2" role="tab" id="key_information">
                    <div class="mb-0" style="display: flex">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="collapsed">
                   
                    <h3>Project Contact Personal </h3>
                    
                    </a>
                    <!-- <a data-toggle="modal" data-target="#myModaltlofcmngr" class="collapsed pull-right" title="Add / Edit Project Contact Personal Details"  style="width: auto"> <i class="fa fa-pencil" aria-hidden="true"></i> </a> -->
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree" class="collapsed pull-right" style="width: auto">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    </div>
                    </div>


                    <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="key_information" aria-expanded="false">
                    <div class="card-block">
                   

                                           
                    <form name="projectPersonelForm" id="projectPersonelForm" method="post" action="" enctype="">

                    <div class="row">

                    <div class="form-group required col-md-6">
                    <label class="control-label" for="usr"> Project Coordinator: </label>
                    <select class="form-control" name="project_coordinator" id="project_coordinator">
                    <option value=""> -- Select -- </option>
                    <?php
                    if ($empListArr) {
                    foreach ($empListArr as $rowR) {
                    ?>
                    <option <?= ($proj_con_per_detail['coor_emp_id'] == $rowR->user_id) ? "Selected" : ''; ?> value="<?= $rowR->user_id; ?>"><?= $rowR->userfullname . " [ " . $rowR->employeeId . " ]"; ?></option>
                    <?php
                    }
                    }
                    ?>
                    </select>
                    </div>

                    <div class="form-group required col-md-6">
                    <label class="control-label" for="usr"> Project Engineer : </label>
                    <select class="form-control" name="project_engineer" id="project_engineer">
                    <option value=""> -- Select -- </option>
                    <?php
                    if ($empListArr) {
                    foreach ($empListArr as $rowR) {
                    ?>
                    <option <?= ($proj_con_per_detail['eng_emp_id'] == $rowR->user_id) ? "Selected" : ''; ?> value="<?= $rowR->user_id; ?>"> <?= $rowR->userfullname . " [ " . $rowR->employeeId . " ]"; ?></option>
                    <?php
                    }
                    }
                    ?>
                    </select>
                    </div>

                    
          

                    <div class="form-group required col-md-6">
                    <label class="control-label" for="usr"> Contact Person : </label>
                    <select class="form-control" name="cont_pers_typer" id="cont_pers_typer">
                    <option value=""> -- Select -- </option>
                    <option value="tl"  <?= ($personelContactData->cont_pers_typer == 'tl') ? "Selected" : ''; ?>> Team Leader </option>
                    <option value="ofc_mngr"  <?= ($personelContactData->cont_pers_typer == 'ofc_mngr') ? "Selected" : ''; ?>> Office Manager </option>
                    <option value="other"  <?= ($personelContactData->cont_pers_typer == 'other') ? "Selected" : ''; ?>> Other </option>
                    </select>
                    </div>

                    <div class="form-group required col-md-6">
                    <label class="control-label" for="usr"> Employee : </label>
                    <select class="form-control" name="emp_id" id="emp_id">
                    <option value=""> -- Select -- </option>
                    <?php
                    if ($empListArr) {
                    foreach ($empListArr as $rowR) {
                    ?>
                    <option value="<?= $rowR->user_id; ?>"   <?= ($personelContactData->emp_id == $rowR->user_id) ? "Selected" : ''; ?>><?= $rowR->userfullname . " [ " . $rowR->employeeId . " ]"; ?></option>
                    <?php
                    }
                    }
                    ?>
                    </select>
                    </div>

                    <div class="form-group required col-md-12"> <br>
                    <input type="hidden" name="project_id" value="<?= $bddetail->fld_id; ?>">
                    <a href="javascript:void(0)" class="btn btn-info pull-right mb-2" onclick="saveProjectPersonelForm()">Save</a>
                    </div>
                    </div>
                    </form>
                                           
                   
                                               
                    </div>
                    </div>
                    
                    <!-- End -->




                      <!-- Start -->
                      <div class="card-header  mt-2" role="tab" id="client_detail">
                    <div class="mb-0" style="display: flex">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="collapsed">
                   
                    <h3>Client Details  </h3>
                    
                    </a>
                    <!-- <a data-toggle="modal" data-target="#myModalclientaddress" class="collapsed pull-right" title="Add Client Details"  style="width: auto"> <i class="fa fa-pencil" aria-hidden="true"></i> </a> -->
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapseThree" class="collapsed pull-right" style="width: auto">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    </div>
                    </div>


                    <div id="collapse5" class="collapse" role="tabpanel" aria-labelledby="client_detail" aria-expanded="false">
                    <div class="card-block">
                    
                                           
    
                    <div class="mb-2">
                                <h5 class="modal-title">Client Info</h5>
                                <hr style="border: 1px solid" />
                            </div>
                            
                                <form name="clientInfoForm" id="clientInfoForm" method="post" enctype="">
                                    <div class="row">
                                        <div class="form-group required col-md-6">
                                            <label> Client Name</label>



                                            <div class="select-editable">

                                                <select id="list3" onchange="editableFuncClientContact(this.value)">
                                                    
                                                    <?php
                                                    if (isset($clientName)) {
                                                        foreach ($clientName as $key => $list) {
                                                    ?>
                                                            <option value="<?php echo $list->id; ?>"><?php echo $list->client_name; ?></option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <input type="hidden" id="mVal3" name="client_contact_name" value="" class="form-control" />

                                                <input type="text" id="mText3" name="client_contact_name_text" value="" class="form-control" />


                                            </div>
                                        </div>



                                        <div class="form-group required col-md-6">
                                            <label> Client Contact</label>
                                            <input type="text" class="form-control" id="client_contact" name="client_contact" value="">
                                        </div>
                                        <div class="form-group required col-md-12">
                                            <label> Client Address</label>
                                            <textarea class="form-control" id="client_address" name="client_address"></textarea>&nbsp;
                                            <a onclick="addClientContact()" class="btn btn-outline-dark pull-right mt-4">+</a>
                                        </div>

                                        <div class="form-group required col-md-6">
                                            
                                            <input type="hidden" name="project_id"  id="project_id" class="btn green" value="<?= @$bddetail->fld_id; ?>">
                                            <input type="hidden" name="sectionCounter" id="sectionCounter" value="0" class="btn green">

                                        </div>
                                    </div>

                                    <div id="contactSection">



                                    </div>
                                    <div class="row">
                                        <div class="form-group required col-md-12">
                                            <table border="1" id="clientadd" style="width:100%"></table>
                                        </div>
                                    </div>

                           
                           <div class="row">
                                        <div class="form-group required col-md-12">
                                        <a href="javascript:void(0)" class="btn btn-info btn-sm  action-button pull-right ml-2" 
                            onclick="saveClientInfo()">Save</a>
                                        </div>
                                    </div>
                          
                           
                            <!-- <a href="javascript:void(0)"  class="btn btn-info btn-sm  action-button pull-right"  onclick="stepProcessor(3, 'next')">Next</a> -->
                            
                            
                            
                            </form>
                           

                    </div>
                    </div>
                     
                    <!-- End -->


                    <!-- Start -->
                    <div class="card-header  mt-2" role="tab" id="site_office_detail">
                    <div class="mb-0" style="display: flex">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapseThree" class="collapsed">
                    
                    <h3>Site Office Details </h3>

                    </a>
                    <a data-toggle="modal" data-target="#myModalssiteofcdetails" class="collapsed pull-right" title="Add Site Office Details"  style="width: auto"> <i class="fa fa-pencil" aria-hidden="true"></i> </a>
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapseThree" class="collapsed pull-right" style="width: auto">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    </div>
                    </div>


                    <div id="collapse6" class="collapse" role="tabpanel" aria-labelledby="site_office_detail" aria-expanded="false">
                    <div class="card-block">

                        <?php 
                                        
                                        if(empty($RecSiteOfcDetails)) { ?>                       
                                        <div class="row"> 
                                        <div class="col-md-12">
                                        <div class="alert alert-warning text-left">No Data Available !</div>
                                        </div>
                                        </div>
                                        <?php } ?>

<?php
if ($RecSiteOfcDetails) {
    foreach ($RecSiteOfcDetails as $rowRecC) {
        ?>
        <div class="row"> 
            <div class="col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="">Addess: <a style="cursor: pointer; color: green" title="Edit Site Office Detail" data-toggle="modal" data-target="#myModalseditsiteofcdetails" onclick="editsiteofficeaddress('<?= $rowRecC->fld_id; ?>')"><span class="glyphicon glyphicon-edit"></span></a></label>
                    <textarea disabled="disabled" class="form-control"><?php if ($rowRecC->street_address) { ?><?= @$rowRecC->street_address; ?><?php } ?></textarea>
                </div>
            </div>     
        </div>


        <div class="row">
            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <label for="">Country:</label>
                    <input disabled="disabled" type="text" class="form-control" value="<?php if ($rowRecC->country_name) { ?><?= @$rowRecC->country_name; ?><?php } ?>">
                </div>
            </div> 
            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <label for="">State:</label>
                    <input disabled="disabled" type="text" class="form-control" value="<?php if ($rowRecC->state_name) { ?><?= @$rowRecC->state_name; ?><?php } ?>">
                </div>
            </div> 
            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <label for="">City:</label>
                    <input disabled="disabled" type="text" class="form-control" value="<?php if ($rowRecC->city_name) { ?><?= @$rowRecC->city_name; ?><?php } ?>">
                </div>
            </div>      
            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <label for="">Pincode:</label>
                    <input disabled="disabled" type="text" class="form-control" value="<?php if ($rowRecC->siteofc_pincode) { ?><?= @$rowRecC->siteofc_pincode; ?><?php } ?>">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <label for="">Contact Person Email 3:</label>
                    <input disabled="disabled" type="text" class="form-control" value="<?php if ($rowRecC->contper_emailaddress) { ?><?= @$rowRecC->contper_emailaddress; ?><?php } ?>">
                </div>
            </div>    
            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <label for=""> Contact Person Name:</label>
                    <input disabled="disabled" type="text" class="form-control" value="<?php if ($rowRecC->contper_userfullname) { ?><?= @$rowRecC->contper_userfullname; ?><?php } ?>">
                </div>
            </div> 
            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <label for="">Contact Person Number</label>
                    <input disabled="disabled" type="text" class="form-control" value="<?php if ($rowRecC->contper_contactnumber) { ?><?= @$rowRecC->contper_contactnumber; ?><?php } ?>">
                </div>
            </div> 
        </div>  
        <?php
    }
}
?>
                   




                    </div>
                    </div>
                    
                    <!-- End -->





                      <!-- Start -->
                      <div class="card-header  mt-2" role="tab" id="key_date">
                    <div class="mb-0" style="display: flex">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="collapsed">
                   
                    <h3>Key Dates </h3>
                    
                    </a>
                    <a data-toggle="modal" data-target="#myModalskeydate" class="collapsed pull-right" title="Add / Edit Key Dates"  style="width: auto"> <i class="fa fa-pencil" aria-hidden="true"></i> </a>
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapseThree" class="collapsed pull-right" style="width: auto">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    </div>
                    </div>


                    <div id="collapse7" class="collapse" role="tabpanel" aria-labelledby="key_date" aria-expanded="false">
                    <div class="card-block">
                    
                                     <?php 
                                        
                                        if(empty($ReckeydateSingle)) { ?>                       
                                        <div class="row"> 
                                        <div class="col-md-12">
                                        <div class="alert alert-warning text-left">No Data Available !</div>
                                        </div>
                                        </div>
                                        <?php } ?>
                

                                            <?php if ($ReckeydateSingle) { ?>   
                                                <div class="row">    
                                                    <div class="col-sm-3 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="">Bid Submission :</label>
                                                            <input disabled="disabled" placeholder="mm/dd/YYYY" type="text" id="date_bid_submission"  class="form-control" value="<?php if (($ReckeydateSingle->date_bid_submission) and ( $ReckeydateSingle->date_bid_submission != '1970-01-01')) { ?><?= date("m/d/Y", strtotime($ReckeydateSingle->date_bid_submission)); ?><?php } ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="">Financial Opening :</label>
                                                            <input disabled="disabled" placeholder="mm/dd/YYYY" type="text" id="date_financialopening"  class="form-control" value="<?php if (($ReckeydateSingle->date_financialopening) and ( $ReckeydateSingle->date_financialopening != '1970-01-01')) { ?><?= date("m/d/Y", strtotime($ReckeydateSingle->date_financialopening)); ?><?php } ?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="">Letter of Award :</label>
                                                            <input disabled="disabled" placeholder="mm/dd/YYYY" type="text" id="date_latter_award"  class="form-control" value="<?php if (($ReckeydateSingle->date_latter_award) and ( $ReckeydateSingle->date_latter_award != '1970-01-01')) { ?><?= date("m/d/Y", strtotime($ReckeydateSingle->date_latter_award)); ?><?php } ?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="">Contract Agrement :</label>
                                                            <input disabled="disabled" placeholder="mm/dd/YYYY" type="text" class="form-control" id="date_contract_agrement" value="<?php if (($ReckeydateSingle->date_contract_agrement) and ( $ReckeydateSingle->date_contract_agrement != '1970-01-01')) { ?><?= date("m/d/Y", strtotime($ReckeydateSingle->date_contract_agrement)); ?><?php } ?>">
                                                        </div>
                                                    </div>
                                                </div> 
                                            <?php } ?> 

                                            <?php if (@$ReckeydateSingle) { ?> 
                                                <div class="row"> 
                                                    <div class="col-sm-2 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="">Internal Mou (If Any) :</label>
                                                            <input disabled="disabled" placeholder="mm/dd/YYYY" type="text" class="form-control" id="internal_mou" value="<?php if (($ReckeydateSingle->date_of_mov) and ( $ReckeydateSingle->date_of_mov != '1970-01-01')) { ?><?= date("m/d/Y", strtotime($ReckeydateSingle->date_of_mov)); ?><?php } ?>">
                                                        </div>
                                                    </div> 

                                                    <div class="col-sm-2 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="">Client Mou (If Any) :</label>
                                                            <input disabled="disabled" placeholder="mm/dd/YYYY" type="text" class="form-control" id="client_mou" value="<?php if (($ReckeydateSingle->client_mou) and ( $ReckeydateSingle->client_mou != '1970-01-01')) { ?><?= date("m/d/Y", strtotime($ReckeydateSingle->client_mou)); ?><?php } ?>">
                                                        </div>
                                                    </div>  

                                                    <div class="col-sm-2 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="">Commencement :</label>
                                                            <input disabled="disabled" placeholder="mm/dd/YYYY" type="text" class="form-control" id="date_of_commencement" value="<?php if (isset($ReckeydateSingle->date_of_commencement) and ( $ReckeydateSingle->date_of_commencement != '1970-01-01')) { ?><?= date("m/d/Y", strtotime($ReckeydateSingle->date_of_commencement)); ?><?php } ?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="">Completion as per contract :</label>
                                                            <input disabled="disabled" placeholder="mm/dd/YYYY" type="text" class="form-control" id="date_stipulatedcompletion" value="<?php if (($ReckeydateSingle->date_stipulatedcompletion) and ( $ReckeydateSingle->date_stipulatedcompletion != '1970-01-01')) { ?><?= date("m/d/Y", strtotime($ReckeydateSingle->date_stipulatedcompletion)); ?><?php } ?>">
                                                        </div>
                                                    </div>    

                                                    <div class="col-sm-3 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="">Actual Completion :</label>
                                                            <input disabled="disabled" placeholder="mm/dd/YYYY" type="text" class="form-control" id="date_of_assignment" value="<?php if (($ReckeydateSingle->date_of_assignment) and ( $ReckeydateSingle->date_of_assignment != '1970-01-01')) { ?><?= date("m/d/Y", strtotime($ReckeydateSingle->date_of_assignment)); ?><?php } ?>">
                                                        </div>
                                                    </div> 
                                                </div> 
                                            <?php } ?>     
                                         
                                                            




                    </div>
                    </div>
                    
                    <!-- End -->



                      <!-- Start -->
                      <div class="card-header  mt-2" role="tab" id="financial_detail">
                    <div class="mb-0" style="display: flex">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapseThree" class="collapsed">
                   
                    <h3>Financial Details </h3>
                    
                    </a>
                    <a data-toggle="modal" data-target="#myModalfinancial" class="collapsed pull-right" title="Add / Edit Financial Details"  style="width: auto"> <i class="fa fa-pencil" aria-hidden="true"></i> </a>
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapseThree" class="collapsed pull-right" style="width: auto">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    </div>
                    </div>


                    <div id="collapse8" class="collapse" role="tabpanel" aria-labelledby="financial_detail" aria-expanded="false">
                    <div class="card-block">
                    
                        <?php 
                                        
                        if(empty($financial_record)) { ?>                       
                        <div class="row"> 
                        <div class="col-md-12">
                        <div class="alert alert-warning text-left">No Data Available !</div>
                        </div>
                        </div>
                        <?php } ?>



                                            <div class="row"> 

                                                <?php if (@$financial_record) { ?>  
                                                    <div class="col-sm-3 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="">Financial Proprosal:</label>
                                                            <input disabled="disabled" type="text" class="form-control" value="<?php if (!empty($financial_record->financial_proprosal)) { ?><?= $financial_record->financial_proprosal; ?><?php } ?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="">Value as per LOA :</label>
                                                            <input disabled="disabled" type="text" class="form-control" value="<?php if (!empty($financial_record->value_as_per_loa)) { ?><?= $financial_record->value_as_per_loa; ?><?php } ?>">
                                                        </div>
                                                    </div>


                                                    <div class="col-md-3 col-xs-12">
                                                        <div class="form-group">  
                                                            <label for="">Revised contract Value : </label>
                                                            <input disabled type="text" class="form-control" id="revised_contract_value" name="revised_contract_value" value="<?php if (!empty($financial_record->Revised_Contract_Value)) { ?><?= $financial_record->Revised_Contract_Value; ?><?php } ?>">
                                                        </div> 
                                                    </div> 

                                                <?php } ?>
                                            </div>

                                            <?php if (@$financial_record) { ?>       
                                                <div class="col-md-12 col-xs-12">
                                                    <table align="center" class="table table-bordered">
                                                        <thead>
                                                        <th style="width:50%;">Name</th>
                                                        <th>Amount</th>
                                                        <th>Share</th>
                                                        </thead> 
                                                        <tbody>
                                                            <?php //if (@$competitor_jv_record) { ?>
                                                                <?php
                                                               // foreach ($competitor_jv_record as $jv_record) {
                                                                   // if ($jv_record->compt_comp_id == '74') {
                                                                        //$rrEccT1 = GetCompAlldetails($jv_record->project_id, $jv_record->compt_comp_id, '1');
                                                                        //$rrEccT2 = GetCompAlldetails($jv_record->project_id, $jv_record->compt_comp_id, '2');
                                                                        //$rrEccT3 = GetCompAlldetails($jv_record->project_id, $jv_record->compt_comp_id, '3');
												        $rrEccT1 = GetCompAlldetailsnewash($bddetail->fld_id, '1');
                                                        $rrEccT2 = GetCompAlldetailsnewash($bddetail->fld_id, '2');
                                                        $rrEccT3 = GetCompAlldetailsnewash($bddetail->fld_id, '3');
                                                                        ?>
                                                                        <?php if ($rrEccT1) : ?>
                                                                            <tr>
                                                                                <td><?= $rrEccT1; ?></td>
                                                                                <td><?= number_format($financial_record->lead_amount); ?></td>
                                                                                <td><?= $financial_record->lead_share; ?></td>
                                                                            </tr>
                                                                        <?php endif; ?>
                                                                        <?php if ($rrEccT2) : ?>
                                                                            <tr>                                                     
                                                                                <td><?= $rrEccT2; ?></td>
                                                                                <td><?= number_format($financial_record->jv_amount); ?></td>
                                                                                <td><?= $financial_record->jv_share; ?></td>
                                                                            </tr>
                                                                        <?php endif; ?>
                                                                        <?php if ($rrEccT3) : ?>
                                                                            <tr>  
                                                                                <td><?= $rrEccT3; ?></td>
                                                                                <td><?= number_format($financial_record->assoc_amount); ?></td>
                                                                                <td><?= $financial_record->assoc_share; ?></td>
                                                                            </tr>
                                                                        <?php endif; ?>
                                                                        <?php
                                                                   // }
                                                              //  }
                                                                ?>  
                                                            <?php //} ?>
                                                        </tbody>
                                                    </table>
                                                </div>  
                                            <?php } ?>
                             




                    </div>
                    </div>
                    
                    <!-- End -->


                                                                       


                    </div>
                    

                 
                  
                     
 


                    <div class="panel-group mt-2 ml-0 pt-4 pl-0 pr-0">
                        
                       <div class="box col-md-12">
                                <div class="panel panel-default">
                                     
                                    <div class="panel-body">
                                        <form id="form-filter" class="form-horizontal">
                                        <div class="table-responsive">

                                        <a href="javacsript:void(0)" data-toggle="modal" data-target="#Modalweightage" class="btn btn-warning pull-right 
                                        p-2 pl-4 pr-4  ml-2"><i class="fa fa-eye" aria-hidden="true"></i> &nbsp;&nbsp;Add Weightage</a>
                    <a href="javacsript:void(0)" data-toggle="modal" data-target="#myModalcompt" 
                    class="btn btn-success  p-2 pl-4 pr-4  pull-right ml-4"><i class="fa fa-hourglass-end" aria-hidden="true"></i> &nbsp;&nbsp;
                    Add Competitor</a>



                   
    <input type="hidden" name="putText1" id="putText1" value="<?php echo  $getSingleData->TenderDetails;?>">
    <input type="hidden" name="putText2" id="putText2" value="<?php echo  $getSingleData->generated_tenderid;?>">
    <input type="hidden" name="putText3" id="putText3" value="<?php echo  $getSingleData->client_name;?>">
    <input type="hidden" name="putText4" id="putText4" value="<?php echo  $getProposalManager->userfullname;?>">
                                    


                   
                                            <table id="example1" class="table table-striped display">                                                         
                                                <thead>
                                                    <tr>
                                                        <th>Sr. No. </th>
                                                        <!-- <th style="width: 150px;">Project Name </th>
                                                        <th>Code </th>

                                                        <th>Proposal Manager </th>
                                                        <th>Client </th> -->

                                                        <th>Company </th>
                                                        <th>Technical</th>
                                                        <th>Marks &nbsp;
                                                            <a href="javascript:void(0)" onclick="reloadscore_tech('<?= $bddetail->fld_id; ?>')">
                                                                <li style="cursor:pointer" title="Generate" class="glyphicon glyphicon-refresh"></li>
                                                            </a>
                                                        </th>
                                                        <th>Financial</th>
                                                        <th>
                                                            Marks &nbsp; 

                                                            <a href="javascript:void(0)" onclick="reloadscore_finan('<?= $bddetail->fld_id; ?>')">
                                                                <li style="cursor:pointer" title="Generate" class="glyphicon glyphicon-refresh"></li>
                                                            </a>
                                                        </th>
                                                        <th>Total</th>
                                                        <th>Rank &nbsp;&nbsp; 

                                                        </th>

                                                        <th>Action &nbsp;&nbsp; 

</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php
//start Rank Cod
                                                    $sortArr = array();
                                                    $temrArr = array();
                                                    foreach ($cegcompcomp as $comptrow) {
                                                        $totRec7 = ($comptrow->technical_marks + $comptrow->financial_marks);
                                                        $temrArr[$comptrow->fld_id] = number_format($totRec7, 2);
                                                    }
                                                    arsort($temrArr);
                                                    $finalRank = array();
                                                    $nnmms = 1;
                                                    foreach ($temrArr as $kky => $rwrank):
                                                        $finalRank[$kky] = $nnmms;
                                                        $nnmms++;
                                                    endforeach;
//Close Rank Code
                                                    $ssnn = 0;
                                                    if ($cegcompcomp):




                                                        foreach ($cegcompcomp as $comptrow) {
                                                            


                                                            

                                                            ?>
                                                            <tr>
                                                                <td><?= $ssnn = $ssnn + 1; ?></td>
                                                                






                                                                <td>
                                                                   
                                                                <table class="table ">
                                                                            
                                                                                <tr>
                                                                                    <td><?= $this->mastermodel->GetCompNameById($comptrow->compt_comp_id); ?></td>






                                                                                </tr>
                                                                             
                                                                            
 
                                                                
                                                                

                                                                    <?php
                                                                    $rrEccT1 = '';
                                                                    $rrEccT2 = '';
                                                                    $rrEccT3 = '';



                                                                    

                                                                    $slimSelect1 = GetCompAlldetailsSlimSelect($bddetail->fld_id, $comptrow->compt_comp_id, '1');
                                                                    $slimSelect2 = GetCompAlldetailsSlimSelect($bddetail->fld_id, $comptrow->compt_comp_id, '2');
                                                                    $slimSelect3 = GetCompAlldetailsSlimSelect($bddetail->fld_id, $comptrow->compt_comp_id, '3');




                                                                    $rrEccT1 = GetCompAlldetails($bddetail->fld_id, $comptrow->compt_comp_id, '1');
                                                                    $rrEccT2 = GetCompAlldetails($bddetail->fld_id, $comptrow->compt_comp_id, '2');
                                                                    $rrEccT3 = GetCompAlldetails($bddetail->fld_id, $comptrow->compt_comp_id, '3');
                                                                      if ($rrEccT1 or $rrEccT2 or $rrEccT3) {   if ($rrEccT1) : ?>
                                                                                <tr>
                                                                                    <td><?= $rrEccT1; ?></td>
                                                                                </tr>
                                                                            <?php endif;   if ($rrEccT2) : ?>
                                                                                <tr>
                                                                                    <td><?= $rrEccT2; ?></td>
                                                                                </tr>
                                                                            <?php endif;  if ($rrEccT3) : ?>
                                                                                <tr>
                                                                                    <td><?= $rrEccT3; ?></td>
                                                                                </tr>
                                                                            <?php endif;   } ?>

                                                                    </table>
                                                                </td>
                                                                <td><?= number_format($comptrow->technical_score, 2); ?></td>
                                                                <td><?= number_format($comptrow->technical_marks, 2); ?></td>
                                                                <!--<td>
                                                                <?php
                                                                if (($comptrow->techn_weightage) and ( $comptrow->technical_score)) {
                                                                    $reeMulScore = ($comptrow->technical_score * $comptrow->techn_weightage);
                                                                }
                                                                echo isset($reeMulScore) ? $reeMulScore : '0.00';
                                                                ?>
                                                                                                                                                                                                        </td>-->
                                                                <td><?= number_format($comptrow->financial_score, 2); ?></td>
                                                                <td><?= number_format($comptrow->financial_marks, 2); ?></td>
                                                                <td>
                                                                    <?php
                                                                    $totRec = ($comptrow->technical_marks + $comptrow->financial_marks);
                                                                    echo number_format($totRec, 2);
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?= $finalRank[$comptrow->fld_id]; ?>  
                                                        </td>


                                                                <td>
                                                                
                                                                

                                                                   


                                                                    <div  onclick="jvforcompsetid('<?= $comptrow->compt_comp_id ?>','<?= $slimSelect1 ?>', '<?= $slimSelect2 ?>','<?= $slimSelect3 ?>')" 
                                                                    
                                                                    title="Add Join joint Venture" class="btn btn-success"
                                                                    > <i class="fa fa-info"></i> JV </div>
                                                                     
                                                                    <div onclick="delcomptcom('<?= $comptrow->fld_id ?>')" title="Delete" 
                                                                    class="btn btn-danger">
                                                                        <i class="fa fa-trash"></i> Delete
                                                                    </div>
 
                                                                    <div style="cursor:pointer" onclick="setupdid('<?= $comptrow->fld_id; ?>')" data-toggle="modal" 
                                                                    data-target="#myModalscore" title="Generate Rank" 
                                                                    class="btn btn-info mt-2"><i class="glyphicon glyphicon-edit"></i> Generate Rank</div>


                                                                   
                                                                
                                                                
                                                                
                                                                </td>


                                                        </tr>
                                                        <?php
                                                    }
                                                endif;
                                                ?>
                                                </tbody>
                                            </table>

                                        </div>
                                            <span>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" class="alert alert-info glyphicon-envelope  pull-right  mt-2  ml-2" onclick="mailSenderCom('mailsender')"> Send Mail Competitor</a></span>
                                            <span>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)"  class="alert alert-info glyphicon-envelope  pull-right mt-2 ml-2" onclick="mailSender('mailsendercom')"> Send Mail 8020</a></span>
                                        </form>
                                    </div>
                                </div>


                               
                               </div>


                                
</div>
</div>



                          

                <div class="tab-pane" id="info-tab"> 
                <div class="table-responsive">
                                     
                                         <p class="alert alert-success  mt-2"><?= $bddetail->TenderDetails; ?></p>
                                <table class="table table-striped display">                                                         
                                    <thead>
                                        <tr>
                                            <th>Sr no</th>
                                            <th>Position</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Contact</th>
                                            <th>Age Limit</th>
                                            <th>Certifcate</th>
                                            <th>Firm</th>
                                            <th>Marks</th>
                                            <th>Total MM</th>
                                            <th>RFP Marks</th>
                                            <th>Weightage Marks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $fixArr = array('1' => 'Yes', '2' => 'No');
                                        $srn = 0;
                                        $totalmmcount = $rfpcount = $weightagemmcount = 0;
                                        $cegexpID = $this->uri->segment(3);
                                        if ($tqRecArr):
                                            foreach ($tqRecArr as $roRec) {
                                                $srn++;
                                                ?>
                                                <tr>
                                                    <?php
                                                    $empNM = '';
                                                    $empNM2 = '';
                                                    $desiname = DesignationNameById($roRec->designation_id);
                                                    if ($roRec->empname):
                                                        $empNMs = UserNameById($roRec->empname);
                                                        $empNM = $empNMs->userfullname;
                                                    else:
                                                        $empNM2 = UserOtherNameById($roRec->empnameother);
                                                    endif;
                                                    ?>
                                                </tr>
                                            <td><?= $srn; ?></td>
                                            <td><?= $desiname; ?></td>
                                            <td><?= $empNM . " " . $empNM2; ?></td>
                                            <td><?= $roRec->emailaddress; ?></td>
                                            <td><?= $roRec->contactnumber; ?></td>
                                            <td><?= $roRec->age_limit; ?></td>
                                            <td><?= @$fixArr[$roRec->certificate]; ?></td>
                                            <td><?= $roRec->firm; ?></td>
                                            <td><?= $roRec->marks; ?></td>
                                            <td><?= $roRec->man_months; ?><?php $totalmmcount += $roRec->man_months; ?></td>
                                            <td>
                                                <?php
                                                $realrfp = GetRfpByPid_desId($cegexpID, $roRec->designation_id);
                                                echo @$realrfp;
                                                $rfpcount += $realrfp;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $realWei = ($roRec->marks / 100) * $realrfp;
                                                echo $realWei;
                                                $weightagemmcount += $realWei;
                                                ?>
                                            </td>
                                            <?php
                                        }
                                    endif;
                                    ?>   
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th><div class="alert alert-danger"><?php echo $totalmmcount; ?></div></th>
                                            <th><div class="alert alert-success"><?php echo $rfpcount; ?></div></th>
                                            <th><div class="alert alert-info"><?php echo $weightagemmcount; ?></div></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>


                        

</div>
  



           
                    
          

      

        <div class="modal fade" id="Modalweightage" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title">Manage Weightage</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      
                    </div>


                    <div class="modal-body">
                        <form name="updateWeightageForm" id="updateWeightageForm" method="post" action="" enctype="">
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <label> Technical Weightage </label>
                                    <input type="number" min="0" step="any" name="techn_weightage" class="form-control" value="<?= number_format($cegcompcomp[0]->techn_weightage, 2); ?>" >
                                </div>
                                <div class="form-group required col-md-12">
                                    <label> Financial Weightage </label>
                                    <input type="number" min="0" step="any" name="finan_weightage" class="form-control" value="<?= number_format($cegcompcomp[0]->finan_weightage, 2); ?>">
                                </div>   
                                <div class="form-group required col-md-12">
                                    <input type="hidden" name="project_id" class="btn green" value="<?= $bddetail->fld_id; ?>">
                                    <a href="javascript:void(0)" class="btn btn-info pull-right" onclick="updateWeightage()">Save</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal For Add Comptitor Company Details -->
        <div class="modal fade" id="myModalcompt" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title">Add Competitor</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <div class="modal-body">
                        <form name="frmm" id="addCompetitorForm" method="post" action="" enctype="">
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <label>Add Company </label>
                                    <select id="multipleComp" name="leadcompany[]" style="width:100%" multiple>    
                                        <?php foreach ($compnameArr as $rowrec) { ?>
                                            <option value="<?= $rowrec->fld_id; ?>">
                                                <?= ucfirst($rowrec->company_name); ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group required col-md-12">
                                    <input type="hidden" name="project_id" class="btn green" value="<?= $bddetail->fld_id; ?>">
                                    <a href="javascript:void(0)" class="btn btn-info pull-right" onclick="addCompetitor()">Add</a>
                                </div>
                            </div>
                        </form>




                    </div>

                </div>
            </div>
        </div>

       


        <div class="modal fade" id="myModaljvedit" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Coming Soon </h4>
                    </div>

                </div>
            </div>
        </div>

        <!-- Modal For Project Status Code By Durgesh.. content--> 
        <div class="modal fade" id="myModalprojstatus" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 id="tender_title" class="modal-title">Project Status</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <div class="modal-body">
                        <form id="projectStatusForm" action="" method="post">
                            <div class="box-content">
                                <div class="controls">
                                    <label>Project Status</label>
                                    <select required="required" class="form-control"  name="project_status">
                                        <option <?= ($project_status->project_status == '') ? 'selected' : ''; ?> value="">--select--</option>
                                        <option <?= ($project_status->project_status == '0') ? 'selected' : ''; ?> value="0">Awaiting</option>
                                        <option <?= ($project_status->project_status == '1') ? 'selected' : ''; ?> value="1">Won</option>
                                        <option <?= ($project_status->project_status == '2') ? 'selected' : ''; ?> value="2">Loose</option>
                                        <option <?= ($project_status->project_status == '3') ? 'selected' : ''; ?> value="3">Cancel</option>
                                    </select>
                                </div>
                            </div>
                            <div class="box-content">
                                <input type="hidden" id="project_id" name="project_id" value="<?= $bddetail->fld_id; ?>">
                                <a href="javascript:void(0)" class="btn btn-info pull-right mt-4 mb-2" onclick="changeProjectStatus()">Save</a>

                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal For Edit Basic Details -->
        <div class="modal fade" id="myModalbasic" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- <div class="modal-header">
                        <h4 class="modal-title">Project Details</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div> -->
                    <div class="modal-body">
                                           </div>
                </div>
            </div>
        </div>

        <!-- Modal For Edit Summery Details -->
        <div class="modal fade" id="myModalsummery" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Summery of Project</h4>
                        <h4 id="uploaderror2" class="modal-title" style="color:red"></h4>                  
                    </div>
                    <div class="modal-body">
                        <form method="post" action="<?= base_url('company/updatesummery'); ?>" enctype="multipart/form-data">
                            <div class="row">
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Project Code </label>
                                    <input type="text" value="<?= $expres->generated_tenderid; ?>" disabled class="form-control" id="project_code">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Project Number </label>
                                    <select id="project_number" name="project_number" class="form-control col-md-4 col-xs-12">
                                        <option value="" selected >Select Project </option>
                                        <?php
                                        if ($project):
                                            foreach ($project as $val) {
                                                ?>
                                                <option <?= (@$expsummery->project_numberid == $val->id) ? 'selected' : ''; ?> value="<?= $val->id; ?>"> <?= $val->project_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Sector </label>
                                    <!--<input type="text" class="form-control" value="<?= $cegexpSummery->sector; ?>" name="sector" id="sector">-->
                                    <select id="sector_id" name="sector_id" class="form-control col-md-4 col-xs-12">
                                        <option value=""> -- Select Service -- </option>
                                        <?php
                                        if ($sector):
                                            foreach ($sector as $rowr) {
                                                ?>
                                                <option <?= (@$projdetail->sector_id == $rowr->fld_id) ? 'selected' : ''; ?> value="<?= $rowr->fld_id; ?>"> <?= $rowr->sectName; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>

                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Start Date </label>
                                    <input type="date" name="start_date" class="form-control" value="<?= $expsummery->start_date; ?>" id="start_date">
                                </div>  
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">End Date </label>
                                    <input type="date" class="form-control" name="end_date" value="<?= $expsummery->end_date; ?>" id="end_date">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Project Cost </label>
                                    <input type="number"  min="0" step="any" class="form-control" name="project_cost" value="<?= ($expsummery->project_cost > 0) ? $expsummery->project_cost : ''; ?>" id="project_cost">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Revised Contract Value RCV </label>
                                    <input type="number"  min="0" step="any" class="form-control" name="project_rcv" value="<?= ($expsummery->project_rcv > 0) ? $expsummery->project_rcv : ''; ?>" id="project_rcv">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Consultancy Fee </label>
                                    <input type="number"  min="0" step="any" class="form-control" name="consultancy_fee" value="<?= ($expsummery->consultancy_fee > 0) ? $expsummery->consultancy_fee : ''; ?>" id="consultancy_fee">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">CEG Share</label>
                                    <input type="number"  min="0" step="any" class="form-control" name="cegshare_val" value="<?= ($expsummery->cegshare_val > 0) ? $expsummery->cegshare_val : ''; ?>" id="cegshare_val">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Other Share</label>
                                    <input type="number"  min="0" step="any" class="form-control" name="othershare_val" value="<?= ($expsummery->othershare_val > 0) ? $expsummery->othershare_val : ''; ?>" id="othershare_val">
                                </div> 
                                <div class="form-group required col-md-12">
                                    <label class="control-label" for="usr">Lane </label>
                                </div>
                                <div class="form-group fieldGroup col-md-12">
                                    <div class="col-md-5">
                                        <?php
                                        $sec = '2';
                                        ?>
                                        <select id="lane_chk" name="lane_chk[]" class="form-control">
                                            <option value='' selected>-Lane-</option>
                                            <?php
                                            for ($i = $sec; $i <= 12; $i = $i + 2) {
                                                ?>
                                                <option value=<?= $i; ?>><?= $i; ?></option>

                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" name="lane_length[]" class="form-control"/>
                                    </div>
                                    <div class="col-md-2">		
                                        <a href="javascript:void(0)" class="btn btn-success addMore"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> Add</a>
                                    </div>
                                </div>
                                <div class="form-group required col-md-12">
                                    <label class="control-label" for="usr">Length </label>
                                    <input type="text"  class="form-control" name="length" value="<?= $expsummery->length; ?>" id="length">
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">PDS File </label>
                                    <input type="file" class="form-control" name="pds_file"  id="pds_file">
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Certificate File </label>
                                    <input type="file" class="form-control" name="certificate_file" id="certificate_file">
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Contract Agreement </label>
                                    <input type="file" class="form-control" name="contract_agreement_file" id="contract_agreement_file">
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">&nbsp; </label>
                                    <input type="hidden" id="project_id" name="project_id" value="<?= $bddetail->fld_id; ?>">
                                    <input type="submit" required class="btn-primary"  value="Update">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- PDS Edit  -->
        <div class="modal fade" id="mypsdModel" role="dialog">
            <div class="modal-dialog" style="width:80%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">PDS Details</h4>
                    </div>
                    <div class="modal-body">

                        <form name="savepds" id="savepds" method="post" action="<?= base_url('company/savepds'); ?>" enctype="">
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <label class="control-label" for="usr">Project Name </label>
                                    <textarea rows="3" disabled required cols="27" class="form-control"><?= $bddetail->TenderDetails; ?></textarea>
                                </div>

                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Project Location </label>
                                    <?php
                                    $country = $this->mastermodel->countNameById($expres->countries_id);
                                    $state = $this->mastermodel->GetStateNameById($expres->state_id);
                                    ?>
                                    <input type="text" value="<?= $state; ?> , <?= $country; ?>" readOnly class="form-control">
                                </div>

                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Professional staff provided</label>
                                    <input type="text" value="<?= isset($cegpds->professional_staff_provided) ? $cegpds->professional_staff_provided : ''; ?>" name="professional_staff_provided" class="form-control" id="professional_staff_provided">
                                </div> 

                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Client </label>
                                    <input type="text" value="<?= $expres->client; ?>" class="form-control" readOnly>
                                </div>

                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">No. of man-months </label>
                                    <input type="text" value="<?= isset($cegpds->numberof_man_month) ? $cegpds->numberof_man_month : ''; ?>" name="numberof_man_month" class="form-control" id="numberof_man_month">
                                </div>

                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Date of Commencement</label>
                                    <input type="text" value="<?= isset($cegpds->date_of_commencement) ? $cegpds->date_of_commencement : ''; ?>" name="date_of_commencement" class="form-control" id="date_of_commencement">
                                </div>

                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Date of Completion </label>
                                    <input type="text" value="<?= isset($cegpds->stipulated_date_of_completion) ? $cegpds->stipulated_date_of_completion : ''; ?>" name="stipulated_date_of_completion" class="form-control" id="stipulated_date_of_completion">
                                </div>

                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Value of Consultancy Services </label>
                                    <input type="text" value="<?= number_format($expsummery->consultancy_fee, 2); ?>"  class="form-control" readOnly>
                                </div>

                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">Name of Association Firm(s), if any </label>
                                    <br/>
                                    <?php if ($associate_company): ?>
                                        <textarea readOnly rows="5" cols="50"><?= @$compName2; ?></textarea>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">No. of man-months of professional staff provided by associated firm (s)</label>
                                    <input type="text" value="<?= isset($cegpds->mmby_associated_firm) ? $cegpds->mmby_associated_firm : ''; ?>" name="mmby_associated_firm" class="form-control" id="mmby_associated_firm">
                                </div>

                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Senior Staff involved and functions performed</label>
                                    <div>
                                        <?php
                                        if ($tqRecArr):
                                            foreach ($tqRecArr as $roRec) {
                                                ?>
                                                <?php
                                                $empNM = '';
                                                $empNM2 = '';
                                                $desiname = DesignationNameById($roRec->designation_id);
                                                if ($roRec->empname):
                                                    $empNM = UserNameById($roRec->empname);
                                                else:
                                                    $empNM2 = UserOtherNameById($roRec->empnameother);
                                                endif;
                                                ?>
                                                <?= $srn = $srn + 1; ?>. <?= $empNM . " " . $empNM2 . '-'; ?> <?= $desiname . ' , '; ?>
                                                <?php
                                            }
                                        endif;
                                        ?>
                                    </div>	
                                </div>

                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Narrative Description of Project</label>
                                    <textarea name="narrative_description" id="narrative_description" class="form-control">
                                        <?= isset($cegpds->narrative_description) ? $cegpds->narrative_description : ''; ?>
                                    </textarea>
                                </div>

                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Details of Major Bridges</label>
                                    <textarea name="major_bridges" id="major_bridges" class="form-control">
                                        <?= isset($cegpds->major_bridges) ? $cegpds->major_bridges : ''; ?>
                                    </textarea>
                                </div>

                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Details of Urban Flyovers</label>
                                    <textarea name="urbon_flyovers" id="urbon_flyovers" class="form-control">
                                        <?= isset($cegpds->urbon_flyovers) ? $cegpds->urbon_flyovers : ''; ?>
                                    </textarea>
                                </div>

                                <div class="form-group required col-md-6">
                                    <input type="hidden" name="project_id" value="<?= $bddetail->fld_id; ?>">
                                    <input type="submit" class="btn btn-success" value="Update">
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Model Popup For Add Client -->
        <div class="modal fade" id="myModalclientaddress" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        
                        <h4 class="modal-title"> Client Details </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                       
                    </div> 
                    <div class="modal-body">
                        <form name="savepds" id="savepds" method="post" action="<?= base_url('dashboard/clientaddressupdinsert'); ?>" enctype="">
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <label class="control-label" for="usr"> Client Full Address : </label>
                                    <textarea rows="2" name="clientfull_address" id="clientfull_address" required cols="27" class="form-control"><?= ($clientaddrsdetails->full_address) ? $clientaddrsdetails->full_address : ''; ?></textarea>
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr"> City / District : </label>
                                    <input type="text" value="<?= ($clientaddrsdetails->client_city) ? $clientaddrsdetails->client_city : ''; ?>" name="clientcityname" class="form-control" id="clientcityname">
                                </div>  
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr"> Postal Code / Pin : </label>
                                    <input type="number" value="<?= ($clientaddrsdetails->client_postalpin) ? $clientaddrsdetails->client_postalpin : ''; ?>" name="clientpostalcodepin" id="clientpostalcodepin" class="form-control" >
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr"> Client Contact No.: </label>
                                    <input type="number" value="<?= ($clientaddrsdetails->client_contactno) ? $clientaddrsdetails->client_contactno : ''; ?>" name="client_contno" class="form-control" id="client_contno" />
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr"> Contact Person Name : </label>
                                    <input type="text" value="<?= ($clientaddrsdetails->client_cont_pers_name) ? $clientaddrsdetails->client_cont_pers_name : ''; ?>" name="client_contpersname" class="form-control" id="client_contpersname">
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr"> Contact Person No : </label>
                                    <input type="number" value="<?= ($clientaddrsdetails->client_cont_pers_contact) ? $clientaddrsdetails->client_cont_pers_contact : ''; ?>" name="client_contpers_num" class="form-control" id="client_contpers_num">
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr"> Contact Person Email : </label>
                                    <input type="email" value="<?= isset($clientaddrsdetails->client_email_pers) ? $clientaddrsdetails->client_email_pers : ''; ?>" name="client_email_pers" class="form-control" id="client_email_pers">
                                </div>
                                <div class="form-group required col-md-12">
                                    <input type="hidden" name="project_id" value="<?= $bddetail->fld_id; ?>">
                                    <input type="submit" class="btn btn-info pull-right" value="Save">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Model Popup For Edit Client -->
        <div class="modal fade" id="myModaleditclientaddress" role="dialog">
            <div class="modal-dialog" style="width:80%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Edit Client Contact/Address Details </h4>
                    </div> 
                    <div class="modal-body">
                        <form name="savepds" id="savepds" method="post" action="<?= base_url('dashboard/clientaddressupdate'); ?>" enctype="">
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <label class="control-label" for="usr"> Client Full Address : </label>
                                    <textarea rows="2" name="clientfull_address" id="clientfull_address1" required cols="27" class="form-control"></textarea>
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr"> City / District : </label>
                                    <input type="text" value="" name="clientcityname" class="form-control" id="clientcityname1">
                                </div>  
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr"> Postal Code / Pin : </label>
                                    <input type="number" value="" name="clientpostalcodepin" id="clientpostalcodepin1" class="form-control" >
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr"> Client Contact No.: </label>
                                    <input type="number" value="" name="client_contno" class="form-control" id="client_contno1" />
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr"> Contact Person Name : </label>
                                    <input type="text" value="" name="client_contpersname" class="form-control" id="client_contpersname1">
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr"> Contact Person No : </label>
                                    <input type="number" value="" name="client_contpers_num" class="form-control" id="client_contpers_num1">
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr"> Contact Person Email 1: </label>
                                    <input type="email" value="" name="client_email_pers" class="form-control" id="client_email_pers1">
                                </div>

                                <div class="form-group required col-md-6">
                                    <input type="hidden" name="project_id" value="<?= $bddetail->fld_id; ?>">
                                    <input type="hidden" id="ids" name="ids" value="">
                                    <input type="submit" class="btn btn-success" value="Update">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <!-- Project Ofc Address -->
        <div class="modal fade" id="myModalprojectoffcaddress" role="dialog">
            <div class="modal-dialog" style="width:80%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Project Office Address Details </h4>
                    </div> 
                    <div class="modal-body">
                        <form name="savepds" id="savepds" method="post" action="<?= base_url('dashboard/offcaddressupdinsert'); ?>" enctype="">
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <label class="control-label" for="usr"> Full Address : </label>
                                    <textarea rows="2" name="clientfull_address" required cols="27" class="form-control"><?= ($projoffcAddress->full_address) ? $projoffcAddress->full_address : ''; ?></textarea>
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr"> City / District : </label>
                                    <input type="text" value="<?= isset($projoffcAddress->projoffc_city) ? $projoffcAddress->projoffc_city : ''; ?>" name="clientcityname" class="form-control" >
                                </div>  
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr"> Postal Code / Pin : </label>
                                    <input type="number" value="<?= ($projoffcAddress->projoffc_postalpin) ? $projoffcAddress->projoffc_postalpin : ''; ?>" name="clientpostalcodepin" class="form-control" >
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr">  Contact No.: </label>
                                    <input type="number" value="<?= isset($projoffcAddress->projoffc_contactno) ? $projoffcAddress->projoffc_contactno : ''; ?>" name="client_contno" class="form-control"  />
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr"> Contact Person Name : </label>
                                    <input type="text" value="<?= isset($projoffcAddress->projoffc_cont_pers_name) ? $projoffcAddress->projoffc_cont_pers_name : ''; ?>" name="client_contpersname" class="form-control" >
                                </div>
                                <div class="form-group required col-md-4">
                                    <label class="control-label" for="usr"> Contact Person No : </label>
                                    <input type="number" value="<?= isset($projoffcAddress->projoffc_cont_pers_contact) ? $projoffcAddress->projoffc_cont_pers_contact : ''; ?>" name="client_contpers_num" class="form-control" >
                                </div>
                                <div class="form-group required col-md-6">
                                    <input type="hidden" name="project_id" value="<?= $bddetail->fld_id; ?>">
                                    <input type="submit" class="btn green" value="Update">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- copy of input fields group -->
        <div class="form-group fieldGroupCopy col-md-12" style="display: none;">
            <div class="col-md-5">
                <?php
                $sec = '2';
                ?>
                <select id="lane_chk" name="lane_chk[]" class="form-control">
                    <option value="">-Lane-</option>
                    <?php for ($i = $sec; $i <= 12; $i = $i + 2) { ?>
                        <option value=<?= $i; ?>><?= $i; ?></option>
                    <?php }
                    ?>
                </select>
            </div>
            <div class="col-md-5">
                <input type="text" name="lane_length[]" class="form-control" required />
            </div>
            <div class="col-md-2">		
                <a href="javascript:void(0)" class="btn btn-danger remove"><span style="font-size:0px;" class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> Remove</a>
            </div>
        </div> 


        <div class="modal fade" id="modalreplacementeam" role="dialog">
            <div class="modal-dialog" style="width:50%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Replacement Details</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-striped display">
                            <thead>
                                <tr>
                                    <th>Designation Name</th>
                                    <th>Emp Name</th>
                                    <th>Replace Date</th>
                                    <th>MM</th>
                                </tr>
                            </thead>
                            <tbody id="dtrespons">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Key Date Model Section -->
        <div class="modal fade" id="myModalskeydate" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title"> Manage Key Dates  </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      
                    </div>
                    <div class="modal-body">
                        <form name="frmmDate" id="keyfrmmDate" method="post" action="<?= base_url("dashboard/keydatesaddupdate"); ?>" enctype="multipart/form-data">
                            <div class="row">
                                <div class="form-group required col-md-6">  
                                    <label> Date of Bid Submission : </label>
                                    <input readonly="readonly" placeholder="mm/dd/YYYY" type="text" class="form-control" id="date_bid_submission" name="date_bid_submission" value="<?php if (($ReckeydateSingle->date_bid_submission) and ( $ReckeydateSingle->date_bid_submission != '1970-01-01')) { ?><?= date("m/d/Y", strtotime(@$ReckeydateSingle->date_bid_submission)); ?><?php } ?>" >
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> Date of Financial Opening : </label>
                                    <input  readonly="readonly" placeholder="mm/dd/YYYY" type="text" class="form-control" id="date_financialopening" name="date_financialopening" value="<?php if (($ReckeydateSingle->date_financialopening) and ( $ReckeydateSingle->date_financialopening != '1970-01-01')) { ?><?= date("m/d/Y", strtotime(@$ReckeydateSingle->date_financialopening)); ?><?php } ?>" >
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> Date of Letter of Award : </label>
                                    <input readonly="readonly" placeholder="mm/dd/YYYY" type="text" class="form-control" id="date_latter_award" name="date_latter_award" value="<?php if (($ReckeydateSingle->date_latter_award) and ( $ReckeydateSingle->date_latter_award != '1970-01-01')) { ?><?= date("m/d/Y", strtotime(@$ReckeydateSingle->date_latter_award)); ?><?php } ?>" >
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> Date of Contract Agrement : </label>
                                    <input readonly="readonly" placeholder="mm/dd/YYYY" type="text" class="form-control" id="date_contract_agrement" name="date_contract_agrement" value="<?php if (($ReckeydateSingle->date_contract_agrement) and ( $ReckeydateSingle->date_contract_agrement != '1970-01-01')) { ?><?= date("m/d/Y", strtotime(@$ReckeydateSingle->date_contract_agrement)); ?><?php } ?>" >
                                </div>
                                <div class="form-group required col-md-6">
                                    <label>Internal Mou (if any) : </label>
                                    <input readonly="readonly" placeholder="mm/dd/YYYY" type="text" class="form-control" name="internal_mou" id="internal_mou" value="<?php if (($ReckeydateSingle->date_of_mov) and ( $ReckeydateSingle->date_of_mov != '1970-01-01')) { ?><?= date("m/d/Y", strtotime(@$ReckeydateSingle->date_of_mov)); ?><?php } ?>">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label>Client Mou (if any) : </label>
                                    <input readonly="readonly" placeholder="mm/dd/YYYY" type="text" class="form-control" name="client_mou" id="client_mou" value="<?php if (isset($ReckeydateSingle->client_mou) and ( $ReckeydateSingle->client_mou != '1970-01-01')) { ?><?= date("m/d/Y", strtotime($ReckeydateSingle->client_mou)); ?><?php } ?>">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label>Date of Commencement : </label>
                                    <input readonly="readonly" placeholder="mm/dd/YYYY" type="text" class="form-control" name="date_of_commencement" id="date_of_commencement" value="<?php if (isset($ReckeydateSingle->date_of_commencement) and ( $ReckeydateSingle->date_of_commencement != '1970-01-01')) { ?><?= date("m/d/Y", strtotime(@$ReckeydateSingle->date_of_commencement)); ?><?php } ?>">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label> Date of Completion as per contract : </label>
                                    <input readonly="readonly" placeholder="mm/dd/YYYY" type="text" class="form-control" name="date_stipulatedcompletion" id="date_stipulatedcompletion" value="<?php if (isset($ReckeydateSingle->date_stipulatedcompletion) and ( $ReckeydateSingle->date_stipulatedcompletion != '1970-01-01')) { ?><?= date("m/d/Y", strtotime(@$ReckeydateSingle->date_stipulatedcompletion)); ?><?php } ?>">
                                </div>
                                <div class="form-group required col-md-12">
                                    <label>Actual Completion : </label>
                                    <input readonly="readonly" placeholder="mm/dd/YYYY" type="text" class="form-control" name="date_of_assignment" id="date_of_assignment" value="<?php if (isset($ReckeydateSingle->date_of_assignment) and ( $ReckeydateSingle->date_of_assignment != '1970-01-01')) { ?><?= date("m/d/Y", strtotime(@$ReckeydateSingle->date_of_assignment)); ?><?php } ?>">
                                </div>

                                
                                <div class="form-group required col-md-12">
                                    <input type="hidden" name="projectid" class="btn green" value="<?= @$bddetail->fld_id; ?>">
                                    <input type="submit" class="btn btn-info pull-right" value="Save">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <!-- Financial Detail Model Section -->
        <div class="modal fade" id="myModalfinancial" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title"> Financial Detail </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                       
                    </div>

                    <div class="modal-body">
                        <form name="frmmDate" id="frmmDate" method="post" action="<?= base_url("company/updatefinancialdetail"); ?>" enctype="multipart/form-data">
                            <div class="row">

                                <div class="form-group required col-md-12">  
                                    <label>Financial Proprosal: </label>
                                    <input type="text" class="form-control" id="financial_proprosal" name="financial_proprosal" value="<?php if ($financial_record->financial_proprosal) { ?><?= $financial_record->financial_proprosal; ?><?php } ?>" >
                                </div>

                                <div class="form-group required col-md-12">  
                                    <label>Value as per LOA : </label>
                                    <input onchange ="lead();" onkeyup ="lead();" onfocus ="lead();" onclick ="lead();" onchange ="jv();" onkeyup ="jv();" onfocus ="jv();" onclick ="jv();" onchange ="assoc();" onkeyup ="assoc();" onfocus ="assoc();" onclick ="assoc();" type="text" class="form-control" id="value_as_per_loa" name="value_as_per_loa" value="<?php if ($financial_record->value_as_per_loa) { ?><?= $financial_record->value_as_per_loa; ?><?php } ?>">
                                </div>
                            </div>


                            <?php //if (@$competitor_jv_record) { ?>
                                <?php //foreach (@$competitor_jv_record as $jv_record) { ?>
                                    <?php //if ($jv_record->compt_comp_id == '74') { ?>
                                        <?php
                                        //$rrEccT1 = GetCompAlldetails($jv_record->project_id, $jv_record->compt_comp_id, '1');
                                        //$rrEccT2 = GetCompAlldetails($jv_record->project_id, $jv_record->compt_comp_id, '2');
                                        //$rrEccT3 = GetCompAlldetails($jv_record->project_id, $jv_record->compt_comp_id, '3');
                                      $rrEccT1 = GetCompAlldetailsnewash($bddetail->fld_id, '1');
                                      $rrEccT2 = GetCompAlldetailsnewash($bddetail->fld_id, '2');
                                      $rrEccT3 = GetCompAlldetailsnewash($bddetail->fld_id, '3');
									   ?> 
                                        <?php if (@$rrEccT1) { ?>
                                            <div class="row">
                                                <div class="form-group required col-md-4" id="fin_amount1">  
                                                    <label>Name : </label>
                                                    <input type="hidden" name="lead_id" class="btn green" value="<?= @$rrEccT1->lead_comp_id; ?>">
                                                    <input disabled="disabled" type="text" class="form-control" id="lead_name" name="lead_name" value="<?= str_replace("<b>LEAD</b>", "LEAD", $rrEccT1); ?>" >
                                                </div>


                                                <div class="form-group required col-md-4" id="fin_amount1">  
                                                    <label>Amount : </label>
                                                    <input readonly="readonly" type="text" class="form-control" id="lead_amount" name="lead_amount" value="<?php if ($financial_record->lead_amount) { ?><?= number_format($financial_record->lead_amount); ?><?php } ?>" >
                                                </div>

                                                <div class="form-group required col-md-4" id="fin_share1">  
                                                    <label>Share : </label>
                                                    <input onchange ="lead();" onkeyup ="lead();" onfocus ="lead();" onclick ="lead();" type="text" class="form-control" id="lead_share" name="lead_share" value="<?php if ($financial_record->lead_share) { ?><?= $financial_record->lead_share; ?><?php } ?>" >
                                                </div>
                                            </div>
                                        <?php } ?> 

                                        <?php if (@$rrEccT2) { ?>  
                                            <div class="row">    
                                                <div class="form-group required col-md-4" id="fin_amount1">  
                                                    <label>Name : </label>
                                                    <input type="hidden" name="jv_id" class="btn green" value="<?= @$rrEccT2->joint_venture; ?>">
                                                    <input disabled="disabled" type="text" class="form-control" id="jv_name" name="jv_name" value="<?= str_replace("<b>JV</b>", "JV", $rrEccT2); ?>" >
                                                </div>


                                                <div class="form-group required col-md-4" id="fin_amount1">  
                                                    <label>Amount : </label>
                                                    <input readonly="readonly" type="text" class="form-control" id="jv_amount" name="jv_amount" value="<?php if (isset($financial_record->jv_amount)) { ?><?= number_format($financial_record->jv_amount); ?><?php } ?>">
                                                </div>

                                                <div class="form-group required col-md-4" id="fin_share1">  
                                                    <label>Share : </label>
                                                    <input onchange ="jv();" onkeyup ="jv();" onfocus ="jv();" onclick ="jv();" type="text" class="form-control" id="jv_share" name="jv_share" value="<?php if (isset($financial_record->jv_share)) { ?><?= $financial_record->jv_share; ?><?php } ?>">
                                                </div>    
                                            </div>


                                        <?php } ?>

                                        <?php if (@$rrEccT3) { ?>  
                                            <div class="row"> 
                                                <div class="form-group required col-md-4" id="fin_amount1">  
                                                    <label>Name : </label>
                                                    <input type="hidden" name="assoc_id" class="btn green" value="<?= @$rrEccT3->asso_comp; ?>">
                                                    <input disabled="disabled" type="text" class="form-control" id="assoc_name" name="assoc_name" value="<?= str_replace("<b>ASSOCIATE</b>", "ASSOCIATE", $rrEccT3); ?>" >
                                                </div>


                                                <div class="form-group required col-md-4" id="fin_amount1">  
                                                    <label>Amount : </label>
                                                    <input readonly="readonly" type="text" class="form-control" id="assoc_amount" name="assoc_amount" value="<?php if ($financial_record->assoc_amount) { ?><?= number_format($financial_record->assoc_amount); ?><?php } ?>" >
                                                </div>

                                                <div class="form-group required col-md-4" id="fin_share1">  
                                                    <label>Share : </label>
                                                    <input onchange ="assoc();" onkeyup ="assoc();" onfocus ="assoc();" onclick ="assoc();" type="text" class="form-control" id="assoc_share" name="assoc_share" value="<?php if ($financial_record->assoc_share) { ?><?= $financial_record->assoc_share; ?><?php } ?>" >
                                                </div>    
                                            </div>    
                                        <?php } ?>  


                                        <?php
                                    //}
                                //}
                            //}
                            ?>



                            <div class="row">
                                <div class="form-group required col-md-12" id="revised_contract_value1">  
                                    <label>Revised contract Value : </label>
                                    <input type="text" class="form-control" id="revised_contract_value" name="revised_contract_value" value="<?php if (isset($financial_record->Revised_Contract_Value)) { ?><?= $financial_record->Revised_Contract_Value; ?><?php } ?>" >
                                </div>    


                                 
                                <div class="form-group required col-md-12">
                                    <input type="hidden" name="project_id" class="btn green" value="<?= @$bddetail->fld_id; ?>">
                                    <input type="submit" class="btn btn-info pull-right" value="Save">
                                </div>  
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

         	
        <div class="modal fade" id="myModalssiteofcdetails" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title">Site Office Details </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <div class="modal-body">
                        <form name="frmmDate" id="frmmDate" method="post" action="<?= base_url("dashboard/siteofcdetailsave"); ?>" enctype="multipart/form-data">
                            <div class="row">

                                <div class="form-group required col-md-12">
                                    <label> Street Address : </label>
                                    <textarea required autocomplete="off" class="form-control" name="street_address" rows="2"></textarea>
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> Country : </label>
                                    <select required autocomplete="off" id="countrynew" name="countries_idnew" onchange="setstatebycidnew()" class="form-control">
                                        <option value=""> -- Select Country -- </option>
                                        <?php
                                        if ($country_Arr):
                                            foreach ($country_Arr as $rowr) {
                                                ?>
                                                <option value="<?= $rowr->country_id; ?>"> <?= $rowr->country_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> State : </label>
                                    <select required id="statenew" name="state_idnew" autocomplete="off" onchange="setcitybystateid()" class="form-control">
                                        <option value=""> -- Select -- </option>
                                    </select>
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> City : </label>
                                    <select required class="form-control" autocomplete="off" name="city_idnew" id="city_idnew">
                                        <option value=""> -- Select -- </option>
                                    </select>
                                </div>
                                <div class="form-group required col-md-6">
                                    <label> Pin Code : </label>
                                    <input required autocomplete="off" type="number" min="0" maxlength="6" class="form-control" name="siteofc_pincode" id="siteofc_pincode" value="">
                                </div>

                                <div class="form-group required col-md-12">
                                    <label> Cont. Pers. Name : </label>
                                    <select required autocomplete="off" class="form-control" name="cont_persname" id="cont_persname">
                                        <option value=""> -- Select -- </option>
                                        <?php
                                        if ($empListArr) {
                                            foreach ($empListArr as $rowR) {
                                                ?>
                                                <option value="<?= $rowR->user_id; ?>"> <?= $rowR->userfullname . " [ " . $rowR->employeeId . " ]"; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                
                                <div class="form-group required col-md-12">
                                <!--<input type="hidden" name="ofc_mnger_empid" class="btn green" value="<? //= @$projofc_mngremp->id;              ?>">-->
                                    <input type="hidden" name="projectid" class="btn green" value="<?= @$bddetail->fld_id; ?>">
                                    <input type="submit" class="btn btn-info pull-right" value="Save">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- modal for Edit site office details -->
        <div class="modal fade" id="myModalseditsiteofcdetails" role="dialog">
            <div class="modal-dialog" style="width:50%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Edit Site Office Details </h4>
                    </div>
                    <div class="modal-body">
                        <form name="frmmDate" id="frmmDate" method="post" action="<?= base_url("dashboard/siteofcdetailupdate"); ?>" enctype="multipart/form-data">
                            <div class="row">

                                <div class="form-group required col-md-12">  
                                    <label> Street Address : </label>
                                    <textarea required autocomplete="off" class="form-control" id="street_address1" name="street_address" rows="2"></textarea>
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> Country : </label>
                                    <select required autocomplete="off" id="countrynew1" name="countries_idnew"  onchange="setstate()" class="form-control">
                                        <option value=""> -- Select Country -- </option>
                                        <?php
                                        if ($country_Arr):
                                            foreach ($country_Arr as $rowr) {
                                                ?>
                                                <option value="<?= $rowr->country_id; ?>"> <?= $rowr->country_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> State : </label>
                                    <select required id="statenew1" name="state_idnew" autocomplete="off"  onchange="setcity()" class="form-control">
                                        <option value=""> -- Select -- </option>
                                    </select>
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> City : </label>
                                    <select required class="form-control" autocomplete="off" name="city_idnew" id="city_idnew1">
                                        <option value=""> -- Select -- </option>
                                    </select>
                                </div>
                                <div class="form-group required col-md-6">
                                    <label> Pin Code : </label>
                                    <input required autocomplete="off" type="number" min="0" maxlength="6" class="form-control" name="siteofc_pincode" id="siteofc_pincode1" value="">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label> Contact Person Name : </label>
                                    <select required autocomplete="off" class="form-control" name="cont_persname" id="cont_persname1">
                                        <option value=""> -- Select -- </option>
                                        <?php
                                        if ($empListArr) {
                                            foreach ($empListArr as $rowR) {
                                                ?>
                                                <option value="<?= $rowR->user_id; ?>"> <?= $rowR->userfullname . " [ " . $rowR->employeeId . " ]"; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group required col-md-8">
                                    &nbsp;
                                </div>
                                <div class="form-group required col-md-2">
                                    <input type="hidden" name="ofc_mnger_empid" class="btn green" value="<?= @$projofc_mngremp->id; ?>">
                                    <input type="hidden" name="projectid" class="btn green" value="<?= @$bddetail->fld_id; ?>">
                                    <input type="hidden" id="id_site" name="id_site" class="btn green" value="">
                                    <input type="submit" class="btn btn-success" value="Update">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="myModaltlofcmngr" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title">Project Contact Personal Details</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div> 
                    <div class="modal-body">
                       
                    </div>
                </div>
            </div>
        </div>



        <div class="modal fade" id="myModalscore" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title">Generate Rank</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     




                    </div>
                    <div class="modal-body">
                        <form name="generateRankForm" id="generateRankForm" method="post" action="" enctype="">
                            <div class="row">
                                <div class="form-group required col-md-6">
                                    <label> Technical Weightage </label>
                                    <input type="number" readonly min="0" step="any" name="techn_weightage" class="form-control" id="techn_weightage">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label> Financial Weightage </label>
                                    <input type="number" readonly min="0" step="any" name="finan_weightage" class="form-control" id="finan_weightage">
                                </div>
                                <div class="form-group required col-md-6">
                                    <label> Technical </label>
                                    <input type="number" min="0" step="any" name="technical_score" class="form-control" id="technical_score">
                                </div>

                                <div class="form-group required col-md-6">
                                    <label> Financial  </label>
                                    <input type="number" step="any" name="financial_score" class="form-control" id="financial_score">
                                </div>

                                <div class="form-group required col-md-12">
                                    <input type="hidden" name="project_id" class="btn green" value="<?= $bddetail->fld_id; ?>">
                                    <input type="hidden" name="actid" id="actionid" >
                                    <a href="javascript:void(0)" class="btn btn-info pull-right mr-0" onclick="generateRank()">Save</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal For Add Comptitor Company Details -->
        <div class="modal fade" id="myModalcompt" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Comptitor</h4>
                    </div>
                    <div class="modal-body">
                        <form name="frmm" id="frmm" method="post" action="<?= base_url('company/addcomptcompn'); ?>" enctype="">
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <label>Add Company </label>
                                    <select id="multiple" name="leadcompany[]" style="width:100%" multiple>    
                                        <?php foreach ($compnameArr as $rowrec) { ?>
                                            <option value="<?= $rowrec->fld_id; ?>">
                                                <?= ucfirst($rowrec->company_name); ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group required col-md-6">
                                    <input type="hidden" name="project_id" class="btn green" value="<?= $bddetail->fld_id; ?>">
                                    <input type="submit" class="btn green" value="Add">
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <!--//Add Joint Venture Code Bty Asheesh -->

      



        <div class="modal fade" id="myModaljv" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                         <h4 class="modal-title"> Consortium </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                       
                        <!--<h5 class="modal-title" align="center"> Sole : <input type="checkbox" checked id="soleornot1"> </h5>-->
                    </div> 
                    <form name="jvConsortium" id="jvConsortium" method="post" action="" enctype="">
                        <div class="modal-body" id="jvsection1">
                            <div class="form-group">
                                <label>Lead Company </label>
                                <select   id="multipleConsortium" name="leadcompany[]" style="width:100%" multiple>    
                                    <?php foreach ($compnameArr as $rowrec) {
                                        ?>
                                        <option value="<?= $rowrec->fld_id; ?>">
                                            <?= ucfirst($rowrec->company_name); ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">   
                                <label> JV (<small> joint venture </small>)</label>
                                <select   id="multiple1" name="joint_venture[]" style="width:100%" multiple>    
                                    <?php foreach ($compnameArr as $rowrec) { ?>
                                        <option value="<?= $rowrec->fld_id; ?>">
                                            <?= ucfirst($rowrec->company_name); ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label> Associate company..</label>
                                <select   id="multiple2" name="assoc[]" style="width:100%" multiple>    
                                    <?php foreach ($compnameArr as $rowrec) { ?>
                                        <option value="<?= $rowrec->fld_id; ?>">
                                            <?= ucfirst($rowrec->company_name); ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <br>
                            <div class="form-group required col-md-12">
                                <input type="hidden" name="project_id" value="<?= $bddetail->fld_id; ?>" id="project_id">
                                <input type="hidden" name="leadcompanyid" value="" id="leadcompanyid">
                                <a href="javascript:void(0)" onclick="saveJvConsortium()"  class="btn pull-right btn-info mb-4">Save</a>

                            </div>


                        </div>
                    </form>  
                </div>
            </div>
        </div>

      
        <div class="modal fade" id="myModaljvedit" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Coming Soon </h4>
                    </div>
                    <!--       <div class="modal-body">
                    <form name="frmm" id="frmm" method="post" action="<?= base_url('company/updatejointventure'); ?>" enctype="">
                    <div class="row">
                    <div class="form-group required col-md-12">
                    <label>Add Company </label>
                    <select id="multiplejvUPD" name="leadcompanyUPD[]" style="width:100%" multiple>    
                    <?php //foreach ($compnameArr as $rowrec) {          ?>
                    <option value="<?php // $rowrec->fld_id;                              ?>">
                    <?php // ucfirst($rowrec->company_name);          ?>
                    </option>
                    <?php // }          ?>
                    </select>
                    </div>
                    <div class="form-group required col-md-6">
                    <input type="hidden" name="projid" value="<?= $cegexpRec->fld_id; ?>">
                    <input type="hidden" id="base_company" value="">
                    <input type="submit" class="btn green" value="Update">
                    </div>
                    </div>
                    </form>
                    </div>-->
                </div>
            </div>
        </div>

        <!-- modal for Edit pbg details -->
        <div class="modal fade" id="myModaleditpbgdetail" role="dialog">
            <div class="modal-dialog" style="width:50%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Edit Pbg Details </h4>
                    </div>
                    <div class="modal-body">
                        <form name="frmmDate" id="frmmDate" method="post" action="<?= base_url("company/updateperformancepbgdetail"); ?>" enctype="multipart/form-data">
                            <div class="row">
                                <div class="form-group required col-md-6">
                                    <label for="">PBG: </label>
                                    <select disabled="disabled" class="form-control" name="pbg_name1" id="pbg_name1">
                                        <option value="">-Select PBG-</option>
                                        <option <?= ($perf_bd_record->pbg == 'Yes') ? "Selected" : ''; ?> value="Yes">Yes</option>
                                        <option <?= ($perf_bd_record->pbg == 'No') ? "Selected" : ''; ?> value="No">No</option>
                                    </select>
                                </div>

                            
                                <div class="form-group required col-md-6">  
                                    <label> PBG Amount : </label>
                                    <input disabled="disabled" type="text" class="form-control" id="pbg_amount" name="pbg_amount" value="<?= $perf_bd_record->pbg_amount; ?>"></option>
                                    </select>
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> PBG Number: </label>
                                    <input disabled="disabled" type="text" class="form-control" id="pbg_number" name="pbg_number" value="<?= $perf_bd_record->pbg_number; ?>">
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> Bank : </label>
                                    <input disabled="disabled" type="text" class="form-control" id="pbg_bank" name="pbg_bank" value="<?= $perf_bd_record->pbg_bank; ?>">
                                </div>

                                <div class="form-group required col-md-6">
                                    <label> PBG Start : </label>
                                    <input type="date" onkeydown="return false" class="form-control" id="pbg_start" name="pbg_start" value="<?= $perf_bd_record->pbg_start; ?>">
                                </div>

                                <div class="form-group required col-md-6">  
                                    <label> PBG Validity : </label>
                                    <input type="date" onkeydown="return false" class="form-control" id="pbg_validity" name="pbg_validity" value="<?= $perf_bd_record->pbg_validity; ?>">
                                </div>


                                <div class="form-group required col-md-8">
                                    &nbsp;
                                </div>
                                <div class="form-group required col-md-2">
                                    <input type="hidden" name="project_id" class="btn green" value="<?= @$bddetail->fld_id; ?>">
                                    <input type="hidden" id="id_pbg" name="id_pbg" class="btn green" value="">
                                    <input type="submit" class="btn btn-success" value="Update">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>


        <!-- modal for Edit pbg details -->
        <div class="modal fade" id="myModaleditabgdetail" role="dialog">
            <div class="modal-dialog" style="width:50%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Edit Abg Details </h4>
                    </div>

                 
                    <div class="modal-body">
                        <form name="frmmDate" id="frmmDate" method="post" action="<?= base_url("company/updateperformanceabgdetail"); ?>" enctype="multipart/form-data">
                            <div class="row">
                                <div class="form-group required col-md-6">
                                    <label for="">ABG: </label>
                                    <select disabled="disabled" class="form-control" name="abg_name1" id="abg_name1">
                                        <option value="">-Select ABG-</option>
                                        <option <?= ($perf_bd_record->abg == 'Yes') ? "Selected" : ''; ?> value="Yes">Yes</option>
                                        <option <?= ($perf_bd_record->abg == 'No') ? "Selected" : ''; ?> value="No">No</option>
                                    </select>
                                </div>
                              


                                <div class="form-group required col-md-6">  
                                    <label> ABG Amount : </label>
                                    <input disabled="disabled" type="text" class="form-control" id="abg_amount" name="abg_amount" value="<?= $perf_bd_record->abg_amount; ?>"></option>
                                    </select>
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> ABG Number: </label>
                                    <input disabled="disabled" type="text" class="form-control" id="abg_number" name="abg_number" value="<?= $perf_bd_record->abg_number; ?>">
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> Bank : </label>
                                    <input disabled="disabled" type="text" class="form-control" id="abg_bank" name="abg_bank" value="<?= $perf_bd_record->abg_bank; ?>">
                                </div>

                                <div class="form-group required col-md-6">
                                    <label> ABG Start : </label>
                                    <input type="date" onkeydown="return false" class="form-control" id="abg_start" name="abg_start" value="<?= $perf_bd_record->abg_start; ?>">
                                </div>

                                <div class="form-group required col-md-6">  
                                    <label> ABG Validity : </label>
                                    <input type="date" onkeydown="return false" class="form-control" id="abg_validity" name="abg_validity" value="<?= $perf_bd_record->abg_validity; ?>">
                                </div>


                                <div class="form-group required col-md-8">
                                    &nbsp;
                                </div>
                                <div class="form-group required col-md-2">
                                    <input type="hidden" name="project_id" class="btn green" value="<?= @$bddetail->fld_id; ?>">
                                    <input type="hidden" id="id_abg" name="id_abg" class="btn green" value="">
                                    <input type="submit" class="btn btn-success" value="Update">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        






               


                <?php $this->load->view('admin/includes/footer'); ?>


                <!-- Code For Multi Select By Asheesh -->
                <link href="<?= FRONTENDASSETS; ?>fm.selectator.jquery.css" rel="stylesheet" type="text/css">
                <script src="<?= FRONTENDASSETS; ?>fm.selectator.jquery.js"></script> 
                <script src="<?= FRONTENDASSETS; ?>ckeditor/ckeditor.js"></script> 
                <script src="<?= FRONTENDASSETS; ?>js/jquery.multiselect.js"></script>


                  
                <!-- #############   -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.18.10/slimselect.min.js"></script>
                <script src="<?= FRONTASSETS; ?>js/jquery-editable-select.js"></script>

                <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
                <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
                <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

                <!-- ########### -->


            
                <script>
                     $(document).ready(function(){

                        // list3
                        // mVal3
                        // mText3
                        // client_contact
                        // client_address

                        var pbg_val = "<?php echo $perf_bd_record->pbg; ?>";
                        if (pbg_val == 'Yes') {
                        $('#pbg_row').show();
                        } else {
                        $('#pbg_row').hide();
                        }

                        var abg_val = "<?php echo $perf_bd_record->abg; ?>";
                        if (abg_val == 'Yes') {
                        $('#abg_row').show();
                        } else {
                        $('#abg_row').hide();
                        }
                        

                         $("#client_contact").val("<?php echo $clientcontactData->client_contact;?>");
                         $("#client_address").val("<?php echo $clientcontactData->client_address;?>");


                            $("#sf_bankname").val("<?php echo $projdetail->sf_bankname;?>");
                            $("#sf_bgnumber").val("<?php echo $projdetail->sf_bgnumber;?>");
                            $("#sf_dateofstart").val("<?php echo $projdetail->sf_dateofstart;?>");
                            $("#rfp_valdity").val("<?php echo $projdetail->rfp_valdity;?>");
                            $("#rpf_bankname").val("<?php echo $projdetail->rpf_bankname;?>");
                            $("#rpf_bgnumber").val("<?php echo $projdetail->rpf_bgnumber;?>");
                            $("#rpf_dateofstart").val("<?php echo $projdetail->rpf_dateofstart;?>");
                            $("#pf_valdity").val("<?php echo $projdetail->pf_valdity;?>");
                            $("#pf_bankname").val("<?php echo $projdetail->pf_bankname;?>");
                            $("#pf_bgnumber").val("<?php echo $projdetail->pf_bgnumber;?>");
                            $("#pf_dateofstart").val("<?php echo $projdetail->pf_dateofstart;?>");
                       
 

                            $("#bid_security").val("<?php echo $projdetail->bid_security;?>");
                            var comp_bs = "<?php echo $projdetail->bid_security;?>";
                            if (comp_bs == 1) {
                                $('#security_demnd').css('display', 'block');

                            } else {
                                $('#security_demnd').css('display', 'none');
                            }


                            $("#bid_securitytype").val("<?php echo $projdetail->bid_securitytype;?>");
                            var comp_bst = "<?php echo $projdetail->bid_securitytype;?>";
                            if (comp_bst == 5) {
                                $('#showSpfBg').css('display', 'block');
                            } else {
                                $('#showSpfBg').css('display', 'none');
                            }



                            $('#amount').val("<?php echo $projdetail->amount;?>");
                            $('#bs_valdity').val("<?php echo $projdetail->bs_valdity;?>");
                            $("#rfp_cost").val("<?php echo $projdetail->rfp_cost;?>");

                            var comp_rc = "<?php echo $projdetail->rfp_cost;?>";
                            if (comp_rc == 1) {
                                $('#security_demnd2').css('display', 'block');
                            } else {
                                $('#security_demnd2').css('display', 'none');
                            }

                            $("#rfp_amount").val("<?php echo $projdetail->rfp_amount;?>");
                            $('#rfp_type').val("<?php echo $projdetail->rfp_type;?>");

                             var comp_rt = "<?php echo $projdetail->rfp_type;?>";
                            if (comp_rt == 5) {
                                $('#showRpfBg').css('display', 'block');
                            } else {
                                $('#showRpfBg').css('display', 'none');
                            }



                            $('#bs_valdity').val("<?php echo $projdetail->bs_valdity;?>");

                            $("#processing_fee").val("<?php echo $projdetail->processing_fee;?>");


                             var comp_pf = "<?php echo $projdetail->processing_fee;?>";
                            if (comp_pf == 1) {
                                $('#security_demnd3').css('display', 'block');
                            } else {
                                $('#security_demnd3').css('display', 'none');
                            }

                            $('#pfee_amount').val("<?php echo $projdetail->pfee_amount;?>");
                            $('#processing_fee_type').val("<?php echo $projdetail->processing_fee_type;?>");

                             var comp_pft = "<?php echo $projdetail->processing_fee_type;?>";
                            if (comp_pft == 5) {
                                $('#showPfBg').css('display', 'block');
                            } else {
                                $('#showPfBg').css('display', 'none');
                            }

                            


                         
                         let arrLead, arrJoint, arrAssoc;
                            var dataLead = "<?php echo $consortiumData->lead_companes;?>";

                            
                            
                            if (dataLead) {

                                if (dataLead.indexOf(",") > 0) {
                                    arrLead = dataLead.split(',');
                                } else {
                                    arrLead = [<?php echo $consortiumData->lead_companes?>];
                                }

                            }
                            var dataJoint = "<?php echo $consortiumData->joint_ventures?>"; 
                            if (dataJoint) {
                                if (dataJoint.indexOf(",") > 0) {
                                    arrJoint = dataJoint.split(',');
                                } else {
                                    arrJoint = [<?php echo $consortiumData->joint_ventures?>];

                                }
                            }



                            var dataAssoc = "<?php echo $consortiumData->associate_company?>";
                            if (dataAssoc) {
                                if (dataAssoc !== null && dataAssoc.indexOf(",") > 0) {
                                    arrAssoc = dataAssoc.split(',');
                                } else {
                                    arrAssoc = [<?php echo $consortiumData->associate_company;?>];
                                }
                            }












                             var x = new SlimSelect({
                                select: '#leadCom',
                                hideSelectedOption: true,
                                
                            });



                            var y = new SlimSelect({
                                select: '#jointCom',
                                hideSelectedOption: true,
                               
                            });



                            var z = new SlimSelect({
                                select: '#assoCom',
                                hideSelectedOption: true
                            });



                             var a = new SlimSelect({
                                select: '#multipleComp',
                                hideSelectedOption: true
                            });



                           




                            if (Array.isArray(arrLead) && arrLead.length) {
                                x.set(arrLead)
                            }

                            if (Array.isArray(arrJoint) && arrJoint.length) {
                                y.set(arrJoint)
                            }

                            if (Array.isArray(arrAssoc) && arrAssoc.length) {
                                z.set(arrAssoc)
                            }




                            var mVal1 = "<?= $bddetail->client_id; ?>";
                             console.log("++++++" + mVal1);
                             editableFunc(mVal1);


                              editableFuncClientContact(mVal1);


                             var mVal2 = "<?= $bddetail->bd_funding_master_id; ?>";
                             console.log(mVal2);
                             editableFuncSecond(mVal2);

                              var mVal3 = "<?= $bddetail->fld_id;?>";
                              savedClientContact(mVal3);



                        });





function jvConsortiumSlimSelect(slimSelect1,slimSelect2,slimSelect3){
                        slimSelectn1 = [];
                        slimSelectn2 = [];
                        slimSelectn3 = [];

                        var b = new SlimSelect({
                        select: '#multipleConsortium',
                        hideSelectedOption: true
                        });

                        b.set(slimSelectn1);

                        if (slimSelect1) {
                        if (slimSelect1 !== null && slimSelect1.indexOf(",") > 0) {
                            slimSelectn1 = slimSelect1.split(',');
                        } else {
                            slimSelectn1 = [slimSelect1];
                        }
                        }

                        if (Array.isArray(slimSelectn1) && slimSelectn1.length) {
                            b.set(slimSelectn1)
                        }

                        var c = new SlimSelect({
                        select: '#multiple1',
                        hideSelectedOption: true
                        });

                        c.set(slimSelectn2);

                        if (slimSelect2) {
                        if (slimSelect2 !== null && slimSelect2.indexOf(",") > 0) {
                        slimSelectn2 = slimSelect2.split(',');
                        } else {
                        slimSelectn2 = [slimSelect2];
                        }
                        }

                        if (Array.isArray(slimSelectn2) && slimSelectn2.length) {
                        c.set(slimSelectn2)
                        }

                        var d = new SlimSelect({
                        select: '#multiple2',
                        hideSelectedOption: true
                        });

                         d.set(slimSelectn3);

                        if (slimSelect3) {
                        if (slimSelect3 !== null && slimSelect3.indexOf(",") > 0) {
                            slimSelectn3 = slimSelect3.split(',');
                        } else {
                            slimSelectn3 = [slimSelect3];
                        }
                        }

                        if (Array.isArray(slimSelectn3) && slimSelectn3.length) {
                            d.set(slimSelectn3)
                        }

}

            

 function jvforcompsetid(jvcompid,slimSelect1,slimSelect2,slimSelect3) {


    $('#myModaljv').modal('toggle');
    jvConsortiumSlimSelect(slimSelect1,slimSelect2,slimSelect3);
                       

                        $('#leadcompanyid').val(jvcompid);
                        var companyid = jvcompid;
                        var projectID = <?= $bddetail->fld_id; ?>;
                        $.ajax({
                            url: '<?= base_url('company/getcomptdata') ?>',
                            method: 'post',
                            data: {'companyid': companyid, 'projectID': projectID},
                            success: function (response) {
                                if (response) {
                                    var data = jQuery.parseJSON(response);
                                    var numbersString = data.lead_comp_id;
                                    var numbersArray = numbersString.split(',');

                                    // $.each(numbersArray, function (index, value) {


                                         
                                    //     //$(".leaddataid option[value='" + value + "']").attr("selected", "selected");
                                    // });

                                }

                               

                               

                            },

                        });

                    }







                    $('.lane_id').click(function () {
                        if (confirm("Are you sure you want to remove")) {
                            var id = $(this).attr('data-laneid');
                            var url = '<?= base_url('company/ceg_laneRemove'); ?>';
                            $.ajax({
                                type: 'POST',
                                url: url,
                                data: {id: id},
                                success: function (res) {
                                    location.reload(1);
                                },
                            });
                        } else {
                            return false;
                        }
                    });



                    //////////////////////
                    // function jvforcompsetid(jvcompid) {
                    //     $('#leadcompanyid').val(jvcompid);
                    //     var companyid = jvcompid;
                    //     var projectID = <?= $bddetail->fld_id; ?>;
                    //     $.ajax({
                    //         url: '<?= base_url('company/getcomptdata') ?>',
                    //         method: 'post',
                    //         data: {'companyid': companyid, 'projectID': projectID},
                    //         success: function (response) {
                    //             console.log(response);
                    //             if (response) {
                    //                 var data = jQuery.parseJSON(response);
                    //                 var numbersString = data.lead_comp_id;
                    //                 var numbersArray = numbersString.split(',');
                    //                 console.log(numbersArray);
                    //                 //console.log(data.lead_comp_id);
                    //                 //$(".leaddataid option[value='763']").attr("selected", "selected");
                    //                 //$('.id_100 option[value=val2]').attr('selected','selected');
                    //                 $.each(numbersArray, function (index, value) {
                    //                     //$('.leaddataid').val(value).attr("selected", "selected");
                    //                     //jQuery(".leaddataid option:selected").val(value);
                    //                     $(".leaddataid option[value='" + value + "']").attr("selected", "selected");
                    //                 });

                    //             }
                    //         },
                    //     });
                    // }

                    /////////////////
                    $(document).ready(function () {
                        $('#service2_name_id').on('change', function () {
                            var service2_name = $(this).val();
                            var sector_name = $('#sector_name_id').val();
                            var service1_name = $('#service1_name_id').val();
                            CKEDITOR.instances.service_description.setData('');
                            if (sector_name != '' && service2_name != '' && service1_name != '') {
                                var url = '<?= base_url('company/getPDSDesc_ajax') ?>';
                                $.ajax({
                                    type: 'POST',
                                    url: url,
                                    data: {'service2_name': service2_name, 'sector_name': sector_name, 'service1_name': service1_name},
                                    success: function (res) {
                                        var data = JSON.parse(res);
                                        //$('#service_description').val(data.desc);
                                        CKEDITOR.instances['service_description'].setData(data.desc);
                                    },
                                });
                            } else {
                                alert('Please Select All Type');
                            }

                        });


                        //////////////////////
                        $('#sector_name_id').on('change', function () {
                            var sectID = $(this).val();
                            $('#service1_name_id').empty();
                            if (sectID != '') {
                                $.ajax({
                                    type: 'POST',
                                    url: '<?= base_url('company/getservice1_ajax') ?>',
                                    data: {'secid': sectID},
                                    success: function (res) {
                                        var data = JSON.parse(res);
                                        $("#service1_name_id").append("<option>--Select--</option>");
                                        for (var i = 0; i < data.length; i++) {
                                            $("#service1_name_id").append("<option value=" + data[i].id + ">" + data[i].service_name + "</option>");
                                        }
                                    },
                                });
                            } else {
                                alert('Please Select Sector');
                            }
                        });


                        /////////////////
                        $('#service1_name_id').on('change', function () {
                            var sectID = $('#sector_name_id').val();
                            var service1_name_id = $(this).val();
                            $('#service2_name_id').empty();
                            if (service1_name_id != '' && sectID != '') {
                                $.ajax({
                                    type: 'POST',
                                    url: '<?= base_url('company/getservice2_ajax') ?>',
                                    data: {'secid': sectID, 'service1_name_id': service1_name_id},
                                    success: function (res) {
                                        var data = JSON.parse(res);
                                        $("#service2_name_id").append("<option>--Select--</option>");
                                        for (var i = 0; i < data.length; i++) {
                                            $("#service2_name_id").append("<option value=" + data[i].id + ">" + data[i].service_name + "</option>");
                                        }
                                    },
                                });
                            } else {
                                alert('Please Select');
                            }
                        });
                    });

                </script>

                <script>
                    //Validate..
                    function setupdid(valfield) {
                        $("#actionid").val(valfield);
                        $.ajax({
                            type: 'POST',
                            dataType: "text",
                            url: "<?= base_url('company/ajax_score_byid'); ?>",
                            data: {'editId': valfield},
                            success: function (responData) {
                                var parsed = JSON.parse(responData);
                                var arr = [];
                                for (var x in parsed) {
                                    arr.push(parsed[x]);
                                }
                                $("#techn_weightage").val(arr[4]);
                                $("#finan_weightage").val(arr[5]);
                                $("#technical_score").val(arr[6]);
                                $("#technical_marks").val(arr[7]);
                                $("#financial_score").val(arr[8]);
                                $("#financial_marks").val(arr[9]);
                            },
                        });
                    }
                </script>   
                <style>
                    .tab {
                        overflow: hidden;
                        border: 1px solid #ccc;
                        background-color: #f1f1f1;
                    }
                    /* Style the buttons inside the tab */
                    .tab button {
                        background-color: inherit;
                        float: left;
                        border: none;
                        outline: none;
                        cursor: pointer;
                        padding: 14px 16px;
                        transition: 0.3s;
                        font-size: 17px;
                    }
                    /* Change background color of buttons on hover */
                    .tab button:hover {
                        background-color: #ddd;
                    }
                    /* Create an active/current tablink class */
                    .tab button.active {
                        background-color: #ccc !important;
                    }
                    /* Style the tab content */
                    .tabcontent {
                        display: none;
                        padding: 6px 12px;
                        border: 1px solid #ccc;
                        border-top: none;
                    }
                    /* Style the close button */
                    .topright {
                        float: right;
                        cursor: pointer;
                        font-size: 28px;
                    }
                    .topright:hover {color: red;}
                    /*        .dt-buttons, .dataTables_filter {
                            display: none;
                        }*/
                    .panel-title a[aria-expanded="true"] {
                        background: #1c4e7f!important;
                        padding: 10px 30px 10px 10px!important;
                        border-radius: 5px!important;
                        margin: -15px!important;
                    }   
                    .panel-title a[aria-expanded="true"]:active {
                        background: #1c4e7f!important;
                        padding: 10px 30px 10px 10px!important;
                        border-radius: 5px!important;
                        margin: -15px!important;
                    }  
                    .panel-title a[data-toggle="collapse"] {
                        padding: 10px 30px 10px 10px!important;
                        margin: -15px!important;
                    }
                </style>
                <script>
                    // function openCity(evt, cityName) {
                    //     var i, tabcontent, tablinks;
                    //     tabcontent = document.getElementsByClassName("tabcontent");
                    //     for (i = 0; i < tabcontent.length; i++) {
                    //         tabcontent[i].style.display = "none";
                    //     }
                    //     tablinks = document.getElementsByClassName("tablinks");
                    //     for (i = 0; i < tablinks.length; i++) {
                    //         tablinks[i].className = tablinks[i].className.replace(" active", "");
                    //     }
                    //     document.getElementById(cityName).style.display = "block";
                    //     evt.currentTarget.className += " active";
                    // }
                    // // Get the element with id="defaultOpen" and click on it
                    // document.getElementById("defaultOpen").click();
                </script> 
                <script>
                    // code by durgesh For Financial details

                    // lead caculation
                    function lead() {
                        var val_as_per_loa = $('#value_as_per_loa').val();
                        var val_per_loa = val_as_per_loa.replace(/\,/g, '');
                        var lead_share = $('#lead_share').val();
                        var lead = lead_share.replace(/\,/g, '');
                        var lead_amount = parseFloat(((val_per_loa) * (lead) / 100));
                        $('#lead_amount').val(lead_amount);
                    }
                    // jv caculation
                    function jv() {
                        var val_as_per_loa = $('#value_as_per_loa').val();
                        var val_per_loa = val_as_per_loa.replace(/\,/g, '');
                        var jv_share = $('#jv_share').val();
                        var jv = jv_share.replace(/\,/g, '');
                        var jv_amount = parseFloat(((val_per_loa) * (jv) / 100));
                        $('#jv_amount').val(jv_amount);
                    }
                    // Assoc caculation
                    function assoc() {
                        var val_as_per_loa = $('#value_as_per_loa').val();
                        var val_per_loa = val_as_per_loa.replace(/\,/g, '');
                        var assoc_share = $('#assoc_share').val();
                        var assoc = assoc_share.replace(/\,/g, '');
                        var assoc_amount = parseFloat(((val_per_loa) * (assoc) / 100));
                        $('#assoc_amount').val(assoc_amount);
                    }
                </script>


                <script>
                    // code by durgesh For Key dates details
                    $(function () {
                        $("#date_bid_submission,#date_financialopening,#date_latter_award,#date_contract_agrement,#internal_mou,#client_mou,#date_of_commencement,#date_stipulatedcompletion,#date_of_assignment").datepicker({
                            /*startDate: new Date(),*/
                            format: 'mm/dd/yyyy',
                            todayHighlight: true,
                            autoclose: true,
                            ignoreReadonly: true,
                            defaultDate: new Date(),

                        });
                    });
                </script>

                <script>
                    // code by durgesh for edit client details
                    function editclient(id) {
                        $.ajax({
                            url: '<?= base_url('dashboard/edit_client'); ?>/' + id,
                            type: "GET",
                            dataType: "json",
                            success: function (data) {
                                $('#ids').val(data.fld_id);
                                $('#clientfull_address1').html(data.full_address);
                                $('#clientcityname1').val(data.client_city);
                                $('#clientpostalcodepin1').val(data.client_postalpin);
                                $('#client_contno1').val(data.client_contactno);
                                $('#client_contpersname1').val(data.client_cont_pers_name);
                                $('#client_contpers_num1').val(data.client_cont_pers_contact);
                                $('#client_email_pers1').val(data.client_email_pers);
                            }
                        });
                    }
                </script>

                <script>
                    // code by durgesh for edit site office details
                    function editsiteofficeaddress(id) {
                        $.ajax({
                            url: '<?= base_url('dashboard/edit_site_office_address'); ?>/' + id,
                            type: "GET",
                            dataType: "json",
                            success: function (data) {
                                $('#street_address1').html(data.street_address);
                                $('#siteofc_pincode1').val(data.siteofc_pincode);
                                $('#countrynew1').val(data.countries_idnew);
                                $('#cont_persname1').val(data.cont_persname);
                                $('#id_site').val(data.fld_id);
                                var countID = data.countries_idnew;
                                var stateID = data.state_idnew;
                                var cityID = data.city_idnew;
                                statelistbyid(countID, stateID);
                                citylistbyid(stateID, cityID);
                            }
                        });
                    }

                    // code by durgesh for get selected state
                    function statelistbyid(countID, stateID) {
                        $.ajax({
                            url: '<?= base_url('company/ajax_state_getbycid') ?>',
                            method: 'post',
                            data: {country: countID},
                            dataType: 'json',
                            success: function (response) {
                                $.each(response, function (index, data) {
                                    $('#statenew1').append('<option value="' + data['state_id'] + '">' + data['state_name'] + '</option>');
                                });
                                $('#statenew1').val(stateID);
                            }
                        });
                    }

                    // code by durgesh for get selected city 
                    function citylistbyid(stateID, cityID) {
                        $.ajax({
                            url: '<?= base_url('company/ajax_city_getbysid') ?>',
                            method: 'post',
                            data: {stateidv: stateID},
                            dataType: 'json',
                            success: function (response) {
                                // $('#city_idnew1').append('<option value=""> -- Select -- </option>');
                                $.each(response, function (index, data) {
                                    $('#city_idnew1').append('<option value="' + data['city_id'] + '">' + data['city_name'] + '</option>');
                                });
                                $('#city_idnew1').val(cityID);
                            }
                        });
                    }

                    // code by durgesh all get state onchange  of country
                    function setstate() {
                        var countryid1 = $("#countrynew1").val();
                        $('#statenew1').html('');
                        $.ajax({
                            url: '<?= base_url('company/ajax_state_getbycid') ?>',
                            method: 'post',
                            data: {country: countryid1},
                            dataType: 'json',
                            success: function (response) {
                                $('#statenew1').append('<option value=""> -- Select -- </option>');
                                $.each(response, function (index, data) {
                                    $('#statenew1').append('<option value="' + data['state_id'] + '">' + data['state_name'] + '</option>');
                                });
                            }
                        });
                    }

                    // code by durgesh all get city onchange  of state       
                    function setcity() {
                        var stateid1 = $("#statenew1").val();
                        $('#city_idnew1').html('');
                        $.ajax({
                            url: '<?= base_url('company/ajax_city_getbysid') ?>',
                            method: 'post',
                            data: {stateidv: stateid1},
                            dataType: 'json',
                            success: function (response) {
                                $('#city_idnew1').append('<option value=""> -- Select -- </option>');
                                $.each(response, function (index, data) {
                                    $('#city_idnew1').append('<option value="' + data['city_id'] + '">' + data['city_name'] + '</option>');
                                });
                            }
                        });
                    }

                </script>

                <script>
                    //add
                    function jvforcompset(compid) {
                        $("#base_company").val(compid);
                    }
                    //Edit..
                    $(document).ready(function () {
                        $("#soleornot2").click(function () {
                            if ($(this).prop('checked') == true) {
                                $("#jvsection2").hide();
                            } else {
                                $("#jvsection2").show();
                            }
                        });
                    });
                </script>


                <script>
                    // code by durgesh For Key dates details

                </script>
                <script>
                    $('#pbg_row').hide();
                    $('#abg_row').hide();
                    $(document).ready(function () {
                        $('#pbg_name').on('change', function () {
                            var pbg_val = $('#pbg_name').val();
                            if (pbg_val == 'Yes') {
                                $('#pbg_row').show();
                            } else {
                                $('#pbg_row').hide();
                            }
                        });
                        $('#abg_name').on('change', function () {
                            var abg_val = $('#abg_name').val();
                            if (abg_val == 'Yes') {
                                $('#abg_row').show();
                            } else {
                                $('#abg_row').hide();
                            }
                        });



                       





                         $('#bid_security').on('change', function() {
            var id = $(this).val();
            if (id == 1) {
                $('#security_demnd').css('display', 'block');
            } else {
                $('#security_demnd').css('display', 'none');
            }
        });
        //sTEP 2 hIDE sHOW,,.. 
        $('#rfp_cost').on('change', function() {
            var id = $(this).val();
            if (id == 1) {
                $('#security_demnd2').css('display', 'block');
            } else {
                $('#security_demnd2').css('display', 'none');
            }
        });
        //sTEP 3 hIDE sHOW..
        $('#processing_fee').on('change', function() {
            var id = $(this).val();
            if (id == 1) {
                $('#security_demnd3').css('display', 'block');
            } else {
                $('#security_demnd3').css('display', 'none');
            }
        });









                    });
                </script>

                <script type="text/javascript">
                   
         


                    /*************************/
                    $(document).ready(function () {
                        //group add limit
                        var maxGroup = 10;
                        //add more fields group
                        $(".addMore").click(function () {
                            if ($('body').find('.fieldGroup').length < maxGroup) {
                                var fieldHTML = '<div class="fieldGroup">' + $(".fieldGroupCopy").html() + '</div>';
                                $('body').find('.fieldGroup:first').after(fieldHTML);
                            } else {
                                alert('Maximum ' + maxGroup + ' Lane are allowed.');
                            }
                        });
                        //remove fields group
                        $("body").on("click", ".remove", function () {
                            $(this).parents(".fieldGroup").remove();
                        });
                    });

                    /*************************/
                    $('#sector').multiselect({
                        columns: 1,
                        placeholder: 'Select Sector',
                        search: true
                    });

                    /*************************/
                    CKEDITOR.editorConfig = function (config) {
                        config.language = 'es';
                        config.uiColor = '#F7B42C';
                        config.height = 300;
                        config.toolbarCanCollapse = true;
                    };
                    CKEDITOR.replace('narrative_description');
                    CKEDITOR.replace('major_bridges');
                    CKEDITOR.replace('urbon_flyovers');
                    // /* CKEDITOR.replace('service_description');
                    // CKEDITOR.instances.service_description.config.readOnly = true; */
                    // $('#multiple,#multiplejv,#multiplejvUPD,#multiple1,#multiple2').selectator({
                    //     showAllOptionsOnFocus: true,
                    //     searchFields: 'value text subtitle right'
                    // });

                    /*************************/
                    $(document).ready(function () {
                        $("li#cegexp").addClass('active');
                    });


                    //Files Upload Validation Code By Asheesh..
                    $(document).ready(function () {
                        $("#uploaderror2").hide();
                        $('INPUT[type="file"]').change(function () {
                            var ext = this.value.match(/\.(.+)$/)[1];
                            switch (ext) {
                                case 'doc':
                                case 'DOC':
                                case 'docx':
                                case 'jpg':
                                case 'JPG':
                                case 'jpeg':
                                case 'pdf':
                                case 'PDF':
                                    $('#uploadButton').attr('disabled', false);
                                    $("#uploaderror2").hide();
                                    break;
                                default:
                                    $("#uploaderror2").show();
                                    $("#uploaderror2").html('This is not an allowed file type.');
                                    this.value = '';
                            }
                        });
                        $('INPUT[type="file"]').bind('change', function () {
                            var imgfilesize = (this.files[0].size / 1001376);
                            if (imgfilesize > 100) {
                                $("#uploaderror2").show();
                                $("#uploaderror2").html('Reducing File Size Max Uploads (99 MB). ');
                                this.value = '';
                            } else {
                                $("#uploaderror2").hide();
                            }
                        });
                    });

                    /*************************/
                    function setstatebycid() {
                        var countryid = $("#country").val();
                        $('#state').html('');
                        $('#city_idnew').html('');
                        $.ajax({
                            url: '<?= base_url('company/ajax_state_getbycid') ?>',
                            method: 'post',
                            data: {country: countryid},
                            dataType: 'json',
                            success: function (response) {
                                $.each(response, function (index, data) {
                                    $('#state').append('<option value="' + data['state_id'] + '">' + data['state_name'] + '</option>');
                                });
                            }
                        });
                    }

                    /*************************/
                    function setcitybystateid() {
                        var stateid = $("#statenew").val();
                        $('#city_idnew').html('');
                        $.ajax({
                            url: '<?= base_url('company/ajax_city_getbysid') ?>',
                            method: 'post',
                            data: {stateidv: stateid},
                            dataType: 'json',
                            success: function (response) {
                                $('#city_idnew').append('<option value=""> -- Select -- </option>');
                                $.each(response, function (index, data) {
                                    $('#city_idnew').append('<option value="' + data['city_id'] + '">' + data['city_name'] + '</option>');
                                });
                            }
                        });
                    }

                    /*************************/
                    function setstatebycidnew() {
                        var countryid = $("#countrynew").val();
                        $('#statenew').html('');
                        $.ajax({
                            url: '<?= base_url('company/ajax_state_getbycid') ?>',
                            method: 'post',
                            data: {country: countryid},
                            dataType: 'json',
                            success: function (response) {
                                $('#statenew').append('<option value=""> -- Select -- </option>');
                                $.each(response, function (index, data) {
                                    $('#statenew').append('<option value="' + data['state_id'] + '">' + data['state_name'] + '</option>');
                                });
                            }
                        });
                    }

                    /*************************/
                   
                    /*************************/
                    $(document).ready(function () {
                        $('#service2_name_id').on('change', function () {
                            var service2_name = $(this).val();
                            var sector_name = $('#sector_name_id').val();
                            var service1_name = $('#service1_name_id').val();
                            CKEDITOR.instances.service_description.setData('');
                            if (sector_name != '' && service2_name != '' && service1_name != '') {
                                var url = '<?= base_url('company/getPDSDesc_ajax') ?>';
                                $.ajax({
                                    type: 'POST',
                                    url: url,
                                    data: {'service2_name': service2_name, 'sector_name': sector_name, 'service1_name': service1_name},
                                    success: function (res) {
                                        var data = JSON.parse(res);
                                        //$('#service_description').val(data.desc);
                                        CKEDITOR.instances['service_description'].setData(data.desc);
                                    },
                                });
                            } else {
                                alert('Please Select All Type');
                            }

                        });

                        /*************************/
                        $('#sector_name_id').on('change', function () {
                            var sectID = $(this).val();
                            $('#service1_name_id').empty();
                            if (sectID != '') {
                                $.ajax({
                                    type: 'POST',
                                    url: '<?= base_url('company/getservice1_ajax') ?>',
                                    data: {'secid': sectID},
                                    success: function (res) {
                                        var data = JSON.parse(res);
                                        $("#service1_name_id").append("<option>--Select--</option>");
                                        for (var i = 0; i < data.length; i++) {
                                            $("#service1_name_id").append("<option value=" + data[i].id + ">" + data[i].service_name + "</option>");
                                        }
                                    },
                                });
                            } else {
                                alert('Please Select Sector');
                            }
                        });

                        /*************************/
                        $('#service1_name_id').on('change', function () {
                            var sectID = $('#sector_name_id').val();
                            var service1_name_id = $(this).val();
                            $('#service2_name_id').empty();
                            if (service1_name_id != '' && sectID != '') {
                                $.ajax({
                                    type: 'POST',
                                    url: '<?= base_url('company/getservice2_ajax') ?>',
                                    data: {'secid': sectID, 'service1_name_id': service1_name_id},
                                    success: function (res) {
                                        var data = JSON.parse(res);
                                        $("#service2_name_id").append("<option>--Select--</option>");
                                        for (var i = 0; i < data.length; i++) {
                                            $("#service2_name_id").append("<option value=" + data[i].id + ">" + data[i].service_name + "</option>");
                                        }
                                    },
                                });
                            } else {
                                alert('Please Select');
                            }
                        });
                    });
                </script>


                <style>
                    table{
                        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
                        font-size: 14px;
                    }
                    .ms-options >ul >li{list-style-type:none;}
                </style>
                
                 

                 

        

               
                <script>
                    
                

                $(document).ready(function() {
                
                 
                $('#example1').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                'copy',

                {
                extend: 'print',
                messageTop: "      Project Name : " + $("#putText1").val(),
                messageBottom: "       Project Code : " + $("#putText2").val() + "       Client Name : " + $("#putText3").val() + "      Proposal Manager : " + $("#putText4").val(),
                exportOptions: {
                stripHtml: true,
                stripNewlines: false,
                columns: [1,2,3,4,5,6,7]
                },
                },
                {
                extend: 'excelHtml5',
                messageTop: "      Project Name : " + $("#putText1").val(),
                messageBottom: "       Project Code : " + $("#putText2").val() + "      Client Name : " + $("#putText3").val() + "      Proposal Manager : " + $("#putText4").val(),
                exportOptions: {
                stripHtml: true,
                stripNewlines: false,
                columns: [1,2,3,4,5,6,7]
                },
                 
                }
                ]
                } );
                } );



                    $('.lane_id').click(function () {
                        if (confirm("Are you sure you want to remove")) {
                            var id = $(this).attr('data-laneid');
                            var url = '<?= base_url('company/ceg_laneRemove'); ?>';
                            $.ajax({
                                type: 'POST',
                                url: url,
                                data: {id: id},
                                success: function (res) {
                                    location.reload(1);
                                },
                            });
                        } else {
                            return false;
                        }
                    });

                    //Team Replacement Set..
                    function setreplacement(replacement_id) {
                        var url = '<?= base_url('dashboard/replacement_data_ajax'); ?>';
                        if (replacement_id) {
                            $.ajax({
                                type: 'POST',
                                url: url,
                                data: {Ereplacement_id: replacement_id},
                                success: function (res) {
                                    $("#dtrespons").html(res);
                                },
                            });
                        }
                    }



                    function editableFunc(mVal1) {

                      

                    var optionText = $("#list1 option[value='" + mVal1 + "']").text();
                    $("#mVal1").val(mVal1);
                    $("#mText1").val(optionText);
                    console.log("Getting Val " + mVal1);
                    $("#mTextHid1").val(optionText);

                    }

 $("#soleornot").click(function() {
                if ($(this).prop('checked') == true) {
                    $("#jvsection").hide();
                } else {
                    $("#jvsection").show();
                }
            });


            $("#soleornotAccordian").click(function() {
                if ($(this).prop('checked') == true) {
                    $("#jvsectionAccordian").hide();
                } else {
                    $("#jvsectionAccordian").show();
                }
            });

                    function editableFuncSecond(mVal1) {
                     
                    var optionText = $("#list2 option[value='" + mVal1 + "']").text();

                    $("#mVal2").val(mVal1);
                    $("#mText2").val(optionText);
                    console.log("Getting Val " + mVal1);
                    $("#mTextHid2").val(optionText);


                    }

                    function editableFuncClientContact(mVal1) {

                    console.log(">>>>" + mVal1);

                    var optionText = $("#list3 option[value='" + mVal1 + "']").text();
                    console.log(">>>>" + optionText);

                    $("#mVal3").val(mVal1);
                    $("#mText3").val(optionText);


                    }

                    function showBankGuaranteeDetail(val, divId) {

                    if (val == 5) {
                    $("#" + divId).show();
                    } else {
                    $("#" + divId).hide();
                    }


                    }



                    function saveProjectBasics() {
                    var data = $("#projectBasicsForm").serialize();
                    var clientText = $("#mTextHid1").val();
                    var fundingText = $("#mTextHid2").val();
                    var client_text = $("#mText1").val();
                    var clientID = $("#mVal1").val();

                    $.ajax({
                    type: 'POST',
                    url: "<?= base_url('company/updatecegexp'); ?>",
                    data: data + "&clientText=" + clientText + "&fundingText=" + fundingText,
                    dataType: "json",
                    success: function(responData) {

                    toastr.success(responData.msg, 'Message', {
                    timeOut: 5000
                    });



                     editableFuncClientContact(responData.id);

                    },
                    });
                    }







                  


                    function delcomptcom(delid) {
                        if (confirm("Are You Sure Delete This ?") == true) {
                            $.ajax({
                            type: 'POST',
                            url: "<?= base_url('company/removeDelcFunc'); ?>",
                            data: "delc=" + delid,
                            dataType: "json",
                            success: function(responData) {
                                // $('#example1').DataTable().ajax.reload(null, false);
                                toastr.success(responData.msg, 'Message', {
                                timeOut: 5000
                                });
                            },
                            });
                        }
                    }



                    

                     function mailSender(mailsendercom) {
                        var project_id = "<?= $bddetail->fld_id; ?>";
                            $.ajax({
                            type: 'POST',
                            url: "<?= base_url('company/cegexpeditMail'); ?>",
                            data: "mailsendercom=" + mailsendercom+"&project_id="+project_id,
                            dataType: "json",
                            success: function(responData) {
                                
                                toastr.success(responData.msg, 'Message', {
                                timeOut: 5000
                                });
                            },
                            });
                        }


                        function mailSenderCom(mailsender) {
                         var project_id = "<?= $bddetail->fld_id; ?>";
                            $.ajax({
                            type: 'POST',
                            url: "<?= base_url('company/cegexpeditMail'); ?>",
                            data: "mailsender=" + mailsender+"&project_id="+project_id,
                            dataType: "json",
                            success: function(responData) {
                                
                                toastr.success(responData.msg, 'Message', {
                                timeOut: 5000
                                });
                            },
                            });
                        }

                        function reloadscore_tech() {
                         var project_id = "<?= $bddetail->fld_id; ?>";
                            $.ajax({
                            type: 'POST',
                            url: "<?= base_url('company/reloadscore_tech'); ?>",
                            data: "project_id="+project_id,
                            dataType: "json",
                            success: function(responData) {
                                
                                toastr.success(responData.msg, 'Message', {
                                timeOut: 5000
                                });
                            },
                            });
                        }


                        function reloadscore_finan() {
                                                 var project_id = "<?= $bddetail->fld_id; ?>";
                                                    $.ajax({
                                                    type: 'POST',
                                                    url: "<?= base_url('company/reloadscore_finan'); ?>",
                                                    data: "project_id="+project_id,
                                                    dataType: "json",
                                                    success: function(responData) {
                                                        
                                                        toastr.success(responData.msg, 'Message', {
                                                        timeOut: 5000
                                                        });
                                                    },
                                                    });
                                                }



                         
                     

                   
                    function saveBidCosting() {
                    var data = $("#bidCostingForm").serialize();
                    $.ajax({
                    type: 'POST',
                    url: "<?= base_url("company/updateperformancebddetail"); ?>",
                    data: data,
                    dataType: "json",
                    success: function(responData) {
                    $('#alert').css('display', 'block');
                    toastr.success(responData.msg, 'Message', {
                    timeOut: 5000
                    });
                     
                    },
                    });
                    }


                     
                    
                    

                    function saveProjectPersonelForm() {
                    var data = $("#projectPersonelForm").serialize();
                    $.ajax({
                    type: 'POST',
                    url: "<?= base_url('dashboard/editorupdatetlofcmngr'); ?>",
                    data: data,
                    dataType: "json",
                    success: function(responData) {
                    $('#alert').css('display', 'block');
                    toastr.success(responData.msg, 'Message', {
                    timeOut: 5000
                    });
                     
                    },
                    });
                    }


 
                         
                        
                        

                    function generateRank() {
                    var data = $("#generateRankForm").serialize();
                    $.ajax({
                    type: 'POST',
                    url: "<?= base_url('company/updateweightage'); ?>",
                    data: data,
                    dataType: "json",
                    success: function(responData) {
                    $('#alert').css('display', 'block');
                    toastr.success(responData.msg, 'Message', {
                    timeOut: 5000
                    });

                    
                    $('#myModalscore').modal('toggle');
                     
                    },
                    });
                    }




                        


                     function changeProjectStatus() {
                    var data = $("#projectStatusForm").serialize();
                    $.ajax({
                    type: 'POST',
                    url: "<?= base_url('company/changeprojstatus'); ?>",
                    data: data,
                    dataType: "json",
                    success: function(responData) {
                    $('#alert').css('display', 'block');
                    toastr.success(responData.msg, 'Message', {
                    timeOut: 5000
                    });

                    $('#myModalprojstatus').modal('toggle');
                     
                    },
                    });
                    }


                     
                    
                    
                    function updateWeightage() {
                    var data = $("#updateWeightageForm").serialize();
                    $.ajax({
                    type: 'POST',
                    url: "<?= base_url('company/updatefixweightage'); ?>",
                    data: data,
                    dataType: "json",
                    success: function(responData) {
                    $('#alert').css('display', 'block');
                    toastr.success(responData.msg, 'Message', {
                    timeOut: 5000
                    });

                    $('#Modalweightage').modal('toggle');
                     
                    },
                    });
                    } 


 function saveJvConsortium() {
                    var data = $("#jvConsortium").serialize();
                    $.ajax({
                    type: 'POST',
                    url: "<?= base_url('company/addjointventure'); ?>",
                    data: data,
                    dataType: "json",
                    success: function(responData) {
                    $('#alert').css('display', 'block');
                    toastr.success(responData.msg, 'Message', {
                    timeOut: 5000
                    });

                    $('#myModaljv').modal('toggle');
                     
                    },
                    });
                    } 


                        
        
        
        
                        

                    function addCompetitor() {
                    var data = $("#addCompetitorForm").serialize();
                    $.ajax({
                    type: 'POST',
                    url: "<?= base_url('company/addcomptcompn'); ?>",
                    data: data,
                    dataType: "json",
                    success: function(responData) {
                    $('#alert').css('display', 'block');
                    toastr.success(responData.msg, 'Message', {
                    timeOut: 5000
                    });

                    $('#myModalcompt').modal('toggle');
                     
                    },
                    });
                    }



                   
                                



            function saveClientInfo() {

            var data = $("#clientInfoForm").serialize();
            countDiv = $('#contactSection').children('div.highlight').length;

            if (countDiv > 0) {
                var splitStr = $('#contactSection').children().last().attr('id');
                var resContact = splitStr.split("removeContactSection_");
                console.log(resContact[1]);
            }

            var counterTotal = (countDiv > 0) ? resContact[1] : $("#sectionCounter").val();

            $.ajax({
                type: 'POST',
                url: "<?= base_url('togoproject/clientinfo'); ?>",
                data: data + "&counterTotal=" + counterTotal,
                dataType: "json",
                success: function(responData) {
                    $('#alert').css('display', 'block');
                    toastr.success(responData.msg, 'Message', {
                        timeOut: 5000
                    });
                     
                },
            });
        }

  function addClientContact() {

            

countDiv = $('#contactSection').children('div.highlight').length;

if (countDiv > 0) {
    var splitStr = $('#contactSection').children().last().attr('id');
    var resContact = splitStr.split("removeContactSection_");
    console.log(resContact[1]);
}

i = (countDiv > 0) ? resContact[1] : $("#sectionCounter").val();
i++;
$("#sectionCounter").val(i);
$("#contactSection").append('<div class="row highlight mt-2" style="margin: 0px" id="removeContactSection_' + i + '"><div class="form-group required col-md-12"> <a onclick="removeClientContact(' + i + ')" class="btn btn-outline-danger pull-right mt-4">-</a></div><div class="form-group required col-md-3"><label> Name</label><input type="text" class="form-control" id="clientName' + i + '" name="clientName' + i + '" value=""></div><div class="form-group required col-md-3"><label> Designation</label><input type="text" class="form-control" id="clientDesignation' + i + '" name="clientDesignation' + i + '" value=""></div><div class="form-group required col-md-3"><label> Email</label><input type="text" class="form-control" id="clientEmail' + i + '" name="clientEmail' + i + '" value=""></div><div class="form-group required col-md-3"><label> Phone No.</label><input type="text" class="form-control" id="clientPhone' + i + '" name="clientPhone' + i + '" value=""></div><div class="form-group required col-md-12"><label> Address</label><textarea required cols="10" class="form-control" id="clientAddress' + i + '" name="clientAddress' + i + '" rows="3"></textarea></div></div>');
}




function removeClientContact(id) {

console.log(id);

$("#removeContactSection_" + id).remove();
$("#sectionCounter").val(id--);
}


function savedClientContact(projid) {

// console.log(">>>"+projid);

$.ajax({
    url: '<?= base_url('togoproject/savedClientContact') ?>',
    method: 'post',
    data: {
        project_id: projid
    },
    dataType: 'json',
    success: function(response) {
        $("#contactSection").html("");
        console.log(">>>>>>>>>>"+response);
        var i = 1;
        $.each(response, function(index, data) {

            $("#sectionCounter").val(i);
            $("#contactSection").append('<div class="row highlight mt-2" style="margin: 0px" id="removeContactSection_' + i + '"><div class="form-group required col-md-12"> <a onclick="removeClientContact(' + i + ')" class="btn btn-outline-danger pull-right mt-4">-</a></div><div class="form-group required col-md-3"><label> Name</label><input type="text" class="form-control" id="clientName' + i + '" name="clientName' + i + '" value="' + data.cdetail_name + '"></div><div class="form-group required col-md-3"><label> Designation</label><input type="text" class="form-control" id="clientDesignation' + i + '" name="clientDesignation' + i + '" value="' + data.cdetail_designation + '"></div><div class="form-group required col-md-3"><label> Email</label><input type="text" class="form-control" id="clientEmail' + i + '" name="clientEmail' + i + '" value="' + data.cdetail_email + '"></div><div class="form-group required col-md-3"><label> Phone No.</label><input type="text" class="form-control" id="clientPhone' + i + '" name="clientPhone' + i + '" value="' + data.cdetail_phone + '"></div><div class="form-group required col-md-12"><label> Address</label><textarea required cols="10" class="form-control" id="clientAddress' + i + '" name="clientAddress' + i + '" rows="3">' + data.cdetail_address + '</textarea></div></div>');
            i++;
        });

        $("#sectionCounter").val(i);


    }
});

}



                </script>
                </body>
                </html>