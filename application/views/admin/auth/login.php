<?php $this->load->view('admin/includes/head'); ?>
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle auth-main">
			<?php if ($this->session->flashdata('error_msg')): ?>
					<div class="alert alert-danger alert-dismissable">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
						<strong>Error ! </strong> <?= $this->session->flashdata('error_msg'); ?>
					</div>
				<?php endif; ?>
				<div class="auth-box">
                    <div class="top">
                       <!-- <img src="http://www.wrraptheme.com/templates/lucid/html/assets/images/logo-white.svg" alt="Lucid">-->
                        <h3 style="color:#fff;">HRMS - BD</h3>
                    </div>
					<div class="card">
                        <div class="header">
                            <p class="lead">Login to your account</p>
                        </div>
                        <div class="body">
                            <form class="form-auth-small" action="<?= base_url('admin/loginchecker'); ?>" method="POST" id="myform1">
                                <div class="form-group">
                                    <label for="signin-email" class="control-label sr-only">Email</label>
                                    <input type="text" class="form-control" name="username" id="signin-email" value="" placeholder="Enter Email">
                                </div>
                                <div class="form-group">
                                    <label for="signin-password" class="control-label sr-only">Password</label>
                                    <input type="password" class="form-control" name="password" id="signin-password" value="" placeholder="Enter Password">
                                </div>
                                <!--<div class="form-group clearfix">
                                    <label class="fancy-checkbox element-left">
                                        <input type="checkbox">
                                        <span>Remember me</span>
                                    </label>								
                                </div>-->
                                <button type="submit" class="btn btn-primary btn-lg btn-block" name="submit_login">LOGIN</button>
                               <!-- <div class="bottom">
                                    <span class="helper-text m-b-10"><i class="fa fa-lock"></i> <a href="page-forgot-password.html">Forgot password?</a></span>
                                    <span>Don't have an account? <a href="page-register.html">Register</a></span>
                                </div>-->
                            </form>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	<?php $this->load->view('admin/includes/footer'); ?>