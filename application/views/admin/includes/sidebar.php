<?php
$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri_segments = array_filter(explode('/', $uri_path));
$menuType = 2;
$loginUserRec = GetBasicRecLoginUser();
?>

<?php if ((in_array("add_project", $uri_segments)) or ( in_array("editproject", $uri_segments))) { ?>
    <style>
        li#mymenu_2{background:#c8e9d7;}
    </style>
    <?php
    $menuType = 1;
}
?>

<?php if ((in_array("package_view", $uri_segments)) or ( in_array("managework_in_package", $uri_segments))) { ?>
    <style>
        li#mymenu_9{background:#c8e9d7;}
    </style>
    <?php
    $menuType = 1;
}
?>

<?php if (in_array("clearance_work", $uri_segments)) { ?>
    <style>
        li#mymenu_11{background:#c8e9d7;}
    </style>
    <?php
    $menuType = 1;
}
?>

<?php if (in_array("phys_progress_details", $uri_segments)) { ?>
    <style>
        li#mymenu_13{background:#c8e9d7;}
    </style>
    <?php
    $menuType = 1;
}
?>

<?php if ((in_array("habitation_on_workid", $uri_segments)) or ( in_array("entryinhabitation", $uri_segments))) { ?>
    <style>
        li#mymenu_12{background:#c8e9d7;}
    </style>
    <?php
    $menuType = 1;
}
?>

<?php if ((in_array("milestone_fin_progr", $uri_segments)) or ( in_array("entryinmilestone", $uri_segments))) { ?>
    <style>
        li#mymenu_14{background:#c8e9d7;}
    </style>
    <?php
    $menuType = 1;
}
?>

<!-- Menu 2 Lvl !-->
<?php if ((in_array("district_management", $uri_segments)) or ( in_array("editdistrict", $uri_segments))) { ?>
    <style>
        li#mymenu_3{background:#c8e9d7;}
    </style>
    <?php
    $menuType = 2;
}
?>

<div id="left-sidebar" class="pt-2 sidebar">
    <div class="sidebar-scroll">
        <div class="user-account">
            <?php if ($loginUserRec->profileimg) { ?>
                <img src="<?= EMPLPROFILE . $loginUserRec->profileimg; ?>" class="rounded-circle user-photo" alt="">
            <?php } ?>  
            <div class="dropdown">
                <span>Welcome,</span>
                <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong><?= $this->session->userdata('username'); ?></strong></a>
                <ul class="dropdown-menu dropdown-menu-right account">
                    <li><a href="<?= base_url('myprofile'); ?>"><i class="icon-user"></i>My Profile</a></li>
                    <li><a href="#"><i class="icon-envelope-open"></i>Messages</a></li>
                    <li><a href="javascript:void(0);"><i class="icon-settings"></i>Settings</a></li>
                    <li class="divider"></li>
                    <li><a href="<?= base_url('logout'); ?>"><i class="icon-power"></i>Logout</a></li>
                </ul>
            </div>
        </div>

        <?php $arrayIcon = ["plus", "docs", "puzzle", "grid", "tag", "diamond"];?>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu"> BD Section</a></li>   
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#master_menu">Report(s)</a></li> 
        </ul>
        <!-- Tab panes -->
        <div class="tab-content p-l-0 p-r-0">   
            <div class="tab-pane active" id="menu">
                <nav id="left-sidebar-nav" class="sidebar-nav">
                    <ul id="main-menu" class="metismenu">
                        <?php
                        $MasterMenuArr = getAllMasterMenu();
                        if ($MasterMenuArr) {
                            $i=0;
                            foreach($MasterMenuArr as $rcdRows) {
                               $urlParent = base_url($rcdRows->action_url);
                                ?>
                                <li class="" id="tr_<?= $rcdRows->fld_id; ?>">
                                    <a style="cursor: pointer" href="<?php echo ($rcdRows->action_url == "#") ? "javascript:void(0)" : $urlParent; ?>"  class="<?= ($rcdRows->is_in_childmenu == "1") ? "has-arrow" : ""; ?> activeMenu">
                                    <i class="icon-<?php echo $arrayIcon[$i];?> "></i><span><?= $rcdRows->menu_name; ?></span></a>
                                    <?php
                                    $childMenuArr = getChildMenuByParentID($rcdRows->fld_id);
                                    if ($childMenuArr) {
                                        foreach ($childMenuArr as $childmenuRows) { 
                                            
                                            if($childmenuRows->fld_id != 58){
                                            
                                            ?>
                                            <ul>
                                                <li id="mymenu_<?= $childmenuRows->fld_id; ?>" class="">
                                                    <a <?php if($childmenuRows->is_in_childmenu == "0"):?> href="<?= base_url($childmenuRows->action_url); ?>" <?php endif;?> class="<?= ($childmenuRows->is_in_childmenu == "1") ? "has-arrow" : ""; ?> activeMenu"><?= $childmenuRows->menu_name; ?></a>
													<?php
													$childMenuArr1 = getChildMenuByParentID($childmenuRows->fld_id);
													if ($childMenuArr1) {
														foreach ($childMenuArr1 as $childmenuRows1) { ?>
															<ul>
																<li id="1mymenu_<?= $childmenuRows1->fld_id; ?>" class="">
																	<a href="<?= base_url($childmenuRows1->action_url); ?>" class="<?= ($childmenuRows1->is_in_childmenu == "1") ? "has-arrow" : ""; ?> "><?= $childmenuRows1->menu_name; ?></a>
																</li>
															</ul>
															<?php
														}
													}
													?>
                                                </li>
                                            </ul>
                                            <?php
                                            }
                                        }
                                    }
                                    ?>
                                </li>
                            <?php 
                        $i++;    
                        
                        }
                        }
                        ?>
                    </ul>
                </nav>
            </div>

            <div class="tab-pane" id="master_menu"> 
                <nav id="left-sidebar-nav" class="sidebar-nav">
                    <ul id="main-menu" class="metismenu">
                        <?php
                        $MasterMenuArr = getAllMasterMenuReports();
                        if ($MasterMenuArr) {
                            $i=0;
                            foreach($MasterMenuArr as $rcdRows) {
                               $urlParent = base_url($rcdRows->action_url);
                                ?>
                                <li class="active">
                                    <a style="cursor: pointer" href="<?php echo ($rcdRows->action_url == "#") ? "javascript:void(0)" : $urlParent; ?>"  class="<?= ($rcdRows->is_in_childmenu == "1") ? "has-arrow" : ""; ?>">
                                    <i class="icon-<?php echo $arrayIcon[3];?>"></i><span><?= $rcdRows->menu_name; ?></span></a>
                                    <?php
                                    $childMenuArr = getChildMenuByParentID($rcdRows->fld_id);
                                    if ($childMenuArr) {
                                        foreach ($childMenuArr as $childmenuRows) { 
                                            
                                            if($childmenuRows->fld_id != 58){
                                            
                                            ?>
                                            <ul>
                                                <li id="mymenu_<?= $childmenuRows->fld_id; ?>" class="">
                                                    <a <?php if($childmenuRows->is_in_childmenu == "0"):?> href="<?= base_url($childmenuRows->action_url); ?>" <?php endif;?> class="<?= ($childmenuRows->is_in_childmenu == "1") ? "has-arrow" : ""; ?> activeMasterMenu"><?= $childmenuRows->menu_name; ?></a>
                                                    <?php
                                                    $childMenuArr1 = getChildMenuByParentID($childmenuRows->fld_id);
                                                    if ($childMenuArr1) {
                                                        foreach ($childMenuArr1 as $childmenuRows1) { ?>
                                                            <ul>
                                                                <li id="1mymenu_<?= $childmenuRows1->fld_id; ?>" class="">
                                                                    <a href="<?= base_url($childmenuRows1->action_url); ?>" class="<?= ($childmenuRows1->is_in_childmenu == "1") ? "has-arrow" : ""; ?>" ><?= $childmenuRows1->menu_name; ?></a>
                                                                </li>
                                                            </ul>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </li>
                                            </ul>
                                            <?php
                                            }
                                        }
                                    }
                                    ?>
                                </li>
                            <?php 
                        $i++;    
                        
                        }
                        }
                        ?>
                    </ul>
                </nav>
            </div>
        </div>          
    </div>
</div>