<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">
    <head>
        <title><?= PROJECTNAME; ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta name="description" content="Lucid Bootstrap 4.1.1 Admin Template">
        <meta name="author" content="CEG india HRMS">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>jquery-datatable/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>sweetalert/sweetalert.css"/>
        <link rel="stylesheet" href="<?= FRONTASSETS; ?>css/main.css">
        <link rel="stylesheet" href="<?= FRONTASSETS; ?>css/color_skins.css">
        <link rel="stylesheet" type="text/css" href="<?= HOSTNAME; ?>assets/customselect/jquery-customselect.css" rel="stylesheet">
    </head>
