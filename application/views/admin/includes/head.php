<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">
    <head>
        <title><?= PROJECTNAME; ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta name="description" content="Lucid Bootstrap 4.1.1 Admin Template">
        <meta name="author" content="CEG india HRMS">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        
        
<!-- Start New Section For All Form  -->
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>bootstrap-multiselect/bootstrap-multiselect.css">
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>multi-select/css/multi-select.css">
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>bootstrap-tagsinput/bootstrap-tagsinput.css">
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>nouislider/nouislider.min.css" />
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>select2/select2.css" />
        <link rel="stylesheet" href="<?= FRONTASSETS; ?>css/main.css">
        <link rel="stylesheet" href="<?= FRONTASSETS; ?>css/color_skins.css">
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>font-awesome/css/font-awesome.min.css">
        <!-- Close Section For All Form . -->

        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>jquery-datatable/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>sweetalert/sweetalert.css"/>
        <link rel="stylesheet" href="<?= FRONTASSETS; ?>css/main.css">
        <link rel="stylesheet" href="<?= FRONTASSETS; ?>css/custom.css">
        <link rel="stylesheet" href="<?= FRONTASSETS; ?>css/color_skins.css">

        <link rel="stylesheet" href="<?= ASSETSVENDOR; ?>toastr/toastr.min.css">

        
     <link href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.18.10/slimselect.min.css" rel="stylesheet">
     <link href="//cdn.datatables.net/colvis/1.1.2/css/dataTables.colVis.min.css" rel="stylesheet">
     <link href="//cdn.datatables.net/colvis/1.1.2/css/dataTables.colvis.jqueryui.css" rel="stylesheet">

     
	
	
	

        <style>


                .select-editable select {
                
                width: 99% !important;
                 
                cursor: pointer !important;
                }


            .highlight{
             
            background-color: #28a7451a;
            padding: 10px;
            border-radius: 2px;
            }


            .highlightDashed{

            background-color: #28a7451a;
            padding: 10px;
            border-bottom: 1px dashed;
            }


            

            .highlightMenu {
    background-color: #ffc10759;
    border-left: 5px solid #ffc107;
}

        .modal-dialog.modal-lg {
        max-width: 80%!important;
        
        }

        fieldset{
            background: white !important;
        }
 
        button.ColVis_Button.ColVis_MasterButton {
    float: right !important;
    margin-top: 30px;
    margin-left: 10px;
    padding: 17px;
    line-height: 0px;
}

button.ColVis_Button.ColVis_MasterButton.export_team.btn.btn-info.btn-sm.mt-0 {
    line-height: 0px;
    padding: 20px;
}
        </style>

        
        
    </head>
