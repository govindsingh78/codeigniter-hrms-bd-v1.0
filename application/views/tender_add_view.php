<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
		
        ?>
		<script src="<?= FRONTASSETS; ?>ckeditor/ckeditor.js"></script>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div> 
                    </div>
                </div>
				<?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Success!</strong>  Tender Added Success !
                        </div>
                    <?php } ?>
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="accordion" id="accordion">
                                    <div>
									<form action="<?= base_url('tender/tender_add'); ?>" method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data mvc">
                                        <!--<div class="card-header" id="headingOne">-->
                                            <div class="row clearfix">
                                                    <div class="col-lg-3 col-md-6">
                                                        <label>Tender Type :</label>
                                                        <div class="input-group mb-3">
                                                            
                                                          <select name="tndrType" class="select floating form-control" data-placeholder="Tender/Project  Type" id="selectError2" data-rel="chosen">
															<option value="New_project">New Tender </option>
															<option value="Active_project">Active Project</option>
															<option value="Important_project">Important Project</option>
															<option value="In_Review_project">In Review Project</option>
															<!--<option value="To_Go_project">To Go Project</option>
															<option value="Bid_project">Bid Project</option>  -->
														</select>
                                                        </div>
                                                    </div>
													<div class="col-lg-3 col-md-6">
                                                        <label>Tender Sector :</label>
                                                        <div class="input-group mb-3"><br>
                                                            
                                                       <select id="multiselect-size" name="tendersector[]" class="multiselect-container dropdown-menu select floating" multiple="multiple">
															<?php
															if ($activesectArr) {
																foreach ($activesectArr as $rowsect):
																	?>
																	<option value="<?php echo $rowsect->fld_id; ?>"><?php echo $rowsect->sectName; ?></option>
																	<?php
																endforeach;
															}
															?>
														</select>
                                                        </div>
                                                    </div>
													<!--<div class="col-lg-3 col-md-6">
                                                        <label>Tender Sector :</label>
                                                        <div class="input-group mb-3">
                                                            
															<select name="tendersector[]" id="selectError1" multiple="multiple" class="select floating form-control" data-rel="chosen">
															<?php
															if ($activesectArr) {
																foreach ($activesectArr as $rowsect):
																	?>
																	<option value="<?php echo $rowsect->fld_id; ?>"><?php echo $rowsect->sectName; ?></option>
																	<?php
																endforeach;
															}
															?>
														</select>
                                                        </div>
                                                    </div>-->
													<div class="col-lg-3 col-md-6">
                                                        <label>Tender Name :</label>
                                                        <div class="input-group mb-3">
                                                          <input type="text" id="tendername" name="tendername" required="required" class="form-control required">
                                                        </div>
                                                    </div>
													
                                                    <div class="col-lg-3 col-md-6">
                                                        <label>Deadline Date :</label>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                                                            </div>
                                                            <input type="date" id="single_cal1" name="deadline" required="required" class="form-control required">
                                                        </div>
                                                    </div>
													
                                                    <div class="col-lg-3 col-md-6">
                                                        <label>Link URL :</label>
                                                        <div class="input-group mb-3">
                                                            <input type="text" id="linkurl" name="linkurl" required="required" class="form-control required url">
                                                        </div>
                                                    </div>
													
                                                    <div class="col-lg-3 col-md-6">
                                                        <label>Tender Value :</label>
                                                        <div class="input-group mb-3">
														<input type="text" id="tendervalue" name="tendervalue" class="form-control">
                                                        </div>
                                                    </div>
                                                    
													<div class="col-lg-3 col-md-6">
                                                        <label>Location :</label>
                                                        <div class="input-group mb-3">
														<input type="text" id="location" name="location" class="form-control">
                                                        </div>
                                                    </div>
													
													<div class="col-lg-3 col-md-6">
                                                        <label>Located :</label>
                                                        <div class="input-group mb-3">
														<select id="national_intern" name="national_intern" class="form-control">
															<option value="1">National</option>
															<option value="2">International</option>
															<option value="3">Other</option> 
														</select>
                                                        </div>
                                                    </div>
													
													<div class="col-lg-12 col-md-6">
                                                        <label>Organisation :</label>
                                                        <div class="input-group mb-3">
														<input type="text" id="organisation" name="organisation" class="form-control">
                                                        </div>
                                                    </div>
													
													
                                                    
													
													<div class="col-lg-12 col-md-6">
                                                        
														<div class="mb-2">
                                                            <label>Tender Details</label>
															 <textarea cols="80" id="edi" name="editor1" rows="10"></textarea>
															<script>CKEDITOR.replace('edi');</script>
                                                        </div>
                                                    </div>
													
													
													
													<div class="col-lg-12 col-md-6">
                                                        
														<div class="mb-2">
                                                             
															<button type="submit" class="btn btn-info pull-right">Submit</button>
                                                        </div>
                                                    </div>
													
													
                                                </div>
                                        <!--</div> -->                               
                                        
										</form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>
