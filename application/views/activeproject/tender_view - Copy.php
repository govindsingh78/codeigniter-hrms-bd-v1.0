<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <?php $this->load->view('include/tabbar'); ?>
        <div class="wrapper">
            <?php $this->load->view('include/sidebar'); ?>
            <div id="content" class="col-lg-10 col-sm-10">
                <div><ul class="breadcrumb">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li><a href="#"><?= $title; ?></a></li>
                    </ul>
                </div>
                <div class="row">
                    <?php if ($this->session->flashdata('msg')) { ?>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong><div id="msgs"> Success ! </div></strong> <?= $this->session->flashdata('msg'); ?>
                        </div>
                    <?php } ?>
                    <div class="box col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form id="form-filter" class="form-horizontal">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label class="col-sm-3 control-label" for="selectError2"> Sector : </label>
                                            <div class="col-sm-8">
                                                <select name="sectorinput" id="sectorinput"  class="form-control">
                                                    <option <?= ($secId == "") ? 'Selected' : ''; ?> value="">--All--</option>
                                                    <?php if ($sectorArr):
                                                        foreach ($sectorArr as $row) {
                                                            ?>
                                                            <option <?= ($secId == $row->fld_id) ? 'Selected' : ''; ?> value="<?= $row->fld_id; ?>"><?= $row->sectName; ?></option>
    <?php } endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3" >
                                            <label for="Business Unit" class="col-sm-2 control-label"> Filter : </label>
                                            <div class="col-sm-10">
                                                <select id="company_name" class="form-control"><option value="" selected="selected">--All--</option>
                                                    <option value="Design">Design</option>
                                                    <option value="Construction">Construction</option>
                                                    <option value="Electrical">Electrical</option>
                                                    <option value="Maintenance">Maintenance</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="col-sm-10" title="Tender Located">
                                                <select id="national_intern" name="national_intern" class="form-control">
                                                    <option value="1">National</option>
                                                    <option value="2">International</option>
                                                    <option value="3">Other</option> 
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2" >
                                            <div class="col-sm-15" title="Tender Located"><input type="date" id="date_chk" name="date_chk" class="form-control" /> </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4" style="margin: 1%;">
                                        <div class="col-sm-12"><button type="button" id="btn-filter" class="btn btn-primary">Filter</button><button type="button" id="btn-reset" class="btn btn-default">Reset</button></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="box-inner">
                        <div class="box-content">
                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="colvis"></div>
                                        </div>
                                    </div>
                                    <form action="<?= base_url('activeproject/importantprojectbycheckbox'); ?>" method="post">
                                        <table id="table" class="display" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Sr. No</th>
                                                    <th style="width:10%">Exp-Date</th>
                                                    <th>Tender Details</th>
                                                    <th>Location</th>
                                                    <th>Client</th>
                                                    <th style="width:15%">Actions </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Sr. No</th>
                                                    <th>Exp-Date</th>
                                                    <th>Tender Details</th>
                                                    <th>Location</th>
                                                    <th>Client</th>
                                                    <th>Actions </th>
                                                </tr>
                                            </tfoot>
                                            <tfoot>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <select name="act_type">
                                                            <option value="Important"> Important  </option>
                                                            <option value="Notimportant"> Not important </option>
                                                        </select>
                                                    </td>
                                                    <td><button type="submit" class="btn btn-success">Submit</button></td>
                                                    <td>All <input type="checkbox" id="checkAll"> </td>
                                                </tr>
                                            </tfoot>
                                            <hr>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>	
        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>
        <hr>
        <?php $this->load->view('include/commenthtml'); ?>
        <?php $this->load->view('include/footer'); ?>
<?php $this->load->view('include/datatablejs'); ?>
        <style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            #chatbox {padding: 15px;overflow: scroll;height: 300px;}
        </style>
        <script type="text/javascript">
            $(document).ready(function () {
                $("li#activeproject").addClass('active');
                $("li#tender_search").addClass('active');
            });
            $(document).ready(function () {
                $("#checkAll").click(function () {
                    $('input:checkbox').not(this).prop('checked', this.checked);
                });
            });
            function gocomment(tndrid) {
                $("#projId").val(tndrid);
                $.ajax({
                    type: 'POST',
                    dataType: "text",
                    data: {'tndrid': tndrid},
                    url: "<?= base_url('activeproject/commentset'); ?>",
                    success: function (responData) {
                        $("#chatbox").html(responData);
                    },
                });
            }
            $(document).ready(function () {
                $('#commentmsg').keypress(function (e) {
                    var key = e.which;
                    if (key == 13) {
                        var comnt = $("#commentmsg").val();
                        var projId = $("#projId").val();
                        $.ajax({
                            type: 'POST',
                            dataType: "text",
                            url: "<?= base_url('activeproject/commentsubmit'); ?>",
                            data: {'projId': projId, 'comnt': comnt},
                            success: function (responData) {
                                $("#commentmsg").val('');
                                return gocomment(projId);
                            },
                        });
                    }
                });
            });
            function setnotimportant(delid, sectid) {
                if (confirm("Are You Sure Not Important This.") == true) {
                    $.ajax({
                        type: 'POST',
                        url: "<?= base_url('activeproject/projNotImportantMark'); ?>",
                        data: {'actid': delid},
                        success: function (responData) {
                            $('#alert').css('display', 'block');
                            $('#msgs').html('Tender Not Important.');
                            setTimeout(function () {
                                location.reload(1);
                            }, 1000);
                            console.log(responData);
                        }, });
                }
            }
            function setimportant(actid, sectid) {
                $.ajax({
                    type: 'POST',
                    url: "<?= base_url('activeproject/projImportantMark'); ?>",
                    data: {'actid': actid},
                    success: function (responData) {
                        console.log(responData);
                        $('#alert').css('display', 'block');
                        $('#msgs').html('Tender Important.');
                        setTimeout(function () {
                            location.reload(1);
                        }, 1000);
                    },
                });
            }
            var table;
            $(document).ready(function () {
                table = $('#table').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "order": [], //Initial no order.
                    "ajax": {
                        "url": "<?php echo site_url('activeproject/newProjectAll') ?>",
                        "type": "POST",
                        "data": function (data) {
                            data.project_name = $('#company_name').val();
                            data.sectorinput = $('#sectorinput').val();
                            data.national_intern = $('#national_intern').val();
                            data.date_chk = $('#date_chk').val();
                            console.log(data);
                        },
                    },
                    "dom": 'lBfrtip',
                    "buttons": [{extend: 'collection',
                            text: 'Export',
                            buttons: ['copy',
                                'excel',
                                'csv',
                                'pdf',
                                'print']}
                    ],
                    "columnDefs": [{"targets": [0], "orderable": false, }, ],
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]], });
                var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
                $('#colvis').html(colvis.button());
                $('#btn-filter').click(function () {
                    table.ajax.reload();
                });
                $('#btn-reset').click(function () { //button reset event click
                    $('#form-filter')[0].reset();
                    table.ajax.reload();  //just reload table
                });
            });
        </script>
    </div>
</body>
</html>