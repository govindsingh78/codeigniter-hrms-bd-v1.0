<html lang="en">
    <?php
    error_reporting(E_ALL); //error_reporting(0);
    ?>
	
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        <div class="wrapper">
            <?php $this->load->view('include/sidebar'); ?>
            <div id="content" class="col-lg-10 col-sm-10">
                    <div>
                        <ul class="breadcrumb">
                            <li> <a href="<?= base_url(); ?>">Home</a></li>
                            <li><a href="#">Project </a></li>
                        </ul>
                    </div>
                    <div class="row">
                        <?php if ($this->session->flashdata('msg')) { ?>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                            </div>
                        <?php } ?>
                        <div class="box col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <form id="form-filter" class="form-horizontal">
                                        <div class="row">
                                            
                                            <div class="box">
                                                
                                                
                                                <!-- /.box-header -->
                                                <div class="box-body">
                                             
                                                    


                                                    <div class="container">
                                                       
                                                        <table id="example" class="display">                                                         
                                                            <thead>
                                                                <tr>
                                                                    <th>Sr </th>
                                                                    <th>Project Name </th>
                                                                    <th>Project ID</th>
                                                                    <th>Status</th>
                                                                    <th style="width:80px">Total MM</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>

                                                            <tbody>
                                                                <?php
                                                                $ssnn = 0;
                                                                if ($data):
                                                                    foreach ($data as $value) {
																		/* if($value->status == 1){
																			$class = 'disabled';
																		}else{
																			$class= '';
																		} */
																		$class= '';
																		$projectDetail = getProjectDetail($value->fld_id);
																		//echo '<pre>'; print_r($projectDetail);
                                                                        ?>
																		<!--<form method="post" action="<?= base_url('projectdetail/saveproject'); ?>" > -->
																			<tr>
																				<td><?= $ssnn = $ssnn + 1; ?></td>
																				<td><?= $value->project_name;?>  </td>
																				<td>
																					<select <?= $class ?> name="cegproject_id" class="form-control cegproject_id" data-id="<?= $ssnn; ?>">
																					<option value="">Select Project</option>
																					<?php 
																						foreach($tmProject as $val){
																					?>	
																						<option <?= ($projectDetail[0]->cegproject_id == $val->id) ? 'selected' : ''; ?> value="<?=  $val->id; ?>"><?=  $val->project_name; ?></option>
																					<?php
																						}
																					?>
																					</select>
																				</td>
																				<td>
																					<select <?= $class ?> name="project_status" class="form-control project_status">
																						<option value="">Select Status</option>
																						<option <?= ($projectDetail[0]->project_status == 'in-progress') ? 'selected' : ''; ?> value="in-progress">In Progress</option>
																						<option <?= ($projectDetail[0]->project_status == 'hold') ? 'selected' : ''; ?> value="hold">Hold</option>
																						<option <?= ($projectDetail[0]->project_status == 'completed') ? 'selected' : ''; ?> value="completed">Completed</option>
																					</select>
																				</td>
																				<td style="width:80px;">
																					<input type="number" step="any" <?php // isset($projectDetail[0]->total_mm) ? 'disabled' : ''; ?> value="<?= isset($projectDetail[0]->total_mm) ? $projectDetail[0]->total_mm : ''; ?>" name="total_mm" class="total_mm form-control total_<?= $ssnn; ?>" style="width:100%"/>
																				</td>
																				<td>
																					<input type="hidden" value="<?= $value->fld_id; ?>" name="cegexp_id" class="cegexp_id" />
																					<input type="hidden" value="<?= $value->project_id; ?>" name="projectbd_id" class="projectbd_id"/>
																					<button <?= $class ?> type="button" id="btn-filter" class="btn_submit btn btn-primary">Submit</button>
																				</td>
																			</tr>
																		<!--</form>	-->
                                                                <?php } endif; ?>
                                                            </tbody>   
                                                        </table>
														
                                                    </div>
                                                </div>
                                                <!-- /.box-footer -->
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- open Phase Section-->
                        <div class="box-inner">
                            <div class="box-content">
                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="colvis"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Close phase Section-->
                </div>
            
        </div>	


        <!-- BootStrap Model For Comments.. -->
        <?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->
        <?php $this->load->view('include/footer'); ?>
       <?php $this->load->view('include/datatablejs'); ?>
        <!-- Code For Multi Select By Asheesh -->
        <link href="<?= FRONTASSETS; ?>fm.selectator.jquery.css" rel="stylesheet" type="text/css">
        <script src="<?= FRONTASSETS; ?>fm.selectator.jquery.js"></script> 
                                      
    </body>
</html>

<style>
    table{
        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
        font-size: 14px;
    }
</style>
<script>
	$('.cegproject_id').on('change',function(){
		var id = $(this).val();
		var attrID = $(this).attr('data-id');
		var url = '<?= base_url('projectdetail/getManmonth'); ?>';
		$.ajax({
			type:'post',
			url:url,
			data:{'id':id},
			success:function(res){
				var data = JSON.parse(res);
				if(data.total_mm !='0'){
					console.log(data.total_mm);
					$('.total_'+attrID).val(data.total_mm);
					$('.total_'+attrID).attr('disabled','disabled');
				}else{
					$('.total_'+attrID).val('');
					$('.total_'+attrID).removeAttr('disabled','disabled');
				}
			},
		});
	});
	
	$(document).ready(function(){
		$(".btn_submit").on("click", function () {
			var projectbd_id = $(this).closest("td").find("input[name=projectbd_id]").val();
			var cegexp_id = $(this).closest("td").find("input[name=cegexp_id]").val();
			var cegproject_id = $(this).closest("tr").find(".cegproject_id option:selected").val();
			var project_status = $(this).closest("tr").find(".project_status option:selected").val();
			var total_mm = $(this).closest("tr").find("input[name=total_mm]").val();
			if(project_status != ''){
				var url = '<?= base_url('projectdetail/saveproject'); ?>';
				$.ajax({
					type:'post',
					url:url,
					data:{'projectbd_id':projectbd_id,'cegexp_id':cegexp_id,'project_status':project_status,'cegproject_id':cegproject_id,'total_mm':total_mm},
					success:function(res){
						//console.log(res);
						location.reload(1);
					},
				});
			}else{
				alert('Please select project or status or mm');
			}
		});
	});
		 
    $(document).ready(function () {
        $('#example').dataTable({
            responsive: true,
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],     // page length options
			"dom": 'Bfrtip',
            "buttons": ['excel','csv','pdf', 'print','pageLength'],    
        });
    });
</script>