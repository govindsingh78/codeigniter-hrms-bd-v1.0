<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>

<style>
    #table_length,
    #table1_length,
    #table2_length,
    #table3_length {
        position: absolute;
        margin-left: 100px !important;
    }

    .table tbody tr td,
    .table tbody th td {
        vertical-align: middle;
        white-space: normal !important;
    }

    ul.nav.nav-tabs-new {
        float: right !important;
        margin: auto;
    }
</style>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : "Project Detail Page"; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : "Project Detail Page"; ?></li>
                            </ul>
                        </div>
                    </div>
				</div>
				



				<div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
							 
								
                                <div class="col-lg-12 col-md-12">
								<button class="btn btn-info btn-sm p-2 pull-left"  data-toggle="modal" data-target="#clientmodel" onclick="editclientmodel(<?= $data['tender'][0]->fld_id; ?>)"><i class="fa fa-plus" aria-hidden="true"></i> Add / View Reminder</button>
								 
                                    <ul class="nav nav-tabs-new">
                                        <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#Home-new">Profile</a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Profile-new">Project Info</a></li>
										<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#other-data">Competitor</a></li>
										<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#all-new">Team</a></li>
                                    </ul>
                                </div>

							 
                                <div class="tab-content">

								<div class="tab-pane  show active" id="Home-new">
									

								 
						 

							<!--<h3 class="mgbt-xs-15 mgtp-10 font-semibold"><i class="fa fa-user"></i> ABOUT</h3>-->
							<div class="row mt-5">
								 
								 
								<div class="col-md-12 alert alert-warning">
								 <?= isset($data['tender'][0]->TenderDetails) ? $data['tender'][0]->TenderDetails  : '';  ?>
								</div>
								 
								<div class="col-md-12 alert alert-info">
								<strong>Expiry Date : </strong> <?= isset($data['tender'][0]->Expiry_Date) ? date('Y-m-d',strtotime($data['tender'][0]->Expiry_Date))  : '';  ?>
								</div>

								
							
								 
								
							</div>
							<hr class="pd-10"  />
							<div class="row clearfix">
							  <div class="col-sm-7 mgbt-xs-20">
								<h5 class="mgbt-xs-15 font-semibold">  Life Cycle</h5>
								<div class="content-list content-menu">

								<?php if($moved_eoi_status){
								 
								?>	
								<div class="highlight mb-2">
								<strong>Project set as Won from EOI !</strong> <?= isset($moved_eoi_status->entry_date) ? date('d F Y',strtotime($moved_eoi_status->entry_date)) : ''; ?>   By <?php  if($moved_eoi_status->user_id){ $user =  UserNameById($moved_eoi_status->user_id); echo $user->userfullname; }  ?>
								</div>
								<?php 
								 
								}
								?>



								<?php 
 
								if($data['status']){
								foreach($data['status'] as $valuedata){
								if($valuedata->detailuser){	
									?>	
								<div class="highlight mb-2">
								<strong>Tender Uploaded !</strong> <?= isset($valuedata->detaildate) ? date('d F Y',strtotime($valuedata->detaildate)) : ''; ?>   By <?php  if($valuedata->detailuser){ $user =  UserNameById($valuedata->detailuser); echo $user->userfullname; }  ?>
								</div>

								   
										 
									<?php 

												}
												if($valuedata->activeuser){
									?>
													
													
													<div class="highlight mb-2">
								<strong>Tender Moved To Active From New Project </strong> <?= isset($valuedata->activedate) ? date('d F Y',strtotime($valuedata->activedate)) : ''; ?>   By <?php  if($valuedata->activeuser){ $user =  UserNameById($valuedata->activeuser); echo $user->userfullname; }  ?>
								</div>
													 
											
									<?php 		}	
									
												if($valuedata->impuser){
									?>
													
													<div class="highlight mb-2">
								<strong>Tender Moved To Important </strong> <?= isset($valuedata->impdate) ? date('d F Y',strtotime($valuedata->impdate)) : ''; ?>   By <?php  if($valuedata->impuser){ $user =  UserNameById($valuedata->impuser); echo $user->userfullname; }  ?>
								</div>
													
												 
									<?php 		
												}	
												if($valuedata->notimpuser){
									?>
													
													<div class="highlight mb-2">
								<strong>Tender Moved To NotImportant </strong> <?= isset($valuedata->notimpdate) ? date('d F Y',strtotime($valuedata->notimpdate)) : ''; ?>   By <?php  if($valuedata->notimpuser){ $user =  UserNameById($valuedata->notimpuser); echo $user->userfullname; }  ?>
								</div>
												 
											
									<?php 		
												}	
												if($valuedata->reviewuser){
									?>
													<div class="highlight mb-2">
								<strong>Tender Moved To In Review </strong> <?= isset($valuedata->reviewdate) ? date('d F Y',strtotime($valuedata->reviewdate)) : ''; ?>   By <?php  if($valuedata->reviewuser){ $user =  UserNameById($valuedata->reviewuser); echo $user->userfullname; }  ?>
								</div>
								 
											
									<?php 		
												}		
												if($valuedata->nogouser){
									?>
												<div class="highlight mb-2">
								<strong>Tender Moved To NoGO </strong> <?= isset($valuedata->nogodate) ? date('d F Y',strtotime($valuedata->nogodate)) : ''; ?>   By <?php  if($valuedata->nogouser){ $user =  UserNameById($valuedata->nogouser); echo $user->userfullname; }  ?>
								</div> 
											
									<?php 		
												}
												if($valuedata->togouser){
									?>
													<div class="highlight mb-2">
								<strong>Tender Moved To Ongoing Biding </strong> <?= isset($valuedata->togodate) ? date('d F Y',strtotime($valuedata->togodate)) : ''; ?>   By <?php  if($valuedata->togouser){ $user =  UserNameById($valuedata->togouser); echo $user->userfullname; }  ?>
								</div> 
											
									<?php 		
												}
												if($valuedata->inactiveuser){
									?>
													<div class="highlight mb-2">
								<strong>Tender Moved To In Active </strong> <?= isset($valuedata->inactivedate) ? date('d F Y',strtotime($valuedata->inactivedate)) : ''; ?>   By <?php  if($valuedata->inactiveuser){ $user =  UserNameById($valuedata->inactiveuser); echo $user->userfullname; }  ?>
								</div> 
											
									<?php 		
												}
												
												if($valuedata->biduser){
									?>
													<div class="highlight mb-2">
								<strong>Tender Moved To Submitted </strong> <?= isset($valuedata->biddate) ? date('d F Y',strtotime($valuedata->biddate)) : ''; ?>   By <?php  if($valuedata->biduser){ $user =  UserNameById($valuedata->biduser); echo $user->userfullname; }  ?>
								</div>
											
									<?php 		
												}
												if($valuedata->notsubmituser){
									?>
												<div class="highlight mb-2">
								<strong>Tender Moved To Not Submitted </strong> <?= isset($valuedata->notsubmitdate) ? date('d F Y',strtotime($valuedata->notsubmitdate)) : ''; ?>   By <?php  if($valuedata->notsubmituser){ $user =  UserNameById($valuedata->notsubmituser); echo $user->userfullname; }  ?>
								</div> 
											
									<?php 		
												}		
									
									?>
									<?php 
											}
										}
										
									?>
								  </ul>
								</div>
							  </div>
							  <div class="col-sm-5">
								<h5 class="mgbt-xs-15 font-semibold">  Description 
								 
								<button class="btn btn-info btn-sm p-2 pull-right"  data-toggle="modal" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i> Add Description Info</button>
								
								</h5>
								<div class="content-list">
								  <?php 
									if($data['description']){
								?>
									 


									<div class="">


									<?php 
											foreach($data['description'] as $valuedesc){
										?>
										<div class="col-sm-12">
										<div class="card border-danger mb-4 mt-2" style="box-shadow: none !important; border: 1px solid #dddddd !important; ">
										<div class="card-body text-danger">
										<?php 
										if($valuedesc->user_id){ $user = UserNameById($valuedesc->user_id);
										if($user->profileimg){
										?>	 
										<img class="img-responsive pull-right " style="height: auto; width: 60px;" src="http://172.16.1.30:8087/public/uploads/profile/<?= $user->profileimg ?>"> 
										<?php
										}else{
										?>
										<img class="img-responsive pull-right " style="height: auto; width: 60px; border-radius: 20px" src="<?= FRONTASSETS; ?>user/img/avatar/profile.jpg"> 		
										<?php 	
										}
										}
										?>


<h5 class="card-title"><?= isset($valuedesc->concern_details) ? $valuedesc->concern_details : ''; ?></h5>
										<p class="card-text">
										<span class="menu-info">
										<span class="menu-date">
										<?php 
										$OldDate = strtotime($valuedesc->entrydate);
										$NewDate = date('M j, Y', $OldDate);
										$diff = date_diff(date_create($NewDate),date_create(date("M j, Y")));
										$daysdiff =  $diff->format('%R%a days'); 
										?>
										~ <?= isset($daysdiff) ? $daysdiff : ''; ?> Ago By <?php  if($valuedesc->user_id){ $user =  UserNameById($valuedesc->user_id); echo $user->userfullname; }  ?>
										</span>
										</span></p>
										</div>
										</div>
										</div>	 
										
      

	  <?php 
												}
											
														
										?>

	  </div>
	  



										 
								 
								<?php 
									}
								?>	
								</div>            
							  </div>
							</div>
							<!-- row -->
							<hr class="pd-10"  />
							<div class="row clearfix">
							  <div class="col-sm-7">
								<h5 class="mgbt-xs-15 font-semibold">Document Link </h5>
								<div class="">
								  <div class="content-list">
									<?php
									if($data['comment']){
									?>
									<div data-rel="scroll" style="overflow:scroll;">
										<ul  class="list-wrapper">
										<?php 
											foreach($data['comment'] as $commenval){
										?>
												<li>  
													<span class="menu-icon vd_green">
														<?php 
															if($commenval->user_id){ $user = UserNameById($commenval->user_id);
															if($user->profileimg){
														?>	 
															<img alt="example image" src="http://172.16.1.30:8087/public/uploads/profile/<?= $user->profileimg ?>"> 
														<?php
															}else{
														?>
															<img alt="example image" src="<?= FRONTASSETS; ?>user/img/avatar/profile.jpg"> 		
														<?php 	
															}
															}
														?>
													</span> 
													<span class="menu-text"> 
														<?= isset($commenval->commentText) ? $commenval->commentText : ''; ?>
														<span class="menu-info">
															<span class="menu-date">
																<?php 
																	$OldDate = strtotime($commenval->entrydate);
																	$NewDate = date('M j, Y', $OldDate);
																	$diff = date_diff(date_create($NewDate),date_create(date("M j, Y")));
																	$daysdiff =  $diff->format('%R%a days'); 
																?>
																~ <?= isset($daysdiff) ? $daysdiff : ''; ?> Ago By <?php  if($commenval->user_id){ $user =  UserNameById($commenval->user_id); echo $user->userfullname; }  ?>
															</span>
														</span>
													</span>
												</li>
										<?php 
												}
										?>
										</ul>
									</div>
									<?php
									}
									?>
									
									<form method="post" action="<?php /* base_url('/projectplanning/usercomment/') */ ?>" id="submit_cmnt">
										<div class="closing" style=""> 
											<textarea name="user_comment" id="user_comment" class="form-control" cols="92" rows="4" style="display: none"> </textarea> 
											<input type="hidden" name="comment_id" id="comment_id" value="<?= $data['tender'][0]->fld_id; ?>" /> 
										</div>
									</form>
								  </div>
								</div>
							  </div>
							  <!-- col-sm-7 --> 
								<div class="col-sm-5">
									<h3 class="mgbt-xs-15 font-semibold">
										 
										<button class="btn btn-info btn-sm p-2 pull-right" title="Add Document Link" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i> Add Document Link </button>
									</h3>
									<div class="content-list content-menu">
										<ul class="list-wrapper">
											<?php 
												if($data['document']){
													foreach($data['document'] as $valuedoc){
											?>
													<li class="mgbt-xs-10"> 
														<span class="menu-icon vd_grey">
															<i class=" fa  fa-circle-o"></i>
														</span> 
														<span class="menu-text"> 
															<?php
																if($valuedoc->file_name) {
																	$filname = explode('__',$valuedoc->file_name);
																	

																	?>
																	<a href='<?php echo  HOSTURL . "projectdoc/" . $valuedoc->file_name ?>' target="_blank"><?= $filname[1] ?></a> &nbsp;&nbsp;
																	<a href='<?php echo  base_url("importantproject/removeproject/". $valuedoc->id .'/'.$data['tender'][0]->fld_id); ?>'><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span></a>
																<?php 
																}	
															?>
														</span> 
													</li>
											<?php 
													}
												}
												
											?>
										</ul>
									</div>
										
								</div>
							  <!-- col-sm-7 -->           
							</div>
							
							 


								</div>
								<div class="tab-pane mt-4" id="Profile-new">

								<div class="">
								<div class="header">
                            <h2>Profile Info</h2>
                        </div>
                        <div class="body">
								 
						<div class="row">
			 
				<div class="col-md-4">
				  <label class="col-xs-5 control-label">Service:</label>
				  <div class="col-xs-7 controls"><?= isset($data['info'][0]->service_name) ? $data['info'][0]->service_name  : '';  ?></div>
				  <!-- col-sm-10 --> 
				</div>
			 
			 
				<div class="col-md-4">
				  <label class="col-xs-5 control-label">Sector:</label>
				  <div class="col-xs-7 controls"><?= isset($data['info'][0]->sectName) ? $data['info'][0]->sectName  : '';  ?></div>
				  <!-- col-sm-10 --> 
				</div>
		 
		 
				<div class="col-md-4">
					<label class="col-xs-5 control-label">State:</label>
					<div class="col-xs-7 controls"><?= isset($data['info'][0]->state_name) ? $data['info'][0]->state_name  : '';  ?></div>
				</div>
			 
			<?php if($data['info'][0]->bid_security ==1){
			?>
				 
					<div class="col-md-4">
						<label class="col-xs-5 control-label">Bid Vallidity Date:</label>
						<div class="col-xs-7 controls"><?= isset($data['info'][0]->tender_openingdate) ? $data['info'][0]->tender_openingdate  : '';  ?></div>
					</div>
				 
				 
					<div class="col-md-4">
						<label class="col-xs-5 control-label">Security Type:</label>
						<div class="col-xs-7 controls"><?= isset($data['info'][0]->security_type) ? $data['info'][0]->security_type  : '';  ?></div>
					</div>
				 
				 
					<div class="col-md-4">
						<label class="col-xs-5 control-label">Amount:</label>
						<div class="col-xs-7 controls"><?= isset($data['info'][0]->amount) ? $data['info'][0]->amount  : '';  ?></div>
					</div>
				 
			<?php } ?>
			 
		
			</div>	</div>
			</div>



								</div>
								<div class="tab-pane mt-4" id="all-new">


								<div class="">
								<div class="header">
                            <h2>Team Info</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table center-aligned-table">
                                    <thead>
                                    <tr>
									<th>Name</th>
									<th>Position</th>
									<th>Work Category</th>
									<th>Total MM</th>
									<th>Balance MM</th>
									<th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                     
									<tr><td colspan="6" style="font-size: 16px;    color: white;padding: 10px;background-color: #7386d5;">Key Professional</td></tr>
				<?php 
				foreach($teamdata as $val){
					if(!empty($val->userfullname)){
						if($val->key_id == 1){
							
				?>
				<tr>
					<td style="font-size: 12px;"><?= $val->userfullname; ?></td>
					<td style="font-size: 12px;"><?= $val->PositionName; ?></td>
					<td style="font-size: 12px;"><?= $val->WorkCategory; ?></td>
					<td style="font-size: 12px;"><?= $val->man_months; ?></td>
					<td style="font-size: 12px;"><?= $val->balancemm; ?></td>
					<td style="font-size: 12px;"><b><a target="_blank" href="<?= base_url('/projectplanning/userview/'.$val->project_id .'/'. $val->empname); ?>">View</a></b></td>
				</tr>
				<?php 
						}
					}
				} ?>
				
				<tr><td  colspan="6" style="font-size: 16px;    color: white;padding: 10px;background-color: #7386d5;">Sub Professional</td></tr>
				<?php 
				foreach($teamdata as $val){
					if(!empty($val->userfullname)){
						if($val->key_id == 2){
							
				?>
				<tr>
					<td style="font-size: 12px;"><?= $val->userfullname; ?></td>
					<td style="font-size: 12px;"><?= $val->PositionName; ?></td>
					<td style="font-size: 12px;"><?= $val->WorkCategory; ?></td>
					<td style="font-size: 12px;"><?= $val->man_months; ?></td>
					<td style="font-size: 12px;"><?= $val->balancemm; ?></td>
					<td style="font-size: 12px;"><b><a target="_blank" href="<?= base_url('/projectplanning/userview/'.$val->project_id .'/'. $val->empname); ?>">View</a></b></td>
				</tr>
				<?php 
						}
					}
				} ?>
				
				<tr><td  colspan="6" style="font-size: 16px;color: white;padding: 10px;background-color: #7386d5;">Administration Staff</td></tr>
				<?php 
				foreach($teamdata as $val){
					if(!empty($val->userfullname)){
						if($val->key_id == 2){
							
				?>
				<tr>
					<td style="font-size: 12px;"><?= $val->userfullname; ?></td>
					<td style="font-size: 12px;"><?= $val->PositionName; ?></td>
					<td style="font-size: 12px;"><?= $val->WorkCategory; ?></td>
					<td style="font-size: 12px;"><?= $val->man_months; ?></td>
					<td style="font-size: 12px;"><?= $val->balancemm; ?></td>
					<td style="font-size: 12px;"><b><a target="_blank" href="<?= base_url('/projectplanning/userview/'.$val->project_id .'/'. $val->empname); ?>">View</a></b></td>
				</tr>
				<?php 
						}
					}
				} ?>
                                    </tbody>
                                </table>
                             
                </div>
				</div> </div>




 
	 
	 


								</div>
								<div class="tab-pane mt-4" id="other-data"> 



								 
								<div class="">
                        <div class="header">
                            <h2>Competitor Detail</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table center-aligned-table">
                                    <thead>
                                    <tr>
									<th>Sr.</th>
									<th>Company Name</th>
									<th>Technical</th>
									<th>Marks</th>
									<th>Financial</th>
									<th>Marks</th>
									<th>Total</th>
									<th>Rank</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                     
                                    <?php 
					if($cegcompcomp){
						$ssnn = 0;
						//start Rank Code
						$sortArr = array();
						$temrArr = array();
						foreach ($cegcompcomp as $comptrow) {
							$totRec7 = ($comptrow->technical_marks + $comptrow->financial_marks);
							$temrArr[$comptrow->fld_id] = number_format($totRec7, 2);
						}
						arsort($temrArr);
						$finalRank = array();
						$nnmms = 1;
						foreach ($temrArr as $kky => $rwrank):
								$finalRank[$kky] = $nnmms;
								$nnmms++;
							endforeach;
							//Close Rank Code
							
						if ($cegcompcomp):
							foreach ($cegcompcomp as $comptrow) {
						?>
							<tr>
								<td><?= $ssnn = $ssnn + 1; ?></td>
								<td>&nbsp; <?= $this->mastermodel->GetCompNameDetailById($comptrow->compt_comp_id,$comptrow->project_id); ?></td>
								<td><?= number_format($comptrow->technical_score, 2); ?></td>
								<td><?= number_format($comptrow->technical_marks, 2); ?></td>
								<td><?= number_format($comptrow->financial_score, 2); ?></td>
								<td><?= number_format($comptrow->financial_marks, 2); ?></td>
								<td><?php 
									$totRec = ($comptrow->technical_marks + $comptrow->financial_marks);
									echo number_format($totRec, 2);
								?></td>
									<td><?= $finalRank[$comptrow->fld_id]; ?></td>
							</tr>
								<?php
							}
						endif;
					}
						?>
                                    </tbody>
                                </table>
                             
                </div>
			</div>
								</div>




		
                                    
                    </div>
                </div>
            </div>
        </div>
    </div>

                <div class="row clearfix">
			
			<!-- <div id="content" class="full-layout  nav-right-hide nav-right-start-hide  nav-top-fixed      responsive    clearfix" data-active="pages "  data-smooth-scrolling="1">
				
				
				<div class="vd_body">

<div class="content"> -->
  <div class="container">
       
    <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container" style="margin-left:0;">
        <div class="vd_content clearfix">
          
           
         
          <!-- .vd_content-section --> 
	
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
			
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header">
			<h4 class="modal-title">Description</h4>  
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			  
			</div>
			<div class="modal-body">
				<form action="<?= base_url('importantproject/projectdesc'); ?>" method="post" enctype="multipart/form-data mvc">
					<script src="<?= FRONTASSETS; ?>ckeditor/ckeditor.js"></script>
					<textarea required cols="70" id="editor1" name="editor1" rows="10"><?= ($data != '') ? $data[0]->concern_details : ''; ?></textarea>
					<script type="text/javascript"> CKEDITOR.replace( 'editor1' );</script>
					<input type="hidden" name="desc_id" id="desc_id" value="<?= $data['tender'][0]->fld_id; ?>" /> 
					
				
			</div>
			<div class="modal-footer">
			<button type="submit" id="btmsubmit" class="btn btn-info btn-sm pull-right">Submit</button>
			</div>
		  </div>
		  </form>
		</div>
	</div>
	
	
	
	<div class="modal fade" id="myModal1" role="dialog">
		<div class="modal-dialog">
			
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header">
			<h4 class="modal-title">Document Upload</h4>  
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			 
			    
			</div>
			<div class="modal-body">
			<h4 id="uploaderror2" class="modal-title" style="color:red"></h4>  
				<form action="<?= base_url('importantproject/projectdocumentupload'); ?>" method="post" enctype="multipart/form-data">
					<input type="file" class="form-control" name="docfile[]"  id="docfile" multiple>
					<input type="hidden" name="proj_id" id="proj_id" value="<?= $data['tender'][0]->fld_id; ?>" /> 
					
			</div>
			<div class="modal-footer">
			<button type="submit" id="btmsubmit" class="btn btn-info btn-sm pull-right">Submit</button>
			
			</div>
		  </div>
		  </form>
		</div>
	</div>
  
	
	<div class="modal fade" id="clientmodel" role="dialog">
            <div class="modal-dialog" style="width:60%;">
                <div class="modal-content">
					<div class="modal-header">
					<h4 class="modal-title">Add Reminder</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
					
                    <div class="modal-body">
                        <form name="frmm" id="frmm" method="post" action="<?= base_url('importantproject/reminderinfo'); ?>" enctype="">
                            <div class="row">
								<div class="form-group required col-md-6">  
									<label> Reminder Date</label>
									<input required type="date" class="form-control" id="reminder_date" name="reminder_date" value="">
								</div>
								<div class="form-group required col-md-6">  
									<label> Subject</label>
									<input required type="text" class="form-control" id="reminder_subject" name="reminder_subject" value="">
								</div>
								<div class="form-group required col-md-12">  
									<label> Message</label>
									<textarea required name="reminder_message" class="form-control"> </textarea>
								</div>
                                <div class="form-group required col-md-12">
                                    <input type="hidden" name="project_id" id="project_iddata" class="btn green" >
                                    <input type="submit" class="btn btn-info pull-right" value="Submit">
                                </div>
                            </div>
							
							<div class="row">
								<div class="form-group required col-md-12"> 
									<table border="1" id="clientadd" style="width:100%"></table>
								</div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
	</div>
	
  
</div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 
    
    <!-- Middle Content End --> 
    
  <?php /*</div>
   
</div>


  

</div>*/?>

<!-- .vd_body END  -->
<!--<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>-->
	

			<!-- </div> -->


			<!-- </div> -->
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    <?php $this->load->view('admin/includes/footer'); ?>
    </div>
	
 
<script type="text/javascript">

function editclientmodel(projid) {
	if (projid) {
		$('#project_iddata').val(projid);
		$('#clientadd').html('');
		$.ajax({
			type: 'POST',
			url: "<?= base_url('importantproject/ajax_reminderinfo'); ?>",
			data: {'project_id': projid},
			success: function (responData) {
				var parsed = jQuery.parseJSON(responData);
				if(parsed){
					$('#clientadd').append('<tr><th>Date</th><th>Subject</th><th>Message</th><th>Action</th></tr>');
					$.each(parsed,function(index,val){
						//console.log(val.client_name);
						$('#clientadd').append('<tr><td>'+ val.reminder_date +'</td><td>'+ val.reminder_subject +'</td><td>'  + val.reminder_message + '</td><td><a href="<?= base_url('importantproject/ajax_reminderdel'); ?>/'+ val.id +'/'+ projid +'" class="clientinfodel">Delete</a></td></tr>');
					});
				}
				
			},
		});
	}
}

var acc = document.getElementsByClassName("accordion1");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}

$(document).ready(function() {
	"use strict";
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	  $container.isotope('layout');
	});

  // bind filter button click
	$('#filters').on( 'click', 'a', function() {
		var filterValue = $( this ).attr('data-filter');
		$('#filters li').removeClass('active');
		$(this).parent().addClass('active');	
		$container.isotope({ filter: filterValue });
	});

	
	/* $('#user_comment').keydown(function(){
		var message = $("#user_comment").val();
		alert(message);
		if (event.keyCode == 13) {
			if (message == "") {
				alert("Enter Some Text In Textarea");
			} else {
				$('#submit_cmnt').submit();
				alert("Succesfully submitted:- " + message);
			}
			$("textarea").val('');
			return false;
		}
	});  */
	
	
	
});

	$(document).ready(function () {
		$('#user_comment').keypress(function (e) {
			var key = e.which;
			if (key == 13) {
				var user_comment = $("#user_comment").val();
				var comment_id = $("#comment_id").val();
				//Comment Submit..
				 $.ajax({
					type: 'POST',
					dataType: "text",
					url: "<?= base_url('importantproject/usercommentonproject/'); ?>",
					data: {'user_comment': user_comment, 'comment_id': comment_id},
					success: function (responData) {
						location.reload(1);
					},
				}); 
			}
		});
	});

	
	
	$(document).ready(function () {
		$("#uploaderror2").hide();
		$('INPUT[type="file"]').change(function () {
			var ext = this.value.match(/\.(.+)$/)[1];
			switch (ext) {
				case 'doc':
				case 'DOC':
				case 'docx':
				case 'pdf':
				case 'PDF':
				case 'xlsx':
				case 'xls':
				case 'jpg':
				case 'jpeg':
				case 'JPG':
				case 'JPEG':
				case 'png':
				case 'PNG':
					$('#uploadButton').attr('disabled', false);
					$("#uploaderror2").hide();
					break;
				default:
					$("#uploaderror2").show();
					$("#uploaderror2").html('This is not an allowed file type.');
					this.value = '';
			}
		});
		$('INPUT[type="file"]').bind('change', function () {
			var imgfilesize = (this.files[0].size / 1001376);
			if (imgfilesize > 100) {
				$("#uploaderror2").show();
				$("#uploaderror2").html('Reducing File Size Max Uploads (99 MB). ');
				this.value = '';
			} else {
				$("#uploaderror2").hide();
			}
		});
	});
</script>
	</body>
</html>
