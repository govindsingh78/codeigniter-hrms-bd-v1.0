<html lang="en">
    <?php error_reporting(E_ALL); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <?php
//Financial Year Condition Code By Asheesh.. 14-02-2019.
        $year = '2016';
        $YearPluse1 = (date('Y') + 1);
        $finCdateStart = "01-04-" . date('Y');
// $finCdateEnd = "31-03-" . $YearPluse1;
        $gEtCuurentDate = date("d-m-Y");
        $gEtCuurentDateCONV = strtotime($gEtCuurentDate);
        $finCdateStartCONV = strtotime($finCdateStart);
        if ($finCdateStartCONV >= $gEtCuurentDateCONV):
            $currentyear = date('Y') - 1;
        else:
            $currentyear = date('Y');
        endif;
        ?>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <div class="wrapper">
            <?php $this->load->view('include/sidebar'); ?>
            <div id="content" class="col-lg-10 col-sm-10">
                <!-- content starts -->
                <div>
                    <ul class="breadcrumb">
                        <li> <a href="<?= base_url(); ?>">Home</a></li>
                        <li>
                            <a href="#"><?= $title; ?></a>
                        </li>
                    </ul>
                </div>

                <div class="row">
                    <?php if ($this->session->flashdata('msg')) { ?>
                        <div class="alert alert-success alert-dismissable" style="">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                        </div>
                    <?php } ?>


                    <div class="box col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form id="form-filter" class="form-horizontal">
                                    <div class="row">
                                        <div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                            <label class="col-sm-3 control-label" for="selectError2">Sector </label>
                                            <div class="col-sm-8">
                                                <select name="sectorinput" id="sectorinput"  class="form-control">
                                                    <option <?= ($secId == "") ? 'Selected' : ''; ?> value="">--All--</option>
                                                    <?php
                                                    if ($sectorArr):
                                                        foreach ($sectorArr as $row) {
                                                            ?>
                                                            <option <?= ($secId == $row->fld_id) ? 'Selected' : ''; ?> value="<?= $row->fld_id; ?>"><?= $row->sectName; ?></option>
                                                            <?php
                                                        }
                                                    endif;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                            <label for="" class="col-sm-2 control-label">Filter </label>
                                            <div class="col-sm-10">
                                                <select id="company_name" class="form-control">
                                                    <option value="" selected="selected">--Type--</option>
                                                    <option value="E">EOI</option>
                                                    <option value="P">RFP</option>
                                                    <option value="FQ">FQ</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4" style="padding: 15px;background-color: #f1f1f1;border: 1px solid #D8D8D8;margin: 1%;">
                                            <label for="" class="col-sm-4 control-label">Financial Year </label>
                                            <div class="col-sm-6">
                                                <select id="financial_year" class="form-control">
                                                    <!--<option value="" selected="selected">--Financial Year--</option>-->
													<option <?php echo ($currentyear == '') ? 'selected' : ''; ?> value="">--Select All--</option>
                                                    
                                                    <?php
                                                    for ($i = $year; $i <= 2022; $i++) {
                                                        ?>
                                                        <option <?php echo ($currentyear == $i) ? 'selected' : ''; ?> value=<?= $i; ?>><?= $i; ?>-<?= $i+1; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4" style="margin: 1%;">
                                        <div class="col-sm-12">
                                            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button> &nbsp;&nbsp;
                                            <button type="button" id="btn-reset" class="btn btn-primary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- open Phase Section-->
                    <div class="box-inner">
                        <div class="box-content">
                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="colvis"></div>
                                        </div>
                                    </div>
                                    <table id="table" class="display" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th style="width:10%">Exp-Date</th>
                                                <th>TenderID</th>
                                                <th>Tender Details</th>
                                                <th>Location</th>
                                                <th>Client</th>
                                                <th style="width:20%">Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Exp-Date</th>
                                                <th>TenderID</th>
                                                <th>Tender Details</th>
                                                <th>Location</th>
                                                <th>Client</th>
                                                <th>Actions </th>
                                            </tr>
                                        </tfoot>
                                        <hr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Close phase Section-->

            </div>


        </div>	

        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>
        <hr>
        <!-- Assign Project Manager Modal Popup -->
        <div class="modal fade" id="assignmanager" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 id="tender_title" class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <form action="<?= base_url('togoproject/assign_proposal_manager'); ?>" method="post">
                            <div class="box-content">
                                <div class="controls">
                                    <?php // $userAll = GetAllUserRec();            ?>
                                    <label>Select Proposal Manager..</label>
                                    <select required="required" class="form-control"  name="proposal_manager">
                                        <option value="">--select--</option>
                                        <?php foreach ($proposalManager as $rowrec) { ?>
                                            <option value="<?php echo $rowrec->user_id; ?>"><?php echo $rowrec->userfullname; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="box-content">
                                <input type="hidden" id="assigntendid" name="project_id" value="">
                                <input type="submit"  value="Assign" class="btn btn-success glyphicon glyphicon-user" >
                            </div>

                            <div class="modal-footer">
                                <button class="btn btn-default" data-dismiss="modal">X</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



        <!-- Generate Project ID Modal -->
        <div class="modal fade" id="generateprojid" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 id="tender_title" class="modal-title">Generate Project No. </h4>
                    </div>
                    <div class="modal-body">
                        <form action="<?= base_url('togoproject/generate_project_no'); ?>" method="post">
                            <div class="box-content">
                                <div class="controls">
                                    <?php $userAll = GetAllPrefixRec(); ?>
                                    <label>Prefix</label>
                                    <select required="required" class="form-control" id="prefx_name" name="prefx_name">
                                        <option value="">--select--</option>
                                        <?php
                                        foreach ($userAll as $rowrec) {
                                            ?>
                                            <option value="<?= $rowrec->prefix_id; ?>"><?= $rowrec->prefix_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div id="projno" style="text-align: center;">&nbsp;</div>
                            <div class="box-content">
                                <?php foreach ($userAll as $rowrec) { ?>
                                    <input type="hidden" id="<?= $rowrec->prefix_id; ?>" name="<?= $rowrec->prefix_id; ?>" value="<?= $rowrec->last_generate_id; ?>">
                                <?php } ?>
                                <input type="hidden" id="assigntenderid" name="project_id" value="">
                                <input type="submit"  value="Generate" class="btn btn-success glyphicon glyphicon-user" >
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-default" data-dismiss="modal">X</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <!-- Modal For Add  Jv In Project -->
        <div id="myModaljv" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <form action="<?= base_url('company/savejvdata'); ?>" method="post">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"> Consortium </h4>
                            <h5 class="modal-title" align="center"> Sole : <input type="checkbox" checked id="soleornot"> </h5>
                        </div>
                        <div class="modal-body" id="jvsection" style="display:none">
                            <div class="controls">
                                <label>Lead Company </label>
                                <select id="multiple" name="leadcompany[]" style="width:100%" multiple>    
                                    <?php foreach ($compnameArr as $rowrec) { ?>
                                        <option value="<?= $rowrec->fld_id; ?>">
                                            <?= ucfirst($rowrec->company_name); ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="controls">   
                                <label> JV (<small> joint venture </small>)</label>
                                <select id="multiple1" name="joint_venture[]" style="width:100%" multiple>    
                                    <?php foreach ($compnameArr as $rowrec) { ?>
                                        <option value="<?= $rowrec->fld_id; ?>">
                                            <?= ucfirst($rowrec->company_name); ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="controls">
                                <label> Associate company..</label>
                                <select id="multiple2" name="assoc[]" style="width:100%" multiple>    
                                    <?php foreach ($compnameArr as $rowrec) { ?>
                                        <option value="<?= $rowrec->fld_id; ?>">
                                            <?= ucfirst($rowrec->company_name); ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <br>
                            <div class="controls">
                                <input type="hidden" name="projid" id="projid">
                                <input type="submit" class="btn" name="submit" value="Add / Save">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>


        <div id="editproj" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <form action="<?= base_url('togoproject/editupdateproject'); ?>"  name="edtform" id="edtform" method="post">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"> Edit / Update Project </h4>
                            <h2 style="color:red" id="errorp"></h2>
                        </div>
                        <div class="modal-body" id="jvsection" style="">
                            <div class="controls">   
                                <label class="control-label" for="inputSuccess1"> Project Details </label>
                                <textarea required cols="10" class="form-control" id="eprojectdt" name="eprojectdt" rows="7"></textarea>
                            </div>
                            <div class="controls">   
                                <label> Expiry Date</label>
                                <input type="date" class="form-control" id="expr_date" name="expr_date" value="">
                            </div>
                            <div class="controls">   
                                <label>Location</label>
                                <input type="text" class="form-control" id="location" name="location" value="">
                            </div>
                            <div class="controls">   
                                <label>Client</label>
                                <input type="text" class="form-control" id="organization" name="organization" value="">
                            </div> 
                            <br>
                            <div class="controls">
                                <input type="hidden" name="projid" id="updprojid">
                                <input type="button" id="updprojectbtn" class="btn" name="submit" value="Update / Save">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>



        <!-- Modal For Edit Basic Details -->
        <div class="modal fade" id="myModalbasic" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Basic Project Details</h4>
                    </div>

                    <div class="modal-body">
                        <form name="frmm" id="frmm" method="post" action="<?= base_url('togoproject/insertproject'); ?>" enctype="">
                            <div class="row">
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Sector : </label>
                                    <select required id="sector" name="sectors_id" class="form-control col-md-4 col-xs-12">
                                        <option value=""> -- Select Sector -- </option>
                                        <?php
                                        if ($sectorArr):
                                            foreach ($sectorArr as $rowr) {
                                                ?>
                                                <option value="<?= $rowr->fld_id; ?>"> <?= $rowr->sectName; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>  
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Services : </label>
                                    <select required id="services" name="services_id" class="form-control col-md-4 col-xs-12">
                                        <option value=""> -- Select Services -- </option>
                                        <?php
                                        if ($serviceArr):
                                            foreach ($serviceArr as $rowr) {
                                                ?>
                                                <option value="<?= $rowr->id; ?>"> <?= $rowr->service_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Country : </label>
                                    <select required id="country" name="countries_id" onchange="setstatebycid()" class="form-control col-md-4 col-xs-12">
                                        <option value=""> -- Select Country -- </option>
                                        <?php
                                        if ($country_Arr):
                                            foreach ($country_Arr as $rowr) {
                                                ?>
                                                <option value="<?= $rowr->country_id; ?>"> <?= $rowr->country_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>

                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">State : </label>
                                    <select id="state" name="state_id" class="form-control col-md-4 col-xs-12" >
                                        <?php
                                        if ($stateArr):
                                            foreach ($stateArr as $rowrsec) {
                                                ?>
                                                <option value="<?= $rowrsec->state_id; ?>"> <?= $rowrsec->state_name; ?></option>
                                            <?php } endif; ?>
                                    </select>
                                </div>
                                <div class="form-group required col-md-6">
                                    <label class="control-label" for="usr">Bid Security : <small style="color:green"> (Bid Security)</small> </label>
                                    <select required id="bid_security" name="bid_security" class="form-control col-md-4 col-xs-12">
                                        <option value="2"> No </option>
                                        <option value="1"> Yes </option>
                                    </select>
                                </div>

                                <div id="security_demnd" style="display:none">
                                    <div class="form-group required col-md-6">
                                        <label class="control-label" for="usr">Bid Security Type : <small style="color:green"> (Bid Security)</small></label>
                                        <select id="bid_securitytype" name="bid_securitytype" class="form-control col-md-4 col-xs-12" >
                                            <option value=""> -- Select Type-- </option>
                                            <?php
                                            if ($security_Type):
                                                foreach ($security_Type as $rowrsec) {
                                                    ?>
                                                    <option value="<?= $rowrsec->id; ?>"> <?= $rowrsec->security_type; ?></option>
                                                <?php } endif; ?>
                                        </select>
                                    </div>
                                    <div class="form-group required col-md-6">  
                                        <label> Amount : <small style="color:green"> (Bid Security)</small> </label>
                                        <input type="text" class="form-control" id="amount" name="amount" value="">
                                    </div>
                                    <div class="form-group required col-md-6">  
                                        <label> Validity : <small style="color:green"> (Bid Security)</small>  </label>
                                        <input type="date" class="form-control" id="bs_valdity" name="bs_valdity" value="">
                                    </div>
                                </div>

                                <!--                                RFP Section Start-->
                                <div class="form-group required col-md-6">  
                                    <label>Cost Of RFP : <small style="color:#7386D5">(RFP)</small> </label>
                                    <select required id="rfp_cost" name="rfp_cost" class="form-control col-md-4 col-xs-12">
                                        <option value="2"> No </option>
                                        <option value="1"> Yes </option>
                                    </select> 
                                </div>

                                <div id="security_demnd2" style="display:none">
                                    <div class="form-group required col-md-6">  
                                        <label>Amount : <small style="color:#7386D5">(RFP)</small></label>
                                        <input type="text" class="form-control" id="rfp_amount" name="rfp_amount" value="">
                                    </div>
                                    <div class="form-group required col-md-6">  
                                        <label> Type : <small style="color:#7386D5">(RFP)</small></label>
                                        <select id="rfp_type" name="rfp_type" class="form-control col-md-4 col-xs-12" >
                                            <option value=""> -- Select Type-- </option>
                                            <?php
                                            if ($security_Type):
                                                foreach ($security_Type as $rowrsec) {
                                                    if ($rowrsec != '5') {
                                                        ?>
                                                        <option value="<?= $rowrsec->id; ?>"> <?= $rowrsec->security_type; ?></option>
                                                        <?php
                                                    }
                                                } endif;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <!--   RFP Section Close -->


                                <!-- Proccessing Section Start-->
                                <div class="form-group required col-md-6">  
                                    <label>Processing Fee : <small style="color:#1995dc">(Processing Fee)</small></label>
                                    <select required id="processing_fee" name="processing_fee" class="form-control col-md-4 col-xs-12">
                                        <option value="2"> No </option>
                                        <option value="1"> Yes </option>
                                    </select> 
                                </div>

                                <div id="security_demnd3" style="display:none">
                                    <div class="form-group required col-md-6">  
                                        <label>Amount : <small style="color:#1995dc">(Processing Fee)</small> </label>
                                        <input type="number" step="any" min="1" class="form-control" id="pfee_amount" name="pfee_amount" value="">
                                    </div>
                                    <div class="form-group required col-md-6">  
                                        <label> Type : <small style="color:#1995dc">(Processing Fee)</small></label>
                                        <select id="processing_fee_type" name="processing_fee_type" class="form-control col-md-4 col-xs-12" >
                                            <option value=""> -- Select Type-- </option>
                                            <?php
                                            if ($security_Type):
                                                foreach ($security_Type as $rowrsec) {
                                                    if ($rowrsec != '5') {
                                                        ?>
                                                        <option value="<?= $rowrsec->id; ?>"> <?= $rowrsec->security_type; ?></option>
                                                        <?php
                                                    }
                                                } endif;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <!-- Close Section Start-->

                                <div class="form-group required col-md-6">  
                                    <label> Submission Date : </label>
                                    <input type="date" class="form-control" id="submission_date" name="submission_date" value="">
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> Bid Validity Date : </label>
                                    <input type="date" class="form-control" id="tender_openingdate" name="tender_openingdate" value="">
                                </div>
                                <div class="form-group required col-md-12">
                                    <input type="hidden" name="serviceprojid" id="serviceprojid" class="btn green" >
                                    <input type="submit" class="btn green" value="Update">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



        <div class="modal fade" id="clientmodel" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Client Info</h4>
                    </div>

                    <div class="modal-body">
                        <form name="frmm" id="frmm" method="post" action="<?= base_url('togoproject/clientinfo'); ?>" enctype="">
                            <div class="row">
                                <div class="form-group required col-md-6">  
                                    <label> Client Name</label>
                                    <input type="text" class="form-control" id="client_name" name="client_name" value="">
                                </div>
                                <div class="form-group required col-md-6">  
                                    <label> Client Contact</label>
                                    <input type="number" class="form-control" id="client_contact" name="client_contact" value="">
                                </div>
                                <div class="form-group required col-md-12">  
                                    <label> Client Position</label>
                                    <input type="text" class="form-control" id="client_position" name="client_position" value="">
                                </div>
                                <div class="form-group required col-md-6">
                                    <input type="hidden" name="project_id" id="project_iddata" class="btn green" >
                                    <input type="submit" class="btn green" value="Submit">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group required col-md-12"> 
                                    <table border="1" id="clientadd" style="width:100%"></table>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>



        <!-- BootStrap Model For Comments.. -->
        <?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->
        <?php $this->load->view('include/footer'); ?>
        <?php $this->load->view('include/datatablejs'); ?>
        <style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            #chatbox {padding: 15px;overflow: scroll;height: 300px;}
        </style>
        <script type="text/javascript">
            $('#bid_security').on('change', function () {
                var id = $(this).val();
                if (id == 1) {
                    $('#security_demnd').css('display', 'block');
                } else {
                    $('#security_demnd').css('display', 'none');
                }
            });
            //sTEP 2 hIDE sHOW,,.. 
            $('#rfp_cost').on('change', function () {
                var id = $(this).val();
                if (id == 1) {
                    $('#security_demnd2').css('display', 'block');
                } else {
                    $('#security_demnd2').css('display', 'none');
                }
            });
            //sTEP 3 hIDE sHOW..
            $('#processing_fee').on('change', function () {
                var id = $(this).val();
                if (id == 1) {
                    $('#security_demnd3').css('display', 'block');
                } else {
                    $('#security_demnd3').css('display', 'none');
                }
            });


            function setstatebycid() {
                var countryid = $("#country").val();
                $('#state').html('');
                $.ajax({
                    url: '<?= base_url('company/ajax_state_getbycid') ?>',
                    method: 'post',
                    data: {country: countryid},
                    dataType: 'json',
                    success: function (response) {
                        $.each(response, function (index, data) {
                            $('#state').append('<option value="' + data['state_id'] + '">' + data['state_name'] + '</option>');
                        });
                    }
                });
            }

            $(document).ready(function () {
                $("#soleornot").click(function () {
                    if ($(this).prop('checked') == true) {
                        $("#jvsection").hide();
                    } else {
                        $("#jvsection").show();
                    }
                });
            });

            $(document).ready(function () {
                $("li#ongoing_search").addClass('active');
                $("li#togoproject").addClass('active');
            });

            $(document).ready(function () {
                $("#checkAll").click(function () {
                    $('input:checkbox').not(this).prop('checked', this.checked);
                });
            });
            function assignpm(projid) {
                $("#assigntenderid").val(projid);
                $("#assigntendid").val(projid);
            }
            function gocomment(tndrid) {
                //Put ProjId And Secot Id..
                $("#projId").val(tndrid);
                $.ajax({
                    type: 'POST',
                    dataType: "text",
                    data: {'tndrid': tndrid},
                    url: "<?= base_url('togoproject/commentset'); ?>",
                    success: function (responData) {
                        $("#chatbox").html(responData);
                    },
                });
            }
            //Comment Submitted...
            $(document).ready(function () {
                $('#commentmsg').keypress(function (e) {
                    var key = e.which;
                    if (key == 13) {
                        var comnt = $("#commentmsg").val();
                        var projId = $("#projId").val();
                        //Comment Submit..
                        $.ajax({
                            type: 'POST',
                            dataType: "text",
                            url: "<?= base_url('togoproject/commentsubmit'); ?>",
                            data: {'projId': projId, 'comnt': comnt},
                            success: function (responData) {
                                $("#commentmsg").val('');
                                return gocomment(projId);
                                //location.reload(1);
                            },
                        });
                    }
                });
            });

            //Project No Go..
            function  nosubmitproj(tndrid, sectid) {
                if (confirm("Are You Sure No Submitted This.") == true) {
                    $.ajax({
                        type: 'POST',
                        url: "<?= base_url('togoproject/projnosubmit'); ?>",
                        data: {'actid': tndrid},
                        success: function (responData) {

                            console.log(responData);
                            $('#alert').css('display', 'block');
                            $('#msgs').html('Tender Important.');
                            setTimeout(function () {
                                location.reload(1);
                            }, 1000);

                        },
                    });
                }
            }

            function bidproject(actprojid, sectid) {
                if (confirm("Are You Sure Submitted This Project ? ") == true) {
                    $.ajax({
                        type: 'POST',
                        url: "<?= base_url('togoproject/bidprojectbyuser'); ?>",
                        data: {'projid': actprojid},
                        success: function (responData) {
                            console.log(responData);
                            $('#alert').css('display', 'block');
                            $('#msgs').html('Tender Important.');
                            setTimeout(function () {
                                location.reload(1);
                            }, 1000);

                        },
                    });
                }
            }
            

            //Edit Project Code By Asheesh..
            function editproject(projid) {
                if (projid) {

                    $.ajax({
                        type: 'POST',
                        url: "<?= base_url('togoproject/ajax_projectdetailbyid'); ?>",
                        data: {'projid': projid},
                        success: function (responData) {
                            var parsed = JSON.parse(responData);
                            var arr = [];
                            for (var x in parsed) {
                                arr.push(parsed[x]);
                            }
                            console.log(arr);

                            $("#expr_date").val(arr[14]);
                            $("#location").val(arr[5]);
                            $("#organization").val(arr[6]);
                            $("#updprojid").val(arr[0]);
                            $("#eprojectdt").val(arr[3]);
                        },
                    });
                }
            }
            //Close Edit Project Section By Asheesh..
            function editprojectdetail(projid, exp_date) {
                if (projid) {
                    $('#serviceprojid').val(projid);
                    $('#submission_date').val(exp_date);
                }
            }

            function editclientmodel(projid) {
                if (projid) {
                    $('#project_iddata').val(projid);
                    $('#clientadd').html('');
                    $.ajax({
                        type: 'POST',
                        url: "<?= base_url('togoproject/ajax_clientinfo'); ?>",
                        data: {'project_id': projid},
                        success: function (responData) {
                            var parsed = jQuery.parseJSON(responData);
                            if (parsed) {
                                $('#clientadd').append('<tr><th>Client Name</th><th>Client Contact</th><th>Client Position</th><th>Action</th></tr>');
                                $.each(parsed, function (index, val) {
                                    //console.log(val.client_name);
                                    $('#clientadd').append('<tr><td>' + val.client_name + '</td><td>' + val.client_contact + '</td><td>' + val.client_position + '</td><td><a href="<?= base_url('togoproject/ajax_clientinfodel'); ?>/' + val.id + '" class="clientinfodel">Delete</a></td></tr>');
                                });
                            }

                        },
                    });
                }
            }

            //Update Afdter Click On Button Code By Asheesh..
            $("#updprojectbtn").click(function () {
                var form = $("#edtform");
                $.ajax({
                    type: 'POST',
                    url: "<?= base_url('togoproject/editupdateproject'); ?>",
                    data: form.serialize(),
                    success: function (responData) {
                        if (responData) {
                            $('#modal').modal('toggle');
                            location.reload(1);
                        } else {
                            $("#errorp").html(' Please fill All Fields');
                        }
                    },
                });
            });



            function  nogoprojs(tndrid, sectid) {
                if (confirm("Are You Sure No Go This.") == true) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php base_url('togoproject/nogosubmit'); ?>",
                        data: {'actid': tndrid},
                        success: function (responData) {
                            console.log(responData);
                            $('#alert').css('display', 'block');
                            $('#msgs').html('Tender Important.');
                            setTimeout(function () {
                                location.reload(1);
                            }, 1000);

                        },
                    });
                }
            }
            var table;
            $(document).ready(function () {
                //datatables
                table = $('#table').DataTable({
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url('togoproject/newProjectAll') ?>",
                        "type": "POST",
                        "data": function (data) {
                            data.project_name = $('#company_name').val();
                            data.sectorinput = $('#sectorinput').val();
                            data.financial_year = $('#financial_year').val();
                        },
                    },
                    "dom": 'lBfrtip',
                    "buttons": [
                        {
                            extend: 'collection',
                            text: 'Export',
                            buttons: [
                                'copy',
                                'excel',
                                'csv',
                                'pdf',
                                'print'
                            ]

                        }
                    ],
                    "columnDefs": [{
                            "targets": [0], //first column / numbering column
                            "orderable": false, //set not orderable
                        },
                    ],
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                });
                // var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
                // $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
                $('#btn-filter').click(function () { //button filter event click
                    table.ajax.reload();  //just reload table
                });
                $('#btn-reset').click(function () { //button reset event click
                    $('#form-filter')[0].reset();
                    table.ajax.reload();  //just reload table
                });
            });
        </script>

        <!-- Code For Multi Select By Asheesh -->
        <link href="<?= FRONTASSETS; ?>fm.selectator.jquery.css" rel="stylesheet" type="text/css">
        <script src="<?= FRONTASSETS; ?>fm.selectator.jquery.js"></script> 
        <script>
            $('#multiple,#multiple1,#multiple2').selectator({
                showAllOptionsOnFocus: true,
                searchFields: 'value text subtitle right'
            });
            //Add Jv Record Section..
            function addjvset(tndrid) {
                $('#projid').val(tndrid);
                $.ajax({type: 'POST', url: "<?php base_url('company/showjvrecord'); ?>", data: {'jvprojid': tndrid}, success: function (responData) {
                        if (responData) {
                            $(".modal-body").css("display", "block");
                            $(".modal-body").html(responData);
                            $(".modal-title").hide();
                            $('.close').click(function () {
                                setTimeout(function () {
                                    location.reload(1);
                                }, 100);
                            });

                        }
                    }});
            }
        </script>
    </body>
</html>
