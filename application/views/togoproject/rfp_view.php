<?php
// echo "tests"; die;
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<?php
//Financial Year Condition Code By Asheesh.. 14-02-2019.
$year = '2016';
$YearPluse1 = (date('Y') + 1);
$finCdateStart = "01-04-" . date('Y');
// $finCdateEnd = "31-03-" . $YearPluse1;
$gEtCuurentDate = date("d-m-Y");
$gEtCuurentDateCONV = strtotime($gEtCuurentDate);
$finCdateStartCONV = strtotime($finCdateStart);
if ($finCdateStartCONV >= $gEtCuurentDateCONV) :
    $currentyear = date('Y') - 1;
else :
    $currentyear = date('Y');
endif;
?>
<link rel="stylesheet" href="<?= FRONTASSETS; ?>css/editable-select.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.18.10/slimselect.min.css" rel="stylesheet">
<style>
    #table_length,
    #table1_length,
    #table2_length,
    #table3_length {
        position: absolute;
        margin-left: 100px !important;
    }

    .table tbody tr td,
    .table tbody th td {
        vertical-align: middle;
        white-space: normal !important;
    }

    ul.nav.nav-tabs-new {
        float: right !important;
        margin: auto;
    }

    .dropdown-box {
        width: 100% !important;
    }

    .dropdown-box>.inputbox {
        width: 98% !important;

    }

    /* .select-editable {
     position:relative;
     background-color:white;
     border:solid grey 1px;
     width:120px;
     height:18px;
 } */

    .select-editable {
        position: relative;
        background-color: white;
        border: 1px solid #ddd;
        width: 100%;
        /* height: 18px; */
        border-radius: 5px;
        /* padding: 24px; */
        height: 40px;
        line-height: 40px;
        padding: 0px;
    }

    /* .select-editable select {
     position:absolute;
     top:0px;
     left:0px;
     font-size:14px;
     border:none;
     width:120px;
     margin:0;
 } */

    .select-editable select {
        position: absolute;
        top: 0px;
        left: 0;
        font-size: 14px;
        border: none;
        width: 98%;
        margin: 0;
        padding: 10px;
    }

    /* .select-editable input {
     position:absolute;
     top:0px;
     left:0px;
     width:100px;
     padding:1px;
     font-size:12px;
     border:none;
 } */

    .select-editable input {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 96%;
        padding: 10px;
        font-size: 12px;
        border: none;
    }

    .select-editable select:focus,
    .select-editable input:focus {
        outline: none;
    }
</style>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="accordion" id="accordion">
                                    <div>
                                        <form id="form-filter">
                                            <!--<div class="card-header" id="headingOne">-->
                                            <div class="row clearfix">
                                                <div class="col-lg-3 col-md-6">
                                                    <label>Sector :</label>
                                                    <div class="input-group mb-3">
                                                        <select name="sectorinput" id="sectorinput" class="form-control">
                                                            <option <?= ($secId == "") ? 'Selected' : ''; ?> value="">--All--</option>
                                                            <?php
                                                            if ($sectorArr) :
                                                                foreach ($sectorArr as $row) {
                                                            ?>
                                                                    <option <?= ($secId == $row->fld_id) ? 'Selected' : ''; ?> value="<?= $row->fld_id; ?>"><?= $row->sectName; ?></option>
                                                            <?php
                                                                }
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <label>Company Name :</label>
                                                    <div class="input-group mb-3">
                                                        <select id="company_name" class="form-control">
                                                            <option value="" selected="selected">--Type--</option>
                                                            <option value="E">EOI</option>
                                                            <option value="P">RFP</option>
                                                            <option value="FQ">FQ</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-6">
                                                    <label>Financial Year :</label>
                                                    <div class="input-group mb-3">
                                                        <select id="financial_year" class="form-control">
                                                            <!--<option value="" selected="selected">--Financial Year--</option>-->
                                                            <option <?php echo ($currentyear == '') ? 'selected' : 'selected'; ?> value="">--Select All--</option>

                                                            <?php
                                                            for ($i = $year; $i <= 2022; $i++) {
                                                            ?>
                                                                <option <?php echo ($currentyear == $i) ? '' : ''; ?> value=<?= $i; ?>><?= $i; ?>-<?= $i + 1; ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>




                                                <div class="col-lg-3 col-md-6">

                                                    <div class="mb-2 mt-4">
                                                        <button type="button" id="btn-filter" class="btn btn-info pull-right ml-2"> Filter </button>
                                                        <button type="button" id="btn-reset" class="btn btn-default pull-right"> Reset </button>
                                                    </div>
                                                </div>


                                            </div>
                                            <!--</div> -->

                                        </form>
                                    </div>
                                </div>
                                <hr />
                                <div class="table-responsive">

                                    <table id="table" class="table table-striped display" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th style="width:10%">Exp-Date</th>
                                                <th>TenderID</th>
                                                <th>Tender Details</th>
                                                <th>Location</th>
                                                <th>Client</th>
                                                <th style="width:20%">Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th style="width:10%">Exp-Date</th>
                                                <th>TenderID</th>
                                                <th>Tender Details</th>
                                                <th>Location</th>
                                                <th>Client</th>
                                                <th style="width:20%">Actions </th>
                                            </tr>
                                        </tfoot>


                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Assign Project Manager Modal Popup -->
        <div class="modal fade" id="assignmanager" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 id="tender_title" class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <form action="<?= base_url('togoproject/assign_proposal_manager'); ?>" method="post">
                            <div class="box-content">
                                <div class="controls">
                                    <?php // $userAll = GetAllUserRec();          
                                    ?>
                                    <label>Select Proposal Manager..</label>
                                    <select required="required" class="form-control" name="proposal_manager">
                                        <option value="">--select--</option>
                                        <?php foreach ($proposalManager as $rowrec) { ?>
                                            <option value="<?php echo $rowrec->user_id; ?>"><?php echo $rowrec->userfullname; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="box-content">
                                <!-- <input type="hidden" id="assigntendid" name="project_id" value=""> -->
                                <input type="submit" value="Assign" class="btn btn-success glyphicon glyphicon-user">
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-default" data-dismiss="modal">X</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Generate Project ID Modal -->
        <div class="modal fade" id="generateprojid" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 id="tender_title" class="modal-title">Generate Project No. </h4>
                    </div>
                    <div class="modal-body">
                        <form action="<?= base_url('togoproject/generate_project_no'); ?>" method="post">
                            <div class="box-content">
                                <div class="controls">
                                    <?php $userAll = GetAllPrefixRec(); ?>
                                    <label>Prefix</label>
                                    <select required="required" class="form-control" id="prefx_name" name="prefx_name">
                                        <option value="">--select--</option>
                                        <?php
                                        foreach ($userAll as $rowrec) {
                                        ?>
                                            <option value="<?= $rowrec->prefix_id; ?>"><?= $rowrec->prefix_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div id="projno" style="text-align: center;">&nbsp;</div>
                            <div class="box-content">
                                <?php foreach ($userAll as $rowrec) { ?>
                                    <input type="hidden" id="<?= $rowrec->prefix_id; ?>" name="<?= $rowrec->prefix_id; ?>" value="<?= $rowrec->last_generate_id; ?>">
                                <?php } ?>
                                <input type="hidden" id="assigntenderid" name="project_id" value="">
                                <input type="submit" value="Generate" class="btn btn-success glyphicon glyphicon-user">
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-default" data-dismiss="modal">X</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



        <!-- Modal For Edit Basic Details -->
        <div class="modal fade" id="myModalbasic" role="dialog">
            <div class="modal-dialog modal-lg">



                <div class="modal-content">
                    <!-- <div class="modal-header">

                        <h4 class="modal-title">Edit Basic Project Details  </h4> 
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div> -->







                    <div class="col-md-12 mb-2 mt-4">
                        <!-- <h5 class="modal-title">Project Detail</h5> -->

                        <ul id="progressbar">
                            <li class="active">Project Basic Detail</li>
                            <li>Bid Costing</li>
                            <li>Client Info</li>
                            <li>Consortium</li>
                        </ul>


                    </div>



                    <div id="msform">


                        <fieldset class="col-md-12 mb-4">



                            <div class="mb-2">
                                <h5 class="modal-title">Project Basic Detail</h5>
                                <hr style="border: 1px solid" />
                            </div>



                            <form name="projectBasicsForm" id="projectBasicsForm" method="post">
                                <input type="hidden" name="tender_id" id="tender_id" />



                                <div class="row" id="jvsection_new">
                                    <div class="form-group col-md-12">
                                        <label class="control-label" for="inputSuccess1"> Project Name </label>






                                        <textarea required class="form-control" id="projectName" name="projectName">


                                </textarea>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label> Submission Date</label>
                                        <input type="date" class="form-control" id="subm_date" name="subm_date" value="">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label> Bid Validity Date</label>
                                        <input type="date" class="form-control" id="expr_date" name="expr_date">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>Location</label>
                                        <input type="text" class="form-control" id="location" name="location" value="">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Client</label>

                                        <input type="hidden" id="mTextHid1" name="mTextHid1" value="" />

                                        <div class="select-editable">
                                            <input type="hidden" id="mVal1" name="client_name" value="" class="form-control" />
                                            <select id="list1" onchange="editableFunc(this.value)">
                                                
                                                <?php
                                                if (isset($clientName)) {
                                                    foreach ($clientName as $key => $list) {
                                                ?>
                                                        <option value="<?php echo $list->id; ?>"><?php echo $list->client_name; ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <input type="text" id="mText1" name="client_text" value="" class="form-control" />


                                        </div>


                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Funding</label>
                                        <input type="hidden" id="mTextHid2" name="mTextHid2" value="" />
                                        <div class="select-editable">
                                            <input type="hidden" id="mVal2" name="funding_org" value="" class="form-control" />
                                            <select id="list2" onchange="editableFuncSecond(this.value)">
                                                
                                                <?php
                                                if (isset($fundingOrg)) {
                                                    foreach ($fundingOrg as $key => $list) {
                                                ?>
                                                        <option value="<?php echo $list->id; ?>"><?php echo $list->funding_org; ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <input type="text" id="mText2" name="funding_org_text" value="" class="form-control" />

                                        </div>
                                    </div>
 


                                    <br>
                                    <div class="controls">
                                        <input type="hidden" name="projid" id="updprojid">
                                        <!-- <input type="button" id="updprojectbtn" class="btn" name="submit" value="Update / Save"> -->
                                    </div>



                                </div>


                                <a href="javascript:void(0)" class="btn btn-info btn-sm  action-button pull-right ml-2" onclick="saveProjectBasics()">Next</a>
                                <!-- <a href="javascript:void(0)" id="updprojectbtn"   class=" btn btn-info btn-sm action-button pull-right" onclick="stepProcessor(1, 'next')">Next</a> -->
                            </form>
                            <a class="next1" href="javascript:void(0)" style="display: none">Step 1</a>
                        </fieldset>








                        <fieldset class="col-md-12 mb-4">

                            <form name="bidCostingForm" id="bidCostingForm" method="post" enctype="">
                                <div class="row">

                                    <div class="col-md-12 mb-2">
                                        <h5 class="modal-title">Bid Costing</h5>
                                        <hr style="border: 1px solid" />
                                    </div>

                                    <div class="form-group required col-md-3">
                                        <label class="control-label" for="usr">Sector : </label>
                                        <select required id="sector" name="sectors_id" class="form-control">
                                            <option value=""> -- Select Sector -- </option>
                                            <?php
                                            if ($sectorArr) :
                                                foreach ($sectorArr as $rowr) {
                                            ?>
                                                    <option value="<?= $rowr->fld_id; ?>"> <?= $rowr->sectName; ?></option>
                                            <?php }
                                            endif; ?>
                                        </select>
                                    </div>
                                    <div class="form-group required col-md-3">
                                        <label class="control-label" for="usr">Services : </label>
                                        <select required id="services" name="services_id" class="form-control">
                                            <option value=""> -- Select Services -- </option>
                                            <?php
                                            if ($serviceArr) :
                                                foreach ($serviceArr as $rowr) {
                                            ?>
                                                    <option value="<?= $rowr->id; ?>"> <?= $rowr->service_name; ?></option>
                                            <?php }
                                            endif; ?>
                                        </select>
                                    </div>
                                    <div class="form-group required col-md-3">
                                        <label class="control-label" for="usr">Country : </label>
                                        <select required id="country" name="countries_id" onchange="setstatebycid()" class="form-control">
                                            <option value=""> -- Select Country -- </option>
                                            <?php
                                            if ($country_Arr) :
                                                foreach ($country_Arr as $rowr) {
                                            ?>
                                                    <option value="<?= $rowr->country_id; ?>"> <?= $rowr->country_name; ?></option>
                                            <?php }
                                            endif; ?>
                                        </select>
                                    </div>

                                    <div class="form-group required col-md-3">
                                        <label class="control-label" for="usr">State : </label>
                                        <select id="state" name="state_id" class="form-control">
                                            <?php
                                            if ($stateArr) :
                                                foreach ($stateArr as $rowrsec) {
                                            ?>
                                                    <option value="<?= $rowrsec->state_id; ?>"> <?= $rowrsec->state_name; ?></option>
                                            <?php }
                                            endif; ?>
                                        </select>
                                    </div>

                                    <div class="form-group required col-md-12">
                                        <label class="control-label" for="usr">Bid Security : <small style="color:green"> (Bid Security)</small> </label>
                                        <select required id="bid_security" name="bid_security" class="form-control">
                                            <option value="2"> No </option>
                                            <option value="1"> Yes </option>
                                        </select>
                                    </div>
                                    <div id="security_demnd" class="col-md-12" style="display:none">
                                        <div class="highlight">
                                            <div class="form-group required">
                                                <label class="control-label" for="usr">Bid Security Type : <small style="color:green"> (Bid Security)</small></label>
                                                <select id="bid_securitytype" name="bid_securitytype" class="form-control" onchange="showBankGuaranteeDetail(this.value, 'showSpfBg')">
                                                    <option value=""> -- Select Type-- </option>
                                                    <?php
                                                    if ($security_Type) :
                                                        foreach ($security_Type as $rowrsec) {
                                                    ?>
                                                            <option value="<?= $rowrsec->id; ?>"> <?= $rowrsec->security_type; ?></option>
                                                    <?php }
                                                    endif; ?>
                                                </select>
                                            </div>
                                            <div class="form-group required">
                                                <label> Amount : <small style="color:green"> (Bid Security)</small> </label>
                                                <input type="text" class="form-control" id="amount" name="amount" value="">
                                            </div>
                                            <div class="form-group required">
                                                <label> Validity : <small style="color:green"> (Bid Security)</small> </label>
                                                <input type="date" class="form-control" id="bs_valdity" name="bs_valdity" value="">
                                            </div>


                                            <div id="showSpfBg" class="row" style="display:none">
                                                <div class="form-group required col-md-12">
                                                    <label> Bank Name : </label>
                                                    <input type="text" class="form-control" id="sf_bankname" name="sf_bankname" value="">
                                                </div>


                                                <div class="form-group required col-md-12">
                                                    <label> BG Number : </label>
                                                    <input type="text" class="form-control" id="sf_bgnumber" name="sf_bgnumber" value="">
                                                </div>

                                                <div class="form-group required col-md-12">
                                                    <label> Date of Start : </label>
                                                    <input type="date" class="form-control" id="sf_dateofstart" name="sf_dateofstart" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--                                RFP Section Start-->
                                    <div class="form-group required col-md-12">
                                        <label>Cost Of RFP : <small style="color:#7386D5">(RFP)</small> </label>
                                        <select required id="rfp_cost" name="rfp_cost" class="form-control">
                                            <option value="2"> No </option>
                                            <option value="1"> Yes </option>
                                        </select>
                                    </div>
                                    <div id="security_demnd2" class="col-md-12" style="display:none">

                                        <div class="highlight">
                                            <div class="form-group required">
                                                <label>Amount : <small style="color:#7386D5">(RFP)</small></label>
                                                <input type="text" class="form-control" id="rfp_amount" name="rfp_amount" value="">
                                            </div>
                                            <div class="form-group required">
                                                <label> Type : <small style="color:#7386D5">(RFP)</small></label>
                                                <select id="rfp_type" name="rfp_type" class="form-control" onchange="showBankGuaranteeDetail(this.value, 'showRpfBg')">
                                                    <option value=""> -- Select Type-- </option>
                                                    <?php
                                                    if ($security_Type) :
                                                        foreach ($security_Type as $rowrsec) {
                                                            if ($rowrsec != '5') {
                                                    ?>
                                                                <option value="<?= $rowrsec->id; ?>"> <?= $rowrsec->security_type; ?></option>
                                                    <?php
                                                            }
                                                        }
                                                    endif;
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group required">
                                                <label> Validity : </label>
                                                <input type="date" class="form-control" id="rfp_valdity" name="rfp_valdity" value="">
                                            </div>


                                            <div id="showRpfBg" class="row" style="display:none">
                                                <div class="form-group required col-md-12">
                                                    <label> Bank Name : </label>
                                                    <input type="text" class="form-control" id="rpf_bankname" name="rpf_bankname" value="">
                                                </div>


                                                <div class="form-group required col-md-12">
                                                    <label> BG Number : </label>
                                                    <input type="text" class="form-control" id="rpf_bgnumber" name="rpf_bgnumber" value="">
                                                </div>

                                                <div class="form-group required col-md-12">
                                                    <label> Date of Start : </label>
                                                    <input type="date" class="form-control" id="rpf_dateofstart" name="rpf_dateofstart" value="">
                                                </div>
                                            </div>





                                        </div>
                                    </div>
                                    <!--   RFP Section Close -->

                                    <!-- Proccessing Section Start-->
                                    <div class="form-group required col-md-12">
                                        <label>Processing Fee : <small style="color:#1995dc">(Processing Fee)</small></label>
                                        <select required id="processing_fee" name="processing_fee" class="form-control">
                                            <option value="2"> No </option>
                                            <option value="1"> Yes </option>
                                        </select>
                                    </div>
                                    <div id="security_demnd3" class="col-md-12" style="display:none">

                                        <div class="highlight">
                                            <div class="form-group required">
                                                <label>Amount : <small style="color:#1995dc">(Processing Fee)</small> </label>
                                                <input type="number" step="any" min="1" class="form-control" id="pfee_amount" name="pfee_amount" value="">
                                            </div>
                                            <div class="form-group required">
                                                <label> Type : <small style="color:#1995dc">(Processing Fee)</small></label>
                                                <select id="processing_fee_type" name="processing_fee_type" class="form-control" onchange="showBankGuaranteeDetail(this.value, 'showPfBg')">
                                                    <option value=""> -- Select Type-- </option>
                                                    <?php
                                                    if ($security_Type) :
                                                        foreach ($security_Type as $rowrsec) {
                                                            if ($rowrsec != '5') {
                                                    ?>
                                                                <option value="<?= $rowrsec->id; ?>"> <?= $rowrsec->security_type; ?></option>
                                                    <?php
                                                            }
                                                        }
                                                    endif;
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group required">
                                                <label> Validity : </label>
                                                <input type="date" class="form-control" id="pf_valdity" name="pf_valdity" value="">
                                            </div>




                                            <div id="showPfBg" class="row" style="display:none">
                                                <div class="form-group required col-md-12">
                                                    <label> Bank Name : </label>
                                                    <input type="text" class="form-control" id="pf_bankname" name="pf_bankname" value="">
                                                </div>


                                                <div class="form-group required col-md-12">
                                                    <label> BG Number : </label>
                                                    <input type="text" class="form-control" id="pf_bgnumber" name="pf_bgnumber" value="">
                                                </div>

                                                <div class="form-group required col-md-12">
                                                    <label> Date of Start : </label>
                                                    <input type="date" class="form-control" id="pf_dateofstart" name="pf_dateofstart" value="">
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <!-- Close Section Start-->
                                    <!-- <div class="form-group required col-md-6">
                                    <label> Submission Date : </label>
                                    <input type="date" class="form-control" id="submission_date" name="submission_date" value="">
                                </div> -->
                                    <!-- <div class="form-group required col-md-12">
                                    <label> Bid Validity Date : </label>
                                    <input type="date" class="form-control" id="tender_openingdate" name="tender_openingdate" value="">
                                </div> -->
                                    <div class="form-group required col-md-12">
                                        <input type="hidden" name="serviceprojid" id="serviceprojid" class="btn green">

                                    </div>
                                </div>
                                <a href="javascript:void(0)" class="btn btn-warning btn-sm action-button-previous pull-left" onclick="stepProcessor(2, 'prev')">Previous</a>


                                <a href="javascript:void(0)" class="btn btn-info btn-sm  action-button pull-right ml-2" onclick="saveBidCosting()">Next</a>

                                <!-- <a href="javascript:void(0)" class="btn btn-info btn-sm pull-right"  onclick="stepProcessor(2, 'next')">Next</a> -->
                            </form>
                            <a class="previous2" href="javascript:void(0)" style="display: none">Step 2 previous</a>
                            <a class="next2" href="javascript:void(0)" style="display: none">Step 2 Next</a>
                        </fieldset>



                        <fieldset class="col-md-12 mb-4">

                            <div class="mb-2">
                                <h5 class="modal-title">Client Info</h5>
                                <hr style="border: 1px solid" />
                            </div>
                            <div class="">
                                <form name="clientInfoForm" id="clientInfoForm" method="post" enctype="">
                                    <div class="row">
                                        <div class="form-group required col-md-6">
                                            <label> Client Name</label>



                                            <div class="select-editable">

                                                <select id="list3" onchange="editableFuncClientContact(this.value)">
                                                     
                                                    <?php
                                                    if (isset($clientName)) {
                                                        foreach ($clientName as $key => $list) {
                                                    ?>
                                                            <option value="<?php echo $list->id; ?>"><?php echo $list->client_name; ?></option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <input type="hidden" id="mVal3" name="client_contact_name" value="" class="form-control" />

                                                <input type="text" id="mText3" name="client_contact_name_text" value="" class="form-control" />


                                            </div>
                                        </div>



                                        <div class="form-group required col-md-6">
                                            <label> Client Contact</label>
                                            <input type="text" class="form-control" id="client_contact" name="client_contact" value="">
                                        </div>
                                        <div class="form-group required col-md-12">
                                            <label> Client Address</label>
                                            <textarea class="form-control" id="client_address" name="client_address"></textarea>&nbsp;<a onclick="addClientContact()" class="btn btn-outline-dark pull-right mt-4">+</a>
                                        </div>

                                        <div class="form-group required col-md-6">
                                            <input type="hidden" name="project_id" id="project_id" class="btn green">
                                            <input type="hidden" name="sectionCounter" id="sectionCounter" value="0" class="btn green">

                                        </div>
                                    </div>

                                    <div id="contactSection">



                                    </div>
                                    <div class="row">
                                        <div class="form-group required col-md-12">
                                            <table border="1" id="clientadd" style="width:100%"></table>
                                        </div>
                                    </div>

                            </div>

                            <a href="javascript:void(0)" class="btn btn-warning btn-sm action-button-previous pull-left" onclick="stepProcessor(3, 'prev')">Previous</a>

                            <a href="javascript:void(0)" class="btn btn-info btn-sm  action-button pull-right ml-2" onclick="saveClientInfo()">Next</a>
                            <!-- <a href="javascript:void(0)"  class="btn btn-info btn-sm  action-button pull-right"  onclick="stepProcessor(3, 'next')">Next</a> -->
                            </form>

                            <a class="previous3" href="javascript:void(0)" style="display: none">Step 3 previous</a>
                            <a class="next3" href="javascript:void(0)" style="display: none">Step 3 Next</a>
                        </fieldset>


                        <fieldset class="col-md-12 mb-4">
                            <div class="mb-2">
                                <h4 class="modal-title"> Consortium </h4>

                                <hr style="border: 1px solid" />
                            </div>
                            <div class="mt-2">

                                <div class="highlight">

                                    <label class="fancy-checkbox">
                                        <input type="checkbox" checked id="soleornot">
                                        <span>Sole</span>
                                    </label>

                                </div>

                            </div>
                            <div class="mt-2">
                                <form name="consortiumForm" id="consortiumForm" method="post" enctype="">
                                    <div class="row" id="jvsection" style="display: none">
                                        <div class="form-group required col-md-12">
                                            <label>Lead Company </label>
                                            <select id="multiple" name="leadcompany[]" style="width:100%" multiple>
                                                <?php foreach ($compnameArr as $rowrec) { ?>
                                                    <option value="<?= $rowrec->fld_id; ?>">
                                                        <?= ucfirst($rowrec->company_name); ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group required col-md-12">
                                            <label> JV (<small> joint venture </small>)</label>
                                            <select id="multiple1" name="joint_venture[]" style="width:100%" multiple>
                                                <?php foreach ($compnameArr as $rowrec) { ?>
                                                    <option value="<?= $rowrec->fld_id; ?>">
                                                        <?= ucfirst($rowrec->company_name); ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group required col-md-12">
                                            <label> Associate company..</label>
                                            <select id="multiple2" name="assoc[]" style="width:100%" multiple>
                                                <?php foreach ($compnameArr as $rowrec) { ?>
                                                    <option value="<?= $rowrec->fld_id; ?>">
                                                        <?= ucfirst($rowrec->company_name); ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>


                                        <div class="form-group required col-md-6">
                                            <input type="hidden" name="projid" id="projid">

                                        </div>
                                    </div>

                                </form>
                            </div>





                            <a href="javascript:void(0)" class="btn btn-warning btn-sm action-button-previous pull-left" onclick="stepProcessor(4, 'prev')">Previous</a>

                            <a href="javascript:void(0)" class="btn btn-info btn-sm  action-button pull-right ml-2" onclick="saveConsortiumInfo()">Save</a>

                            </form>
                            <!-- <input type="button" name="previous" class="previous action-button-previous" value="Previous">
                    <input type="submit" name="submit" class="submit action-button" value="Submit"> -->
                            <a class="previous4" href="javascript:void(0)" style="display: none">Step 4 previous</a>
                            <a class="next4" href="javascript:void(0)" style="display: none">Step 4 Next</a>
                        </fieldset>




                        <!-- <div class="modal-footer">
                    <input type="submit" class="btn btn-info btn-sm pull-right" value="Save">

                </div> -->
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="clientmodel" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Client Info</h4>
                    </div>
                    <div class="modal-body">
                        <form name="frmm" id="frmm" method="post" action="<?= base_url('togoproject/clientinfo'); ?>" enctype="">
                            <div class="row">
                                <div class="form-group required col-md-6">
                                    <label> Client Name</label>
                                    <div class="form-group col-md-6">

                                    </div>
                                </div>
                                <div class="form-group required col-md-6">
                                    <label> Client Contact</label>
                                    <input type="number" class="form-control" id="client_contact" name="client_contact" value="">
                                </div>
                                <div class="form-group required col-md-12">
                                    <label> Client Position</label>
                                    <input type="text" class="form-control" id="client_position" name="client_position" value="">
                                </div>
                                <div class="form-group required col-md-6">
                                    <!-- <input type="hidden" name="project_id" id="project_iddata" class="btn green"> -->
                                    <input type="submit" class="btn green" value="Submit">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <table border="1" id="clientadd" style="width:100%"></table>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <?php $this->load->view('admin/includes/footer'); ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.18.10/slimselect.min.js"></script>
                                                    



    </div>

    <style>
        #table_length {
            margin-left: 20px;
        }

        #table_filter {
            margin-right: 2%;
        }

        #chatbox {
            padding: 15px;
            overflow: scroll;
            height: 300px;
        }
    </style>
    <script type="text/javascript">
        var current_fs, next_fs, previous_fs, current_fs_pro, next_fs_pro; //fieldsets
        var left, opacity, scale; //fieldset properties which we will animate
        var animating; //flag to prevent quick multi-click glitches
        var stepCount = 0;
        var i, countDiv;

        function stepProcessor(stepCount, nextPrev) {

            savedClientContact($("#tender_id").val());

            if (nextPrev == "next") {
                if (animating) return false;
                animating = true;
                current_fs = $(".next" + stepCount).parent();
                next_fs = $(".next" + stepCount).parent().next();
                console.log(current_fs);
                console.log(next_fs);
                //activate next step on progressbar using the index of next_fs
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                //show the next fieldset
                next_fs.show();
                //hide the current fieldset with style
                current_fs.animate({
                    opacity: 0
                }, {
                    step: function(now, mx) {
                        //as the opacity of current_fs reduces to 0 - stored in "now"
                        //1. scale current_fs down to 80%
                        scale = 1 - (1 - now) * 0.2;
                        //2. bring next_fs from the right(50%)
                        left = (now * 50) + "%";
                        //3. increase opacity of next_fs to 1 as it moves in
                        opacity = 1 - now;
                        current_fs.css({
                            'position': 'absolute',
                            'transform': 'scale(' + scale + ')',

                        });
                        next_fs.css({
                            'left': left,
                            'opacity': opacity
                        });
                    },
                    duration: 800,
                    complete: function() {
                        current_fs.hide();
                        animating = false;
                    },
                    //this comes from the custom easing plugin
                    easing: 'easeInOutBack'
                });
            }
            if (nextPrev == "prev") {

                savedClientContact($("#tender_id").val());

                if (animating) return false;
                animating = true;
                current_fs = $(".previous" + stepCount).parent();
                previous_fs = $(".previous" + stepCount).parent().prev();
                //de-activate current step on progressbar
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
                //show the previous fieldset
                previous_fs.show();
                //hide the current fieldset with style
                current_fs.animate({
                    opacity: 0
                }, {
                    step: function(now, mx) {
                        //as the opacity of current_fs reduces to 0 - stored in "now"
                        //1. scale previous_fs from 80% to 100%
                        scale = 0.8 + (1 - now) * 0.2;
                        //2. take current_fs to the right(50%) - from 0%
                        left = ((1 - now) * 50) + "%";
                        //3. increase opacity of previous_fs to 1 as it moves in
                        opacity = 1 - now;
                        current_fs.css({
                            'left': left
                        });
                        previous_fs.css({
                            'transform': 'scale(' + scale + ')',
                            'opacity': opacity
                        });
                    },
                    duration: 800,
                    complete: function() {
                        current_fs.hide();
                        previous_fs.css('position', 'relative', 'important');
                        animating = false;
                    },
                    //this comes from the custom easing plugin
                    easing: 'easeInOutBack'
                });
            }
        }


        function addClientContact() {

            // countDiv = $('#contactSection').children('div.highlight').length;
            // console.log(countDiv);
            //  i = (countDiv > 0) ? countDiv : $("#sectionCounter").val();

            countDiv = $('#contactSection').children('div.highlight').length;

            if (countDiv > 0) {
                var splitStr = $('#contactSection').children().last().attr('id');
                var resContact = splitStr.split("removeContactSection_");
                console.log(resContact[1]);
            }

            i = (countDiv > 0) ? resContact[1] : $("#sectionCounter").val();
            i++;
            $("#sectionCounter").val(i);
            $("#contactSection").append('<div class="row highlight mt-2" style="margin: 0px" id="removeContactSection_' + i + '"><div class="form-group required col-md-12"> <a onclick="removeClientContact(' + i + ')" class="btn btn-outline-danger pull-right mt-4">-</a></div><div class="form-group required col-md-3"><label> Name</label><input type="text" class="form-control" id="clientName' + i + '" name="clientName' + i + '" value=""></div><div class="form-group required col-md-3"><label> Designation</label><input type="text" class="form-control" id="clientDesignation' + i + '" name="clientDesignation' + i + '" value=""></div><div class="form-group required col-md-3"><label> Email</label><input type="text" class="form-control" id="clientEmail' + i + '" name="clientEmail' + i + '" value=""></div><div class="form-group required col-md-3"><label> Phone No.</label><input type="text" class="form-control" id="clientPhone' + i + '" name="clientPhone' + i + '" value=""></div><div class="form-group required col-md-12"><label> Address</label><textarea required cols="10" class="form-control" id="clientAddress' + i + '" name="clientAddress' + i + '" rows="3"></textarea></div></div>');
        }




        function removeClientContact(id) {

            console.log(id);

            $("#removeContactSection_" + id).remove();
            $("#sectionCounter").val(id--);
        }


        function savedClientContact(projid) {



            $.ajax({
                url: '<?= base_url('togoproject/savedClientContact') ?>',
                method: 'post',
                data: {
                    project_id: projid
                },
                dataType: 'json',
                success: function(response) {
                    $("#contactSection").html("");
                    console.log(response);
                    var i = 1;
                    $.each(response, function(index, data) {

                        $("#sectionCounter").val(i);
                        $("#contactSection").append('<div class="row highlight mt-2" style="margin: 0px" id="removeContactSection_' + i + '"><div class="form-group required col-md-12"> <a onclick="removeClientContact(' + i + ')" class="btn btn-outline-danger pull-right mt-4">-</a></div><div class="form-group required col-md-3"><label> Name</label><input type="text" class="form-control" id="clientName' + i + '" name="clientName' + i + '" value="' + data.cdetail_name + '"></div><div class="form-group required col-md-3"><label> Designation</label><input type="text" class="form-control" id="clientDesignation' + i + '" name="clientDesignation' + i + '" value="' + data.cdetail_designation + '"></div><div class="form-group required col-md-3"><label> Email</label><input type="text" class="form-control" id="clientEmail' + i + '" name="clientEmail' + i + '" value="' + data.cdetail_email + '"></div><div class="form-group required col-md-3"><label> Phone No.</label><input type="text" class="form-control" id="clientPhone' + i + '" name="clientPhone' + i + '" value="' + data.cdetail_phone + '"></div><div class="form-group required col-md-12"><label> Address</label><textarea required cols="10" class="form-control" id="clientAddress' + i + '" name="clientAddress' + i + '" rows="3">' + data.cdetail_address + '</textarea></div></div>');
                        i++;
                    });

                    $("#sectionCounter").val(i);


                }
            });

        }





        $('#bid_security').on('change', function() {
            var id = $(this).val();
            if (id == 1) {
                $('#security_demnd').css('display', 'block');
            } else {
                $('#security_demnd').css('display', 'none');
            }
        });
        //sTEP 2 hIDE sHOW,,.. 
        $('#rfp_cost').on('change', function() {
            var id = $(this).val();
            if (id == 1) {
                $('#security_demnd2').css('display', 'block');
            } else {
                $('#security_demnd2').css('display', 'none');
            }
        });
        //sTEP 3 hIDE sHOW..
        $('#processing_fee').on('change', function() {
            var id = $(this).val();
            if (id == 1) {
                $('#security_demnd3').css('display', 'block');
            } else {
                $('#security_demnd3').css('display', 'none');
            }
        });

        function setstatebycid() {
            var countryid = $("#country").val();
            console.log(countryid);
            $('#state').html('');
            $.ajax({
                url: '<?= base_url('company/ajax_state_getbycid') ?>',
                method: 'post',
                data: {
                    country: countryid
                },
                dataType: 'json',
                success: function(response) {
                    $.each(response, function(index, data) {
                        $('#state').append('<option value="' + data['state_id'] + '">' + data['state_name'] + '</option>');
                    });
                }
            });
        }



        function showBankGuaranteeDetail(val, divId) {

            if (val == 5) {
                $("#" + divId).show();
            } else {
                $("#" + divId).hide();
            }


        }

        $(document).ready(function() {


            $("#soleornot").click(function() {
                if ($(this).prop('checked') == true) {
                    $("#jvsection").hide();
                } else {
                    $("#jvsection").show();
                }
            });
        });

        $(document).ready(function() {
            $("li#ongoing_search").addClass('active');
            $("li#ongoing_rfp").addClass('active');
        });

        $(document).ready(function() {
            $("#checkAll").click(function() {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });
        });

        function assignpm(projid) {
            $("#assigntenderid").val(projid);
            $("#assigntendid").val(projid);
        }

        function gocomment(tndrid) {
            //Put ProjId And Secot Id..
            $("#projId").val(tndrid);
            $.ajax({
                type: 'POST',
                dataType: "text",
                data: {
                    'tndrid': tndrid
                },
                url: "<?= base_url('togoproject/commentset'); ?>",
                success: function(responData) {
                    $("#chatbox").html(responData);
                },
            });
        }
        //Comment Submitted...
        $(document).ready(function() {
            $('#commentmsg').keypress(function(e) {
                var key = e.which;
                if (key == 13) {
                    var comnt = $("#commentmsg").val();
                    var projId = $("#projId").val();
                    //Comment Submit..
                    $.ajax({
                        type: 'POST',
                        dataType: "text",
                        url: "<?= base_url('togoproject/commentsubmit'); ?>",
                        data: {
                            'projId': projId,
                            'comnt': comnt
                        },
                        success: function(responData) {
                            $("#commentmsg").val('');
                            return gocomment(projId);
                            //location.reload(1);
                        },
                    });
                }
            });
        });

        //Project No Go..
        function nosubmitproj(tndrid, sectid) {
            if (confirm("Are You Sure No Submitted This.") == true) {
                $.ajax({
                    type: 'POST',
                    url: "<?= base_url('togoproject/projnosubmit'); ?>",
                    data: {
                        'actid': tndrid
                    },
                    success: function(responData) {
                        console.log(responData);
                        $('#alert').css('display', 'block');
                        $('#msgs').html('Tender Important.');
                        setTimeout(function() {
                            location.reload(1);
                        }, 1000);
                    },
                });
            }
        }

        function bidproject(actprojid, sectid) {
            if (confirm("Are You Sure Submitted This Project ? ") == true) {
                $.ajax({
                    type: 'POST',
                    url: "<?= base_url('togoproject/bidprojectbyuser'); ?>",
                    data: {
                        'projid': actprojid
                    },
                    success: function(responData) {
                        console.log(responData);
                        $('#alert').css('display', 'block');
                        $('#msgs').html('Tender Important.');
                        setTimeout(function() {
                            location.reload(1);
                        }, 1000);

                    },
                });
            }
        }

        //Edit Project Code By Asheesh..
        function editproject(projid) {
            if (projid) {

                $.ajax({
                    type: 'POST',
                    url: "<?= base_url('togoproject/ajax_projectdetailbyid'); ?>",
                    data: {
                        'projid': projid
                    },
                    success: function(responData) {
                        var parsed = JSON.parse(responData);
                        var arr = [];
                        for (var x in parsed) {
                            arr.push(parsed[x]);
                        }
                        /*  $("#expr_date").val(arr[15]);
                         $("#location").val(arr[6]);
                         $("#organization").val(arr[7]);
                         $("#updprojid").val(arr[0]);
                         $("#eprojectdt").val(arr[3]); */
                        // //$("#expr_date").val(arr[14]);
                        // $("#location").val(arr[5]);
                        // $("#organization").val(arr[6]);
                        // $("#updprojid").val(arr[0]);
                        // $("#eprojectdt").val(arr[3]);
                    },
                });





            }
        }
        function  resetModal(){
            $('#myModalbasic').on('hidden.bs.modal', function (e) {
            $(this)
            .find("input,textarea,select")
            .val('')
            .end()
            .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();
            });

            $("#contactSection").html("");
        }

            
        //Close Edit Project Section By Asheesh..
        function editprojectdetail(projid, exp_date) {


             resetModal();



            if (projid) {

                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url('company/fetchProjectData'); ?>",
                    data: {
                        'projid': projid
                    },
                    dataType: "json",
                    success: function(responData) {
                        if (responData) {


                            console.log(responData[0]);

                            $('#project_id').val(responData[0].fld_id);
                            $('#projectName').val(responData[0].TenderDetails);
                            $('#location').val(responData[0].Location);
                            $('#expr_date').val(responData[0].bid_validity_date);
                            $("#client_name").editableSelect();
                            $("#client_name").val(responData[0].client_id);
                            $("#funding_org").editableSelect();
                            $("#funding_org").val(responData[0].bd_funding_master_id);
                            $('#client_contact_name').editableSelect();
                            $("#client_contact_name").val(responData[0].client_id);
                            $("#client_contact").val(responData[0].client_contact);
                            $("#client_address").val(responData[0].client_address);
                            $("#sf_bankname").val(responData[0].sf_bankname);
                            $("#sf_bgnumber").val(responData[0].sf_bgnumber);
                            $("#sf_dateofstart").val(responData[0].sf_dateofstart);
                            $("#rfp_valdity").val(responData[0].rfp_valdity);
                            $("#rpf_bankname").val(responData[0].rpf_bankname);
                            $("#rpf_bgnumber").val(responData[0].rpf_bgnumber);
                            $("#rpf_dateofstart").val(responData[0].rpf_dateofstart);
                            $("#pf_valdity").val(responData[0].pf_valdity);
                            $("#pf_bankname").val(responData[0].pf_bankname);
                            $("#pf_bgnumber").val(responData[0].pf_bgnumber);
                            $("#pf_dateofstart").val(responData[0].pf_dateofstart);

                            $("#sector").val(responData[0].sector_id);
                            $("#services").val(responData[0].service_id);
                            $("#country").val(responData[0].country_id);
                            setstatebycid(function() {
                                $("#state").val(responData[0].state_id);
                            });




                            $("#state_id").val(responData[0].state_id);

                            $("#bid_security").val(responData[0].bid_security);

                            if (responData[0].bid_security == 1) {
                                $('#security_demnd').css('display', 'block');

                            } else {
                                $('#security_demnd').css('display', 'none');
                            }


                            $("#bid_securitytype").val(responData[0].bid_securitytype);

                            if (responData[0].bid_securitytype == 5) {
                                $('#showSpfBg').css('display', 'block');
                            } else {
                                $('#showSpfBg').css('display', 'none');
                            }



                            $('#amount').val(responData[0].amount);
                            $('#bs_valdity').val(responData[0].bs_valdity);


                            $("#rfp_cost").val(responData[0].rfp_cost);
                            if (responData[0].rfp_cost == 1) {
                                $('#security_demnd2').css('display', 'block');
                            } else {
                                $('#security_demnd2').css('display', 'none');
                            }

                            $("#rfp_amount").val(responData[0].rfp_amount);
                            $('#rfp_type').val(responData[0].rfp_type);
                            if (responData[0].rfp_type == 5) {
                                $('#showRpfBg').css('display', 'block');
                            } else {
                                $('#showRpfBg').css('display', 'none');
                            }



                            $('#bs_valdity').val(responData[0].bs_valdity);


                            $("#processing_fee").val(responData[0].processing_fee);
                            if (responData[0].processing_fee == 1) {
                                $('#security_demnd3').css('display', 'block');
                            } else {
                                $('#security_demnd3').css('display', 'none');
                            }

                            $('#pfee_amount').val(responData[0].pfee_amount);
                            $('#processing_fee_type').val(responData[0].processing_fee_type);
                            if (responData[0].processing_fee_type == 5) {
                                $('#showPfBg').css('display', 'block');
                            } else {
                                $('#showPfBg').css('display', 'none');
                            }

                            $('#tender_openingdate').val(responData[0].tender_openingdate);

                            editableFunc(responData[0].client_id);
                            editableFuncClientContact(responData[0].client_id);
                            editableFuncSecond(responData[0].bd_funding_master_id);

                            //$("select#client_contact_name").val($("#client_name").val());
                            // jc.lead_companes, jc.joint_ventures, jc.associate_company
                            let arrLead, arrJoint, arrAssoc;
                            var dataLead = responData[0].lead_companes;
                            //console.log(dataLead.indexOf(","));
                            if (dataLead) {

                                if (dataLead.indexOf(",") > 0) {
                                    arrLead = dataLead.split(',');
                                } else {
                                    arrLead = [responData[0].lead_companes];
                                }

                            }
                            var dataJoint = responData[0].joint_ventures;
                            if (dataJoint) {
                                if (dataJoint.indexOf(",") > 0) {
                                    arrJoint = dataJoint.split(',');
                                } else {
                                    arrJoint = [responData[0].joint_ventures];

                                }
                            }
                            var dataAssoc = responData[0].associate_company;
                            if (dataAssoc) {
                                if (dataAssoc !== null && dataAssoc.indexOf(",") > 0) {
                                    arrAssoc = dataAssoc.split(',');
                                } else {
                                    arrAssoc = [responData[0].associate_company];
                                }
                            }






                            var arrayCompany = <?php echo json_encode($compnameArr); ?>;
                            console.log(arrayCompany);

                            var x = new SlimSelect({
                                select: '#multiple',
                                hideSelectedOption: true,
                                onChange: (info) => {


                                    $.each(info, function(i) {






                                        $("div.ss-option").each(function() {
                                            if ($(this).text().trim() == info[i].text) {



                                                $("#multiple1 option[value='" + info[i].value + "']").remove();
                                                $("#multiple2 option[value='" + info[i].value + "']").remove();
                                            }
                                        });

                                    });


                                }
                            });



                            var y = new SlimSelect({
                                select: '#multiple1',
                                hideSelectedOption: true,
                                onChange: (info) => {



                                    $.each(info, function(i) {

                                        // $('.ss-list div[data-id="'+info[i].value+'"]').addClass('ss-disabled');
                                        console.log(info[i].text);
                                        $("div.ss-option").each(function() {
                                            if ($(this).text().trim() == info[i].text) {
                                                $(this).addClass("ss-disabled");

                                                $("#multiple2 option[value='" + info[i].value + "']").remove();
                                            }
                                        });

                                    });


                                }
                            });



                            var z = new SlimSelect({
                                select: '#multiple2',
                                hideSelectedOption: true
                            });


                            // x.setData(
                            // {text: arrLead[0]});

                            if (Array.isArray(arrLead) && arrLead.length) {
                                x.set(arrLead)
                            }

                            if (Array.isArray(arrJoint) && arrJoint.length) {
                                y.set(arrJoint)
                            }

                            if (Array.isArray(arrAssoc) && arrAssoc.length) {
                                z.set(arrAssoc)
                            }







                        }
                    }
                });




                // var optionTextFd1 = $("#list2 option[value='"++"']").text();
                // $("#mVal1").val(responData[0].client_id);
                // $("#mText1").val(optionTextFd1);


                // var optionTextFd2 = $("#list2 option[value='"++"']").text();
                // $("#mVal2").val(responData[0].bd_funding_master_id);
                // $("#mText2").val(optionTextFd2);


                savedClientContact(projid);



                $('#serviceprojid').val(projid);
                $('#submission_date').val(exp_date);
                $('#tender_id').val(projid);


                // countDiv = $('#contactSection').children('div.highlight').length;
                // i = (countDiv > 0) ? countDiv : $("#sectionCounter").val();


                countDiv = $('#contactSection').children('div.highlight').length;

                if (countDiv > 0) {
                    var splitStr = $('#contactSection').children().last().attr('id');
                    var resContact = splitStr.split("removeContactSection_");
                    console.log(resContact[1]);
                }

                i = (countDiv > 0) ? resContact[1] : $("#sectionCounter").val();
            }
        }




        function editableFunc(mVal1) {

            var optionText = $("#list1 option[value='" + mVal1 + "']").text();
            $("#mVal1").val(mVal1);
            $("#mText1").val(optionText);
            console.log("Getting Val " + mVal1);
            $("#mTextHid1").val(optionText);

        }

        function editableFuncClientContact(mVal1) {

            var optionText = $("#list3 option[value='" + mVal1 + "']").text();
            $("#mVal3").val(mVal1);
            $("#mText3").val(optionText);


        }

        function editableFuncSecond(mVal1) {

            var optionText = $("#list2 option[value='" + mVal1 + "']").text();

            $("#mVal2").val(mVal1);
            $("#mText2").val(optionText);
            console.log("Getting Val " + mVal1);
            $("#mTextHid2").val(optionText);


        }

        function editclientmodel(projid) {
            if (projid) {
                $('#project_iddata').val(projid);
                $('#clientadd').html('');
                $.ajax({
                    type: 'POST',
                    url: "<?= base_url('togoproject/ajax_clientinfo'); ?>",
                    data: {
                        'project_id': projid
                    },
                    success: function(responData) {
                        var parsed = jQuery.parseJSON(responData);
                        if (parsed) {
                            $('#clientadd').append('<tr><th>Client Name</th><th>Client Contact</th><th>Client Position</th><th>Action</th></tr>');
                            $.each(parsed, function(index, val) {
                                //console.log(val.client_name);
                                $('#clientadd').append('<tr><td>' + val.client_name + '</td><td>' + val.client_contact + '</td><td>' + val.client_position + '</td><td><a href="<?= base_url('togoproject/ajax_clientinfodel'); ?>/' + val.id + '" class="clientinfodel">Delete</a></td></tr>');
                            });
                        }

                    },
                });
            }
        }

        //Update Afdter Click On Button Code By Asheesh..
        // $("#updprojectbtn").click(function() {
        //     var form = $("#edtform");
        //     $.ajax({
        //         type: 'POST',
        //         url: "<?= base_url('togoproject/editupdateproject'); ?>",
        //         data: form.serialize(),
        //         success: function(responData) {
        //             if (responData) {
        //                 $('#modal').modal('toggle');
        //                 location.reload(1);
        //             } else {
        //                 $("#errorp").html(' Please fill All Fields');
        //             }
        //         },
        //     });
        // });


        function nogoprojs(tndrid, sectid) {
            if (confirm("Are You Sure No Go This.") == true) {
                $.ajax({
                    type: 'POST',
                    url: "<?= base_url('togoproject/nogosubmit'); ?>",
                    data: {
                        'actid': tndrid
                    },
                    success: function(responData) {
                        console.log(responData);
                        $('#alert').css('display', 'block');
                        $('#msgs').html('Tender Important.');
                        setTimeout(function() {
                            location.reload(1);
                        }, 1000);

                    },
                });
            }
        }
        var table;
        $(document).ready(function() {
            //datatables
            table = $('#table').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url('togoproject/rfpProjectAll') ?>",
                    "type": "POST",
                    "data": function(data) {
                        data.sectorinput = $('#sectorinput').val();
                        data.financial_year = $('#financial_year').val();
                    },
                },
                "dom": 'lBfrtip',
                "buttons": [{
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        'copy',
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ]

                }],
                "columnDefs": [{
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                }, ],
                "aLengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
            });
            // var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
            // $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
            $('#btn-filter').click(function() { //button filter event click
                table.ajax.reload(); //just reload table
            });
            $('#btn-reset').click(function() { //button reset event click
                $('#form-filter')[0].reset();
                table.ajax.reload(); //just reload table
            });
        });
    </script>

    <script>
        // $('#multiple,#multiple1,#multiple2').selectator({
        //     showAllOptionsOnFocus: true,
        //     searchFields: 'value text subtitle right'
        // });
        //Add Jv Record Section..
        function addjvset(tndrid) {
            $('#projid').val(tndrid);
            $.ajax({
                type: 'POST',
                url: "<?php base_url('company/showjvrecord'); ?>",
                data: {
                    'jvprojid': tndrid
                },
                success: function(responData) {
                    if (responData) {
                        $(".modal-body").css("display", "block");
                        $(".modal-body").html(responData);
                        $(".modal-title").hide();
                        $('.close').click(function() {
                            setTimeout(function() {
                                location.reload(1);
                            }, 100);
                        });

                    }
                }
            });
        }

        // #### JS to Update Steps
        function saveProjectBasics() {
            var data = $("#projectBasicsForm").serialize();
            var clientText = $("#mTextHid1").val();
            var fundingText = $("#mTextHid2").val();

            var client_text = $("#mText1").val();

            console.log("GD >>" + clientText + " >" + fundingText);



            var clientID = $("#mVal1").val();




            $.ajax({
                type: 'POST',
                url: "<?= base_url('togoproject/editupdateproject'); ?>",
                data: data + "&clientText=" + clientText + "&fundingText=" + fundingText,
                dataType: "json",
                success: function(responData) {
                    $('#alert').css('display', 'block');
                    toastr.success(responData.msg, 'Message', {
                        timeOut: 5000
                    });



                    if ($("#list3 option[value='" + responData.id + "']").length === 0) {

                      

                        $('#list3').append($('<option>', {
                            value: responData.id,
                            text: client_text
                        }), function() {



                        });




                    }

                    editableFuncClientContact(responData.id);
                    console.log("success" + responData.id);
                    stepProcessor(1, 'next');


                },
            });
        }

        function saveBidCosting() {
            var data = $("#bidCostingForm").serialize();
            $.ajax({
                type: 'POST',
                url: "<?= base_url('togoproject/insertproject'); ?>",
                data: data,
                dataType: "json",
                success: function(responData) {
                    $('#alert').css('display', 'block');
                    toastr.success(responData.msg, 'Message', {
                        timeOut: 5000
                    });
                    stepProcessor(2, 'next');
                },
            });
        }



        function saveClientInfo() {
            var data = $("#clientInfoForm").serialize();


            countDiv = $('#contactSection').children('div.highlight').length;

            if (countDiv > 0) {
                var splitStr = $('#contactSection').children().last().attr('id');
                var resContact = splitStr.split("removeContactSection_");
                console.log(resContact[1]);
            }

            var counterTotal = (countDiv > 0) ? resContact[1] : $("#sectionCounter").val();

            $.ajax({
                type: 'POST',
                url: "<?= base_url('togoproject/clientinfo'); ?>",
                data: data + "&counterTotal=" + counterTotal,
                dataType: "json",
                success: function(responData) {
                    $('#alert').css('display', 'block');
                    toastr.success(responData.msg, 'Message', {
                        timeOut: 5000
                    });
                    stepProcessor(3, 'next')
                },
            });
        }

        function saveConsortiumInfo() {
            var data = $("#consortiumForm").serialize();
            $.ajax({
                type: 'POST',
                url: "<?= base_url('company/savejvdata'); ?>",
                data: data + "&tender_id=" + $("#tender_id").val(),
                dataType: "json",
                success: function(responData) {
                    $('#alert').css('display', 'block');
                    toastr.success(responData.msg, 'Message', {
                        timeOut: 5000
                    });
                },
            });
        }


        function nogoprojs(tndrid, sectid) {
            if (confirm("Are You Sure No Go This.") == true) {
                $.ajax({
                    type: 'POST',
                    url: "<?= base_url('togoproject/nogosubmit'); ?>",
                    data: {
                        'actid': tndrid
                    },
                    success: function(responData) {
                         
                        table.ajax.reload(null, false);
                        toastr.success(responData.msg, 'Message', {
                            timeOut: 5000
                        });



                    },
                });
            }
        }

        function bidproject(actprojid, sectid) {
            if (confirm("Are You Sure Submitted This Project ? ") == true) {
                $.ajax({
                    type: 'POST',
                    url: "<?= base_url('togoproject/bidprojectbyuser'); ?>",
                    data: {
                        'projid': actprojid
                    },
                    success: function(responData) {
                       
                        table.ajax.reload(null, false);
                        toastr.success(responData.msg, 'Message', {
                            timeOut: 5000
                        });

                    },
                });
            }
        }
    </script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.18.10/slimselect.min.js"></script>
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
    <script src="<?= FRONTASSETS; ?>js/jquery-editable-select.js"></script>






</body>