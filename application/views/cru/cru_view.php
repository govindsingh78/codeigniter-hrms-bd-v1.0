<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        <link href="<?= FRONTASSETS; ?>Customselect/css/jquery-customselect-1.9.1.css?>" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">


        <div class="ch-container">
            <div class="row">
                <!-- left menu starts -->
                <div class="col-sm-2 col-lg-2">
                    <div class="sidebar-nav">
                        <div class="nav-canvas">
                            <div class="nav-sm nav nav-stacked">
                            </div>
                            <?php $this->load->view('include/sidebar'); ?>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div id="content" class="col-lg-10 col-sm-10">
                    <!-- content starts -->
                    <div>
                        <ul class="breadcrumb">
                            <li> <a href="<?= base_url(); ?>">Home</a></li>
                            <li>
                                <a href="#">Teamrequisition Team Details </a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <?php if ($this->session->flashdata('msg')) { ?>
                            <div class="alert alert-success alert-dismissable" >
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><div id="msgs">Success ! </div></strong> <?= $this->session->flashdata('msg'); ?>
                            </div>
                        <?php } ?>

                        <!-- open Phase Section-->
                        <div class="box-inner">
                            <div class="box-content">
                                <div class="tab-content">

                                    <div id="home" class="tab-pane fade in active">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span class="col-md-12" style="
                                                      color: green;margin: 0,auto;text-align: center;font-size: 20px;"><?php echo $resulstArr[0]['generated_tenderid']; ?></span>
                                                <div id="colvis" style="float:right">
                                                    <form action="<?= base_url('cru/exportTenderTeam'); ?>" method="post" enctype="">
                                                        <input type="hidden" name="project_id" value ="<?php echo $resulstArr[0]['project_id']; ?>" />
                                                        <button class="ColVis_Button ColVis_MasterButton export_team" onclick="form.submit()">												
                                                            <span>Export</span>
                                                        </button>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php echo $resulstArr[0]['TenderDetails']; ?>

                                            </div>
                                            <div class="col-md-12">
                                                <div style="overflow-x: scroll;">
                                                    <form name="req_form" action="<?= base_url('cru/saveteamdata'); ?>" method="post" >
                                                        <table id="table" class="display" cellspacing="0" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 22%;padding: 1% 0%;">Designation</th>
                                                                    <th>Personal Details</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Marks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Firm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TotalMM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                                    <th>Construction Man Month</th>
                                                                    <th>Development Man Month</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O&M &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AgeLimit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Certifcate&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Undertaking&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Salary&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Remarks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                                    <th>Lock/Unloack</th>


                                                                </tr>
                                                            </thead>
                                                            <hr/>

                                                            <?php
                                                            $n = 0;
                                                            foreach ($resulstArr as $rows) {
                                                                ?>
                                                                <tbody>

                                                                    <tr style="border: #32a6e8 1px solid;">

                                                                <input type="hidden" id="team_id<?= $n; ?>" name="team_id[]" value="<?php echo $rows['id']; ?>">
                                                                <td><?php echo $rows['designation_name']; ?></td>
                                                                <td>
                                                                    <div class="controls">

                                                                        <select  id="selectuser<?= $n; ?>" class='custom-select' name="username[]" class="standard">
                                                                            <option <?= ($rows['user_id'] == 0) ? "selected" : ''; ?> value="">Please select Name </option>
                                                                            <?php
                                                                            if ($userData):
                                                                                foreach ($userData as $row) {
                                                                                    ?>
                                                                                    <option <?= ($rows['user_id'] == $row->fld_id) ? "selected" : ''; ?> value="<?= $row->fld_id; ?>"><?= $row->userfullname; ?></option>
                                                                                <?php } ?>
                                                                                <option <?php // echo (isset($rows['user_id']) && $rows['user_id'] == 0) ? "selected='selected'" : '';        ?> value="0">other</option>				
                                                                                <?php
                                                                            endif;
                                                                            ?>
                                                                        </select>
                                                                        <br/>
                                                                        <br/>
                                                                        <?php if (!empty($rows['otheruser_name'])) { ?>
                                                                            <input type='text' value="<?php echo $rows['otheruser_name']; ?>" class="form-control" id="hideuser<?= $n; ?>"/>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                        <input type='text' placeholder ="Name" name="other[]" id="other<?= $n; ?>" value="" class="form-control hidden1"/>
                                                                        <input type="email" name="emailaddress[]" value="<?= isset($rows['emailaddress']) ? $rows['emailaddress'] : ''; ?>" maxlength="50" value="" class="form-control" id="emailaddress<?= $n; ?>" placeholder ="Email Address">
                                                                        <input maxlength="10"  pattern="[0-9]{10}" value="<?= isset($rows['contactnumber']) ? $rows['contactnumber'] : ''; ?>" name="contactnumber[]" class="form-control" id="contactnumber<?= $n; ?>"  placeholder ="Contact Number">
                                                                        <script>

                                                                            $('#selectuser<?= $n; ?>').on('change', function () {
                                                                                var userID = this.value;
                                                                                //alert(userID);
                                                                                if (userID == "0") {
                                                                                    $('#other<?= $n; ?>').val("").removeClass('hidden1');
                                                                                } else {
                                                                                    $('#other<?= $n; ?>').val(userID).addClass('hidden1');
                                                                                    $('#hideuser<?= $n; ?>').addClass('hidden1');
                                                                                }
                                                                                $.ajax({
                                                                                    type: 'POST',
                                                                                    url: '<?= base_url('cru/userDatas'); ?>',
                                                                                    data: {'userID': userID},
                                                                                    success: function (response) {
                                                                                        var data = JSON.parse(response);
                                                                                        $('#contactnumber<?= $n; ?>').val(data[0]);
                                                                                        $('#emailaddress<?= $n; ?>').val(data[1]);
                                                                                    }
                                                                                });
                                                                            });

                                                                            $(document).ready(function () {
    <?php if ($rows['lock_mgmt'] == 1) { ?>
                                                                                    $('#team_id<?= $n; ?>').attr('disabled', true);
                                                                                    $('#hideuser<?= $n; ?>').attr('disabled', true);
                                                                                    $('#selectuser<?= $n; ?>').attr('disabled', true);
                                                                                    $('#other<?= $n; ?>').attr('disabled', true);
                                                                                    $('#emailaddress<?= $n; ?>').attr('disabled', true);
                                                                                    $('#contactnumber<?= $n; ?>').attr('disabled', true);
                                                                                    $('#marks<?= $n; ?>').attr('disabled', true);
                                                                                    $('#firm<?= $n; ?>').attr('disabled', true);
                                                                                    $('#man_months<?= $n; ?>').attr('disabled', true);
                                                                                    $('#construction_months<?= $n; ?>').attr('disabled', true);
                                                                                    $('#development_months<?= $n; ?>').attr('disabled', true);
                                                                                    $('#om_months<?= $n; ?>').attr('disabled', true);
                                                                                    $('#age_limit<?= $n; ?>').attr('disabled', true);
                                                                                    $('#certificate<?= $n; ?>').attr('disabled', true);
                                                                                    $('#undertaking<?= $n; ?>').attr('disabled', true);
                                                                                    $('#salary<?= $n; ?>').attr('disabled', true);
                                                                                    $('#remarks<?= $n; ?>').attr('disabled', true);
        <?php
    } else {
        if ($rows['lock_manager'] == 1) {
            ?>
                                                                                        $('#hideuser<?= $n; ?>').attr('disabled', true);
                                                                                        $('#team_id<?= $n; ?>').attr('disabled', true);
                                                                                        $('#selectuser<?= $n; ?>').attr('disabled', true);
                                                                                        $('#other<?= $n; ?>').attr('disabled', true);
                                                                                        $('#emailaddress<?= $n; ?>').attr('disabled', true);
                                                                                        $('#contactnumber<?= $n; ?>').attr('disabled', true);
                                                                                        $('#marks<?= $n; ?>').attr('disabled', true);
                                                                                        $('#firm<?= $n; ?>').attr('disabled', true);
                                                                                        $('#man_months<?= $n; ?>').attr('disabled', true);
                                                                                        $('#construction_months<?= $n; ?>').attr('disabled', true);
                                                                                        $('#development_months<?= $n; ?>').attr('disabled', true);
                                                                                        $('#om_months<?= $n; ?>').attr('disabled', true);
                                                                                        $('#age_limit<?= $n; ?>').attr('disabled', true);
                                                                                        $('#certificate<?= $n; ?>').attr('disabled', true);
                                                                                        $('#undertaking<?= $n; ?>').attr('disabled', true);
                                                                                        $('#salary<?= $n; ?>').attr('disabled', true);
                                                                                        $('#remarks<?= $n; ?>').attr('disabled', true);

            <?php
        } else {
            if ($rows['lock_by_cru'] == 1) {
                ?>
                                                                                            $('#hideuser<?= $n; ?>').attr('disabled', true);
                                                                                            $('#team_id<?= $n; ?>').attr('disabled', true);
                                                                                            $('#selectuser<?= $n; ?>').attr('disabled', true);
                                                                                            $('#other<?= $n; ?>').attr('disabled', true);
                                                                                            $('#emailaddress<?= $n; ?>').attr('disabled', true);
                                                                                            $('#contactnumber<?= $n; ?>').attr('disabled', true);
                                                                                            $('#marks<?= $n; ?>').attr('disabled', true);
                                                                                            $('#firm<?= $n; ?>').attr('disabled', true);
                                                                                            $('#man_months<?= $n; ?>').attr('disabled', true);
                                                                                            $('#construction_months<?= $n; ?>').attr('disabled', true);
                                                                                            $('#development_months<?= $n; ?>').attr('disabled', true);
                                                                                            $('#om_months<?= $n; ?>').attr('disabled', true);
                                                                                            $('#age_limit<?= $n; ?>').attr('disabled', true);
                                                                                            $('#certificate<?= $n; ?>').attr('disabled', true);
                                                                                            $('#undertaking<?= $n; ?>').attr('disabled', true);
                                                                                            $('#salary<?= $n; ?>').attr('disabled', true);
                                                                                            $('#remarks<?= $n; ?>').attr('disabled', true);

                <?php
            }
        }
        ?>


    <?php } ?>
                                                                            });
                                                                        </script>

                                                                    </div>	

                                                                </td>
                                                                <td><input type="number" value="<?= isset($rows['marks']) ? $rows['marks'] : ''; ?>" id="marks<?= $n; ?>" name="marks[]" class="form-control"></td>
                                                                <td><input type="text" value="<?= isset($rows['firm']) ? $rows['firm'] : ''; ?>" id="firm<?= $n; ?>" name="firm[]" class="form-control"></td>
                                                                <td><input type="number" id="man_months<?= $n; ?>" name="man_months[]" value="<?php echo $rows['man_months']; ?>" class="form-control"></td>
                                                                <td><input type="number" id="construction_months<?= $n; ?>" name="construction_months[]" value="<?php echo $rows['construction_months']; ?>" class="form-control"></td>
                                                                <td><input type="number" id="development_months<?= $n; ?>" name="development_months[]" value="<?php echo $rows['development_months']; ?>" class="form-control"></td>
                                                                <td><input type="number" id="om_months<?= $n; ?>" name="om_months[]" value="<?php echo $rows['om_months']; ?>" class="form-control"></td>
                                                                <td><input type="number" id="age_limit<?= $n; ?>" name="age_limit[]" value="<?php echo $rows['age_limit']; ?>" class="form-control"></td>
                                                                <td>
                                                                    <select name="certificate[]" class="form-control" id="certificate<?= $n; ?>">
                                                                        <option >Select</option>
                                                                        <option <?= (isset($rows['certificate']) && $rows['certificate'] == '1') ? "selected='selected'" : ''; ?> value="1"> Yes </option>
                                                                        <option <?= (isset($rows['certificate']) && $rows['certificate'] == '2') ? "selected='selected'" : ''; ?> value="2"> No </option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select name="undertaking[]" class="form-control" id="undertaking<?= $n; ?>">
                                                                        <option>Select</option>
                                                                        <option <?= (isset($rows['undertaking']) && $rows['undertaking'] == '1') ? "selected='selected'" : ''; ?>  value="1"> Yes </option>
                                                                        <option <?= (isset($rows['undertaking']) && $rows['undertaking'] == '2') ? "selected='selected'" : ''; ?> value="2"> No </option>
                                                                    </select>
                                                                </td>
                                                                <td><input type="number" id="salary<?= $n; ?>" name="salary[]" value="<?php echo $rows['salary']; ?>" class="form-control"></td>
                                                                <td> <textarea name="remarks[]" id="remarks<?= $n; ?>" class="form-control"><?php echo $rows['remarks']; ?></textarea></td>
                                                                <td> 
                                                                    <?php if ($rows['lock_mgmt'] == 1) { ?>
                                                                        <a href="#" title="Lock"><i class="fa fa-lock" aria-hidden="true"></i></a>
                                                                        <?php
                                                                    } else {
                                                                        if ($rows['lock_manager'] == 1) {
                                                                            ?>
                                                                            <a href="#" title="Lock"><i class="fa fa-lock" aria-hidden="true"></i></a>

                                                                            <?php
                                                                        } else {
                                                                            if ($rows['lock_by_cru'] == 1) {
                                                                                ?>		<a href="#" title="Lock"><i class="fa fa-lock" aria-hidden="true"></i></a>
                                                                            <?php } else { ?>
                                                                                <a href="#" title="Unlock" onclick="lock_data(<?php echo $rows['id']; ?>)"><i class="fa fa-unlock-alt" aria-hidden="true"></i></a>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>

                                                                                        <!--<i class="fa fa-lock" aria-hidden="true"></i>-->
                                                                    <?php } ?>
                                                                </td>

                                                                </tr>

                                                                </tbody>
                                                                <?php
                                                                $n++;
                                                            }
                                                            ?>
                                                        </table>
                                                        <div class="form-group has-success col-md-6"> 
                                                            <input type="hidden" name="url_chk" value="1">
                                                            <input type="hidden" name="project_id" value="<?php echo $resulstArr[0]['project_id']; ?>">
                                                        </div>
                                                        <div class="form-group has-success col-md-12"> 
                                                            <input class="btn btn-success"  type="submit" name="submit" value="Submit">
                                                        </div>

                                                    </form>
                                                </div>

                                            </div>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Close phase Section-->

                </div>

            </div>
        </div>	

        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>

        <hr>




        <!-- BootStrap Model For Comments.. -->
        <?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->
        <?php $this->load->view('include/footer'); ?>
        <script src="<?= FRONTASSETS; ?>jquery/jquery-2.2.3.min.js?>"></script>
        <script src="<?= FRONTASSETS; ?>js/bootstrap.min.js?>" ></script>
        <script src="<?= FRONTASSETS; ?>Customselect/js/jquery-customselect-1.9.1.js"></script>
        <script src="<?= FRONTASSETS; ?>Customselect/js/jquery-customselect-1.9.1.min.js"></script>
        <style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            .designation_detail{display:none;}
            #selectuser_chosen{width:251px!important;}
            .hidden1{display:none;}

        </style>
        <script type="text/javascript">
                                                                    $(document).ready(function () {
                                                                        $("li#teamrequisition").addClass('active');
                                                                    });

                                                                    function lock_data(ids) {
                                                                        $.ajax({
                                                                            type: 'post',
                                                                            url: "<?= base_url('cru/locadata'); ?>",
                                                                            data: {id: ids},
                                                                            success: function (response) {
                                                                                //console.log(response);
                                                                                location.reload(1);
                                                                            }
                                                                        });
                                                                    }
        </script>

    </div>
</body>
</html>
