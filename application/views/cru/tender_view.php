<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');


//Financial Year Condition Code By Asheesh.. 14-02-2019.
$year = '2016';
$YearPluse1 = (date('Y') + 1);
$finCdateStart = "01-04-" . date('Y');

// $finCdateEnd = "31-03-" . $YearPluse1;
$gEtCuurentDate = date("d-m-Y");
$gEtCuurentDateCONV = strtotime($gEtCuurentDate);
$finCdateStartCONV = strtotime($finCdateStart);

if ($finCdateStartCONV >= $finCdateStartCONV):
$currentyear = date('Y') - 1;
else:
$currentyear = date('Y');
endif;
?>

<style>
    #table_length {
        position: absolute;
        margin-left: 100px !important;
    }

    .table tbody tr td,
    .table tbody th td {
        vertical-align: middle;
        white-space: normal !important;
    }
    div.ColVis {
    float: right;
    margin-top: 35px;
}
</style>
<link href="http://172.16.1.30:8089/frontendassets/datatables/css/dataTables.colVis.css?>" rel="stylesheet">
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row clearfix">
                    <div class="col-lg-12">

                    <?php if ($this->session->flashdata('msg')) { ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>
                                            <div id="msgs">Success!</div>
                                        </strong> <?= $this->session->flashdata('msg'); ?>
                                    </div>
                                <?php } ?>
                                <?php if ($this->session->flashdata('errmsg')) { ?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close"> X </a>
                                        <strong>Error ! </strong> <?= $this->session->flashdata('errmsg'); ?>
                                    </div>
                                <?php } ?>

                        <div class="card">
                            <div class="header">

                               

                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="email"> Sector : </label>
                                            <select name="sectorinput" id="sectorinput" class="form-control">
                                                <option <?= ($secId == "") ? 'Selected' : ''; ?> value=""> --All-- </option>
                                                <?php
                                                if ($sectorArr) :
                                                    foreach ($sectorArr as $row) {
                                                ?>
                                                        <option <?= ($secId == $row->fld_id) ? 'Selected' : ''; ?> value="<?= $row->fld_id; ?>"><?= $row->sectName; ?></option>
                                                <?php }
                                                endif; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="email"> Status : </label>
                                            <select id="pro_status" name="pro_status" class="form-control">
                                                <option selected="" value="">All</option>
                                                <option value="To_Go_project"> To be Submitted </option>
                                                <option value="Bid_project">Submitted</option>
                                                <option value="No_Submit_project">Not Submitted</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="email"> Financial Year : </label>
                                            <select id="financial_year" class="form-control">
                                                <option value="" selected="selected">--Financial Year--</option>
                                                <?php
                                                for ($i = $year; $i <= 2022; $i++) {
                                                ?>
                                                    <option <?php echo (date('Y') == $i) ? 'selected' : ''; ?> value=<?= $i; ?>><?= $i; ?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="email"> Project status : </label>
                                            <select id="status" class="form-control">
                                                <option value=""> -- Select Status -- </option>
                                                <option value="1">Won</option>
                                                <option value="2">Lose</option>
                                                <!--                                        <option value="3">Cancel</option>-->
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">

                                            <button type="button" id="btn-filter" class="btn btn-primary pull-right ml-2 mt-4"> Filter </button> &nbsp;
                                            <button type="button" id="btn-reset" class="btn btn-default pull-right mt-4"> Reset </button>
                                        </div>
                                    </div>
                                </div>



                            </div>







 

                            <div class="row clearfix">
                                <div class="col-lg-12">
                                    <div class="card">
                                    
                                        <div class="body">
                                            <div class="table-responsive">

                                            <div id="colvis"></div>
                                               
                                            


                                                <table id="table" class="table table-striped display" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Sr. No</th>
                                                            <th>Tender Name</th>
                                                            <th>Tender ID</th>
                                                            <th>Location</th>
                                                            <th>Client</th>
                                                            <th>Actions </th>
                                                            <th>Team Clone</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Sr. No</th>
                                                            <th>Tender Name</th>
                                                            <th>Tender ID</th>
                                                            <th>Location</th>
                                                            <th>Client</th>
                                                            <th>Actions</th>
                                                            <th>Team Clone</th>
                                                        </tr>
                                                    </tfoot>
                                                    <hr>
                                                </table>




                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>





                        </div>

                    </div>
                </div>
            </div>

             

            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">To : <span id="projcodee"></span> </h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="email"> From :- Select Project Code</label>
                                        <select id='nosearch' onchange="truteamclone()" name='project_code' class="custom-select form-control">
                                            <option value=''> - Please Code -</option>
                                            <?php foreach ($tenderGenCode as $rows) { ?>
                                                <option value='<?= $rows->project_id; ?>'><?= $rows->generated_tenderid; ?></option>
                                            <?php } ?>
                                        </select>
                                        <input type="hidden" name="clonetoproj" id="clonetoproj" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('admin/includes/footer'); ?>
    </div>
    <script src="http://172.16.1.30:8089/frontendassets/datatables/js/dataTables.colVis.js?>" ></script>
    <script src='https://www.jqueryscript.net/demo/jQuery-Plugin-For-Custom-Searchable-Select-List-Customselect/src/jquery-customselect.js'></script>
    <link href='https://www.jqueryscript.net/demo/jQuery-Plugin-For-Custom-Searchable-Select-List-Customselect/src/jquery-customselect.css' rel='stylesheet' />
    <script>
        $(function() {
            $("#nosearch").customselect();
        });

        function truteamclone() {
            var cloneforprojid = $("#nosearch").val();
            var clonetoprojid = $("#clonetoproj").val();
            $.ajax({
                type: 'post',
                url: "<?php echo base_url('cru/ajax_clonecondition'); ?>",
                data: {
                    cloneforprojid: cloneforprojid,
                    clonetoprojid: clonetoprojid
                },
                success: function(response) {
                    var parsed = JSON.parse(response);
                    var arr = [];
                    for (var x in parsed) {
                        arr.push(parsed[x]);
                    }
                    if (arr[1] == false) {
                        alert("Error : " + arr[0]);
                    }

                    if (arr[1] == true) {
                        alert("Success : " + arr[0]);
                        //window.location = '';
                    }

                }
            });
        }

        function setnotimportant(projid, projcodee) {
            $("#clonetoproj").val(projid);
            $("#projcodee").html(projcodee);
        }
    </script>
  
    <style>
        #table_length {
            margin-left: 20px;
        }

        #table_filter {
            margin-right: 2%;
        }

        #chatbox {
            padding: 15px;
            overflow: scroll;
            height: 300px;
        }

        .chkempclassred {
            color: red;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            $("li#cruproject").addClass('active');
        });

        var table;
        $(document).ready(function() {
            //datatables
            table = $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "order": [], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url('cru/newProjectAll') ?>",
                    "type": "POST",
                    "data": function(data) {
                        data.project_name = $('#company_name').val();
                        data.sectorinput = $('#sectorinput').val();
                        data.pro_status = $('#pro_status').val();
                        data.financial_year = $('#financial_year').val();
                        data.status = $('#status').val();
                    },
                },
                "dom": 'lBfrtip',
                "buttons": [{
                    extend: 'collection',
                    text: 'Export',
                    buttons: ['copy', 'excel', 'csv', 'pdf', 'print']
                }],
                //Set column definition initialisation properties.
                "columnDefs": [{
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                }, ],
                "aLengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
            });
            var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
            $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
            $('#btn-filter').click(function() { //button filter event click
                table.ajax.reload(); //just reload table
            });
            $('#btn-reset').click(function() { //button reset event click
                $('#form-filter')[0].reset();
                table.ajax.reload(); //just reload table
            });
        });
    </script>
</body>