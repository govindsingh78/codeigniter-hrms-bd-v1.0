<html lang="en">
    <?php  error_reporting(E_ALL); //error_reporting(0); ?>
    <?php 
    $this->load->view('include/innerhead');
    ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        <link href="<?= FRONTASSETS; ?>Customselect/css/jquery-customselect-1.9.1.css?>" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <div class="ch-container">
            <div class="row">
                <!-- left menu starts -->
                <div class="col-sm-2 col-lg-2">
                    <div class="sidebar-nav">
                        <div class="nav-canvas">
                            <div class="nav-sm nav nav-stacked">
                            </div>
                            <?php $this->load->view('include/sidebar'); ?>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div id="content" class="col-lg-10 col-sm-10">
                    <!-- content starts -->
                    <div>
                        <ul class="breadcrumb">
                            <li> <a href="<?= base_url(); ?>">Home</a></li>
                            <li><a href="#">Teamrequisition Team Details </a></li>
                        </ul>
                    </div>

                    <div class="row">
                        <?php if ($this->session->flashdata('msg')) { ?>
                            <div class="alert alert-success alert-dismissable" >
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><div id="msgs">Success ! </div></strong> <?= $this->session->flashdata('msg'); ?>
                            </div>
                        <?php } ?>

                        <!-- open Phase Section-->
                        <div class="box-inner">
                            <div class="box-content">
                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span class="col-md-12" style="color: green;margin: 0,auto;text-align: center;font-size: 20px;"><?php echo $resulstArr[0]['project_no']; ?></span>
                                                <div class="container-fluid">
                                                    <p><?= ProjNameById($resulstArr[0]['project_id']); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container" id="formsection" style="display:none">
                                            <form name="frmm" id="frmm" method="post" action="<?= base_url('cru/save_cruteam_req'); ?>" enctype="">
                                                <div class="well well-sm">
                                                    <h3> Position : </h3>
                                                    <input type="text" id="designation_name" class="form-control" >
                                                    <input type="hidden" id="designation_id" name="designation_id" class="form-control" >

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group required">
                                                            <label class="control-label" for="usr">Select Name (In House) </label> &nbsp;&nbsp;&nbsp;
                                                            <select  class="form-control" id="empid" name="empname" onchange="setdetailsemp()">
                                                                <option value="">Select Employee</option>
                                                                <?php
                                                                if ($userData):
                                                                    foreach ($userData as $row) {
                                                                        ?>
                                                                        <option value="<?= $row->fld_id; ?>"><?= $row->userfullname; ?></option>
                                                                        <?php
                                                                    }
                                                                endif;
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group required">
                                                            <label class="control-label" for="usr">Select Name (Other) </label> &nbsp;&nbsp;&nbsp;
                                                            <span style="cursor:pointer" title="Add New Other Employee" data-toggle="modal" data-target="#myModalotherEmp"> <i class="fa fa-users"></i> </span>
                                                            <select  class="form-control" id="empidother" name="empnameother" onchange="setdetailsempother()">
                                                                <option value="">Select Employee Other</option>
                                                                <?php
                                                                $recArr = GetAllOtherUserRec();
                                                                foreach ($recArr as $row) {
                                                                    ?>
                                                                    <option value="<?= $row->fld_id; ?>"><?= $row->emp_name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group required">
                                                            <label class="control-label" for="usr">Email </label>
                                                            <input type="email" name="emailaddress" required id="emailaddress"  class="form-control" >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group required">
                                                            <label class="control-label" for="usr">Contact No </label>
                                                            <input type="number" min="1" name="contactnumber" required class="form-control" id="contactnumber">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group required">
                                                            <label class="control-label" for="usr">Marks </label>
                                                            <input type="text" required class="form-control" name="marks" id="remarks_bd">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label" for="usr">Firm</label>
                                                            <input type="text" class="form-control" name="firm" id="remarks_bd">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label" for="usr">Certifcate </label>
                                                            <select class="form-control" name="certificate" id="remarks_bd">
                                                                <option>Select</option>
                                                                <option value="1"> Yes </option>
                                                                <option value="2"> No </option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label" for="usr">Undertaking </label>
                                                            <select class="form-control" name="undertaking" id="remarks_bd">
                                                                <option>Select</option>
                                                                <option value="1"> Yes </option>
                                                                <option value="2"> No </option>
                                                            </select>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="row">



                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label" for="usr">Salary</label>
                                                            <input type="text" class="form-control" name="salary" id="remarks_bd">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label" for="usr">Remarks (CRU) </label>
                                                            <input type="text" class="form-control" name="remarks_cru" id="remarks_bd">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label" for="usr">Remarks (BD)</label>
                                                            <input type="text" class="form-control" name="remarks_bd" id="remarks_bd" >
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <br>
                                                            <input type="hidden" name="project_id" value="<?= $resulstArr[0]['project_id']; ?>">
                                                            <input type="submit" class="btn-primary" value="Submit">
                                                            <input type="hidden" id="fld_id" name="fld_id" class="form-control" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                        <br>

                                        <div class="container">
                                            <button onclick="window.open('<?= base_url('cru/exportTenderTeam?project_id=' . $resulstArr[0]['project_id']); ?>', '_blank')" class="btn pull-right"> Export </button>
                                            <p>&nbsp;</p>
                                            <table style="overflow: scroll; overflow: auto;" border="1" width="100%" sortable="true" class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Sr no</th>
                                                        <th>Position</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Contact</th>
                                                        <th>Age Limit</th>
                                                        <th>Certifcate</th>
                                                        <th>Firm</th>
                                                        <th>Marks</th>
                                                        <th>Total MM</th>
                                                        <th>RFP Marks</th>
                                                        <th>Weightage Marks</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $fixArr = array('1' => 'Yes', '2' => 'No');
                                                    $srn = 0;
                                                    if ($tqRecArr):
                                                        foreach ($tqRecArr as $roRec) {
                                                            ?>
                                                            <tr>
                                                                <?php
                                                                $empNM = '';
                                                                $empNM2 = '';
                                                                $desiname = DesignationNameById($roRec->designation_id);
                                                                if ($roRec->empname):
                                                                    $empNM = UserNameById($roRec->empname);
                                                                else:
                                                                    $empNM2 = UserOtherNameById($roRec->empnameother);
                                                                endif;
                                                                ?>
                                                                <td>
                                                                    <?= $srn = $srn + 1; ?> &nbsp;
                                                                    <?php if (($empNM == '') AND ( $empNM2 == '')) { ?>
                                                                        <i onclick="setnewadd('<?= $desiname; ?>', '<?= $roRec->designation_id; ?>', '<?= $roRec->id; ?>')" style="cursor:pointer" title="Add Details" class="fa fa-user"></i>
                                                                    <?php } ?>
                                                                </td>
                                                                <td><?= $desiname; ?></td>
                                                                <td><?= $empNM . " " . $empNM2; ?></td>
                                                                <td><?= $roRec->emailaddress; ?></td>
                                                                <td><?= $roRec->contactnumber; ?></td>
                                                                <td><?= $roRec->age_limit; ?></td>
                                                                <td><?= @$fixArr[$roRec->certificate]; ?></td>
                                                                <td><?= $roRec->firm; ?></td>
                                                                <td><?= $roRec->marks; ?></td>
                                                                <td><?= $roRec->man_months; ?></td>
                                                                <td>
                                                                    <?php
                                                                    $realrfp = GetRfpByPid_desId($resulstArr[0]['project_id'], $roRec->designation_id);
                                                                    echo @$realrfp;
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $realWei = ($roRec->marks / 100) * $realrfp;
                                                                    echo $realWei;
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php if ($roRec->locks == '') : ?>
                                                                        <i title="Lock " id="lock<?= $roRec->id; ?>" onclick="lockteamreq('<?= $roRec->id; ?>')" style="cursor:pointer" class="fa fa-lock"></i> &nbsp;
                                                                    <?php endif; ?>
                                                                    <i title="Delete" onclick="deleteteamreq('<?= $roRec->id; ?>')" style="cursor:pointer" class="fa fa-trash"></i> &nbsp;
                                                                    <br>
                                                                    <!--  Emp Lock Cond-->
                                                                    <?php if (($roRec->locks == '') and ( $loginRoleEmp->role_name == 'employee')): ?>
                                                                        <i title="Edit / View" data-toggle="modal" data-target="#modelTQedit" onclick="editteamreq('<?= $roRec->id; ?>')" style="cursor:pointer" class="fa fa-edit"></i> &nbsp;
                                                                    <?php endif; ?>
                                                                    <!--  cru Lock Cond-->
                                                                    <?php if (($roRec->locks == '' or $roRec->locks == 'employee') and ( $loginRoleEmp->role_name == 'cru_manager')): ?>
                                                                        <i title="Edit / View" data-toggle="modal" data-target="#modelTQedit" onclick="editteamreq('<?= $roRec->id; ?>')" style="cursor:pointer" class="fa fa-edit"></i> &nbsp;
                                                                    <?php endif; ?>
                                                                    <!--Perpos Manager Cond-->
                                                                    <?php if (($roRec->locks == '' or $roRec->locks == 'employee' or $roRec->locks == 'cru_manager') and ( $loginRoleEmp->role_name == 'proposal_manager')): ?>
                                                                        <i title="Edit / View" data-toggle="modal" data-target="#modelTQedit" onclick="editteamreq('<?= $roRec->id; ?>')" style="cursor:pointer" class="fa fa-edit"></i> &nbsp;
                                                                    <?php endif; ?>
                                                                    <!--Top Manager Cond-->
                                                                    <?php if ($loginRoleEmp->role_name == 'top_management'): ?>
                                                                        <i title="Edit / View" data-toggle="modal" data-target="#modelTQedit" onclick="editteamreq('<?= $roRec->id; ?>')" style="cursor:pointer" class="fa fa-edit"></i> &nbsp;
                                                                    <?php endif; ?>
                                                                    <br>
                                                                    <?php if ($roRec->resume_file == '') { ?>
                                                                        <i title="Upload Resume" data-toggle="modal" data-target="#modeUpdResume" onclick="uploadresume('<?= $roRec->id; ?>', '<?= $empNM . " " . $empNM2; ?>')" style="cursor:pointer" class="fa fa-paperclip"></i>
                                                                    <?php } else { ?>
                                                                        <br>
                                                                        <a href="<?= HOSTNAME . "uploads/resume/" . $roRec->resume_file; ?>" target="_blank"> <i title="Resume" style="cursor:pointer" class="fa fa-download"></i> </a>
                                                                    <?php } ?>
                                                                </td>  
                                                            </tr>
                                                        <?php } else: ?>
                                                        <tr>
                                                            <td colspan="13">
                                                                <p align="center" style="color:red">No Any Entry for this Project</p>
                                                            </td>
                                                        <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Close phase Section-->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>
        <hr>

        <?php $this->load->view('include/footer'); ?>

        <script src="<?= FRONTASSETS; ?>jquery/jquery-2.2.3.min.js?>"></script>
        <script src="<?= FRONTASSETS; ?>js/bootstrap.min.js?>" ></script>
        <script src="<?= FRONTASSETS; ?>Customselect/js/jquery-customselect-1.9.1.js"></script>
        <script src="<?= FRONTASSETS; ?>Customselect/js/jquery-customselect-1.9.1.min.js"></script>
        <style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;}
            .designation_detail{display:none;}
            #selectuser_chosen{width:251px!important;}
            .hidden1{display:none;}

        </style>
        <script type="text/javascript">
                                                                            $(document).ready(function () {
                                                                                $("li#cruproject").addClass('active');
                                                                            });

                                                                            function setdetailsemp() {
                                                                                var userID = $("#empid").val();
                                                                                $("#empidother").val('');
                                                                                $("#contactnumber").val('');
                                                                                $("#emailaddress").val('');
                                                                                if (userID) {
                                                                                    $.ajax({
                                                                                        type: 'POST',
                                                                                        url: '<?= base_url('cru/userDatas'); ?>',
                                                                                        data: {'userID': userID},
                                                                                        success: function (response) {
                                                                                            var data = JSON.parse(response);
                                                                                            $('#contactnumber').val(data[0]);
                                                                                            $('#emailaddress').val(data[1]);
                                                                                        }
                                                                                    });
                                                                                }
                                                                            }

                                                                            function setdetailsempother() {
                                                                                var userIDOther = $("#empidother").val();
                                                                                $("#empid").val('');
                                                                                $("#contactnumber").val('');
                                                                                $("#emailaddress").val('');
                                                                                $.ajax({
                                                                                    type: 'POST',
                                                                                    url: '<?= base_url('cru/userDatasOther'); ?>',
                                                                                    data: {'userIDOther': userIDOther},
                                                                                    success: function (response) {
                                                                                        var data = JSON.parse(response);
                                                                                        $('#contactnumber').val(data[1]);
                                                                                        $('#emailaddress').val(data[0]);
                                                                                    }
                                                                                });
                                                                            }

                                                                            //Edit Process By Asheesh,,
                                                                            function editteamreq(edtid) {
                                                                                $.ajax({
                                                                                    type: 'post',
                                                                                    url: "<?= base_url('cru/tqDetailsByID'); ?>",
                                                                                    data: {edtid: edtid},
                                                                                    success: function (response) {
                                                                                        var obj = jQuery.parseJSON(response);
                                                                                        $("#emailaddressUPD").val(obj.emailaddress);
                                                                                        $("#contactnumberUPD").val(obj.contactnumber);
                                                                                        $("#marksUPD").val(obj.marks);
                                                                                        $("#firmUPD").val(obj.firm);
                                                                                        $("#man_monthsUPD").val(obj.man_months);
                                                                                        $("#age_limitUPD").val(obj.age_limit);
                                                                                        $("#salaryUPD").val(obj.salary);
                                                                                        $("#remarks_cruUPD").val(obj.remarks_cru);
                                                                                        $("#remarks_bdUPD").val(obj.remarks_bd);
                                                                                        $("#editid").val(obj.id);
                                                                                        $("#project_idUPD").val(obj.project_id);
                                                                                        $("#certificateUPD").val(obj.certificate);
                                                                                        $("#undertakingUPD").val(obj.undertaking);
                                                                                        $("#designation_idUPD").val(obj.designation_id);
                                                                                        $("#empnameUPD").html("Employee Name : " + obj.empname);
                                                                                    }
                                                                                });
                                                                            }

                                                                            //Delete Process
                                                                            function deleteteamreq(delid) {
                                                                                if (confirm("Are You Sure Delete This ? ") == true) {
                                                                                    window.location = "<?= base_url('cru/deleteam_treq?delid='); ?>" + delid;
                                                                                }
                                                                            }

                                                                            //Lock Proc..
                                                                            function lockteamreq(lockid) {
                                                                                $.ajax({
                                                                                    type: 'POST',
                                                                                    url: '<?= base_url('cru/lockbylbl'); ?>',
                                                                                    data: {'lockid': lockid},
                                                                                    success: function (response) {
                                                                                        $('#lock' + lockid).hide();
                                                                                    }
                                                                                });
                                                                            }


                                                                            //Upload Resume Ajax Section..
                                                                            function uploadresume(actid, empname) {
                                                                                $("#emplnname").html(" Resume of : " + empname);
                                                                                $("#uploaderror2").hide();
                                                                                $("#actionid").val(actid);
                                                                            }

                                                                            //Resume Upload Validation Code By Asheesh..
                                                                            $(document).ready(function () {
                                                                                $("#uploaderror2").hide();
                                                                                $('INPUT[type="file"]').change(function () {
                                                                                    var ext = this.value.match(/\.(.+)$/)[1];
                                                                                    switch (ext) {

                                                                                        case 'doc':
                                                                                        case 'docx':
                                                                                        case 'pdf':
                                                                                            $('#uploadButton').attr('disabled', false);
                                                                                            $("#uploaderror2").hide();
                                                                                            break;
                                                                                        default:
                                                                                            $("#uploaderror2").show();
                                                                                            $("#uploaderror2").html('This is not an allowed file type.');
                                                                                            this.value = '';
                                                                                    }
                                                                                });
                                                                                $('INPUT[type="file"]').bind('change', function () {
                                                                                    var imgfilesize = (this.files[0].size / 17024);
                                                                                    if (imgfilesize > 100) {
                                                                                        $("#uploaderror2").show();
                                                                                        $("#uploaderror2").html('Reducing File Size Max Uploads (17MB). ');
                                                                                        this.value = '';
                                                                                    } else {
                                                                                        $("#uploaderror2").hide();
                                                                                    }
                                                                                });
                                                                            });
        </script>


        <!-- Modal For Add New Other EMP -->
        <div class="modal fade" id="modelTQedit" role="dialog">
            <div class="modal-dialog">
                <form name="othrfrm" id="othrfrm" method="post" action="<?= base_url('cru/updaterqdata'); ?>">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add New Other Employee</h4>
                            <h4 id="empnameUPD" class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">Position </label>
                                <select required="" name="designation_id" id="designation_idUPD" class="form-control" >
                                    <option value=""> Select </option>
                                    <?php foreach ($resulstArr as $rows) { ?>
                                        <option value="<?= $rows['designation_id']; ?>"><?= $rows['designation_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="control-label" for="usr">Email </label>
                                <input type="email" name="emailaddress" required id="emailaddressUPD"  class="form-control" >
                            </div>
                            <div class="form-group required col-md-4">
                                <label class="control-label" for="usr">Contact No </label>
                                <input type="number" min="1" name="contactnumber" required class="form-control" id="contactnumberUPD">
                            </div>
                            <div class="form-group required col-md-4">
                                <label class="control-label" for="usr">Marks </label>
                                <input type="text" required class="form-control" name="marks" id="marksUPD">
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label" for="usr"> Firm </label>
                                <input type="text" class="form-control" name="firm" id="firmUPD">
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label" for="usr"> Total MM </label>
                                <input type="text" class="form-control" name="man_months" id="man_monthsUPD">
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label" for="usr">Age Limit </label>
                                <input type="number" min="1" class="form-control" name="age_limit" id="age_limitUPD">
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label" for="usr">Certifcate </label>
                                <select class="form-control" name="certificate" id="certificateUPD">
                                    <option value="">Select</option>
                                    <option value="1"> Yes </option>
                                    <option value="2"> No </option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label" for="usr">Undertaking </label>
                                <select class="form-control" name="undertaking" id="undertakingUPD">
                                    <option value="">Select</option>
                                    <option value="1"> Yes </option>
                                    <option value="2"> No </option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label" for="usr">Salary </label>
                                <input type="text" class="form-control" name="salary" id="salaryUPD">
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label" for="usr">Remarks (CRU) </label>
                                <input type="text" class="form-control" name="remarks_cru" id="remarks_cruUPD">
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label" for="usr">Remarks (BD) </label>
                                <input type="text" class="form-control" name="remarks_bd" id="remarks_bdUPD" >
                            </div>

                            <div class="form-group">
                                <br>
                                <input type="hidden" id="project_idUPD" name="project_id" value="" />
                                <input type="hidden" id="editid" name="editid" value="" />
                                <input type="submit"  class="btn-primary" value="Update & Save" >
                            </div>
                            <br>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- Modal  Edit Team Req Mode Pop UP-->
        <div class="modal fade" id="myModalotherEmp" role="dialog">
            <div class="modal-dialog">
                <form name="othrfrm" id="othrfrm" method="post" action="<?= base_url('cru/saveotherempdata'); ?>">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit / Update Team ReQ</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group required">
                                <label class="control-label" for="usr">Name </label>
                                <input type="text" required id="oname" name="oname" class="form-control" >
                            </div>
                            <div class="form-group required">
                                <label class="control-label" for="usr">Email </label>
                                <input type="email" required id="oemail" name="oemail" class="form-control" >
                            </div>
                            <div class="form-group required">
                                <label class="control-label" for="usr">Contact No </label>
                                <input type="number" min="1" required id="ocontact" name="ocontact" class="form-control" >
                            </div>
                            <div class="form-group required">
                                <input type="hidden" name="project_id" value ="<?= $resulstArr[0]['project_id']; ?>" />
                                <input type="submit"  class="btn-primary" value="Add" >
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- Modal  Cand Resume Upload Mode Pop UP-->
        <div class="modal fade" id="modeUpdResume" role="dialog">
            <div class="modal-dialog">
                <form name="othrfrmres" id="othrfrmres" method="post" enctype="multipart/form-data" action="<?= base_url('cru/saveupld_resume'); ?>">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 id="emplnname" class="modal-title"></h4>
                        </div>
                        <h4 id="uploaderror2" style="color:red" class="modal-title"></h4>
                        <div class="modal-body">
                            <div class="form-group required">
                                <label class="control-label" for="usr">Upload File (<small> .pdf .doc </small>) </label>
                                <input type="file" required id="resume" name="resume" class="form-control" >
                            </div>

                            <div class="form-group">
                                <label class="control-label" for="usr">Extra Contact No </label>
                                <input type="number" maxlength="12" min="1" required id="ocontact" name="ocontact" class="form-control" >
                            </div>
                            <div class="form-group required">
                                <input type="hidden" name="project_id" id="projid_resume" value ="<?= $resulstArr[0]['project_id']; ?>" />
                                <input type="hidden" name="act_id" id="actionid" value ="" />
                                <input type="submit"  class="btn-primary" value="Upload & Save" >
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>


    </body>
    <style>
        .form-group.required .control-label:after {
            content:"*";
            color:red;
        }
    </style>
    <link href="<?= FRONTASSETS; ?>fm.selectator.jquery.css" rel="stylesheet" type="text/css">
    <script src="<?= FRONTASSETS; ?>fm.selectator.jquery.js"></script>
    <script>
                                                                            $('#empid').selectator({
                                                                                showAllOptionsOnFocus: true,
                                                                                searchFields: 'value text subtitle right'
                                                                            });


                                                                            function setnewadd(designame, designid, fldid) {
                                                                                $('#designation_name').val(designame);
                                                                                $('#designation_id').val(designid);
                                                                                $('#fld_id').val(fldid);
                                                                                $('#formsection').show();
                                                                            }
    </script>

</html>
