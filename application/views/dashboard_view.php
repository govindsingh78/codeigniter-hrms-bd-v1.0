<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<style>
    
    div#tableEoi_length, div#tableProp_length, div#tableAwait_length, div#tableWon_length {
    display: inline-block;
}

.dt-buttons {
    margin-bottom: 10px;
    margin-left: 10px;
}
.dataTables_filter{
position: absolute !important;
}

div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: 180px;
}

.dt-bootstrap4 .dt-buttons+.dataTables_filter, .dt-bootstrap4 .dt-buttons+.dataTables_paginate, .dt-bootstrap4 .dataTables_info+.dataTables_filter, .dt-bootstrap4 .dataTables_info+.dataTables_paginate {
    float: right;
    margin-left: 7px;
}


.top_counter .icon {
    float: left;
    width: 33px;
    height: 33px;
    background: #f7f7f7;
    border-radius: 0px;
}
.top_counter .content{
    height: 35px;
}

.top_counter .icon i {
    font-size: 14px;
    line-height: 33px;
}

.text {
    font-size: 12px;
}

h5.number {
    font-size: 14px;
    font-weight: 900;
}
 

.col-md-4.wrapDiv {
    display: flex;
    padding-right: 0;
    margin-right: -5px;
}

#table_length,
    #table1_length,
    #table2_length,
    #table3_length {
        position: absolute;
        margin-left: 100px !important;
    }

    .table tbody tr td,
    .table tbody th td {
        vertical-align: middle;
        white-space: normal !important;
    }

    ul.nav.nav-tabs-new {
        float: right !important;
        margin: auto;
    }

    .dropdown-box {
        width: 100% !important;
    }

    .dropdown-box>.inputbox {
        width: 98% !important;

    }

    .round-highlight {
    font-size: 12px;
    color: #1b301b;
    background: #bbefe8;
    border-radius: 50%;
    padding: 10px;
    float: right;
    margin-top: -5px;
    margin-left: 5px;
}

</style>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?> </h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>                            
                                <li class="breadcrumb-item active">Home / <?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>   
                        
                        
                        
                    </div>
                </div>
            <!-- Page Content Holder -->
            <div id="content">
            
            <!--   -->
            
            <div class="row clearfix">
                 
            <div id="loading"   style="display: none; position: absolute; top: 10%; left: 42%; text-align: center; z-index: 999999999999;"> <img src="<?= FRONTASSETS; ?>images/loader.gif" />
</div>

                <!-- New Card 1 Starts -->

                <div class="col-lg-3 col-md-6">
                <div class="card top_counter">
                <div class="body">
              
                <?php 
                $oncount = 0;
                $oneoi = 0;
                $onfrp = 0;
                $onfq = 0;
 
                foreach ($submittedres as $result) {
                if ($result->generate_type == "E") {
                    $oneoi = $oneoi + 1;
                }
                if ($result->generate_type == "P") {
                    $onfrp = $onfrp + 1;
                }
                if ($result->generate_type == "FQ") {
                    $onfq = $onfq + 1;
                }
                
                }
                $oncount = $oneoi + $onfrp + $onfq;
                ?>
                <h3 style="text-transform: uppercase; font-size: 16px;">Ongoing Biding   <span class="round-highlight" id="oncount"><?= $oncount;?></span></h3>
                <hr>

                <div class="row displayFlex">
                <div class="col-md-4 wrapDiv">
                <div class="icon text-info"><i class="icon-notebook"></i></div>
                <div class="content">
                <div class="text">EOI</div>
                <h5 class="number" id="oneoi"><?= $oneoi; ?></h5>
                </div>
                </div>

                
                               
                 
                <div class="col-md-4 wrapDiv">
                <div class="icon text-warning"><i class="icon-wallet"></i></div>
                <div class="content">
                <div class="text">FQ</div>
                <h5 class="number" id="onfq"><?= $onfq; ?></h5>
                </div>
                </div>

                
                               
                <div class="col-md-4 wrapDiv">
                <div class="icon text-danger"><i class="icon-frame"></i></div>
                <div class="content">
                <div class="text">RFP</div>
                <h5 class="number" id="onfrp"><?= $onfrp; ?></h5>
                </div>
                </div>


                
                </div>
                </div>
                </div>
                </div>
                
                
                <!-- New Card 1 Ends -->






               

                


                <!-- New Card 2 Starts -->

                <div class="col-lg-3 col-md-6">
                <div class="card top_counter">
                <div class="body">
                <?php
                $woneoi = 0;
                $wonfrp = 0;
                $wonfq = 0;
                $woncount = 0;
                foreach ($wonresdata as $result1) {
                    if ($result1->generate_type == "E") {
                        $woneoi = $woneoi + 1;
                    }
                    if ($result1->generate_type == "P") {
                        $wonfrp = $wonfrp + 1;
                    }
                    if ($result1->generate_type == "FQ") {
                        $wonfq = $wonfq + 1;
                    }
                    
                }
                $woncount = $woneoi + $wonfrp + $wonfq;
                ?>
                <h3 style="text-transform: uppercase; font-size: 16px;">Won   <span class="round-highlight" id="woncount"><?= $woncount;?></span></h3>
                <hr>
                 <div class="row displayFlex">
                <div class="col-md-4 wrapDiv">
                <div class="icon text-info"><i class="icon-notebook"></i> </div>
                <div class="content">
                <div class="text">EOI</div>
                <h5 class="number">N/A</h5>
                </div>
            </div>

                
                               
                 

                <div class="col-md-4 wrapDiv">
                <div class="icon text-warning"><i class="icon-wallet"></i></div>
                <div class="content">
                <div class="text">FQ</div>
                <h5 class="number" id="wonfq"><?= $wonfq; ?></h5>
                </div>
                 </div>

                
                               
                 
                <div class="col-md-4 wrapDiv">
                <div class="icon text-danger"><i class="icon-frame"></i></div>
                <div class="content">
                <div class="text">RFP</div>
                <h5 class="number" id="wonfrp"><?= $wonfrp; ?></h5>
            </div>

        </div>

               

                
                </div>
                </div>
                </div>
                </div>
                
                
                <!-- New Card 2 Ends -->


                 

                 <!-- New Card 3 Starts -->

                 <div class="col-lg-3 col-md-6">
                <div class="card top_counter">
                <div class="body">
                <?php
                $awaitcount = 0;
                $awaiteoi = 0;
                $awaitfrp = 0;
                $awaitfq = 0;
                foreach ($awaitingres as $result1) {
                if ($result1->generate_type == "E") {
                    $awaiteoi = $awaiteoi + 1;
                }
                if ($result1->generate_type == "P") {
                    $awaitfrp = $awaitfrp + 1;
                }
                if ($result1->generate_type == "FQ") {
                    $awaitfq = $awaitfq + 1;
                }
               
                }

                $awaitcount = $awaiteoi + $awaitfrp + $awaitfq;
                ?>
                <h3 style="text-transform: uppercase; font-size: 16px;">Result Awaiting   <span class="round-highlight" id="awaitcount"><?= $awaitcount;?></span></h3>
                <hr>
                 <div class="row displayFlex">
                <div class="col-md-4 wrapDiv">
                <div class="icon text-info"><i class="icon-notebook"></i> </div>
                <div class="content">
                <div class="text">EOI</div>
                <h5 class="number" id="awaiteoi"><?= $awaiteoi; ?></h5>
                </div>

                
                               
               </div>


                <div class="col-md-4 wrapDiv">

                <div class="icon text-warning"><i class="icon-wallet"></i> </div>
                <div class="content">
                <div class="text">FQ</div>
                <h5 class="number" id="awaitfq"><?= $awaitfq; ?></h5>
                </div>
</div>
                
                               
                 
                <div class="col-md-4 wrapDiv">
                <div class="icon text-danger"><i class="icon-frame"></i> </div>
                <div class="content">
                <div class="text">RFP</div>
                <h5 class="number" id="awaitfrp"><?= $awaitfrp; ?></h5>

               </div>
           </div>
                 
                
                </div>
                </div>
                </div>
                </div>
                
                
                <!-- New Card 3 Ends -->




                



                            

                 <!-- New Card 4 Starts -->

                 <div class="col-lg-3 col-md-6">
                <div class="card top_counter">
                <div class="body">
                <?php
                            $subeoi = 0;
                            $subfrp = 0;
                            $subfq = 0;
                            $subcount = 0;
                            foreach ($queryres as $result1) {
                                if ($result1->generate_type == "E") {
                                    $subeoi = $subeoi + 1;
                                }
                                if ($result1->generate_type == "P") {
                                    $subfrp = $subfrp + 1;
                                }
                                if ($result1->generate_type == "FQ") {
                                    $subfq = $subfq + 1;
                                }
                                
                            }

                            $subcount = $subeoi + $subfrp + $subfq;

                             
                            ?>
                <h3 style="text-transform: uppercase; font-size: 16px;">Submitted  <span class="round-highlight" id="subcount"><?= $subcount;?></span></h3>
                <hr>
                 <div class="row displayFlex">
                <div class="col-md-4 wrapDiv">
                <div class="icon text-info"><i class="icon-notebook"></i> </div>
                <div class="content">
                <div class="text">EOI</div>
                <h5 class="number" id="subeoi"><?= $subeoi; ?></h5>
                </div>
</div>
                
                               
                 
                <div class="col-md-4 wrapDiv">

                <div class="icon text-warning"><i class="icon-wallet"></i></div>
                <div class="content">
                <div class="text">FQ</div>
                <h5 class="number" id="subfq"><?= $subfq; ?></h5>
                </div>
</div>
                
                               
                 
                <div class="col-md-4 wrapDiv">
                <div class="icon text-danger"><i class="icon-frame"></i></div>
                <div class="content">
                <div class="text">RFP</div>
                <h5 class="number" id="subfrp"><?= $subfrp; ?></h5>
</div>

</div>
               
                 
                
                </div>
                </div>
                </div>
                </div>
                
                
                <!-- New Card 4 Ends -->




            </div>
            
        <div class="row clearfix">
                <div class="col-lg-6 col-md-6"> 
            <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table id="tableEoi" class="table table-striped display" >
                                        <thead>
                                            <tr >
                                                <th >Ongoing Biding EOI (Project) (<span id="queryreseoi" ><?= isset($queryreseoicount->eoi) ? $queryreseoicount->eoi : ''; ?></span>)
                                              



                                                <a href="<?= base_url('togoproject/ongoingeoiproject'); ?>" target="_blank" class="btn btn-info pull-right">View All</a></th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="tbody_1">
                                        
                                       
                                           
                                            
                                        </tbody>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>Project Name</th>
                                               
                                               
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                
            <!--   -->
            <div class="col-lg-6 col-md-6"> 
            <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table id="tableProp" class="table table-striped display">
                                        <thead>
                                            <tr>
                                                <th>Ongoing Biding Proposals (Project) (<span id="queryresprop"><?= isset($queryreseoicount->rfp) ? $queryreseoicount->rfp : ''; ?></span>)  <a href="<?= base_url('togoproject/ongoingeoiproject'); ?>" target="_blank" class="btn btn-info pull-right">View All</a></th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                         
                                       
                                           
                                            
                                        </tbody>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>Project Name</th>
                                               
                                               
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                
                <div class="row clearfix">
                <div class="col-lg-6 col-md-6">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table id="tableAwait" class="table table-striped display">
                                        <thead>
                                            <tr>
                                                <th>Result Awaiting RFP (Project) </th>
                                                
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                        
                                       
                                           
                                            
                                        </tbody>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>Project Name</th>
                                               
                                               
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-6 col-md-6">                 
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                <table id="tableWon" class="table table-striped display">
                                        <thead>
                                            <tr>
                                                <th>Wining List (Project) </th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        

                                        </tbody>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>Wining List (Project)</th>
                                               
                                               
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php /* ?><div class="row clearfix">
                    <!--<div class="col-lg-3 col-md-6">
                        <div class="social-box facebook">
                            <?php 
                            $oncount = 0;
                            $oneoi = 0;
                            $onfrp = 0;
                            $onfq = 0;
                            // echo "<pre>"; print_r($submittedres); die;
                            foreach ($submittedres as $result) {
                                if ($result->generate_type == "E") {
                                    $oneoi = $oneoi + 1;
                                }
                                if ($result->generate_type == "P") {
                                    $onfrp = $onfrp + 1;
                                }
                                if ($result->generate_type == "FQ") {
                                    $onfq = $onfq + 1;
                                }
                                $oncount = $oneoi + $onfrp + $onfq;
                            }
                            ?>
                                
                            <span class="act">Ongoing Biding<br/><?=  $oncount; ?></span>
                            <span class="col-md-4 spandiv">EOI <br> <?= $oneoi; ?></span>
                            <span class="col-md-4 spandiv">FQ <br> <?= $onfq; ?></span>
                            <span class="col-md-4 spandiv">RFP <br> <?= $onfrp; ?></span>

                        </div>
                    </div>-->
                    
                    <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <div class="row">
                                <div class="col-8">
                                <?php 
                            $oncount = 0;
                            $oneoi = 0;
                            $onfrp = 0;
                            $onfq = 0;
                            // echo "<pre>"; print_r($submittedres); die;
                            foreach ($submittedres as $result) {
                                if ($result->generate_type == "E") {
                                    $oneoi = $oneoi + 1;
                                }
                                if ($result->generate_type == "P") {
                                    $onfrp = $onfrp + 1;
                                }
                                if ($result->generate_type == "FQ") {
                                    $onfq = $onfq + 1;
                                }
                                $oncount = $oneoi + $onfrp + $onfq;
                            }
                            ?>
                                    <h6 class="m-t-2">Ongoing Biding</h6><br>
                                    <small class="text-small">EOI</small>(<b><?= $oneoi; ?></b>)<br>
                                    <small class="text-small">FQ</small>(<b><?= $onfq; ?></b>)<br>
                                    <small class="text-small">RFP</small>(<b><?= $onfrp; ?></b>)<br>
                                </div>
                                <div class="col-4 text-right">
                                    <h3 class="m-b-0"><?=  $oncount; ?></h3>
                                    <!--<small class="info">of 1Tb</small>-->
                                </div><br>
                                <div class="col-12 body">
                                     <ul class="list-unstyled list-referrals"> <li>
                                    <p><span class="value">2301</span><span class="text-muted float-right">visits from Facebook</span></p>
                                    <div class="progress progress-xs progress-transparent custom-color-blue">
                                        <div class="progress-bar" data-transitiongoal="20"></div>
                                    </div>
                                </li></ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    
                    
                    
                    
                    <div class="col-lg-3 col-md-6">
                        <div class="social-box twitter">
                            <span class="act">Won<br/><?= $woncount; ?></span>
                            <?php
                            $woneoi = 0;
                            $wonfrp = 0;
                            $wonfq = 0;
                            foreach ($wonresdata as $result1) {
                                if ($result1->generate_type == "E") {
                                    $woneoi = $woneoi + 1;
                                }
                                if ($result1->generate_type == "P") {
                                    $wonfrp = $wonfrp + 1;
                                }
                                if ($result1->generate_type == "FQ") {
                                    $wonfq = $wonfq + 1;
                                }
                            }
                            ?>
                            
                            <span class="col-md-4 spandiv">FQ <br> <?= $wonfq; ?></span>
                            <span class="col-md-4 spandiv">&nbsp; <br/>&nbsp;</span>
                            <span class="col-md-4 spandiv">RFP <br> <?= $wonfrp; ?></span>

                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="social-box linkedin">
                            <?php
                            $awaitcount = 0;
                            $awaiteoi = 0;
                            $awaitfrp = 0;
                            $awaitfq = 0;
                            foreach ($awaitingres as $result1) {
                                if ($result1->generate_type == "E") {
                                    $awaiteoi = $awaiteoi + 1;
                                }
                                if ($result1->generate_type == "P") {
                                    $awaitfrp = $awaitfrp + 1;
                                }
                                if ($result1->generate_type == "FQ") {
                                    $awaitfq = $awaitfq + 1;
                                }
                                $awaitcount = $awaiteoi + $awaitfrp + $awaitfq;
                            }
                            ?>
                            <!--<span class="act">Result Awaiting<br/><?= $awatingcount; ?></span>-->
                            <span class="act">Result Awaiting<br/><?= $awaitcount; ?></span>
                            <span class="col-md-4 spandiv">EOI <br> <?= $awaiteoi; ?></span>
                            <span class="col-md-4 spandiv">FQ <br> <?= $awaitfq; ?></span>
                            <span class="col-md-4 spandiv">RFP <br> <?= $awaitfrp; ?></span>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="social-box google-plus">
                            <span class="act"> Submitted <br/><?= $submitcount; ?></span>
                            <?php
                            $subeoi = 0;
                            $subfrp = 0;
                            $subfq = 0;
                            foreach ($queryres as $result1) {
                                if ($result1->generate_type == "E") {
                                    $subeoi = $subeoi + 1;
                                }
                                if ($result1->generate_type == "P") {
                                    $subfrp = $subfrp + 1;
                                }
                                if ($result1->generate_type == "FQ") {
                                    $subfq = $subfq + 1;
                                }
                            }
                            ?>
                            <span class="col-md-4 spandiv">EOI <br> <?= $subeoi; ?></span>
                            <span class="col-md-4 spandiv">FQ <br> <?= $subfq; ?></span>
                            <span class="col-md-4 spandiv">RFP <br> <?= $subfrp; ?></span>
                        </div>
                    </div>
                </div>

                <div class="row"><br></div>
                <div class="row-fluid" style="margin-top: 10px;">
                    <div class="span6">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Ongoing Biding EOI (<?= isset($queryreseoicount->eoi) ? $queryreseoicount->eoi : ''; ?>)</h3>
                                <div class="box-tools pull-right">
                                        <!--<a href="<?= base_url('eoiproject'); ?>" target="_blank">View All</a>-->
                                    <a href="<?= base_url('togoproject/ongoingeoiproject'); ?>" target="_blank">View All</a>
                                </div>
                            </div>
                            <div class="box-body chart-responsive">
                                <table class="table table-bordered table-striped">
                                    <tbody>
                                        <?php
                                        if (!empty($eoi)) {
                                            foreach ($eoi as $value) {
                                                echo '<tr>
                                                    <td>' . $value['TenderDetails'] . '<br/><br/>' . $value['Created_Date'] . '</td>
                                                </tr>';
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>                  
                        
                        
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Result Awaiting RFP</h3>
                                <div class="box-tools pull-right">
                                    <a href="#" target="_blank">View All</a>
                                </div>
                            </div>
                            <div class="box-body chart-responsive">
                                <table class="table table-bordered table-striped">
                                    <tbody>
                                        <?php
                                        if (!empty($resultawaitrfp)) {
                                            foreach ($resultawaitrfp as $value) {
                                                echo '<tr>
                                                    <td>' . $value['TenderDetails'] . '<br/><br/>' . $value['Created_Date'] . '</td>
                                                </tr>';
                                            }
                                        }
                                        ?>   
                                    </tbody></table>
                            </div>
                        </div>

                    </div>
                    <div class="span6">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Ongoing Biding Proposals  (<?= isset($queryreseoicount->rfp) ? $queryreseoicount->rfp : ''; ?>) </h3>
                                <div class="box-tools pull-right">
                                    <a href="#">View All</a>
                                </div>
                            </div>
                            <div class="box-body chart-responsive">
                                <table class="table table-bordered table-striped">
                                    <tbody>
                                        <?php
                                        if (!empty($rfp)) {
                                            foreach ($rfp as $value) {
                                                echo '<tr>
                                                <td>' . $value['TenderDetails'] . '<br/><br/>' . $value['Created_Date'] . '</td>
                                            </tr>';
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Wining List</h3>

                                <div class="box-tools pull-right">
                                    <a href="#" target="_blank">View All</a>
                                </div>
                            </div>
                            <div class="box-body chart-responsive">
                                <table class="table table-bordered table-striped">
                                    <tbody>
                                        <?php
                                        // echo '<pre>'; print_r($resultwonrfp); die;
                                        if (!empty($resultwonrfp)) {
                                            foreach ($resultwonrfp as $value) {
                                                echo '<tr>
                                                    <td>' . $value['TenderDetails'] . '<br/><br/>' . $value['Created_Date'] . '</td>
                                                </tr>';
                                            }
                                        }
                                        ?>
                                    </tbody></table>
                            </div>
                        </div>
                        <?php */?>
                    </div>
                </div>    
            </div>
        </div>
        </div>

        <input type="hidden" id="dashboardVal" name="dashboardVal" value="1">

        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>
<script>
    $(document).ready(function () {

        

        
 
        $("li#dashboard").addClass('active');
       
        dashboardAnalyticsOn();
        dashboardAnalyticsWon();
        dashboardAnalyticsSub();
        dashboardAnalyticsAwait();






        });



 
        
       
      
    




    function dashboardAnalyticsOn() {
        var oneoi = 0;
        var onfrp = 0;
        var onfq = 0;
 

        $.ajax({
            type: 'GET',
            url: "<?= base_url('Dashboard/dashboardAnalyticsOn'); ?>",
            dataType: 'json',
             
            success: function(responData) {
              

                $.each(responData.submittedres, function(i, item) {
                    if(responData.submittedres[i].generate_type === "E"){
                        oneoi = oneoi + 1;
                        
                    }
                    if(responData.submittedres[i].generate_type === "P"){
                        onfrp = onfrp + 1;
                        
                    }
                    if(responData.submittedres[i].generate_type === "FQ"){
                        onfq = onfq + 1;
                    }
        
                });

                $("#oneoi").html(oneoi);
                $("#onfrp").html(onfrp);
                $("#onfq").html(onfq);
                var oncount = parseInt(oneoi) + parseInt(onfrp) + parseInt(onfq);
                $("#oncount").html(oncount);

               
                $("#queryreseoi").html(responData.queryreseoicount.eoi);
                $("#queryresprop").html(responData.queryreseoicount.rfp);
              

            },
        });
    }


    function dashboardAnalyticsWon() {
      

        var woneoi = 0;
        var wonfrp = 0;
        var wonfq = 0;
 
        $.ajax({
            type: 'GET',
            url: "<?= base_url('Dashboard/dashboardAnalyticsWon'); ?>",
            dataType: 'json',
             
            success: function(responData) {
               

                $.each(responData.wonresdata, function(i, item) {
                    if(responData.wonresdata[i].generate_type === "E"){
                        woneoi = woneoi + 1;
                        
                    }
                    if(responData.wonresdata[i].generate_type === "P"){
                        wonfrp = wonfrp + 1;
                        
                    }
                    if(responData.wonresdata[i].generate_type === "FQ"){
                        wonfq = wonfq + 1;
                    }
        
                });

                $("#woneoi").html(woneoi);
                $("#wonfrp").html(wonfrp);
                $("#wonfq").html(wonfq);
                var woncount = parseInt(woneoi) + parseInt(wonfrp) + parseInt(wonfq);
                $("#woncount").html(woncount);
  

            },
        });
    }

    

    function dashboardAnalyticsSub() {
        
        
        var subeoi = 0;
        var subfrp = 0
        var subfq = 0

    

        $.ajax({
            type: 'GET',
            url: "<?= base_url('Dashboard/dashboardAnalyticsSub'); ?>",
            dataType: 'json',
             
            success: function(responData) {
                
 
                $.each(responData.queryres, function(i, item) {
                    if(responData.queryres[i].generate_type === "E"){
                        subeoi = subeoi + 1;
                        
                    }
                    if(responData.queryres[i].generate_type === "P"){
                        subfrp = subfrp + 1;
                        
                    }
                    if(responData.queryres[i].generate_type === "FQ"){
                        subfq = subfq + 1;
                    }
        
                });

                $("#subeoi").html(subeoi);
                $("#subfrp").html(subfrp);
                $("#subfq").html(subfq);
                var subcount = parseInt(subeoi) + parseInt(subfrp) + parseInt(subfq);
                $("#subcount").html(subcount);
             

            },
        });
    }

   

    function dashboardAnalyticsAwait() {
       

        var awaiteoi = 0;
        var awaitfrp = 0;
        var awaitfq = 0;
 

    

        $.ajax({
            type: 'GET',
            url: "<?= base_url('Dashboard/dashboardAnalyticsAwait'); ?>",
            dataType: 'json',
             
            success: function(responData) {
                
 

                    $.each(responData.awaitingres, function(i, item) {
                        if(responData.awaitingres[i].generate_type === "E"){
                            awaiteoi = awaiteoi + 1;
                            
                        }
                        if(responData.awaitingres[i].generate_type === "P"){
                            awaitfrp = awaitfrp + 1;
                            
                        }
                        if(responData.awaitingres[i].generate_type === "FQ"){
                            awaitfq = awaitfq + 1;
                        }

                    });

                    $("#awaiteoi").html(awaiteoi);
                    $("#awaitfrp").html(awaitfrp);
                    $("#awaitfq").html(awaitfq);
                    var awaitcount = parseInt(awaiteoi) + parseInt(awaitfrp) + parseInt(awaitfq);
                    $("#awaitcount").html(awaitcount);
 
                   
                  

        // Datatables Starts here

        var tableEoi = $('#tableEoi').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": false, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url('Dashboard/tableEoi') ?>",
                    "type": "POST",
                    "data": function(data) {
                        data.sectorinput = 1;
                         
                    },
                },
                "dom": 'lBfrtip',
                "buttons": [{
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        'copy',
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ]

                }],
                "columnDefs": [{
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                }, ],
                "aLengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
            });
     



   var tableProp = $('#tableProp').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": false, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url('Dashboard/tableProp') ?>",
                    "type": "POST",
                    "data": function(data) {
                        data.sectorinput = 1;
                         
                    },
                },
                "dom": 'lBfrtip',
                "buttons": [{
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        'copy',
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ]

                }],
                "columnDefs": [{
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                }, ],
                "aLengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
            });
    

          

        var tableAwait = $('#tableAwait').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": false, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url('Dashboard/tableAwait') ?>",
                    "type": "POST",
                    "data": function(data) {
                        data.sectorinput = 1;
                         
                    },
                },
                "dom": 'lBfrtip',
                "buttons": [{
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        'copy',
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ]

                }],
                "columnDefs": [{
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                }, ],
                "aLengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
            });


            var tableWon = $('#tableWon').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": false, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url('Dashboard/tableWon') ?>",
                    "type": "POST",
                    "data": function(data) {
                        data.sectorinput = 1;
                         
                    },
                },
                "dom": 'lBfrtip',
                "buttons": [{
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        'copy',
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ]

                }],
                "columnDefs": [{
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                }, ],
                "aLengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
            });


        // Datatables Ends here
                

            },
        });
    }





    
        
    
</script>
<script>

</script>

 

<?php

function stringShort($string2) {
    $string = str_replace("_x000D_", "<br>", strip_tags($string2));
    if (strlen($string) > 75) {
        $stringCut = substr($string, 0, 75);
        $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . '... <a href="#"> View More..</a>';
    }
    echo $string;
}

//include('include/customjs_last.php');
?>
