<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
		
        ?>
		<script src="<?= FRONTASSETS; ?>ckeditor/ckeditor.js"></script>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div> 
                    </div>
                </div>
				<?php if ($this->session->flashdata('successfile')) { ?>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> <?= $this->session->flashdata('successfile'); ?>
                            </div>
                        <?php } ?>

                        <?php if ($this->session->flashdata('errmsg')) { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Error !</strong> <?= $this->session->flashdata('errmsg'); ?>
                            </div>
                        <?php } ?>
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="accordion" id="accordion">
                                    <div>
									<form action="<?= base_url('tender/uploadtndrtwentyfourcsv'); ?>" method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                                        <!--<div class="card-header" id="headingOne">-->
                                            <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-6">
                                                        <label>Tender Type :</label>
                                                        <div class="input-group mb-3">
                                                            
                                                          <select name="tndrType" class="form-control" data-placeholder="Tender/Project  Type" id="selectError2" data-rel="chosen">
                                           
                                                          <option value="New_project">New Tender </option>
															<option value="Active_project">Active Project</option>
															<option value="Important_project">Important Project</option>
															<option value="In_Review_project">In Review Project</option>
                                           
                                                          <!-- <option value="1">New Tender </option> -->
                                            <!--                                            <option value="2">Active Project</option>
                                                                                        <option value="3">Important Project</option>
                                                                                        <option value="4">To Go Project</option>
                                                                                        <option value="5">Bid Project</option>  -->
                                        </select>
                                                        </div>
                                                    </div>
													<div class="col-lg-3 col-md-6">
                                                        <label>Tender24 Csv File :</label>
                                                        <div class="input-group mb-3"><br>
															<input type="file" required="required" class="form-control" name="tenderinfocsv" > 
                                                        </div>
                                                    </div>
													<div class="col-lg-3 col-md-6">
                                                        <label>Tender24 Url / Link :</label>
                                                        <div class="input-group mb-3"><br>
															<input type="file" required="required" class="form-control" name="tenderlink" > 
                                                        </div>
                                                    </div>
													
													
													<div class="col-lg-2 col-md-6">
                                                        
														<div class="mb-2">
                                                            <label>&nbsp;</label>
															<button type="submit"  class="btn btn-info mt-4 pull-right">Submit</button>
                                                        </div>
                                                    </div>
													
													
                                                </div>
                                        <!--</div> -->                               
                                        
										</form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>
