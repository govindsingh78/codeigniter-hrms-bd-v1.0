<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        <div class="wrapper">
            <?php $this->load->view('include/sidebar'); ?>
            <div id="content" class="col-lg-10 col-sm-10">
                <!-- content starts -->
                <div>
                    <ul class="breadcrumb">
                        <li> <a href="<?= base_url(); ?>">Home</a></li>
                        <li>
                            <a href="#">Teamrequisition Details </a>
                        </li>
                    </ul>
                </div>

                <div class="row">
                    <div class="box col-md-12">

                        <?php if ($this->session->flashdata('msg')) { ?>
                            <div class="alert alert-success alert-dismissable" style="">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><div id="msgs"> Success ! </div></strong> <?= $this->session->flashdata('msg'); ?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('error_msg')) { ?>
                            <div class="alert alert-danger alert-dismissable" style="">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><div id="msgs"> Error ! </div></strong> <?= $this->session->flashdata('error_msg'); ?>
                            </div>
                        <?php } ?>

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form id="form-filter" class="form-horizontal">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label class="email"> Sector : </label>
                                            <select name="sectorinput" id="sectorinput"  class="form-control">
                                                <option <?= ($secId == "") ? 'Selected' : ''; ?> value="">--All--</option>
                                                <?php
                                                if ($sectorArr):
                                                    foreach ($sectorArr as $row) {
                                                        ?>
                                                        <option <?= ($secId == $row->fld_id) ? 'Selected' : ''; ?> value="<?= $row->fld_id; ?>"><?= $row->sectName; ?></option>
                                                    <?php } endif; ?>
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Filter : </label>
                                            <select id="company_name" class="form-control">
                                                <option value="" selected="selected">--All--</option>
                                                <option value="E">EOI</option>
                                                <option value="P">RFP</option>
                                                <option value="FQ">FQ</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Status Time : </label>
                                            <select id="tndr_status" name="tndr_status" class="form-control" >
                                                <option value="" selected="selected">--All--</option>
                                                <option value="To_Go_project">To Be Submited</option>
                                                <option value="Bid_project">Submited</option>
                                                <option value="3">Not Required</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="email"> &nbsp; </label> <br>
                                            <button type="button" id="btn-filter" class="btn btn-primary"> Filter </button> &nbsp;&nbsp;&nbsp;
                                            <button type="button" id="btn-reset" class="btn btn-default"> Reset </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                    <!-- open Phase Section-->
                    <div class="box-inner">
                        <div class="box-content">
                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="colvis"></div>
                                            <div class="form-group has-success col-md-2">
                                                <button type="button" class="btn-info" data-toggle="modal" data-target="#Designation">Add More Designation</button>
                                            </div>
                                        </div>
                                    </div>
                                    <table id="table" class="display" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Tender Name</th>
                                                <th>Tender ID</th>
                                                <th>Clone</th>
                                                <th>Location</th>
                                                <th>Client</th>
                                                <?php if (bd_rolecheck() != 1) { ?>
                                                    <th>Proposal Manager</th>
                                                <?php } ?>
                                                <th>Status </th>
                                                <th>Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Tender Name</th>
                                                <th>Tender ID</th>
                                                <th>Clone</th>
                                                <th>Location</th>
                                                <th>Client</th>
                                                <?php if (bd_rolecheck() != 1) { ?>
                                                    <th>Proposal Manager</th>
                                                <?php } ?>
                                                <th>Status </th>
                                                <th>Actions </th>
                                            </tr>
                                        </tfoot>
                                        <hr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Close phase Section-->
            </div>
        </div>

        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>
        <hr>




        <div class="modal fade teamclone" id="teamclone" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 id="tender_title" class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <form  action="<?= base_url('teamrequisition/cloneteamreq'); ?>" method="post">
                            <div class="box-content">
                                <div class="controls col-md-12">
                                    <label>Select Year</label>
                                    <select required="required" class="form-control"  name="years" id="years">
                                        <option value="">--select--</option>
                                        <?php
                                        $year = '2022';
                                        for ($i = '2016'; $i <= $year; $i++) {
                                            ?>
                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                            <?php
                                            //$year ++;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="box-content">
                                <div class="controls col-md-12">
                                    <label>Select Project</label>
                                    <select required="required" class="form-control project_copy"  name="project_copy" id="project_copy">
                                        <option value="">--select--</option>
                                    </select>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                            <div class="box-content">
                                <div class="controls col-md-6">
                                    <input type="hidden" id="teamprojid" name="teamprojid" value="" class="form-control">
                                    <input type="submit"  value="Submit" class="btn btn-success glyphicon glyphicon-user" >
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                            <!--<div class="modal-footer">
                                    <button class="btn btn-default" data-dismiss="modal">X</button>
                            </div>-->
                        </form>
                    </div>
                </div>
            </div>
        </div>



        <!-- Add  Designation -->
        <div class="modal fade" id="Designation" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" style="color:red" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Designation </h4>
                    </div>

                    <div class="modal-body">
                        <form method="post" id="designations_add" action="<?= base_url("teamrequisition/addnewdesignation"); ?>">
                            <div class="box-content">
                                <div class="form-group">
                                    <label class="email"> Select Sector : </label>
                                    <select required="required" onchange="categsetbysector()" class="form-control" name="sector_id" id="sector_id">
                                        <option value=""> --select-- </option>
                                        <?php foreach ($SectorDesigArr as $rowrec) { ?>
                                            <option value="<?= $rowrec->id; ?>"><?= $rowrec->sector_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group" id="common_categ">
                                    <label class="email" > Select Category : </label>
                                    <select required="required" class="form-control" name="addcat_ids" id="addcat_ids"></select>
                                </div>
                                <div class="form-group">
                                    <label for="email"> Designation Name : </label>
                                    <input required="required" type="text" autocomplete="off" class="form-control" name="designation" id="designation">
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-success" id="submit" name="Add" value="Add">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



        <!-- Assign Project Manager Modal Popup -->
        <div class="modal fade assignteam" id="assignteam" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" id="reqrd" data-dismiss="modal">&times;</button>
                        <h4 id="tender_title" class="modal-title"> Add New Team  dd </h4>
                    </div>

                    <div class="modal-body">
                        <form action="<?= base_url('teamrequisition/assignteamreq'); ?>" method="post">
                            <div class="box-content">
                                <div class="box-content">
                                    <div class="form-group">
                                        <label class="email"> Select Sector : <span id="reqrd"> * </span> </label>
                                        <select required="required" onchange="categsetbysector_team()" class="form-control sector_ids"  name="sector_ids" id="sector_ids">
                                            <option value="">--select--</option>
                                            <?php foreach ($SectorDesigArr as $rowrec) { ?>
                                                <option value="<?php echo $rowrec->id; ?>"><?php echo $rowrec->sector_name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="email"> Select Category : <span id="reqrd"> * </span> </label>
                                        <select required="required" class="form-control" onchange="setdesignationdropdown()" name="cat_ids" id="cat_ids"></select>
                                    </div>

                                    <div class="form-group">
                                        <label class="email"> Select Designation : <span id="reqrd"> * </span></label>
                                        <select required="required" class="form-control designation_id" name="designation_ofsec" id="designation_ofsec">
                                            <option value="">--select--</option>
                                        </select>
                                    </div>

                                    <div class="form-group" id="metroproj_position_iddiv" style="display:none">
                                        <label class="email"> Project position ID : <span id="reqrd"> * </span></label>
                                        <input type="text" name="proj_position_idmetro" id="proj_position_idmetro" class="form-control">
                                    </div>


                                    <div class="box-content designation_detail">

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label class="email" > Age Limit : </label>
                                                <input type="number" id="age_limit" name="age_limit" value="65" class="form-control">
                                            </div>
                                            
                                            <div class="col-sm-4">
                                                <label class="email"> Total Man Months : </label>
                                                <input type="number" step="any" id="man_months" name="man_months" value="" class="form-control">
                                            </div>
                                            
                                            <div class="col-sm-5">
                                                <label class="email"> Weightage Marks (RFP) : </label>
                                                <input type="number" step="any" id="weightage_marks_rfp" name="weightage_marks_rfp" value="" class="form-control">
                                            </div>
                                            
                                            <div class="col-sm-12">
                                                <label class="email"> Comment : </label>
                                                <textarea class="form-control" rows="2" cols="10" id="single_comment" name="single_comment"></textarea>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="box-content">
                                <div class="controls col-md-6">
                                    <input type="hidden" id="assignteamprojid" name="project_id" value="" class="form-control">
                                    <input type="hidden" id="urlchk" name="url_chk" value="1">
                                    <input type="hidden" id="assignpropmangridSEC" name="assignpropmangrid" value="" class="form-control">
                                    <input type="submit"  value="Submit" class="btn btn-success glyphicon glyphicon-user" >	
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>




        <!-- BootStrap Model For Comments.. -->
        <?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->
        <?php $this->load->view('include/footer'); ?>
        <?php $this->load->view('include/datatablejs'); ?>
        <style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;}
            .designation_detail{display:none;}
        </style>

        <script type="text/javascript">
            $('.close').click(function () {
                $('.assignteam').hide();
                location.reload(1);
            });

            $(document).on("change", "#sector_ids", function () {
                var id = $(this).val();
                var cat_ids = $('#cat_ids').val();
                $.ajax({
                    type: 'POST',
                    data: {'id': id, 'cat_ids': cat_ids},
                    url: "<?= base_url('teamrequisition/getDesignation'); ?>",
                    success: function (data) {
                        $('.designation_id').html(data);
                    }
                });
            });

            $(document).on("change", "#years", function () {
                var year = $(this).val();
                $.ajax({
                    type: 'post',
                    url: "<?= base_url('teamrequisition/getProject'); ?>",
                    data: {'year': year},
                    success: function (data) {
                        $('.project_copy').html(data);
                    }
                });
            });

            $(document).ready(function () {
                $("li#teamrequisition").addClass('active');
            });

            $(document).on("change", "#designation_name", function () {
                $('.designation_detail').css('display', 'block');
            });

            $(document).on("change", "#designation_ofsec", function () {
                $('.designation_detail').css('display', 'block');
            });

            $(document).ready(function () {
                $("#checkAll").click(function () {
                    $('input:checkbox').not(this).prop('checked', this.checked);
                });
            });
            
            function assignteam(projid, projmangrid, genr_tndrid) {
                
                $("#assignteamprojid").val(projid);
                $("#assignpropmangrid").val(projmangrid);
                $("#genr_tndrid").val(genr_tndrid);
                
            }
            
            function teamclone(projid) {
                $("#teamprojid").val(projid);
            }
            var table;
            $(document).ready(function () {
                //datatables
                table = $('#table').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "order": [],
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo site_url('teamrequisition/newProjectAll') ?>",
                        "type": "POST",
                        "data": function (data) {
                            data.project_name = $('#company_name').val();
                            data.sectorinput = $('#sectorinput').val();
                            data.tndr_status = $('#tndr_status').val();
                        },
                    },
                    "dom": 'lBfrtip',
                    "buttons": [{
                            extend: 'collection',
                            text: 'Export',
                            buttons: ['copy', 'excel', 'csv', 'pdf', 'print']
                        }
                    ],
                    //Set column definition initialisation properties.
                    "columnDefs": [{"targets": [0], "orderable": false, }, ],
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                });

                var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
                $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
                $('#btn-filter').click(function () { //button filter event click
                    table.ajax.reload();  //just reload table
                });
                $('#btn-reset').click(function () { //button reset event click
                    $('#form-filter')[0].reset();
                    table.ajax.reload();  //just reload table
                });
            });

            function setnotrequired(sctid) {
                if (confirm("Are You Sure No Team Required ? ")) {
                    window.location = "<?php echo base_url('teamrequisition/setnotreq/'); ?>/" + sctid;
                }
                $('<div></div>').appendTo('body').html('<div><h6>' + message + '?</h6></div>').dialog({
                    modal: true, title: 'Delete message', zIndex: 10000, autoOpen: true,
                    width: 'auto', resizable: false,
                    buttons: {
                        Yes: function () {
                            $('body').append('<h1>Confirm Dialog Result: <i>Yes</i></h1>');
                            $(this).dialog("close");
                        },
                        No: function () {
                            $('body').append('<h1>Confirm Dialog Result: <i>No</i></h1>');
                            $(this).dialog("close");
                        }
                    },
                    close: function (event, ui) {
                        $(this).remove();
                    }
                });
            }


            function categsetbysector() {
                var sectID = $("#sector_id").val();

                if (sectID) {
                    $('select[name="addcat_ids"]').empty();
                    $.ajax({
                        url: '<?= base_url('teamrequisition/setdesignationcateg_ajax'); ?>/' + sectID,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            $('select[name="addcat_ids"]').empty();
                            $('select[name="addcat_ids"]').append('<option value="">-- Select Category --</option>');
                            $.each(data, function (key, value) {
                                $('select[name="addcat_ids"]').append('<option value="' + value.k_id + '">' + value.ktype_name + '</option>');
                            });
                        }
                    });
                }
            }


            function categsetbysector_team() {
                var sectID = $("#sector_ids").val();
                if (sectID) {
                    $('select[name="cat_ids"]').empty();
                    $.ajax({
                        url: '<?= base_url('teamrequisition/setdesignationcateg_ajax'); ?>/' + sectID,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            $('select[name="cat_ids"]').empty();
                            $('select[name="cat_ids"]').append('<option value="">-- Select Category --</option>');
                            $.each(data, function (key, value) {
                                $('select[name="cat_ids"]').append('<option value="' + value.k_id + '">' + value.ktype_name + '</option>');
                            });
                        }
                    });
                }
            }

            function setdesignationdropdown() {
                var sectrID = $("#sector_ids").val();
                var categrID = $("#cat_ids").val();
                $('select[name="designation_ofsec"]').empty();
                if (sectrID && categrID) {
                    $.ajax({
                        url: '<?= base_url('teamrequisition/setdesignationcateg_teamaddajax'); ?>',
                        type: "POST",
                        dataType: "json",
                        data: {'sectrID_id': sectrID, 'categrID_id': categrID},
                        success: function (data) {
                            $('select[name="designation_ofsec"]').append('<option value="">-- Select Designation --</option>');
                            $.each(data, function (key, value) {
                                $('select[name="designation_ofsec"]').append('<option value="' + value.fld_id + '">' + value.designation_name + '</option>');
                            });
                        }
                    });
                }
            }


        </script>
    </div>
</body>
</html>