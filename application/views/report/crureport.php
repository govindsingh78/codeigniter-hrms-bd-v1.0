<?php
error_reporting(0);
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');

?>
<style>
#table_length {
    position: absolute;
    margin-left: 100px !important;
}
.table tbody tr td, .table tbody th td {
    vertical-align: middle;
    white-space: normal !important;
}
</style>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row clearfix">
                    <div class="col-lg-12">

                        <?php if ($this->session->flashdata('msg')) { ?>

                            <!-- <script>
                            toastr.success(<?//= //$this->session->flashdata('msg'); ?> , 'Message', {timeOut: 5000});
                            </script> -->
                            <div class="alert alert-success alert-dismissable" style="">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>
                                    <div id="msgs"> Success ! 
                                </strong> <?= $this->session->flashdata('msg'); ?></div>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('error_msg')) { ?>
                            <div class="alert alert-danger alert-dismissable" style="">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>
                                    <div id="msgs"> Error !
                                </strong> <?= $this->session->flashdata('error_msg'); ?> </div>
                            </div>
                        <?php } ?>




                            <div class="row clearfix">
                                <div class="col-lg-12">
                    <div class="card">
                        
                        <div class="body">
						
                        <div class="table-responsive"> 
                                   
                        <div class="col-md-12">
                                    <div id="colvis"></div>
                                    <div class="form-group has-success pull-right"> 
                                        
                                    </div>
                                </div>
                                    
                               
                                    <table id="table" class="table table-striped display" cellspacing="0" width="100%">                  
                                <thead>
                                    <tr>
                                        <th>Sr </th>
                                        <th>Key Personnel</th>
                                        <th>Age</th>
                                        <th>
                                            Proposed In Project
                                            <table  class="table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th width="50px">Total</th>
                                                        <th width="50px">Awaiting</th>
                                                        <th width="50px">Won</th>
                                                        <th width="50px">Lost</th>
                                                        <th width="50px">Cancel</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </th>
                                        <th>Position</th>
                                        <th>Inhouse / Outside</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    if (!empty($list)) {
                                        $i = 0;
                                        foreach ($list as $datas) {
                                            if ($datas->empname == '' && $datas->empnameother == '') {
                                                
                                            } else {
                                                if (!empty($datas->emp_name)) {
                                                    $name = $datas->emp_name;
                                                    $house = 'Outside';
                                                } else {
                                                    $name = $datas->userfullname;
                                                    $house = 'Inhouse';
                                                }

                                                if (!empty($datas->empname)) {
                                                    $userid = $datas->empname;
                                                    $res = $this->Front_model->getProjectstatus('1', $userid);
                                                } else {
                                                    $userid = $datas->empnameother;
                                                    $res = $this->Front_model->getProjectstatus('2', $userid);
                                                }
                                                // echo '<pre>';
                                                // print_r($res); 
                                                /* foreach($res as $reschk){
                                                  $res11 = $this->Front_model->getProjectstatuschk($reschk->project_id);
                                                  //print_r($reschk->project_id);
                                                  //print_r($res11);
                                                  }
                                                  print_r($res11); */
                                                ?>  
                                                <tr>
                                                    <td><?php echo $i = $i + 1; ?></td>
                                                    <td>

                                                        <?php if ($house == 'Inhouse') { ?>
                                                            <span style="cursor:pointer;color:blue;" onclick="empID(<?= $userid ?>)" data-toggle="modal" data-target="#myModalposition"><?php echo $name; ?></span>
                                                        <?php } else { ?>
                                                            <span style="cursor:pointer;color:blue;" onclick="empIDother(<?= $userid ?>)" data-toggle="modal" data-target="#myModalposition"><?php echo $name; ?></span>
                                                        <?php } ?>
                                                    </td>
                                                    <td><?php echo $datas->age_limit; ?></td>
                                                    <td>
                                                        <table class="table-bordered">
                                                            <tbody> 
                                                                <tr>
                                                                    <td width="75px"><?php echo $datas->total_project; ?></td>
                                                                    <?php
                                                                    foreach ($res['status'] as $reschk) {
                                                                        if ($reschk['awaiting'] != '') {
                                                                            echo '<td width="67px">' . $reschk['awaiting'] . '</td>';
                                                                        } else {
                                                                            echo '<td width="67px"></td>';
                                                                        }
                                                                        if ($reschk['won'] != '') {
                                                                            echo '<td width="67px">' . $reschk['won'] . '</td>';
                                                                        } else {
                                                                            echo '<td width="67px"></td>';
                                                                        }
                                                                        if ($reschk['lose'] != '') {
                                                                            echo '<td width="67px">' . $reschk['lose'] . '</td>';
                                                                        } else {
                                                                            echo '<td width="67px"></td>';
                                                                        }
                                                                        if ($reschk['cancel'] != '') {
                                                                            echo '<td width="67px">' . $reschk['cancel'] . '</td>';
                                                                        } else {
                                                                            echo '<td width="67px"></td>';
                                                                        }
                                                                        ?>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                    </td>
                                                    <td>
                                                        <table>
                                                            <tbody> 
                                                                <?php
                                                                foreach ($res['result'] as $res1) {
                                                                    if (!empty($res1->empname)) {
                                                                        $designation = $res1->designation_name;
                                                                    } else {
                                                                        $designation = $res1->designation_name;
                                                                    }
                                                                    ?>
                                                                    <tr>
                                                                        <td><?= $designation; ?></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <table>
                                                            <tbody> 
                                                                <?php
                                                                /* foreach($res as $res1){
                                                                  if(!empty($res1->empname)){
                                                                  $house = 'Inhouse';
                                                                  }else{
                                                                  $house = 'Outside';
                                                                  } */
                                                                ?>
                                                                <tr>
                                                                    <td><?= $house; ?></td>
                                                                </tr>
                                                                <?php
                                                                //}
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </tbody>   
                            </table>
							</div>
                        </div>
                    </div>
                </div>
            </div>




                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModalposition" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Project Status</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row"><div class="col-md-12"><div class="table-responsive p-2"><table class="table table-striped display"  ><tbody id="table_add">
                         </tbody></table></div></div></div>
                    </div>
                </div>
            </div>
        </div>

    <?php $this->load->view('admin/includes/footer'); ?>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?= FRONTASSETS; ?>js/jquery.multiselect.js"></script>
    <link href="<?= FRONTASSETS; ?>bower_components/chosen/chosen.css" rel="stylesheet"></link>
    <script src="<?= FRONTASSETS; ?>bower_components/chosen/chosen.jquery.js"></script>
    </div>
   <style>
    table{
        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
        font-size: 14px;
    }
    #example_filter{display:none;}
</style>
<script>
    // For Inhouse.. Emp..
    function empID(id) {
        //alert("testing")
        if (id != '') {
            var url = '<?= base_url('report/getProjectDetailByUserID') ?>';
            $.ajax({
                type: 'POST',
                url: url,
                data: {id: id},
                success: function (res) {
                   // alert("tests")
                    if (res != '') {
                        $('#table_add').html('');
                        var data = JSON.parse(res);
                        $.each(data, function (index, value) {
                            if (value.TenderDetails == '' || value.TenderDetails == null) {
                                var proname = '';
                            } else {
                                var proname = value.TenderDetails;
                            }
                            if (value.project_status == '' || value.project_status == null) {
                                var prostatus = 'Ongoing';
                            } else {
                                var prostatusc = value.project_status;
                                 if (prostatusc == '0') {
                                    prostatus = '<div class="alert alert-warning "> <i class="fa fas fa-exclamation-circle"></i> Awaiting';
                                } else if (prostatusc == '1') {
                                    prostatus = '<div class="alert alert-success "> <i class="fa fas fa-exclamation-circle"></i> Won';
                                } else if (prostatusc == '2') {
                                    prostatus = '<div class="alert alert-danger "> <i class="fa fas fa-exclamation-circle"></i> loose';
                                } else if (prostatusc == '3') {
                                    prostatus = '<div class="alert alert-danger "> <i class="fa fas fa-exclamation-circle"></i> Cancel';
                                }
                            }

                            $('#table_add').append('<tr><td>' + proname + '</td> <td style="width: 150px; text-transform: uppercase"> <div class="alert alert-warning "> <i class="fa fa-exclamation-triangle"></i> ' + value.designation_name + '</div></td><td style="width: 150px; text-transform: uppercase"> ' + prostatus + '</div></td></tr>');
                        });
                    }
                },
            });
        }
    }


    // for Other EMP..
    function empIDother(id) {

        if (id != '') {
            //alert(id);
            var url = '<?= base_url('report/getProjectDetailByUserIDother') ?>';
            $.ajax({
                type: 'POST',
                url: url,
                data: {id: id},
                success: function (res) {
                    //alert(res);
                    if (res != '') {
                        $('#table_add').html('');
                        var data = JSON.parse(res);
                        $.each(data, function (index, value) {
                            if (value.TenderDetails == '' || value.TenderDetails == null) {
                                var proname = '';
                            } else {
                                var proname = value.TenderDetails;
                            }
                            if (value.project_status == '' || value.project_status == null) {
                                var prostatus = 'Ongoing';
                            } else {
                                var prostatusc = value.project_status;
                                if (prostatusc == '0') {
                                    prostatus = '<div class="alert alert-warning "> <i class="fa fas fa-exclamation-circle"></i> Awaiting';
                                } else if (prostatusc == '1') {
                                    prostatus = '<div class="alert alert-success "> <i class="fa fas fa-exclamation-circle"></i> Won';
                                } else if (prostatusc == '2') {
                                    prostatus = '<div class="alert alert-danger "> <i class="fa fas fa-exclamation-circle"></i> loose';
                                } else if (prostatusc == '3') {
                                    prostatus = '<div class="alert alert-danger "> <i class="fa fas fa-exclamation-circle"></i> Cancel';
                                }
                            }

                            $('#table_add').append('<tr><td>' + proname + '</td> <td style="width: 150px; text-transform: uppercase"> <div class="alert alert-warning "> <i class="fa fa-exclamation-triangle"></i> ' + value.designation_name + '</div></td><td style="width: 150px; text-transform: uppercase"> ' + prostatus + '</div></td></tr>');
                        });
                    }
                },
            });
        }
    }


    $(document).ready(function () {
        $('#table').dataTable({
            responsive: true,

            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]], // page length options

            "dom": 'Bfrtip',
            "buttons": ['excel', 'csv', 'pdf', 'print', 'pageLength'],
        });
    });
</script>
</body>