<html lang="en">
    <?php error_reporting(E_ALL); //error_reporting(0); ?>
    <?php $this->load->view('include/innerhead'); ?>
    <body>
        <!-- topbar starts -->
        <?php $this->load->view('include/tabbar'); ?>
        <!-- topbar ends -->
        

        <div class="wrapper">
            <?php $this->load->view('include/sidebar'); ?>
            <div id="content" class="col-lg-10 col-sm-10">
                    <!-- content starts -->
                    

                    <div class="row">
                        <?php if ($this->session->flashdata('msg')) { ?>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><div id="msgs">Success!</div></strong> <?= $this->session->flashdata('msg'); ?>
                            </div>
                        <?php } ?>




                        <!-- open Phase Section-->
                        <div class="box-inner">
                            <div class="box-content">
                                <div class="tab-content">

                                    <div id="home" class="tab-pane fade in active">
									
                                        <!-- <div id="curve_chart" style="width: 900px; height: 500px"></div>
									 <div id="radar-chart" style="width:55%!important;"></div>-->

<div id="chart-container"></div>

                                    </div>
<?php
										 // echo '<pre>';
										// print_r($arr);  
										// print_r($arrDatas);  
										//print_r($reportData);  
									?>
									<div class="container">
										<div class="panel-heading"></div>
											<?php 
														$k = 1;
														if(!empty($arr)){
															for($i=0; $i<count($arr); $i++){
																$projID =  $arr[$i]['projcode'];
																$data = $this->Front_model->getCegScore($projID);
																$financialScore = $data->financial_score;
																echo '<h4>'.$arr[$i]['projname'].'</h4>';
															?>
																<table class="table table-bordered">                                                           
																	<tbody>
																		<tr>
																			<th>Fin Score</th>
																			<th>Firm</th>
																			<th>Difference</th>
																		</tr>
																		<?php
																			//for($i=0; $i<count($arrDatas); $i++){												
																				for($j=0; $j<count($arrDatas[$i]); $j++){																
																					//if($arrDatas[$i][$j]['compt_comp_id'] !='74'){
																					echo '<tr>
																						<td>'.number_format($arrDatas[$i][$j]['financial_score'],2).'</td>
																						<td>'.$arrDatas[$i][$j]['compcompanyname'].'</td>
																						<td>'.number_format(($arrDatas[$i][$j]['financial_score'] - $financialScore),2).'</td>
																						</tr>';
																					//}
																				}
																			//}
																		?>
																		
																	</tbody>
																</table>
																<br/>
															<?php
															}
														}
													?>
											<!--<table class="table table-bordered">                                                           
												<tbody>
													<tr>
														<th>Sr. No</th>
														<th>Project Name</th>
														<th>Fin Score</th>
														<th>Firm</th>
														<th>Difference</th>
													</tr>
													<?php 
														$k = 1;
														//echo '<pre>';
														if(!empty($arr)){
															for($i=0; $i<count($arr); $i++){
																$projID =  $arr[$i]['projcode'];
																$data = $this->Front_model->getCegScore($projID);
																$financialScore = $data->financial_score;
																echo '<tr>
																		<td>'.$k.'</td>
																		<td>'.$arr[$i]['projname'].'</td>
																		<td>'.number_format($financialScore,2).'</td>
																		<td>CEG</td>
																		<td>&nbsp;</td>
																	</tr>'; 
																
																//for($i=0; $i<count($arrDatas); $i++){												
																	for($j=0; $j<count($arrDatas[$i]); $j++){																
																		//print_r($arrDatas[$i][$j]);
																		 //echo $arrDatas[$i][$j]['compt_comp_id'] .'<br/>';
																	if($arrDatas[$i][$j]['compt_comp_id'] !='74'){
																		echo '<tr>
																			<td>&nbsp;</td>
																			<td>&nbsp;</td>
																			<td>'.number_format($arrDatas[$i][$j]['financial_score'],2).'</td>
																			<td>'.$arrDatas[$i][$j]['compcompanyname'].'</td>
																			<td>'.number_format(($arrDatas[$i][$j]['financial_score'] - $financialScore),2).'</td>
																			</tr>';
																		}
																	}
																//}	
																$k ++;
															}
														}
														
														/* if(!empty($arr)){
															for($i=0; $i<count($arr); $i++){
																$projID =  $arr[$i]['projcode'];
																$data = $this->Front_model->getCegScore($projID);
																$financialScore = $data->financial_score;
																echo '<tr>
																		<td>'.$k.'</td>
																		<td>'.$arr[$i]['projname'].'</td>
																		<td>'.number_format($financialScore,2).'</td>
																		<td>CEG</td>
																		<td>&nbsp;</td>
																	</tr>'; 
																
																//for($i=0; $i<count($arrDatas); $i++){													
																	for($j=0; $j<count($arrDatas[$i]); $j++){																
																		//print_r($arrDatas[$i][$j]);
																		 //echo $arrDatas[$i][$j]['compt_comp_id'] .'<br/>';
																	if($arrDatas[$i][$j]['compt_comp_id'] !='74'){
																		echo '<tr>
																			<td>&nbsp;</td>
																			<td>&nbsp;</td>
																			<td>'.number_format($arrDatas[$i][$j]['financial_score'],2).'</td>
																			<td>'.$arrDatas[$i][$j]['compcompanyname'].'</td>
																			<td>'.number_format(($arrDatas[$i][$j]['financial_score'] - $financialScore),2).'</td>
																			</tr>';
																		}
																	}
																//}	
																$k ++;
															}
														} */
														
													?>
												</tbody>   
											</table>-->
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                    <!--Close phase Section-->

                </div>

            
        </div>	

        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9 hidden-xs">
            </div>
        </div>

        <hr>
        <!-- BootStrap Model For Comments.. -->
        <?php $this->load->view('include/commenthtml'); ?>
        <!-- close Model -->

        <?php $this->load->view('include/footer'); ?>

        
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
        
	<!--<script type="text/javascript" src="https://static.fusioncharts.com/code/latest/fusioncharts.js"></script>
    <script type="text/javascript" src="https://static.fusioncharts.com/code/latest/fusioncharts.powercharts.js"></script>-->
    <script src="<?= FRONTASSETS; ?>jquery/fusioncharts.js?>"></script>
    <script src="<?= FRONTASSETS; ?>jquery/fusioncharts.powercharts.js?>"></script>
    <script src="<?= FRONTASSETS; ?>jquery/fusioncharts-jquery-plugin.js?>"></script>
    <script src="<?= FRONTASSETS; ?>jquery/jquery-2.2.3.min.js?>"></script>
    <script src="<?= FRONTASSETS; ?>jquery/fusioncharts.theme.fint.js?>"></script>
	
	<!--<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="https://static.fusioncharts.com/code/latest/fusioncharts.js"></script>
    <script type="text/javascript" src="https://static.fusioncharts.com/code/latest/fusioncharts.powercharts.js"></script>
    <script type="text/javascript" src="fusioncharts-jquery-plugin.js"></script>
     <script type="text/javascript" src="https://static.fusioncharts.com/code/latest/themes/fusioncharts.theme.fint.js"></script>-->
    <script type="text/javascript">
	FusionCharts.ready(function () {
    var visitChart = new FusionCharts({
        type: 'msline',
        renderAt: 'chart-container',
        width: '1200',
        height: '750',
        dataFormat: 'json',
        dataSource: {
            "chart": {
               "caption": "Financial Report",
              /*    "subCaption": "Bakersfield Central vs Los Angeles Topanga", */
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "paletteColors": "#0075c2,#1aaf5d,#008ee4,#6baa01,#C0C0C0,#800000,#808000,#00FFFF,#008080,#800080,#000080",
                "bgcolor": "#ffffff",
                "showBorder": "0",
                "showShadow": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "showAxisLines": "0",
                "showAlternateHGridColor": "0",
                "divlineThickness": "1",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "xAxisName": "Company Name",
				"showValues": "1" ,
				"numVDivLines": "<?php echo $comcount; ?>",
				"vDivLineColor": "#99ccff",
				"vDivLineThickness": "1",
				"vDivLineAlpha": "50"  	,
				"anchorRadius":"5"
				
				
            },
            "categories": [
                {
					
                    "category": <?php echo json_encode($competorcompany); ?>
                }
            ],
            "dataset": <?php echo json_encode($chkData); ?>, 
            /* "trendlines": [
                {
                    "line": [
                        {
                            "startvalue": "17022",
                            "color": "#6baa01",
                            "valueOnRight": "1",
                            "displayvalue": "Average"
                        }
                    ]
                }
            ] */
        }
    }).render();
});
/* $("#radar-chart").insertFusionCharts({ 
  type: 'radar',
  width: '100%',
  height: '500',
  dataFormat: 'json',
  dataSource: {
    "chart": {
      // "caption": "2015 Budget for Acme Inc.", 
      "captionFontSize": "22",
      "numberPrefix": "RS",
      "baseFont": "Open Sans",
      "baseFontSize": "14",
      "paletteColors": "#008ee4,#6baa01,#C0C0C0,#800000,#808000,#00FFFF,#008080,#800080,#000080",
      "bgColor": "#ffffff",
      "radarfillcolor": "#ffffff",
      "showCanvasBorder": "0",
      "legendShadow": "0",
      "divLineAlpha": "35",
      "usePlotGradientColor": "0",
      "numberPreffix": "$",
      "legendBorderAlpha": "0",
      "yAxisMaxValue": "20000",

      // line and anchor customizations
      "anchorRadius": "4",
      "anchorBorderThickness": "2",
      "anchorTrackingRadius": "20",
      "showHoverEffect": "1",

      // tool tip customizations
      "toolTipColor": "#e0e4e6",
      "toolTipBorderColor": "#e0e4e6",
      "toolTipBorderThickness": "1",
      "toolTipBgColor": "#000000",
      "toolTipBgAlpha": "70",
      "toolTipBorderRadius": "2",
      "toolTipPadding": "10",
      "plotToolText": "<div style='text-align:center; line-height:1.5;'>$seriesname <br> $label: $dataValue</div>"

    },
	"categories": [
		{
		  "category": <?php echo json_encode($competorcompany); ?>
		}
	],
    "dataset": <?php echo json_encode($chkData); ?>
	
}
});
 */
 
    </script>
<script src="<?= FRONTASSETS; ?>js/bootstrap.min.js?>" ></script>
    </div>
</body>
</html>
