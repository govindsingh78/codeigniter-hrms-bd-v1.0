<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<style>
#table_length {
    position: absolute;
    margin-left: 100px !important;
}
.table tbody tr td, .table tbody th td {
    vertical-align: middle;
    white-space: normal !important;
}
</style>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row clearfix">
                    <div class="col-lg-12">

                        <?php if ($this->session->flashdata('msg')) { ?>

                            <!-- <script>
                            toastr.success(<?//= //$this->session->flashdata('msg'); ?> , 'Message', {timeOut: 5000});
                            </script> -->
                            <div class="alert alert-success alert-dismissable" style="">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>
                                    <div id="msgs"> Success ! 
                                </strong> <?= $this->session->flashdata('msg'); ?></div>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('error_msg')) { ?>
                            <div class="alert alert-danger alert-dismissable" style="">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>
                                    <div id="msgs"> Error !
                                </strong> <?= $this->session->flashdata('error_msg'); ?> </div>
                            </div>
                        <?php } ?>


                        <div class="card">
                            <div class="header">
                                <form id="form-filter" class="form-horizontal">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label class="email"> Service : </label>
                                    <input type="text" id="serviceinput" class="form-control">
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Operator : </label>
                                            <select id="service_cond" class="form-control">
                                                        <option value="0">None</option>
                                                        <option value="1">AND</option>
                                                        <option value="2">OR</option>
                                                    </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Client : </label>
                                            <input type="text" id="clientnput" class="form-control">
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Operator : </label>
                                            <select id="service_cond" class="form-control">
                                                        <option value="0">None</option>
                                                        <option value="1">AND</option>
                                                        <option value="2">OR</option>
                                                    </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Sector : </label>
                                            <input type="text" id="sectorinput" class="form-control">
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> Operator : </label>
                                            <select id="service_cond" class="form-control">
                                                        <option value="0">None</option>
                                                        <option value="1">AND</option>
                                                        <option value="2">OR</option>
                                                    </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="email"> State : </label>
                                            <input type="text" id="stateinput" class="form-control">
                                        </div>


                                        <div class="col-sm-3">
                                            <label class="email"> &nbsp; </label> <br>
                                            <button type="button" id="btn-filter" class="btn btn-primary pull-right ml-2"> Filter </button> &nbsp;&nbsp;&nbsp;
                                            <button type="button" id="btn-reset" class="btn btn-default pull-right"> Reset </button>
                                        </div>
                                    </div>
                                </form>


                            </div>



                            






                            <div class="row">
                                
                            </div>


                            <div class="row clearfix">
                                <div class="col-lg-12">
                    <div class="">
                        
                        <div class="body">
						
                        <div class="table-responsive"> 
                                   
                        <div class="col-md-12">
                                    <div id="colvis"></div>
                                    <div class="form-group has-success pull-right"> 
                                        
                                    </div>
                                </div>
                                <form action="<?= base_url('report/financialchart'); ?>" method="post">
                               
                                    <table id="table" class="table table-striped display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        
                                        <th>Sr. No</th>
                                        <th>Tender Details</th>
                                        <th>Service</th>
                                        <th>client</th>
                                        <th style="width: 120px">Status</th>
                                        <th>Project Type</th>
                                        <!--<th>Start Year</th>-->
                                        <th>Sector</th>
                                        <th>State</th>
                                        <th style="width: 10%">Project Code</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>

                                <tfoot>
                                    <tr>
                                        
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                     
                                       <td colspan="2"><div class="pull-right p-2"><button type="submit" class="btn btn-success pull-left mt-1 mr-2">Submit</button><label class="fancy-checkbox mt-2">
                                                                    <input type="checkbox" id="checkAll">
                                                                    <span>All</span>
                                                                </label></div>


                                                            </td>
                                    </tr>
                                </tfoot>
                                <hr>
                            </table>
                            </form>   
                                    
                                    
                                    
							</div>
                        </div>
                    </div>
                </div>
            </div>




                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view('admin/includes/footer'); ?>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?= FRONTASSETS; ?>js/jquery.multiselect.js"></script>
    <link href="<?= FRONTASSETS; ?>bower_components/chosen/chosen.css" rel="stylesheet"></link>
    <script src="<?= FRONTASSETS; ?>bower_components/chosen/chosen.jquery.js"></script>
    </div>
   <style>
            #table_length{margin-left:20px;}
            #table_filter{margin-right:2%;} 
            #chatbox {padding: 15px;overflow: scroll;height: 300px;}
        </style>
        <script type="text/javascript">
           
            $(document).ready(function () {
                $("#checkAll").click(function () {
                    $('input:checkbox').not(this).prop('checked', this.checked);
                });
            });
            
            var table;

            $(document).ready(function () {
                
                
                //datatables
                table = $('#table').DataTable({
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.

                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url('report/financialreportAll') ?>",
                        "type": "POST",
                        "data": function (data) {
                             data.serviceinput = $('#serviceinput').val();
                             data.service_cond = $('#service_cond').val();
                             data.clientnput = $('#clientnput').val();
                             data.client_cond = $('#client_cond').val();
                             // data.yearinput = $('#yearinput').val();
                             // data.year_cond = $('#year_cond').val();
                             data.sectorinput = $('#sectorinput').val();
                             data.sector_cond = $('#sector_cond').val();
                             data.stateinput = $('#stateinput').val();

                            // data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                        },
                        //"data": {'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>','employeeId' : $('#employeeId').val()},
                    },
                    "dom": 'lBfrtip',
                    "buttons": [
                        {
                            extend: 'collection',
                            text: 'Export',
                            buttons: [
                                'copy',
                                'excel',
                                'csv',
                                'pdf',
                                'print'
                            ]

                        }
                    ],

                    //Set column definition initialisation properties.
                    "columnDefs": [
                        {
                            "targets": [0], //first column / numbering column
                            "orderable": false, //set not orderable
                        },
                    ],
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],

                });

                var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
                $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"

                $('#btn-filter').click(function () { //button filter event click
                    table.ajax.reload();  //just reload table
                });

                $('#btn-reset').click(function () { //button reset event click
                    $('#form-filter')[0].reset();
                    table.ajax.reload();  //just reload table
                });
                
                
            });
            
            

        </script>
</body>