<?php
if (($this->session->userdata('loginid') == '')):
    redirect(base_url('login'));
endif;
?>
<!doctype html>
<html lang="en">


<!-- Mirrored from www.wrraptheme.com/templates/lucid/html/light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:48:45 GMT -->
<head>
<title>:: CEG HRM :: Dashboard</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Lucid Bootstrap 4.1.1 Admin Template">
<meta name="author" content="WrapTheme, design by: ThemeMakker.com">

<link rel="icon" href="favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="<?= HOSTNAME; ?>assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?= HOSTNAME; ?>assets/vendor/font-awesome/css/font-awesome.min.css">

<link rel="stylesheet" href="<?= HOSTNAME; ?>assets/vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
<link rel="stylesheet" href="<?= HOSTNAME; ?>assets/vendor/chartist/css/chartist.min.css">
<link rel="stylesheet" href="<?= HOSTNAME; ?>assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
<link rel="stylesheet" href="<?= HOSTNAME; ?>assets/vendor/toastr/toastr.min.css">

<!-- MAIN CSS -->
<link rel="stylesheet" href="<?= HOSTNAME; ?>assets/css/main.css">
<link rel="stylesheet" href="<?= HOSTNAME; ?>assets/css/color_skins.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">


<style>
    td.details-control {
    background: url('../assets/images/details_open.png') no-repeat center center;
    cursor: pointer;
}
    tr.shown td.details-control {
        background: url('../assets/images/details_close.png') no-repeat center center;
    }
</style>
</head>
<body class="theme-cyan">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img src="<?= HOSTNAME; ?>assets/images/cegth-logo.png" width="200" alt="CEG HRM Logo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->

<div id="wrapper">

     <?php include('top-navbar.php'); ?>
	 
	 
	 <div id="left-sidebar" class="sidebar">
        <div class="sidebar-scroll">
		
		 	<?php include('user-account.php'); ?>
			           
					<!-- Nav tabs -->
					
					<?php include('left-navbar.php'); ?>
				              
            </div>          
        </div>

 
    </div>

    <div id="main-content">
	
		
        <div class="container-fluid">
            <div class="block-header">
			
			<?php include('breadcrumb-item.php'); ?>
               
            </div>
		 			
			<?php include('report-content.php'); ?>
            
        </div>
    </div>
    
</div>
  
<!-- Javascript -->
<!-- Javascript -->
<?/*<script src="<?= HOSTNAME; ?>assets/bundles/libscripts.bundle.js"></script>    
<script src="<?= HOSTNAME; ?>assets/bundles/vendorscripts.bundle.js"></script>

<script src="<?= HOSTNAME; ?>assets/bundles/chartist.bundle.js"></script>
<script src="<?= HOSTNAME; ?>assets/bundles/knob.bundle.js"></script> 
<script src="<?= HOSTNAME; ?>assets/vendor/toastr/toastr.js"></script>

<script src="<?= HOSTNAME; ?>assets/bundles/mainscripts.bundle.js"></script>
<script src="<?= HOSTNAME; ?>assets/js/index.js"></script>*/?>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="<?= HOSTNAME; ?>assets/bundles/libscripts.bundle.js"></script>    
<script src="<?= HOSTNAME; ?>assets/bundles/vendorscripts.bundle.js"></script>

<script src="<?= HOSTNAME; ?>assets/bundles/datatablescripts.bundle.js"></script>
<script src="<?= HOSTNAME; ?>assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="<?= HOSTNAME; ?>assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="<?= HOSTNAME; ?>assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="<?= HOSTNAME; ?>assets/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="<?= HOSTNAME; ?>assets/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>

<script src="<?= HOSTNAME; ?>assets/vendor/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 


<script src="<?= HOSTNAME; ?>assets/bundles/mainscripts.bundle.js"></script>
<script src="<?= HOSTNAME; ?>assets/js/pages/tables/jquery-datatable.js"></script>

</body>

<!-- Mirrored from www.wrraptheme.com/templates/lucid/html/light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:49:56 GMT -->
</html>

 