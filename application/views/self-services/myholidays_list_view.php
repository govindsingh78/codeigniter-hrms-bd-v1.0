<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row clearfix">
                    <div class="col-lg-12">

                        <div class="card">
                            <div class="body">
                                <form method="post" action="<?= base_url("myholidays"); ?>">
                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="mb-3">
                                                        <b>Holiday Type : </b>
                                                        <select class="form-control" name="holidaystype" data-placeholder="Select">
                                                            <option <?= (@$holidaystype == "") ? "Selected" : ""; ?> value=""> -- All -- </option>
                                                            <option <?= (@$holidaystype == "1") ? "Selected" : ""; ?> value="1"> Holidays </option>
                                                            <option <?= (@$holidaystype == "2") ? "Selected" : ""; ?> value="2"> RH (Restricted Holidays) </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="mb-3">
                                                        <b>Year : </b>
                                                        <select class="form-control" name="holidaysyear" data-placeholder="Select">
                                                            <option <?= (@$holidaysyear == "") ? "Selected" : ""; ?> value=""> -- All -- </option>
                                                            <option <?= (@$holidaysyear == "2016") ? "Selected" : ""; ?> value="2016"> 2016 </option>
                                                            <option <?= (@$holidaysyear == "2017") ? "Selected" : ""; ?> value="2017"> 2017 </option>
                                                            <option <?= (@$holidaysyear == "2018") ? "Selected" : ""; ?> value="2018"> 2018 </option>
                                                            <option <?= (@$holidaysyear == "2019") ? "Selected" : ""; ?> value="2019"> 2019 </option>
                                                            <option <?= (@$holidaysyear == "2020") ? "Selected" : ""; ?> value="2020"> 2020 </option>
                                                            <option <?= (@$holidaysyear == "2021") ? "Selected" : ""; ?> value="2021"> 2021 </option>
                                                            <option <?= (@$holidaysyear == "2022") ? "Selected" : ""; ?> value="2022"> 2022 </option>
                                                            <option <?= (@$holidaysyear == "2023") ? "Selected" : ""; ?> value="2023"> 2023 </option>
                                                            <option <?= (@$holidaysyear == "2024") ? "Selected" : ""; ?> value="2024"> 2024 </option>
                                                            <option <?= (@$holidaysyear == "2025") ? "Selected" : ""; ?> value="2025"> 2025 </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-6">
                                                    <div class="mb-2">
                                                        <br>
                                                        <input type="submit" value="Filter Record" name="filter" class="btn btn-round btn-primary">
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                        <thead>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Holiday</th>
                                                <th>H-Type</th>
                                                <th>H-Date</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($HolidaysRecArr) {
                                                foreach ($HolidaysRecArr as $kEy => $dataRow) {
                                                    ?>
                                                    <tr style="<?= ($dataRow->groupid == 2) ? "color:#bd2727" : ""; ?>">
                                                        <td><?= $kEy + 1; ?></td>
                                                        <td><?= ($dataRow->holidayname) ? $dataRow->holidayname : ""; ?></td>
                                                        <td><?= ($dataRow->groupname) ? $dataRow->groupname : ""; ?></td>
                                                        <td><?= ($dataRow->holidaydate) ? date("d-m-Y", strtotime($dataRow->holidaydate)) : ""; ?></td>
                                                        <td><?= ($dataRow->description) ? $dataRow->description : ""; ?></td>
                                                        <td><i class="fa fa-eye"></i></td>
                                                    </tr>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td style="color:red" colspan="6"> Record Not Found. </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Holiday</th>
                                                <th>H-Type</th>
                                                <th>H-Date</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>