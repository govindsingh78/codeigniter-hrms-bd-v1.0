<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
//$statusArr = array("0"=>"A","1"=>"","2"=>"","3"=>"","4"=>);
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div> 
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">

                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Apply Tour
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="header">
                                        <ul class="header-dropdown">
                                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="javascript:void(0);">View Holidays</a></li>
                                                    <li><a href="javascript:void(0);">Employee Leave</a></li>
                                                    <li><a href="javascript:void(0);">Employee Tour</a></li>
                                                </ul>
                                            </li>
                                            <li class="remove">
                                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-3 col-md-6">
                                                <div class="mb-3">
                                                    <b>Available Leaves</b>
                                                    <input type="text" class="form-control" name="avl_leave" value="05.5" disabled="disabled">    
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6">
                                                <div class="mb-3">
                                                    <b>Reporting Manager</b>
                                                    <input disabled="disabled" type="text" value="Mr. Kapil Arora" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6">
                                                <div class="mb-3">
                                                    <b>Project</b>
                                                    <select class="form-control show-tick ms search-select" data-placeholder="Select">
                                                        <option value=""> -- Select -- </option>
                                                        <option value="1"> PL </option>
                                                        <option value="2"> RH </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6">
                                                <div class="mb-3">
                                                    <b>Reason</b>
                                                    <textarea class="form-control" name="reason"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6">
                                                <b>From Date</b>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="icon-calendar"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control date" placeholder="Ex: 30/07/2016">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6">
                                                <b>To Date</b>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="icon-calendar"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control date" placeholder="Ex: 30/07/2016">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6">
                                                <div class="mb-3">
                                                    <b>Leave For</b>
                                                    <select class="form-control" data-placeholder="Select">
                                                        <option value="1"> Full Day </option>
                                                        <option value="2"> Half Day </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6">
                                                <div class="mb-3">
                                                    <b>Days</b>
                                                    <input readonly="readonly" type="number" step="any" value="" id="array-select" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-6">
                                                <div class="mb-2">
                                                    <b>&nbsp;</b>
                                                    <input type="submit" id="array-select" class="btn btn-round btn-primary">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>


                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                        <thead>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Project </th>
                                                <th>From Date</th>
                                                <th>To Date</th>
                                                <th>Location</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($AppliedTourRecArr) {
                                                foreach ($AppliedTourRecArr as $kEy => $dataRow) {
                                                    ?>
                                                    <tr style="<?= ($dataRow->approved_bylmanager == 2) ? "color:#bd2727" : ""; ?>">
                                                        <td><?= $kEy + 1; ?></td>
                                                        <td><?= ($dataRow->project_name) ? $dataRow->project_name : ""; ?></td>
                                                        <td><?= ($dataRow->start_date) ? date("d-m-Y", strtotime($dataRow->start_date)) : ""; ?></td>
                                                        <td><?= ($dataRow->end_date) ? date("d-m-Y", strtotime($dataRow->end_date)) : ""; ?></td>
                                                        <td><?= ($dataRow->tour_location) ? $dataRow->tour_location : ""; ?></td>
                                                        <td><?= ($dataRow->description) ? $dataRow->description : ""; ?></td>
                                                        <td><?= ($dataRow->approved_bypmanager) ? $dataRow->approved_bypmanager : ""; ?></td>
                                                        <td><i class="fa fa-eye"></i></td>
                                                    </tr>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td style="color:red" colspan="8"> Record Not Found. </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Project </th>
                                                <th>From Date</th>
                                                <th>To Date</th>
                                                <th>Location</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>