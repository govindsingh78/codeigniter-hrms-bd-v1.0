<?PHP

//current_url
function thisurl() {
    $actual_link = (isset($_SERVER['HTTP']) ? "http" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    return $actual_link;
}





function get_basecomp($projid, $comid) {
    $CI = & get_instance();
    $CI->db->select('a.base_comp_id');
    $CI->db->from('jv_cegexp as a');
    $CI->db->where(array('a.project_id' => $projid, 'a.status' => '1','a.base_comp_id'=>$comid));
    //$CI->db->where(array('a.project_id' => $projid, 'a.status' => '1'));
   // $CI->db->where("(find_in_set($comid, a.lead_comp_id) OR find_in_set($comid, a.joint_venture) OR find_in_set($comid, a.asso_comp))", NULL, FALSE);
    $CI->db->group_by('a.project_id');
    $result = $CI->db->get()->row();
    return ($result) ? $result : '';
}


function get_leadcomp($projid, $comid) {
    $CI = & get_instance();
    $CI->db->select('a.lead_comp_id');
    $CI->db->from('jv_cegexp as a');
    $CI->db->where(array('a.project_id' => $projid, 'a.status' => '1','a.base_comp_id'=>$comid));
    $CI->db->where("(find_in_set(74, a.lead_comp_id))", NULL, FALSE);
    $CI->db->group_by('a.project_id');
    $result = $CI->db->get()->row();
    return ($result) ? $result : '';
}

function get_assoccomp($projid, $comid) {
    $CI = & get_instance();
    $CI->db->select('a.asso_comp');
    $CI->db->from('jv_cegexp as a');
    $CI->db->where(array('a.project_id' => $projid, 'a.status' => '1','a.base_comp_id'=>$comid));
    $CI->db->where("(find_in_set(74, a.asso_comp))", NULL, FALSE);
    $CI->db->group_by('a.project_id');
    $result = $CI->db->get()->row();
    return ($result) ? $result : '';
}

//code by durgesh for LeadJvAssoc record BY Project ID(19-12-2019)
function get_jvcomp($projid, $comid) {
    $CI = & get_instance();
    $CI->db->select('a.joint_venture');
    $CI->db->from('jv_cegexp as a');
    $CI->db->where(array('a.project_id' => $projid, 'a.status' => '1','a.base_comp_id'=>$comid));
    $CI->db->where("(find_in_set(74, a.joint_venture))", NULL, FALSE);
    $CI->db->group_by('a.project_id');
    $result = $CI->db->get()->row();
    return ($result) ? $result : '';
}


//Get Education By User ID...Though Durgesh
function GetEducationByUserid($userid) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select("*");
    $ci->db->from("main_empeducationdetails");
    $ci->db->where(array('isactive' => '1', 'user_id' => $userid));
    $ci->db->order_by("id", "ASC");
    $data = $ci->db->get()->result();
    return ($data) ? $data : null;
}
//Get All Notified UserID..
function NotifiedUserIDs($userId) {
    $CI = & get_instance();
    $UserIdArr = array('296' => '296', '287' => '287', '190' => '190', '260' => '260', '175' => '175', '346' => '346', '257' => '257', '256' => '256', '191' => '191', '259' => '259', '211' => '211');
    unset($UserIdArr[$userId]);
    return $UserIdArr;
}
function get_companyname($companyid) {
    $CI = & get_instance();
    $CI->db->select('company_name');
    $CI->db->from('main_company');
    $CI->db->where('status', '1');
    $CI->db->where_in('fld_id', $companyid);
    $result = $CI->db->get()->result();
    return ($result) ? $result : '';
}

function get_LeadjvAssocById($projid) {
    $CI = & get_instance();
    $CI->db->select('a.project_id,a.lead_comp_id,a.joint_venture,a.asso_comp,b.financial_detail');
    $CI->db->from('jv_cegexp as a');
    $CI->db->join('proj_financial_details as b', 'b.bd_projid=a.project_id', 'left');
    $CI->db->where(array('a.project_id' => $projid, 'a.status' => '1'));
    $CI->db->where("(a.base_comp_id='74' OR a.lead_comp_id='74' OR find_in_set(74, a.joint_venture) OR find_in_set(74, a.asso_comp) )", NULL, FALSE);
    $result = $CI->db->get()->result();
    return ($result) ? $result : '';
}
function GetCompAlldetailsnewash($projId, $typS) {
    $CI = & get_instance();
    $CI->db->select("base_comp_id,lead_comp_id,joint_venture,asso_comp");
    $CI->db->from("jv_cegexp");
    $CI->db->where(array("project_id" => $projId, "status" => "1"));
    $CI->db->where("(base_comp_id='74' OR lead_comp_id='74' OR find_in_set(74, joint_venture) OR find_in_set(74, asso_comp) )", NULL, FALSE);
    $CI->db->order_by("fld_id", 'DESC');
    $CI->db->limit(1);
    $restRow = $CI->db->get()->row();
    if ($restRow) {
        $arrayLeadsID = explode(",", $restRow->lead_comp_id);
        $arrayjoint_ventureID = explode(",", $restRow->joint_venture);
        $arrayasso_compID = explode(",", $restRow->asso_comp);
        //01..
        if ((count($arrayLeadsID) > 0) and ( $typS == 1)):
            $CI->db->SELECT("company_name");
            $CI->db->FROM("main_company");
            $CI->db->where_in("fld_id", $arrayLeadsID);
            $restArrRow1 = $CI->db->get()->result();
            $D1 = '';
            if ($restArrRow1) {
                foreach ($restArrRow1 as $rEcord1) {
                    $D1 .= $rEcord1->company_name . " , ";
                }
                return $D1 . "<b>LEAD</b>";
            }
        endif;
        //02..
        if ((count($arrayjoint_ventureID) > 0) and ( $typS == 2)):
            $CI->db->SELECT("company_name");
            $CI->db->FROM("main_company");
            $CI->db->where_in("fld_id", $arrayjoint_ventureID);
            $restArrRow2 = $CI->db->get()->result();
            $D2 = '';
            if ($restArrRow2) {
                foreach ($restArrRow2 as $rEcord2) {
                    $D2 .= $rEcord2->company_name . " , ";
                }
                return $D2 . "<b>JV</b>";
            }
        endif;
        //03..
        if ((count($arrayasso_compID) > 0) and ( $typS == 3)):
            $CI->db->SELECT("company_name");
            $CI->db->FROM("main_company");
            $CI->db->where_in("fld_id", $arrayasso_compID);
            $restArrRow3 = $CI->db->get()->result();
            $D3 = '';
            if ($restArrRow3) {
                foreach ($restArrRow3 as $rEcord3) {
                    $D3 .= $rEcord3->company_name . " , ";
                }
                return $D3 . "<b>ASSOCIATE</b>";
            }
        endif;
    }
}
function GetCompAlldetails($projId, $baseCompId, $typS) {
    $CI = & get_instance();
    $CI->db->select("base_comp_id,lead_comp_id,joint_venture,asso_comp");
    $CI->db->from("jv_cegexp");
    $CI->db->where(array("project_id" => $projId, "base_comp_id" => $baseCompId, "status" => "1"));
    $CI->db->order_by("fld_id", 'DESC');
    $CI->db->limit(1);
    $restRow = $CI->db->get()->row();
    if ($restRow) {
        $arrayLeadsID = explode(",", $restRow->lead_comp_id);
        $arrayjoint_ventureID = explode(",", $restRow->joint_venture);
        $arrayasso_compID = explode(",", $restRow->asso_comp);
        //01..
        if ((count($arrayLeadsID) > 0) and ( $typS == 1)):
            $CI->db->SELECT("company_name");
            $CI->db->FROM("main_company");
            $CI->db->where_in("fld_id", $arrayLeadsID);
            $restArrRow1 = $CI->db->get()->result();
            $D1 = '';
            if ($restArrRow1) {
                foreach ($restArrRow1 as $rEcord1) {
                    $D1 .= $rEcord1->company_name . " , ";
                }
                return $D1 . "<b>LEAD</b>";
            }
        endif;
        //02..
        if ((count($arrayjoint_ventureID) > 0) and ( $typS == 2)):
            $CI->db->SELECT("company_name");
            $CI->db->FROM("main_company");
            $CI->db->where_in("fld_id", $arrayjoint_ventureID);
            $restArrRow2 = $CI->db->get()->result();
            $D2 = '';
            if ($restArrRow2) {
                foreach ($restArrRow2 as $rEcord2) {
                    $D2 .= $rEcord2->company_name . " , ";
                }
                return $D2 . "<b>JV</b>";
            }
        endif;
        //03..
        if ((count($arrayasso_compID) > 0) and ( $typS == 3)):
            $CI->db->SELECT("company_name");
            $CI->db->FROM("main_company");
            $CI->db->where_in("fld_id", $arrayasso_compID);
            $restArrRow3 = $CI->db->get()->result();
            $D3 = '';
            if ($restArrRow3) {
                foreach ($restArrRow3 as $rEcord3) {
                    $D3 .= $rEcord3->company_name . " , ";
                }
                return $D3 . "<b>ASSOCIATE</b>";
            }
        endif;
    }
}

function GetCompAlldetailsSlimSelect($projId, $baseCompId, $typS) {
    $CI = & get_instance();
    $CI->db->select("base_comp_id,lead_comp_id,joint_venture,asso_comp");
    $CI->db->from("jv_cegexp");
    $CI->db->where(array("project_id" => $projId, "base_comp_id" => $baseCompId, "status" => "1"));
    $CI->db->order_by("fld_id", 'DESC');
    $CI->db->limit(1);
    $restRow = $CI->db->get()->row();
    if ($restRow) {
        $arrayLeadsID = $restRow->lead_comp_id;
        $arrayjoint_ventureID = $restRow->joint_venture;
        $arrayasso_compID = $restRow->asso_comp;


         

        if ($typS == 1){
           
                 
                return $arrayLeadsID;
        }
            

            if ($typS == 2){
            
                return $arrayjoint_ventureID;
            }

            if ($typS == 3){
             
                return $arrayasso_compID;
            }
        
    }
}




function sendcomptitorProjectMail($to, $subject, $msgDetails) {
    //return true;
    $CI = & get_instance();
    $CI->load->library('email');
    $CI->email->initialize(array(
        'protocol' => 'smtp',
        'smtp_host' => 'mail.cegindia.com',
        'smtp_user' => 'marketing@cegindia.com',
        'smtp_pass' => 'MARK-2015ceg',
        'smtp_port' => 587,
        'crlf' => "\r\n",
        'newline' => "\r\n",
        'mailtype' => 'html',
        'charset' => 'iso-8859-1',
        'wordwrap' => TRUE
    ));

    $CI->email->from('tender@cegindia.com', 'Do_not_reply');
    //$CI->email->from('durgeshgarg32@gmail.com', 'Do_not_reply');
    $CI->email->to($to);
    //$cc = array('marketing@cegindia.com');
    $cc = array('ceg@cegindia.com');
    $CI->email->cc($cc);
    $CI->email->bcc('marketing@cegindia.com');
    //$CI->email->bcc('jitendra00752@gmail.com');
    $CI->email->subject($subject);
    $CI->email->message($msgDetails);
    $resp = $CI->email->send();
    return ($resp) ? $resp : $CI->email->print_debugger();
}
//Other Emp Name By Id..
function UserOtherNameById($userId) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->UserOtherNameById($userId);
    return $details;
}

//Code By Asheesh For Get Rfp Enterted By Proposal Manager..
function GetRfpByPid_desId($pid, $designID) {
    $CI = & get_instance();
    $respon = $CI->mastermodel->GetRfpByPm($pid, $designID);
    return ($respon) ? $respon : false;
}

//Designation Name By Id..
function DesignationNameById($userId) {
   
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->DesignationNameById($userId);
    return $details;
}
//code by durgesh for value convert in word
function numberconvert1($number) {
    $no = round($number);
    $point = round($number - $no, 2) * 100;
    $hundred = null;
    $digits_1 = strlen($no);
    $i = 0;
    $str = array();
    $words = array('0' => '', '1' => 'one', '2' => 'two',
        '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
        '7' => 'seven', '8' => 'eight', '9' => 'nine',
        '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
        '13' => 'thirteen', '14' => 'fourteen',
        '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
        '18' => 'eighteen', '19' => 'nineteen', '20' => 'twenty',
        '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
        '60' => 'sixty', '70' => 'seventy',
        '80' => 'eighty', '90' => 'ninety');
    $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
    while ($i < $digits_1) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += ($divider == 10) ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] .
                    " " . $digits[$counter] . $plural . " " . $hundred :
                    $words[floor($number / 10) * 10]
                    . " " . $words[$number % 10] . " "
                    . $digits[$counter] . $plural . " " . $hundred;
        } else
            $str[] = null;
    }
    $str = array_reverse($str);
    $result = implode('', $str);
    $points = ($point) ?
            "." . $words[$point / 10] . " " .
            $words[$point = $point % 10] : '';
    $recReturn = "( " . $result . "Rupees Only )";
    return ucwords($recReturn);
} 
function UserNameById($userId) {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    //$details = $CI->Front_model->UserNameById($userId);
    $details = $CI->SecondDB_model->getUserByID($userId);
    return $details;
}
//Get Experience By User ID...Though Durgesh
function GetExperienceByUserid($userid) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select("*");
    $ci->db->from("main_empexperiancedetails");
    $ci->db->where(array('isactive' => '1', 'user_id' => $userid));
    $ci->db->order_by("id", "ASC");
    $data = $ci->db->get()->result();
    return ($data) ? $data : null;
}
function sendMail($to, $subject, $msgDetails) {
    //return true;
    $CI = & get_instance();
    $CI->load->library('email');
    $CI->email->initialize(array(
        'protocol' => 'smtp',
        'smtp_host' => 'mail.cegindia.com',
        'smtp_user' => 'marketing@cegindia.com',
        'smtp_pass' => 'MARK-2015ceg',
        'smtp_port' => 587,
        'crlf' => "\r\n",
        'newline' => "\r\n",
        'mailtype' => 'html',
        'charset' => 'iso-8859-1',
        'wordwrap' => TRUE
    ));
    $CI->email->from('tender@cegindia.com', 'Do_not_reply_Mail');
    $CI->email->to($to);
    $CI->email->cc('marketing@cegindia.com');
    $CI->email->bcc('marketing@cegindia.com');
    $CI->email->subject($subject);
    $CI->email->message($msgDetails);
    $resp = $CI->email->send();
    return ($resp) ? $resp : $CI->email->print_debugger();
}
//code by durgesh for package work details
function getpkgworkdetail($pkg_sum_id, $work_id) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['pkg_work_id' => $work_id, 'pkg_summary_id' => $pkg_sum_id]);
    $datapkgworkdetail = $ci->db->get('package_work_detail')->row();
    return ($datapkgworkdetail) ? $datapkgworkdetail : null;
}

//code by durgesh for city name
function getprojectname($projid) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['fld_id' => $projid, 'status' => "1"]);
    $dataproject = $ci->db->get('project_master')->row();
    return ($dataproject) ? $dataproject : null;
}

//code by durgesh for contractor name
function getcontractorname($contractorid) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['fld_id' => $contractorid, 'status' => "1"]);
    $dataContractor = $ci->db->get('contractor_master')->row();
    return ($dataContractor) ? $dataContractor : null;
}

//code by durgesh for city name
function getcityname($cityid) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['fld_id' => $cityid, 'status' => "1"]);
    $dataCity = $ci->db->get('tbl_cities')->row();
    return ($dataCity) ? $dataCity : null;
}

//code by durgesh for package data
function get_package_data($proj_summr_id, $pkg_id) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['proj_summ_id' => $proj_summr_id, "pkg_number" => $pkg_id]);
    $datapackage = $ci->db->get('package_summary')->result();
    return ($datapackage) ? $datapackage : null;
}

function getAllMasterMenu() {
	// $ci = & get_instance();
    // $ci->load->database();
	
    $ci = & get_instance();
    
	$ci->db1 = $ci->load->database('online', TRUE);//sentrifugo
    $ci->db2 = $ci->load->database('another_db', TRUE);//bdtechno  ==default
	
	$db1 = $ci->db1->database;
    $db2 = $ci->db2->database;
    
    $ci->db1->where(['parent' => "0", 'status' => "1", 'menu_category' => "1"]);
    $dataMenu = $ci->db1->get("bd_menu_master")->result();
    return ($dataMenu) ? $dataMenu : null;
}



function getAllMasterMenuReports() {
    // $ci = & get_instance();
    // $ci->load->database();
    
    $ci = & get_instance();
    
    $ci->db1 = $ci->load->database('online', TRUE);//sentrifugo
    $ci->db2 = $ci->load->database('another_db', TRUE);//bdtechno  ==default
    
    $db1 = $ci->db1->database;
    $db2 = $ci->db2->database;
    
    $ci->db1->where(['parent' => "0", 'status' => "1", 'menu_category' => "2"]);
    $dataMenu = $ci->db1->get("bd_menu_master")->result();
    return ($dataMenu) ? $dataMenu : null;
}




function getSecondAllMasterMenu() {
    $ci = & get_instance();
	$ci->db1 = $ci->load->database('online', TRUE);//sentrifugo
    $ci->db2 = $ci->load->database('another_db', TRUE);//bdtechno  ==default
	
	$db1 = $ci->db1->database;
    $db2 = $ci->db2->database;

    $ci->db1->where(['parent' => "0", 'status' => "1"]);
    $ci->db1->where(['fld_id' => "15"]);
    $dataMenu = $ci->db1->get("bd_menu_master")->result();
    return ($dataMenu) ? $dataMenu : null;
}

function getChildMenuByParentID($parentID) {
    $ci = & get_instance();
	$ci->db1 = $ci->load->database('online', TRUE);//sentrifugo
    $ci->db2 = $ci->load->database('another_db', TRUE);//bdtechno  ==default
	
	$db1 = $ci->db1->database;
    $db2 = $ci->db2->database;
    $ci->db1->where(['parent' => $parentID, 'status' => "1"]);
    $dataMenu = $ci->db1->get("bd_menu_master")->result();
    return ($dataMenu) ? $dataMenu : null;
}

function getChildSubMenuByChildID($clildID) {
    $ci = & get_instance();
	$ci->db1 = $ci->load->database('online', TRUE);//sentrifugo
    $ci->db2 = $ci->load->database('another_db', TRUE);//bdtechno  ==default
	
	$db1 = $ci->db1->database;
    $db2 = $ci->db2->database;
    $ci->db1->where(['child' => $parentID, 'status' => "1"]);
    $dataMenu = $ci->db1->get("bd_menu_master")->result();
    return ($dataMenu) ? $dataMenu : null;
}

function checkRolePermission($accessurl) {
    $ci = & get_instance();
    $LoginUserRole = $ci->session->userdata('assign_role');
    if ($LoginUserRole == "1") {
        $accesArr = array("add_project", "project_data_ajax", "editproject", "district_management", "district_data_ajax", "editdistrict", "component_management", "component_data_ajax", "edit_component");
    }
    if (in_array($accessurl, $accesArr)) {
        return true;
    } else {
        redirect(base_url("access_denied"));
    }
}

// Get Single Rec Details By Clearnce ID..
function GetSingleRec_Clearance($tblWorkID) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['work_id' => $tblWorkID, 'status' => "1"]);
    $dataMenu = $ci->db->get('clearance')->row();
    return ($dataMenu) ? $dataMenu : null;
}

function bd_rolecheck() {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->bd_rolecheck();
    return ($details) ? $details : '';
}

// Get Single Rec Details By Physical Progress ID..
function GetSingleRec_PhysicalProgress($tblWorkID) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['work_id' => $tblWorkID, 'status' => "1"]);
    $dataMenu = $ci->db->get('physical_progress_details')->row();
    return ($dataMenu) ? $dataMenu : null;
}

//Get Profile Details..
function GetBasicRecLoginUser() {
    $ci = & get_instance();
    $ci->load->database();
	$ci->load->model('mastermodel');
    $dataRec = $ci->mastermodel->GetBasicRecLoginUser();
    return ($dataRec) ? $dataRec : null;
}

//code by gaurav
function get_emp_dpt_data($id) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select("department_id");
    $ci->db->from("main_employees_summary");
    $ci->db->where(array('isactive' => '1', 'user_id' => $id));
    $data = $ci->db->get()->result();
    return ($data) ? $data : null;
}

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if (getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if (getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if (getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if (getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if (getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

//All Prefix Master..
function GetAllPrefixRec() {
    $CI = & get_instance();
    $CI->load->model('Front_model');
    $details = $CI->Front_model->GetAllBdPrefixRec();
    return ($details) ? $details : '';
}


?>