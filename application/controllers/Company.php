<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Company extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Front_model');
        $this->load->model('SecondDB_model');
        $this->load->model('mastermodel');
        $this->load->model('company_model');
        $this->load->model('website_model');
        $this->load->model('cegexp_model');
        $this->load->model('tender_model');
        $this->load->model('Bidprojectrfp_model');
        $this->load->helper(array('form', 'url'));
        
        $this->load->library('session', 'form_validation');
        if (!($this->session->userdata('loginid'))) {
            redirect(base_url());
        }
        $this->db2 = $this->load->database('another_db', TRUE);
        $db2 = $this->db2->database;
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    //Dashbord After Login..
    public function add() {
        $data['title'] = 'Add Company';
        $this->form_validation->set_rules('company_name', 'Company Name', 'required|min_length[3]|max_length[100]|is_unique[main_company.company_name]');
        // $this->form_validation->set_rules('projectcode', 'Project Code', 'required|is_unique[cegth_project.projectcode]');
        if ($this->form_validation->run() == FALSE) {
            $data['sectArr_rec'] = $this->mastermodel->SelectRecord('designation_sector', array('status' => '1'), 'sector_name', 'ASC');
            $data['comp_field_Arr'] = $this->mastermodel->SelectRecordFld('comp_field_master', array('status' => '1'), 'field_name', 'ASC');
            //All Country..
            $data['country_Arr'] = $this->mastermodel->SelectRecordC('countries', array('status' => '1'));
            $this->load->view('company/addcompany_view', $data);
        } else {
            $_REQUEST['entryby'] = $this->session->userdata('loginid');
            $_REQUEST['operating_countries'] = implode(",", $_REQUEST['operating_countries']);
            $_REQUEST['com_sector'] = implode(",", $_REQUEST['com_sector']);
            $_REQUEST['comp_field_id'] = implode(",", $_REQUEST['comp_field_id']);
            $respon = $this->mastermodel->InsertMasterData($_REQUEST, 'main_company');
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Company Added successfully');
                redirect(thisurl());
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(thisurl());
            endif;
        }
    }

    //Add or Save.. Sector..
    public function addsector() {
        $this->form_validation->set_rules('sector_name', 'Sector Name', 'required|min_length[3]|max_length[100]|is_unique[designation_sector.sector_name]');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_msg', 'Something went gone wrong');
            redirect(base_url('company/add'));
        } else {
            $respon = $this->mastermodel->InsertMasterData($_REQUEST, 'designation_sector');
            if ($respon):
                $this->session->set_flashdata('error_msg', null);
                $this->session->set_flashdata('success_msg', 'Sector Added successfully');
                redirect(base_url('company/add'));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('company/add'));
            endif;
        }
    }

    //Add Field Name or Nature of Company..
    public function addfield_name() {
        $this->form_validation->set_rules('field_name', 'Field Name', 'required|min_length[3]|max_length[100]|is_unique[comp_field_master.field_name]');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_msg', 'Something went gone wrong');
            redirect(base_url('company/add'));
        } else {
            $respon = $this->mastermodel->InsertMasterData($_REQUEST, 'comp_field_master');
            if ($respon):
                $this->session->set_flashdata('error_msg', null);
                $this->session->set_flashdata('success_msg', 'Field Added successfully');
                redirect(base_url('company/add'));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('company/add'));
            endif;
        }
    }

    //Save JV Record Code By Asheesh
    public function savejvdata() {






        if ($_REQUEST['cegexp_id']) {
            $insertedArr = array(
                'lead_companes' => implode(",", $_REQUEST['leadcompany']),
                'joint_ventures' => implode(",", $_REQUEST['joint_venture']),
                'associate_company' => implode(",", $_REQUEST['assoc']),
                'cegexp_id' => $_REQUEST['cegexp_id'],
                'entry_by' => $this->session->userdata('empid'));
 
            $this->db->where('project_id', $_REQUEST['tender_id']);
            $query = $this->db->get('jv_companyrecords');
            
            if ($query->num_rows() > 0){
               
                $respon = $this->mastermodel->UpdateRecords('jv_companyrecords', array('project_id' =>  $_REQUEST['tender_id']), $insertedArr);
            }
            else{
                
                $respon = $this->mastermodel->InsertMasterData($insertedArr, 'jv_companyrecords');
            }



            if ($respon):
                // $this->session->set_flashdata('success_msg', 'Consortium Added successfully');
                // redirect(base_url('company/cegexpedit/' . $_REQUEST['cegexp_id']));
                $message = "Consortium Added successfully";
            else:
                // $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                // redirect(base_url('company/cegexpedit/' . $_REQUEST['cegexp_id']));
                $message = "Something went gone wrong";


            endif;
        } else {

            // echo "<pre/>";
            // print_r($_REQUEST);
            // die;
            $insertedArr = array(
                'lead_companes' => implode(",", $_REQUEST['leadcompany']),
                'joint_ventures' => implode(",", $_REQUEST['joint_venture']),
                'associate_company' => implode(",", $_REQUEST['assoc']),
                'project_id' => $_REQUEST['tender_id'],
                'entry_by' => $this->session->userdata('loginid'));

            $this->db->where('project_id', $_REQUEST['tender_id']);
            $query = $this->db->get('jv_companyrecords');

            if ($query->num_rows() > 0){
               
                $respon = $this->mastermodel->UpdateRecords('jv_companyrecords', array('project_id' =>  $_REQUEST['tender_id']), $insertedArr);
            }
            else{
                
                $respon = $this->mastermodel->InsertMasterData($insertedArr, 'jv_companyrecords');
            }
           



            if ($respon):
                // $this->session->set_flashdata('success_msg', 'Consortium Added successfully');
                // redirect(base_url('togoproject'));
                $message = "Consortium Added successfully";
            else:
                // $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                // redirect(base_url('togoproject'));
                $message = "Something went gone wrong";
            endif;
        }

        // print_r($this->db->last_query());
        // die;

        $arrMessage = array(
            'msg' => $message,
        );

        echo json_encode($arrMessage);
    }

    //Ajax For Show If Exist..
    public function showjvrecord() {
        $projId = $_REQUEST['jvprojid'];
        $Rec = $this->Front_model->selectRecord('jv_companyrecords', array('*'), array('status' => '1', 'project_id' => $projId));
        if ($Rec) {
            $recData = $Rec->result();
            $LC = '';
            $jv = '';
            $as = '';
            $lead_companes = explode(",", $recData[0]->lead_companes);
            $joint_ventures = explode(",", $recData[0]->joint_ventures);
            $associate_company = explode(",", $recData[0]->associate_company);
            $entry_by = $recData[0]->entry_by;
            foreach ($joint_ventures as $r11) {
                $jv .= $this->mastermodel->GetCompNameById($r11) . " , ";
            }
            foreach ($lead_companes as $r12) {
                $LC .= $this->mastermodel->GetCompNameById($r12) . " , ";
            }
            foreach ($associate_company as $r13) {
                $as .= $this->mastermodel->GetCompNameById($r13) . " , ";
            }
            ?>
            <table class="table table-striped">
                <thead><tr><th>Lead Company</th></tr></thead>  
                <tbody><tr><td><?= $LC ?></td></tr></tbody>
                <thead><tr><th> JV ( joint venture )</th></tr></thead>
                <tbody><tr><td><?= $jv ?></td></tr></tbody>
                <thead><tr><th>Associate company.</th></tr></thead>
                <tbody><tr><td><?= $as ?></td></tr></tbody>
            </table>
            <?php
        } else {
            echo null;
        }
    }

    //Compitior View By Project Id Ajax
    public function compititorview_ajax() {
        if ($_REQUEST['projid']):
            $projectID = $_REQUEST['projid'];
            $Rec = $this->Front_model->selectRecord('cegexp_competitor_comp', array('compt_comp_id'), array('status' => '1', 'project_id' => $projectID));
            if ($Rec) {
                $recData = $Rec->result();
                foreach ($recData as $rroww) {
                    echo "<span class='btn btn-link' >" . ucwords(strtolower($this->mastermodel->GetCompNameById($rroww->compt_comp_id))) . "</span>, ";
                }
            }
        endif;
        exit();
    }

    //Ceg Exp..
    public function cegexp() {
        $data['error'] = '';
        $data['title'] = ' CEG Experience List';
        $data['service'] = $this->mastermodel->SelectRecord('main_services', array('is_active' => '1'));
        $this->load->view('company/exp_list_view', $data);
    }

    //Save Compititors..
    function savecomptitors() {
        //echo '<pre>'; print_r($_REQUEST); die;
        if (($_REQUEST['comptitors']) and ( $_REQUEST['projid'])):
            foreach ($_REQUEST['comptitors'] as $comptrow) {
                $count = $this->mastermodel->count_rows('competitors_company', array('project_id' => $_REQUEST['projid'], 'compt_com_id' => $comptrow));
                if ($count < 1):
                    $IncrtArr = array(
                        'project_id' => $_REQUEST['projid'],
                        'compt_com_id' => $comptrow,
                        'entry_by' => $this->session->userdata('loginid'));
                    $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'competitors_company');
                endif;
            }
            if ($respon):
                $this->session->set_flashdata('msg', 'Competitors Record Added successfully');
                redirect(base_url('completeproject'));

            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('completeproject'));
            endif;
        endif;
    }

    // Project Display
    public function companyListAll() {
        $list = $this->company_model->get_datatables();
        //echo '<pre>';
        //print_r($list);
        //die;
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';
        foreach ($list as $comprow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ucwords(strtolower($comprow->company_name));
            $row[] = $this->mastermodel->GetnatureofCompanyById($comprow->comp_field_id);
            //$row[] = '';
            $row[] = "<a style='font-size:10px;' target='_blank' href='" . $comprow->company_website . "'>" . $comprow->company_website . "</a>";
            // $row[] = '';
            //$row[] = '';
            $row[] = $this->mastermodel->GetSectNameByIdC($comprow->com_sector);
            $row[] = $this->mastermodel->GetcountryByHOId($comprow->country);
            $row[] = $comprow->email_id;
            $row[] = $comprow->contact_no;
            $row[] = "<a href='" . base_url("company/addcompany_extra?compnm=" . $comprow->fld_id) . "'>Edit</a>" . "&nbsp&nbsp&nbsp&nbsp<a href='" . base_url("company/contdelete?delid=" . $comprow->fld_id) . "'>Delete</a>";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->company_model->count_all(),
            "recordsTotal" => $this->company_model->count_filtered(),
            "recordsFiltered" => $this->company_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }


    public function fetchProjectData() {

        $projid = $this->input->post('projid');
        $tenderDetail = $this->company_model->getTenderDetail($projid);
        // print_r($tenderDetail); die; 
        
        echo json_encode($tenderDetail);
    }

    // Project Display
    public function websiteListAll() {
        $list = $this->website_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';

        foreach ($list as $comprow) {
            $actualink = $comprow->website_url;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = "<a title='" . $actualink . "' target='_blank' href='" . $actualink . "'>Website</a>";
            $row[] = $comprow->department;
            $row[] = "<a title='" . $comprow->eproc . "' target='_blank' href='" . $comprow->eproc . "'>Eproc</a>";
            $row[] = $comprow->user_name;
            $row[] = '<i onclick="showpass(' . "'" . $comprow->password_encode . "','" . $comprow->fld_id . "'" . ')" title="' . $comprow->password_encode . '" class="glyphicon glyphicon-eye-open"></i>' . '<span id="dv' . $comprow->fld_id . '"></span>';
            $row[] = $comprow->country;
            $row[] = "<a class='glyphicon glyphicon-edit' href='" . base_url("company/websiteedit/" . $comprow->fld_id) . "'></a>" . "&nbsp&nbsp&nbsp&nbsp<a class='glyphicon glyphicon-trash' href='" . base_url("company/websitedelete/" . $comprow->fld_id) . "'></a>";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->website_model->count_all(),
            "recordsTotal" => '0',
            "recordsFiltered" => $this->website_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    //CEG Exp List data Table..
    public function expListAll() {
        $list = $this->cegexp_model->get_datatables();
        //echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';
        foreach ($list as $comprow) {
            $projectname = $this->SecondDB_model->getProjectBYId($comprow->project_numberid);
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $comprow->TenderDetails;
            $row[] = $projectname->project_name;
            $row[] = $comprow->service_name;
            $row[] = $comprow->client;
            $row[] = $comprow->start_year;
            $row[] = $comprow->sectName;
            $row[] = $comprow->state_name;
            $row[] = $comprow->funding;
            $row[] = '<a href="cegexpedit/' . $comprow->project_id . '"><i title="EDIT" class="glyphicon glyphicon-edit icon-white"></i></a>&nbsp&nbsp';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => '0',
            "recordsFiltered" => $this->cegexp_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }



   public function removeDelcFunc(){
                $message = "Some issues occured !";
                if (isset($_REQUEST['delc'])):
                $respp = $this->mastermodel->UpdateRecords('cegexp_competitor_comp', array('fld_id' => $_REQUEST['delc']), array('status' => '0'));
                // $this->session->set_flashdata('msg', 'Record Deleted Successfully');
                // redirect(base_url('company/cegexpedit/' . $cegexpID));
                $message = "Record deleted Successfully";
                endif;

                $arrMessage = array(
                'msg' => $message,
                );

                echo json_encode($arrMessage);
    }

      //Edit CEG Exp.. Code By Asheesh.. 
    public function cegexpedit() {
        $data['title'] = "Submitted Project";
        $data['error'] = '';
        $projstatus = '';
        $compnameArr = array();
        $cegexpID = $this->uri->segment(3);
 
        if ($cegexpID) {
            // Find Data
            $data['bddetail'] = $this->mastermodel->SelectRecordSingle('bd_tenderdetail', array('fld_id' => $cegexpID));
 

            $dataBd = $this->Bidprojectrfp_model->getSingleData($cegexpID);

            $data['getSingleData'] = $dataBd[0];

            $pm_id = $dataBd[0]->assign_to;




            $data['project_status'] = $this->mastermodel->SelectRecordSingle('bdproject_status', array('project_id' => $cegexpID));
            //bdcegexp data
            $this->db->select('a.*,c.service_name,d.generated_tenderid');
            $this->db->from('bdceg_exp as a');
            $this->db->join('main_services as c', 'a.service_id = c.id', 'left');
            $this->db->join('tender_generated_id as d', 'a.project_id = d.project_id', 'left');
            $this->db->where('a.project_id', $cegexpID);
            $resultdata = $this->db->get()->result();
            $data['expres'] = $resultdata[0];

            $data['projdetail'] = $this->mastermodel->SelectRecordSingle('project_description', array('project_id' => $cegexpID));
            $country_id = $data['projdetail']->country_id;
            $bid_securitytype = $data['projdetail']->bid_securitytype;
            $sectors = $data['projdetail']->sector_id;
            $service_id = $data['projdetail']->service_id;

           


          



            
            $data['stateArr'] = $this->mastermodel->SelectRecordS('states', array('country_id' => $country_id, 'status' => '1'));
            
            $data['securitytype'] = $this->mastermodel->SelectRecordSingle('bidsecurity_type', array('id' => $bid_securitytype, 'is_active' => '1'));
           
            $data['project'] = $this->SecondDB_model->getProjectName();
            
             
            $data['cegpds'] = $this->mastermodel->SelectRecordSingle('ceg_pds_detail', array('project_id' => $cegexpID));
           
            $data['expsummery'] = $this->mastermodel->SelectRecordSingle('bdcegexp_proj_summery', array('project_id' => $cegexpID, 'status' => '1'));
            $data['sectorname'] = $this->mastermodel->SelectRecordSingle('sector', array('fld_id' => $sectors));
            $data['service'] = $this->mastermodel->SelectRecordSingle('main_services', array('id' => $service_id));
            
            $data['projectname'] = $this->SecondDB_model->getProjectBYId($data['expsummery']->project_numberid);
            
            $data['cegcompcomp'] = $this->mastermodel->SelectRecordFldNew('cegexp_competitor_comp', array('project_id' => $cegexpID, 'status' => '1'));
          
            $data['tqRecArr'] = $this->mastermodel->SelectRecord('team_assign_member', array('status' => '1', 'project_id' => $cegexpID));
           
            $data['mailby'] = $this->SecondDB_model->getUserByID($this->session->userdata('uid'));

            // if (isset($_REQUEST['delc'])):
            //     $respp = $this->mastermodel->UpdateRecords('cegexp_competitor_comp', array('fld_id' => $_REQUEST['delc']), array('status' => '0'));
            //     $this->session->set_flashdata('msg', 'Record Deleted Successfully');
            //     redirect(base_url('company/cegexpedit/' . $cegexpID));
            // endif;

            

            
            //Summery...
            $respCount = $this->mastermodel->count_allByCond('cegexp_proj_summery', array('cegexp_id' => $cegexpID, 'status' => '1'));
            if ($respCount < 1):
                $this->mastermodel->InsertMasterData(array('cegexp_id' => $cegexpID), 'cegexp_proj_summery');
            endif;

            $Rec = $this->Front_model->selectRecord('main_company', array('fld_id', 'company_name'), array('status' => '1'));

            if ($Rec != '') {
                $data['compnameArr'] = $Rec->result();
            }

            //Edit Proc..
            $data['cegexpSummery'] = $this->mastermodel->SelectRecordSingle('cegexp_proj_summery', array('cegexp_id' => $cegexpID, 'status' => '1'));
            $data['cegexpRec'] = $this->mastermodel->SelectRecordSingle('ceg_exp', array('fld_id' => $cegexpID));
            $data['country_Arr'] = $this->mastermodel->SelectRecordC('countries', array('status' => '1'));
            $data['security_Type'] = $this->mastermodel->SelectRecord('bidsecurity_type', array('is_active' => '1'));
            $countID = $data['cegexpRec']->countries_id;
            $ProjId = $this->mastermodel->ProjIdByGenId($data['cegexpRec']->project_code);
            @$consortiumRecArr = $this->mastermodel->SelectRecordSingle('jv_companyrecords', array('project_id' => $ProjId, 'status' => '1'));
            $projectID = $data['cegexpRec']->project_id;

            $data['ceglane'] = $this->mastermodel->SelectRecord('ceg_lane', array('project_id' => $cegexpID));
            $data['sector'] = $this->Front_model->GetActiveSector();
            $data['serviceArr'] = $this->Front_model->mainServices();

            $data['servicename'] = $this->mastermodel->SelectRecordSingle('main_services', array('id' => $data['cegexpRec']->service_id));
            if ($consortiumRecArr):
                $data['consortiumArr'] = $consortiumRecArr;
            else:
                $data['consortiumArr'] = $this->mastermodel->SelectRecordSingle('jv_companyrecords', array('cegexp_id' => $cegexpID, 'status' => '1'));
            endif;

            



            if ($_REQUEST['mailsender']):


                // echo "<pre>";
                // print_r($data); die;
                $req = $_REQUEST['mailsender'];
                $this->mailSender($req, $data);
               
            endif;

            if ($_REQUEST['mailsendercom']):
                // echo "<pre>";
                // print_r($data); die;

                $req = $_REQUEST['mailsendercom'];
                $this->mailSenderCom($req, $data);

            endif;
            $data['chk8020'] = $this->mastermodel->SelectRecord('mail_sendornot_chk', array('bd_projid' => $cegexpID, 'mail_type' => '8020'));
            $data['chkcompetitor'] = $this->mastermodel->SelectRecord('mail_sendornot_chk', array('bd_projid' => $cegexpID, 'mail_type' => 'Comptitor'));

// ##########################################################
//********** Mail Send Last Update Date By Asheesh ********** Close
//###########################################################//
/// code by durgesh 
//Code By Asheesh... Coordinator , Proj Name of Eng..

            $db1 = $this->db1->database;
            $db2 = $this->db2->database;
            $this->db->SELECT("$db1.main_users.userfullname");
            $this->db->FROM("$db2.project_assign_engnr");
            $this->db->join("$db1.main_users", "$db1.main_users.id=$db2.project_assign_engnr.emp_id", "LEFT");
            $this->db->WHERE(array("$db2.project_assign_engnr.bd_project_id" => $cegexpID, "$db2.project_assign_engnr.status" => '1'));
            $engnrArr = $this->db->get()->row();
            $data['recname_eng'] = ($engnrArr) ? $engnrArr->userfullname : '';


 
            $db2 = $this->db1->database;
            $db1 = $this->db2->database;
//Project Coodinator Get...
            $this->db->SELECT("$db2.main_users.userfullname");
            $this->db->FROM("$db1.project_coordinator");
            $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.project_coordinator.emp_id", "LEFT");
            $this->db->WHERE(array("$db1.project_coordinator.bd_project_id" => $cegexpID, "$db1.project_coordinator.status" => '1'));
            $coordArr = $this->db->get()->row();
            $data['recname_coordinator'] = ($coordArr) ? $coordArr->userfullname : '';
           
           
//Get By Project Coordinator Durgesh...   
            $this->db->select('emp_id');
            $this->db->from('project_coordinator');
            $this->db->where(array('bd_project_id' => $cegexpID, 'status' => 1));
            $coordinator_result = $this->db->get()->row_array();
            $data['coordinator_row'] = ($coordinator_result) ? $coordinator_result : '';


//Get By Project engineer Durgesh...   
            $this->db->select('emp_id');
            $this->db->from('project_assign_engnr');
            $this->db->where(array('bd_project_id' => $cegexpID, 'status' => 1));
            $engineer_result = $this->db->get()->row_array();
            $data['engineer_row'] = ($engineer_result) ? $engineer_result : '';


//Financial details Get By Durgesh...   
            $this->db->select('*');
            $this->db->from('proj_financial_details');
            $this->db->where(array('bd_projid' => $cegexpID, 'status' => 1));
            $financial_result = $this->db->get()->row();
            $data['financial_record'] = ($financial_result) ? $financial_result : '';

//Performance BD Details Get By Durgesh...   
            $this->db->select('*');
            $this->db->from('proj_performance_Bd_details');
            $this->db->where(array('bd_projid' => $cegexpID, 'status' => 1));
            $performance_bd_result = $this->db->get()->row();
            $data['perf_bd_record'] = ($performance_bd_result) ? $performance_bd_result : '';

//Edit/Update TL And Ofc Manager Section..
            $this->db->select("$db2.main_employees_summary.user_id,$db2.main_employees_summary.userfullname,$db2.main_employees_summary.employeeId");
            $this->db->from("$db2.main_employees_summary");
            $this->db->where("($db2.main_employees_summary.businessunit_id='1' OR $db2.main_employees_summary.businessunit_id='3')", NULL, FALSE);
            $this->db->where("$db2.main_employees_summary.isactive", '1');
            $this->db->order_by("$db2.main_employees_summary.userfullname", "ASC");
            $empArr = $this->db->get()->result();
            $data['empListArr'] = ($empArr) ? $empArr : '';

//Fetch Details of TL ..
            $this->db->SELECT("$db2.main_employees_summary.position_name,$db2.main_users.userfullname,$db2.main_users.contactnumber");
            $this->db->FROM("$db1.cegexp_proj_cont_persdetails");
            $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.cegexp_proj_cont_persdetails.emp_id", "LEFT");
            $this->db->join("$db2.main_employees_summary", "$db2.main_employees_summary.user_id=$db1.cegexp_proj_cont_persdetails.emp_id", "LEFT");
            $this->db->WHERE(array("$db1.cegexp_proj_cont_persdetails.bd_projid" => $cegexpID, "$db1.cegexp_proj_cont_persdetails.status" => '1', "$db1.cegexp_proj_cont_persdetails.cont_pers_typer" => 'tl'));
            $projTLEmp = $this->db->get()->row();
            $data['projtlemp'] = ($projTLEmp) ? $projTLEmp : '';
            //$data['projtlemp'] = ($projTLEmp) ? $projTLEmp->userfullname . " ( <small style='color:green'>" . $projTLEmp->contactnumber . " )</small>" : '';
//Fetch Details of Project Ofc Manager ..
            $this->db->SELECT("$db2.main_employees_summary.position_name,$db2.main_users.id,$db2.main_users.userfullname,$db2.main_users.contactnumber");
            $this->db->FROM("$db1.cegexp_proj_cont_persdetails");
            $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.cegexp_proj_cont_persdetails.emp_id", "LEFT");
            $this->db->join("$db2.main_employees_summary", "$db2.main_employees_summary.user_id=$db1.cegexp_proj_cont_persdetails.emp_id", "LEFT");
            $this->db->WHERE(array("$db1.cegexp_proj_cont_persdetails.bd_projid" => $cegexpID, "$db1.cegexp_proj_cont_persdetails.status" => '1', "$db1.cegexp_proj_cont_persdetails.cont_pers_typer" => 'ofc_mngr'));
            $projTLEmp = $this->db->get()->row();
            $data['projofc_mngremp'] = ($projTLEmp) ? $projTLEmp : '';
            //$data['projofc_mngremp'] = ($projTLEmp) ? $projTLEmp->userfullname . " ( <small style='color:green'>" . $projTLEmp->contactnumber . " )</small>" : '';
//Fetch Details..

            $this->db->select("$db2.tm_projects.id,$db2.tm_projects.project_name");
            $this->db->from("$db2.tm_projects");
            $this->db->where(array("$db2.tm_projects.is_active" => '1'));
            // $this->db->where("($db2.tm_projects.project_category='ie' OR $db2.tm_projects.project_category='ae')", NULL, FALSE);
            $this->db->order_by("$db2.tm_projects.project_name", 'ASC');
            $data['ProjRec'] = $this->db->get()->result();

//Client Address Details...
            $data['clientaddrsdetails'] = $this->Getclient_addbyid($cegexpID);

//Get Client Address Details...
            $data['getclientaddrsdetails'] = $this->db->get_where(' bdceg_exp_clientmore_details', array('bdproj_id' => $cegexpID))->row_array();

//Get project Contract personal Details...	
            $this->db->select("$db2.main_employees_summary.position_name,$db1.cegexp_proj_cont_persdetails.emp_id as con_emp_id,$db1.cegexp_proj_cont_persdetails.cont_pers_typer as con_per_type,$db1.project_coordinator.emp_id as coor_emp_id,$db1.project_assign_engnr.emp_id as eng_emp_id");
            $this->db->from("$db1.cegexp_proj_cont_persdetails");
            $this->db->where("$db1.cegexp_proj_cont_persdetails.bd_projid", $cegexpID);
            $this->db->join("$db1.project_coordinator", "$db1.project_coordinator.bd_project_id=$db1.cegexp_proj_cont_persdetails.bd_projid", "inner");
            $this->db->join("$db1.project_assign_engnr", "$db1.project_assign_engnr.bd_project_id=$db1.cegexp_proj_cont_persdetails.bd_projid", "inner");
            $this->db->join("$db2.main_employees_summary", "$db2.main_employees_summary.user_id=$db1.cegexp_proj_cont_persdetails.emp_id", 'inner');
            $data['proj_con_per_detail'] = $this->db->get()->row_array();

//Project Ofc Address..
            $data['projoffcAddress'] = $this->Getprojofcaddress_addbyid($cegexpID);
            $data['hrmsprojid'] = $this->getprojidbdtohrms($cegexpID);


// code by durgesh for Get Detail About the lead, Jv, Associate Company.....            
            $this->db->select('b.company_name,a.project_id,a.compt_comp_id');
            $this->db->from('cegexp_competitor_comp as a');
            $this->db->where(array('a.project_id' => $cegexpID, 'a.status' => 1));
            $this->db->join('main_company as b', 'a.compt_comp_id=b.fld_id', 'inner');
            $comp_record = $this->db->get()->result();
            $data['competitor_jv_record'] = ($comp_record) ? $comp_record : '';

            $data['Jv_Lead_Associate_Comp'] = $this->mastermodel->GetCompNameDetailById('74', $cegexpID);
            
//Get Staffs Arr Code By Asheesh...
            $data['keyProfStaffArr'] = $this->mastermodel->GetStaffByProjID($cegexpID, "1");
           
            $data['subProfessionalStaffArr'] = $this->mastermodel->GetStaffByProjID($cegexpID, "2");
            $data['adminStaffArr'] = $this->mastermodel->GetStaffByProjID($cegexpID, "3");

//March 2019..
            $this->db->select("$db1.proj_keydate_details.*");
            $this->db->from("$db1.proj_keydate_details");
            $this->db->where(array("$db1.proj_keydate_details.bd_projid" => $cegexpID, "$db1.proj_keydate_details.status" => "1"));
            $data['ReckeydateSingle'] = $this->db->get()->row();

            //Get All  Ofc Details....
            $this->db->SELECT("$db1.proj_siteofc_detail.*,$db2.countries.country_name,$db2.states.state_name,$db2.cities.city_name,$db2.main_employees_summary.userfullname as ofc_manager_userfullname,$db2.main_employees_summary.contactnumber as ofc_manager_contactnumber,$db2.main_employees_summary.emailaddress as ofc_manager_emailaddress,$db2.main_employees_summary.department_name as ofc_manager_department_name,$db2.d.userfullname as contper_userfullname,$db2.d.contactnumber as contper_contactnumber,$db2.d.emailaddress as contper_emailaddress,$db2.d.department_name as contper_department_name");
            $this->db->FROM("$db1.proj_siteofc_detail");
            $this->db->Join("$db2.countries", "$db1.proj_siteofc_detail.countries_idnew = $db2.countries.country_id", "LEFT");
            $this->db->Join("$db2.states", "$db1.proj_siteofc_detail.state_idnew = $db2.states.state_id", "LEFT");
            $this->db->Join("$db2.cities", "$db1.proj_siteofc_detail.city_idnew = $db2.cities.city_id", "LEFT");
            $this->db->Join("$db2.main_employees_summary", "$db1.proj_siteofc_detail.ofc_mnger_empid = $db2.main_employees_summary.user_id", "LEFT");
            $this->db->Join("$db2.main_employees_summary as d", "$db1.proj_siteofc_detail.cont_persname = $db2.d.user_id", "LEFT");
            $this->db->WHERE(array("$db1.proj_siteofc_detail.project_id" => $cegexpID, "$db1.proj_siteofc_detail.status" => "1"));
            $this->db->order_by("$db1.proj_siteofc_detail.fld_id", "DESC");
            $data['RecSiteOfcDetails'] = $this->db->get()->result();

            //Get single  Ofc Details....
            $this->db->SELECT("$db2.main_employees_summary.user_id,$db1.proj_siteofc_detail.*,$db2.countries.country_id,$db2.countries.country_name,$db2.states.state_name,$db2.cities.city_name,$db2.main_employees_summary.userfullname as ofc_manager_userfullname,$db2.main_employees_summary.contactnumber as ofc_manager_contactnumber,$db2.main_employees_summary.emailaddress as ofc_manager_emailaddress,$db2.main_employees_summary.department_name as ofc_manager_department_name,$db2.d.userfullname as contper_userfullname,$db2.d.contactnumber as contper_contactnumber,$db2.d.emailaddress as contper_emailaddress,$db2.d.department_name as contper_department_name");
            $this->db->FROM("$db1.proj_siteofc_detail");
            $this->db->Join("$db2.countries", "$db1.proj_siteofc_detail.countries_idnew = $db2.countries.country_id", "LEFT");
            $this->db->Join("$db2.states", "$db1.proj_siteofc_detail.state_idnew = $db2.states.state_id", "LEFT");
            $this->db->Join("$db2.cities", "$db1.proj_siteofc_detail.city_idnew = $db2.cities.city_id", "LEFT");
            $this->db->Join("$db2.main_employees_summary", "$db1.proj_siteofc_detail.ofc_mnger_empid = $db2.main_employees_summary.user_id", "LEFT");
            $this->db->Join("$db2.main_employees_summary as d", "$db1.proj_siteofc_detail.cont_persname = $db2.d.user_id", "LEFT");
            $this->db->WHERE(array("$db1.proj_siteofc_detail.project_id" => $cegexpID, "$db1.proj_siteofc_detail.status" => "1"));
            $this->db->order_by("$db1.proj_siteofc_detail.fld_id", "DESC");












            $data['RecSingleSiteOfcDetails'] = $this->db->get()->row_array();

            
            ###############

            $clientName = $this->Front_model->selectRecord('bd_client_master', array('id', 'client_name'), array('status' => '0'));
            if ($clientName != '') {
                $clientName = $clientName->result();
                $data['clientName'] = $clientName;
            }
    
            $fundingOrg = $this->Front_model->selectRecord('bd_funding_master', array('id', 'funding_org'), array('status' => '0'));
            if ($fundingOrg != '') {
                $fundingOrg = $fundingOrg->result();
                $data['fundingOrg'] = $fundingOrg;
            }




            $consortiumData = $this->mastermodel->SelectRecordSingle('jv_companyrecords', array('project_id' => $cegexpID));

            $data['consortiumData'] = $consortiumData;

            $clientcontactData = $this->mastermodel->SelectRecordSingle('clientcontact', array('project_id' => $cegexpID));

            $data['clientcontactData'] = $clientcontactData;



            $personelContactData = $this->mastermodel->SelectRecordSingle('cegexp_proj_cont_persdetails', array('bd_projid' => $cegexpID));

            $data['personelContactData'] = $personelContactData;

            
            
           
            //print_r($data['bddetail']); die;
            
            $this->db1->select('a.*');
            $this->db1->from('main_users as a');
            $this->db1->where('a.isactive', '1');
            $this->db1->where('a.id', $pm_id);
            $Rec = $this->db1->get()->result();
            $data['getProposalManager'] = $Rec[0];


            
     
            ################


            

            $this->load->view('company/cegexp_editproj_view', $data);
        }
        else {
            redirect(base_url('company'));
        }
    }




public function cegexpeditMail() {
        $data['title'] = "Submitted Project";
        $data['error'] = '';
        $projstatus = '';
        $compnameArr = array();
        $cegexpID = $_REQUEST['project_id'];
 
        if ($cegexpID) {
            // Find Data
            $data['bddetail'] = $this->mastermodel->SelectRecordSingle('bd_tenderdetail', array('fld_id' => $cegexpID));
            $data['project_status'] = $this->mastermodel->SelectRecordSingle('bdproject_status', array('project_id' => $cegexpID));
            //bdcegexp data
            $this->db->select('a.*,c.service_name,d.generated_tenderid');
            $this->db->from('bdceg_exp as a');
            $this->db->join('main_services as c', 'a.service_id = c.id', 'left');
            $this->db->join('tender_generated_id as d', 'a.project_id = d.project_id', 'left');
            $this->db->where('a.project_id', $cegexpID);
            $resultdata = $this->db->get()->result();
            $data['expres'] = $resultdata[0];

            $data['projdetail'] = $this->mastermodel->SelectRecordSingle('project_description', array('project_id' => $cegexpID));
            $country_id = $data['projdetail']->country_id;
            $bid_securitytype = $data['projdetail']->bid_securitytype;
            $sectors = $data['projdetail']->sector_id;
            $service_id = $data['projdetail']->service_id;

           


          



            
            $data['stateArr'] = $this->mastermodel->SelectRecordS('states', array('country_id' => $country_id, 'status' => '1'));
            
            $data['securitytype'] = $this->mastermodel->SelectRecordSingle('bidsecurity_type', array('id' => $bid_securitytype, 'is_active' => '1'));
           
            $data['project'] = $this->SecondDB_model->getProjectName();
            
             
            $data['cegpds'] = $this->mastermodel->SelectRecordSingle('ceg_pds_detail', array('project_id' => $cegexpID));
           
            $data['expsummery'] = $this->mastermodel->SelectRecordSingle('bdcegexp_proj_summery', array('project_id' => $cegexpID, 'status' => '1'));
            $data['sectorname'] = $this->mastermodel->SelectRecordSingle('sector', array('fld_id' => $sectors));
            $data['service'] = $this->mastermodel->SelectRecordSingle('main_services', array('id' => $service_id));
            
            $data['projectname'] = $this->SecondDB_model->getProjectBYId($data['expsummery']->project_numberid);
            
            $data['cegcompcomp'] = $this->mastermodel->SelectRecordFldNew('cegexp_competitor_comp', array('project_id' => $cegexpID, 'status' => '1'));
          
            $data['tqRecArr'] = $this->mastermodel->SelectRecord('team_assign_member', array('status' => '1', 'project_id' => $cegexpID));
           
            $data['mailby'] = $this->SecondDB_model->getUserByID($this->session->userdata('uid'));

            // if (isset($_REQUEST['delc'])):
            //     $respp = $this->mastermodel->UpdateRecords('cegexp_competitor_comp', array('fld_id' => $_REQUEST['delc']), array('status' => '0'));
            //     $this->session->set_flashdata('msg', 'Record Deleted Successfully');
            //     redirect(base_url('company/cegexpedit/' . $cegexpID));
            // endif;

            

            
            //Summery...
            $respCount = $this->mastermodel->count_allByCond('cegexp_proj_summery', array('cegexp_id' => $cegexpID, 'status' => '1'));
            if ($respCount < 1):
                $this->mastermodel->InsertMasterData(array('cegexp_id' => $cegexpID), 'cegexp_proj_summery');
            endif;

            $Rec = $this->Front_model->selectRecord('main_company', array('fld_id', 'company_name'), array('status' => '1'));

            if ($Rec != '') {
                $data['compnameArr'] = $Rec->result();
            }

            //Edit Proc..
            $data['cegexpSummery'] = $this->mastermodel->SelectRecordSingle('cegexp_proj_summery', array('cegexp_id' => $cegexpID, 'status' => '1'));
            $data['cegexpRec'] = $this->mastermodel->SelectRecordSingle('ceg_exp', array('fld_id' => $cegexpID));
            $data['country_Arr'] = $this->mastermodel->SelectRecordC('countries', array('status' => '1'));
            $data['security_Type'] = $this->mastermodel->SelectRecord('bidsecurity_type', array('is_active' => '1'));
            $countID = $data['cegexpRec']->countries_id;
            $ProjId = $this->mastermodel->ProjIdByGenId($data['cegexpRec']->project_code);
            @$consortiumRecArr = $this->mastermodel->SelectRecordSingle('jv_companyrecords', array('project_id' => $ProjId, 'status' => '1'));
            $projectID = $data['cegexpRec']->project_id;

            $data['ceglane'] = $this->mastermodel->SelectRecord('ceg_lane', array('project_id' => $cegexpID));
            $data['sector'] = $this->Front_model->GetActiveSector();
            $data['serviceArr'] = $this->Front_model->mainServices();

            $data['servicename'] = $this->mastermodel->SelectRecordSingle('main_services', array('id' => $data['cegexpRec']->service_id));
            if ($consortiumRecArr):
                $data['consortiumArr'] = $consortiumRecArr;
            else:
                $data['consortiumArr'] = $this->mastermodel->SelectRecordSingle('jv_companyrecords', array('cegexp_id' => $cegexpID, 'status' => '1'));
            endif;

            



            if ($_REQUEST['mailsender']):


                // echo "<pre>";
                // print_r($data); die;
                $req = $_REQUEST['mailsender'];
                $this->mailSender($req, $data);
               
            endif;

            if ($_REQUEST['mailsendercom']):
                // echo "<pre>";
                // print_r($data); die;

                $req = $_REQUEST['mailsendercom'];
                $this->mailSenderCom($req, $data);

            endif;
            $data['chk8020'] = $this->mastermodel->SelectRecord('mail_sendornot_chk', array('bd_projid' => $cegexpID, 'mail_type' => '8020'));
            $data['chkcompetitor'] = $this->mastermodel->SelectRecord('mail_sendornot_chk', array('bd_projid' => $cegexpID, 'mail_type' => 'Comptitor'));

// ##########################################################
//********** Mail Send Last Update Date By Asheesh ********** Close
//###########################################################//
/// code by durgesh 
//Code By Asheesh... Coordinator , Proj Name of Eng..

            $db1 = $this->db1->database;
            $db2 = $this->db2->database;
            $this->db->SELECT("$db1.main_users.userfullname");
            $this->db->FROM("$db2.project_assign_engnr");
            $this->db->join("$db1.main_users", "$db1.main_users.id=$db2.project_assign_engnr.emp_id", "LEFT");
            $this->db->WHERE(array("$db2.project_assign_engnr.bd_project_id" => $cegexpID, "$db2.project_assign_engnr.status" => '1'));
            $engnrArr = $this->db->get()->row();
            $data['recname_eng'] = ($engnrArr) ? $engnrArr->userfullname : '';


 
            $db2 = $this->db1->database;
            $db1 = $this->db2->database;
//Project Coodinator Get...
            $this->db->SELECT("$db2.main_users.userfullname");
            $this->db->FROM("$db1.project_coordinator");
            $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.project_coordinator.emp_id", "LEFT");
            $this->db->WHERE(array("$db1.project_coordinator.bd_project_id" => $cegexpID, "$db1.project_coordinator.status" => '1'));
            $coordArr = $this->db->get()->row();
            $data['recname_coordinator'] = ($coordArr) ? $coordArr->userfullname : '';
           
           
//Get By Project Coordinator Durgesh...   
            $this->db->select('emp_id');
            $this->db->from('project_coordinator');
            $this->db->where(array('bd_project_id' => $cegexpID, 'status' => 1));
            $coordinator_result = $this->db->get()->row_array();
            $data['coordinator_row'] = ($coordinator_result) ? $coordinator_result : '';


//Get By Project engineer Durgesh...   
            $this->db->select('emp_id');
            $this->db->from('project_assign_engnr');
            $this->db->where(array('bd_project_id' => $cegexpID, 'status' => 1));
            $engineer_result = $this->db->get()->row_array();
            $data['engineer_row'] = ($engineer_result) ? $engineer_result : '';


//Financial details Get By Durgesh...   
            $this->db->select('*');
            $this->db->from('proj_financial_details');
            $this->db->where(array('bd_projid' => $cegexpID, 'status' => 1));
            $financial_result = $this->db->get()->row();
            $data['financial_record'] = ($financial_result) ? $financial_result : '';

//Performance BD Details Get By Durgesh...   
            $this->db->select('*');
            $this->db->from('proj_performance_Bd_details');
            $this->db->where(array('bd_projid' => $cegexpID, 'status' => 1));
            $performance_bd_result = $this->db->get()->row();
            $data['perf_bd_record'] = ($performance_bd_result) ? $performance_bd_result : '';

//Edit/Update TL And Ofc Manager Section..
            $this->db->select("$db2.main_employees_summary.user_id,$db2.main_employees_summary.userfullname,$db2.main_employees_summary.employeeId");
            $this->db->from("$db2.main_employees_summary");
            $this->db->where("($db2.main_employees_summary.businessunit_id='1' OR $db2.main_employees_summary.businessunit_id='3')", NULL, FALSE);
            $this->db->where("$db2.main_employees_summary.isactive", '1');
            $this->db->order_by("$db2.main_employees_summary.userfullname", "ASC");
            $empArr = $this->db->get()->result();
            $data['empListArr'] = ($empArr) ? $empArr : '';

//Fetch Details of TL ..
            $this->db->SELECT("$db2.main_employees_summary.position_name,$db2.main_users.userfullname,$db2.main_users.contactnumber");
            $this->db->FROM("$db1.cegexp_proj_cont_persdetails");
            $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.cegexp_proj_cont_persdetails.emp_id", "LEFT");
            $this->db->join("$db2.main_employees_summary", "$db2.main_employees_summary.user_id=$db1.cegexp_proj_cont_persdetails.emp_id", "LEFT");
            $this->db->WHERE(array("$db1.cegexp_proj_cont_persdetails.bd_projid" => $cegexpID, "$db1.cegexp_proj_cont_persdetails.status" => '1', "$db1.cegexp_proj_cont_persdetails.cont_pers_typer" => 'tl'));
            $projTLEmp = $this->db->get()->row();
            $data['projtlemp'] = ($projTLEmp) ? $projTLEmp : '';
            //$data['projtlemp'] = ($projTLEmp) ? $projTLEmp->userfullname . " ( <small style='color:green'>" . $projTLEmp->contactnumber . " )</small>" : '';
//Fetch Details of Project Ofc Manager ..
            $this->db->SELECT("$db2.main_employees_summary.position_name,$db2.main_users.id,$db2.main_users.userfullname,$db2.main_users.contactnumber");
            $this->db->FROM("$db1.cegexp_proj_cont_persdetails");
            $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.cegexp_proj_cont_persdetails.emp_id", "LEFT");
            $this->db->join("$db2.main_employees_summary", "$db2.main_employees_summary.user_id=$db1.cegexp_proj_cont_persdetails.emp_id", "LEFT");
            $this->db->WHERE(array("$db1.cegexp_proj_cont_persdetails.bd_projid" => $cegexpID, "$db1.cegexp_proj_cont_persdetails.status" => '1', "$db1.cegexp_proj_cont_persdetails.cont_pers_typer" => 'ofc_mngr'));
            $projTLEmp = $this->db->get()->row();
            $data['projofc_mngremp'] = ($projTLEmp) ? $projTLEmp : '';
            //$data['projofc_mngremp'] = ($projTLEmp) ? $projTLEmp->userfullname . " ( <small style='color:green'>" . $projTLEmp->contactnumber . " )</small>" : '';
//Fetch Details..

            $this->db->select("$db2.tm_projects.id,$db2.tm_projects.project_name");
            $this->db->from("$db2.tm_projects");
            $this->db->where(array("$db2.tm_projects.is_active" => '1'));
            // $this->db->where("($db2.tm_projects.project_category='ie' OR $db2.tm_projects.project_category='ae')", NULL, FALSE);
            $this->db->order_by("$db2.tm_projects.project_name", 'ASC');
            $data['ProjRec'] = $this->db->get()->result();

//Client Address Details...
            $data['clientaddrsdetails'] = $this->Getclient_addbyid($cegexpID);

//Get Client Address Details...
            $data['getclientaddrsdetails'] = $this->db->get_where(' bdceg_exp_clientmore_details', array('bdproj_id' => $cegexpID))->row_array();

//Get project Contract personal Details...  
            $this->db->select("$db2.main_employees_summary.position_name,$db1.cegexp_proj_cont_persdetails.emp_id as con_emp_id,$db1.cegexp_proj_cont_persdetails.cont_pers_typer as con_per_type,$db1.project_coordinator.emp_id as coor_emp_id,$db1.project_assign_engnr.emp_id as eng_emp_id");
            $this->db->from("$db1.cegexp_proj_cont_persdetails");
            $this->db->where("$db1.cegexp_proj_cont_persdetails.bd_projid", $cegexpID);
            $this->db->join("$db1.project_coordinator", "$db1.project_coordinator.bd_project_id=$db1.cegexp_proj_cont_persdetails.bd_projid", "inner");
            $this->db->join("$db1.project_assign_engnr", "$db1.project_assign_engnr.bd_project_id=$db1.cegexp_proj_cont_persdetails.bd_projid", "inner");
            $this->db->join("$db2.main_employees_summary", "$db2.main_employees_summary.user_id=$db1.cegexp_proj_cont_persdetails.emp_id", 'inner');
            $data['proj_con_per_detail'] = $this->db->get()->row_array();

//Project Ofc Address..
            $data['projoffcAddress'] = $this->Getprojofcaddress_addbyid($cegexpID);
            $data['hrmsprojid'] = $this->getprojidbdtohrms($cegexpID);


// code by durgesh for Get Detail About the lead, Jv, Associate Company.....            
            $this->db->select('b.company_name,a.project_id,a.compt_comp_id');
            $this->db->from('cegexp_competitor_comp as a');
            $this->db->where(array('a.project_id' => $cegexpID, 'a.status' => 1));
            $this->db->join('main_company as b', 'a.compt_comp_id=b.fld_id', 'inner');
            $comp_record = $this->db->get()->result();
            $data['competitor_jv_record'] = ($comp_record) ? $comp_record : '';

            $data['Jv_Lead_Associate_Comp'] = $this->mastermodel->GetCompNameDetailById('74', $cegexpID);
            
//Get Staffs Arr Code By Asheesh...
            $data['keyProfStaffArr'] = $this->mastermodel->GetStaffByProjID($cegexpID, "1");
           
            $data['subProfessionalStaffArr'] = $this->mastermodel->GetStaffByProjID($cegexpID, "2");
            $data['adminStaffArr'] = $this->mastermodel->GetStaffByProjID($cegexpID, "3");

//March 2019..
            $this->db->select("$db1.proj_keydate_details.*");
            $this->db->from("$db1.proj_keydate_details");
            $this->db->where(array("$db1.proj_keydate_details.bd_projid" => $cegexpID, "$db1.proj_keydate_details.status" => "1"));
            $data['ReckeydateSingle'] = $this->db->get()->row();

            //Get All  Ofc Details....
            $this->db->SELECT("$db1.proj_siteofc_detail.*,$db2.countries.country_name,$db2.states.state_name,$db2.cities.city_name,$db2.main_employees_summary.userfullname as ofc_manager_userfullname,$db2.main_employees_summary.contactnumber as ofc_manager_contactnumber,$db2.main_employees_summary.emailaddress as ofc_manager_emailaddress,$db2.main_employees_summary.department_name as ofc_manager_department_name,$db2.d.userfullname as contper_userfullname,$db2.d.contactnumber as contper_contactnumber,$db2.d.emailaddress as contper_emailaddress,$db2.d.department_name as contper_department_name");
            $this->db->FROM("$db1.proj_siteofc_detail");
            $this->db->Join("$db2.countries", "$db1.proj_siteofc_detail.countries_idnew = $db2.countries.country_id", "LEFT");
            $this->db->Join("$db2.states", "$db1.proj_siteofc_detail.state_idnew = $db2.states.state_id", "LEFT");
            $this->db->Join("$db2.cities", "$db1.proj_siteofc_detail.city_idnew = $db2.cities.city_id", "LEFT");
            $this->db->Join("$db2.main_employees_summary", "$db1.proj_siteofc_detail.ofc_mnger_empid = $db2.main_employees_summary.user_id", "LEFT");
            $this->db->Join("$db2.main_employees_summary as d", "$db1.proj_siteofc_detail.cont_persname = $db2.d.user_id", "LEFT");
            $this->db->WHERE(array("$db1.proj_siteofc_detail.project_id" => $cegexpID, "$db1.proj_siteofc_detail.status" => "1"));
            $this->db->order_by("$db1.proj_siteofc_detail.fld_id", "DESC");
            $data['RecSiteOfcDetails'] = $this->db->get()->result();

            //Get single  Ofc Details....
            $this->db->SELECT("$db2.main_employees_summary.user_id,$db1.proj_siteofc_detail.*,$db2.countries.country_id,$db2.countries.country_name,$db2.states.state_name,$db2.cities.city_name,$db2.main_employees_summary.userfullname as ofc_manager_userfullname,$db2.main_employees_summary.contactnumber as ofc_manager_contactnumber,$db2.main_employees_summary.emailaddress as ofc_manager_emailaddress,$db2.main_employees_summary.department_name as ofc_manager_department_name,$db2.d.userfullname as contper_userfullname,$db2.d.contactnumber as contper_contactnumber,$db2.d.emailaddress as contper_emailaddress,$db2.d.department_name as contper_department_name");
            $this->db->FROM("$db1.proj_siteofc_detail");
            $this->db->Join("$db2.countries", "$db1.proj_siteofc_detail.countries_idnew = $db2.countries.country_id", "LEFT");
            $this->db->Join("$db2.states", "$db1.proj_siteofc_detail.state_idnew = $db2.states.state_id", "LEFT");
            $this->db->Join("$db2.cities", "$db1.proj_siteofc_detail.city_idnew = $db2.cities.city_id", "LEFT");
            $this->db->Join("$db2.main_employees_summary", "$db1.proj_siteofc_detail.ofc_mnger_empid = $db2.main_employees_summary.user_id", "LEFT");
            $this->db->Join("$db2.main_employees_summary as d", "$db1.proj_siteofc_detail.cont_persname = $db2.d.user_id", "LEFT");
            $this->db->WHERE(array("$db1.proj_siteofc_detail.project_id" => $cegexpID, "$db1.proj_siteofc_detail.status" => "1"));
            $this->db->order_by("$db1.proj_siteofc_detail.fld_id", "DESC");












            $data['RecSingleSiteOfcDetails'] = $this->db->get()->row_array();

            
            ###############

            $clientName = $this->Front_model->selectRecord('bd_client_master', array('id', 'client_name'), array('status' => '0'));
            if ($clientName != '') {
                $clientName = $clientName->result();
                $data['clientName'] = $clientName;
            }
    
            $fundingOrg = $this->Front_model->selectRecord('bd_funding_master', array('id', 'funding_org'), array('status' => '0'));
            if ($fundingOrg != '') {
                $fundingOrg = $fundingOrg->result();
                $data['fundingOrg'] = $fundingOrg;
            }




            $consortiumData = $this->mastermodel->SelectRecordSingle('jv_companyrecords', array('project_id' => $cegexpID));

            $data['consortiumData'] = $consortiumData;

            $clientcontactData = $this->mastermodel->SelectRecordSingle('clientcontact', array('project_id' => $cegexpID));

            $data['clientcontactData'] = $clientcontactData;



            $personelContactData = $this->mastermodel->SelectRecordSingle('cegexp_proj_cont_persdetails', array('bd_projid' => $cegexpID));

            $data['personelContactData'] = $personelContactData;

            
            
           


            //  print_r($this->db->last_query()); die;



            
     
            ################


            

            $message = "Mail Sent Successfully !";

           
        }
        

         $arrMessage = array(
            'msg' => $message,
            );

            echo json_encode($arrMessage);
    }

    public function mailSender($req, $data){
            $msgDetails = $this->load->view('email_template/comptitor_list_email', $data, true);
            $emailsAddress = $this->SecondDB_model->GetBdEmailsListNew();
            $email = 'bdcru@cegindia.com';
            //$email = 'marketing@cegindia.com';
            // $reSponS1 = sendcomptitorMail("$email", 'Comptitor List of Project', $msgDetails);
            $reSponS1 = 1;
            if ($reSponS1):

            //print_r("test"); die;

            $inseRmailArr = array("bd_projid" => $data['bddetail']->fld_id, "is_sent" => "1", "mail_type" => "Comptitor", "send_by_loginid" => $this->session->userdata('loginid'));
            $resp = $this->mastermodel->InsertMasterData($inseRmailArr, 'mail_sendornot_chk');
            endif;

           // $this->session->set_flashdata('msg', "Competitor List Mail Sent Successfully.");
           // redirect(base_url('company/cegexpedit/' . $cegexpID));

          

           
    }



    public function mailSenderCom($req,$data){



                $msgDetails = $this->load->view('email_template/comptitor_list_email8020', $data, true);
                $email = 'bdcru@cegindia.com';
                // $email = 'marketing@cegindia.com';
                //$reSponS = sendcomptitorProjectMail("$email", 'Project Result', $msgDetails);
                $reSponS = 1;
                if ($reSponS):
                    $inseRmailArr = array("bd_projid" => $data['bddetail']->fld_id, "is_sent" => "1", "mail_type" => "8020", "send_by_loginid" => $this->session->userdata('loginid'));
                    $resp = $this->mastermodel->InsertMasterData($inseRmailArr, 'mail_sendornot_chk');
                endif;
                //$this->session->set_flashdata('msg', "Project Result Mail Sent Successfully.");
                //redirect(base_url('company/cegexpedit/' . $cegexpID));

                

    }

    //code by durgesh
    public function Getclient_addbyid($bdprojId) {
        if ($bdprojId) {
            $this->db->SELECT('a.*');
            $this->db->FROM('bdceg_exp_clientmore_details as a');
            $this->db->WHERE(array('a.bdproj_id' => $bdprojId, 'a.status' => '1'));
            $resultdata = $this->db->get()->result();
            return ($resultdata) ? $resultdata : '';
        } else {
            return false;
        }
    }

//code by durgesh Project Ofc Address Details...
    public function Getprojofcaddress_addbyid($bdprojId) {
        if ($bdprojId) {
            $this->db->SELECT('a.*');
            $this->db->FROM('bdceg_exp_offcaddress as a');
            $this->db->WHERE(array('a.bdproj_id' => $bdprojId, 'a.status' => '1'));
            $resultdata = $this->db->get()->row();
            return ($resultdata) ? $resultdata : '';
        } else {
            return false;
        }
    }

//code by durgesh
    public function getprojidbdtohrms($projID) {
        $this->db->select("project_numberid");
        $this->db->from("bdcegexp_proj_summery");
        $this->db->where("project_id", $projID);
        $RecArr = $this->db->get()->row();
        return ($RecArr) ? $RecArr->project_numberid : null;
    }

    /*     * ********* change this function by durgesh***************** */

    public function updatecegexp() {

        // echo "<pre>";
        // print_r($_REQUEST);
        // die;


        //         Array
        // (
        // )







        //     [sector_id] => 1
        //     [service_id] => 2
        //     [countries_id] => 34
        //     [state_id] => 378

        if ($_REQUEST) {
            $recArr = array(
                'project_id' => $_REQUEST['projectid'],
                'sector_id' => $_REQUEST['sector_id'],
                'service_id' => $_REQUEST['service_id'],
                'countries_id' => $_REQUEST['countries_id'],
                'state_id' => $_REQUEST['state_id'],
                // 'funding' => $_REQUEST['funding'],
                //'certificate_file_name' => $_REQUEST['certificate_file'],
                // 'pds_file_name' => $_REQUEST['pds_file'],
                'entry_by' => $this->session->userdata('loginid')
            );

            $recArr1 = array(
                'project_id' => $_REQUEST['projectid'],
                'sector_id' => $_REQUEST['sector_id'],
                'service_id' => $_REQUEST['service_id'],
                'country_id' => $_REQUEST['countries_id'],
                'state_id' => $_REQUEST['state_id'],
            );

            $ProjrecArr = $this->mastermodel->SelectRecordFldGd('bdceg_exp', array('project_id' => $_REQUEST['projectid']));

       

            if (count($ProjrecArr) < 1) {
                //$insertedArr = array('project_id' => $cegexpID);
                $resp = $this->mastermodel->InsertMasterData($recArr, 'bdceg_exp');
            } else {
                $respp = $this->mastermodel->UpdateRecords('bdceg_exp', array('project_id' => $_REQUEST['projectid']), $recArr);
            }

            
            
            
              
           
            
            
            
            $ProjrecArr1 = $this->mastermodel->SelectRecordFldGd('project_description', array('project_id' => $_REQUEST['projectid']));
            if (count($ProjrecArr1) < 1) {
                //$insertedArr = array('project_id' => $cegexpID);
                $respbdceg = $this->mastermodel->InsertMasterData($recArr1, 'project_description');
            } else {
                $respp12 = $this->mastermodel->UpdateRecords('project_description', array('project_id' => $_REQUEST['projectid']), $recArr1);
            }




          


            /// Add lane lenth array table code by durgesh

            if (!empty($_REQUEST['lane_chk'][0])) {
                $lane = $_REQUEST['lane_chk'];
                $lane_length = $_REQUEST['lane_length'];
                for ($i = 0; $i < count($lane); $i++) {
                    $count = $this->mastermodel->count_rows('ceg_lane', array('project_id' => $_REQUEST['projectid'], 'lane' => $lane[$i]));
                    if ($count < 1):
                        $resp = $this->mastermodel->InsertMasterData(array('project_id' => $_REQUEST['projectid'], 
                        'lane' => $lane[$i], 'lane_length' => $lane_length[$i]), 'ceg_lane');
                    else:
                        $resp = $this->mastermodel->UpdateRecords('ceg_lane', array('project_id' => $_REQUEST['projectid'], 
                        'lane' => $lane[$i]), array('lane_length' => $lane_length[$i]));
                    endif;
                }
            }

            //     [project_cost] => 121212
            //     [length] => 1000
            //     [project_number] => 121


            $ProjrecArr5 = $this->mastermodel->SelectRecordFldGd('bdcegexp_proj_summery', array('project_id' => $_REQUEST['projectid']));
            if (count($ProjrecArr5) < 1) {
                $resp = $this->mastermodel->InsertMasterData(array('project_id' => $_REQUEST['projectid'], 
                'project_numberid' => $_REQUEST['project_number'], 'project_cost' => $_REQUEST['project_cost'], 
                'length' => $_REQUEST['length']), 'bdcegexp_proj_summery');
            } else {
                $respp = $this->mastermodel->UpdateRecords('bdcegexp_proj_summery', 
                array('project_id' => $_REQUEST['projectid']), array('project_id' => $_REQUEST['projectid'],
                 'project_numberid' => $_REQUEST['project_number'], 'project_cost' => $_REQUEST['project_cost'], 'length' => $_REQUEST['length']));
            }

            $ProjrecArr6 = $this->mastermodel->SelectRecordFldGd('bd_tenderdetail', array('fld_id' => $_REQUEST['projectid']));
            if (count($ProjrecArr6) < 1) {
                $resp = $this->mastermodel->InsertMasterData(array('fld_id' => $_REQUEST['projectid'], 'TenderDetails' => $_REQUEST['project_name']), 'bdcegexp_proj_summery');
            } else {
               // $respp = $this->mastermodel->UpdateRecords('bd_tenderdetail', array('fld_id' => $_REQUEST['projectid']), array('TenderDetails' => $_REQUEST['project_name']));
            }





                // code copied 


                //     [projectid] => 349705
                //     [clientText] => XYZ Consulting
                //     [fundingText] => ZZZ Fund Consulting
                //     [mTextHid1] => XYZ Consulting
                //     [client_name] => 1
                //     [client_text] => XYZ Consulting
                //     [mTextHid2] => ZZZ Fund Consulting
                //     [funding_org] => 2
                //     [funding_org_text] => ZZZ Fund Consulting

                if($_REQUEST['client_text'] !== ""){


                        $qlClient = $this->db->select('*')->from('bd_client_master')->where('client_name', $_REQUEST['client_text'])->get();
                        if( $qlClient->num_rows() > 0 ) {
                        $clientId = $_REQUEST['client_name'];
                        $clientVal = $_REQUEST['client_text'];

                        } else {

                        $clientData = array(
                        'client_name'=> $_REQUEST['client_text'],
                        );

                        $clientRes = $this->db->insert('bd_client_master',$clientData);
                        $clientId = $this->db->insert_id();
                        $clientVal = $_REQUEST['client_text'];
                        }

                }
            
                
   
             
   
   
                if($_REQUEST['funding_org_text'] !== ""){


                            $qlFunding = $this->db->select('*')->from('bd_funding_master')->where('funding_org', $_REQUEST['funding_org_text'])->get();

                            if( $qlFunding->num_rows() > 0 ) {
                            $fundingId = $_REQUEST['funding_org'];
                            $fundingVal = $_REQUEST['funding_org_text'];

                            } else {


                            $fundData = array(
                            'funding_org'=> $_REQUEST['funding_org_text'],
                            );

                            $fundRes = $this->db->insert('bd_funding_master',$fundData);
                            $fundingId = $this->db->insert_id();
                            $fundingVal = $_REQUEST['funding_org_text'];
                            }



                }
               
   
                //     [project_name] => 	
                // 	   Detail Investigation and Design of Maukhola Bridge at Gelephu
                //     [subm_date] => 
                //     [expr_date] => 2020-03-22
                //     [location] => qwqwqw


               // fetch its id else go $respon Organization client_id
               $Respon = $this->mastermodel->UpdateRecords('bd_tenderdetail', array('fld_id' => $_REQUEST['projectid']), 
               array('TenderDetails' => $this->input->post('project_name'), 'bid_validity_date' => $_REQUEST['expr_date'], 
               'Location' => $this->input->post('location'), 'client_id' => $clientId, 'Organization' => $clientVal, 
               'bd_funding_master_id' => $fundingId, 'bd_funding_master_org' => $fundingVal));
               //print_r($this->db->last_query()); die;




                // consortium data will be saved here

                //     [leadcompany11] => Array
                //         (
                //             [0] => 2
                //             [1] => 3
                //         )

                //     [joint_venture22] => Array
                //         (
                //             [0] => 4
                //             [1] => 5
                //         )

                //     [assoc33] => Array
                //         (
                //             [0] => 60
                //         )

                




                  

                $insertedArr = array(
                    'lead_companes' => implode(",", $_REQUEST['leadcompany11']),
                    'joint_ventures' => implode(",", $_REQUEST['joint_venture22']),
                    'associate_company' => implode(",", $_REQUEST['assoc33']),
                    'project_id' => $_REQUEST['projectid'],
                    'entry_by' => $this->session->userdata('loginid'));
    
                $this->db->where('project_id', $_REQUEST['projectid']);
                $query = $this->db->get('jv_companyrecords');
    
                if ($query->num_rows() > 0){
                   
                    $respon = $this->mastermodel->UpdateRecords('jv_companyrecords', 
                    array('project_id' =>  $_REQUEST['projectid']), $insertedArr);
                }
                else{
                    
                    $respon = $this->mastermodel->InsertMasterData($insertedArr, 'jv_companyrecords');
                }
               
    






               
               
              
           
          











            // $respp1 = $this->mastermodel->UpdateRecords('bd_tenderdetail', array('fld_id' => $_REQUEST['projectid']), array('TenderDetails' => $_REQUEST['project_name']));
        }







        // if ($respp):
        //     $this->session->set_flashdata('msg', 'Record Updated successfully');
        //     //redirect(base_url('dashboard/viewedit/' . $_REQUEST['projectid']));
        //     redirect($_SERVER['HTTP_REFERER']);
        // else:
        //     //redirect(base_url('dashboard/viewedit/' . $_REQUEST['projectid']));
        //     redirect($_SERVER['HTTP_REFERER']);
        // endif;


        if($respp){
            $message = "Data updated successfully !";
        }else{
            $message = "Some issues occured";
        }


        $arrMessage = array(
            'msg' => $message,
            'id' => $clientId,
        );

        echo json_encode($arrMessage);




    }

    //Update Weightage Record..
    public function updateweightage() {
        $recArr = $_REQUEST;
        array_pop($recArr);
        $respp = $this->mastermodel->UpdateRecords('cegexp_competitor_comp', array('fld_id' => $this->input->post('actid')), $recArr);
        if ($respp):
            //$this->session->set_flashdata('msg', 'Record Updated successfully');
            $message = "Rank generated successfully !";
            //redirect(base_url('company/cegexpedit/' . $this->input->post('project_id')));
        else:
            $message = "Some issue occured !";
            //redirect(base_url('company/cegexpedit/' . $this->input->post('project_id')));
        endif;

          $arrMessage = array(
            'msg' => $message
        );

        echo json_encode($arrMessage);
    }

    //Ajax Edit Score..
    public function ajax_score_byid() {
        $editId = $_REQUEST['editId'];
        $recArr = $this->mastermodel->SelectRecordSingle('cegexp_competitor_comp', array('fld_id' => $editId));
        echo json_encode($recArr);
    }

    //Upadte Weightage
    public function updatefixweightage() {
        $respp = $this->mastermodel->UpdateRecords('cegexp_competitor_comp', array('project_id' => $this->input->post('project_id')), $_REQUEST);
        if ($respp):
            // $this->session->set_flashdata('msg', 'Record Updated successfully');
            // redirect(base_url('company/cegexpedit/' . $this->input->post('project_id')));
            $message = "Weightage updated successfully !";
        else:
            // redirect(base_url('company/cegexpedit/' . $this->input->post('project_id')));
             $message = "Some issues occured !";
        endif;


        $output = array(
        
            "msg" => $message,
        );
        echo json_encode($output);
    }

    public function updatesummery() {
        $Arr = $_REQUEST;
        if ($Arr) {
            $count = $this->mastermodel->count_rows('bdcegexp_proj_summery', array('project_id' => $Arr['project_id']));
            if ($count < 1):
                $recArr = array(
                    'project_id' => $Arr['project_id'],
                    'project_numberid' => $Arr['project_number'],
                    //'sector'=> $Arr['sector_id'],
                    'start_date' => $Arr['start_date'],
                    'end_date' => $Arr['end_date'],
                    'project_cost' => $Arr['project_cost'],
                    'project_rcv' => $Arr['project_rcv'],
                    'consultancy_fee' => $Arr['consultancy_fee'],
                    'cegshare_val' => $Arr['cegshare_val'],
                    'othershare_val' => $Arr['othershare_val'],
                    'length' => $Arr['length'],
                    'entry_by' => $this->session->userdata('loginid')
                );
                $respp = $this->mastermodel->InsertMasterData($recArr, 'bdcegexp_proj_summery');
            else:
                $recArr = array(
                    'project_numberid' => $Arr['project_number'],
                    //'sector'=> $Arr['sector_id'],
                    'start_date' => $Arr['start_date'],
                    'end_date' => $Arr['end_date'],
                    'project_cost' => $Arr['project_cost'],
                    'project_rcv' => $Arr['project_rcv'],
                    'consultancy_fee' => $Arr['consultancy_fee'],
                    'cegshare_val' => $Arr['cegshare_val'],
                    'othershare_val' => $Arr['othershare_val'],
                    'length' => $Arr['length'],
                    'entry_by' => $this->session->userdata('loginid')
                );
                $respp = $this->mastermodel->UpdateRecords('bdcegexp_proj_summery', array('project_id' => $Arr['project_id']), $recArr);
            endif;

            //$respp12 = $this->mastermodel->UpdateRecords('project_description', array('project_id' => $_REQUEST['projectid']), array('sector_id'=> $Arr['sector_id']));
            $count12 = $this->mastermodel->count_rows('project_description', array('project_id' => $Arr['project_id']));
            if ($count12 < 1):
                $recArr = array(
                    'project_id' => $Arr['project_id'],
                    'sector_id' => $Arr['sector_id']
                );
                $respp12 = $this->mastermodel->InsertMasterData($recArr, 'project_description');
            else:
                $recArr = array(
                    'sector_id' => $Arr['sector_id']
                );
                echo $respp12 = $this->mastermodel->UpdateRecords('project_description', array('project_id' => $_REQUEST['project_id']), $recArr);
            endif;


            if (!empty($Arr['lane_chk'][0])) {
                $lane = $Arr['lane_chk'];
                $lane_length = $Arr['lane_length'];
                for ($i = 0; $i < count($lane); $i++) {
                    $count = $this->mastermodel->count_rows('ceg_lane', array('project_id' => $Arr['project_id'], 'lane' => $lane[$i]));
                    if ($count < 1):
                        $this->mastermodel->InsertMasterData(array('project_id' => $Arr['project_id'], 'lane' => $lane[$i], 'lane_length' => $lane_length[$i]), 'ceg_lane');
                    else:
                        $this->mastermodel->UpdateRecords('ceg_lane', array('project_id' => $Arr['project_id'], 'lane' => $lane[$i]), array('lane_length' => $lane_length[$i]));
                    endif;
                }
            }


            //File Upload code 1..
            if ($_FILES['pds_file']['name']) {
                $folder = './uploads/pdsfile/';
                $filename = 'pdsfile_' . $Arr['project_id'];
                $configThm = $this->getUpload($folder, $filename);
                $this->load->library('upload', $configThm);
                $this->upload->initialize($configThm);
                if ($this->upload->do_upload('pds_file')) {
                    $uploadData = $this->upload->data();
                    $filename1 = $uploadData['file_name'];
                    $this->mastermodel->UpdateRecords('bdcegexp_proj_summery', array('project_id' => $Arr['project_id']), array('pds_file' => $filename1));
                }
            }
            //File Upload code 2..
            if ($_FILES['certificate_file']['name']) {
                $folder = './uploads/certificate/';
                $filename = 'certificate_' . $Arr['project_id'];
                $configThm = $this->getUpload($folder, $filename);
                $this->load->library('upload', $configThm);
                $this->upload->initialize($configThm);
                if ($this->upload->do_upload('certificate_file')) {
                    $uploadData = $this->upload->data();
                    $filename2 = $uploadData['file_name'];
                    $this->mastermodel->UpdateRecords('bdcegexp_proj_summery', array('project_id' => $Arr['project_id']), array('certificate_file' => $filename2));
                }
            }

            //File Upload code 3..
            if ($_FILES['contract_agreement_file']['name']) {
                $folder = './uploads/contract/';
                $filename = 'contract_' . $Arr['project_id'];
                $configThm = $this->getUpload($folder, $filename);
                $this->load->library('upload', $configThm);
                $this->upload->initialize($configThm);
                if ($this->upload->do_upload('contract_agreement_file')) {
                    $uploadData = $this->upload->data();
                    $filename3 = $uploadData['file_name'];
                    $this->mastermodel->UpdateRecords('bdcegexp_proj_summery', array('project_id' => $Arr['project_id']), array('contract_agreement_file' => $filename3));
                }
            }
        }

        if ($respp):
            $this->session->set_flashdata('msg', 'Record Updated successfully');
            redirect(base_url('company/cegexpedit/' . $Arr['project_id']));
        else:
            redirect(base_url('company/cegexpedit/' . $Arr['project_id']));
        endif;
    }

    //Add Comptitor Company By Asheesh .. 
    public function addcomptcompn() {




        $projId = $_REQUEST['project_id'];
        if ($_REQUEST['leadcompany']):
            foreach ($_REQUEST['leadcompany'] as $compId) {
                $count = $this->mastermodel->count_rows('cegexp_competitor_comp', array('project_id' => $projId, 'compt_comp_id' => $compId));
                if ($count < 1):
                    $this->mastermodel->InsertMasterData(array('project_id' => $projId, 'entry_by' => $this->session->userdata('loginid'), 'compt_comp_id' => $compId), 'cegexp_competitor_comp');
                endif;
            }
        endif;
        //$this->session->set_flashdata('msg', 'Record Inserted successfully');
        //redirect(base_url('company/cegexpedit/' . $projId));

        $message = "Compititor added successfully";


        $output = array(
        
            "msg" => $message,
        );
        echo json_encode($output);


    }

    //Score Reload And Update Technical..
    public function reloadscore_tech() {
        $projID = $_REQUEST['project_id']; //$this->uri->segment(3);


        $recArr = $this->mastermodel->SelectRecordFldNew('cegexp_competitor_comp', array('project_id' => $projID, 'status' => '1'));


        if ($recArr):
            foreach ($recArr as $rowdt) {
                if (($rowdt->techn_weightage) and ( $rowdt->technical_score)) {
                    $reeMulScore = ($rowdt->technical_score * $rowdt->techn_weightage);
                    $this->mastermodel->UpdateRecords('cegexp_competitor_comp', array('fld_id' => $rowdt->fld_id), array('technical_marks' => $reeMulScore));
                }
            }
        endif;
        //$this->session->set_flashdata('msg', 'Technical Score Updated successfully');
        //redirect(base_url('company/cegexpedit/' . $projID));


        $message = "Technical Score Updated successfully";

         $arrMessage = array(
            'msg' => $message,
        );

        echo json_encode($arrMessage);
    }

    //Score Reload And Update Financial.. .
    public function reloadscore_finan() {
        $projID = $_REQUEST['project_id'];  //$this->uri->segment(3);
        $recArr = $this->mastermodel->SelectRecordFldNew('cegexp_competitor_comp', array('project_id' => $projID, 'status' => '1'));


        $getminVal = $this->mastermodel->getMinVal('cegexp_competitor_comp', 'financial_score', array('project_id' => $projID, 'status' => '1'));

        //print_r('expression'); die;
        if ($recArr):
            foreach ($recArr as $rowdt) {
                $recUpd = 0;
                $rrrFinal = 0;
                if (($rowdt->finan_weightage) and ( $rowdt->financial_score)) {
                    $recUpd = ($getminVal / $rowdt->financial_score);
                    $rrrFinal = $recUpd * $rowdt->finan_weightage;
                    $this->mastermodel->UpdateRecords('cegexp_competitor_comp', array('fld_id' => $rowdt->fld_id), array('financial_marks' => $rrrFinal));
                }
            }
        endif;
        // $this->session->set_flashdata('msg', 'Financial Score Updated successfully');
        // redirect(base_url('company/cegexpedit/' . $projID));


         $message = "Financial Score Updated successfully";

         $arrMessage = array(
            'msg' => $message,
        );

        echo json_encode($arrMessage);
    }

    //Upload Config By Asheesh..
    protected function getUploadConfig() {
        $config = array();
        $config['upload_path'] = './uploads/awardedproject/';
        $config['allowed_types'] = 'pdf|doc|PDF';
        $config['max_size'] = '200000'; //20MB
        $config['file_name'] = "Resume_" . time();
        return $config;
    }

    public function getUpload($folder, $filename) {
        $config = array();
        $config['upload_path'] = $folder;
        $config['allowed_types'] = 'pdf|doc|PDF';
        $config['max_size'] = '200000'; //20MB
        //$config['file_name'] = "news_" . time();
        $config['file_name'] = $filename;
        $config['overwrite'] = true;
        return $config;
    }

    //Add Extra About Company.. 
    public function addcompany_extra() {
        $data = array();
        $data['comp_single_rec'] = null;
        $data['title'] = 'Manage Company Details';
        $data['ComprecArr'] = $this->mastermodel->SelectRecordFldNew('main_company', array('status' => '1'));
        if ($_REQUEST['compnm']):
            $data['comp_single_rec'] = $this->mastermodel->SelectRecordFldNew('main_company', array('status' => '1', 'fld_id' => $_REQUEST['compnm']));
            $data['sectArr_rec'] = $this->mastermodel->SelectRecord('designation_sector', array('status' => '1'), 'sector_name', 'ASC');
            $data['comp_extraRec'] = $this->mastermodel->SelectRecordFldNew('company_extra_details', array('status' => '1', 'main_comp_id' => $_REQUEST['compnm']));
            $data['selectedcomp'] = $_REQUEST['compnm'];
            $data['country_Arr'] = $this->mastermodel->SelectRecordC('countries', array('status' => '1'));
            $countID = $data['comp_single_rec'][0]->country;
            $data['stateArr'] = $this->mastermodel->SelectRecordS('states', array('country_id' => $countID, 'status' => '1'));
            $data['comp_field_Arr'] = $this->mastermodel->SelectRecordFldNew('comp_field_master', array('status' => '1'), 'field_name', 'ASC');
        endif;
        $this->load->view('company/extradetails_company_view', $data);
    }

    //Save Extra Act..
    public function savecompany_extra() {
        $recInsertArr = $this->input->post();
        $recInsertArr['entry_by'] = $this->session->userdata('loginid');
        $respon = $this->mastermodel->InsertMasterData($recInsertArr, 'company_extra_details');
        if ($respon):
            $this->session->set_flashdata('msg', 'Extra Company Detail Added successfully');
            redirect(base_url('company/addcompany_extra?compnm=' . $recInsertArr['main_comp_id']));
        else:
            $this->session->set_flashdata('error_msg', 'Something went gone wrong');
            redirect(base_url('company/addcompany_extra?compnm=' + $recInsertArr['main_comp_id']));
        endif;
    }

    //Update Extra Details..
    public function updatecompany_extra() {
        $UpdArr = $this->input->post();
        array_pop($UpdArr);
        $respp = $this->mastermodel->UpdateRecords('company_extra_details', array('fld_id' => $this->input->post('edtid')), $UpdArr);
        if ($respp):
            //$this->session->set_flashdata('msg', 'Record Updated successfully');
            //redirect(base_url('company/addcompany_extra?compnm=' . $UpdArr['main_comp_id']));

             $message = "Company extra detail saved successfully";
        else:
           // redirect(base_url('company/addcompany_extra?compnm=' . $UpdArr['main_comp_id']));
        


         $message = "Some issue occured";
        endif;





            $output = array(

            "msg" => $message,
            );
            echo json_encode($output);
    }

    //Update Main Company Basic..
    public function updatecompany_basic() {
        $UpdArr = $this->input->post();
        array_pop($UpdArr);
        $UpdArr['operating_countries'] = implode(",", $_REQUEST['operating_countries']);
        $UpdArr['com_sector'] = implode(",", $_REQUEST['com_sector']);
        $UpdArr['comp_field_id'] = implode(",", $_REQUEST['comp_field_id']);
        $respp = $this->mastermodel->UpdateRecords('main_company', array('fld_id' => $this->input->post('fld_id')), $UpdArr);
        if ($respp):
           // $this->session->set_flashdata('msg', 'Record Updated successfully');
            //redirect(base_url('company/addcompany_extra?compnm=' . $this->input->post('fld_id')));


             $message = "Company details updated successfully";
        else:
            //redirect(base_url('company/addcompany_extra?compnm=' . $this->input->post('fld_id')));

             $message = "Some issue occured";
        endif;





            $output = array(

            "msg" => $message,
            );
            echo json_encode($output);
    }

    //Edit Ajax Extra Address By Id..
    public function ajax_getextcomp_byid() {
        $edtid = $_REQUEST['editId'];
        $CextraRec = $this->mastermodel->SelectRecordFld('company_extra_details', array('status' => '1', 'fld_id' => $edtid));
        echo json_encode($CextraRec[0]);
        exit;
    }

    //Add joint venture.. Consortium
    public function addjointventure() {
        $leadCompString = '';
        $joint_venture = '';
        $assoc = '';
        if ($_REQUEST['leadcompany']):
            $leadCompString = implode(",", $_REQUEST['leadcompany']);
        endif;
        if ($_REQUEST['joint_venture']):
            $joint_venture = implode(",", $_REQUEST['joint_venture']);
        endif;
        if ($_REQUEST['assoc']):
            $assoc = implode(",", $_REQUEST['assoc']);
        endif;
        $count = $this->mastermodel->count_rows('jv_cegexp', array('project_id' => $_REQUEST['project_id'], 'base_comp_id' => $_REQUEST['leadcompanyid']));
        if ($count < 1):
            $inserArr = array(
                'project_id' => $_REQUEST['project_id'],
                'base_comp_id' => $_REQUEST['leadcompanyid'],
                'lead_comp_id' => $leadCompString,
                'joint_venture' => $joint_venture,
                'asso_comp' => $assoc,
                'entry_by' => $this->session->userdata('loginid')
            );
            $respon = $this->mastermodel->InsertMasterData($inserArr, 'jv_cegexp');
        else:
            $inserArr1 = array(
                'base_comp_id' => $_REQUEST['leadcompanyid'],
                'lead_comp_id' => $leadCompString,
                'joint_venture' => $joint_venture,
                'asso_comp' => $assoc,
                'entry_by' => $this->session->userdata('loginid')
            );
            $respon = $this->mastermodel->UpdateRecords('jv_cegexp', array('project_id' => $_REQUEST['project_id'], 'base_comp_id' => $_REQUEST['leadcompanyid']), $inserArr1);
        endif;
        //$this->session->set_flashdata('msg', 'Insert Jv Details Record successfully');
        //redirect(base_url('company/cegexpedit/' . $_REQUEST['project_id']));

        $message = "Consortium added successfully";
        

        $output = array(
        
            "msg" => $message,
        );
        echo json_encode($output);
    }

    //Ajax Get All Sate By Country I..
    public function ajax_state_getbycid() {
        $counid = $_REQUEST['country'];
       

        $stateRec = $this->mastermodel->GetAllRecStatesByCid($counid);
        echo json_encode($stateRec);
    }

    public function ajax_city_getbysid() {
        $stateid = $_REQUEST['stateidv'];
        $CitiesRec = $this->mastermodel->SelectRecordCity($stateid);
        echo json_encode($CitiesRec);
    }

    public function changeprojstatus() {
        $formArrData = $_REQUEST;
        if ($formArrData) {
            $projstatus = $this->mastermodel->SelectRecordSingle('bdproject_status', array('project_id' => $formArrData['project_id']));






            if ($projstatus) {
                //Update if record Existance..
                $updRec = array('project_status' => $formArrData['project_status']);
                $this->mastermodel->UpdateRecords('bdproject_status', array('project_id' => $formArrData['project_id']), $updRec);

               // print_r($this->db->last_query()); die;
            } else {
                //Insert if record New
                $insertedArr = array(
                    'user_id' => $this->session->userdata('loginid'),
                    'project_id' => $formArrData['project_id'],
                    'project_status' => $formArrData['project_status'],
                    'action_date' => date('Y-m-d H:i:s'),
                    'actionIP' => get_client_ip()
                );
                $this->mastermodel->InsertMasterData($insertedArr, 'bdproject_status');
            }








            //print_r($this->db->last_query()); die;
            //$this->session->set_flashdata('msg', "Project Status Changed Successfully.");
            $message = "Project Status Changed Successfully.";
        } else {
            //$this->session->set_flashdata('msg', "Something Went Wrong.");
            $message = "Something Went Wrong.";
        }


        
        if ($formArrData['project_status'] == 1) {
            $updRec = array('status_date' => date('Y-m-d'));
            $this->mastermodel->UpdateRecords('bdproject_status', array('project_id' => $formArrData['project_id']), $updRec);


            // Handle Code if Won save in bd_tenderDetail , bdtender_scope table, save detail to bd_moved_eoi
            $projectDetail = $this->mastermodel->SelectRecordSingle('bd_tenderdetail', array('fld_id' => $formArrData['project_id']));

            // print_r($projectDetail); die;

            if($_REQUEST['eoi_page']){
                $this->tender_add($projectDetail);
            }

            





        }
        //redirect(base_url('company/cegexpedit/' . $_REQUEST['project_id']));

        $output = array(
        
            "msg" => $message,
        );
        echo json_encode($output);


    }



      public function tender_add($projectDetail) {

            // echo "<pre>";
            // print_r($projectDetail);
            // print_r($ArrRec->fld_id); 
            // die;

            $ArrRec = $projectDetail; 


 
            $insertArr = Array(
                'created_By' => $this->session->userdata('loginid'),
                'tender24_ID' => $ArrRec->tender24_ID,  
                'TenderDetails' => $ArrRec->TenderDetails,
                'TenderValue' => $ArrRec->TenderValue,
                'Location' => $ArrRec->Location,
                'Organization' => $ArrRec->Organization, 
                'Tender_url' => $ArrRec->Tender_url, 
                'Sector_IDs' => $ArrRec->Sector_IDs,
                'Sector_Keyword_IDs' => '',
                'Keyword_IDs' => '',
                'keyword_phase' => $ArrRec->keyword_phase,  
                'Expiry_Date' => $ArrRec->Expiry_Date,
                'national_intern' => $ArrRec->national_intern  
            );

             
             
                $this->tender_model->insertRecord('bd_tenderdetail', $insertArr);
                //Get New Project ID
                $uniq = $this->db->insert_id();



               
                $insertscopeArr = Array(
                    'project_id' => $uniq,
                    'visible_scope' => 'In_Review_project',
                    'businessunit_id' => 1,
                );

                $uniq1 = $this->tender_model->insertRecord('bdtender_scope', $insertscopeArr);
                $insertscopeArr1 = Array(
                    'project_id' => $uniq,
                    'visible_scope' => 'In_Review_project',
                    'businessunit_id' => 2,
                );

                $uniq2 = $this->tender_model->insertRecord('bdtender_scope', $insertscopeArr1);

                $inserArr = array(
                    'old_project_id' => $ArrRec->fld_id,
                    'new_project_id' => $uniq,
                    'user_id' => $this->session->userdata('loginid'),
                    'status' => 1,
                    
                );

                $Respon = $this->Front_model->insertRecord('bd_moved_eoi', $inserArr);
            
                // echo "<pre>";
                // print_r($this->db->last_query()); 
                // die;

    }


    public function getcomptdata() {
        if ($_REQUEST) {
            $sectorname = $this->mastermodel->SelectRecordSingle('jv_cegexp', array('project_id' => $_REQUEST['projectID'], 'base_comp_id' => $_REQUEST['companyid']));
            echo json_encode($sectorname);
        }
    }

    //Company List Show
    function showlist() {
        $data['error'] = '';
        $data['title'] = 'Company Contact List ';
        $data['sectArr_rec'] = $this->mastermodel->SelectRecord('designation_sector', array('status' => '1'));
        $data['country_Arr'] = $this->mastermodel->SelectRecordC('countries', array('status' => '1'));
        $data['comp_field_Arr'] = $this->mastermodel->SelectRecordFld('comp_field_master', array('status' => '1'), 'field_name', 'ASC');
        $this->load->view('company/list_view', $data);
    }

    //All Tender Search Website Details Code By Asheesh..
    public function websites() {
        $data['error'] = '';
        $data['title'] = 'Websites';
        $this->load->view('company/list_view_websites', $data);
    }

    //Test Asheesh Pdf Merge..
    public function pdfm() {
        echo "Asheesh Yadav";
        die;
    }

    //Company Contact Delete Code By Asheesh..
    public function contdelete() {
        $delId = $_REQUEST['delid'];
        if ($delId):
            $respp = $this->mastermodel->UpdateRecords('main_company', array('fld_id' => $delId), array('status' => '0'));
            $respp = $this->mastermodel->UpdateRecords('company_extra_details', array('main_comp_id' => $delId), array('status' => '0'));
            $this->session->set_flashdata('msg', 'Company Contact Deleted Successfully');
        endif;
        redirect(base_url('company/showlist'));
    }

    //Upload updcompany_brochure
    public function updcompany_brochure() {
        if (($_FILES['comp_brochure_file']['name']) and ( $_REQUEST['main_compn_id'])) {
            $configThm = $this->getUploadConfigcomp();
            $this->load->library('upload', $configThm);
            $this->upload->initialize($configThm);
            if ($this->upload->do_upload('comp_brochure_file')) {
                $uploadData = $this->upload->data();
                $filename3 = $uploadData['file_name'];
                $main_compn_id = $_REQUEST['main_compn_id'];
                $this->session->set_flashdata('msg', 'File Upload Success');
                $this->mastermodel->UpdateRecords('main_company', array('fld_id' => $this->input->post('main_compn_id')), array('brochure' => $filename3));
                redirect(base_url('company/addcompany_extra?compnm=' . $main_compn_id));
            } else {
                $this->session->set_flashdata('errormsg', 'Something went wrong');
                redirect(base_url('company/addcompany_extra?compnm=' . $main_compn_id));
            }
        }
    }

    //Upload Config By Asheesh..
    protected function getUploadConfigcomp() {
        $config = array();
        $config['upload_path'] = './uploads/companybrochure/';
        $config['allowed_types'] = 'pdf|doc|DOC|docx|jpg|JPG|jpeg|PDF';
        $config['max_size'] = '200000'; //20MB
        $config['file_name'] = "Compb_" . time();
        return $config;
    }

    //Save Compititors..
    function savepds() {
        //echo '<pre>'; print_r($_REQUEST); die;
        if (!empty($_REQUEST['project_id'])) {
            $arr = array(
                'professional_staff_provided' => $_REQUEST['professional_staff_provided'],
                'numberof_man_month' => $_REQUEST['numberof_man_month'],
                'date_of_commencement' => $_REQUEST['date_of_commencement'],
                'stipulated_date_of_completion' => $_REQUEST['stipulated_date_of_completion'],
                'mmby_associated_firm' => $_REQUEST['mmby_associated_firm'],
                'narrative_description' => $_REQUEST['narrative_description'],
                'major_bridges' => $_REQUEST['major_bridges'],
                'urbon_flyovers' => $_REQUEST['urbon_flyovers'],
                'project_id' => $_REQUEST['project_id']
            );

            $count = $this->mastermodel->count_rows('ceg_pds_detail', array('project_id' => $_REQUEST['project_id']));
            if ($count < 1):
                $respon = $this->mastermodel->InsertMasterData($arr, 'ceg_pds_detail');
            else:
                $respon = $this->mastermodel->UpdateRecords('ceg_pds_detail', array('project_id' => $_REQUEST['project_id']), $arr);
            endif;
        }
        if ($respon):
            $this->session->set_flashdata('msg', 'Competitors Record Added successfully');
            redirect(base_url('company/cegexpedit/' . $_REQUEST['project_id']));
        else:
            $this->session->set_flashdata('error_msg', 'Something went gone wrong');
            redirect(base_url('company/cegexpedit/' . $_REQUEST['project_id']));
        endif;
    }

    public function save_download() {
        $cegexpID = $this->uri->segment(3);
        //load mPDF library
        $this->load->library('m_pdf');
        //load mPDF library
        if (!empty($cegexpID)) {
            $data['cegpds'] = $this->mastermodel->GetTableData('ceg_pds_detail', array('ceg_exp_id' => $cegexpID));
            $data['cegexpRec'] = $this->mastermodel->SelectRecordSingle('ceg_exp', array('fld_id' => $cegexpID));
            $data['cegexpSummery'] = $this->mastermodel->SelectRecordSingle('cegexp_proj_summery', array('cegexp_id' => $cegexpID, 'status' => '1'));
            $data['consortiumArr'] = $this->mastermodel->SelectRecordSingle('jv_companyrecords', array('cegexp_id' => $ProjId, 'status' => '1'));
            $projectID = $data['cegexpRec']->project_id;
            $data['tqRecArr'] = $this->mastermodel->SelectRecord('team_assign_member', array('status' => '1', 'project_id' => $projectID));

            $html = $this->load->view('company/pdf_output', $data, true); //load the pdf_output.php by passing our data and get all data in $html varriable.
            //this the the PDF filename that user will get to download
            $pdfFilePath = "pds_" . $projectID . ".pdf";

            $arr = array('pds_file_name' => $pdfFilePath);
            $where = array('fld_id' => $cegexpID);
            $respon = $this->mastermodel->UpdateRecords('ceg_exp', $where, $arr);

            //actually, you can pass mPDF parameter on this load() function
            $pdf = $this->m_pdf->load();
            //$pdf->WriteHTML($stylesheet, 1);
            //generate the PDF!
            $pdf->WriteHTML($html);
            //offer it to user via browser download! (The PDF won't be saved on your server HDD)
            $pdf->Output("./uploads/pds/" . $pdfFilePath, "F");
            $pdf->Output($pdfFilePath, "D");
        }
    }

    public function pds_description() {
        $title = 'PDS Description';
        $sector = $this->Front_model->GetActiveSector();
        $service1 = $this->Front_model->GetActiveService1();
        $service2 = $this->Front_model->GetActiveService2();
        $this->load->view('pds_description/tender_view', compact('sector', 'service1', 'service2', 'title'));
    }

    public function add_sector() {
        $this->form_validation->set_rules('sector_name', 'Sector Name', 'required|min_length[3]|max_length[100]|is_unique[sector.sectName]');
        if ($this->form_validation->run() == FALSE) {
            redirect(base_url('company/pds_description'));
        } else {
            $arr = array('sectName' => $_REQUEST['sector_name'], 'is_active' => '1');
            $respon = $this->mastermodel->InsertMasterData($arr, 'sector');
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Sector Added successfully');
                redirect(base_url('company/pds_description'));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('company/pds_description'));
            endif;
        }
    }

    public function add_service1() {
        if (!empty($_REQUEST)) {
            $count = $this->mastermodel->count_rows('pds_service1', array('sector_id' => $_REQUEST['sector_id'], 'service_name' => $_REQUEST['service1_name'], 'is_active' => 1));
            if ($count < 1):
                $arr = array('service_name' => $_REQUEST['service1_name'], 'sector_id' => $_REQUEST['sector_id'], 'is_active' => '1');
                $respon = $this->mastermodel->InsertMasterData($arr, 'pds_service1');
                if ($respon):
                    $this->session->set_flashdata('success_msg', 'Service Added successfully');
                    redirect(base_url('company/pds_description'));
                else:
                    $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                    redirect(base_url('company/pds_description'));
                endif;
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('company/pds_description'));
            endif;
        }
        //}
    }

    public function add_service2() {
        if (!empty($_REQUEST)) {
            $count = $this->mastermodel->count_rows('pds_service2', array('sector_id' => $_REQUEST['sector_id1'], 'service1_id' => $_REQUEST['service1_id'], 'service_name' => $_REQUEST['services2_name'], 'is_active' => 1));
            if ($count < 1):
                $arr = array('sector_id' => $_REQUEST['sector_id1'], 'service1_id' => $_REQUEST['service1_id'], 'service_name' => $_REQUEST['services2_name'], 'is_active' => '1');
                $respon = $this->mastermodel->InsertMasterData($arr, 'pds_service2');
                if ($respon):
                    $this->session->set_flashdata('success_msg', 'Service Added successfully');
                    redirect(base_url('company/pds_description'));
                else:
                    $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                    redirect(base_url('company/pds_description'));
                endif;
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('company/pds_description'));
            endif;
        }
        //}
    }

    public function add_pdsDescription() {
        if (!empty($_REQUEST['sector_name']) && !empty($_REQUEST['service1_name']) && !empty($_REQUEST['service2_name'])) {
            $count = $this->mastermodel->count_rows('pds_service_description', array('sector_id' => $_REQUEST['sector_name'], 'service1_id' => $_REQUEST['service1_name'], 'service2_id' => $_REQUEST['service2_name'], 'is_active' => 1));
            if ($count < 1):
                $arr = array('sector_id' => $_REQUEST['sector_name'], 'service1_id' => $_REQUEST['service1_name'], 'service2_id' => $_REQUEST['service2_name'], 'description' => $_REQUEST['service_description'], 'is_active' => 1);
                $respon = $this->mastermodel->InsertMasterData($arr, 'pds_service_description');
                redirect(base_url('company/pds_description'));
            else:
                $arr = array('description' => $_REQUEST['service_description']);
                $where = array('sector_id' => $_REQUEST['sector_name'], 'service1_id' => $_REQUEST['service1_name'], 'service2_id' => $_REQUEST['service2_name'], 'is_active' => 1);
                $respon = $this->mastermodel->UpdateRecords('pds_service_description', $where, $arr);
                redirect(base_url('company/pds_description'));
            endif;
        }else {
            $this->session->set_flashdata('error_msg', 'Something went gone wrong');
            redirect(base_url('company/pds_description'));
        }
    }

    public function getPDSDesc_ajax() {
        //print_r($_REQUEST); die;
        if (!empty($_REQUEST)) {
            $wheres = array('sector_id' => $_REQUEST['sector_name'], 'service1_id' => $_REQUEST['service1_name'], 'service2_id' => $_REQUEST['service2_name'], 'is_active' => 1);
            $pdsrec = $this->mastermodel->SelectRecord('pds_service_description', $wheres);
            if (!empty($pdsrec)) {
                echo json_encode(array('desc' => $pdsrec[0]->description));
            } else {
                return false;
            }
            die;
        }
    }

    public function getservice1_ajax() {
        $secID = $_REQUEST['secid'];
        if ($secID):
            $wheres = array('sector_id' => $secID, 'is_active' => 1);
            $pdsrec = $this->mastermodel->SelectRecord('pds_service1', $wheres);
            if (!empty($pdsrec)) {
                echo json_encode($pdsrec);
            } else {
                return false;
            }
            die;
        endif;
    }

    public function getservice2_ajax() {
        $secID = $_REQUEST['secid'];
        $service1_name_id = $_REQUEST['service1_name_id'];
        if ($secID):
            $wheres = array('sector_id' => $secID, 'service1_id' => $service1_name_id, 'is_active' => 1);
            $pdsrec = $this->mastermodel->SelectRecord('pds_service2', $wheres);
            if (!empty($pdsrec)) {
                echo json_encode($pdsrec);
            } else {
                return false;
            }
            die;
        endif;
    }

    public function ceg_laneRemove() {
        $id = $_POST['id'];
        $res = $this->db->delete('ceg_lane', array('id' => $id));
        echo '1';
    }

    // First Cron 
    public function insertbdscope() {
        $this->db->select('fld_id,visible_scope');
        $this->db->from('bd_tenderdetail');
        $res = $this->db->get()->result_object();
        //echo '<pre>'; print_r($res);
        foreach ($res as $value) {
            $count = $this->mastermodel->count_rows('bdtender_scope', array('project_id' => $value->fld_id));
            if ($count < 1):
                $IncrtArr = array(
                    'project_id' => $value->fld_id,
                    'visible_scope' => $value->visible_scope,
                    'businessunit_id' => 1,
                );
                $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdtender_scope');
                echo $value->fld_id . '<br/>';
            endif;
        }
    }






    // Second Cron 
    public function insertProjectHistory() {
        $arr = array('Active_project', 'Important_project', 'In_Review_project', 'To_Go_project', 'Bid_project');
        $this->db->select('id,project_id,visible_scope');
        $this->db->from('bdtender_scope');
        $this->db->where_in('visible_scope', $arr);
        $res = $this->db->get()->result_object();
        //echo '<pre>'; print_r($res); die;
        foreach ($res as $value) {
            if ($value->visible_scope == 'Active_project') {
                $count = $this->mastermodel->count_rows('bdactive_project_byuser', array('project_id' => $value->id));
                if ($count < 1):
                    $IncrtArr = array(
                        'project_id' => $value->id,
                        'user_id' => $this->session->userdata('loginid'),
                        'action_date' => date('Y-m-d H:i:s'),
                        'actionIP' => get_client_ip(),
                        'message' => 'Active Project'
                    );
                    $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdactive_project_byuser');
                    echo $value->fld_id . '<br/>';
                endif;
            }
            elseif ($value->visible_scope == 'Important_project') {
                $count = $this->mastermodel->count_rows('bdimportant_project_byuser', array('project_id' => $value->id));
                if ($count < 1):
                    $IncrtArr = array(
                        'project_id' => $value->id,
                        'user_id' => $this->session->userdata('loginid'),
                        'action_date' => date('Y-m-d H:i:s'),
                        'actionIP' => get_client_ip(),
                        'message' => 'Important Project'
                    );
                    $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdimportant_project_byuser');
                    echo $value->fld_id . '<br/>';
                endif;
            }
            elseif ($value->visible_scope == 'In_Review_project') {
                $count = $this->mastermodel->count_rows('bdinreview_project_byuser', array('project_id' => $value->id));
                if ($count < 1):
                    $IncrtArr = array(
                        'project_id' => $value->id,
                        'user_id' => $this->session->userdata('loginid'),
                        'action_date' => date('Y-m-d H:i:s'),
                        'actionIP' => get_client_ip(),
                        'message' => 'In Review Project'
                    );
                    $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdinreview_project_byuser');
                    echo $value->fld_id . '<br/>';
                endif;
            }
            elseif ($value->visible_scope == 'To_Go_project') {
                $count = $this->mastermodel->count_rows('bdtogo_project_byuser', array('project_id' => $value->id));
                if ($count < 1):
                    $IncrtArr = array(
                        'project_id' => $value->id,
                        'user_id' => $this->session->userdata('loginid'),
                        'action_date' => date('Y-m-d H:i:s'),
                        'actionIP' => get_client_ip(),
                        'message' => 'Ongoing Bidding Project'
                    );
                    $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdtogo_project_byuser');
                    echo $value->fld_id . '<br/>';
                endif;
            }
            elseif ($value->visible_scope == 'Bid_project') {
                $count = $this->mastermodel->count_rows('bdbid_project_byuser', array('project_id' => $value->id));
                if ($count < 1):
                    $IncrtArr = array(
                        'project_id' => $value->id,
                        'user_id' => $this->session->userdata('loginid'),
                        'action_date' => date('Y-m-d H:i:s'),
                        'actionIP' => get_client_ip(),
                        'message' => 'Bid Project'
                    );
                    $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdbid_project_byuser');
                    echo $value->fld_id . '<br/>';
                endif;
            }
        }
    }

    // Third Cron 
    public function insertdeletebdscope() {
        $this->db->select('parent_tbl_id,visible_scope');
        $this->db->from('tenderdetails_dump');
        $res = $this->db->get()->result_object();
        //echo '<pre>'; print_r($res);
        foreach ($res as $value) {
            $count = $this->mastermodel->count_rows('bdtender_scopedump', array('project_id' => $value->parent_tbl_id));
            if ($count < 1):
                $IncrtArr = array(
                    'project_id' => $value->parent_tbl_id,
                    'visible_scope' => $value->visible_scope,
                    'businessunit_id' => 1,
                );
                $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdtender_scopedump');
                echo $value->parent_tbl_id . '<br/>';
            endif;
        }
    }

    // Fourth Cron 
    public function insertdeleteProjectHistory() {
        $arr = array('InActive_project', 'Not_Important_project', 'No_Go_project', 'No_Submit_project');
        $this->db->select('id,project_id,visible_scope');
        $this->db->from('bdtender_scope');
        $this->db->where_in('visible_scope', $arr);

        $res = $this->db->get()->result_object();
        foreach ($res as $value) {
            if ($value->visible_scope == 'InActive_project') {
                $count = $this->mastermodel->count_rows('bdinactive_project_byuser', array('project_id' => $value->id));
                if ($count < 1):
                    $IncrtArr = array(
                        'project_id' => $value->id,
                        'user_id' => $this->session->userdata('loginid'),
                        'action_date' => date('Y-m-d H:i:s'),
                        'actionIP' => get_client_ip(),
                        'message' => 'InActive Project'
                    );
                    $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdinactive_project_byuser');
                    echo $value->fld_id . '<br/>';
                endif;
            }
            elseif ($value->visible_scope == 'Not_Important_project') {
                $count = $this->mastermodel->count_rows('bdnotimp_project_byuser', array('project_id' => $value->id));
                if ($count < 1):
                    $IncrtArr = array(
                        'project_id' => $value->id,
                        'user_id' => $this->session->userdata('loginid'),
                        'action_date' => date('Y-m-d H:i:s'),
                        'actionIP' => get_client_ip(),
                        'message' => 'Not Important Project'
                    );
                    $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdnotimp_project_byuser');
                    echo $value->fld_id . '<br/>';
                endif;
            }
            elseif ($value->visible_scope == 'No_Go_project') {
                $count = $this->mastermodel->count_rows('bdnogo_project_byuser', array('project_id' => $value->id));
                if ($count < 1):
                    $IncrtArr = array(
                        'project_id' => $value->id,
                        'user_id' => $this->session->userdata('loginid'),
                        'action_date' => date('Y-m-d H:i:s'),
                        'actionIP' => get_client_ip(),
                        'message' => 'Nogo Project'
                    );
                    $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdnogo_project_byuser');
                    echo $value->fld_id . '<br/>';
                endif;
            }
            elseif ($value->visible_scope == 'No_Submit_project') {
                $count = $this->mastermodel->count_rows('bdnotsubmit_project_byuser', array('project_id' => $value->id));
                if ($count < 1):
                    $IncrtArr = array(
                        'project_id' => $value->id,
                        'user_id' => $this->session->userdata('loginid'),
                        'action_date' => date('Y-m-d H:i:s'),
                        'actionIP' => get_client_ip(),
                        'message' => 'Not Submit Project'
                    );
                    $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdnotsubmit_project_byuser');
                    echo $value->fld_id . '<br/>';
                endif;
            }
        }
    }

    // First 
    public function insertDumpData() {
        $arr = array('InActive_project', 'Not_Important_project', 'No_Go_project', 'No_Submit_project');
        $this->db->select('*');
        $this->db->from('bdtenderdetails_dumpchk13');
        $this->db->where_in('visible_scope', $arr);
        $this->db->where('fld_id >', 117536);

        $res = $this->db->get()->result_object();
        echo '<pre>';
        print_r($res);
        die;
        foreach ($res as $value) {
            $count = $this->mastermodel->count_rows('bdtenderdetails_dump', array('parent_tbl_id' => $value->fld_id));
            if ($count < 1):
                $IncrtArr = array(
                    'created_By' => $value->created_By,
                    'tender24_ID' => $value->tender24_ID,
                    'TenderDetails' => $value->TenderDetails,
                    'TenderValue' => $value->TenderValue,
                    'Location' => $value->Location,
                    'Organization' => $value->Organization,
                    'Tender_url' => $value->Tender_url,
                    'Sector_IDs' => $value->Sector_IDs,
                    'Sector_Keyword_IDs' => $value->Sector_Keyword_IDs,
                    'Keyword_IDs' => $value->Keyword_IDs,
                    'keyword_phase' => $value->keyword_phase,
                    'Created_Date' => $value->Created_Date,
                    'Expiry_Date' => $value->Expiry_Date,
                    'source_file' => $value->source_file,
                    'visible_scope' => $value->visible_scope,
                    'project_type' => $value->project_type,
                    'country' => $value->country,
                    'remarks' => $value->remarks,
                    'bid_validity' => $value->bid_validity,
                    'national_intern' => $value->national_intern,
                    'national_status' => $value->national_status,
                    'parent_tbl_id' => $value->fld_id
                );
                $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bdtenderdetails_dump');
                echo $value->fld_id . '<br/>';
            endif;
        }
    }

    public function cronexpiry() {
        $arr = array('Active_project', 'In_Review_project', 'New_project', 'Important_project');
        $this->db->select('a.*,b.*');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('bdtender_scope as b', 'a.fld_id = b.project_id', 'left');
        $this->db->where_in('visible_scope', $arr);
        $res = $this->db->get()->result_object();
        //echo '<pre>'; print_r($res); die;
        foreach ($res as $value) {

            $expdate = str_replace('/', '-', $value->Expiry_Date);
            $realDateFormate = date("Y-m-d", strtotime($expdate));

            $updaterec = array('Expiry_Date' => $realDateFormate);
            $respp = $this->mastermodel->UpdateRecords('bd_tenderdetail', array('fld_id' => $value->fld_id), $updaterec);

            $expire_time = strtotime($realDateFormate);
            $today_time = strtotime(date('Y-m-d'));


            if ($expire_time < $today_time) {
                $count = $this->mastermodel->count_rows('expiry_table', array('parent_tbl_id' => $value->fld_id));
                if ($count < 1):
                    $IncrtArr = array(
                        'created_By' => $value->created_By,
                        'tender24_ID' => $value->tender24_ID,
                        'TenderDetails' => $value->TenderDetails,
                        'TenderValue' => $value->TenderValue,
                        'Location' => $value->Location,
                        'Organization' => $value->Organization,
                        'Tender_url' => $value->Tender_url,
                        'Sector_IDs' => $value->Sector_IDs,
                        'Sector_Keyword_IDs' => $value->Sector_Keyword_IDs,
                        'Keyword_IDs' => $value->Keyword_IDs,
                        'keyword_phase' => $value->keyword_phase,
                        'Created_Date' => $value->Created_Date,
                        'Expiry_Date' => $value->Expiry_Date,
                        'source_file' => $value->source_file,
                        'visible_scope' => $value->visible_scope,
                        'project_type' => $value->project_type,
                        'country' => $value->country,
                        'remarks' => $value->remarks,
                        //'bid_validity' => $value->bid_validity,
                        'national_intern' => $value->national_intern,
                        'national_status' => $value->national_status,
                        'parent_tbl_id' => $value->fld_id,
                        'businessunit_id' => $value->businessunit_id,
                    );
                    $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'expiry_table');
                    echo $value->fld_id . '<br/>';
                    if ($respon) {
                        $res = $this->db->delete('bd_tenderdetail', array('fld_id' => $value->fld_id));
                        $res = $this->db->delete('bdtender_scope', array('project_id' => $value->fld_id));
                    }
                endif;
            }
        }
    }

    public function cronexpirydump() {
        //$arr = array('No_Go_project', 'Not_Important_project');
        $this->db->select('a.*,b.*');
        $this->db->from('bdtenderdetails_dump as a');
        $this->db->join('bdtender_scopedump as b', 'a.parent_tbl_id = b.project_id', 'left');
        //$this->db->where_in('b.visible_scope', $arr);
        //$this->db->where('b.visible_scope','No_Go_project');
        //$this->db->limit(50);
        $res = $this->db->get()->result_object();
        //echo '<pre>'; print_r($res); die;
        foreach ($res as $value) {

            $expdate = str_replace('/', '-', $value->Expiry_Date);
            $realDateFormate = date("Y-m-d", strtotime($expdate));

            $updaterec = array('Expiry_Date' => $realDateFormate);
            $respp = $this->mastermodel->UpdateRecords('bdtenderdetails_dump', array('parent_tbl_id' => $value->parent_tbl_id), $updaterec);


            $expire_time = strtotime($realDateFormate);
            $today_time = strtotime(date('Y-m-d'));


            if ($expire_time < $today_time) {
                $count = $this->mastermodel->count_rows('expiry_table', array('parent_tbl_id' => $value->parent_tbl_id));
                if ($count < 1):
                    $IncrtArr = array(
                        'created_By' => $value->created_By,
                        'tender24_ID' => $value->tender24_ID,
                        'TenderDetails' => $value->TenderDetails,
                        'TenderValue' => $value->TenderValue,
                        'Location' => $value->Location,
                        'Organization' => $value->Organization,
                        'Tender_url' => $value->Tender_url,
                        'Sector_IDs' => $value->Sector_IDs,
                        'Sector_Keyword_IDs' => $value->Sector_Keyword_IDs,
                        'Keyword_IDs' => $value->Keyword_IDs,
                        'keyword_phase' => $value->keyword_phase,
                        'Created_Date' => $value->Created_Date,
                        'Expiry_Date' => $value->Expiry_Date,
                        'source_file' => $value->source_file,
                        'visible_scope' => $value->visible_scope,
                        'project_type' => $value->project_type,
                        'country' => $value->country,
                        'remarks' => $value->remarks,
                        //'bid_validity' => $value->bid_validity,
                        'national_intern' => $value->national_intern,
                        'national_status' => $value->national_status,
                        'parent_tbl_id' => $value->parent_tbl_id,
                        'businessunit_id' => $value->businessunit_id,
                    );
                    $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'expiry_table');

                    if ($respon) {
                        $res = $this->db->delete('bdtenderdetails_dump', array('parent_tbl_id' => $value->parent_tbl_id));
                        $res = $this->db->delete('bdtender_scopedump', array('project_id' => $value->parent_tbl_id));
                    }
                    echo $value->parent_tbl_id . '<br/>';
                endif;
            }
        }
    }

    public function getdumpdata() {
        $this->db->select('a.*,b.*');
        $this->db->from('bdtenderdetails_dump as a');
        $this->db->join('bdtender_scopedump as b', 'a.parent_tbl_id = b.project_id', 'left');
        $this->db->where('b.visible_scope', 'No_Submit_project');
        $res = $this->db->get()->result_object();
        foreach ($res as $value) {
            $count = $this->mastermodel->count_rows('bd_tenderdetail', array('fld_id' => $value->parent_tbl_id));
            if ($count < 1):
                $IncrtArr = array(
                    'fld_id' => $value->parent_tbl_id,
                    'created_By' => $value->created_By,
                    'tender24_ID' => $value->tender24_ID,
                    'TenderDetails' => $value->TenderDetails,
                    'TenderValue' => $value->TenderValue,
                    'Location' => $value->Location,
                    'Organization' => $value->Organization,
                    'Tender_url' => $value->Tender_url,
                    'Sector_IDs' => $value->Sector_IDs,
                    'Sector_Keyword_IDs' => $value->Sector_Keyword_IDs,
                    'Keyword_IDs' => $value->Keyword_IDs,
                    'keyword_phase' => $value->keyword_phase,
                    'Created_Date' => $value->Created_Date,
                    'Expiry_Date' => $value->Expiry_Date,
                    'source_file' => $value->source_file,
                    'project_type' => $value->project_type,
                    'country' => $value->country,
                    'remarks' => $value->remarks,
                    //'bid_validity' => $value->bid_validity,
                    'national_intern' => $value->national_intern,
                    'national_status' => $value->national_status,
                );
                $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bd_tenderdetail');

                if ($respon) {
                    $IncrtArr1 = array(
                        'project_id' => $value->parent_tbl_id,
                        'visible_scope' => $value->visible_scope,
                        'businessunit_id' => $value->businessunit_id
                    );
                    $respon = $this->mastermodel->InsertMasterData($IncrtArr1, 'bdtender_scope');
                    $res = $this->db->delete('bdtenderdetails_dump', array('fld_id' => $value->parent_tbl_id));
                    $res = $this->db->delete('bdtender_scopedump', array('project_id' => $value->parent_tbl_id));
                }
                echo $value->parent_tbl_id . '<br/>';
            endif;
        }
    }

    public function addgeneretedID() {
        $this->db->select('*');
        $this->db->from('tender_generated_id');
        $res = $this->db->get()->result();
        foreach ($res as $data) {
            $val = explode('/', $data->generated_tenderid);
            $str = preg_replace("/[^0-9]/", "", $val[3]) . '<br/>';
            $respp = $this->mastermodel->UpdateRecords('tender_generated_id', array('fld_id' => $data->fld_id), array('generate_id' => $str));
            echo $val->fld_id;
        }
    }

    public function websiteedit() {
        $data['title'] = 'Edit Website';
        $id = $this->uri->segment(3);
        if ($id) {
            $this->db->select('*');
            $this->db->from('tender_websites');
            $this->db->where('status', '1');
            $this->db->where('fld_id', $id);
            $data['res'] = $this->db->get()->result();
            if ($data['res']) {
                $this->load->view('company/websiteedit', $data);
            } else {
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('company/websites'));
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Something went gone wrong');
            redirect(base_url('company/websites'));
        }
    }

    public function savewebsite() {
        if ($_REQUEST) {
            $updaterec = array(
                'website_url' => $_REQUEST['website_url'],
                'department' => $_REQUEST['department'],
                'eproc' => $_REQUEST['eproc'],
                'user_name' => $_REQUEST['user_name'],
                'password_encode' => $_REQUEST['password_encode'],
                'valid_to' => $_REQUEST['valid_to'],
                'valid_from' => $_REQUEST['valid_from'],
                'other_details' => $_REQUEST['other_details']
            );

            $respp = $this->mastermodel->UpdateRecords('tender_websites', array('fld_id' => $_REQUEST['company_id']), $updaterec);
        }
        if ($respon) {
            $this->session->set_flashdata('error_msg', 'Something went gone wrong');
            redirect(base_url('company/websiteedit/' . $_REQUEST['company_id']));
        } else {
            $this->session->set_flashdata('error_msg', 'Something went gone wrong');
            redirect(base_url('company/websiteedit/' . $_REQUEST['company_id']));
        }
    }

    public function websitedelete() {
        $delId = $this->uri->segment(3);
        if ($delId):
            $respp = $this->mastermodel->UpdateRecords('tender_websites', array('fld_id' => $delId), array('status' => '0'));
            $this->session->set_flashdata('msg', 'Website Deleted Successfully');
        endif;
        redirect(base_url('company/websites'));
    }

    public function websiteadd() {
        if ($_REQUEST) {
            $respon = $this->mastermodel->InsertMasterData($_REQUEST, 'tender_websites');
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Website Added successfully');
                redirect(base_url('company/websites'));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('company/websites'));
            endif;
        }
        $this->load->view('company/websiteadd');
    }

    public function getProjectData() {
        $project = $this->mastermodel->getProjectData();
        //echo '<pre>'; print_r($project); die;
        $delimiter = ",";
        $filename = 'tender' . date('Ymd') . '.csv';
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/csv; ");

        // file creation 
        $file = fopen('php://output', 'w');

        $header = array("ID", "Exp-Date", "Tender Details", "Tender ID", "Tender Type", "Location", "Organization", "Actions", "Proposal Manager");
        fputcsv($file, $header, $delimiter);
        $i = 1;
        foreach ($project as $val) {
            $expdate = date('d/m/Y', strtotime($val->Expiry_Date));
            if ($val->project_status == 0) {
                $status = 'Awaiting';
            } else if ($val->project_status == 1) {
                $status = 'Won';
            } else if ($val->project_status == 2) {
                $status = 'Loose';
            } else if ($val->project_status == 3) {
                $status = 'Cancel';
            }
            $user = $this->SecondDB_model->getUserByID($val->assign_to);
            $line = array($i, $expdate, $val->TenderDetails, $val->generated_tenderid, $val->generate_type, $val->Location, $val->Organization, $status, $user->userfullname);
            fputcsv($file, $line, $delimiter);
            $i++;
        }
        fclose($file);
        exit;
    }

    public function getexpirydata() {
        $this->db->select('*');
        $this->db->from('expiry_table');
        // 104794 , 100133 , 105141 , 107295
        $this->db->where('parent_tbl_id', '107295');
        $res = $this->db->get()->result_object();
        //echo '<pre>'; print_r($res); die;
        foreach ($res as $value) {
            $count = $this->mastermodel->count_rows('bd_tenderdetail', array('fld_id' => $value->parent_tbl_id));
            if ($count < 1):
                $IncrtArr = array(
                    'fld_id' => $value->parent_tbl_id,
                    'created_By' => $value->created_By,
                    'tender24_ID' => $value->tender24_ID,
                    'TenderDetails' => $value->TenderDetails,
                    'TenderValue' => $value->TenderValue,
                    'Location' => $value->Location,
                    'Organization' => $value->Organization,
                    'Tender_url' => $value->Tender_url,
                    'Sector_IDs' => $value->Sector_IDs,
                    'Sector_Keyword_IDs' => $value->Sector_Keyword_IDs,
                    'Keyword_IDs' => $value->Keyword_IDs,
                    'keyword_phase' => $value->keyword_phase,
                    'Created_Date' => $value->Created_Date,
                    'Expiry_Date' => $value->Expiry_Date,
                    'source_file' => $value->source_file,
                    'project_type' => $value->project_type,
                    'country' => $value->country,
                    'remarks' => $value->remarks,
                    //'bid_validity' => $value->bid_validity,
                    'national_intern' => $value->national_intern,
                    'national_status' => $value->national_status,
                );
                $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'bd_tenderdetail');

                if ($respon) {
                    $IncrtArr1 = array(
                        'project_id' => $value->parent_tbl_id,
                        'visible_scope' => 'Bid_project',
                        'businessunit_id' => $value->businessunit_id
                    );
                    $respon = $this->mastermodel->InsertMasterData($IncrtArr1, 'bdtender_scope');
                    $res = $this->db->delete('expiry_table', array('parent_tbl_id' => $value->parent_tbl_id));
                }
                echo $value->parent_tbl_id . '<br/>';
            endif;
        }
    }

    public function cronexpirytable() {
        $arr = array('In_Review_project');
        $this->db->select('a.*,b.*');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('bdtender_scope as b', 'a.fld_id = b.project_id', 'left');
        $this->db->where_in('visible_scope', $arr);
        $res = $this->db->get()->result_object();
        foreach ($res as $value) {

            /* $expdate = str_replace('/', '-', $value->Expiry_Date);
              $realDateFormate = date("Y-m-d", strtotime($expdate));

              $updaterec = array('Expiry_Date' => $realDateFormate);
              $respp = $this->mastermodel->UpdateRecords('bd_tenderdetail', array('fld_id' => $value->fld_id), $updaterec);
             */

            //$expire_time = strtotime($realDateFormate);
            $expire_time = strtotime($value->Expiry_Date);
            $today_time = strtotime(date('Y-m-d'));

            if ($expire_time < $today_time) {
                $count = $this->mastermodel->count_rows('expiry_table', array('parent_tbl_id' => $value->fld_id));
                if ($count < 1):
                    $IncrtArr = array(
                        'created_By' => $value->created_By,
                        'tender24_ID' => $value->tender24_ID,
                        'TenderDetails' => $value->TenderDetails,
                        'TenderValue' => $value->TenderValue,
                        'Location' => $value->Location,
                        'Organization' => $value->Organization,
                        'Tender_url' => $value->Tender_url,
                        'Sector_IDs' => $value->Sector_IDs,
                        'Sector_Keyword_IDs' => $value->Sector_Keyword_IDs,
                        'Keyword_IDs' => $value->Keyword_IDs,
                        'keyword_phase' => $value->keyword_phase,
                        'Created_Date' => $value->Created_Date,
                        'Expiry_Date' => $value->Expiry_Date,
                        'source_file' => $value->source_file,
                        'visible_scope' => $value->visible_scope,
                        'project_type' => $value->project_type,
                        'country' => $value->country,
                        'remarks' => $value->remarks,
                        //'bid_validity' => $value->bid_validity,
                        'national_intern' => $value->national_intern,
                        'national_status' => $value->national_status,
                        'parent_tbl_id' => $value->fld_id,
                        'businessunit_id' => $value->businessunit_id,
                    );
                    $respon = $this->mastermodel->InsertMasterData($IncrtArr, 'expiry_table');
                    echo $value->fld_id . '<br/>';
                    if ($respon) {
                        $res = $this->db->delete('bd_tenderdetail', array('fld_id' => $value->fld_id));
                        $res = $this->db->delete('bdtender_scope', array('project_id' => $value->fld_id));
                    }
                endif;
            }
        }
    }

    /*     * ************** Code For Add and Update Financial Detail by durgesh******************** */

    public function updatefinancialdetail() {
        $Arr = $this->input->post();
        if ($Arr) {
            $count = $this->mastermodel->count_rows('proj_financial_details', array('bd_projid' => $Arr['project_id']));
            if ($count < 1):
                $recArr = array(
                    'bd_projid' => $Arr['project_id'],
                    'financial_proprosal' => $Arr['financial_proprosal'],
                    'value_as_per_loa' => $Arr['value_as_per_loa'],
                    'Revised_Contract_Value' => $Arr['revised_contract_value'],
                    'financial_detail' => json_encode($Arr),
                    'entry_by' => $this->session->userdata('loginid'),
                    'entry_date' => date('Y-m-d h:i:s')
                );
                $respp = $this->mastermodel->InsertMasterData($recArr, 'proj_financial_details');
            else:
                $recArr = array(
                    'bd_projid' => $Arr['project_id'],
                    'financial_proprosal' => $Arr['financial_proprosal'],
                    'value_as_per_loa' => $Arr['value_as_per_loa'],
                    'Revised_Contract_Value' => $Arr['revised_contract_value'],
                    'financial_detail' => json_encode($Arr),
                    'entry_by' => $this->session->userdata('loginid'),
                    'entry_date' => date('Y-m-d h:i:s')
                );
                $respp = $this->mastermodel->UpdateRecords('proj_financial_details', array('bd_projid' => $Arr['project_id']), $recArr);
            endif;
            if ($respp):
                $this->session->set_flashdata('msg', 'Record Updated successfully');
                // redirect(base_url('dashboard/viewedit/' . $Arr['project_id']));
                redirect($_SERVER['HTTP_REFERER']);
            else:
                // redirect(base_url('dashboard/viewedit/' . $Arr['project_id']));
                redirect($_SERVER['HTTP_REFERER']);
            endif;
        }
    }

//    public function updatefinancialdetail() {
//        $Arr = $_REQUEST;
//        if ($Arr) {
//            $count = $this->mastermodel->count_rows('proj_financial_details', array('bd_projid' => $Arr['project_id']));
//            if ($count < 1):
//                $recArr = array(
//                    'bd_projid' => $Arr['project_id'],
//                    'financial_proprosal' => str_replace(",", "", $Arr['financial_proprosal']),
//                    'value_as_per_loa' => str_replace(",", "", $Arr['value_as_per_loa']),
//                    'Revised_Contract_Value' => str_replace(",", "", $Arr['revised_contract_value']),
//                    'lead_amount' => str_replace(",", "", $Arr['lead_amount']),
//                    'lead_share' => str_replace(",", "", $Arr['lead_share']),
//                    'jv_amount' => str_replace(",", "", $Arr['jv_amount']),
//                    'jv_share' => str_replace(",", "", $Arr['jv_share']),
//                    'assoc_amount' => str_replace(",", "", $Arr['assoc_amount']),
//                    'assoc_share' => str_replace(",", "", $Arr['assoc_share']),
//                    'entry_by' => $this->session->userdata('loginid')
//                );
//                $respp = $this->mastermodel->InsertMasterData($recArr, 'proj_financial_details');
//            else:
//                $recArr = array(
//                    'bd_projid' => $Arr['project_id'],
//                    'financial_proprosal' => str_replace(",", "", $Arr['financial_proprosal']),
//                    'value_as_per_loa' => str_replace(",", "", $Arr['value_as_per_loa']),
//                    'revised_contract_value' => str_replace(",", "", $Arr['revised_contract_value']),
//                    'lead_amount' => str_replace(",", "", $Arr['lead_amount']),
//                    'lead_share' => str_replace(",", "", $Arr['lead_share']),
//                    'jv_amount' => str_replace(",", "", $Arr['jv_amount']),
//                    'jv_share' => str_replace(",", "", $Arr['jv_share']),
//                    'assoc_amount' => str_replace(",", "", $Arr['assoc_amount']),
//                    'assoc_share' => str_replace(",", "", $Arr['assoc_share']),
//                    'entry_by' => $this->session->userdata('loginid')
//                );
//                $respp = $this->mastermodel->UpdateRecords('proj_financial_details', array('bd_projid' => $Arr['project_id']), $recArr);
//            endif;
//            if ($respp):
//                $this->session->set_flashdata('msg', 'Record Updated successfully');
//                // redirect(base_url('dashboard/viewedit/' . $Arr['project_id']));
//                redirect($_SERVER['HTTP_REFERER']);
//            else:
//                // redirect(base_url('dashboard/viewedit/' . $Arr['project_id']));
//                redirect($_SERVER['HTTP_REFERER']);
//            endif;
//        }
//    }

    /*     * ************** Code For Add and Update Performance BD Detail by durgesh******************** */

    public function updateperformancebddetail() {



        $Arr = $_REQUEST;
        if ($Arr) {
            $count = $this->mastermodel->count_rows('proj_performance_Bd_details', array('bd_projid' => $Arr['project_id']));
            if ($count < 1):
                $recArr = array(
                    'bd_projid' => $Arr['project_id'],
                    'pbg_amount' => $Arr['pbg_amount'],
                    'pbg_number' => $Arr['pbg_number'],
                    'pbg_start' => $Arr['pbg_start'],
                    'pbg_validity' => $Arr['pbg_validity'],
                    'pbg_bank' => $Arr['pbg_bank'],
                    'abg_amount' => $Arr['abg_amount'],
                    'abg_number' => $Arr['abg_number'],
                    'abg_start' => $Arr['abg_start'],
                    'abg_validity' => $Arr['abg_validity'],
                    'abg_bank' => $Arr['abg_bank'],
                    'pbg' => $Arr['pbg_name'],
                    'abg' => $Arr['abg_name'],
                    'entry_by' => $this->session->userdata('loginid')
                );
                $respp = $this->mastermodel->InsertMasterData($recArr, 'proj_performance_Bd_details');
            else:
                $recArr = array(
                    'bd_projid' => $Arr['project_id'],
                    'pbg_amount' => $Arr['pbg_amount'],
                    'pbg_number' => $Arr['pbg_number'],
                    'pbg_start' => $Arr['pbg_start'],
                    'pbg_validity' => $Arr['pbg_validity'],
                    'pbg_bank' => $Arr['pbg_bank'],
                    'abg_amount' => $Arr['abg_amount'],
                    'abg_number' => $Arr['abg_number'],
                    'abg_start' => $Arr['abg_start'],
                    'abg_validity' => $Arr['abg_validity'],
                    'abg_bank' => $Arr['abg_bank'],
                    'pbg' => $Arr['pbg_name'],
                    'abg' => $Arr['abg_name'],
                    'entry_by' => $this->session->userdata('loginid')
                );
                $respp = $this->mastermodel->UpdateRecords('proj_performance_Bd_details', array('bd_projid' => $Arr['project_id']), $recArr);
            endif;



            
        $inserArr = array(
            'user_id' => $this->session->userdata('loginid'),
            'project_id' => $_REQUEST['project_id'],
            // 'country_id' => $_REQUEST['countries_id'],
            // 'state_id' => $_REQUEST['state_id'],
            // 'service_id' => $_REQUEST['services_id'],
            // 'sector_id' => $_REQUEST['sectors_id'],
            //BID Security..
            'bid_security' => $_REQUEST['bid_security'],
            'bid_securitytype' => $_REQUEST['bid_securitytype'],
            'amount' => $_REQUEST['amount'],
            'bs_valdity' => $_REQUEST['bs_valdity'],

             
            'sf_bankname' => $_REQUEST['sf_bankname'],
            'sf_bgnumber' => $_REQUEST['sf_bgnumber'],
            'sf_dateofstart' => $_REQUEST['sf_dateofstart'],
            



            'pf_valdity' => $_REQUEST['pf_valdity'],
            'pf_bankname' => $_REQUEST['pf_bankname'],
            'pf_bgnumber' => $_REQUEST['pf_bgnumber'],
            'pf_dateofstart' => $_REQUEST['pf_dateofstart'],


            


            //RFP..
            'rfp_cost' => $_REQUEST['rfp_cost'],
            'rfp_amount' => $_REQUEST['rfp_amount'],
            'rfp_type' => $_REQUEST['rfp_type'],


            'rfp_valdity' => $_REQUEST['rfp_valdity'],
            'rpf_bankname' => $_REQUEST['rpf_bankname'],
            'rpf_bgnumber' => $_REQUEST['rpf_bgnumber'],
            'rpf_dateofstart' => $_REQUEST['rpf_dateofstart'],

          


            //Processing Fee..
            'processing_fee' => $_REQUEST['processing_fee'],
            'pfee_amount' => $_REQUEST['pfee_amount'],
            'processing_fee_type' => $_REQUEST['processing_fee_type'],
            //Last Section..
            // 'submission_date' => $_REQUEST['submission_date'],
            'tender_openingdate' => $_REQUEST['tender_openingdate']);

            




            

            $ql = $this->db->select('*')->from('project_description')->where('project_id',$_REQUEST['project_id'])->get();

            if( $ql->num_rows() > 0 ) {

                $updateArray = array_filter($inserArr);
                $Respon = $this->Front_model->updateRecord('project_description', $updateArray, array('project_id' => $_REQUEST['project_id']));

            } else {
                $Respon = $this->Front_model->insertRecord('project_description', $inserArr);
            }








          

            if($Respon > 0){
                $message = "Data updated successfully !";
            }else{
                $message = "Some issues occured";
            }



        }




       


        $arrMessage = array(
            'msg' => $message,
        );

        echo json_encode($arrMessage);
    }

    //code by durgesh for update pbg details
    public function updateperformancepbgdetail() {
        $Arr = $_REQUEST;
        //print_r($Arr);
        //die;
        $recArr = array(
            'bd_projid' => $Arr['project_id'],
            'pbg' => $Arr['pbg_name1'],
            'pbg_amount' => $Arr['pbg_amount'],
            'pbg_number' => $Arr['pbg_number'],
            'pbg_bank' => $Arr['pbg_bank'],
            'pbg_start' => $Arr['pbg_start'],
            'pbg_validity' => $Arr['pbg_validity'],
            'entry_by' => $this->session->userdata('loginid')
        );
        $SqLquery = "SELECT fld_id FROM proj_performance_Bd_details WHERE status='1' AND bd_projid='" . $Arr['project_id'] . "' AND ((`pbg_start` BETWEEN '" . $Arr['pbg_start'] . "' AND '" . $Arr['pbg_validity'] . "') OR (`pbg_validity` BETWEEN '" . $Arr['pbg_start'] . "' AND '" . $Arr['pbg_validity'] . "') OR (`pbg_start`<='" . $Arr['pbg_start'] . "' AND `pbg_validity`>='" . $Arr['pbg_validity'] . "')) ";
        $query = $this->db->query($SqLquery);
        if ($query->num_rows() > 0) {
            $this->session->set_flashdata('errormsg', 'Apply PBG Detail already exist');
        } else {
            $result = $this->db->insert('proj_performance_Bd_details', $recArr);
            $this->session->set_flashdata('msg', 'PBG Detail added successfully');
        }
        //redirect(base_url('dashboard/viewedit/' . $Arr['project_id']));
        redirect($_SERVER['HTTP_REFERER']);
    }

    //code by durgesh for update abg details
    public function updateperformanceabgdetail() {
        $Arr = $_REQUEST;
        $recArr = array(
            'bd_projid' => $Arr['project_id'],
            'abg' => $Arr['abg_name1'],
            'abg_amount' => $Arr['abg_amount'],
            'abg_number' => $Arr['abg_number'],
            'abg_bank' => $Arr['abg_bank'],
            'abg_start' => $Arr['abg_start'],
            'abg_validity' => $Arr['abg_validity'],
            'entry_by' => $this->session->userdata('loginid')
        );
        $SqLquery = "SELECT fld_id FROM proj_performance_Bd_details WHERE status='1' AND bd_projid='" . $Arr['project_id'] . "' AND ((`abg_start` BETWEEN '" . $Arr['abg_start'] . "' AND '" . $Arr['abg_validity'] . "') OR (`abg_validity` BETWEEN '" . $Arr['abg_start'] . "' AND '" . $Arr['abg_validity'] . "') OR (`abg_start`<='" . $Arr['abg_start'] . "' AND `abg_validity`>='" . $Arr['abg_validity'] . "')) ";
        $query = $this->db->query($SqLquery);
        if ($query->num_rows() > 0) {
            $this->session->set_flashdata('errormsg', 'Apply ABG Detail already exist');
        } else {
            $result = $this->db->insert('proj_performance_Bd_details', $recArr);
            $this->session->set_flashdata('msg', 'ABG Detail added successfully');
        }
        //redirect(base_url('dashboard/viewedit/' . $Arr['project_id']));
        redirect($_SERVER['HTTP_REFERER']);
    }

    // code by durgesh for bid scurity update on Bg Details
    public function update_bid_security() {
        $Arr = $_REQUEST;
        if ($Arr) {
            $updateArr = array(
                'bid_security' => $Arr['bid_security'],
                'bid_securitytype' => $Arr['bid_security_type'],
                'amount' => $Arr['bid_security_amount'],
                'bs_valdity' => date("Y-m-d", strtotime($Arr['bid_validity']))
            );
            $updateArr1 = array(
                'bidsecurity_type' => $Arr['bid_security_type'],
                'bidsecurity_amount' => $Arr['bid_security_amount'],
                'bid_validity' => date("Y-m-d", strtotime($Arr['bid_validity']))
            );
            $updateArr3 = array(
                'bid_security' => $Arr['bid_security'],
                'bid_securitytype' => 'NULL',
                'amount' => 'NULL',
                'bs_valdity' => 'NULL'
            );
            //print_r($updateArr);
            //die;
            if ($Arr['bid_security'] == "1") {
                $respp = $this->mastermodel->UpdateRecords('project_description', array('project_id' => $Arr['project_id']), $updateArr);
                $respp = $this->mastermodel->UpdateRecords('bdceg_exp', array('project_id' => $Arr['project_id']), $updateArr1);
            } else if ($Arr['bid_security'] == "2") {
                $respp = $this->mastermodel->UpdateRecords('project_description', array('project_id' => $Arr['project_id']), $updateArr3);
            }
            if ($respp):
                $this->session->set_flashdata('msg', 'Record Updated successfully');
                //redirect(base_url('dashboard/viewedit/' . $Arr['project_id']));
                redirect($_SERVER['HTTP_REFERER']);
            else:
                //redirect(base_url('dashboard/viewedit/' . $Arr['project_id']));
                redirect($_SERVER['HTTP_REFERER']);
            endif;
        }
    }

    //10-10-2019-- Asheesh..
    public function reportv() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->SELECT("$db1.company_extra_details.*");
        $this->db->FROM("$db1.company_extra_details");
        $this->db->WHERE(array("$db1.company_extra_details.status" => "1"));
        $this->db->order_by("$db1.company_extra_details.main_comp_id", "DESC");
        $CompArr = $this->db->get()->result();
        echo "<pre>";
        print_r($CompArr);
        die;

    }

    
    //code by durgesh 20-12-2019(for company project summary)
    public function project_summary_of_company($com_id) {
        $title = 'Project Summary Of Company';


        // print_r("expression"); die;
        
        ///get total list of project
        $this->db->select('a.project_id,a.compt_comp_id');
        $this->db->from('cegexp_competitor_comp as a');
        $this->db->where(array('a.compt_comp_id' => $com_id, 'a.status' => '1'));
        $this->db->order_by('a.fld_id', 'DESC');
        $this->db->group_by('a.project_id');
        $total_project_count = $this->db->get()->result();
        
        $counttotalproject = 0;
        $counttotalcompetitor = 0;
        $counttotallead = 0;
        $counttotaljv = 0;
        $counttotalassoc = 0;
        
        foreach ($total_project_count as $countproject) {
            
         //get total project  
         $counttotalproject += count($countproject->project_id);
         
         //for base competitor
         $countcompetitor=  get_basecomp($countproject->project_id, $countproject->compt_comp_id);
         
         //count base competitor
         $counttotalcompetitor+=count($countcompetitor->base_comp_id);
         
         //for lead partner
         $countlead = get_leadcomp($countproject->project_id, $countproject->compt_comp_id);
         
         //for jv partner
         $countjv = get_jvcomp($countproject->project_id, $countproject->compt_comp_id);
         
         //for assoc partner
         $countassoc = get_assoccomp($countproject->project_id, $countproject->compt_comp_id);
         
         //count lead partner with CEG
         $counttotallead+=count($countlead->lead_comp_id);
         
         //count Jv partner with CEG
         $counttotaljv+=count($countjv->joint_venture);
         
         //count Assoc partner with CEG
         $counttotalassoc+=count($countassoc->asso_comp);
        }
        $this->load->view('company/projectsummary_company_view', compact('title', 'counttotalproject','counttotalcompetitor','counttotallead','counttotaljv','counttotalassoc'));
    }

}
