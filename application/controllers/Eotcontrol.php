<?php

/* Database connection start */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Eotcontrol extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Front_model');
        $this->load->helper(array('form', 'url', 'user_helper'));

        date_default_timezone_set("Asia/Kolkata");
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);

        $bduserid = $this->session->userdata('uid');
        if ($bduserid == "") {
            redirect(base_url('welcome/logout'));
        }
    }

    public function eotmanage() {
        $data["error"] = "";
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $Formrec = "";
        $data['eoTrecArr'] = "";
        $data["title"] = "EOT - Manage Invoice";
        $data['hrmsProjListArr'] = $this->GetAllProjectArr();
        $Formrec = $this->input->post();
        if ($Formrec and $Formrec["ts_proj_filter"] and $Formrec["filter"]) {
            $this->db->select("$db1.eot_maineot_record.*");
            $this->db->from("$db1.eot_maineot_record");
            $this->db->where(array("$db1.eot_maineot_record.ts_proj_id" => $Formrec["ts_proj_filter"], "$db1.eot_maineot_record.status" => "1"));
            $data['eoTrecArr'] = $this->db->get()->result();
            $data['filterprojid'] = $Formrec["ts_proj_filter"];
        }
        $this->load->view('eot/manage_eot_view', $data);
    }

    public function save_eotrecords() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $Formrec = $this->input->post();
        $bdProjID = $this->Front_model->getprojectid_tstobd($Formrec['ts_project_id']);
        //For New EOT Entry..
        if ($Formrec and $Formrec['eot_type'] == "new" and $Formrec['submit']) {
            $insertArr = array("eot_type" => $Formrec['eot_type'],
                "parent_id" => "0",
                "ts_proj_id" => $Formrec['ts_project_id'],
                "bd_proj_id" => $bdProjID,
                "current_status" => "new",
                "rec_entryby" => $this->session->userdata('uid'),
                "eot_name" => $Formrec['eot_name'],
                "submision_date" => date("Y-m-d", strtotime($Formrec['submission_date'])),
                "eot_months" => $Formrec['eot_month']);
            $this->db->insert("$db1.eot_maineot_record", $insertArr);
            $lastId = $this->db->insert_id();
            //Generated ID.
            if ($lastId > 0):
                $str = trim($Formrec['eot_name']);
                $str2 = substr($str, 0, 4);
                $geNrAtedID = strtoupper($str2) . "/" . date("d/m/Y") . "/" . $lastId;
                $this->db->where("$db1.eot_maineot_record.fld_id", $lastId);
                $this->db->update("$db1.eot_maineot_record", array("$db1.eot_maineot_record.eot_generated_no" => $geNrAtedID));
                $this->session->set_flashdata('msg', 'New EOT Details Added Successfully');
            endif;
        }
        redirect(base_url("eotcontrol/eotmanage"));
    }

    //Edit Team....
    public function editeotdetails() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $projId = $_REQUEST["projid"];
        $RowId = $_REQUEST["rowid"];
       // $viewD = $_REQUEST["vw"];
        $data["viewD"] = (@$_REQUEST["vw"]) ? $_REQUEST["vw"] : "";
        
        //Start Get Team Records if Inserted..
        $this->db->select("$db1.eot_maineot_teamdata_temp.*");
        $this->db->from("$db1.eot_maineot_teamdata_temp");
        $this->db->where(array("$db1.eot_maineot_teamdata_temp.bd_projid" => $projId, "$db1.eot_maineot_teamdata_temp.eot_main_id" => $RowId, "$db1.eot_maineot_teamdata_temp.status" => "1"));
        $dataeoTrecArr = $this->db->get()->result();
        $allEOTrec = array();
        if ($dataeoTrecArr):
            foreach ($dataeoTrecArr as $rEcd) {
                $allEOTrec[$rEcd->designation_id] = $rEcd->eot_mm;
            }
        endif;
        //Close Get Team Records if Inserted..
        //Start Get Transportation Records if Inserted..
        $this->db->select("$db1.eot_maineot_transport_temp.*");
        $this->db->from("$db1.eot_maineot_transport_temp");
        $this->db->where(array("$db1.eot_maineot_transport_temp.bd_projid" => $projId, "$db1.eot_maineot_transport_temp.eot_main_id" => $RowId, "$db1.eot_maineot_transport_temp.status" => "1"));
        $dataeoTransportArr = $this->db->get()->result();
        $allEOTrecT = array();
        if ($dataeoTransportArr):
            foreach ($dataeoTransportArr as $rEcdT) {
                $allEOTrecT[$rEcdT->assign_main_id] = $rEcdT->eot_mm;
            }
        endif;
        //Close Get Transportation Records if Inserted..
        //Start Get Site Travel Records if Inserted..
        $this->db->select("$db1.eot_maineot_dutytravsite_temp.*");
        $this->db->from("$db1.eot_maineot_dutytravsite_temp");
        $this->db->where(array("$db1.eot_maineot_dutytravsite_temp.bd_projid" => $projId, "$db1.eot_maineot_dutytravsite_temp.eot_main_id" => $RowId, "$db1.eot_maineot_dutytravsite_temp.status" => "1"));
        $dataeoTDuty = $this->db->get()->result();
        $allEOTrecTR = array();
        if ($dataeoTDuty):
            foreach ($dataeoTDuty as $rEcdTr) {
                $allEOTrecTR[$rEcdTr->assign_main_id] = $rEcdTr->eot_mm;
            }
        endif;

        //Close Get Site Travel Records if Inserted..
        //Start Office Rent  Records if Inserted..
        $this->db->select("$db1.eot_maineot_ofcrent_temp.*");
        $this->db->from("$db1.eot_maineot_ofcrent_temp");
        $this->db->where(array("$db1.eot_maineot_ofcrent_temp.bd_projid" => $projId, "$db1.eot_maineot_ofcrent_temp.eot_main_id" => $RowId, "$db1.eot_maineot_ofcrent_temp.status" => "1"));
        $dataeoOfcRentArr = $this->db->get()->result();
        $allEOTrecOR = array();
        if ($dataeoOfcRentArr):
            foreach ($dataeoOfcRentArr as $rEcdoRe) {
                $allEOTrecOR[$rEcdoRe->assign_main_id] = $rEcdoRe->eot_mm;
            }
        endif;
        //Close Office Rent  Records if Inserted..
        //Start ofc suppl  Records if Inserted..
        $this->db->select("$db1.eot_maineot_ofcsuppl_temp.*");
        $this->db->from("$db1.eot_maineot_ofcsuppl_temp");
        $this->db->where(array("$db1.eot_maineot_ofcsuppl_temp.bd_projid" => $projId, "$db1.eot_maineot_ofcsuppl_temp.eot_main_id" => $RowId, "$db1.eot_maineot_ofcsuppl_temp.status" => "1"));
        $dataeoTOfcSupplArr = $this->db->get()->result();

        $allEOTrecOS = array();
        if ($dataeoTOfcSupplArr):
            foreach ($dataeoTOfcSupplArr as $rEcdoRe) {
                $allEOTrecOS[$rEcdoRe->assign_main_id] = $rEcdoRe->eot_mm;
            }
        endif;
        //Start Office Furniture  Records if Inserted..
        $this->db->select("$db1.eot_maineot_ofcfurniture_temp.*");
        $this->db->from("$db1.eot_maineot_ofcfurniture_temp");
        $this->db->where(array("$db1.eot_maineot_ofcfurniture_temp.bd_projid" => $projId, "$db1.eot_maineot_ofcfurniture_temp.eot_main_id" => $RowId, "$db1.eot_maineot_ofcfurniture_temp.status" => "1"));
        $dataeoTOfcFurnArr = $this->db->get()->result();

        $allEOTrecOF = array();
        if ($dataeoTOfcFurnArr):
            foreach ($dataeoTOfcFurnArr as $rEcdoRe) {
                $allEOTrecOF[$rEcdoRe->assign_main_id] = $rEcdoRe->eot_mm;
            }
        endif;
        //Close Office Furniture  Records if Inserted..
        //Start Office Equipment Records if Inserted..
        $this->db->select("$db1.eot_maineot_ofcequipment_temp.*");
        $this->db->from("$db1.eot_maineot_ofcequipment_temp");
        $this->db->where(array("$db1.eot_maineot_ofcequipment_temp.bd_projid" => $projId, "$db1.eot_maineot_ofcequipment_temp.eot_main_id" => $RowId, "$db1.eot_maineot_ofcequipment_temp.status" => "1"));
        $dataeoTOfcEquitArr = $this->db->get()->result();

        $allEOTrecOEQ = array();
        if ($dataeoTOfcEquitArr):
            foreach ($dataeoTOfcEquitArr as $rEcdoeq) {
                $allEOTrecOEQ[$rEcdoeq->assign_main_id] = $rEcdoeq->eot_mm;
            }
        endif;
        //Close Office Equipment  Records if Inserted..
        //Start Printer Report Records if Inserted..
        $this->db->select("$db1.eot_maineot_ofcprintreport_temp.*");
        $this->db->from("$db1.eot_maineot_ofcprintreport_temp");
        $this->db->where(array("$db1.eot_maineot_ofcprintreport_temp.bd_projid" => $projId, "$db1.eot_maineot_ofcprintreport_temp.eot_main_id" => $RowId, "$db1.eot_maineot_ofcprintreport_temp.status" => "1"));
        $dataeoTPrinterReportArr = $this->db->get()->result();

        $allEOTrecRep = array();
        if ($dataeoTPrinterReportArr):
            foreach ($dataeoTPrinterReportArr as $rEcrep) {
                $allEOTrecRep[$rEcrep->assign_main_id] = $rEcrep->eot_mm;
            }
        endif;
        //close Printer Report Records if Inserted..
        $this->db->select("$db1.eot_maineot_surveyequip_temp.*");
        $this->db->from("$db1.eot_maineot_surveyequip_temp");
        $this->db->where(array("$db1.eot_maineot_surveyequip_temp.bd_projid" => $projId, "$db1.eot_maineot_surveyequip_temp.eot_main_id" => $RowId, "$db1.eot_maineot_surveyequip_temp.status" => "1"));
        $dataeoTsurveyequipArr = $this->db->get()->result();
        $allEOTsequip = array();
        if ($dataeoTsurveyequipArr):
            foreach ($dataeoTsurveyequipArr as $rEceq) {
                $allEOTsequip[$rEceq->assign_main_id] = $rEceq->eot_mm;
            }
        endif;


        if (($RowId > 0) and ( $projId > 0)) {
            $data['recArr'] = $this->getSingleRecDetailsByid($RowId);
            $data["projid"] = $projId;
            $data["rowid"] = $RowId;
            $data['InsertedEOTrec'] = $allEOTrec;
            $data['InsertedEOT_transport'] = $allEOTrecT;
            $data['InsertedDuty_Travel_Site'] = $allEOTrecTR;
            $data['InsertedDuty_Office_Rent'] = $allEOTrecOR;
            $data['InsertedDuty_Ofc_Suppl'] = $allEOTrecOS;
            $data['InsertedOfc_furniture'] = $allEOTrecOF;
            $data['InsertedOfc_equipment'] = $allEOTrecOEQ;
            $data['InsertedPrinter_report'] = $allEOTrecRep;
            $data['Inserted_surveyequip'] = $allEOTsequip;
            $this->load->view('eot/edit_eot_view', $data);
        } else {
            redirect(base_url("eotcontrol/eotmanage"));
        }
    }

    
    
    
    //Get Single Row By Id..
    public function getSingleRecDetailsByid($rowid) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        // $rowid = 12;
        $this->db->select("$db2.main_employees_summary.prefix_name,$db2.main_employees_summary.userfullname,$db2.tm_projects.project_name,$db1.eot_maineot_record.eot_name,$db1.eot_maineot_record.submision_date,$db1.eot_maineot_record.eot_months,$db1.eot_maineot_record.eot_generated_no,$db1.eot_maineot_record.entry_date,$db1.eot_maineot_record.bd_proj_id");
        $this->db->from("$db1.eot_maineot_record");
        $this->db->join("$db2.tm_projects", "$db1.eot_maineot_record.ts_proj_id=$db2.tm_projects.id", "LEFT");
        $this->db->join("$db2.main_employees_summary", "$db1.eot_maineot_record.rec_entryby=$db2.main_employees_summary.user_id", "LEFT");
        $this->db->where(array("$db1.eot_maineot_record.fld_id" => $rowid, "$db1.eot_maineot_record.status" => "1"));
        $eoTrecArr = $this->db->get()->row();
        return ($eoTrecArr) ? $eoTrecArr : null;
    }

    public function eotsave_teamdata() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $bdProjId = $this->input->post('bd_projid');
        $Emain_eotRowId = $this->input->post('eot_main_id');
        //Key Profes...
        if ($_REQUEST['actkeyprof']) {
            foreach ($_REQUEST['actkeyprof'] as $rowRecK) {
                $designID = $this->input->post('keyprof_designation_' . $rowRecK);
                $emplId = $this->input->post('keyprof_emplid_' . $rowRecK);
                $eotMMKey = $this->input->post('keyprof_' . $rowRecK);
                //Inserter Array..
                $insertKey = array();
                if ($eotMMKey != ""):
                    $insertKey['bd_projid'] = $bdProjId;
                    $insertKey['eot_main_id'] = $Emain_eotRowId;
                    $insertKey['emp_id'] = $emplId;
                    $insertKey['designation_id'] = $designID;
                    $insertKey['keyid'] = "1";
                    $insertKey['eot_mm'] = $eotMMKey;
                    $insertKey['rec_curr_status'] = "new";
                    $insertKey['entry_by'] = $this->session->userdata('uid');
                    $numRecR = $this->checknumrowseotdata($bdProjId, $Emain_eotRowId, $designID);
                    //Insert..
                    if ($numRecR < 1):
                        $this->db->insert("$db1.eot_maineot_teamdata_temp", $insertKey);
                        $this->session->set_flashdata('msg', 'EOT Details Save Successfully');
                    endif;
                    //Upd..
                    if ($numRecR > 0):
                        $this->db->where(array("$db1.eot_maineot_teamdata_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_teamdata_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_teamdata_temp.designation_id" => $designID));
                        $this->db->update("$db1.eot_maineot_teamdata_temp", array("$db1.eot_maineot_teamdata_temp.eot_mm" => $eotMMKey));
                        $this->session->set_flashdata('msg', 'EOT Details Updated Successfully');
                    endif;
                endif;
            }
        }

        //Sub Proff...
        if ($_REQUEST['actsubprof']) {
            foreach ($_REQUEST['actsubprof'] as $rowRecS) {
                $designID = $this->input->post('subprof_designation_' . $rowRecS);
                $emplId = $this->input->post('subprof_emplid_' . $rowRecS);
                $eotMMKey = $this->input->post('subprof_' . $rowRecS);
                //Inserter Array..
                $insertKey = array();
                if ($eotMMKey != ""):
                    $insertKey['bd_projid'] = $bdProjId;
                    $insertKey['eot_main_id'] = $Emain_eotRowId;
                    $insertKey['emp_id'] = $emplId;
                    $insertKey['designation_id'] = $designID;
                    $insertKey['keyid'] = "2";
                    $insertKey['eot_mm'] = $eotMMKey;
                    $insertKey['rec_curr_status'] = "new";
                    $insertKey['entry_by'] = $this->session->userdata('uid');
                    $numRecR = $this->checknumrowseotdata($bdProjId, $Emain_eotRowId, $designID);
                    //Insert..
                    if ($numRecR < 1):
                        $this->db->insert("$db1.eot_maineot_teamdata_temp", $insertKey);
                        $this->session->set_flashdata('msg', 'EOT Details Save Successfully');
                    endif;
                    //Upd..
                    if ($numRecR > 0):
                        $this->db->where(array("$db1.eot_maineot_teamdata_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_teamdata_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_teamdata_temp.designation_id" => $designID));
                        $this->db->update("$db1.eot_maineot_teamdata_temp", array("$db1.eot_maineot_teamdata_temp.eot_mm" => $eotMMKey));
                        $this->session->set_flashdata('msg', 'EOT Details Updated Successfully');
                    endif;
                endif;
            }
        }
        //Admin Staff..
        if ($_REQUEST['actadminstaff']) {
            foreach ($_REQUEST['actadminstaff'] as $rowRecA) {
                $designID = $this->input->post('admin_designation_' . $rowRecA);
                $emplId = $this->input->post('admin_emplid_' . $rowRecA);
                $eotMMKey = $this->input->post('admnstaff_' . $rowRecA);
                //Inserter Array..
                $insertKey = array();
                if ($eotMMKey != ""):
                    $insertKey['bd_projid'] = $bdProjId;
                    $insertKey['eot_main_id'] = $Emain_eotRowId;
                    $insertKey['emp_id'] = $emplId;
                    $insertKey['designation_id'] = $designID;
                    $insertKey['keyid'] = "3";
                    $insertKey['eot_mm'] = $eotMMKey;
                    $insertKey['rec_curr_status'] = "new";
                    $insertKey['entry_by'] = $this->session->userdata('uid');
                    $numRecR = $this->checknumrowseotdata($bdProjId, $Emain_eotRowId, $designID);
                    //Insert..
                    if ($numRecR < 1):
                        $this->db->insert("$db1.eot_maineot_teamdata_temp", $insertKey);
                        $this->session->set_flashdata('msg', 'EOT Details Save Successfully');
                    endif;
                    //Upd..
                    if ($numRecR > 0):
                        $this->db->where(array("$db1.eot_maineot_teamdata_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_teamdata_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_teamdata_temp.designation_id" => $designID));
                        $this->db->update("$db1.eot_maineot_teamdata_temp", array("$db1.eot_maineot_teamdata_temp.eot_mm" => $eotMMKey));
                        $this->session->set_flashdata('msg', 'EOT Details Updated Successfully');
                    endif;
                endif;
            }
        }
        redirect(base_url("eotcontrol/editeotdetails?projid=" . $bdProjId . "&rowid=" . $Emain_eotRowId));
    }

    //Save Transportation...
    public function eotsave_transportdata() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $bdProjId = $this->input->post('bd_projid');
        $Emain_eotRowId = $this->input->post('eot_main_id');
        $insertKey = array();
        if ($_REQUEST['acttranspor']) {
            foreach ($_REQUEST['acttranspor'] as $rowRec) {

                $trans_assignmaster = $_REQUEST['trans_assignmaster_' . $rowRec];
                $transpEOT = $_REQUEST['transportation_' . $rowRec];

                if ($trans_assignmaster and $transpEOT):
                    $insertKey['bd_projid'] = $bdProjId;
                    $insertKey['eot_main_id'] = $Emain_eotRowId;
                    $insertKey['assign_main_id'] = $trans_assignmaster;
                    $insertKey['eot_mm'] = $transpEOT;
                    $insertKey['rec_curr_status'] = "new";
                    $insertKey['entry_by'] = $this->session->userdata('uid');
                    $numRecR = $this->checknumrow_trans_data($bdProjId, $Emain_eotRowId, $trans_assignmaster);
                    if ($numRecR < 1):
                        $this->db->insert("$db1.eot_maineot_transport_temp", $insertKey);
                        $this->session->set_flashdata('msg', 'EOT Transport Details Save Successfully');
                    endif;
                    //Upd..
                    if ($numRecR > 0):
                        $this->db->where(array("$db1.eot_maineot_transport_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_transport_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_transport_temp.assign_main_id" => $trans_assignmaster));
                        $this->db->update("$db1.eot_maineot_transport_temp", array("$db1.eot_maineot_transport_temp.eot_mm" => $transpEOT));
                        $this->session->set_flashdata('msg', 'EOT Transport Details Updated Successfully');
                    endif;
                endif;
            }
        }
        redirect(base_url("eotcontrol/editeotdetails?projid=" . $bdProjId . "&rowid=" . $Emain_eotRowId));
    }

    //Duty And Travel Site.
    public function eotsave_dutytraveldata() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $bdProjId = $this->input->post('bd_projid');
        $Emain_eotRowId = $this->input->post('eot_main_id');
        $insertKey = array();

        if ($_REQUEST['actdtsite']) {
            foreach ($_REQUEST['actdtsite'] as $rowRec) {

                $trans_assignmaster = $_REQUEST['dtsite_assignmaster_' . $rowRec];
                $transpEOT = $_REQUEST['dtsite_' . $rowRec];

                if ($trans_assignmaster and $transpEOT):

                    $insertKey['bd_projid'] = $bdProjId;
                    $insertKey['eot_main_id'] = $Emain_eotRowId;
                    $insertKey['assign_main_id'] = $trans_assignmaster;
                    $insertKey['eot_mm'] = $transpEOT;
                    $insertKey['rec_curr_status'] = "new";
                    $insertKey['entry_by'] = $this->session->userdata('uid');
                    $numRecR = $this->checknumrow_dutysite_data($bdProjId, $Emain_eotRowId, $trans_assignmaster);
                    if ($numRecR < 1):
                        $this->db->insert("$db1.eot_maineot_dutytravsite_temp", $insertKey);
                        $this->session->set_flashdata('msg', 'EOT Duty Travel to Site Details Save Successfully');
                    endif;
                    //Upd..
                    if ($numRecR > 0):
                        $this->db->where(array("$db1.eot_maineot_dutytravsite_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_dutytravsite_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_dutytravsite_temp.assign_main_id" => $trans_assignmaster));
                        $this->db->update("$db1.eot_maineot_dutytravsite_temp", array("$db1.eot_maineot_dutytravsite_temp.eot_mm" => $transpEOT));
                        $this->session->set_flashdata('msg', 'EOT Duty Travel to Site Details Updated Successfully');
                    endif;
                endif;
            }
        }
        redirect(base_url("eotcontrol/editeotdetails?projid=" . $bdProjId . "&rowid=" . $Emain_eotRowId));
    }

    //Save Edit Ofc Rent...
    public function eotsave_ofcrentdata() {

        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $bdProjId = $this->input->post('bd_projid');
        $Emain_eotRowId = $this->input->post('eot_main_id');
        $insertKey = array();

        if ($_REQUEST['actofcrent']) {
            foreach ($_REQUEST['actofcrent'] as $rowRec) {

                $trans_assignmaster = $_REQUEST['ofcrent_assignmaster_' . $rowRec];
                $transpEOT = $_REQUEST['ofcrent_' . $rowRec];

                if ($trans_assignmaster and $transpEOT):
                    $insertKey['bd_projid'] = $bdProjId;
                    $insertKey['eot_main_id'] = $Emain_eotRowId;
                    $insertKey['assign_main_id'] = $trans_assignmaster;
                    $insertKey['eot_mm'] = $transpEOT;
                    $insertKey['rec_curr_status'] = "new";
                    $insertKey['entry_by'] = $this->session->userdata('uid');
                    $numRecR = $this->checknumrow_ofcrent_data($bdProjId, $Emain_eotRowId, $trans_assignmaster);
                    if ($numRecR < 1):
                        $this->db->insert("$db1.eot_maineot_ofcrent_temp", $insertKey);
                        $this->session->set_flashdata('msg', 'EOT Office Rent Save Successfully');
                    endif;
                    //Upd..
                    if ($numRecR > 0):
                        $this->db->where(array("$db1.eot_maineot_ofcrent_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_ofcrent_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_ofcrent_temp.assign_main_id" => $trans_assignmaster));
                        $this->db->update("$db1.eot_maineot_ofcrent_temp", array("$db1.eot_maineot_ofcrent_temp.eot_mm" => $transpEOT));
                        $this->session->set_flashdata('msg', 'EOT Office Rent Details Updated Successfully');
                    endif;
                endif;
            }
        }
        redirect(base_url("eotcontrol/editeotdetails?projid=" . $bdProjId . "&rowid=" . $Emain_eotRowId));
    }

    //Save Edit insert Update .. 
    public function eotsave_ofcsuppldata() {

        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $bdProjId = $this->input->post('bd_projid');
        $Emain_eotRowId = $this->input->post('eot_main_id');
        $insertKey = array();

        if ($_REQUEST['actofcsuppl']) {
            foreach ($_REQUEST['actofcsuppl'] as $rowRec) {

                $trans_assignmaster = $_REQUEST['ofcsuppl_assignmaster_' . $rowRec];
                $transpEOT = $_REQUEST['ofcsuppl_' . $rowRec];

                if ($trans_assignmaster and $transpEOT):
                    $insertKey['bd_projid'] = $bdProjId;
                    $insertKey['eot_main_id'] = $Emain_eotRowId;
                    $insertKey['assign_main_id'] = $trans_assignmaster;
                    $insertKey['eot_mm'] = $transpEOT;
                    $insertKey['rec_curr_status'] = "new";
                    $insertKey['entry_by'] = $this->session->userdata('uid');
                    $numRecR = $this->checknumrow_ofcsuppl_data($bdProjId, $Emain_eotRowId, $trans_assignmaster);
                    if ($numRecR < 1):
                        $this->db->insert("$db1.eot_maineot_ofcsuppl_temp", $insertKey);
                        $this->session->set_flashdata('msg', 'EOT Office Supplies, Utilities and Communication Save Successfully');
                    endif;
                    //Upd..
                    if ($numRecR > 0):
                        $this->db->where(array("$db1.eot_maineot_ofcsuppl_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_ofcsuppl_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_ofcsuppl_temp.assign_main_id" => $trans_assignmaster));
                        $this->db->update("$db1.eot_maineot_ofcsuppl_temp", array("$db1.eot_maineot_ofcsuppl_temp.eot_mm" => $transpEOT));
                        $this->session->set_flashdata('msg', 'EOT Office Supplies, Utilities and Communication Details Updated Successfully');
                    endif;
                endif;
            }
        }
        redirect(base_url("eotcontrol/editeotdetails?projid=" . $bdProjId . "&rowid=" . $Emain_eotRowId));
    }

    //Ofc Furniture..
    public function eotsave_ofcfurnituredata() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $bdProjId = $this->input->post('bd_projid');
        $Emain_eotRowId = $this->input->post('eot_main_id');
        $insertKey = array();

        if ($_REQUEST['actofcfurn']) {
            foreach ($_REQUEST['actofcfurn'] as $rowRec) {

                $trans_assignmaster = $_REQUEST['ofcfurn_assignmaster_' . $rowRec];
                $transpEOT = $_REQUEST['ofcfurn_' . $rowRec];

                if ($trans_assignmaster and $transpEOT):
                    $insertKey['bd_projid'] = $bdProjId;
                    $insertKey['eot_main_id'] = $Emain_eotRowId;
                    $insertKey['assign_main_id'] = $trans_assignmaster;
                    $insertKey['eot_mm'] = $transpEOT;
                    $insertKey['rec_curr_status'] = "new";
                    $insertKey['entry_by'] = $this->session->userdata('uid');
                    $numRecR = $this->checknumrow_ofcfurniture_data($bdProjId, $Emain_eotRowId, $trans_assignmaster);
                    if ($numRecR < 1):
                        $this->db->insert("$db1.eot_maineot_ofcfurniture_temp", $insertKey);
                        $this->session->set_flashdata('msg', 'EOT Office Furniture Save Successfully');
                    endif;
                    //Upd..
                    if ($numRecR > 0):
                        $this->db->where(array("$db1.eot_maineot_ofcfurniture_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_ofcfurniture_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_ofcfurniture_temp.assign_main_id" => $trans_assignmaster));
                        $this->db->update("$db1.eot_maineot_ofcfurniture_temp", array("$db1.eot_maineot_ofcfurniture_temp.eot_mm" => $transpEOT));
                        $this->session->set_flashdata('msg', 'EOT Office Furniture Details Updated Successfully');
                    endif;
                endif;
            }
        }
        redirect(base_url("eotcontrol/editeotdetails?projid=" . $bdProjId . "&rowid=" . $Emain_eotRowId));
    }

    //Save Ofc Equipment...
    public function eotsave_ofcequipmentdata() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $bdProjId = $this->input->post('bd_projid');
        $Emain_eotRowId = $this->input->post('eot_main_id');
        $insertKey = array();

        if ($_REQUEST['actofcequip']) {
            foreach ($_REQUEST['actofcequip'] as $rowRec) {

                $trans_assignmaster = $_REQUEST['ofcequip_assignmaster_' . $rowRec];
                $transpEOT = $_REQUEST['ofcequip_' . $rowRec];

                if ($trans_assignmaster and $transpEOT):
                    $insertKey['bd_projid'] = $bdProjId;
                    $insertKey['eot_main_id'] = $Emain_eotRowId;
                    $insertKey['assign_main_id'] = $trans_assignmaster;
                    $insertKey['eot_mm'] = $transpEOT;
                    $insertKey['rec_curr_status'] = "new";
                    $insertKey['entry_by'] = $this->session->userdata('uid');
                    $numRecR = $this->checknumrow_ofcequipment_data($bdProjId, $Emain_eotRowId, $trans_assignmaster);
                    if ($numRecR < 1):
                        $this->db->insert("$db1.eot_maineot_ofcequipment_temp", $insertKey);
                        $this->session->set_flashdata('msg', 'EOT Office Equipment Save Successfully');
                    endif;
                    //Upd..
                    if ($numRecR > 0):
                        $this->db->where(array("$db1.eot_maineot_ofcequipment_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_ofcequipment_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_ofcequipment_temp.assign_main_id" => $trans_assignmaster));
                        $this->db->update("$db1.eot_maineot_ofcequipment_temp", array("$db1.eot_maineot_ofcequipment_temp.eot_mm" => $transpEOT));
                        $this->session->set_flashdata('msg', 'EOT Office Equipment Details Updated Successfully');
                    endif;
                endif;
            }
        }
        redirect(base_url("eotcontrol/editeotdetails?projid=" . $bdProjId . "&rowid=" . $Emain_eotRowId));
    }

    public function eotsave_ofc_print_docsreport() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $bdProjId = $this->input->post('bd_projid');
        $Emain_eotRowId = $this->input->post('eot_main_id');
        $insertKey = array();

        if ($_REQUEST['actreport']) {
            foreach ($_REQUEST['actreport'] as $rowRec) {

                $trans_assignmaster = $_REQUEST['report_assignmaster_' . $rowRec];
                $transpEOT = $_REQUEST['report_' . $rowRec];

                if ($trans_assignmaster and $transpEOT):
                    $insertKey['bd_projid'] = $bdProjId;
                    $insertKey['eot_main_id'] = $Emain_eotRowId;
                    $insertKey['assign_main_id'] = $trans_assignmaster;
                    $insertKey['eot_mm'] = $transpEOT;
                    $insertKey['rec_curr_status'] = "new";
                    $insertKey['entry_by'] = $this->session->userdata('uid');
                    $numRecR = $this->checknumrow_ofcreport_data($bdProjId, $Emain_eotRowId, $trans_assignmaster);
                    if ($numRecR < 1):
                        $this->db->insert("$db1.eot_maineot_ofcprintreport_temp", $insertKey);
                        $this->session->set_flashdata('msg', 'EOT Office Report Save Successfully');
                    endif;
                    //Upd..
                    if ($numRecR > 0):
                        $this->db->where(array("$db1.eot_maineot_ofcprintreport_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_ofcprintreport_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_ofcprintreport_temp.assign_main_id" => $trans_assignmaster));
                        $this->db->update("$db1.eot_maineot_ofcprintreport_temp", array("$db1.eot_maineot_ofcprintreport_temp.eot_mm" => $transpEOT));
                        $this->session->set_flashdata('msg', 'EOT Office Report Details Updated Successfully');
                    endif;
                endif;
            }
        }
        redirect(base_url("eotcontrol/editeotdetails?projid=" . $bdProjId . "&rowid=" . $Emain_eotRowId));
    }

    public function eotsave_surveyequipment() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $bdProjId = $this->input->post('bd_projid');
        $Emain_eotRowId = $this->input->post('eot_main_id');
        $insertKey = array();
        if ($_REQUEST['sequip']) {
            foreach ($_REQUEST['sequip'] as $rowRec) {
                $trans_assignmaster = $_REQUEST['sequip_assignmaster_' . $rowRec];
                $transpEOT = $_REQUEST['sequip_' . $rowRec];
                if ($trans_assignmaster and $transpEOT):
                    $insertKey['bd_projid'] = $bdProjId;
                    $insertKey['eot_main_id'] = $Emain_eotRowId;
                    $insertKey['assign_main_id'] = $trans_assignmaster;
                    $insertKey['eot_mm'] = $transpEOT;
                    $insertKey['rec_curr_status'] = "new";
                    $insertKey['entry_by'] = $this->session->userdata('uid');
                    $numRecR = $this->checknumrow_surveyequip_data($bdProjId, $Emain_eotRowId, $trans_assignmaster);
                    if ($numRecR < 1):
                        $this->db->insert("$db1.eot_maineot_surveyequip_temp", $insertKey);
                        $this->session->set_flashdata('msg', 'EOT Office Report Save Successfully');
                    endif;
                    //Upd..
                    if ($numRecR > 0):
                        $this->db->where(array("$db1.eot_maineot_surveyequip_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_surveyequip_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_surveyequip_temp.assign_main_id" => $trans_assignmaster));
                        $this->db->update("$db1.eot_maineot_surveyequip_temp", array("$db1.eot_maineot_ofcprintreport_temp.eot_mm" => $transpEOT));
                        $this->session->set_flashdata('msg', 'EOT Office Report Details Updated Successfully');
                    endif;
                endif;
            }
        }
        redirect(base_url("eotcontrol/editeotdetails?projid=" . $bdProjId . "&rowid=" . $Emain_eotRowId));
    }

    //Check Num Rows Before insert..
    public function checknumrowseotdata($bd_projid, $eot_main_id, $designation_id) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.eot_maineot_teamdata_temp.fld_id");
        $this->db->from("$db1.eot_maineot_teamdata_temp");
        $this->db->where(array("$db1.eot_maineot_teamdata_temp.status" => "1", "$db1.eot_maineot_teamdata_temp.bd_projid" => $bd_projid, "$db1.eot_maineot_teamdata_temp.eot_main_id" => $eot_main_id, "$db1.eot_maineot_teamdata_temp.designation_id" => $designation_id));
        $numRows = $this->db->get()->num_rows();
        return ($numRows) ? $numRows : null;
    }

    public function checknumrow_trans_data($bdProjId, $Emain_eotRowId, $trans_assignmaster) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.eot_maineot_transport_temp.fld_id");
        $this->db->from("$db1.eot_maineot_transport_temp");
        $this->db->where(array("$db1.eot_maineot_transport_temp.status" => "1", "$db1.eot_maineot_transport_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_transport_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_transport_temp.assign_main_id" => $trans_assignmaster));
        $numRows = $this->db->get()->num_rows();
        return ($numRows) ? $numRows : null;
    }

    public function checknumrow_dutysite_data($bdProjId, $Emain_eotRowId, $trans_assignmaster) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.eot_maineot_dutytravsite_temp.fld_id");
        $this->db->from("$db1.eot_maineot_dutytravsite_temp");
        $this->db->where(array("$db1.eot_maineot_dutytravsite_temp.status" => "1", "$db1.eot_maineot_dutytravsite_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_dutytravsite_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_dutytravsite_temp.assign_main_id" => $trans_assignmaster));
        $numRows = $this->db->get()->num_rows();
        return ($numRows) ? $numRows : null;
    }

    public function checknumrow_ofcrent_data($bdProjId, $Emain_eotRowId, $trans_assignmaster) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.eot_maineot_ofcrent_temp.fld_id");
        $this->db->from("$db1.eot_maineot_ofcrent_temp");
        $this->db->where(array("$db1.eot_maineot_ofcrent_temp.status" => "1", "$db1.eot_maineot_ofcrent_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_ofcrent_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_ofcrent_temp.assign_main_id" => $trans_assignmaster));
        $numRows = $this->db->get()->num_rows();
        return ($numRows) ? $numRows : null;
    }

    public function checknumrow_ofcsuppl_data($bdProjId, $Emain_eotRowId, $trans_assignmaster) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.eot_maineot_ofcsuppl_temp.fld_id");
        $this->db->from("$db1.eot_maineot_ofcsuppl_temp");
        $this->db->where(array("$db1.eot_maineot_ofcsuppl_temp.status" => "1", "$db1.eot_maineot_ofcsuppl_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_ofcsuppl_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_ofcsuppl_temp.assign_main_id" => $trans_assignmaster));
        $numRows = $this->db->get()->num_rows();
        return ($numRows) ? $numRows : null;
    }

    public function checknumrow_ofcfurniture_data($bdProjId, $Emain_eotRowId, $trans_assignmaster) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.eot_maineot_ofcfurniture_temp.fld_id");
        $this->db->from("$db1.eot_maineot_ofcfurniture_temp");
        $this->db->where(array("$db1.eot_maineot_ofcfurniture_temp.status" => "1", "$db1.eot_maineot_ofcfurniture_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_ofcfurniture_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_ofcfurniture_temp.assign_main_id" => $trans_assignmaster));
        $numRows = $this->db->get()->num_rows();
        return ($numRows) ? $numRows : null;
    }

    public function checknumrow_ofcequipment_data($bdProjId, $Emain_eotRowId, $trans_assignmaster) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.eot_maineot_ofcequipment_temp.fld_id");
        $this->db->from("$db1.eot_maineot_ofcequipment_temp");
        $this->db->where(array("$db1.eot_maineot_ofcequipment_temp.status" => "1", "$db1.eot_maineot_ofcequipment_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_ofcequipment_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_ofcequipment_temp.assign_main_id" => $trans_assignmaster));
        $numRows = $this->db->get()->num_rows();
        return ($numRows) ? $numRows : null;
    }

    public function checknumrow_ofcreport_data($bdProjId, $Emain_eotRowId, $trans_assignmaster) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.eot_maineot_ofcprintreport_temp.fld_id");
        $this->db->from("$db1.eot_maineot_ofcprintreport_temp");
        $this->db->where(array("$db1.eot_maineot_ofcprintreport_temp.status" => "1", "$db1.eot_maineot_ofcprintreport_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_ofcprintreport_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_ofcprintreport_temp.assign_main_id" => $trans_assignmaster));
        $numRows = $this->db->get()->num_rows();
        return ($numRows) ? $numRows : null;
    }

    public function checknumrow_surveyequip_data($bdProjId, $Emain_eotRowId, $trans_assignmaster) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.eot_maineot_surveyequip_temp.fld_id");
        $this->db->from("$db1.eot_maineot_surveyequip_temp");
        $this->db->where(array("$db1.eot_maineot_surveyequip_temp.status" => "1", "$db1.eot_maineot_surveyequip_temp.bd_projid" => $bdProjId, "$db1.eot_maineot_surveyequip_temp.eot_main_id" => $Emain_eotRowId, "$db1.eot_maineot_surveyequip_temp.assign_main_id" => $trans_assignmaster));
        $numRows = $this->db->get()->num_rows();
        return ($numRows) ? $numRows : null;
    }

    #####################################################################################################
    #************************* OLD CODE ****************************************************************#
    #+++++++++++++++++++++++++++++++++++ Asheesh +++++++++++++++++++ 24-07-2019 ++++++++++++++++++++++++#
    #####################################################################################################
    #
    #
    //Ajax For Data Table Durgesh...

    public function Ajax_GetRecord_vacant_project() {
        $list = $this->Projectvacantposition_model->get_datatables();
        $loginId = $this->session->userdata('uid');
        $links1 = '';
        $links2 = '';
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $val) {
            $no++;
            $StsTss = "<span style='color:red'> Insearch </span>";
            if (($val->curr_status) and ( $val->curr_status == "Pending")) {
                $StsTss = "<span style='color:#6f1e1e'> Sent to Coordinator </span>";
            }
            if (($val->curr_status) and ( $val->curr_status == "Approved")) {
                $StsTss = "<span style='color:green'> Approved By Coordinator </span>";
            }
            if (($val->curr_status) and ( $val->curr_status == "Approved") and ( $val->step_by_cru == "1")) {
                $StsTss = "<span style='color:green'> Cv Sent To Client </span>";
            }
            if (($val->curr_status) and ( $val->curr_status == "Approved") and ( $val->step_by_cru == "2")) {
                $StsTss = "<span style='color:green'> Approved by Client </span>";
            }
            if (($val->curr_status) and ( $val->curr_status == "Approved") and ( $val->step_by_cru == "3")) {
                $StsTss = "<span style='color:green'> Progress in Fianl Approval </span>";
            }
            if (($val->curr_status) and ( $val->curr_status == "Approved") and ( $val->step_by_cru == "3") and ( $val->final_cvappr_yesorno == "yes")) {
                $StsTss = "<span style='color:green'> Approved & Mobilized </span>";
            }
            if (( $val->client_cvappr_yesorno == "no") or ( $val->final_cvappr_yesorno == "no")) {
                $StsTss = "<span style='color:red'> Rejected </span>";
            }
            $row = array();
            $row[] = $no;
            $row[] = $val->project_name;
            $row[] = $val->designation_name;
            $row[] = '<span style="color:green;">' . date('Y-m-d', strtotime($val->last_date_resign)) . '</span>';
            $check = $this->db->get_where('bdcruvacant_crupanel', array('project_id' => $val->project_id, 'designation_id' => $val->designation_id))->row_array();
            $staus = $this->db->get_where('bdcruvacant_status', array('status' => '1'))->result_array();
            $row[] = $StsTss;
            $deptArr = $this->GetAllCruDeptUserIdArr();
            if (in_array($loginId, $deptArr)) {
                $links1 = "<a href='" . base_url('crutraker/edit_crutraker_board_project/' . $val->project_id . '/' . $val->designation_id) . "'><i style='color:red;' title='CRU' class='glyphicon glyphicon-edit icon-red'></i></a>";
            }
            if ($loginId == "271") {
                $links2 = "<a href='" . base_url('crutraker/project_coordinator/' . $val->project_id . '/' . $val->designation_id) . "'><i style='color:green;' title='Cordinator' class='glyphicon glyphicon-user icon-green'></i></a>";
            }
            if ($loginId == "297" or $loginId == "296" or $loginId == "190") {
                $links1 = "<a href='" . base_url('crutraker/edit_crutraker_board_project/' . $val->project_id . '/' . $val->designation_id) . "'><i style='color:red;' title='CRU' class='glyphicon glyphicon-edit icon-red'></i></a>";
                $links2 = "<a href='" . base_url('crutraker/project_coordinator/' . $val->project_id . '/' . $val->designation_id) . "'><i style='color:green;' title='Cordinator' class='glyphicon glyphicon-user icon-green'></i></a>";
            }
//            if ($loginId == "190") {
//                $links1 = "<a href='" . base_url('crutraker/edit_crutraker_board_project/' . $val->project_id . '/' . $val->designation_id) . "'><i style='color:red;' title='CRU' class='glyphicon glyphicon-edit icon-red'></i></a>";
//                $links2 = "<a href='" . base_url('crutraker/project_coordinator/' . $val->project_id . '/' . $val->designation_id) . "'><i style='color:green;' title='Cordinator' class='glyphicon glyphicon-user icon-green'></i></a>";
//            }
            $row[] = $links1 . "&nbsp;&nbsp" . $links2;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Projectvacantposition_model->count_all(),
            "recordsTotal" => '0',
            "recordsFiltered" => $this->Projectvacantposition_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function datediffgap($date1) {
        $date2 = date("Y-m-d");
        $diff = abs(strtotime($date2) - strtotime($date1));
        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
        $dumArr = array("0" => $years, "1" => $months, "2" => $days);
        return $dumArr;
    }

    //Upload Config By Durgesh..
    protected function getUploadConfigcomp() {
        $config = array();
        $config['upload_path'] = './frontendassets/cv/uploads/';
        $config['allowed_types'] = 'pdf|doc|DOC|docx|PDF';
        $config['max_size'] = '200000'; //20MB
        $config['file_name'] = "Cv_" . time();
        return $config;
    }

    //Get Employee Details By ID.
    public function get_inhouse_userdetail_byid() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($_REQUEST['inhouseuserids']) {
            $this->db->select("$db2.main_employees_summary.contactnumber,$db2.main_employees_summary.emailaddress");
            $this->db->from("$db2.main_employees_summary");
            $this->db->where(array("$db2.main_employees_summary.user_id" => $_REQUEST['inhouseuserids']));
            $UserRecResult = $this->db->get()->row();
        }
        echo json_encode($UserRecResult);
    }

    public function get_other_userdetail_byid() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($_REQUEST['otheruserids']) {
            $this->db->select("$db1.team_req_otheremp.*");
            $this->db->from("$db1.team_req_otheremp");
            $this->db->where(array("$db1.team_req_otheremp.fld_id" => $_REQUEST['otheruserids'], "$db1.team_req_otheremp.status" => "1"));
            $OtherUserRecResult = $this->db->get()->row();
        }
        echo json_encode($OtherUserRecResult);
    }

    //Code by durgesh Mail Library
    function sendMail($to, $subject, $msgDetails) {
        //return true;
        $CI = & get_instance();
        $CI->load->library('email');
        $CI->email->set_mailtype("html");
        $CI->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.cegindia.com',
            'smtp_user' => 'marketing@cegindia.com',
            'smtp_pass' => 'MARK-2015ceg',
            'smtp_port' => 587,
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        ));
        $CI->email->from('hrms.admin@cegindia.com', 'Do_not_reply');
        $CI->email->to($to);
        //marketing@cegindia.com
        $CI->email->bcc('durgeshgarg32@gmail.com');
        $CI->email->subject($subject);
        $CI->email->message($msgDetails);
        $resp = $CI->email->send();
        return ($resp) ? $resp : $CI->email->print_debugger();
    }

    //Code by durgesh for client
    public function project_client() {
        $projid = $this->uri->segment(3);
        $desiid = $this->uri->segment(4);
        $data['error'] = '';
        $data['title'] = 'Edit Project Client Tracker Board Project';
        $data['client_record'] = $this->db->get_where('bdcruvacant_crupanel', array('project_id' => $projid, 'designation_id' => $desiid))->row_array();
        $data['cv_sent'] = $this->db->get_where('bdcruvacant_crupanel', array('project_id' => $projid, 'designation_id' => $desiid))->row_array();
        $data['desig_detail'] = $this->Crutrakermodel->designation_name($desiid);
        $data['coor_details'] = $this->Crutrakermodel->Get_project_name_or_code_designation_record($projid);
        $data['project_cru_rport'] = $this->db->get_where('bdcruvacant_crupanel', array('project_id' => $projid, 'designation_id' => $desiid, 'status' => 'Approved'))->result();
        $this->load->view('cru_traker/edit_clienttraker_board_project', $data);
    }

    public function project_coordinator_approve_reject() {
        $projid = $this->uri->segment(3);
        $desiid = $this->uri->segment(4);
        $Arr = $_REQUEST;
        if ($Arr) {
            $updateArr = array('status' => $Arr['coord_action'], 'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('project_id' => $projid, 'designation_id' => $desiid, 'name' => $Arr['emp_name']));
            $response = $this->db->update('bdcruvacant_crupanel', $updateArr);
        }
        if ($response):
            $this->session->set_flashdata('msg', 'Record Updated successfully');
        endif;
        redirect(base_url('crutraker/project_coordinator/' . $projid . '/' . $desiid));
    }

    //code by durgesh.....
    public function interaction_date() {
        $projid = $this->uri->segment(3);
        $desiid = $this->uri->segment(4);
        $Arr = $_REQUEST;
        if ($Arr) {
            $updateArr = array('interaction_date' => $Arr['interaction_date'], 'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('project_id' => $projid, 'designation_id' => $desiid));
            $response = $this->db->update('bdcruvacant_crupanel', $updateArr);
        }
        if ($response):
            $this->session->set_flashdata('msg', 'Record Updated successfully');
        endif;
        redirect($_SERVER['HTTP_REFERER']);
    }

    //code by durgesh.....
    public function mobilization_date() {
        $projid = $this->uri->segment(3);
        $desiid = $this->uri->segment(4);
        $Arr = $_REQUEST;
        if ($Arr) {
            $updateArr = array('mobilization_date' => $Arr['mobilization_date'], 'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('project_id' => $projid, 'designation_id' => $desiid));
            $response = $this->db->update('bdcruvacant_crupanel', $updateArr);
        }
        if ($response):
            $this->session->set_flashdata('msg', 'Record Updated successfully');
        endif;
        redirect($_SERVER['HTTP_REFERER']);
    }

    protected function getUploadConfig($folder) {
        $config = array();
        $config['upload_path'] = $folder;
        $config['max_size'] = '7900'; //7MB
        $config['allowed_types'] = 'PDF|pdf|doc|DOC';
        $config['file_name'] = "resume_" . time();
        return $config;
    }

    //Master of Add New Record...
    public function addnew_otherempl() {
        $Formrec = $this->input->post();
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.team_req_otheremp.fld_id");
        $this->db->from("$db1.team_req_otheremp");
        $this->db->where("($db1.team_req_otheremp.emp_email='" . $Formrec['emp_email'] . "' OR $db1.team_req_otheremp.emp_contact='" . $Formrec['emp_contact'] . "')", NULL, FALSE);
        $numRowChk = $this->db->get()->num_rows();

        if ($numRowChk > 0) {
            $this->session->set_flashdata('error_msg', 'Error : This Employee Record already exists.');
        } else {
            $inserTerArr = array("$db1.team_req_otheremp.emp_name" => $Formrec['emp_name'], "$db1.team_req_otheremp.emp_email" => $Formrec['emp_email'], "$db1.team_req_otheremp.emp_contact" => $Formrec['emp_contact'], "$db1.team_req_otheremp.details" => $Formrec['details'], "$db1.team_req_otheremp.entry_by" => $this->session->userdata('uid'));
            $reTresp = $this->db->insert("$db1.team_req_otheremp", $inserTerArr);
            $this->session->set_flashdata('msg', 'Details submited successfully');
        }
        redirect(base_url('crutraker/edit_crutraker_board_project/' . $Formrec['bdprojid'] . '/' . $Formrec['designation_id']));
    }

    //Code by durgesh project coordinator
    public function project_coordinator() {
        $projid = $this->uri->segment(3);
        $desiid = $this->uri->segment(4);
        if ($projid == "" OR $desiid == "") {
            redirect(base_url('project_vacant'));
        }
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $data['error'] = '';
        $data['title'] = 'Edit Cv Tracker Board Project';
        $data['desig_detail'] = $this->Crutrakermodel->designation_name($desiid);
        $data['tendor_detail'] = $this->Crutrakermodel->Get_project_name_or_code_designation_record($projid);
        $data['user_detail'] = $this->Crutrakermodel->GetUserList();
        //Get All Other Employee..
        $this->db->select("$db1.team_req_otheremp.*");
        $this->db->from("$db1.team_req_otheremp");
        $this->db->where(array("$db1.team_req_otheremp.status" => "1"));
        $data['otherEmprec'] = $this->db->get()->result();
        //After Entry Submited Employee Details Fetch .. Projid and Designation Wise..
        $this->db->select("$db2.main_users.userfullname as cruentryby,$db1.bdcruvacant_crupanel.*,$db2.main_employees_summary.userfullname as userfullname_ihr,$db1.team_req_otheremp.emp_name,$db1.team_req_otheremp.emp_email,$db1.team_req_otheremp.emp_contact,$db2.main_employees_summary.emailaddress as emailaddress_ihr,$db2.main_employees_summary.contactnumber as contactnumber_ihr");
        $this->db->from("$db1.bdcruvacant_crupanel");
        $this->db->join("$db2.main_employees_summary", "$db1.bdcruvacant_crupanel.user_id=$db2.main_employees_summary.user_id", "LEFT");
        $this->db->join("$db1.team_req_otheremp", "$db1.bdcruvacant_crupanel.user_id=$db1.team_req_otheremp.fld_id", "LEFT");
        $this->db->join("$db2.main_users", "$db1.bdcruvacant_crupanel.entry_by=$db2.main_users.id", "LEFT");
        $this->db->where(array("$db1.bdcruvacant_crupanel.status" => "1", "$db1.bdcruvacant_crupanel.project_id" => $projid, "$db1.bdcruvacant_crupanel.designation_id" => $desiid));
        $data['EntryByCruVacantEmpRec'] = $this->db->get()->result();
        $data['TmLineProgressRec'] = $this->GetTimeLineProgress($projid, $desiid);
        if ($data['desig_detail'] and ( $data['desig_detail']['last_date_resign'])) {
            //print_r($data['desig_detail']['last_date_resign']);
            $data['desig_detail']['totvacant_age'] = $this->datediffgap($data['desig_detail']['last_date_resign']);
        }
        $this->load->view('cru_traker/edit_coordinatortraker_board_project', $data);
    }

    //Step Submited Step 1...
    public function rowaction_first() {
        $recFormData = $this->input->post();
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($recFormData) {
            $UpDdata = array("$db1.bdcruvacant_crupanel.step_by_cru" => "1", "$db1.bdcruvacant_crupanel.cv_sent_to_client_by" => $this->session->userdata('uid'), "$db1.bdcruvacant_crupanel.cv_sent_to_client" => date("Y-m-d", strtotime($recFormData['cv_send_toclient_date'])));
            $this->db->where("$db1.bdcruvacant_crupanel.id", $recFormData['rowaction_1']);
            $reps = $this->db->update("$db1.bdcruvacant_crupanel", $UpDdata);
            $inserHistory = array("$db1.cruvacant_crupanel_step_history.crupanel_tblid" => $recFormData['rowaction_1'], "$db1.cruvacant_crupanel_step_history.action_name" => "Cv Send To Client", "$db1.cruvacant_crupanel_step_history.action_by" => $this->session->userdata('uid'), "$db1.cruvacant_crupanel_step_history.date_on_actv" => date("Y-m-d", strtotime($recFormData['cv_send_toclient_date'])));
            $reps = $this->db->insert("$db1.cruvacant_crupanel_step_history", $inserHistory);
            $this->session->set_flashdata('msg', 'Step 1 Cv Sent To Client successfully');
        }
        redirect(base_url("crutraker/edit_crutraker_board_project/" . $recFormData['bdprojid'] . "/" . $recFormData['designation_id']));
    }

    //Step Submitted Step 2...
    public function rowaction_second() {
        $recFormData = $this->input->post();
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($recFormData) {
            $UpDdata = array("$db1.bdcruvacant_crupanel.interaction_date" => ($recFormData['interaction_date']) ? date("Y-m-d", strtotime($recFormData['interaction_date'])) : NULL, "$db1.bdcruvacant_crupanel.step_by_cru" => "2", "$db1.bdcruvacant_crupanel.client_cvappr_yesorno" => $recFormData['client_cvappr_yesorno'], "$db1.bdcruvacant_crupanel.client_cvappr_entryby" => $this->session->userdata('uid'));
            $this->db->where("$db1.bdcruvacant_crupanel.id", $recFormData['rowaction_2']);
            $reps = $this->db->update("$db1.bdcruvacant_crupanel", $UpDdata);
            $YesOrN = $recFormData['client_cvappr_yesorno'];
            $inserHistory = array("$db1.cruvacant_crupanel_step_history.crupanel_tblid" => $recFormData['rowaction_2'], "$db1.cruvacant_crupanel_step_history.action_name" => "Cv Approve By Client $YesOrN And Interaction Date", "$db1.cruvacant_crupanel_step_history.action_by" => $this->session->userdata('uid'), "$db1.cruvacant_crupanel_step_history.date_on_actv" => ($recFormData['interaction_date']) ? date("Y-m-d", strtotime($recFormData['interaction_date'])) : NULL);
            $reps = $this->db->insert("$db1.cruvacant_crupanel_step_history", $inserHistory);
            $this->session->set_flashdata('msg', 'Step 2 Cv Approve By Client Status Set Successfully');
        }
        redirect(base_url("crutraker/edit_crutraker_board_project/" . $recFormData['bdprojid'] . "/" . $recFormData['designation_id']));
    }

    //Save Step 3 ... Final Step...
    public function rowaction_thirdfinal() {
        $recFormData = $this->input->post();
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($recFormData) {
            $UpDdata = array("$db1.bdcruvacant_crupanel.final_cvappr_date" => ($recFormData['final_cvappr_date']) ? date("Y-m-d", strtotime($recFormData['final_cvappr_date'])) : NULL, "$db1.bdcruvacant_crupanel.mobilization_date" => ($recFormData['mobilization_date']) ? date("Y-m-d", strtotime($recFormData['mobilization_date'])) : NULL, "$db1.bdcruvacant_crupanel.step_by_cru" => "3", "$db1.bdcruvacant_crupanel.final_cvappr_yesorno" => $recFormData['final_cvappr_yesorno']);
            $this->db->where("$db1.bdcruvacant_crupanel.id", $recFormData['rowaction_3']);
            $reps = $this->db->update("$db1.bdcruvacant_crupanel", $UpDdata);
            $YesOrN = $recFormData['final_cvappr_yesorno'];
            $inserHistory = array("$db1.cruvacant_crupanel_step_history.crupanel_tblid" => $recFormData['rowaction_3'], "$db1.cruvacant_crupanel_step_history.action_name" => "Final Approval is : $YesOrN ", "$db1.cruvacant_crupanel_step_history.action_by" => $this->session->userdata('uid'), "$db1.cruvacant_crupanel_step_history.date_on_actv" => ($recFormData['final_cvappr_date']) ? date("Y-m-d", strtotime($recFormData['final_cvappr_date'])) : NULL);
            $reps = $this->db->insert("$db1.cruvacant_crupanel_step_history", $inserHistory);
            $this->session->set_flashdata('msg', 'Final Approval Step Set Successfully');
        }
        redirect(base_url("crutraker/edit_crutraker_board_project/" . $recFormData['bdprojid'] . "/" . $recFormData['designation_id']));
    }

    //Get All CRU Dept UserIDs...
    public function GetAllCruDeptUserIdArr() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.main_employees_summary.user_id");
        $this->db->from("$db2.main_employees_summary");
        $this->db->where(array("$db2.main_employees_summary.isactive" => "1", "$db2.main_employees_summary.department_id" => "14"));
        $recDeptArr = $this->db->get()->result();
        $reTarr = array();
        if ($recDeptArr):
            foreach ($recDeptArr as $rec) {
                $reTarr[] = $rec->user_id;
            }
        endif;
        return $reTarr;
    }

    //Code by durgesh edit cro board project
    public function edit_crutraker_board_project() {
        $projid = $this->uri->segment(3);
        $desiid = $this->uri->segment(4);
        if ($projid == "" OR $desiid == "") {
            redirect(base_url('project_vacant'));
        }
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $data['error'] = '';
        $data['title'] = 'Edit CV Tracker Board Project';
        $data['desig_detail'] = $this->Crutrakermodel->designation_name($desiid);
        $data['tendor_detail'] = $this->Crutrakermodel->Get_project_name_or_code_designation_record($projid);
        $data['user_detail'] = $this->Crutrakermodel->GetUserList();
        //Get All Other Employee..
        $this->db->select("$db1.team_req_otheremp.*");
        $this->db->from("$db1.team_req_otheremp");
        $this->db->where(array("$db1.team_req_otheremp.status" => "1"));
        $data['otherEmprec'] = $this->db->get()->result();

        //After Entry Submited Employee Details Fetch .. Projid and Designation Wise..
        $this->db->select("$db1.bdcruvacant_crupanel.*,$db2.main_employees_summary.userfullname as userfullname_ihr,$db1.team_req_otheremp.emp_name,$db1.team_req_otheremp.emp_email,$db1.team_req_otheremp.emp_contact,$db2.main_employees_summary.emailaddress as emailaddress_ihr,$db2.main_employees_summary.contactnumber as contactnumber_ihr");
        $this->db->from("$db1.bdcruvacant_crupanel");
        $this->db->join("$db2.main_employees_summary", "$db1.bdcruvacant_crupanel.user_id=$db2.main_employees_summary.user_id", "LEFT");
        $this->db->join("$db1.team_req_otheremp", "$db1.bdcruvacant_crupanel.user_id=$db1.team_req_otheremp.fld_id", "LEFT");
        $this->db->where(array("$db1.bdcruvacant_crupanel.status" => "1", "$db1.bdcruvacant_crupanel.project_id" => $projid, "$db1.bdcruvacant_crupanel.designation_id" => $desiid));

        // $this->db->where("$db1.bdcruvacant_crupanel.curr_status!=", "Rejected", NULL, FALSE);
        // $this->db->where("$db1.bdcruvacant_crupanel.client_cvappr_yesorno!=", "no", NULL, FALSE);
        // $this->db->where("$db1.bdcruvacant_crupanel.final_cvappr_yesorno!=", "no", NULL, FALSE);

        $data['EntryByCruVacantEmpRec'] = $this->db->get()->result();

        if ($data['desig_detail'] and ( $data['desig_detail']['last_date_resign'])) {
            //print_r($data['desig_detail']['last_date_resign']);
            $data['desig_detail']['totvacant_age'] = $this->datediffgap($data['desig_detail']['last_date_resign']);
        }
        //Time Line Progress ...
        $data['TmLineProgressRec'] = $this->GetTimeLineProgress($projid, $desiid);
        $this->load->view('cru_traker/edit_crutraker_board_project', $data);
    }

    // Time Line Progress
    public function GetTimeLineProgress($projId, $DesignationId) {
        // $projId = "89246";
        // $DesignationId = "6026";
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.bdcruvacant_crupanel.project_id,$db1.bdcruvacant_crupanel.designation_id,$db1.bdcruvacant_crupanel.id,$db1.bdcruvacant_crupanel.cv_upload,$db1.bdcruvacant_crupanel.entry_date,$db2.main_employees_summary.userfullname as entryby,$db2.coord.userfullname as coord_userfullname,$db1.bdcruvacant_crupanel.approval_status_chng_by,$db1.bdcruvacant_crupanel.approval_status_chng_date,$db1.bdcruvacant_crupanel.curr_status,$db1.bdcruvacant_crupanel.step_by_cru,$db1.bdcruvacant_crupanel.cv_sent_to_client,$db2.cvsentclientby.userfullname as cvsentclientby_userfullname,$db2.clntcvappryesornoentryby.userfullname as clntcvappryesornoentryby_userfullname,$db1.bdcruvacant_crupanel.client_cvappr_yesorno,$db1.bdcruvacant_crupanel.interaction_date,$db1.bdcruvacant_crupanel.final_cvappr_yesorno,$db1.bdcruvacant_crupanel.mobilization_date");
        $this->db->from("$db1.bdcruvacant_crupanel");
        $this->db->join("$db2.main_employees_summary", "$db1.bdcruvacant_crupanel.entry_by=$db2.main_employees_summary.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as coord", "$db1.bdcruvacant_crupanel.approval_status_chng_by=$db2.coord.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as cvsentclientby", "$db1.bdcruvacant_crupanel.cv_sent_to_client_by=$db2.cvsentclientby.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as clntcvappryesornoentryby", "$db1.bdcruvacant_crupanel.client_cvappr_entryby=$db2.clntcvappryesornoentryby.user_id", "LEFT");
        $this->db->where(array("$db1.bdcruvacant_crupanel.status" => "1", "$db1.bdcruvacant_crupanel.project_id" => $projId, "$db1.bdcruvacant_crupanel.designation_id" => $DesignationId));
        $this->db->where("$db1.bdcruvacant_crupanel.curr_status!=", "Rejected", NULL, FALSE);
        $this->db->where("$db1.bdcruvacant_crupanel.client_cvappr_yesorno!=", "no", NULL, FALSE);
        $this->db->where("$db1.bdcruvacant_crupanel.final_cvappr_yesorno!=", "no", NULL, FALSE);

        $recDeptArr = $this->db->get()->row();
        return ($recDeptArr) ? $recDeptArr : null;
    }

    // Home Page of CRU vacant Possition Data table..
    public function project_vacant() {
        $data['error'] = '';
        $data['title'] = 'CRU Vacant';
        $data['hrmsProjListArr'] = $this->GetAllProjectArr();
        $data['vacantTypeListArr'] = $this->GetAllVacantTypeMasterArr();
        $this->load->view("cru_traker/cruvacant_homepage_view", $data);
    }

    //Edit Status By Harish..
    public function edit_vacantrec($entryRec) {
        $data['error'] = "";
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->SELECT("$db2.main_users.userfullname,$db1.invc_vacant_resign_date.*,$db2.tm_projects.project_name,$db1.designation_master_requisition.designation_name");
        $this->db->FROM("$db1.invc_vacant_resign_date");
        $this->db->join("$db1.accountinfo", "$db1.accountinfo.project_id = $db1.invc_vacant_resign_date.project_id", 'INNER');
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id=$db1.accountinfo.project_numberid", 'LEFT');
        $this->db->join("$db1.designation_master_requisition", "$db1.designation_master_requisition.fld_id=$db1.invc_vacant_resign_date.designation_id", 'LEFT');
        $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.invc_vacant_resign_date.emp_id", "LEFT");
        $this->db->WHERE(array("$db1.invc_vacant_resign_date.fld_id" => $entryRec, "$db1.invc_vacant_resign_date.status" => '1'));
        $data['ResignArr'] = $this->db->get()->row();
        $data['allvacantTypeArr'] = $this->GetAllVacantTypeMasterArr();
        $data['actionid'] = $entryRec;
        $this->load->view("cru_traker/resign_data_statuschange", $data);
    }

    //Save Status Chng..
    public function vacant_status_update() {
        if ($_REQUEST) {
            $updateArr = array("vacant_typeid" => $_REQUEST['vacant_typeid'],
                "last_date_resign" => $_REQUEST['last_date_resign'],
                "status" => ($_REQUEST['status']) ? $_REQUEST['status'] : "0");
            $this->db->where(array('fld_id' => $_REQUEST['vact_actid']));
            $response = $this->db->update('invc_vacant_resign_date', $updateArr);
            $this->session->set_flashdata('msg', 'Record Updated Successfully..');
        }
        redirect(base_url("crutraker/project_vacant"));
    }

    //Get Project Coordinator Name By Proj Id..
    public function getprojcoordinatorname($projID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->SELECT("$db2.main_users.userfullname");
        $this->db->FROM("$db1.project_coordinator");
        $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.project_coordinator.emp_id", "LEFT");
        $this->db->WHERE(array("$db1.project_coordinator.bd_project_id" => $projID, "$db1.project_coordinator.status" => '1'));
        $coordArr = $this->db->get()->row();
        return ($coordArr) ? $coordArr->userfullname : '';
    }

    //Get All Project Coordinator UserIDs...
    public function GetAllCoordinatorUserIdArr() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.project_coordinator.emp_id");
        $this->db->from("$db1.project_coordinator");
        $this->db->where(array("$db1.project_coordinator.status" => "1"));
        $this->db->group_by(array("$db1.project_coordinator.emp_id"));
        $projCoordrecArr = $this->db->get()->result();
        $reTarr = array();
        if ($projCoordrecArr):
            foreach ($projCoordrecArr as $rec) {
                $reTarr[] = $rec->emp_id;
            }
        endif;
        return $reTarr;
    }

    //Get All Project Coordinator UserIDs...
    public function GetAllProjectArr() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.tm_projects.id,$db2.tm_projects.project_name");
        $this->db->from("$db2.tm_projects");
        $this->db->where(array("$db2.tm_projects.is_active" => "1"));
        $projListArr = $this->db->get()->result();
        return ($projListArr) ? $projListArr : '';
    }

    //Get All Vacnt Type Master Data...
    public function GetAllVacantTypeMasterArr() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.invc_vacant_typemaster.fld_id,$db1.invc_vacant_typemaster.type_name");
        $this->db->from("$db1.invc_vacant_typemaster");
        $this->db->where(array("$db1.invc_vacant_typemaster.status" => "1"));
        $VTypeListArr = $this->db->get()->result();
        return ($VTypeListArr) ? $VTypeListArr : '';
    }

    public function GetAllCoordEmailList() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.main_users.id,$db2.main_users.userfullname,$db2.main_users.emailaddress");
        $this->db->from("$db1.project_coordinator");
        $this->db->join("$db2.main_users", "$db2.main_users.id=$db1.project_coordinator.emp_id", "LEFT");
        $this->db->where(array("$db1.project_coordinator.status" => "1"));
        $this->db->group_by(array("$db1.project_coordinator.emp_id"));
        $CoordrecArr = $this->db->get()->result();
        return ($CoordrecArr) ? $CoordrecArr : null;
    }

    public function GetRecDetailByID($bdcruTbl_RowId) {
        //  $bdcruTbl_RowId = 5;
        // $DesignationId = "6026";
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.bdcruvacant_crupanel.id,$db1.team_req_otheremp.emp_name,$db1.team_req_otheremp.emp_email,$db1.team_req_otheremp.emp_contact,$db2.resourceuser_inhouse.userfullname as resourceuser_inhouse_userfullname,$db2.resourceuser_inhouse.emailaddress as resourceuser_inhouse_emailaddress,$db2.resourceuser_inhouse.contactnumber as resourceuser_inhouse_contactnumber,$db2.tm_projects.project_name,$db1.designation_master_requisition.designation_name,$db1.bdcruvacant_crupanel.cv_upload,$db1.bdcruvacant_crupanel.entry_date,$db2.main_employees_summary.userfullname as entryby,$db2.coord.userfullname as coord_userfullname,$db1.bdcruvacant_crupanel.approval_status_chng_by,$db1.bdcruvacant_crupanel.approval_status_chng_date,$db1.bdcruvacant_crupanel.curr_status,$db1.bdcruvacant_crupanel.step_by_cru,$db1.bdcruvacant_crupanel.cv_sent_to_client,$db2.cvsentclientby.userfullname as cvsentclientby_userfullname,$db2.clntcvappryesornoentryby.userfullname as clntcvappryesornoentryby_userfullname,$db1.bdcruvacant_crupanel.client_cvappr_yesorno,$db1.bdcruvacant_crupanel.interaction_date,$db1.bdcruvacant_crupanel.final_cvappr_yesorno,$db1.bdcruvacant_crupanel.mobilization_date,$db1.bdcruvacant_crupanel.inhouse_or_other,$db2.main_employees_summary.emailaddress as entrybyemail");
        $this->db->from("$db1.bdcruvacant_crupanel");
        $this->db->join("$db2.main_employees_summary", "$db1.bdcruvacant_crupanel.entry_by=$db2.main_employees_summary.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as coord", "$db1.bdcruvacant_crupanel.approval_status_chng_by=$db2.coord.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as cvsentclientby", "$db1.bdcruvacant_crupanel.cv_sent_to_client_by=$db2.cvsentclientby.user_id", "LEFT");
        $this->db->join("$db2.main_employees_summary as clntcvappryesornoentryby", "$db1.bdcruvacant_crupanel.client_cvappr_entryby=$db2.clntcvappryesornoentryby.user_id", "LEFT");
        $this->db->join("$db1.designation_master_requisition", "$db1.designation_master_requisition.fld_id=$db1.bdcruvacant_crupanel.designation_id", 'LEFT');
        $this->db->join("$db1.accountinfo", "$db1.accountinfo.project_id = $db1.bdcruvacant_crupanel.project_id", 'LEFT');
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id=$db1.accountinfo.project_numberid", 'LEFT');
        $this->db->join("$db2.main_employees_summary as resourceuser_inhouse", "$db1.bdcruvacant_crupanel.user_id=$db2.resourceuser_inhouse.user_id", "LEFT");
        $this->db->join("$db1.team_req_otheremp", "$db1.bdcruvacant_crupanel.user_id=$db1.team_req_otheremp.fld_id", "LEFT");
        $this->db->where(array("$db1.bdcruvacant_crupanel.status" => "1", "$db1.bdcruvacant_crupanel.id" => $bdcruTbl_RowId));
        $recDeptArr = $this->db->get()->row();
        return ($recDeptArr) ? $recDeptArr : null;
    }

    //CRU Pannel Save..
    public function cru_panel() {
        $Arr = $_REQUEST;
        if ($Arr['bdprojid'] and $Arr['designation_id'] and $Arr['empltype']) {
            if ($Arr['empltype'] == "inhouse"):
                $inserArr = array("project_id" => $Arr['bdprojid'],
                    "designation_id" => $Arr['designation_id'],
                    "user_id" => $Arr['emp_userid_hrms'],
                    "inhouse_or_other" => $Arr['empltype'],
                    'entry_by' => $this->session->userdata('uid'));
            endif;
            if ($Arr['empltype'] == "other"):
                $inserArr = array("project_id" => $Arr['bdprojid'],
                    "designation_id" => $Arr['designation_id'],
                    "user_id" => $Arr['emp_userid_other'],
                    "inhouse_or_other" => $Arr['empltype'],
                    'entry_by' => $this->session->userdata('uid'));
            endif;
            //Resume Uploaded...
            if (($_FILES['emp_cv_file']['name']) and ( $Arr['bdprojid'] and $Arr['designation_id'] and $Arr['empltype'])) {
                $folder = 'uploads/resume/';
                $configThm = $this->getUploadConfig($folder);
                $this->load->library('upload', $configThm);
                $recc = $this->upload->initialize($configThm);
                if ($this->upload->do_upload('emp_cv_file')) {
                    $fileData = $this->upload->data();
                    $inserArr['cv_upload'] = $fileData['file_name'];
                }
            }
            $this->db->insert('bdcruvacant_crupanel', $inserArr);
            $LastinsertID = $this->db->insert_id();
        }
        if ($LastinsertID):
            $this->session->set_flashdata('msg', 'Details submited successfully');
            $this->SendMailToCoordinator($LastinsertID);
        else:
            $this->session->set_flashdata('error_msg', 'Error : Something Went Wrong');
        endif;
        redirect(base_url('crutraker/edit_crutraker_board_project/' . $Arr['bdprojid'] . '/' . $Arr['designation_id']));
    }

    //Send Mail To Coordinator .. After Fill Employee Details..
    public function SendMailToCoordinator($vacntID) {
        $data['data'] = $this->GetRecDetailByID($vacntID);
        $reCa = $this->GetAllCoordEmailList();
        if ($reCa) {
            foreach ($reCa as $cordRow) {
                $data['coordUserID'] = $cordRow->id;
                $data['coordUserName'] = $cordRow->userfullname;
                $toEmail_PC = $cordRow->emailaddress;
                $msgDetails = $this->load->view('email_template/cvtracker_mailtocoordinator', $data, true);
                $reCc = $this->sendcvtrackerMail($toEmail_PC, "Vacant Position Entry By CRU Team", $msgDetails);
            }
        }
        return ($reCc) ? $reCc : false;
    }

    //Approved ... Rec..
    public function apprv_by_pc() {
        $actID = $this->uri->segment(3);
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($actID) {
            $this->db->select("$db2.main_users.userfullname as cruentryby,$db1.bdcruvacant_crupanel.*,$db2.main_employees_summary.userfullname as userfullname_ihr,$db1.team_req_otheremp.emp_name,$db1.team_req_otheremp.emp_email,$db1.team_req_otheremp.emp_contact,$db2.main_employees_summary.emailaddress as emailaddress_ihr,$db2.main_employees_summary.contactnumber as contactnumber_ihr");
            $this->db->from("$db1.bdcruvacant_crupanel");
            $this->db->join("$db2.main_employees_summary", "$db1.bdcruvacant_crupanel.user_id=$db2.main_employees_summary.user_id", "LEFT");
            $this->db->join("$db1.team_req_otheremp", "$db1.bdcruvacant_crupanel.user_id=$db1.team_req_otheremp.fld_id", "LEFT");
            $this->db->join("$db2.main_users", "$db1.bdcruvacant_crupanel.entry_by=$db2.main_users.id", "LEFT");
            $this->db->where(array("$db1.bdcruvacant_crupanel.status" => "1", "$db1.bdcruvacant_crupanel.id" => $actID));
            $RowRecdata = $this->db->get()->row();
        }
        if ($RowRecdata):
            $UpDdata = array("$db1.bdcruvacant_crupanel.curr_status" => "Approved", "$db1.bdcruvacant_crupanel.approval_status_chng_by" => $this->session->userdata('uid'), "$db1.bdcruvacant_crupanel.approval_status_chng_date" => date("Y-m-d H:i:s"));
            $this->db->where("$db1.bdcruvacant_crupanel.id", $actID);
            $reps = $this->db->update("$db1.bdcruvacant_crupanel", $UpDdata);
            if ($reps):
                $CoordnUserID = $this->session->userdata('uid');
            // $this->sendcruteam_after_projectcoordaction($actID, $CoordnUserID);
            endif;
            $this->session->set_flashdata('msg', 'Employee CV. Record Approved successfully');
        endif;
        redirect(base_url("crutraker/project_coordinator/" . $RowRecdata->project_id . "/" . $RowRecdata->designation_id));
    }

    //Reject Code By Ash...
    public function reject_by_pc() {
        $actID = $projid = $this->uri->segment(3);
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($actID) {
            $this->db->select("$db2.main_users.userfullname as cruentryby,$db1.bdcruvacant_crupanel.*,$db2.main_employees_summary.userfullname as userfullname_ihr,$db1.team_req_otheremp.emp_name,$db1.team_req_otheremp.emp_email,$db1.team_req_otheremp.emp_contact,$db2.main_employees_summary.emailaddress as emailaddress_ihr,$db2.main_employees_summary.contactnumber as contactnumber_ihr");
            $this->db->from("$db1.bdcruvacant_crupanel");
            $this->db->join("$db2.main_employees_summary", "$db1.bdcruvacant_crupanel.user_id=$db2.main_employees_summary.user_id", "LEFT");
            $this->db->join("$db1.team_req_otheremp", "$db1.bdcruvacant_crupanel.user_id=$db1.team_req_otheremp.fld_id", "LEFT");
            $this->db->join("$db2.main_users", "$db1.bdcruvacant_crupanel.entry_by=$db2.main_users.id", "LEFT");
            $this->db->where(array("$db1.bdcruvacant_crupanel.status" => "1", "$db1.bdcruvacant_crupanel.id" => $actID));
            $RowRecdata = $this->db->get()->row();
        }
        if ($RowRecdata):
            $UpDdata = array("$db1.bdcruvacant_crupanel.curr_status" => "Rejected", "$db1.bdcruvacant_crupanel.approval_status_chng_by" => $this->session->userdata('uid'), "$db1.bdcruvacant_crupanel.approval_status_chng_date" => date("Y-m-d"));
            $this->db->where("$db1.bdcruvacant_crupanel.id", $actID);
            $reps = $this->db->update("$db1.bdcruvacant_crupanel", $UpDdata);
            if ($reps):
                $CoordnUserID = $this->session->userdata('uid');
                $this->sendcruteam_after_projectcoordaction($actID, $CoordnUserID);
            endif;
            $this->session->set_flashdata('msg', 'Employee CV. Record Rejected successfully');
        endif;
        redirect(base_url("crutraker/project_coordinator/" . $RowRecdata->project_id . "/" . $RowRecdata->designation_id));
    }

    //Send Mail To CruTeam on the Action of Approve or Reject By P-coordinator.
    public function sendcruteam_after_projectcoordaction($vacntID, $coordUserID) {
        $data['data'] = $this->GetRecDetailByID($vacntID);
        $data['coordUserID'] = $coordUserID;
        $msgDetails = $this->load->view('email_template/cvtracker_tocru_actbyprojcoord_email', $data, true);
        $resp = $this->sendcvtrackerMail($data['data']->entrybyemail, "Vacant Position Credential Status Changed", $msgDetails);
        return ($resp) ? $resp : false;
    }

    function sendcvtrackerMail($to, $subject, $msgDetails) {
        $this->load->library('email');
        $this->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.cegindia.com',
            'smtp_user' => 'marketing@cegindia.com',
            'smtp_pass' => 'MARK-2015ceg',
            'smtp_port' => 587,
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        ));
        $this->email->from('tender@cegindia.com', 'Do_not_reply_Mail');
        $this->email->to($to);
        // $cc = array('marketing@cegindia.com', 'jobs@cegindia.com', 'gmcru@cegindia.com');
        // $CI->email->cc($cc);
        $this->email->bcc('marketing@cegindia.com');
        $this->email->subject($subject);
        $this->email->message($msgDetails);
        $resp = $this->email->send();
        return ($resp) ? $resp : $this->email->print_debugger();
    }

    public function cru_vacant_home_ajax() {
        $list = $this->Projectlist_model->get_datatables();
        $loginId = $this->session->userdata('uid');
        $editPermitedArr = array('297', '296', '1615');
        $links1 = '';
        $links2 = '';
        $links3 = '';

        $StAtUs = '';
        //echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $newproject) {
            $no++;
            $row = array();
            $val = $this->GetTimeLineProgress($newproject->project_id, $newproject->designation_id);
            if ($val == "") {
                $StsTss = "<span style='color:red'> Insearch </span>";
                if (in_array($loginId, $editPermitedArr)) {
                    $links3 = "<a href='" . base_url('crutraker/edit_vacantrec/' . $newproject->resignid) . "'><i style='color:#47748b;' title='Status Change' class='glyphicon glyphicon-edit icon-red'></i></a>";
                }
            } else {

                if (($val->curr_status) and ( $val->curr_status == "Pending")) {
                    $StsTss = "<span style='color:#6f1e1e'> Sent to Coordinator </span>";
                }
                if (($val->curr_status) and ( $val->curr_status == "Approved")) {
                    $StsTss = "<span style='color:green'> Approved By Coordinator </span>";
                }
                if (($val->curr_status) and ( $val->curr_status == "Approved") and ( $val->step_by_cru == "1")) {
                    $StsTss = "<span style='color:green'> Cv Sent To Client </span>";
                }
                if (($val->curr_status) and ( $val->curr_status == "Approved") and ( $val->step_by_cru == "2")) {
                    $StsTss = "<span style='color:green'> Approved by Client </span>";
                }
                if (($val->curr_status) and ( $val->curr_status == "Approved") and ( $val->step_by_cru == "3")) {
                    $StsTss = "<span style='color:green'> Progress in Fianl Approval </span>";
                }
                if (($val->curr_status) and ( $val->curr_status == "Approved") and ( $val->step_by_cru == "3") and ( $val->final_cvappr_yesorno == "yes")) {
                    $StsTss = "<span style='color:green'> Approved & Mobilized </span>";
                }

                if (( $val->client_cvappr_yesorno == "no") or ( $val->final_cvappr_yesorno == "no") or ( $val->curr_status == "Rejected")) {
                    $StsTss = "<span style='color:#f300e0'> Rejected (X) </span>";
                    if (in_array($loginId, $editPermitedArr)) {
                        $links3 = "<a href='" . base_url('crutraker/edit_vacantrec/' . $newproject->resignid) . "'><i style='color:#47748b;' title='Status Change' class='glyphicon glyphicon-edit icon-red'></i></a>";
                    }
                }
            }
            $row[] = $no;
            $row[] = $this->getprojcoordinatorname($newproject->project_id);
            $row[] = $newproject->project_name;
            $row[] = $newproject->designation_name;
            $row[] = $newproject->type_name;
            $row[] = date("d-m-Y", strtotime($newproject->last_date_resign));
            $row[] = $StsTss;
            $deptArr = $this->GetAllCruDeptUserIdArr();
            $projCoordArr = $this->GetAllCoordinatorUserIdArr();

            //Edit Action...
            if (in_array($loginId, $deptArr)) {
                $links1 = "<a href='" . base_url('crutraker/edit_crutraker_board_project/' . $newproject->project_id . '/' . $newproject->designation_id) . "'><i style='color:red;' title='CRU' class='glyphicon glyphicon-edit icon-red'></i></a>";
            }
            if (in_array($loginId, $projCoordArr)) {
                $links2 = "<a href='" . base_url('crutraker/project_coordinator/' . $newproject->project_id . '/' . $newproject->designation_id) . "'><i style='color:green;' title='Cordinator' class='glyphicon glyphicon-user icon-green'></i></a>";
            }
            if ($loginId == "297" or $loginId == "296" or $loginId == "190") {
                $links1 = "<a href='" . base_url('crutraker/edit_crutraker_board_project/' . $newproject->project_id . '/' . $newproject->designation_id) . "'><i style='color:red;' title='CRU' class='glyphicon glyphicon-edit icon-red'></i></a>";
                $links2 = "<a href='" . base_url('crutraker/project_coordinator/' . $newproject->project_id . '/' . $newproject->designation_id) . "'><i style='color:green;' title='Cordinator' class='glyphicon glyphicon-user icon-green'></i></a>";
            }
            $row[] = $links3 . "&nbsp;" . $links1 . "&nbsp;&nbsp" . $links2;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Projectlist_model->count_all(),
            "recordsFiltered" => $this->Projectlist_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function cancelvacantrow() {
        $projId = $this->uri->segment(3);
        $designationID = $this->uri->segment(4);
        $actionID = $this->uri->segment(5);

        if ($projId and $designationID and $actionID):
            //, 'entry_by' => $this->session->userdata('uid')
            $updateArr = array('status' => "0");
            $this->db->where(array('project_id' => $projId, 'id' => $actionID, 'designation_id' => $designationID));
            $response = $this->db->update('bdcruvacant_crupanel', $updateArr);
            //History Insert..
            $hInserArr = array("project_id" => $projId, "designation_id" => $designationID, "bdcruvacant_tbid" => $actionID, "action_by" => $this->session->userdata('uid'));
            $reTresp = $this->db->insert("$db1.bdcruvacant_cancel_history", $hInserArr);
            $this->session->set_flashdata('msg', 'Cancel / Delete Status set successfully');
        endif;
        redirect(base_url("crutraker/edit_crutraker_board_project/" . $projId . "/" . $designationID));
    }

    public function cancelrowbypc() {
        $projId = $this->uri->segment(3);
        $designationID = $this->uri->segment(4);
        $actionID = $this->uri->segment(5);
        if ($projId and $designationID and $actionID):
            //, 'entry_by' => $this->session->userdata('uid')
            $updateArr = array('status' => "0");
            $this->db->where(array('project_id' => $projId, 'id' => $actionID, 'designation_id' => $designationID));
            $response = $this->db->update('bdcruvacant_crupanel', $updateArr);
            //History Insert..
            $hInserArr = array("project_id" => $projId, "designation_id" => $designationID, "bdcruvacant_tbid" => $actionID, "action_by" => $this->session->userdata('uid'));
            $reTresp = $this->db->insert("$db1.bdcruvacant_cancel_history", $hInserArr);
            $this->session->set_flashdata('msg', 'Cancel / Delete Status set successfully');
        endif;
        redirect(base_url("crutraker/project_coordinator/" . $projId . "/" . $designationID));
    }

}
