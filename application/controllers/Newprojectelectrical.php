<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Newprojectelectrical extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('newtenderelectrical_model', 'bd_tenderdetails');
        $this->load->model('Front_model');

        if (!($this->session->userdata('uid'))) {
            redirect(base_url());
        }
    }

    public function index() {
        $this->load->helper('url');
        $this->load->view('customers_view');
    }

    //New Project All Phase Record..
    public function ajax_list() {
        $list = $this->bd_tenderdetails->get_datatables();

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->Expiry_Date;
            $row[] = "<small>" . str_replace("_x000D_", "<br>", strip_tags($customers->TenderDetails)) . "</small>";
            $row[] = ucfirst($customers->Location);
            $row[] = ucwords(strtolower($customers->Organization));
            $row[] = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $customers->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>'
                    . '&nbsp&nbsp<i style="cursor:pointer" title="Active" onclick="activethisrecord(' . "'" . $customers->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-ok"></i>&nbsp&nbsp' .
                    '&nbsp&nbsp<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('dashboard/projurlview?projID=' . $customers->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp&nbsp'
                    . '&nbsp&nbsp<a  href="javascript:void(0)" title="InActive/Delete" onclick="deletethisrecord(' . "'" . $customers->fld_id . "','" . 0 . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a>' . '&nbsp;&nbsp;<input name="actchk[]" value="' . $customers->fld_id . '" type="checkbox">';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            //"recordsTotal" => $this->activeproject->count_all(),
            "recordsTotal" => '0',
            "recordsFiltered" => $this->bd_tenderdetails->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function test() {
        echo $this->session->userdata('uid');
    }

}
