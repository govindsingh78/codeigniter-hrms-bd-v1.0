<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Togoproject extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('Manmonth_model');
        $this->load->model('Togoproject_model', 'togoproject');
        $this->load->model('Togoprojecteoi_model');
        $this->load->model('Togoprojectrfp_model');
        $this->load->model('Togoprojectfq_model');
        $this->load->model('Front_model');
        $this->load->model('SecondDB_model');
        $this->load->model('mastermodel');
        if (!($this->session->userdata('loginid'))) {
            redirect(base_url());
        }
        $bduserid = $this->session->userdata('loginid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";


        
        if ($actual_link == "http://bd.cegtechno.com") {
           if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):
            else:
                redirect(base_url('welcome/logout'));
            endif;
        }
    }

    //Dashbord After Login..
    public function index() {
        $title = 'Ongoing Biding';
        $sectorArr = $this->Front_model->GetActiveSector();
        $serviceArr = $this->Front_model->mainServices();
        $country_Arr = $this->mastermodel->SelectRecordC('countries', array('status' => '1'));
        $secId = '0';
        $compnameArr = array();
        $proposalManager = $this->Front_model->allprojectProposalManager();
        //Code By Asheesh..
        $Rec = $this->Front_model->selectRecord('main_company', array('fld_id', 'company_name'), array('status' => '1'));
        if ($Rec != '') {
            $compnameArr = $Rec->result();
        }
        $security_Type = $this->mastermodel->SelectRecord('bidsecurity_type', array('is_active' => '1'));
        $this->load->view('togoproject/tender_view', compact('security_Type', 'serviceArr', 'title', 'secId', 'sectorArr', 'proposalManager', 'compnameArr', 'country_Arr'));
    }

    // Project Display
    public function newProjectAll() {
        $list = $this->togoproject->get_datatables();

        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $assignTo = '';
        $view1 = '';
        $view2 = '';
        $view3 = '';
        //echo '<pre>'; print_r($list);
        foreach ($list as $togoproject) {
            $count = $this->mastermodel->count_rows('comment_on_project', array('project_id' => $togoproject->fld_id, 'is_active' => '1'));
            if ($count < 1):
                $comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp;&nbsp;';
            else:
                $comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>&nbsp;&nbsp;';
            endif;

            $exitproject = $this->Front_model->getProjectDetails($togoproject->fld_id);
            if (empty($togoproject->generated_tenderid)) {
                $genertedID = '<i id="faction" title="Project No. Generator" onclick="assignpm(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#generateprojid" class="glyphicon glyphicon-grain"></i>&nbsp;&nbsp;';
            } else {
                $genertedID = '';
            }
            if (empty($togoproject->assign_to)) {
                $assignTo = '<a href="#"><i id="faction" title="Set Proposal Manager" onclick="assignpm(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#assignmanager" class="glyphicon glyphicon-user icon-white"></i></a>&nbsp;&nbsp;';
            } else {
                $assignTo = '';
            }

            if ($bdRole == 1) {
                $view1 = '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('togoproject/projurlview?projID=' . $togoproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp;&nbsp;' .
                        '<a href="#"><i title="View Details" style="cursor:pointer" onclick="window.open(' . "'" . base_url('togoproject/projectconcerndetails?projID=' . $togoproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon glyphicon-list-alt icon"></i></a>&nbsp;&nbsp;' .
                        $genertedID;
                if ($togoproject->generated_tenderid):
                    $view1 .= '&nbsp;&nbsp;<i style="cursor:pointer" title=" Submitted" onclick="bidproject(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-scale"></i>&nbsp;&nbsp;';
                endif;
            } elseif ($bdRole == 2) {
                $view2 = '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('togoproject/projurlview?projID=' . $togoproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp;&nbsp;' .
                        '<a href="#"><i title="View Details" style="cursor:pointer" onclick="window.open(' . "'" . base_url('togoproject/projectconcerndetails?projID=' . $togoproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon glyphicon-list-alt icon"></i></a>&nbsp;&nbsp;' .
                        $genertedID;
                if ($togoproject->generated_tenderid):
                    $view2 .= '&nbsp;&nbsp;<i style="cursor:pointer" title=" Submitted" onclick="bidproject(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-scale"></i>&nbsp;&nbsp;';
                endif;
            } else {
                $view3 = '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('togoproject/projurlview?projID=' . $togoproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp;&nbsp;' .
                        '<a href="#"><i title="View Details" style="cursor:pointer" onclick="window.open(' . "'" . base_url('togoproject/projectconcerndetails?projID=' . $togoproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon glyphicon-list-alt icon"></i></a>&nbsp;&nbsp;' .
                        $genertedID .
                        '<a style="cursor:pointer"><i style="cursor:pointer" title="No Submitted" onclick="nosubmitproj(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-remove icon-white"></i></a>&nbsp;&nbsp;' .
                        '<a  href="javascript:void(0)" title="Not Important" onclick="nogoprojs(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')"><i class="glyphicon glyphicon-trash icon-white"></i></a>';
                if ($togoproject->generated_tenderid):
                    $view3 .= '&nbsp;&nbsp;<i style="cursor:pointer" title=" Submitted" onclick="bidproject(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-scale"></i>&nbsp;&nbsp;';
                endif;
            }

            if (empty($exitproject)) {
                $projectfill = '<li style="cursor:pointer" title="Edit Project" data-toggle="modal" data-target="#myModalbasic" onclick="editprojectdetail(' . "'" . $togoproject->fld_id . "'" . ",'" . $togoproject->Expiry_Date . "'" . ')" class="glyphicon glyphicon-edit"></li> 
				<a style="cursor:pointer"><i style="cursor:pointer" title="No Submitted" onclick="nosubmitproj(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-remove icon-white"></i></a></p>';
            } else {
                $projectfill = $view1 . $view2 . $view3 . $assignTo;
            }

            $user = $this->SecondDB_model->getUserByID($togoproject->assign_to);
            $detail = $togoproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $resull = array();
            $resull = explode("/", $togoproject->generated_tenderid);
            $row[] = ($togoproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($togoproject->Expiry_Date));
            $row[] = $resull[3];
            $row[] = $togoproject->TenderDetails . '<hr><p><span style="float:left;">' . $togoproject->generated_tenderid . '</span><span style="float:right; color:green;">' . $user->userfullname . '</span> &nbsp;&nbsp;&nbsp; <button onclick="addjvset(' . "'" . $togoproject->fld_id . "'" . ')" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModaljv" >Consortium</button> <li style="cursor:pointer" title="Edit Project" data-toggle="modal" data-target="#editproj" onclick="editproject(' . "'" . $togoproject->fld_id . "'" . ')" class="glyphicon glyphicon-edit"></li> </p>' . '<br/><br/><a style="color: blue;" href="' . base_url('importantproject/projectdetail_view/' . $togoproject->fld_id) . '" target="_blank">View Project Detail</a>';
            $row[] = $togoproject->Location;
            $row[] = $togoproject->Organization . '&nbsp; <li style="cursor:pointer" title="Add Client" data-toggle="modal" data-target="#clientmodel" onclick="editclientmodel(' . "'" . $togoproject->fld_id . "'" . ')" class="glyphicon glyphicon-edit"></li>';
            // $row[] = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $activeproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp;&nbsp;'
            // . '<i style="cursor:pointer" title="Important" onclick="setimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-star icon-white"></i>&nbsp;&nbsp;' .
            // '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('activeproject/projurlview?projID=' . $activeproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp;&nbsp;'
            // . '<a  href="javascript:void(0)" title="Not Important" onclick="setnotimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a>' . '&nbsp;&nbsp;<input name="actchk[]" value="' . $activeproject->fld_id . '" type="checkbox">';
            // $row[] = $view1 . $view2 . $view3 . $assignTo . $projectfill;
            $row[] = $comment . $projectfill;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            //"recordsTotal" => $this->activeproject->count_all(),
            "recordsTotal" => '0',
            "recordsFiltered" => $this->togoproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }


     //Generate Project No..
     public function generate_project_no() {
        $formArrData = $_REQUEST;

 
         
        //Before Update Insert New ID..
        $insertedArr = array('project_id' => $formArrData['project_id'], 'generated_tenderid' => '', 'generate_type' => $formArrData['prefx_name'], 'generate_by' => $this->session->userdata('loginid'));
        $this->Front_model->insertRecord('tender_generated_id', $insertedArr);
        $insertedID = $this->db->insert_id();
         
        //After Insert Update ID In Same Table..
        if ($formArrData['prefx_name'] == 'E'):
            $last = $formArrData['E'];
        elseif ($formArrData['prefx_name'] == 'P'):
            $last = $formArrData['P'];
        elseif ($formArrData['prefx_name'] == 'FQ'):
            $last = $formArrData['FQ'];
        endif;
        //Codition for Financial Year..

        if (date('m') < 4):
            $generatedId = 'CEGHO/BD/' . (date('Y') - 1) . "-" . date('Y') . "/" . $formArrData['prefx_name'] . '0' . ($last + 1);
        else :
            $generatedId = 'CEGHO/BD/' . date('Y') . "-" . (date('Y') + 1) . "/" . $formArrData['prefx_name'] . '0' . ($last + 1);
        endif;
        $generate_id = '0' . ($last + 1);
        $respon = $this->Front_model->updateRecord('tender_generated_id', array('generated_tenderid' => $generatedId, 'generate_id' => $generate_id), array('fld_id' => $insertedID));
         
       
        // $generatedId = $formArrData['prefx_name'] . uniqid();
        $respon = $this->Front_model->updateRecord('proj_togo_for_user', array('projectgenid' => $generatedId), array('project_id' => $formArrData['project_id']));
        // if ($respon):
        //     $res = $this->Front_model->updateRecord('projectno_prefx', array('last_generate_id' => ($last + 1)), array('prefix_id' => $formArrData['prefx_name']));
        //     $this->session->set_flashdata('msg', "TenderId Generated Success.");
        //     redirect(base_url('/togoproject'));
        // else:
        //     redirect(base_url('/togoproject'));
        // endif;


        $message = "Some issues occurred !";
        if ($respon):
            $res = $this->Front_model->updateRecord('projectno_prefx', array('last_generate_id' => ($last + 1)), array('prefix_id' => $formArrData['prefx_name']));
            
			$message = "TenderId Generated Success.";
        else:
          
			$message = "Something Went Wrong.";
        endif;
 

		$output = array(
        
			"msg" => $message,
		);
		echo json_encode($output);


    }
    //EOI Project
    public function ongoingeoiproject() {
		// echo "testt"; die;
        $title = 'Ongoing Biding EOI';
        $sectorArr = $this->Front_model->GetActiveSector();
        $serviceArr = $this->Front_model->mainServices();
        $country_Arr = $this->mastermodel->SelectRecordC('countries', array('status' => '1'));

        $clientName = $this->Front_model->selectRecord('bd_client_master', array('id', 'client_name'), array('status' => '0'));
        if ($clientName != '') {
            $clientName = array_filter($clientName->result());
        }

        $fundingOrg = $this->Front_model->selectRecord('bd_funding_master', array('id', 'funding_org'), array('status' => '0'));
        if ($fundingOrg != '') {
            $fundingOrg = array_filter($fundingOrg->result());
        }
		// echo "<pre>"; print_r($country_Arr); die;
        $secId = '0';
        $compnameArr = array();
        $proposalManager = $this->Front_model->allprojectProposalManager();
		 
        //Code By Asheesh..
        $Rec = $this->Front_model->selectRecord('main_company', array('fld_id', 'company_name'), array('status' => '1'));
        // echo "<pre>"; print_r($Rec); die;
		if ($Rec != '') {
			// echo "test"; die;
            $compnameArr = $Rec->result();
        }
        $security_Type = $this->mastermodel->SelectRecord('bidsecurity_type', array('is_active' => '1'));
        // echo "<pre>"; print_r($security_Type); die;
		$this->load->view('togoproject/eoi_view', compact('security_Type', 'serviceArr', 'country_Arr', 'title', 'secId', 'sectorArr', 'proposalManager', 'compnameArr', 'clientName', 'fundingOrg'));
    }

    public function eoiProjectAll() {
        $list = $this->Togoprojecteoi_model->get_datatables();

        //print_r($list); die;



        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $assignTo = '';
        $view1 = '';
        $view2 = '';
        $view3 = '';
        //echo '<pre>'; print_r($list); die;
        foreach ($list as $togoproject) {
            $count = $this->mastermodel->count_rows('comment_on_project', array('project_id' => $togoproject->fld_id, 'is_active' => '1'));
            if ($count < 1):
                $comment = '';
                //$comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp;&nbsp;';
            else:
                $comment = '';
                //$comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>&nbsp;&nbsp;';
            endif;

            $exitproject = $this->Front_model->getProjectDetails($togoproject->fld_id);

            if (empty($togoproject->generated_tenderid)) {
                $genertedID = '<i id="faction" title="Project No. Generator" onclick="assignpm(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#generateprojid" class="glyphicon glyphicon-grain"></i>&nbsp;&nbsp;';
            } else {
                $genertedID = '';
            }
            if (empty($togoproject->assign_to)) {
                $assignTo = '<a href="#"><i id="faction" title="Set Proposal Manager" onclick="assignpm(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#assignmanager" class="glyphicon glyphicon-user icon-white"></i></a>&nbsp;&nbsp;';
            } else {
                $assignTo = '';
            }
 
            if ($bdRole == 1) {
                $view1 = '<button   class="btn btn-info btn-sm" title="View link" onclick="newprojurlview('.$togoproject->fld_id.')" ><i class="fa fa-link" aria-hidden="true"></i>
                </button>&nbsp&nbsp
                 <a href="#"><i title="View Details" style="cursor:pointer" onclick="window.open(' . "'" . base_url('togoproject/projectconcerndetails?projID=' . $togoproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon glyphicon-list-alt icon"></i></a>&nbsp;&nbsp;' .
                        $genertedID;
                if ($togoproject->generated_tenderid):
                    $view1 .= '&nbsp;&nbsp;<i style="cursor:pointer" title=" Submitted" onclick="bidproject(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-scale"></i>&nbsp;&nbsp;';
                endif;
            } elseif ($bdRole == 2) {
                $view2 = '<button   class="btn btn-info btn-sm" title="View link" onclick="newprojurlview('.$togoproject->fld_id.')" ><i class="fa fa-link" aria-hidden="true"></i>
                </button>&nbsp&nbsp
                 <a href="#"><i title="View Details" style="cursor:pointer" onclick="window.open(' . "'" . base_url('togoproject/projectconcerndetails?projID=' . $togoproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon glyphicon-list-alt icon"></i></a>&nbsp;&nbsp;' .
                        $genertedID;
                if ($togoproject->generated_tenderid):
                    $view2 .= '&nbsp;&nbsp;<i style="cursor:pointer" title=" Submitted" onclick="bidproject(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-scale"></i>&nbsp;&nbsp;';
                endif;
            } else {
                
                // $view3 = '<button   class="btn btn-info btn-sm" title="View link" onclick="newprojurlview('.$togoproject->fld_id.')" >
                // <i class="fa fa-link" aria-hidden="true"></i>
                // </button>&nbsp&nbsp
                //  <a href="#"><i title="View Details" style="cursor:pointer" onclick="
                //  window.open(' . "'" . base_url('togoproject/projectconcerndetails?projID=' . $togoproject->fld_id) . "', '', 
                //  'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon glyphicon-list-alt icon"></i></a>
                //  &nbsp;&nbsp;' .
                //         $genertedID .
                //         '<a style="cursor:pointer"><i style="cursor:pointer" title="No Submitted" 
                //         onclick="nosubmitproj(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')" 
                //         class="glyphicon glyphicon-remove icon-white"></i></a>&nbsp;&nbsp;' .
                //         '<a  href="javascript:void(0)" title="Not Important" onclick="
                //         nogoprojs(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')"><i class="glyphicon glyphicon-trash icon-white"></i></a>';
                // if ($togoproject->generated_tenderid):
                //     $view3 .= '&nbsp;&nbsp;<i style="cursor:pointer" title=" Submitted" 
                //     onclick="bidproject(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')"
                //      class="glyphicon glyphicon-scale"></i>&nbsp;&nbsp;';
                // endif;


                $view3 = '' .
                        $genertedID .
                        '' .
                        '<div class="btn btn-danger btn-sm" title="Not Important" onclick="
                        nogoprojs(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')"><i class="fa fa-trash" aria-hidden="true"></i></div>';
                if ($togoproject->generated_tenderid):
                    $view3 .= '&nbsp;&nbsp;
                     
                     
                     <div class="btn btn-success btn-sm" style="cursor:pointer" title=" Submitted"
                      onclick="bidproject(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')"> <i class="fa fa-send-o" aria-hidden="true"></i></div>&nbsp;&nbsp;';
                endif;


                






            }

            if (empty($exitproject)) {
                $projectfill = '
                
                <div class="btn btn-success btn-sm" title="Edit Project" data-toggle="modal" data-target="#myModalbasic"   onclick="editprojectdetail(' . "'" . $togoproject->fld_id . "'" . ",'" 
                . $togoproject->Expiry_Date . "'" . ')"> <i class="fa fa-edit" aria-hidden="true"></i></div>&nbsp;&nbsp;

                 <div class="btn btn-danger btn-sm"   title="No Submitted" onclick="nosubmitproj(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')" > <i class="fa fa-trash" aria-hidden="true"></i></div>
                
               ';
            } else {
                $projectfill = $view1 . $view2 . $view3 . $assignTo;
            }

            $user = $this->SecondDB_model->getUserByID($togoproject->assign_to);
            $detail = $togoproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $resull = array();
            $resull = explode("/", $togoproject->generated_tenderid);
            $row[] = ($togoproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($togoproject->Expiry_Date));
            $row[] = $resull[3];
            // <button onclick="addjvset(' . "'" . $togoproject->fld_id . "'" . ')" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModaljv" >Consortium</button>
            // <li style="cursor:pointer" title="Edit Project" data-toggle="modal" data-target="#editproj" onclick="editproject(' . "'" . $togoproject->fld_id . "'" . ')" class="glyphicon glyphicon-edit"></li>
            $row[] = ucFirst($togoproject->TenderDetails) . '<hr><div class="highlight mb-2">' . $togoproject->generated_tenderid . '</div><div class="highlight">' . $user->userfullname . '</div>' ;
            $row[] = $togoproject->Location;
            // &nbsp; <li style="cursor:pointer" title="Add Client" data-toggle="modal" data-target="#clientmodel" onclick="editclientmodel(' . "'" . $togoproject->fld_id . "'" . ')" class="glyphicon glyphicon-edit"></li>
            $row[] = $togoproject->Organization . '';
            //$row[] = $view1 . $view2 . $view3 . $assignTo;
            $row[] = $comment . $projectfill.'&nbsp;&nbsp;<a class="btn btn-warning btn-sm ml-2"   href="' . base_url('importantproject/projectdetail_view/' . $togoproject->fld_id) . '" 
            target="_blank" title=" View Project Detail" alt=" View Project Detail"> <i class="fa fa-list" aria-hidden="true"></i></a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Togoprojecteoi_model->count_filtered(),
            "recordsFiltered" => $this->Togoprojecteoi_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    
    //RFP Project
    public function ongoingrfpproject() {
        $title = 'Ongoing Biding RFP';
        $sectorArr = $this->Front_model->GetActiveSector();

        $clientName = $this->Front_model->selectRecord('bd_client_master', array('id', 'client_name'), array('status' => '0'));
        if ($clientName != '') {
            $clientName = $clientName->result();
        }

        $fundingOrg = $this->Front_model->selectRecord('bd_funding_master', array('id', 'funding_org'), array('status' => '0'));
        if ($fundingOrg != '') {
            $fundingOrg = $fundingOrg->result();
        }
 
 
         
        $serviceArr = $this->Front_model->mainServices();
        $country_Arr = $this->mastermodel->SelectRecordC('countries', array('status' => '1'));
        $secId = '0';
        $compnameArr = array();
        $proposalManager = $this->Front_model->allprojectProposalManager();
        //Code By Asheesh..
        $Rec = $this->Front_model->selectRecord('main_company', array('fld_id', 'company_name'), array('status' => '1'));
        if ($Rec != '') {
            $compnameArr = $Rec->result();
        }
        $security_Type = $this->mastermodel->SelectRecord('bidsecurity_type', array('is_active' => '1'));
        $this->load->view('togoproject/rfp_view', compact('security_Type', 'serviceArr', 'country_Arr', 'title', 'secId', 'sectorArr', 'proposalManager', 'compnameArr', 'clientName', 'fundingOrg'));
    }

    
    public function rfpProjectAll() {
		// echo "tst"; die;
        $list = $this->Togoprojectrfp_model->get_datatables();
	 // print_r($this->db->last_query()); die;
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $assignTo = '';
        $view1 = '';
        $view2 = '';
        $view3 = '';
        //echo '<pre>'; print_r($list);
        foreach ($list as $togoproject) {
            $count = $this->mastermodel->count_rows('comment_on_project', array('project_id' => $togoproject->fld_id, 'is_active' => '1'));
            if ($count < 1):
                $comment = '';
                // $comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp;&nbsp;';
            else:
                // $comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>&nbsp;&nbsp;';
            endif;

            $exitproject = $this->Front_model->getProjectDetails($togoproject->fld_id);

            if (empty($togoproject->generated_tenderid)) {
                $genertedID = '<div div class="btn btn-info btn-sm" id="faction" title="Project No. Generator" onclick="assignpm(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#generateprojid">
                <i class="fa fa-folder" aria-hidden="true"></i>
                </div>&nbsp;&nbsp;';
            } else {
                $genertedID = '';
            }
            if (empty($togoproject->assign_to)) {
                $assignTo = '<div class="btn btn-success btn-sm" id="faction" title="Set Proposal Manager" onclick="assignpm(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#assignmanager"
                ><i class="fa fa-edit" aria-hidden="true"></i></div>&nbsp;&nbsp;';
            } else {
                $assignTo = '';
            }

            if ($bdRole == 1) {
                $view1 = '' .
                        '' .
                        $genertedID;
                if ($togoproject->generated_tenderid):
                    $view1 .= '&nbsp;&nbsp;<div class="btn btn-success btn-sm" style="cursor:pointer" title=" Submitted" onclick="bidproject(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')" > <i class="fa fa-edit" aria-hidden="true"></i></div>&nbsp;&nbsp;';
                endif;
            } elseif ($bdRole == 2) {
                $view2 = '' .
                        '' .
                        $genertedID;
                if ($togoproject->generated_tenderid):
                    $view2 .= '&nbsp;&nbsp;<div class="btn btn-success btn-sm" style="cursor:pointer" title=" Submitted" onclick="bidproject(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')"> <i class="fa fa-edit" aria-hidden="true"></i></div>&nbsp;&nbsp;';
                endif;
            } else {
                $view3 = '' .
                        '' .
                        $genertedID .
                        '' .
                        '<div class="btn btn-success btn-sm" title="Edit Project" data-toggle="modal" data-target="#myModalbasic"   onclick="editprojectdetail(' . "'" . $togoproject->fld_id . "'" . ",'" 
                        . $togoproject->Expiry_Date . "'" . ')"> <i class="fa fa-edit" aria-hidden="true"></i></div>&nbsp;&nbsp<div class="btn btn-danger btn-sm"  title="Not Important" onclick="nogoprojs(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')"> <i class="fa fa-trash" aria-hidden="true"></i></div>&nbsp;&nbsp;';
                if ($togoproject->generated_tenderid):
                    $view3 .= '&nbsp;&nbsp;<div class="btn btn-success btn-sm" style="cursor:pointer" title=" Submitted" onclick="bidproject(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')"> <i class="fa fa-send-o" aria-hidden="true"></i></div>&nbsp;&nbsp;';
                endif;
            }

            if (empty($exitproject)) {
                // $projectfill = '<li style="cursor:pointer" title="Edit Project" data-toggle="modal" data-target="#myModalbasic" onclick="editprojectdetail(' . "'" . $togoproject->fld_id . "'" . ",'" . 
                // $togoproject->Expiry_Date . "'" . ')" class="glyphicon glyphicon-edit"></li> 
                // <a style="cursor:pointer"><i style="cursor:pointer" title="No Submitted" onclick="nosubmitproj(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')" 
                // class="glyphicon glyphicon-remove icon-white"></i></a></p>';

                $projectfill = '

<div class="btn btn-success btn-sm" title="Edit Project" data-toggle="modal" data-target="#myModalbasic"   onclick="editprojectdetail(' . "'" . $togoproject->fld_id . "'" . ",'" 
. $togoproject->Expiry_Date . "'" . ')"> <i class="fa fa-edit" aria-hidden="true"></i></div>&nbsp;&nbsp;

<div class="btn btn-danger btn-sm mr-2"   title="No Submitted" onclick="nosubmitproj(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')" > <i class="fa fa-trash" aria-hidden="true"></i></div>

';

            } else {
                $projectfill = $view1 . $view2 . $view3 . $assignTo;
            }

            $user = $this->SecondDB_model->getUserByID($togoproject->assign_to);

            $detail = $togoproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $resull = array();
            $resull = explode("/", $togoproject->generated_tenderid);
            $row[] = ($togoproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($togoproject->Expiry_Date));
            $row[] = $resull[3];
            // <button onclick="addjvset(' . "'" . $togoproject->fld_id . "'" . ')" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModaljv" >Consortium</button> 
            // <li style="cursor:pointer" title="Edit Project" data-toggle="modal" data-target="#editproj" onclick="editproject(' . "'" . $togoproject->fld_id . "'" . ')" class="glyphicon glyphicon-edit"></li>
            $row[] = ucfirst($togoproject->TenderDetails) . '<hr> <div class="highlight mb-2">' . $togoproject->generated_tenderid . '</div><div class="highlight">' . $user->userfullname . '</div>';
            $row[] = $togoproject->Location;
            // <li style="cursor:pointer" title="Add Client" data-toggle="modal" data-target="#clientmodel" onclick="editclientmodel(' . "'" . $togoproject->fld_id . "'" . ')" class="glyphicon glyphicon-edit"></li>
            $row[] = $togoproject->Organization . '&nbsp;';
            //$row[] = $view1 . $view2 . $view3 . $assignTo;
            $row[] = $comment . $projectfill.'
            <a class="btn btn-warning btn-sm"   href="' . base_url('importantproject/projectdetail_view/' . $togoproject->fld_id) . '" target="_blank" title=" View Project Detail" 
            alt=" View Project Detail"> <i class="fa fa-list" aria-hidden="true"></i></a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Togoprojectrfp_model->count_filtered(),
            "recordsFiltered" => $this->Togoprojectrfp_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    //FQ Project
    public function ongoingfqproject() {
        $title = 'Ongoing Biding FQ';
        $sectorArr = $this->Front_model->GetActiveSector();
        $serviceArr = $this->Front_model->mainServices();
        $country_Arr = $this->mastermodel->SelectRecordC('countries', array('status' => '1'));
        $clientName = $this->Front_model->selectRecord('bd_client_master', array('id', 'client_name'), array('status' => '0'));
        if ($clientName != '') {
            $clientName = array_filter($clientName->result());
        }

        $fundingOrg = $this->Front_model->selectRecord('bd_funding_master', array('id', 'funding_org'), array('status' => '0'));
        if ($fundingOrg != '') {
            $fundingOrg = array_filter($fundingOrg->result());
        }

        $secId = '0';
        $compnameArr = array();
        $proposalManager = $this->Front_model->allprojectProposalManager();
        //Code By Asheesh..
        $Rec = $this->Front_model->selectRecord('main_company', array('fld_id', 'company_name'), array('status' => '1'));
        if ($Rec != '') {
            $compnameArr = $Rec->result();
        }
        $security_Type = $this->mastermodel->SelectRecord('bidsecurity_type', array('is_active' => '1'));
        $this->load->view('togoproject/fq_view', compact('clientName', 'fundingOrg','security_Type', 'serviceArr', 'country_Arr', 'title', 'secId', 'sectorArr', 'proposalManager', 'compnameArr'));
    }

    //Link View Details Popup Open Ash..
    public function projurlview() {
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('bd_tenderdetail', array('Tender_url'), array('fld_id' => $projId));
        if ($Rec) {
            $RowDataArr = $Rec->row();
        }
        $hyperlink = ($RowDataArr->Tender_url) ? $RowDataArr->Tender_url : 'http://www.cegindia.com/';
        $this->load->view('welcome_link', compact('projId', 'hyperlink'));
    }

    //Visibility Status
    public function visibilityStatus($copeName, $actid) {
        return $this->Front_model->updateRecord('bd_tenderdetail', array('visible_scope' => $copeName), array('fld_id' => $actid));
    }

    //Comment Showe
    public function commentset() {
        $tndrid = $_REQUEST['tndrid'];
        if (isset($tndrid)) {
            $Rec = $this->Front_model->selectRecordOrderByASC('comment_on_project', array('*'), array('is_active' => '1', 'project_id' => $tndrid));
            if ($Rec) {
                $data['allcomments'] = $Rec->result();
            }
            $this->load->view('comment_view', $data);
        }
    }


    //For No Go Project set..
    public function nogosubmit() {
        $message = "Some issues occured";
        if ($_REQUEST['actid']) {
            $businessID = $this->session->userdata('businessunit_id');
            $id = $_REQUEST['actid'];

           
            $data = $this->Front_model->getProjectDetail($id);
            //echo '<pre>'; print_r($data); 
            if ($data) {
                $inserArr = array(
                    'created_By' => $data->created_By,
                    'tender24_ID' => $data->tender24_ID,
                    'TenderDetails' => $data->TenderDetails,
                    'Location' => $data->Location,
                    'Organization' => $data->Organization,
                    'Tender_url' => $data->Tender_url,
                    'Sector_IDs' => $data->Sector_IDs,
                    'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
                    'Keyword_IDs' => $data->Keyword_IDs,
                    'keyword_phase' => $data->keyword_phase,
                    'Created_Date' => $data->Created_Date,
                    'Expiry_Date' => $data->Expiry_Date,
                    'source_file' => $data->source_file,
                    'project_type' => $data->project_type,
                    'country' => $data->country,
                    'remarks' => $data->remarks,
                    'national_intern' => $data->national_intern,
                    'national_status' => $data->national_status,
                    'parent_tbl_id' => $data->project_id,
                );

                $insertnogoArr = array(
                    'project_id' => $data->project_id,
                    'user_id' => $this->session->userdata('uid'),
                    'action_date' => date('Y-m-d H:i:s'),
                    'actionIP' => get_client_ip(),
                    'message' => 'No Go Project'
                );

                $inserscopedumpArr = array(
                    'project_id' => $data->project_id,
                    'visible_scope' => 'No_Go_project',
                    'businessunit_id' => $data->businessunit_id
                );
            }


          
            $this->db->select('*');
            $this->db->from('bdtender_scope');
            $this->db->where(array('project_id' => $id, 'businessunit_id' => $businessID));
            $count = $this->db->count_all_results();
             
            
            //$count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));

           

            if ($count <= 5 AND $count > 1):
                $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
                $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
                $Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnogoArr);
                $res = $this->db->delete('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));
            else:
                $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
                $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
                $Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnogoArr);
                $res = $this->db->delete('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));
                $res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $id));
            endif;

            // if ($Respon > 0):
            //     $this->session->set_flashdata('msg', "Project Status  Changed No Go / Not Important. ");
            // endif;

            if($Respon > 0){
                $message = "Project Status  Changed No Go / Not Important. ";
            } 


          

            
        }

        $arrMessage = array(
            'msg' => $message,
        );

        echo json_encode($arrMessage);
    }

    //Comment Submit..
    public function commentsubmit() {
        $projId = $_REQUEST['projId'];
        $comnt = $_REQUEST['comnt'];
        if ($comnt):
            $insertArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $projId, 'sectorId' => '0', 'commentText' => $comnt);
            $Rec = $this->Front_model->insertRecord('comment_on_project', $insertArr);
            $this->notified($projId, 'A new Comment on Tender.');
        endif;
        return $Rec;
    }

    //Submit Project Concern Details..
    public function projectconcerndetails() {
        //Get Project Id..
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('project_concern_details', array('concern_details'), array('project_id' => $projId));
        if ($Rec != '') {
            $data = $Rec->result();
        } else {
            $data[0] = '';
        }
        $this->load->view('welcome_message', compact('projId', 'data'));
    }

    //Assign Proposal manager To Bid Project.
    public function assign_proposal_manager() {
        $formArrData = $_REQUEST;
        $respon = $this->Front_model->updateRecord('bid_project', array('proposal_manager' => $formArrData['proposal_manager']), array('project_id' => $formArrData['project_id']));
        $inserArr = array('project_id' => $formArrData['project_id'], 'assign_to' => $formArrData['proposal_manager'], 'assign_by' => $this->session->userdata('loginid'));
        $Respon2 = $this->Front_model->insertRecord('assign_project_manager', $inserArr);
        if (!empty($formArrData['proposal_manager'])) {
            $selectp = $this->db->query('select a.*,b.* from bd_tenderdetail as a left join tender_generated_id b on a.fld_id=b.project_id where b.project_id =' . $formArrData['project_id']);
            $query = $selectp->result();
            $pmid = $this->session->userdata('loginid');
            $userTbl = $this->Front_model->selectRecord('main_users', array('emailaddress'), array('fld_id' => $formArrData['proposal_manager']));
            $userData = $userTbl->first_row();
            $emailAddress = array($userData->emailaddress);
            $name = $this->Front_model->UserNameById($formArrData['proposal_manager']);
            $assignBy = $this->Front_model->UserNameById($pmid);
            $message = '<html>
<head></head>
<body>
<table bgcolor="#ffffff" width="500px;" cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; max-width: 600px;"  >
	<tbody>
		<tr>
			<td valign="top" style="font-size: 14px; padding-top:2.429em;padding-bottom:0.929em;">
				<p style="text-align:center;margin:0;padding:0;">
					<img src="http://www.cegindia.com/assets/images/logo.png" style="display:inline-block;">
				</p>
			</td>
		</tr>
		<tr>
			<td align="center" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-top:1px solid #e2e2e2;
    border-left:1px solid #e2e2e2;border-right:1px solid #e2e2e2;border-radius: 4px 4px 0 0 ;background-clip: padding-box;
    border-spacing: 0;">
				<tbody>
					<tr>
						<td valign="top" style=" color:#505050;font-family:Helvetica;font-size:14px;line-height:150%;    padding-top:3.143em;
    padding-right:3.5em;padding-left:3.5em; padding-bottom:0.714em;text-align:left;" mc:edit="body_content_01">
							<p style="color:#545454;display:block;font-family:Helvetica;font-size:16px;line-height:1.500em;
     font-style:normal;font-weight:normal;letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:15px;margin-left:0;
    text-align:left;">Dear <strong>' . $name . ',</strong><br/>New Project/Tender has been assign to you.</p>
								<h1 style=" color:#2e2e2e;display:block;font-family:Helvetica;font-size:26px;line-height:1.385em;
     font-style:normal;font-weight:normal;letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:15px;margin-left:0;
     text-align:left;"><strong>Project ID:' . $query[0]->generated_tenderid . '</strong></h1>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style=" border-left:1px solid #e2e2e2;
    border-right:1px solid #e2e2e2; border-bottom: 1px solid #f0f0f0; padding-bottom: 2.286em;">
					<tbody><tr>
						<td valign="top" style=" color:#505050;font-family:Helvetica;font-size:14px;line-height:150%;padding-top:3.143em;padding-right:3.5em;padding-left:3.5em; padding-bottom:0.714em;
     text-align:left;" mc:edit="body_content">
						<p style="color:#545454;   display:block;     font-family:Helvetica;     font-size:16px;     line-height:1.500em;     font-style:normal;
     font-weight:normal;     letter-spacing:normal;    margin-top:0;    margin-right:0;    margin-bottom:15px;    margin-left:0;    text-align:left;">
	 <strong>TenderDetails: </strong>' . $query[0]->TenderDetails . '<br/><strong>Organization: </strong>' . $query[0]->Organization . '<br/><strong>Location: </strong>' . $query[0]->Location . '</p>
	<p style="color:#545454;   display:block;     font-family:Helvetica;     font-size:16px;     line-height:1.500em;     font-style:normal;
     font-weight:normal;     letter-spacing:normal;    margin-top:0;    margin-right:0;    margin-bottom:15px;    margin-left:0;    text-align:left;">
	 Assignd By <br/> ' . $assignBy . '
	 </p>
	 <p style="color:#545454;   display:block;     font-family:Helvetica;     font-size:16px;     line-height:1.500em;     font-style:normal;
     font-weight:normal;     letter-spacing:normal;    margin-top:0;    margin-right:0;    margin-bottom:15px;    margin-left:0;    text-align:left;">
	 Regards <br/> TS Team
	 </p>
						</td>
						
						</tr>
					</tbody>
				</table>
						<!-- // END BODY -->
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>';
            $subject = 'New Project Assign ';
            //Static Email At time Project Manager Assign
            array_push($emailAddress, "harshita@cegindia.com");
            array_push($emailAddress, "v.sagar@cegindia.com");
            array_push($emailAddress, "ts.admin@cegindia.com");
            $emailArrS787 = implode(",", $emailAddress);
            sendMail("$emailArrS787", $subject, $message);
            //die;
        }
        if ($respon):
            $this->session->set_flashdata('msg', "Proposal Manager Assign on Tender Success.");
            redirect(base_url('/togoproject'));
        else:
            $this->session->set_flashdata('msg', "Something Went Wrong.");
            redirect(base_url('/togoproject'));
        endif;
        redirect(base_url('/togoproject'));
        die;
    }

    //Generate Project No..
    public function generate_project_no_old() {
        $formArrData = $_REQUEST;
        //Before Update Insert New ID..
        $insertedArr = array('project_id' => $formArrData['project_id'], 'generated_tenderid' => '', 'generate_type' => $formArrData['prefx_name'], 'generate_by' => $this->session->userdata('loginid'));
        $insertedID = $this->Front_model->insertRecord('tender_generated_id', $insertedArr);
        //After Insert Update ID In Same Table..
        if ($formArrData['prefx_name'] == 'E'):
            $last = $formArrData['E'];
        elseif ($formArrData['prefx_name'] == 'P'):
            $last = $formArrData['P'];
        elseif ($formArrData['prefx_name'] == 'FQ'):
            $last = $formArrData['FQ'];
        endif;
        //Codition for Financial Year..

        if (date('m') < 4):
            $generatedId = 'CEGHO/BD/' . (date('Y') - 1) . "-" . date('Y') . "/" . $formArrData['prefx_name'] . '0' . ($last + 1);
        else :
            $generatedId = 'CEGHO/BD/' . date('Y') . "-" . (date('Y') + 1) . "/" . $formArrData['prefx_name'] . '0' . ($last + 1);
        endif;
        $generate_id = '0' . ($last + 1);
        $respon = $this->Front_model->updateRecord('tender_generated_id', array('generated_tenderid' => $generatedId, 'generate_id' => $generate_id), array('fld_id' => $insertedID));
        // $generatedId = $formArrData['prefx_name'] . uniqid();
        $respon = $this->Front_model->updateRecord('proj_togo_for_user', array('projectgenid' => $generatedId), array('project_id' => $formArrData['project_id']));
        if ($respon):
            $res = $this->Front_model->updateRecord('projectno_prefx', array('last_generate_id' => ($last + 1)), array('prefix_id' => $formArrData['prefx_name']));
            $this->session->set_flashdata('msg', "TenderId Generated Success.");
            redirect(base_url('/togoproject'));
        else:
            redirect(base_url('/togoproject'));
        endif;
    }

    //For No Submitted Project set..
    public function projnosubmit() {
        $message = "Some issue occured !"; 
        if ($_REQUEST['actid']) {
            $businessID = $this->session->userdata('businessunit_id');
            $inserArr = array(
                'project_id' => $_REQUEST['actid'],
                'user_id' => $this->session->userdata('loginid'),
                'action_date' => date('Y-m-d H:i:s'),
                'actionIP' => get_client_ip(),
                'message' => 'No Submit Project'
            );
            $Respon = $this->Front_model->insertRecord('bdnotsubmit_project_byuser', $inserArr);
            $respp = $this->mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $_REQUEST['actid'], 'businessunit_id' => $businessID), array('visible_scope' => 'No_Submit_project'));
            if ($respp > 0):
                //$this->session->set_flashdata('msg', "Project Status  Changed No Submit.");
                $message = "Project Status  Changed No Submit."; 
            endif;
        }

        $arrMessage = array(
            'msg' => $message,
           
        );

        
 

        echo json_encode($arrMessage);



        /* if ($_REQUEST['actid']) {
          $businessID = $this->session->userdata('businessunit_id');
          $id = $_REQUEST['actid'];
          $data = $this->Front_model->getProjectDetail($id);
          if($data){
          $inserArr = array(
          'created_By' => $data->created_By,
          'tender24_ID' => $data->tender24_ID,
          'TenderDetails' => $data->TenderDetails,
          'Location' => $data->Location,
          'Organization' => $data->Organization,
          'Tender_url' => $data->Tender_url,
          'Sector_IDs' => $data->Sector_IDs,
          'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
          'Keyword_IDs' => $data->Keyword_IDs,
          'keyword_phase' => $data->keyword_phase,
          'Created_Date' => $data->Created_Date,
          'Expiry_Date' => $data->Expiry_Date,
          'source_file' => $data->source_file,
          'project_type' => $data->project_type,
          'country' => $data->country,
          'remarks' => $data->remarks,
          'national_intern' => $data->national_intern,
          'national_status' => $data->national_status,
          'parent_tbl_id' => $data->project_id,
          );

          $insertnogoArr = array(
          'project_id' => $data->project_id,
          'user_id' => $this->session->userdata('loginid'),
          'action_date' => date('Y-m-d H:i:s'),
          'actionIP' => get_client_ip(),
          'message'=>'No Submit Project'
          );

          $inserscopedumpArr = array(
          'project_id' => $data->project_id,
          'visible_scope' => 'No_Submit_project',
          'businessunit_id' => $data->businessunit_id
          );
          }
          $count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $id,'businessunit_id'=>$businessID));

          if ($count <= 5 AND $count > 1):
          $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
          $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
          $Respon2 = $this->Front_model->insertRecord('bdnotsubmit_project_byuser', $insertnogoArr);
          $res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID));
          else:
          $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
          $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
          $Respon2 = $this->Front_model->insertRecord('bdnotsubmit_project_byuser', $insertnogoArr);
          $res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID));
          $res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $id));
          endif;

          if ($Respon > 0):
          $this->session->set_flashdata('msg', "Project Status  Changed No Submit.");
          endif;
          } */

        /* if ($_REQUEST['actid']) {
          $inserArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $_REQUEST['actid'], 'actionIP' => get_client_ip());
          $Respon = $this->Front_model->insertRecord('proj_nosubmit_for_user', $inserArr);
          $this->visibilityStatus('No_Submit_project', $_REQUEST['actid']);
          $this->notified($_REQUEST['actid'], 'Tender Status Changed as No Submit.');
          $this->session->set_flashdata('msg', "Project Status  Changed No Submit . ");
          exit;
          // redirect(base_url('/togoproject'));
          } */
    }

    //For Project Bid..
    public function bidprojectbyuser() {

        $message = "Some issues occured";
        if ($_REQUEST['projid']) {
            $businessID = $this->session->userdata('businessunit_id');
            $inserArr = array(
                'project_id' => $_REQUEST['projid'],
                'user_id' => $this->session->userdata('loginid'),
                'action_date' => date('Y-m-d H:i:s'),
                'actionIP' => get_client_ip(),
                'message' => 'Bid Project'
            );
            $Respon = $this->Front_model->insertRecord('bdbid_project_byuser', $inserArr);
            $respp = $this->mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $_REQUEST['projid'], 'businessunit_id' => $businessID), array('visible_scope' => 'Bid_project'));


            //Cond 1..
            if ($_REQUEST['projid']):
                $numCount = $this->numrowproj($_REQUEST['projid']);
                if ($numCount < 1) {
                    $insrtArr = array(
                        "user_id" => $this->session->userdata('loginid'),
                        "project_id" => $_REQUEST['projid'],
                        "project_status" => "0",
                        "status_date" => date('Y-m-d'));
                    $this->db->insert("bdproject_status", $insrtArr);
                }
            endif;

            if($respp > 0){
                $message = "Project submitted successfully !";
            } 
    
    
           


        }

        $arrMessage = array(
            'msg' => $message,
           
        );

        echo json_encode($arrMessage);
    }

    //For No Go Project set..
    public function nogosubmitold() {
        if ($_REQUEST['actid']) {
            $businessID = $this->session->userdata('businessunit_id');
            $id = $_REQUEST['actid'];
            $data = $this->Front_model->getProjectDetail($id);
            //echo '<pre>'; print_r($data); 
            if ($data) {
                $inserArr = array(
                    'created_By' => $data->created_By,
                    'tender24_ID' => $data->tender24_ID,
                    'TenderDetails' => $data->TenderDetails,
                    'Location' => $data->Location,
                    'Organization' => $data->Organization,
                    'Tender_url' => $data->Tender_url,
                    'Sector_IDs' => $data->Sector_IDs,
                    'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
                    'Keyword_IDs' => $data->Keyword_IDs,
                    'keyword_phase' => $data->keyword_phase,
                    'Created_Date' => $data->Created_Date,
                    'Expiry_Date' => $data->Expiry_Date,
                    'source_file' => $data->source_file,
                    'project_type' => $data->project_type,
                    'country' => $data->country,
                    'remarks' => $data->remarks,
                    'national_intern' => $data->national_intern,
                    'national_status' => $data->national_status,
                    'parent_tbl_id' => $data->project_id,
                );

                $insertnogoArr = array(
                    'project_id' => $data->project_id,
                    'user_id' => $this->session->userdata('loginid'),
                    'action_date' => date('Y-m-d H:i:s'),
                    'actionIP' => get_client_ip(),
                    'message' => 'No Go Project'
                );

                $inserscopedumpArr = array(
                    'project_id' => $data->project_id,
                    'visible_scope' => 'No_Go_project',
                    'businessunit_id' => $data->businessunit_id
                );
            }
            $count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));

            if ($count <= 5 AND $count > 1):
                $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
                $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
                $Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnogoArr);
                $res = $this->db->delete('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));
            else:
                $Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
                $Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
                $Respon2 = $this->Front_model->insertRecord('bdnogo_project_byuser', $insertnogoArr);
                $res = $this->db->delete('bdtender_scope', array('project_id' => $id, 'businessunit_id' => $businessID));
                $res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $id));
            endif;

            if ($Respon > 0):
                $this->session->set_flashdata('msg', "Project Status  Changed No Go / Not Important. ");
            endif;
        }
    }

    public function notified($tendrId, $notifData) {
        $rowArrData = NotifiedUserIDs($this->session->userdata('loginid'));
        foreach ($rowArrData as $rowRec) {
            $insertArr = array('user_to_notify' => $rowRec, 'user_who_fired_event' => $this->session->userdata('loginid'),
                'event_id_project' => $tendrId, 'notification_data' => $notifData);
            $Record = $this->Front_model->insertRecord('notification', $insertArr);
        }
    }

    //Get Project Details By Id..
    public function ajax_projectdetailbyid() {
        $projid = $_REQUEST['projid'];
        $Record = $this->mastermodel->SelectRecordSingle('bd_tenderdetail', array('fld_id' => $projid));
        // $Record['TenderDetails'] = strip_tags($str);
        echo json_encode($Record);
        exit;
    }

    //New Function By Asheesh For Project Edit..
    public function editupdateproject() {
             

        // Array
        // (
        //     [tender_id] => 356828
        //     [projectName] => Request for Proposal for Appointment of Project Management Consultant(PMC) for Supervision and Monitoring of Multi- Modal Integration facilities implementation at 15 Stations of Mumbai Metro Line 2A & 7: Package 1
         
        
        //     [subm_date] => 2020-03-07
        //     [expr_date] => 2020-03-04
        //     [location] => Maharashtra
        //     [client_name] => CALLEEOB
        //     [funding_org] => 2
        //     [projid] => 
        //     [clientText] => custom text
        //     [fundingText] => ZZZ Fund Consulting
        // )
            // echo '<pre>';
            // print_r($_REQUEST);
         //   die;

        if ($_REQUEST['projectName'] and $_REQUEST['expr_date'] and $_REQUEST['location']){


            // check if client name already exist if not then save in client master

// echo "<pre>";
//             print_r($_REQUEST); die;

            if($_REQUEST['client_text'] !== ""){

            $qlClient = $this->db->select('*')->from('bd_client_master')->where('client_name', $_REQUEST['client_text'])->get();

           


            if( $qlClient->num_rows() > 0 ) {
                $clientId = $_REQUEST['client_name'];
                $clientVal = $_REQUEST['client_text'];
            
            } else {

                
                $clientData = array(
                   'client_name'=> $_REQUEST['client_text'],
                );
            
                $clientRes = $this->db->insert('bd_client_master',$clientData);
                 
                $clientId = $this->db->insert_id();

                // print_r($clientId); die;

                $clientVal = $_REQUEST['client_text'];
            }

        }

          

            if($_REQUEST['funding_org_text'] !== ""){

                $qlFunding = $this->db->select('*')->from('bd_funding_master')->where('funding_org', $_REQUEST['funding_org_text'])->get();

                if( $qlFunding->num_rows() > 0 ) {
                    $fundingId = $_REQUEST['funding_org'];
                    $fundingVal = $_REQUEST['funding_org_text'];
                
                } else {

                    
                    $fundData = array(
                       'funding_org'=> $_REQUEST['funding_org_text'],
                    );
                
                    $fundRes = $this->db->insert('bd_funding_master',$fundData);
                    $fundingId = $this->db->insert_id();
                    $fundingVal = $_REQUEST['funding_org_text'];
                }

            }


            // fetch its id else go $respon Organization client_id
            $Respon = $this->mastermodel->UpdateRecords('bd_tenderdetail', array('fld_id' => $_REQUEST['tender_id']), array('TenderDetails' => $this->input->post('projectName'), 'bid_validity_date' => $_REQUEST['expr_date'], 'Location' => $this->input->post('location'), 'client_id' => $clientId, 'Organization' => $clientVal, 'bd_funding_master_id' => $fundingId, 'bd_funding_master_org' => $fundingVal));
            //print_r($this->db->last_query()); die;
            
            
           
        }
        if($Respon > 0){
            $message = "Data updated successfully !";
        }else{
            $message = "Some issues occured";
        }


        $arrMessage = array(
            'msg' => $message,
            'id' => $clientId
        );

        echo json_encode($arrMessage);
    }

    public function insertproject() {
        //    echo '<pre>';
        //    print_r($_REQUEST);


//            Array
// (
//     [sectors_id] => 14 done
//     [services_id] => 2 done
//     [countries_id] => 2 done
//     [state_id] => 242 done
//     [bid_security] => 1 done
//     [bid_securitytype] => 5 done
//     [amount] => 12121 done
//     [bs_valdity] => 2020-03-06 done

        ##### Pending where to insert these values #####
//     [sf_bankname] => wqww
//     [sf_bgnumber] => wqwq
//     [sf_dateofstart] => 2020-03-05
        ##### Pending where to insert these values #####

//     [rfp_cost] => 1 done
//     [rfp_amount] => 1222 done
//     [rfp_type] => 5 done

##### Pending where to insert these values #####
//     [rfp_valdity] => 2020-03-07 
//     [rpf_bankname] => gfgfg
//     [rpf_bgnumber] => 3232232
//     [rpf_dateofstart] => 2020-03-05
##### Pending where to insert these values #####

//     [processing_fee] => 1 done
//     [pfee_amount] => 12121209 done
//     [processing_fee_type] => 5 done

##### Pending where to insert these values #####
//     [pf_valdity] => 2020-03-06
//     [pf_bankname] => wewe
//     [pf_bgnumber] => 121212
//     [pf_dateofstart] => 2020-03-06
##### Pending where to insert these values #####

//     [tender_openingdate] => 2020-03-07 done
//     [serviceprojid] => 340867 done
// )



// INSERT INTO `project_description` (`user_id`, `project_id`, `country_id`, `state_id`, `service_id`, `sector_id`, `bid_security`
// , `bid_securitytype`, `amount`
// , `bs_valdity`, `rfp_cost`, `rfp_amount`, `rfp_type`, `processing_fee`, `pfee_amount`, `processing_fee_type`, `tender_openingdate`) 
// VALUES ('296', '340867', '2', '242', '2', '14', '1', '5', '12121', '2020-03-06', '1', '1222', '5', '1', '12121209', '5', '2020-03-07')
         

        



        $inserArr = array(
            'user_id' => $this->session->userdata('loginid'),
            'project_id' => $_REQUEST['serviceprojid'],
            'country_id' => $_REQUEST['countries_id'],
            'state_id' => $_REQUEST['state_id'],
            'service_id' => $_REQUEST['services_id'],
            'sector_id' => $_REQUEST['sectors_id'],
            //BID Security..
            'bid_security' => $_REQUEST['bid_security'],
            'bid_securitytype' => $_REQUEST['bid_securitytype'],
            'amount' => $_REQUEST['amount'],
            'bs_valdity' => $_REQUEST['bs_valdity'],

             
            'sf_bankname' => $_REQUEST['sf_bankname'],
            'sf_bgnumber' => $_REQUEST['sf_bgnumber'],
            'sf_dateofstart' => $_REQUEST['sf_dateofstart'],
            



            'pf_valdity' => $_REQUEST['pf_valdity'],
            'pf_bankname' => $_REQUEST['pf_bankname'],
            'pf_bgnumber' => $_REQUEST['pf_bgnumber'],
            'pf_dateofstart' => $_REQUEST['pf_dateofstart'],


            


            //RFP..
            'rfp_cost' => $_REQUEST['rfp_cost'],
            'rfp_amount' => $_REQUEST['rfp_amount'],
            'rfp_type' => $_REQUEST['rfp_type'],


            'rfp_valdity' => $_REQUEST['rfp_valdity'],
            'rpf_bankname' => $_REQUEST['rpf_bankname'],
            'rpf_bgnumber' => $_REQUEST['rpf_bgnumber'],
            'rpf_dateofstart' => $_REQUEST['rpf_dateofstart'],

          


            //Processing Fee..
            'processing_fee' => $_REQUEST['processing_fee'],
            'pfee_amount' => $_REQUEST['pfee_amount'],
            'processing_fee_type' => $_REQUEST['processing_fee_type'],
            //Last Section..
            // 'submission_date' => $_REQUEST['submission_date'],
            'tender_openingdate' => $_REQUEST['tender_openingdate']);

            




            

            $ql = $this->db->select('*')->from('project_description')->where('project_id',$_REQUEST['serviceprojid'])->get();

            if( $ql->num_rows() > 0 ) {

                $updateArray = array_filter($inserArr);
                $Respon = $this->Front_model->updateRecord('project_description', $updateArray, array('project_id' => $_REQUEST['serviceprojid']));

            } else {
                $Respon = $this->Front_model->insertRecord('project_description', $inserArr);
            }

            // echo "<pre>";
            // print_r($this->db->last_query());
            
            
            
            // die;



            if($Respon > 0){
                $message = "Data updated successfully !";
            }else{
                $message = "Some issues occured";
            }


            $arrMessage = array(
                'msg' => $message,
            );

            echo json_encode($arrMessage);

        //redirect(base_url('/togoproject'));
    }

    public function clientinfo() {
    

//         Array
// (
//     [client_name] => qwqerer
//     [client_contact] => 
//     [client_address] => rerer
//     [project_id] => 357713
//     [sectionCounter] => 2
//     [clientName1] => erer
//     [clientDesignation1] => 
//     [clientEmail1] => re
//     [clientPhone1] => rere
//     [clientAddress1] => erer
//     [clientName2] => erre
//     [clientDesignation2] => 
//     [clientEmail2] => reerer
//     [clientPhone2] => erer
//     [clientAddress2] => erer
// )


           // echo '<pre>';
           // print_r($_REQUEST);
           // die;



         if($_REQUEST['client_contact_name_text'] !== ""){


                        $qlClient = $this->db->select('*')->from('bd_client_master')->where('client_name', $_REQUEST['client_contact_name_text'])->get();
                        if( $qlClient->num_rows() > 0 ) {
                        $clientId = $_REQUEST['client_contact_name'];
                        $clientVal = $_REQUEST['client_contact_name_text'];

                        } else {

                        $clientData = array(
                        'client_name'=> $_REQUEST['client_contact_name_text'],
                        );

                        $clientRes = $this->db->insert('bd_client_master',$clientData);
                        $clientId = $this->db->insert_id();
                        $clientVal = $_REQUEST['client_contact_name_text'];
                        }

                }
            
                
   
             

        if ($_REQUEST['client_contact_name_text'] !== "") {
            $_REQUEST['entryby'] = $this->session->userdata('loginid');

            $qlContact = $this->db->select('*')->from('clientcontact')->where('project_id', $_REQUEST['project_id'])->get();

            $preData = array(
                        
                'client_name'=>$_REQUEST['client_contact_name_text'],
                'project_id'=> $_REQUEST['project_id'],
                'client_contact'=> $_REQUEST['client_contact'],
                'client_address'=>$_REQUEST['client_address'],
                'entryby'=>$_REQUEST['entryby']
            );

            // echo "<pre/>";
            // print_r($_REQUEST);
            // die;

            if( $qlContact->num_rows() > 0 ) {

                $this->db->where('project_id', $_REQUEST['project_id']);
                $res = $this->db->update('clientcontact', $preData);
                // print_r($this->db->last_query()); die;
                $insert_id = $_REQUEST['project_id'];
             
            } else {
               
                $res = $this->db->insert('clientcontact',$preData);
                $insert_id = $this->db->insert_id();
            }
            
            
            
            // fetch its id else go $respon Organization client_id
            $updateMaster = $this->mastermodel->UpdateRecords('bd_tenderdetail', array('fld_id' => $_REQUEST['project_id']), 
            array('client_id' => $clientId, 'Organization' => $clientVal));
            //print_r($this->db->last_query()); die;
          

            //  $this->Front_model->insertRecord('clientcontact', $preData);
           

            //print_r($this->db->last_query());

            if($_REQUEST['sectionCounter'] > 0){
                
                // do a fresh entry each time so delete

                $this->db->delete('bd_client_detail', array('project_id' => $_REQUEST['project_id']));
              


                $i = 1;
                while($i <= $_REQUEST['counterTotal']){
                    
                     if(!empty($_REQUEST['clientName'.$i])){
                        $data = array(
                            // 'clientcontact_id'=> $insert_id,
                            'project_id'=> $_REQUEST['project_id'], 
                            'cdetail_name'=> $_REQUEST['clientName'.$i],
                            'cdetail_designation'=>$_REQUEST['clientDesignation'.$i],
                            'cdetail_email'=>$_REQUEST['clientEmail'.$i],
                            'cdetail_phone'=>$_REQUEST['clientPhone'.$i],
                            'cdetail_address'=>$_REQUEST['clientAddress'.$i]
                        );
                        $this->db->insert('bd_client_detail',$data);
                        
                     }
                        
                   
                    
                    
                    $i++;

                    //print_r($this->db->last_query());
                }

                


            }






        }




        
       if($res > 0){
            $message = "Client Added successfully !";
        }else{
            $message = "Some issues occured";
        }


        $arrMessage = array(
            'msg' => $message,
        );

        echo json_encode($arrMessage);
        
    }



    
    public function savedClientContact() {
        $client_Arr = $this->mastermodel->SelectRecord('bd_client_detail', array('project_id' => $_REQUEST['project_id']));
        
 
        if ($client_Arr) {
            echo json_encode($client_Arr);
        } else {
            return false;
        }
    }

    public function ajax_clientinfo() {
        $client_Arr = $this->mastermodel->SelectRecord('clientcontact', array('project_id' => $_REQUEST['project_id']));
        if ($client_Arr) {
            echo json_encode($client_Arr);
        } else {
            return false;
        }
    }

    public function ajax_clientinfodel() {
        $id = $this->uri->segment(3);
        if ($id) {
            $this->db->delete('clientcontact', array('id' => $id));
        }
        redirect(base_url('/togoproject'));
    }

    public function fqProjectAll() {
        $list = $this->Togoprojectfq_model->get_datatables();




        //echo "<pre>"; print_r($this->db->last_query()); die;

        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $assignTo = '';
        $view1 = '';
        $view2 = '';
        $view3 = '';
        //echo '<pre>'; print_r($list);
        foreach ($list as $togoproject) {
            $count = $this->mastermodel->count_rows('comment_on_project', array('project_id' => $togoproject->fld_id, 'is_active' => '1'));
            if ($count < 1):
                // $comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp;&nbsp;';
                $comment = '';
            else:
                $comment = '';
                // $comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>&nbsp;&nbsp;';
            endif;
            $exitproject = $this->Front_model->getProjectDetails($togoproject->fld_id);
            if (empty($togoproject->generated_tenderid)) {
                $genertedID = '<i id="faction" title="Project No. Generator" onclick="assignpm(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#generateprojid" class="glyphicon glyphicon-grain"></i>&nbsp;&nbsp;';
            } else {
                $genertedID = '';
            }
            if (empty($togoproject->assign_to)) {
                $assignTo = '<a href="#"><i id="faction" title="Set Proposal Manager" onclick="assignpm(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#assignmanager" class="glyphicon glyphicon-user icon-white"></i></a>&nbsp;&nbsp;';
            } else {
                $assignTo = '';
            }
            if ($bdRole == 1) {
                $view1 = '<div style="cursor:pointer" title="Edit Project" class="btn btn-success btn-sm " data-toggle="modal" data-target="#myModalbasic" onclick="editprojectdetail(' . "'" . $togoproject->fld_id . "'" . ",'" . $togoproject->Expiry_Date . "'" . ')"> <i class="fa fa-edit"></i></div>&nbsp;&nbsp;&nbsp;&nbsp;' .
                        $genertedID;
                if ($togoproject->generated_tenderid):
                    if ($togoproject->fqamountid) {
                        $view1 .= '&nbsp;&nbsp;<div style="cursor:pointer" title="Submitted" class="btn btn-info btn-sm "   onclick="bidproject(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')"><i class="fa fa-folder"></i></div>&nbsp;&nbsp;';
                    }
                endif;
            } elseif ($bdRole == 2) {
                $view2 = '<div style="cursor:pointer" title="Edit Project" class="btn btn-success btn-sm " data-toggle="modal" data-target="#myModalbasic" onclick="editprojectdetail(' . "'" . $togoproject->fld_id . "'" . ",'" . $togoproject->Expiry_Date . "'" . ')"> <i class="fa fa-edit"></i></div>&nbsp;&nbsp;&nbsp;&nbsp;' .
                        $genertedID;
                if ($togoproject->generated_tenderid):
                    if ($togoproject->fqamountid) {
                        $view2 .= '&nbsp;&nbsp;<div style="cursor:pointer" title="Submitted" class="btn btn-info btn-sm "   onclick="bidproject(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')"><i class="fa fa-folder"></i></div>&nbsp;&nbsp;';
                    }
                endif;
            } else {
                $view3 = '<div style="cursor:pointer" title="Edit Project" class="btn btn-success btn-sm " data-toggle="modal" data-target="#myModalbasic" onclick="editprojectdetail(' . "'" . $togoproject->fld_id . "'" . ",'" . $togoproject->Expiry_Date . "'" . ')"> <i class="fa fa-edit"></i></div>&nbsp;&nbsp;' .
                        '' .
                        $genertedID .
                        '&nbsp;&nbsp;<div style="cursor:pointer" class="btn btn-danger btn-sm " title="No Submitted" onclick="nosubmitproj(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')"> <i class="fa fa-trash"></i></div>&nbsp;&nbsp;' .
                        '';
                if ($togoproject->generated_tenderid):
                    if ($togoproject->fqamountid) {

                        $view3 .= '&nbsp;&nbsp;<div style="cursor:pointer" title="Submitted" class="btn btn-info btn-sm "   onclick="bidproject(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')"><i class="fa fa-folder"></i></div>&nbsp;&nbsp;';
                    }
                endif;
            }
            if (empty($exitproject)) {
                $projectfill = '<div style="cursor:pointer" title="Edit Project" class="btn btn-success btn-sm " data-toggle="modal" data-target="#myModalbasic" onclick="editprojectdetail(' . "'" . $togoproject->fld_id . "'" . ",'" . $togoproject->Expiry_Date . "'" . ')"> <i class="fa fa-edit"></i></div> 
                &nbsp;&nbsp;<div style="cursor:pointer" class="btn btn-danger btn-sm " title="No Submitted" onclick="nosubmitproj(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')"> <i class="fa fa-trash"></i></div>
                &nbsp;&nbsp;<a title="View Project Detail" class="btn btn-warning btn-sm " href="' . base_url('importantproject/projectdetail_view/' . $togoproject->fld_id) . '" target="_blank"><i class="fa fa-list"></i></a>
                ';
            } else {
                $projectfill = $view1 . $view2 . $view3 . $assignTo;
            }

            //12-02-2019.. Code By Asheesh For Edit...
            if ($togoproject->fqamountid == '') {
                $projectfill .= '&nbsp;&nbsp;<div style="cursor:pointer" data-toggle="modal" data-target="#fqamnt" title="Edit" onclick="editpbidproject(' . "'" . $togoproject->fld_id . "','" . 0 . "'" . ')" class="btn btn-info btn-sm"><i class="fa fa fa-dollar"></i></div>&nbsp;&nbsp;';
            }

            $user = $this->SecondDB_model->getUserByID($togoproject->assign_to);
            $detail = $togoproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $resull = array();
            $resull = explode("/", $togoproject->generated_tenderid);
            $row[] = ($togoproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($togoproject->Expiry_Date));
            $row[] = $resull[3];
            // &nbsp;&nbsp; <button onclick="addjvset(' . "'" . $togoproject->fld_id . "'" . ')" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModaljv" >Consortium</button> <li style="cursor:pointer" title="Edit Project" data-toggle="modal" data-target="#editproj" onclick="editproject(' . "'" . $togoproject->fld_id . "'" . ')" class="glyphicon glyphicon-edit"></li> 

            $row[] = ucFirst($togoproject->TenderDetails) . '<hr> <div class="highlight mb-2">' . $togoproject->generated_tenderid . '</div><div class="highlight">' . $user->userfullname . '</div> &nbsp; ' . '';
            $row[] = $togoproject->Location;
            // <li style="cursor:pointer" title="Add Client" data-toggle="modal" data-target="#clientmodel" onclick="editclientmodel(' . "'" . $togoproject->fld_id . "'" . ')" class="glyphicon glyphicon-edit"></li>
            $row[] = $togoproject->Organization . '&nbsp; ';
            // $row[] = $view1 . $view2 . $view3 . $assignTo;
            $row[] = $comment . $projectfill;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Togoprojectfq_model->count_filtered(),
            "recordsFiltered" => $this->Togoprojectfq_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function savefq_amountdetails() {
        if (count($_REQUEST) > 1) {
            $inserterArr = array('project_id_bd' => $_REQUEST['fqproject_id'], 'amont_details' => implode(",", $_REQUEST['amount_val']), 'status' => '1', 'entry_by' => $this->session->userdata('loginid'));
            $Rec = $this->Front_model->insertRecord('fqamount_details', $inserterArr);
            $this->session->set_flashdata('msg', "FQ Amount Details Inserted Success.");
        }
        redirect(base_url('togoproject/ongoingfqproject'));
    }

    public function numrowproj($projID) {
        // $projID = '372444';
        $this->db->select('a.id');
        $this->db->from('bdproject_status as a');
        $this->db->where('a.is_active', '1');
        $this->db->where('a.project_id', $projID);
        $ProjSres = $this->db->get()->num_rows();
        return $ProjSres;
    }

}
