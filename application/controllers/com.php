<?php
/* Database connection start */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Completeproject extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('Manmonth_model');
        $this->load->model('Completeproject_model', 'completeproject');
        $this->load->model('Cegeoiproject_model');
        $this->load->model('Cegrfpproject_model');
        $this->load->model('Cegfqproject_model');
        $this->load->model('Front_model');
        if (!($this->session->userdata('uid'))) {
            redirect(base_url());
        }
        $bduserid = $this->session->userdata('uid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";
        if ($actual_link == "http://bd.cegtechno.com") {
           if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):
            else:
                redirect(base_url('welcome/logout'));
            endif;
        }
    }

    
    public function index() {
		$title = 'Complete List Project';
        $sectorArr = $this->Front_model->GetActiveSector();
        $secId = '0';
        $compnameArr = array();
        $proposalManager = $this->Front_model->allprojectProposalManager();
        //Code By Asheesh..
        $Rec = $this->Front_model->selectRecord('main_company', array('fld_id', 'company_name'), array('status' => '1'));
        if ($Rec != '') {
            $compnameArr = $Rec->result();
        }
        $this->load->view('completeproject/tender_view', compact('title','secId', 'sectorArr', 'proposalManager', 'compnameArr'));
    }

    // Project Display
    public function newProjectAll() {
        $list = $this->completeproject->get_datatables();
        //echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $status = '';
        //echo '<pre>'; print_r($list);
        foreach ($list as $completeproject) {
            if ($completeproject->visible_scope == 'Bid_project') {
                if ($completeproject->project_status == 0) {
                    $status = 'Bid(Awaiting)';
                } else if ($completeproject->project_status == 1) {
                    $status = 'Bid(Won)';
                } else if ($completeproject->project_status == 2) {
                    $status = 'Bid(Loose)';
                }else if ($completeproject->project_status == 3) {
                    $status = 'Bid(Cancel)';
                }
            } elseif ($completeproject->visible_scope == 'No_Submit_project') {
                $status = 'No Submit';
            } elseif ($completeproject->visible_scope == 'To_Go_project') {
                $status = 'To Go';
            }
            $detail = $completeproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $resull = explode("/", $completeproject->generated_tenderid);
            $row[] = ($completeproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($completeproject->Expiry_Date));
            $row[] = $resull[3];
            $row[] = $completeproject->TenderDetails . '<hr><p><span style="float:left;">' . $completeproject->generated_tenderid . '</span><span style="float:right; color:green;">' . $completeproject->userfullname . '</span></p>';
            $row[] = $completeproject->Location;
            $row[] = $completeproject->Organization;
//            $row[] = '<button title="Add Comptitors" onclick="addcomptitors(' . "'" . $completeproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModalComptitors" class="btn btn-primary btn-xs"> Compititor </button> &nbsp <li data-toggle="modal" data-target="#myModalView" onclick="viewcomptitors(' . "'" . $completeproject->fld_id . "'" . ')" class="glyphicon glyphicon-eye-open icon-white btn"></li>';
			$row[] = '<li data-toggle="modal" data-target="#myModalView" onclick="viewcomptitors(' . "'" . $completeproject->fld_id . "'" . ')" class="glyphicon glyphicon-eye-open icon-white btn"></li>';
            // $row[] = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $activeproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp&nbsp'
            // . '<i style="cursor:pointer" title="Important" onclick="setimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-star icon-white"></i>&nbsp&nbsp' .
            // '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('activeproject/projurlview?projID=' . $activeproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp&nbsp'
            // . '<a  href="javascript:void(0)" title="Not Important" onclick="setnotimportant(' . "'" . $activeproject->fld_id . "','" . 0 . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a>' . '&nbsp;&nbsp;<input name="actchk[]" value="' . $activeproject->fld_id . '" type="checkbox">';

            $row[] = $status;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            //"recordsTotal" => $this->activeproject->count_all(),
            "recordsTotal" => '0',
            "recordsFiltered" => $this->completeproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
	
	
	
	//Eoi Project All
	public function cegeoiproject(){
		$title = 'EOI List Project';
		$sectorArr = $this->Front_model->GetActiveSector();
        $secId = '0';
        $compnameArr = array();
        $proposalManager = $this->Front_model->allprojectProposalManager();
        $Rec = $this->Front_model->selectRecord('main_company', array('fld_id', 'company_name'), array('status' => '1'));
        if ($Rec != '') {
            $compnameArr = $Rec->result();
        }
		$this->load->view('completeproject/eoi_view', compact('title','secId', 'sectorArr', 'proposalManager', 'compnameArr'));
	}
	
	public function cegeoiproject_ajax(){
		$list = $this->Cegeoiproject_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $status = '';
        foreach ($list as $res) {
            if ($res->visible_scope == 'Bid_project') {
                if ($res->project_status == 0) {
                    $status = 'Bid(Awaiting)';
                } else if ($res->project_status == 1) {
                    $status = 'Bid(Won)';
                } else if ($res->project_status == 2) {
                    $status = 'Bid(Loose)';
                }else if ($res->project_status == 3) {
                    $status = 'Bid(Cancel)';
                }
            } elseif ($res->visible_scope == 'No_Submit_project') {
                $status = 'No Submit';
            } elseif ($res->visible_scope == 'To_Go_project') {
                $status = 'To Go';
            }
            $detail = $res->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $resull = explode("/", $res->generated_tenderid);
            $row[] = ($res->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($res->Expiry_Date));
            $row[] = $resull[3];
            $row[] = $res->TenderDetails . '<hr><p><span style="float:left;">' . $res->generated_tenderid . '</span><span style="float:right; color:green;">' . $res->userfullname . '</span></p>';
            $row[] = $res->Location;
            $row[] = $res->Organization;
            //$row[] = '<button title="Add Comptitors" onclick="addcomptitors(' . "'" . $res->fld_id . "'" . ')" data-toggle="modal" data-target="#myModalComptitors" class="btn btn-primary btn-xs"> Compititor </button> &nbsp <li data-toggle="modal" data-target="#myModalView" onclick="viewcomptitors(' . "'" . $res->fld_id . "'" . ')" class="glyphicon glyphicon-eye-open icon-white btn"></li>';
            $row[] = '<li data-toggle="modal" data-target="#myModalView" onclick="viewcomptitors(' . "'" . $completeproject->fld_id . "'" . ')" class="glyphicon glyphicon-eye-open icon-white btn"></li>';
			$row[] = $status;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => '0',
            "recordsFiltered" => $this->Cegeoiproject_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	
	//RFP Project All
	public function cegrfpproject(){
		$title = 'RFP List Project';
		$sectorArr = $this->Front_model->GetActiveSector();
        $secId = '0';
        $compnameArr = array();
        $proposalManager = $this->Front_model->allprojectProposalManager();
        $Rec = $this->Front_model->selectRecord('main_company', array('fld_id', 'company_name'), array('status' => '1'));
        if ($Rec != '') {
            $compnameArr = $Rec->result();
        }
		$this->load->view('completeproject/rfp_view', compact('title','secId', 'sectorArr', 'proposalManager', 'compnameArr'));
	}
	
	public function cegrfpproject_ajax(){
		$list = $this->Cegrfpproject_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $status = '';
		
        foreach ($list as $res) {
            if ($res->visible_scope == 'Bid_project') {
                if ($res->project_status == 0) {
                    $status = 'Bid(Awaiting)';
                } else if ($res->project_status == 1) {
                    $status = 'Bid(Won)';
                } else if ($res->project_status == 2) {
                    $status = 'Bid(Loose)';
                } else if ($res->project_status == 3) {
                    $status = 'Bid(Cancel)';
                }
            } elseif ($res->visible_scope == 'No_Submit_project') {
                $status = 'No Submit';
            } elseif ($res->visible_scope == 'To_Go_project') {
                $status = 'To Go';
            }
            $detail = $res->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $resull = explode("/", $res->generated_tenderid);
            $row[] = ($res->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($res->Expiry_Date));
            $row[] = $resull[3];
            $row[] = $res->TenderDetails . '<hr><p><span style="float:left;">' . $res->generated_tenderid . '</span><span style="float:right; color:green;">' . $res->userfullname . '</span></p>';
            $row[] = $res->Location;
            $row[] = $res->Organization;
            //$row[] = '<button title="Add Comptitors" onclick="addcomptitors(' . "'" . $res->fld_id . "'" . ')" data-toggle="modal" data-target="#myModalComptitors" class="btn btn-primary btn-xs"> Compititor </button> &nbsp <li data-toggle="modal" data-target="#myModalView" onclick="viewcomptitors(' . "'" . $res->fld_id . "'" . ')" class="glyphicon glyphicon-eye-open icon-white btn"></li>';
            $row[] = '<li data-toggle="modal" data-target="#myModalView" onclick="viewcomptitors(' . "'" . $completeproject->fld_id . "'" . ')" class="glyphicon glyphicon-eye-open icon-white btn"></li>';
			$row[] = $status;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => '0',
            "recordsFiltered" => $this->Cegrfpproject_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	
	
	
	
	//FQ Project All
	public function cegfqproject(){
		$title = 'FQ List Project';
		$sectorArr = $this->Front_model->GetActiveSector();
        $secId = '0';
        $compnameArr = array();
        $proposalManager = $this->Front_model->allprojectProposalManager();
        $Rec = $this->Front_model->selectRecord('main_company', array('fld_id', 'company_name'), array('status' => '1'));
        if ($Rec != '') {
            $compnameArr = $Rec->result();
        }
		$this->load->view('completeproject/fq_view', compact('title','secId', 'sectorArr', 'proposalManager', 'compnameArr'));
	}
	
	public function cegfqproject_ajax(){
		$list = $this->Cegfqproject_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $status = '';
        foreach ($list as $res) {
            if ($res->visible_scope == 'Bid_project') {
                if ($res->project_status == 0) {
                    $status = 'Bid(Awaiting)';
                } else if ($res->project_status == 1) {
                    $status = 'Bid(Won)';
                } else if ($res->project_status == 2) {
                    $status = 'Bid(Loose)';
                } else if ($res->project_status == 3) {
                    $status = 'Bid(Cancel)';
                }
            } elseif ($res->visible_scope == 'No_Submit_project') {
                $status = 'No Submit';
            } elseif ($res->visible_scope == 'To_Go_project') {
                $status = 'To Go';
            }
            $detail = $res->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $resull = explode("/", $res->generated_tenderid);
            $row[] = ($res->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($res->Expiry_Date));
            $row[] = $resull[3];
            $row[] = $res->TenderDetails . '<hr><p><span style="float:left;">' . $res->generated_tenderid . '</span><span style="float:right; color:green;">' . $res->userfullname . '</span></p>';
            $row[] = $res->Location;
            $row[] = $res->Organization;
            //$row[] = '<button title="Add Comptitors" onclick="addcomptitors(' . "'" . $res->fld_id . "'" . ')" data-toggle="modal" data-target="#myModalComptitors" class="btn btn-primary btn-xs"> Compititor </button> &nbsp <li data-toggle="modal" data-target="#myModalView" onclick="viewcomptitors(' . "'" . $res->fld_id . "'" . ')" class="glyphicon glyphicon-eye-open icon-white btn"></li>';
            $row[] = '<li data-toggle="modal" data-target="#myModalView" onclick="viewcomptitors(' . "'" . $completeproject->fld_id . "'" . ')" class="glyphicon glyphicon-eye-open icon-white btn"></li>';
			$row[] = $status;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => '0',
            "recordsFiltered" => $this->Cegfqproject_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	
	
	
    //Link View Details Popup Open Ash..
    public function projurlview() {
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('bd_tenderdetail', array('Tender_url'), array('fld_id' => $projId));
        if ($Rec) {
            $RowDataArr = $Rec->row();
        }
        $hyperlink = ($RowDataArr->Tender_url) ? $RowDataArr->Tender_url : 'http://www.cegindia.com/';
        $this->load->view('welcome_link', compact('projId', 'hyperlink'));
    }

    //Visibility Status
    public function visibilityStatus($copeName, $actid) {
        return $this->Front_model->updateRecord('bd_tenderdetail', array('visible_scope' => $copeName), array('fld_id' => $actid));
    }

    //Comment Showe
    public function commentset() {
        $tndrid = $_REQUEST['tndrid'];
        if (isset($tndrid)) {
            $Rec = $this->Front_model->selectRecordOrderByASC('comment_on_project', array('*'), array('is_active' => '1', 'project_id' => $tndrid));
            if ($Rec) {
                $data['allcomments'] = $Rec->result();
            }
            $this->load->view('comment_view', $data);
        }
    }

    //Comment Submit..
    public function commentsubmit() {
        $projId = $_REQUEST['projId'];
        $comnt = $_REQUEST['comnt'];
        if ($comnt):
            $insertArr = array('user_id' => $this->session->userdata('uid'), 'project_id' => $projId, 'sectorId' => '0', 'commentText' => $comnt);
            $Rec = $this->Front_model->insertRecord('comment_on_project', $insertArr);
            $this->notified($projId, 'A new Comment on Tender.');
        endif;
        return $Rec;
    }

    //Submit Project Concern Details..
    public function projectconcerndetails() {
        //Get Project Id..
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('project_concern_details', array('concern_details'), array('project_id' => $projId));
        if ($Rec != '') {
            $data = $Rec->result();
        } else {
            $data[0] = '';
        }
        $this->load->view('welcome_message', compact('projId', 'data'));
    }

    //For No Go Project set..
    public function nogosubmit() {
        if ($_REQUEST['actid']) {
            $inserArr = array('user_id' => $this->session->userdata('uid'), 'project_id' => $_REQUEST['actid'], 'actionIP' => get_client_ip());
            $Respon = $this->Front_model->insertRecord('proj_nogo_for_user', $inserArr);
        }
        if ($Respon > 0):
            $this->visibilityStatus('No_Go_project', $_REQUEST['actid']);
            $this->notified($_REQUEST['actid'], 'Tender Status Changed as No Go.');
            $this->session->set_flashdata('msg', "Project Status  Changed No Go . ");
            redirect(base_url('/togoproject'));
        endif;
    }

    public function notified($tendrId, $notifData) {
        $rowArrData = NotifiedUserIDs($this->session->userdata('uid'));
        foreach ($rowArrData as $rowRec) {
            $insertArr = array('user_to_notify' => $rowRec, 'user_who_fired_event' => $this->session->userdata('uid'),
                'event_id_project' => $tendrId, 'notification_data' => $notifData);
            $Record = $this->Front_model->insertRecord('notification', $insertArr);
        }
    }

}

