<?php

/* Database connection start */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Bidnewreport extends CI_Controller {

    function __construct() {
        parent::__construct();
//$this->load->model('Manmonth_model');
        $this->load->model('mastermodel');
        $this->load->model('Bidproject_model', 'bidproject');
        $this->load->model('Front_model');
        $this->load->model('SecondDB_model');
        $this->load->model('Bidprojecteoi_model');
        $this->load->model('Bidprojectfq_model');
        $this->load->model('Bidprojectrfp_model');
        if (!($this->session->userdata('uid'))) {
            redirect(base_url());
        }
        $bduserid = $this->session->userdata('uid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";
        if ($actual_link == "http://bd.cegtechno.com") {
           if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):
            else:
                redirect(base_url('welcome/logout'));
            endif;
        }

        // global $otherdb;

        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    public function index() {
        $title = 'Bid Project New Report';
        $otherdb = $this->load->database('another_db', TRUE);

        $otherdb->select('id,project_name');
        $otherdb->from('tm_projects');
        $otherdb->Where(array('is_active' => '1', 'project_type' => 'billable'));
        $otherdb->order_by('project_name', 'ASC');
        $AllprojArr = $otherdb->get()->result();

        $this->load->view('bidproject/bidnewreport_view', compact('title', 'AllprojArr'));
    }

    
    public function vacantpositionreport() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $month = '11';
        $year = '2018';
        $resultMonthofAttend = 0;
        $title = 'Vacant Position Report';
        $otherdb = $this->load->database('another_db', TRUE);
        $otherdb->select('id');
        $otherdb->from('tm_projects');
        $otherdb->Where(array('is_active' => '1', 'project_type' => 'billable'));
        $otherdb->order_by('project_name', 'ASC');
        $AllprojArr = $otherdb->get()->result();


        if (($AllprojArr) and ( $month) and ( $year)) {
            foreach ($AllprojArr as $mprojR) {
                $projID = $mprojR->id;
                $bdProjId = getprojectid_tstobd($projID);

                if ($bdProjId) {
                    $this->db->select("$db1.assign_finalteam.designation_id,$db1.assign_finalteam.empname,$db1.designation_master_requisition.designation_name,$db2.main_users.userfullname,$db2.tm_project_employees.is_intermittent,$db1.accountinfo.no_days,$db1.accountinfo.decimal_place");
                    $this->db->from("$db1.assign_finalteam");
                    $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id=$db1.designation_master_requisition.fld_id", "LEFT");
                    $this->db->join("$db2.main_users", "$db1.assign_finalteam.empname=$db2.main_users.id", "LEFT");
                    $this->db->join("$db2.tm_project_employees", "($db1.assign_finalteam.empname=$db2.tm_project_employees.emp_id AND $db2.tm_project_employees.project_id=$projID)", "LEFT");
                    $this->db->join("$db1.accountinfo", "($db1.accountinfo.project_id=$bdProjId)", "LEFT");
                    $this->db->Where(array("$db1.assign_finalteam.project_id" => $bdProjId));
                    $this->db->Where("$db1.assign_finalteam.empname>", "0");
                    // $this->db->Limit("1");
                    $AllrecDetailsArr = $this->db->get()->result();


                    if ($AllrecDetailsArr) {
                        foreach ($AllrecDetailsArr as $rOwR) {
                            $varis_intermittent = $rOwR->is_intermittent;
                            $designation_id = $rOwR->designation_id;
                            $empid = $rOwR->empname;
                            $Month_no_days = $rOwR->no_days;
                            $decimal_place = $rOwR->decimal_place;
                            if ($Month_no_days < 1):
                                $Month_no_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                            endif;


                            if (($designation_id) and ( $empid)) {
                                //in Case of Intermitent..
                                if ($varis_intermittent == '1') {
                                    $SingleUsrArr = $this->Front_model->gettimesheetdetail($bdProjId, $designation_id, $empid, $month, $year);
                                    $SingleUsrArremp_id = $SingleUsrArr->emp_id;
                                    $SingleUsrArrleave = $SingleUsrArr->leave;
                                    $SingleUsrArrabsent = $SingleUsrArr->absent;
                                    $SingleUsrArrpresent = $SingleUsrArr->present;
                                    $resultMonthofAttend = ($SingleUsrArrpresent / $Month_no_days);

                                    $resultMonthofAttend = number_format((float) $resultMonthofAttend, $decimal_place, '.', '');
                                }
                                //in Case of Not Intermitent..
                                if ($varis_intermittent != '1') {
                                    $SingleUsrArr = $this->Front_model->getuseattd($bdProjId, $empid, $designation_id, $month, $year);
                                    $SingleUsrArremp_id = $SingleUsrArr->emp_id;
                                    $SingleUsrArrleave = $SingleUsrArr->leave;
                                    $SingleUsrArrabsent = $SingleUsrArr->absent;
                                    $SingleUsrArrpresent = $SingleUsrArr->present;
                                    $A11 = ($SingleUsrArrleave + $SingleUsrArrabsent);
                                    $resultMonthofAttend = ($A11 / $Month_no_days);
                                    $resultMonthofAttend = (1 - $resultMonthofAttend);
                                    $resultMonthofAttend = number_format((float) $resultMonthofAttend, $decimal_place, '.', '');
                                }

                                //Insert Into Crwn Table..
                                $inserterArr = array('bd_projId' => $bdProjId,
                                    'ts_projid' => $projID,
                                    'emp_id' => $empid,
                                    'designation_id' => $designation_id,
                                    'month' => $month,
                                    'year' => $year,
                                    'current_mm' => $resultMonthofAttend,
                                    'entry_by' => $this->session->userdata('uid'));

                                //Chk Existance ...
                                $whereCondArr = array("$db1.siteofc_attend_invcrwn.bd_projId" => $bdProjId,
                                    "$db1.siteofc_attend_invcrwn.ts_projid" => $projID,
                                    "$db1.siteofc_attend_invcrwn.emp_id" => $empid,
                                    "$db1.siteofc_attend_invcrwn.designation_id" => $designation_id,
                                    "$db1.siteofc_attend_invcrwn.month" => $month,
                                    "$db1.siteofc_attend_invcrwn.year" => $year);
                                $this->db->where($whereCondArr);

                                $nuM1 = $this->db->count_all_results("$db1.siteofc_attend_invcrwn");
                                if ($nuM1 > 0) {
                                    $updRecds = array("$db1.siteofc_attend_invcrwn.current_mm" => $resultMonthofAttend,
                                        "$db1.siteofc_attend_invcrwn.entry_by" => $this->session->userdata('uid'));
                                    $this->db->where($whereCondArr);
                                    $this->db->update("$db1.siteofc_attend_invcrwn", $updRecds);
                                } else {
                                    $this->db->insert("$db1.siteofc_attend_invcrwn", $inserterArr);
                                }
                            }
                        }
                    }
                }
            }
        }

       // $this->load->view('bidproject/vacantpositionreport_view', compact('title', 'AllprojArr'));
    }

    public function getinvc() {
        $otherdb = $this->load->database('another_db', TRUE);
        $onlinedb = $this->load->database('online', TRUE);

        $projectTm = '81';

        $onlinedb->select('a.project_numberid as tmprojid,a.project_id,b.TenderDetails');
        $onlinedb->from('accountinfo as a');
        $onlinedb->join("bd_tenderdetail as b", "a.project_id=b.fld_id", "LEFT");

        $onlinedb->Where(array('a.project_numberid' => $projectTm));
        $onlinedb->group_by('a.project_numberid');
        $AllprojArr = $onlinedb->get()->result();

        echo '<pre>';
        print_r($AllprojArr);
        die;
    }

}
