<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Searchprojectcru extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('Manmonth_model');
        $this->load->model('mastermodel');
        $this->load->model('Front_model');
        $this->load->model('Searchprojectcru_model','searchproject');
        $this->load->model('Searchempcru_model','searchemp');
        if (!($this->session->userdata('uid'))) {
            redirect(base_url());
        }
        $bduserid = $this->session->userdata('uid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";
        if ($actual_link == "http://bd.cegtechno.com") {
           if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):
            else:
                redirect(base_url('welcome/logout'));
            endif;
        }
    }

    //Dashbord After Login..
    public function index() {
		$title = 'Search Project';
		$sectorArr = $this->Front_model->GetActiveSector();
        $serviceArr = $this->Front_model->mainServices();
		$country_Arr = $this->mastermodel->SelectRecordC('countries', array('status' => '1'));
		
        $this->load->view('searchingcru/tender_view', compact('title','sectorArr','serviceArr','country_Arr'));
    }

    // Project Display
    public function newProjectAll() {
        $list = $this->searchproject->get_datatables();
        //echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $status = '';
        //echo '<pre>'; print_r($list);
		
        foreach ($list as $togoproject) {
			if($togoproject->visible_scope == 'To_Go_project'){
				$status = 'Ongoing Bidding';
			}else{
				if ($togoproject->project_status == 0) {
					$status = 'Submitted (Awaiting)';
				} else if ($togoproject->project_status == 1) {
					$status = 'Submitted (Won)';
				} else if ($togoproject->project_status == 2) {
					$status = 'Submitted (Loose)';
				} else if ($togoproject->project_status == 3) {
					$status = 'Submitted (Cancel)';
				} 	
			}
            

			$no++;
            $row = array();
            $row[] = $no;
            $row[] = $togoproject->TenderDetails ;
            $row[] = $status;
            $row[] = $togoproject->sectName;
            $row[] = $togoproject->service_name;
            $row[] = $togoproject->state_name .' '.$togoproject->country_name;
            $row[] = '<a style="cursor:pointer" title="Edit Project" data-toggle="modal" data-target="#myProjedit" onclick="editprojectdetail(' . "'" . $togoproject->fld_id . "'" . ')">View</a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->searchproject->count_all(),
            "recordsFiltered" => $this->searchproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
		
	//getteambyprojectid
	public function getteambyprojectid(){
		if(!empty($_REQUEST['projid'])){
			$projid = $_REQUEST['projid'];
			$result = $this->Front_model->getteambyprojectid($projid);
			echo json_encode($result); 
		}
	}
	
	
	public function searchempcru(){
		$title = 'Search By Employee';
		$userdata = $this->Front_model->getTeamassign();
		$designation = $this->Front_model->getAlldesignation();
		//echo '<pre>'; print_r($userdata);
		$this->load->view('searchingcru/emp_view', compact('title','userdata','designation'));
	}
	
	public function newempAll() {
        $list = $this->searchemp->get_datatables();
        //echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $status = '';
        //echo '<pre>'; print_r($list);
		
        foreach ($list as $togoproject) {
			if(!empty($togoproject->emp_name) || !empty($togoproject->userfullname)){
				if(!empty($togoproject->userfullname)){
					$empname = $togoproject->userfullname;
					$empid = $togoproject->empname;
					$empstatus = 1;
				}else{
					$empname = $togoproject->emp_name;
					$empid = $togoproject->empnameother;
					$empstatus = 2;
				}
				
				if(!empty($togoproject->emailaddress)){
					$emailaddress = $togoproject->emailaddress;
				}else{
					$emailaddress = $togoproject->emp_email;
				}
				
				if(!empty($togoproject->contactnumber)){
					$contactnumber = $togoproject->contactnumber;
				}else{
					$contactnumber = $togoproject->emp_contact;
				}
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $empname ;
				$row[] = $emailaddress;
				$row[] = $contactnumber;
				$row[] = $togoproject->marks;
				$row[] = $togoproject->firm;
				$row[] = $togoproject->designation_name;
				$row[] = '<a style="cursor:pointer" title="Edit Project" data-toggle="modal" data-target="#myProjedit" onclick="editprojectdetail(' . "'" . $empid . "'" . ",'" . $empstatus . "'" . ')">View</a>';
				$data[] = $row;
			}
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->searchemp->count_all(),
            "recordsFiltered" => $this->searchemp->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
	
	public function getprojectbyempid(){
		if(!empty($_REQUEST['empid'])){
			$userID = $_REQUEST['empid'];
			$empstatus = $_REQUEST['empstatus'];
			$result = $this->Front_model->getprojectbyempid($userID,$empstatus);
			//echo '<pre>'; print_r($result); die;
			echo json_encode($result);
			/* $this->db->select('b.TenderDetails,c.designation_name,a.marks,f.project_status,f.project_id');
			$this->db->from('team_assign_member as a');
			$this->db->join('bd_tenderdetails as b','a.project_id = b.fld_id','left');
			$this->db->join('designation_master_requisition as c','a.designation_id = c.fld_id','left');
			$this->db->join('bid_project as f','a.project_id = f.project_id','left');
			$this->db->where('a.status','1');
			$res = $this->db->get()->result_array();
			echo json_encode($res); */
		}
	}
}
