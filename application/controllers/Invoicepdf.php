<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Invoicepdf extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('mp_pdf');
    }

    public function invoice_pdf() { //echo "rtye"; die;
        $data = [];
        $html = $this->load->view('invoice/invoice_report_view', $data, true);
        $html2 = $this->load->view('invoice/invoice_report_view2', $data, true);
        $pdfFilePath = "invoice_report.pdf";
        $margin_left = 2;
        $margin_right = 2;
        $margin_top = 5;
        $margin_bottom = 3;
        $this->mp_pdf->pdf->mPDF('', '', '', '', $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer, '');
        $this->mp_pdf->pdf->mirrorMargins = 1;
        ob_clean();
        ob_flush();
        $this->mp_pdf->pdf->WriteHTML($html);
        $this->mp_pdf->pdf->SetDisplayMode('fullwidth');
        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($html2);
        //$this->m_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->Output($pdfFilePath, "I");
        ob_end_flush();
        ob_end_clean();
    }

}

?>