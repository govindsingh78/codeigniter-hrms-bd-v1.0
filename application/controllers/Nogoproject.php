<?php

/* Database connection start */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Nogoproject extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('Manmonth_model');
        $this->load->model('Nogoproject_model', 'nogoproject');
        $this->load->model('Front_model');
        if (!($this->session->userdata('uid'))) {
            redirect(base_url());
        }

        $bduserid = $this->session->userdata('uid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";

        if ($actual_link == "http://bd.cegtechno.com") {
           if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):

            else:
                redirect(base_url('welcome/logout'));
            endif;
        }
    }

    //Dashbord After Login..
    public function index() {
		$title = 'NoGo Project';
        $sectorArr = $this->GetActiveSector();
        $secId = '0';
        $this->load->view('nogoproject/tender_view', compact('secId', 'sectorArr','title'));
    }

    // Project Display
    public function newProjectAll() {

        $list = $this->nogoproject->get_datatables();
		$data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $view = '';
        $action = '';
        foreach ($list as $nogoproject) {
            if ($bdRole == 1 || $bdRole == 2) {
                $view = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $nogoproject->parent_tbl_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp&nbsp' .
                        '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('nogoproject/projurlview?projID=' . $nogoproject->parent_tbl_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp&nbsp';
            } else {
                $action = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $nogoproject->parent_tbl_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp&nbsp'
                        . '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('nogoproject/projurlview?projID=' . $nogoproject->parent_tbl_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp&nbsp'
                        . '<span style="cursor:pointer" onclick="window.open(' . "'" . base_url('nogoproject/projectconcerndetails?projID=' . $nogoproject->parent_tbl_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" >Details</span>&nbsp&nbsp'
                ;
            }
            $detail = $nogoproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;

            $row[] = ($nogoproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($nogoproject->Expiry_Date));
            $row[] = $nogoproject->TenderDetails;
            $row[] = $nogoproject->Location;
            $row[] = $nogoproject->Organization;

            $row[] = $view . $action;



            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            //"recordsTotal" => $this->activeproject->count_all(),
            "recordsTotal" => '0',
            "recordsFiltered" => $this->nogoproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    //Get Active Sector..
    public function GetActiveSector() {
        //$this->load->model('Front_model');
        $Rec = $this->Front_model->selectRecord('sector', array('*'), array('is_active' => '1'));
        if ($Rec) {
            return $Rec->result();
        } else {
            return false;
        }
    }

    //Link View Details Popup Open Ash..
    public function projurlview() {
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('bd_tenderdetail', array('Tender_url'), array('fld_id' => $projId));
        if ($Rec) {
            $RowDataArr = $Rec->row();
        }
        $hyperlink = ($RowDataArr->Tender_url) ? $RowDataArr->Tender_url : 'http://www.cegindia.com/';
        $this->load->view('welcome_link', compact('projId', 'hyperlink'));
    }

    //Visibility Status
    public function visibilityStatus($copeName, $actid) {
        return $this->Front_model->updateRecord('bd_tenderdetail', array('visible_scope' => $copeName), array('fld_id' => $actid));
    }

    //Comment Showe
    public function commentset() {
        $tndrid = $_REQUEST['tndrid'];
        if (isset($tndrid)) {
            $Rec = $this->Front_model->selectRecordOrderByASC('comment_on_project', array('*'), array('is_active' => '1', 'project_id' => $tndrid));
            if ($Rec) {
                $data['allcomments'] = $Rec->result();
            }
            $this->load->view('comment_view', $data);
        }
    }

    //Comment Submit..
    public function commentsubmit() {
        $projId = $_REQUEST['projId'];
        $comnt = $_REQUEST['comnt'];
        if ($comnt):
            $insertArr = array('user_id' => $this->session->userdata('uid'), 'project_id' => $projId, 'sectorId' => '0', 'commentText' => $comnt);
            $Rec = $this->Front_model->insertRecord('comment_on_project', $insertArr);
            $this->notified($projId, 'A new Comment on Tender.');
        endif;
        return $Rec;
    }

    //Submit Project Concern Details..
    public function projectconcerndetails() {
        //Get Project Id..
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('project_concern_details', array('concern_details'), array('project_id' => $projId));
        if ($Rec != '') {
            $data = $Rec->result();
        } else {
            $data[0] = '';
        }
        $this->load->view('welcome_message', compact('projId', 'data'));
    }

    public function notified($tendrId, $notifData) {
        $rowArrData = NotifiedUserIDs($this->session->userdata('uid'));
        foreach ($rowArrData as $rowRec) {
            $insertArr = array('user_to_notify' => $rowRec, 'user_who_fired_event' => $this->session->userdata('uid'),
                'event_id_project' => $tendrId, 'notification_data' => $notifData);
            $Record = $this->Front_model->insertRecord('notification', $insertArr);
        }
    }

}
