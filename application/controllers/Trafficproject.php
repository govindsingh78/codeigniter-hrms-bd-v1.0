<?php

/* Database connection start */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Trafficproject extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('Manmonth_model');
        $this->load->model('Trafficproject_model', 'trafficproject');
        $this->load->model('Front_model');
		$this->load->model('SecondDB_model');
		$this->load->helper(array('form', 'url','user_helper'));
        $this->load->model('Mastermodel');
        if (!($this->session->userdata('uid'))) {
            redirect(base_url());
        }

        $bduserid = $this->session->userdata('uid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";

        if ($actual_link == "http://bd.cegtechno.com") {
           if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):

            else:
                redirect(base_url('welcome/logout'));
            endif;
        }
    }

    //Dashbord After Login..
    public function index() { 
		$title = 'Traffic Project';
        $sectorArr = $this->Front_model->GetActiveSector();
        $secId = '0';
        $this->load->view('trafficproject/tender_view', compact('secId', 'sectorArr','title'));
    }

    // Project Display
    public function newProjectAll() {
		$list = $this->trafficproject->get_datatables();
		//echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $trafficproject) {
			$count = $this->Mastermodel->count_rows('comment_on_project', array('project_id' => $trafficproject->fld_id,'is_active'=>'1'));
			if ($count < 1):
				$comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $trafficproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white"></i>&nbsp&nbsp';
			else:
				$comment = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $trafficproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>&nbsp&nbsp';
			endif;
			
            $detail = $trafficproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;

            $row[] = ($trafficproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($trafficproject->Expiry_Date));
            //$row[] = "<small>" . str_replace("_x000D_", "<br>", strip_tags($newproject->TenderDetails)) . "</small>";
            // $row[] = $newproject->Expiry_Date;
            $row[] = $trafficproject->TenderDetails;
            $row[] = $trafficproject->Location;
            $row[] = $trafficproject->Organization;
            $row[] = $comment . '<i style="cursor:pointer" title="Active" onclick="activethisrecord(' . "'" . $trafficproject->fld_id . "','" . 0 . "'" . ')" class="glyphicon glyphicon-ok"></i>&nbsp&nbsp' .
                    '<i title="View link" style="cursor:pointer" onclick="window.open(' . "'" . base_url('trafficproject/projurlview?projID=' . $trafficproject->fld_id) . "', '', 'width=600 height=400 left=250 top=150'" . ')" class="glyphicon glyphicon-eye-open icon-white"></i>&nbsp&nbsp'
                    . '<a  href="javascript:void(0)" title="InActive/Delete" onclick="deletethisrecord(' . "'" . $trafficproject->fld_id . "','" . 0 . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a>' . '&nbsp;&nbsp;<input name="actchk[]" value="' . $trafficproject->fld_id . '" type="checkbox">';



            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            //"recordsTotal" => $this->activeproject->count_all(),
            "recordsTotal" => '0',
            "recordsFiltered" => $this->trafficproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    

    //Link View Details Popup Open Ash..
    public function projurlview() {
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('bd_tenderdetail', array('Tender_url'), array('fld_id' => $projId));
        if ($Rec) {
            $RowDataArr = $Rec->row();
        }
        $hyperlink = ($RowDataArr->Tender_url) ? $RowDataArr->Tender_url : 'http://www.cegindia.com/';
        $this->load->view('welcome_link', compact('projId', 'hyperlink'));
    }

    //InActive Project ,,,
    public function gototrashbyuser() {
		if ($_REQUEST['delid']) {
			$businessID = $this->session->userdata('businessunit_id');
			$id = $_REQUEST['delid'];
			$data = $this->Front_model->getProjectDetail($id);
			//echo '<pre>'; print_r($data); 
			if($data){
				$inserArr = array(
					'created_By' => $data->created_By,
					'tender24_ID' => $data->tender24_ID,
					'TenderDetails' => $data->TenderDetails,
					'Location' => $data->Location,
					'Organization' => $data->Organization,
					'Tender_url' => $data->Tender_url,
					'Sector_IDs' => $data->Sector_IDs,
					'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
					'Keyword_IDs' => $data->Keyword_IDs,
					'keyword_phase' => $data->keyword_phase,
					'Created_Date' => $data->Created_Date,
					'Expiry_Date' => $data->Expiry_Date,
					'source_file' => $data->source_file,
					'project_type' => $data->project_type,
					'country' => $data->country,
					'remarks' => $data->remarks,
					'national_intern' => $data->national_intern,
					'national_status' => $data->national_status,
					'parent_tbl_id' => $data->project_id,
				);
				
				$inserinactiveArr = array(
					'project_id' => $data->project_id,
					'user_id' => $this->session->userdata('uid'),
					'action_date' => date('Y-m-d H:i:s'),
					'actionIP' => get_client_ip(),
					'message'=>'InActive Project'
				);
				
				$inserscopedumpArr = array(
					'project_id' => $data->project_id,
					'visible_scope' => 'InActive_project',
					'businessunit_id' => $data->businessunit_id
				);
			}
			$count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $_REQUEST['delid'],'businessunit_id'=>$businessID));
			
			if ($count <= 5 AND $count > 1):
				$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
				$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
				$Respon2 = $this->Front_model->insertRecord('bdinactive_project_byuser', $inserinactiveArr);
				$res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID));
			else: 	
				$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
				$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
				$Respon2 = $this->Front_model->insertRecord('bdinactive_project_byuser', $inserinactiveArr);
				$res = $this->db->delete('bdtender_scope', array('project_id' => $id ,'businessunit_id' => $businessID)); 
				$res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $id)); 
			endif;
			
			if ($Respon > 0):
				$this->session->set_flashdata('msg', "Tender Inactivated.");
			endif;
		}
		/* if ($Respon > 0):
            $this->session->set_flashdata('msg', "Tender Inactivated.");
        endif; */
        /* if ($_REQUEST['delid']) {
            $inserArr = array(
                'user_id' => $this->session->userdata('uid'),
                'project_id' => $_REQUEST['delid'],
                'actionIP' => get_client_ip());
            $Respon = $this->Front_model->insertRecord('trash_for_user', $inserArr);
        }
        if ($Respon > 0):
            //Visible Activities Change..
            $this->visibilityStatus('InActive_project', $_REQUEST['delid']);
            //Notification Status..
            $this->notified($_REQUEST['delid'], 'Tender Deactivated.');
            $this->session->set_flashdata('msg', "Tender Inactivated.");
        endif; */
    }

    //Active Project By User...
    public function activeproject() {
		//echo '<pre>'; print_r($_REQUEST);
		if ($_REQUEST['actid']) {
			$businessID = $this->session->userdata('businessunit_id');
			$inserArr = array(
                'project_id' => $_REQUEST['actid'],
				'user_id' => $this->session->userdata('uid'),
                'action_date' => date('Y-m-d H:i:s'),
				'actionIP' => get_client_ip(),
				'message'=>'Active Project'
			);
			
            $Respon = $this->Front_model->insertRecord('bdactive_project_byuser', $inserArr);
			$respp = $this->Mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $_REQUEST['actid'],'businessunit_id'=>$businessID), array('visible_scope' => 'Active_project'));
        }
		if ($Respon > 0):
            //$this->visibilityStatus('Active_project', $_REQUEST['actid']);
            //$this->notified($_REQUEST['actid'], 'Tender Activated.');
            $this->session->set_flashdata('msg', "Tender Activated.");
        endif;
		//die;
       /*  if ($_REQUEST['actid']) {
            $inserArr = array(
                'user_id' => $this->session->userdata('uid'),
                'project_id' => $_REQUEST['actid'],
                'actionIP' => get_client_ip());
            $Respon = $this->Front_model->insertRecord('active_project_byuser', $inserArr);
        }
        if ($Respon > 0):
            $this->visibilityStatus('Active_project', $_REQUEST['actid']);
            $this->notified($_REQUEST['actid'], 'Tender Activated.');
            $this->session->set_flashdata('msg', "Tender Activated.");
        endif; */
        //redirect(base_url('/newproject'));
    }

    //Visibility Status
    public function visibilityStatus($copeName, $actid) {
        return $this->Front_model->updateRecord('bd_tenderdetail', array('visible_scope' => $copeName), array('fld_id' => $actid));
    }

    //Comment Showe
    public function commentset() {
        $tndrid = $_REQUEST['tndrid'];
        if (isset($tndrid)) {
            $Rec = $this->Front_model->selectRecordOrderByASC('comment_on_project', array('*'), array('is_active' => '1', 'project_id' => $tndrid));
            if ($Rec) {
                $data['allcomments'] = $Rec->result();
            }
            $this->load->view('comment_view', $data);
        }
    }

    //Comment Submit..
    public function commentsubmit() {
        $projId = $_REQUEST['projId'];
        $comnt = $_REQUEST['comnt'];
        if ($comnt):
            $insertArr = array('user_id' => $this->session->userdata('uid'), 'project_id' => $projId, 'sectorId' => '0', 'commentText' => $comnt);
            $Rec = $this->Front_model->insertRecord('comment_on_project', $insertArr);
            //$this->notified($projId, 'A new Comment on Tender.');
        endif;
        return $Rec;
    }

    //Project Active By Check Box Multi Action..
    /* public function activeprojectbycheckbox() {
        $chkBoxArr = $this->input->post('actchk');
        $actTyle = $this->input->post('act_type');
        $secId = $_REQUEST['secId'];

        //Bulk Active Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "Active")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'user_id' => $this->session->userdata('uid'),
                    'project_id' => $tndrID,
                    'actionIP' => get_client_ip());
                $Respon = $this->Front_model->insertRecord('active_project_byuser', $inserArr);
                $this->visibilityStatus('Active_project', $tndrID);
                $countvar ++;
            }
            $this->notified($tndrID, " $countvar Tender Activated.");
            $this->session->set_flashdata('msg', "Total $countvar Tender Activated.");
            redirect(base_url('/newproject'));
        endif;

        //Bulk InActive Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "InActive")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
                    'user_id' => $this->session->userdata('uid'),
                    'project_id' => $tndrID,
                    'actionIP' => get_client_ip());
                $Respon = $this->Front_model->insertRecord('trash_for_user', $inserArr);
                $this->visibilityStatus('InActive_project', $tndrID);
                $countvar ++;
            }
            $this->notified($tndrID, "Total $countvar Tender Deactivated.");
            $this->session->set_flashdata('msg', "Total $countvar Tender Deactivated.");
            redirect(base_url('/newproject'));
        endif;
        redirect(base_url('/newproject'));
    } */

    public function activeprojectbycheckbox() {
        $chkBoxArr = $this->input->post('actchk');
        $actTyle = $this->input->post('act_type');
        $secId = $_REQUEST['secId'];
		$businessID = $this->session->userdata('businessunit_id');
        //Bulk Active Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "Active")):
            $countvar = 0;
			
            foreach ($chkBoxArr as $tndrID) {
                $inserArr = array(
					'project_id' => $tndrID,
					'user_id' => $this->session->userdata('uid'),
					'action_date' => date('Y-m-d H:i:s'),
					'actionIP' => get_client_ip(),
					'message'=>'Active Project'
				);
				
				$Respon = $this->Front_model->insertRecord('bdactive_project_byuser', $inserArr);
				$respp = $this->Mastermodel->UpdateRecords('bdtender_scope', array('project_id' => $tndrID,'businessunit_id'=>$businessID), array('visible_scope' => 'Active_project'));
				$countvar ++;
			}
            $this->session->set_flashdata('msg', "Total $countvar Tender Activated.");
            redirect(base_url('/trafficproject'));
        endif;

        //Bulk InActive Process..
        if (count($chkBoxArr) > 0 && ($actTyle == "InActive")):
            $countvar = 0;
            foreach ($chkBoxArr as $tndrID) {
                $data = $this->Front_model->getProjectDetail($tndrID);
				if($data){
					$inserArr = array(
						'created_By' => $data->created_By,
						'tender24_ID' => $data->tender24_ID,
						'TenderDetails' => $data->TenderDetails,
						'Location' => $data->Location,
						'Organization' => $data->Organization,
						'Tender_url' => $data->Tender_url,
						'Sector_IDs' => $data->Sector_IDs,
						'Sector_Keyword_IDs' => $data->Sector_Keyword_IDs,
						'Keyword_IDs' => $data->Keyword_IDs,
						'keyword_phase' => $data->keyword_phase,
						'Created_Date' => $data->Created_Date,
						'Expiry_Date' => $data->Expiry_Date,
						'source_file' => $data->source_file,
						'project_type' => $data->project_type,
						'country' => $data->country,
						'remarks' => $data->remarks,
						'national_intern' => $data->national_intern,
						'national_status' => $data->national_status,
						'parent_tbl_id' => $data->project_id,
					);
					
					$inserinactiveArr = array(
						'project_id' => $data->project_id,
						'user_id' => $this->session->userdata('uid'),
						'action_date' => date('Y-m-d H:i:s'),
						'actionIP' => get_client_ip(),
						'message'=>'InActive Project'
					);
						
					$inserscopedumpArr = array(
						'project_id' => $data->project_id,
						'visible_scope' => 'InActive_project',
						'businessunit_id' => $data->businessunit_id
					);
				
					$count = $this->Mastermodel->count_rows('bdtender_scope', array('project_id' => $tndrID,'businessunit_id'=>$businessID));
					if ($count <= 5 AND $count > 1):
						$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
						$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
						$Respon2 = $this->Front_model->insertRecord('bdinactive_project_byuser', $inserinactiveArr);
						$res = $this->db->delete('bdtender_scope', array('project_id' => $tndrID ,'businessunit_id' => $businessID));
					else: 	
						$Respon = $this->Front_model->insertRecord('bdtenderdetails_dump', $inserArr);
						$Respon1 = $this->Front_model->insertRecord('bdtender_scopedump', $inserscopedumpArr);
						$Respon2 = $this->Front_model->insertRecord('bdinactive_project_byuser', $inserinactiveArr);
						$res = $this->db->delete('bdtender_scope', array('project_id' => $tndrID ,'businessunit_id' => $businessID)); 
						$res1 = $this->db->delete('bd_tenderdetail', array('fld_id' => $tndrID)); 
					endif;
					$countvar ++;	
				}
            }
            $this->session->set_flashdata('msg', "Total $countvar Tender Deactivated.");
            redirect(base_url('/trafficproject'));
        endif;
        redirect(base_url('/trafficproject'));
    }
	
	
    public function notified($tendrId, $notifData) {
        $rowArrData = NotifiedUserIDs($this->session->userdata('uid'));
        foreach ($rowArrData as $rowRec) {
            $insertArr = array('user_to_notify' => $rowRec, 'user_who_fired_event' => $this->session->userdata('uid'),
                'event_id_project' => $tendrId, 'notification_data' => $notifData);
            $Record = $this->Front_model->insertRecord('notification', $insertArr);
        }
    }
    

}
