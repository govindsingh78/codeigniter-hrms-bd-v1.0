<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

require_once APPPATH . "/third_party/excel1/vendor/autoload.php";

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Font;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Protection;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class Accountdept extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Front_model');
        $this->load->model('SecondDB_model');
        $this->load->model('mastermodel');
        $this->load->model('crutraker/Crutrakermodel');
        $this->load->model('Accountdept_model', 'accdeptmodel');
        $this->load->model('Account_reimbursable', 'accreimbursable');
        $this->load->model('Account_designationcategory', 'accdesigncateg');
        $this->load->model('Account_projdesignationcateglist', 'accdesignationcateglist');
        $this->load->model('Account_assigned_designationlist', 'accassigndesignationlist');
        $this->load->model('Account_teamassignlist', 'assignfinalteam');


        $this->load->helper(array('form', 'url', 'user_helper'));
        $this->load->library('session', 'form_validation');
        if (!($this->session->userdata('uid'))) {
            redirect(base_url());
        }
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    public function getproject() {
        if ($_REQUEST['project_numberid']) {
            $id = $_REQUEST['project_numberid'];
            $this->db->select('b.fld_id,b.TenderDetails');
            $this->db->from('bdcegexp_proj_summery as a');
            $this->db->join('bd_tenderdetail as b', 'a.project_id = b.fld_id', 'left');
            $this->db->where('a.project_numberid', $id);
            $res = $this->db->get()->result_array();
        }
        echo json_encode($res[0]);
        die;
    }

    public function getstate() {
        if ($_REQUEST['registered_country']) {
            $countryid = $_REQUEST['registered_country'];
            $this->db->select('state_id,state_name');
            $this->db->from('states');
            $this->db->where('country_id', $countryid);
            $res = $this->db->get()->result_array();
        }
        echo json_encode($res);
        die;
    }

    public function getcity() {
        if ($_REQUEST['registered_state']) {
            $stateid = $_REQUEST['registered_state'];
            $this->db->select('city_id,city_name');
            $this->db->from('cities');
            $this->db->where('state_id', $stateid);
            $res = $this->db->get()->result_array();
        }
        echo json_encode($res);
        die;
    }

    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Code By Asheesh 22-08-2019 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


    public function reimbursable_assignsave() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $loginUser = $this->session->userdata('uid');
        $bdprojId = $this->input->post('project_id');
        $ts_projID = $this->input->post('project_numberid');
        $reimbursablechk = $this->input->post('reimbursablechk');
        $recExist = $this->accdeptmodel->checkexist("acc2_reimbursable_assign", ["bd_projid" => $bdprojId]);
        if ($recExist > 0) :
            $this->session->set_flashdata('errot_msg', "Reimbursable already Assigned on Project");
        endif;
        if (($bdprojId and $ts_projID and $reimbursablechk) and ( $recExist < 1)) {
            foreach ($reimbursablechk as $reCrow) {
                $insertArr = array("bd_projid" => $bdprojId, "ts_projid" => $ts_projID, "reim_type_id" => $reCrow, "entry_by" => $loginUser);
                $this->db->insert("acc2_reimbursable_assign", $insertArr);
            }
            $this->session->set_flashdata('msg', "Reimbursable Assigned on Project Successfully.");
        }
        redirect(base_url("accountdept/reimbursable_assign"));
    }

    public function assignrecreimbursable_ajax() {
        $list = $this->accreimbursable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';
        foreach ($list as $val) {
            $tsprojID = $val->ts_projid;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $val->project_name;
            $row[] = $this->accdeptmodel->getallreimbursableonproject($tsprojID);
            $row[] = "<a title='Add Reimbursable' style='color:green' href='" . base_url("accountdept/add_reimbursable/" . $val->bd_projid) . "'><span class='glyphicon glyphicon-plus'></span></a>&nbsp;&nbsp;";
            $row[] = "<a title='Edit Info' href='#'><span class='glyphicon glyphicon-edit'></span></a>&nbsp;&nbsp;";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->accreimbursable->count_all(),
            "recordsFiltered" => $this->accreimbursable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    //save Assign Designation Categ..
    public function designationcat_assignsave() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $loginUser = $this->session->userdata('uid');
        $ts_projID = $this->input->post('project_numberid');
        $designcatchk = $this->input->post('designcatchk');
        $bdprojId = $this->tstobdprojid($ts_projID);

        $recExist = $this->accdeptmodel->checkexist("acc2_designcateg_assign", ["bd_projid" => $bdprojId]);
        if ($recExist > 0) :
            $this->session->set_flashdata('errot_msg', "Designation Category already Assigned on Project");
        endif;

        if (($bdprojId and $ts_projID and $designcatchk) and ( $recExist < 1)) {
            foreach ($designcatchk as $reCrow) {
                $insertArr = array("bd_projid" => $bdprojId, "ts_projid" => $ts_projID, "desigcat_id" => $reCrow, "entry_by" => $loginUser);
                $this->db->insert("acc2_designcateg_assign", $insertArr);
            }
            $this->session->set_flashdata('msg', "Designation Category Assigned on Project Successfully.");
        }
        redirect(base_url("accountdept/designcateg_assignonproj"));
    }

    //Designation Category Assign On Project...
    public function designcateg_assignonproj() {
        $data["error"] = '';
        $data["title"] = "Designation category Assign on Project";
        $data["accinfoprojlist"] = $this->accdeptmodel->getallprojaccountinfo();
        $data["designationcat_masterlist"] = $this->accdeptmodel->getall_designationcatlist();
        $this->load->view("accountdept/accdept_designcat_assignproj_view", $data);
    }

    public function assignrec_designation_categ_ajax() {
        $list = $this->accdesigncateg->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';
        foreach ($list as $val) {
            $tsprojID = $val->ts_projid;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $val->project_name;
            $row[] = $this->accdeptmodel->getalldesignationcategonproject($tsprojID);
            $row[] = "<a title='Edit Info' href='#'><span class='glyphicon glyphicon-edit'></span></a>&nbsp;&nbsp;";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->accdesigncateg->count_all(),
            "recordsFiltered" => $this->accdesigncateg->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    //Assign Designation Based on Project/designation category--> Designation..
    public function design_ondesigncategproj() {
        $data["error"] = '';
        $data["accinfoprojlist"] = $this->accdeptmodel->getallprojaccountinfo();
        $data["designcategorylist"] = $this->accdeptmodel->getall_designationcatlist();
        $data["title"] = "Assign Designation on designation category & Project";
        $this->load->view("accountdept/accdept_designondesigncategproj_view", $data);
    }

    //Assign Designation List Ajax..
    public function assigned_designationlist_ajax() {
        $list = $this->accdesignationcateglist->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';
        foreach ($list as $val) {
            $tsprojID = $val->ts_projid;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $val->project_name;
            $row[] = $val->ktype_name;
            $row[] = "<a title='Assign Designation' href='" . base_url("accountdept/assigndesignation/" . $val->ts_projid . "/" . $val->desigcat_id) . "'>" . $val->count_assignondesign . "</a>";
            $row[] = "<a title='Assign Designation' href='" . base_url("accountdept/assigndesignation/" . $val->ts_projid . "/" . $val->desigcat_id) . "'><span class='glyphicon glyphicon-edit'></span></a>";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->accdesignationcateglist->count_all(),
            "recordsFiltered" => $this->accdesignationcateglist->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    //Assign Desination Save..
    public function designation_assignsave() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $designation_id = $this->input->post('designation_id');
        $company_id = $this->input->post('company_id');
        $bddprojid = $this->input->post('bddprojid');
        $hrmssrojid = $this->input->post('hrmssrojid');
        $designcategid = $this->input->post('designcategid');

        $whereArr = array("project_id" => $bddprojid, "status" => '1', "designation_id" => $designation_id, "designatin_categid" => $designcategid);
        $chkrecExist = $this->accdeptmodel->checkexist("assign_finalteam", $whereArr);
        if ($chkrecExist > 0) {
            $this->session->set_flashdata('error_msg', "This Designation Already Exist .");
        }
        if (($designation_id and $company_id and $bddprojid and $designcategid) and ( $chkrecExist < 1)) :
            $insertArr = array("project_id" => $bddprojid,
                "designation_id" => $designation_id,
                "designatin_categid" => $designcategid,
                "company_id" => $company_id,
                "teamstatus" => "0",
                "proj_positioid_metro" => $this->input->post('proj_positioid_metro'));
            $this->db->insert("assign_finalteam", $insertArr);
            $insert_id = $this->db->insert_id();
            if ($insert_id):
                $this->session->set_flashdata('msg', "Designation Assigned on Project Successfully.");
            endif;
        endif;
        redirect(base_url("accountdept/assigndesignation/" . $hrmssrojid . "/" . $designcategid));
    }

    //Ajax For DataTable.. 
    public function assigndesignationonproj_ajax() {
        $list = $this->accassigndesignationlist->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';
        foreach ($list as $val) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $val->ktype_name;
            $row[] = $val->company_name;
            $row[] = $val->designation_name;
            if ($val->teamstatus == "0" and $val->status == "1") {
                $row[] = '<a title="Delete Designation" style="cursor:pointer" onclick="delrecDesignation(' . "'" . $val->id . "'" . ')" ><span class="glyphicon glyphicon-trash"></span></a>';
            } else {
                $row[] = "<span class='glyphicon glyphicon glyphicon-lock'></span>";
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->accassigndesignationlist->count_all(),
            "recordsFiltered" => $this->accassigndesignationlist->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    //Assign Team Member On Designation...
    public function assignteamprojlist() {
        $data["error"] = '';
        $data["accinfoprojlist"] = $this->accdeptmodel->getallprojaccountinfo();
        $data["designcategorylist"] = $this->accdeptmodel->getall_designationcatlist();
        $data["title"] = "Assign Team on Project";
        $this->load->view("accountdept/accdept_assignteamprojlist_view", $data);
    }

    public function assign_teamonproject_ajax() {
        $list = $this->assignfinalteam->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';
        foreach ($list as $val) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $val->project_name;
            $row[] = '';
            $row[] = "<a title='Assign Team' href='" . base_url("accountdept/assign_team_member/" . $val->project_id) . "'><span class='glyphicon glyphicon-user'></span></a>";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->assignfinalteam->count_all(),
            "recordsFiltered" => $this->assignfinalteam->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    //Assign Team
    public function assign_team_member($bdprojID) {
        $data["error"] = '';
        $data["designCategList"] = $this->accdeptmodel->getalldesigncategontsprojid($bdprojID);
        $data["accInfoData"] = $this->accdeptmodel->GetAccountInfoData($bdprojID);
        $data["teamDetailRec"] = $this->accdeptmodel->getDesignationTeamDetails();
        $data["hrmsEmpRecs"] = $this->accdeptmodel->getall_hrmsEmplist();
        $data["OtherEmpRecs"] = $this->accdeptmodel->getall_otherEmplist();
        $data["title"] = "Assign Team on Project Member";
        $this->load->view("accountdept/accdept_assignfinalteam_view", $data);
    }

    public function save_upd_assignfinalteam() {
        $assignfn_design_id = $_REQUEST['assign_fnteam_design_id'];
        $bdproject_id = $_REQUEST['bdproject_id'];
        $loginUser = $this->session->userdata('uid');
        $DesignDetailArr = getDesignationTeamDetail($bdproject_id, $assignfn_design_id);

        if ($DesignDetailArr) {
            foreach ($DesignDetailArr as $reCrow) {
                $empType = $this->input->post("empltype_" . $reCrow->id);
                $empHrmsID = $this->input->post("empllisthrms_" . $reCrow->id);
                $empOtherID = $this->input->post("empllistother_" . $reCrow->id);
                $empDoj = $this->input->post("empdoj_" . $reCrow->id);

                if (($empType) and ( $empHrmsID or $empOtherID)) {
                    //For Default Type Invoce...
                    if ($_REQUEST['invoice_formate_type'] == 1) {
                        $empTotMM = $this->input->post("emptotalmm_" . $reCrow->id);
                        $empRate = $this->input->post("empratemm_" . $reCrow->id);

                        $UpdArr = array("employee_type" => $empType,
                            "empname" => ($empType == 1) ? $empHrmsID : $empOtherID,
                            "account_date" => ($empDoj) ? $empDoj : '',
                            "rate" => ($empDoj) ? $empDoj : '',
                            "man_months" => ($empTotMM) ? $empTotMM : '',
                            "balancemm" => ($empTotMM) ? $empTotMM : '',
                            "rate" => ($empRate) ? $empRate : '',
                            "modified" => $loginUser);
                    }
                    //For Maint & Construction Type invoice..
                    if ($_REQUEST['invoice_formate_type'] == 2) {
                        $empcmm = $this->input->post("empcmm_" . $reCrow->id);
                        $empcratemm = $this->input->post("empcratemm_" . $reCrow->id);
                        $empmaintaincmm = $this->input->post("empmaintaincmm_" . $reCrow->id);
                        $empmaintainrate = $this->input->post("empmaintainrate_" . $reCrow->id);
                        $UpdArr = array("employee_type" => $empType,
                            "empname" => ($empType == 1) ? $empHrmsID : $empOtherID,
                            "account_date" => ($empDoj) ? $empDoj : '',
                            "maintenance_months" => ($empmaintaincmm) ? $empmaintaincmm : '',
                            "construction_months" => ($empcmm) ? $empcmm : '',
                            "maintenance_rate" => ($empmaintainrate) ? $empmaintainrate : '',
                            "construction_rate" => ($empcratemm) ? $empcratemm : '',
                            "modified" => $loginUser);
                    }
                    $this->db->where('id', $reCrow->id);
                    $this->db->update("assign_finalteam", $UpdArr);
                }
            }
            $this->session->set_flashdata('msg', "Assign Team on Project Successfully.");
        }
        redirect(base_url("accountdept/assign_team_member/" . $bdproject_id));
    }

    //Del Team ...
    public function deldesignation($DeLId) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $sIngleRecRow = $this->accdeptmodel->getAssignFinalSingleRow($DeLId);
        if ($sIngleRecRow and $sIngleRecRow->teamstatus == "0") {
            $this->db->where('id', $DeLId);
            $this->db->update("assign_finalteam", array("status" => "0"));
        }
        redirect(base_url("accountdept/assigndesignation/" . $sIngleRecRow->project_id . "/" . $sIngleRecRow->designatin_categid));
    }

    public function saveupd_assignfinalteam() {

        echo "<pre>";
        print_r($_REQUEST);
        die;


        $actevent = $_REQUEST['actevent'];
        if ($actevent) {
            foreach ($actevent as $RoWr) {
                $compnid = $_REQUEST['compnid_' . $RoWr];
                //  $empl = $_REQUEST['empl_' . $RoWr];
                $accdate = $_REQUEST['accdate_' . $RoWr];
                $tmm = $_REQUEST['tmm_' . $RoWr];
                $rate = $_REQUEST['rate_' . $RoWr];
                $constructionmm = $_REQUEST['constructionmm_' . $RoWr];
                $constructionrate = $_REQUEST['constructionrate_' . $RoWr];
                $maintenancemm = $_REQUEST['maintenancemm_' . $RoWr];
                $maintenancerate = $_REQUEST['maintenancerate_' . $RoWr];

                if ($compnid == "74") {
                    $employee_type = "1";
                }
                if ($compnid != "74") {
                    $employee_type = "2";
                }
                $updArr = array("maintenance_months" => ($maintenancemm) ? $maintenancemm : Null,
                    "maintenance_rate" => ($maintenancerate) ? $maintenancerate : Null,
                    "construction_months" => ($constructionmm) ? $constructionmm : Null,
                    "construction_rate" => ($constructionrate) ? $constructionrate : Null,
                    "rate" => ($rate) ? $rate : Null,
                    "account_date" => ($accdate) ? $accdate : Null,
                    "man_months" => ($tmm) ? $tmm : Null,
                    //   "empname" => ($empl) ? $empl : Null,
                    "teamstatus" => "1",
                    "employee_type" => ($employee_type) ? $employee_type : Null);
                $this->db->where(["status" => "1", "id" => $RoWr]);
                $Resp = $this->db->update("assign_finalteam", $updArr);
            }

            $this->session->set_flashdata('msg', "Team Record Updated Successfully.");
        }
        redirect(base_url("accountdept/assignfinal_team/" . $_REQUEST['project_id']));
    }

    public function addupd_teamuser() {

        if ($_REQUEST) {
            $updArr = array("empname" => ($_REQUEST['empl_hrms']) ? $_REQUEST['empl_hrms'] : $_REQUEST['empl_other']);
            $this->db->where(["status" => "1", "id" => $_REQUEST['editrowid']]);
            $Resp = $this->db->update("assign_finalteam", $updArr);
            $this->session->set_flashdata('msg', "Team User Record Updated Successfully.");
        }
        redirect(base_url("accountdept/assignfinal_team/" . $_REQUEST['bdprojids']));
    }

    //Accout Department reimbursable_assign Manage
    public function reimbursable_assign() {
        $data["error"] = '';
        $data["title"] = "Reimbursable Category Assign on Project";
        $data["accinfoprojlist"] = $this->accdeptmodel->getallprojaccountinfo();
        $data["reimbursablelist"] = $this->accdeptmodel->getall_reimbursablelist();
        $this->load->view("accountdept/accdept_reimbursable_assignproj_view", $data);
    }

    /// Save or Upd 
    public function reimburs_description_save() {
        $bdProjId = $_REQUEST['bdprojID'];
        $typerId = $_REQUEST['reimbursable_categ'];
        $description = $this->input->post("description");
        if ($_REQUEST['invoice_formate_type'] == "2") {
            $_REQUEST['no_month'] = ($_REQUEST['construction_months'] + $_REQUEST['maintenance_months']);
            $_REQUEST['per_month'] = ($_REQUEST['construction_rate'] + $_REQUEST['maintenance_rate']);
        }
        //  $numRows = $this->db->where(['status' => '1', 'project_id' => $bdProjId, 'type_id' => $typerId])->from("acc_msterdetail")->count_all_results();
        //  if ($numRows < 1) {
        //Insert..
        $insertArr = array("project_id" => $bdProjId, "type_id" => $typerId, "description" => $description,
            "no_month" => ($_REQUEST['no_month']) ? $_REQUEST['no_month'] : NULL,
            "per_month" => ($_REQUEST['per_month']) ? $_REQUEST['per_month'] : NULL,
            "qty" => ($_REQUEST['qty']) ? $_REQUEST['qty'] : NULL,
            "assign_comp_id" => ($_REQUEST['assign_comp_id']) ? $_REQUEST['assign_comp_id'] : NULL,
            "construction_months" => ($_REQUEST['construction_months']) ? $_REQUEST['construction_months'] : NULL,
            "construction_rate" => ($_REQUEST['construction_rate']) ? $_REQUEST['construction_rate'] : NULL,
            "maintenance_months" => ($_REQUEST['maintenance_months']) ? $_REQUEST['maintenance_months'] : NULL,
            "maintenance_rate" => ($_REQUEST['maintenance_rate']) ? $_REQUEST['maintenance_rate'] : NULL,
            "createdby" => $this->session->userdata('uid'));
        $this->db->insert("acc_msterdetail", $insertArr);
        $this->session->set_flashdata('msg', "Reimbursable Details Insert Successfully.");
        //  } 
//        else {
//            //Update..
//            $UpdArr = array("description" => $description,
//                "no_month" => ($_REQUEST['no_month']) ? $_REQUEST['no_month'] : NULL,
//                "per_month" => ($_REQUEST['per_month']) ? $_REQUEST['per_month'] : NULL,
//                "qty" => ($_REQUEST['qty']) ? $_REQUEST['qty'] : NULL,
//                "construction_months" => ($_REQUEST['construction_months']) ? $_REQUEST['construction_months'] : NULL,
//                "construction_rate" => ($_REQUEST['construction_rate']) ? $_REQUEST['construction_rate'] : NULL,
//                "maintenance_months" => ($_REQUEST['maintenance_months']) ? $_REQUEST['maintenance_months'] : NULL,
//                "maintenance_rate" => ($_REQUEST['maintenance_rate']) ? $_REQUEST['maintenance_rate'] : NULL,
//                "createdby" => $this->session->userdata('uid'));
//            $this->db->where(['status' => '1', 'project_id' => $bdProjId, 'type_id' => $typerId]);
//            $this->db->update("acc_msterdetail", $UpdArr);
//            $this->session->set_flashdata('msg', "Reimbursable Details Update Successfully.");
//        }

        redirect(base_url("accountdept/add_reimbursable/" . $bdProjId));
    }

    //Team Set Assign Final Team...
    public function assignfinal_team($bdProjId) {
        $data['error'] = '';
        $data['desig_categProj'] = $this->accdeptmodel->GetAllDesignCategAndTeamByProjID($bdProjId);
        $data['accInfo'] = $this->accdeptmodel->GetProjectInfobyID($bdProjId);
        $data['reimbursableList'] = $this->accdeptmodel->GetReimbursable_Byproj($bdProjId);
        $data['HrmsEmplList'] = $this->Crutrakermodel->GetUserList();
        $data['OtherEmployeeList'] = $this->Crutrakermodel->GetAllOtherEmployeeList();

        $this->load->view("accountdept/assign_setfinalteam", $data);
    }

    //Add Reimbursable..
    public function add_reimbursable($bdProjectID) {
        $data['error'] = '';
        $data["title"] = "Reimbursable Add on Project";
        $data["accinfoprojlist"] = $this->accdeptmodel->getallprojaccountinfo();
        $data['accInfo'] = $this->accdeptmodel->GetProjectInfobyID($bdProjectID);
        $data['reimbursCategList'] = $this->accdeptmodel->GetReimbursable_Byproj($bdProjectID);
        $data['AddedRecResults'] = $this->db->where(['status' => '1', 'project_id' => $bdProjectID])->from("acc_msterdetail")->get()->result();
        $data['ComponProjListArr'] = $this->accdeptmodel->GetAllCompanyonProj($bdProjectID);
        $this->load->view("accountdept/add_reimbursable_view", $data);
    }

    //My Script For Assign..
//    public function scriptash() {
//        $db1 = $this->db1->database;
//        $db2 = $this->db2->database;
//
//        $this->db->select("$db1.assign_finalteam.project_id,$db1.accountinfo.project_numberid");
//        $this->db->from("$db1.assign_finalteam");
//        $this->db->join("$db1.accountinfo", "$db1.assign_finalteam.project_id=$db1.accountinfo.project_id", 'left');
//        $this->db->where("$db1.assign_finalteam.status", '1');
//        $this->db->where("$db1.accountinfo.project_numberid>", "0");
//        $this->db->group_by("$db1.assign_finalteam.project_id");
//        $recResult = $this->db->get()->result();
//
//        if ($recResult) {
//            foreach ($recResult as $Key => $rOws) {
//
//                //1..
//                $numRows = $this->db->where(['status' => '1', 'bd_projid' => $rOws->project_id, 'desigcat_id' => "1"])->from("acc2_designcateg_assign")->count_all_results();
//                if ($numRows < 1) {
//                    $insertR1 = array("bd_projid" => $rOws->project_id,
//                        "ts_projid" => $rOws->project_numberid,
//                        "desigcat_id" => '1',
//                        "entry_by" => '297');
//                    $this->db->insert("acc2_designcateg_assign", $insertR1);
//                }
//
//                //2..
//                $numRows = $this->db->where(['status' => '1', 'bd_projid' => $rOws->project_id, 'desigcat_id' => "2"])->from("acc2_designcateg_assign")->count_all_results();
//                if ($numRows < 1) {
//                    $insertR2 = array("bd_projid" => $rOws->project_id,
//                        "ts_projid" => $rOws->project_numberid,
//                        "desigcat_id" => '2',
//                        "entry_by" => '297');
//                    $this->db->insert("acc2_designcateg_assign", $insertR2);
//                }
//
//                //3..
//                $numRows = $this->db->where(['status' => '1', 'bd_projid' => $rOws->project_id, 'desigcat_id' => "3"])->from("acc2_designcateg_assign")->count_all_results();
//                if ($numRows < 1) {
//                    $insertR3 = array("bd_projid" => $rOws->project_id,
//                        "ts_projid" => $rOws->project_numberid,
//                        "desigcat_id" => '3',
//                        "entry_by" => '297');
//                    $this->db->insert("acc2_designcateg_assign", $insertR3);
//                }
//            }
//        }
//    }
    //My Script For Assign..
//    public function scriptasheesh() {
//        $db1 = $this->db1->database;
//        $db2 = $this->db2->database;
//        $this->db->select("$db1.assign_finalteam.id,$db1.assign_finalteam.key_id");
//        $this->db->from("$db1.assign_finalteam");
//        // $this->db->join("$db1.accountinfo", "$db1.assign_finalteam.project_id=$db1.accountinfo.project_id", 'left');
//        $this->db->where("$db1.assign_finalteam.status", '1');
//        // $this->db->where("$db1.accountinfo.project_numberid>", "0");
//        // $this->db->group_by("$db1.assign_finalteam.project_id");
//        $recResult = $this->db->get()->result();
//
//        if ($recResult) {
//            foreach ($recResult as $Key => $rOws) {
//                // $designation_id = $rOws->designation_id;
//                // $recsRow = $this->db->where(['is_active' => '1', 'fld_id' => $designation_id])->from("designation_master_requisition")->get()->row();
//                $cat_id = $rOws->key_id;
//                $assignFinalTeamUpd = array("company_id" => "74", "designatin_categid" => $cat_id, "employee_type" => "1");
//
//                $this->db->where(['id' => $rOws->id]);
//                $this->db->update("assign_finalteam", $assignFinalTeamUpd);
//            }
//        }
//    }
    //My Script For Assign..
//    public function scriptceg() {
//        $db1 = $this->db1->database;
//        $db2 = $this->db2->database;
//
//        $this->db->select("$db1.assign_finalteam.id,$db1.assign_finalteam.project_id,$db1.accountinfo.project_numberid");
//        $this->db->from("$db1.assign_finalteam");
//        $this->db->join("$db1.accountinfo", "$db1.assign_finalteam.project_id=$db1.accountinfo.project_id", 'left');
//        $this->db->where("$db1.assign_finalteam.status", '1');
//        // $this->db->where("$db1.accountinfo.project_numberid>", "0");
//        $this->db->group_by("$db1.assign_finalteam.project_id");
//        $recResult = $this->db->get()->result();
//        if ($recResult) {
//            foreach ($recResult as $Key => $rOws) {
//                $project_id = $rOws->project_id;
//                if ($project_id and $rOws->project_numberid) {
//                    $recsRow = $this->db->where(['status' => '1', 'project_id' => $project_id])->group_by("type_id")->from("acc_msterdetail")->get()->result();
//
//                    if ($recsRow) {
//                        foreach ($recsRow as $rEcD) {
//                            $NumRows2 = $this->db->where(['status' => '1', 'bd_projid' => $rEcD->project_id, 'reim_type_id' => $rEcD->type_id])->from("acc2_reimbursable_assign")->count_all_results();
//                            if ($NumRows2 < 1) {
//                                $inSertArr = array("bd_projid" => $rEcD->project_id, "ts_projid" => $rOws->project_numberid, "reim_type_id" => $rEcD->type_id, "entry_by" => $this->session->userdata('uid'));
//                                $this->db->insert("acc2_reimbursable_assign", $inSertArr);
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
    //Asign Designation ...
    public function assigndesignation($tsprojid, $designcatid) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $bdProjId = getprojectid_tstobd($tsprojid);
        $recProj = $this->Front_model->getprojectname($bdProjId);
        $data["error"] = '';
        $data["title"] = "Designation Assign";
        $data['tsprojid'] = $tsprojid;
        $data['designcatid'] = $designcatid;
        $data['bdprojectid'] = $bdProjId;
        $data['projectname'] = $recProj[0]->project_name;
        $data['DesignCategName'] = $this->accdeptmodel->getDesignCategNameById($designcatid);
        $data['DesignationListArr'] = $this->accdeptmodel->GetAllDesignListByDesigCategID($designcatid);
        $data['ComponProjListArr'] = $this->accdeptmodel->GetAllCompanyonProj($bdProjId);
        //Project Type / Invoce type Get if mentro...
        $data['accountinfoData'] = $this->accdeptmodel->GetAccountInfoData($bdProjId);

        $this->load->view("accountdept/accdept_projassign_view", $data);
    }

    public function tstobdprojid($tsprojID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.accountinfo.project_id");
        $this->db->from("$db1.accountinfo");
        $this->db->where("$db1.accountinfo.status", '1');
        $this->db->where("$db1.accountinfo.project_numberid", $tsprojID);
        $recRows = $this->db->get()->row();
        return ($recRows) ? $recRows->project_id : null;
    }

    //Account Department Attendance..
    public function accdept_attendance() {
        $data['error'] = '';
        $data['title'] = "Attendance Fill / Update And View";
        $this->load->view("accountdept/accdept_attendance_view", $data);
    }

    //Attendance Project List
    public function attenprojlist_ajax() {
        $list = $this->accdeptmodel->invoiceProjectList();
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';
        foreach ($list as $val) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $val->project_name;
            $row[] = "<a title='Add/View' href='" . base_url("accountdept/addview_attendance/" . $val->project_id . "/" . date("Y/m")) . "'><span class='glyphicon glyphicon-plus'></span></a>";
            $row[] = "<a title='Edit/Update' href='" . base_url("accountdept/project_attendance/" . $val->project_id . "/" . date("Y/m")) . "'><span class='glyphicon glyphicon-edit'></span></a>";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data,
        );
        echo json_encode($output);
    }

    //Project Attendance .. 
    public function project_attendance($bdProjId, $year, $month) {
        if ($bdProjId and $year and $month) {
            $year = intval($year);
            $month = intval($month);
            $data['DesignCategList'] = $this->accdeptmodel->GetAllDesignCategAndTeamByProjID($bdProjId);
            //Get Existing Attend..
            $data['existAttendRec'] = $this->accdeptmodel->GetAttendRec($bdProjId, $year, $month);


            $data['bdProjId'] = $bdProjId;
            $data['year'] = $year;
            $data['month'] = $month;
            $data['projectinfo'] = $this->accdeptmodel->GetAccountInfoData($bdProjId);
            $data['title'] = "Edit or Update Attendance Project";
            $this->load->view("accountdept/project_attendance_view", $data);
        } else {
            redirect(base_url("accountdept/accdept_attendance"));
        }
    }

    //Set Year Months On Edit...
    public function setyearmonthatten() {
        $bdprojectid = $_REQUEST['attenbdprojectid'];
        $year = $_REQUEST['year_rcd'];
        $month = $_REQUEST['month_rcd'];
        if ($bdprojectid and $year and $month) {
            redirect(base_url("accountdept/project_attendance/" . $bdprojectid . "/" . $year . "/" . $month));
        }
    }

    //Set Year Months On Add...
    public function setyearmonthatten_add() {
        $bdprojectid = $_REQUEST['attenbdprojectid'];
        $year = $_REQUEST['year_rcd'];
        $month = $_REQUEST['month_rcd'];
        if ($bdprojectid and $year and $month) {
            redirect(base_url("accountdept/addview_attendance/" . $bdprojectid . "/" . $year . "/" . $month));
        }
    }

    public function getsinglerow_ajax() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $id = $_REQUEST['rowIDs'];

        $this->db->select("$db1.acc_msterdetail.*");
        $this->db->from("$db1.acc_msterdetail");
        $this->db->where("$db1.acc_msterdetail.status", '1');
        $this->db->where("$db1.acc_msterdetail.id", $id);
        $recRows = $this->db->get()->row();
        echo ($recRows) ? json_encode($recRows) : null;
    }

    //Save Update 
    public function editsave_reimbursable() {
        if ($_REQUEST['editrowid']) {
            $updArr = array("no_month" => $_REQUEST['no_month'],
                "per_month" => $_REQUEST['per_month'],
                "qty" => $_REQUEST['qty'],
                "construction_months" => $_REQUEST['construction_months'],
                "construction_rate" => $_REQUEST['construction_rate'],
                "maintenance_months" => $_REQUEST['maintenance_months'],
                "maintenance_rate" => $_REQUEST['maintenance_rate']);
            $this->db->where('id', $_REQUEST['editrowid']);
            $this->db->update("acc_msterdetail", $updArr);
            $this->session->set_flashdata('msg', "Reimbursable Record Update Successfully.");
        }
        redirect(base_url("/accountdept/add_reimbursable/" . $_REQUEST['bdprojids']));
    }

    public function getsinglerow_assignfinalteamajax() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $id = $_REQUEST['rowIDs'];

        $this->db->select("$db1.assign_finalteam.empname");
        $this->db->from("$db1.assign_finalteam");
        $this->db->where("$db1.assign_finalteam.status", '1');
        $this->db->where("$db1.assign_finalteam.id", $id);
        $recRows = $this->db->get()->row();
        echo ($recRows) ? json_encode($recRows) : null;
    }

    //View Or Add Attendance ..
    public function addview_attendance($bdProjId, $year, $month) {
        $recDArr = GetKeyTeamList($bdProjId, '1');
        if ($bdProjId and $year and $month) {
            $year = intval($year);
            $month = intval($month);
            $data['DesignCategList'] = $this->accdeptmodel->GetAllDesignCategAndTeamByProjID($bdProjId);
            //Get Existing Attend..
            $data['existAttendRec'] = $this->accdeptmodel->GetAttendRec($bdProjId, $year, $month);
            $data['bdProjId'] = $bdProjId;
            $data['year'] = $year;
            $data['month'] = $month;
            $data['projectinfo'] = $this->accdeptmodel->GetAccountInfoData($bdProjId);
            $data['title'] = "Add/View Attendance on Project";
            $this->load->view("accountdept/project_addview_attendance_view", $data);
        } else {
            redirect(base_url("accountdept/accdept_attendance"));
        }
    }

    public function saveupd_attendancedata() {
        $assignFinalTreamID = $_REQUEST['assign_finalteam_id'];
        $bdprojid = $_REQUEST['bdprojid'];
        $year = $_REQUEST['attenyear'];
        $month = $_REQUEST['attenmonth'];
        $assign_finalteam_id = $_REQUEST['assign_finalteam_id'];

        if ($assign_finalteam_id) {
            foreach ($assign_finalteam_id as $rEcD) {
                $UpdRecArr = array("`1`" => ($_REQUEST['atten_' . $rEcD . '_1']) ? $_REQUEST['atten_' . $rEcD . '_1'] : "",
                    "`2`" => ($_REQUEST['atten_' . $rEcD . '_2']) ? $_REQUEST['atten_' . $rEcD . '_2'] : "",
                    "`3`" => ($_REQUEST['atten_' . $rEcD . '_3']) ? $_REQUEST['atten_' . $rEcD . '_3'] : "",
                    "`4`" => $_REQUEST['atten_' . $rEcD . '_4'] ? $_REQUEST['atten_' . $rEcD . '_4'] : "",
                    "`5`" => $_REQUEST['atten_' . $rEcD . '_5'] ? $_REQUEST['atten_' . $rEcD . '_5'] : "",
                    "`6`" => $_REQUEST['atten_' . $rEcD . '_6'] ? $_REQUEST['atten_' . $rEcD . '_6'] : "",
                    "`7`" => $_REQUEST['atten_' . $rEcD . '_7'] ? $_REQUEST['atten_' . $rEcD . '_7'] : "",
                    "`8`" => $_REQUEST['atten_' . $rEcD . '_8'] ? $_REQUEST['atten_' . $rEcD . '_8'] : "",
                    "`9`" => $_REQUEST['atten_' . $rEcD . '_9'] ? $_REQUEST['atten_' . $rEcD . '_9'] : "",
                    "`10`" => $_REQUEST['atten_' . $rEcD . '_10'] ? $_REQUEST['atten_' . $rEcD . '_10'] : "",
                    "`11`" => $_REQUEST['atten_' . $rEcD . '_11'] ? $_REQUEST['atten_' . $rEcD . '_11'] : "",
                    "`12`" => $_REQUEST['atten_' . $rEcD . '_12'] ? $_REQUEST['atten_' . $rEcD . '_12'] : "",
                    "`13`" => $_REQUEST['atten_' . $rEcD . '_13'] ? $_REQUEST['atten_' . $rEcD . '_13'] : "",
                    "`14`" => $_REQUEST['atten_' . $rEcD . '_14'] ? $_REQUEST['atten_' . $rEcD . '_14'] : "",
                    "`15`" => $_REQUEST['atten_' . $rEcD . '_15'] ? $_REQUEST['atten_' . $rEcD . '_15'] : "",
                    "`16`" => $_REQUEST['atten_' . $rEcD . '_16'] ? $_REQUEST['atten_' . $rEcD . '_16'] : "",
                    "`17`" => $_REQUEST['atten_' . $rEcD . '_17'] ? $_REQUEST['atten_' . $rEcD . '_17'] : "",
                    "`18`" => $_REQUEST['atten_' . $rEcD . '_18'] ? $_REQUEST['atten_' . $rEcD . '_18'] : "",
                    "`19`" => $_REQUEST['atten_' . $rEcD . '_19'] ? $_REQUEST['atten_' . $rEcD . '_19'] : "",
                    "`20`" => $_REQUEST['atten_' . $rEcD . '_20'] ? $_REQUEST['atten_' . $rEcD . '_20'] : "",
                    "`21`" => $_REQUEST['atten_' . $rEcD . '_21'] ? $_REQUEST['atten_' . $rEcD . '_21'] : "",
                    "`22`" => $_REQUEST['atten_' . $rEcD . '_22'] ? $_REQUEST['atten_' . $rEcD . '_22'] : "",
                    "`23`" => $_REQUEST['atten_' . $rEcD . '_23'] ? $_REQUEST['atten_' . $rEcD . '_23'] : "",
                    "`24`" => $_REQUEST['atten_' . $rEcD . '_24'] ? $_REQUEST['atten_' . $rEcD . '_24'] : "",
                    "`25`" => $_REQUEST['atten_' . $rEcD . '_25'] ? $_REQUEST['atten_' . $rEcD . '_25'] : "",
                    "`26`" => $_REQUEST['atten_' . $rEcD . '_26'] ? $_REQUEST['atten_' . $rEcD . '_26'] : "",
                    "`27`" => $_REQUEST['atten_' . $rEcD . '_27'] ? $_REQUEST['atten_' . $rEcD . '_27'] : "",
                    "`28`" => $_REQUEST['atten_' . $rEcD . '_28'] ? $_REQUEST['atten_' . $rEcD . '_28'] : "",
                    "`29`" => $_REQUEST['atten_' . $rEcD . '_29'] ? $_REQUEST['atten_' . $rEcD . '_29'] : "",
                    "`30`" => $_REQUEST['atten_' . $rEcD . '_30'] ? $_REQUEST['atten_' . $rEcD . '_30'] : "",
                    "`31`" => $_REQUEST['atten_' . $rEcD . '_31'] ? $_REQUEST['atten_' . $rEcD . '_31'] : "",
                    "modified" => date("Y-m-d"));
                $this->db->where(['id' => $rEcD, 'is_active' => '1']);
                $this->db->update("timeshet_fill", $UpdRecArr);
            }
            $this->session->set_flashdata('msg', "Attendance Record Details Insert/Update Successfully.");
        }
        redirect(base_url("accountdept/project_attendance/" . $bdprojid . "/" . $year . "/" . $month));
    }
    

    //Get Single Row Assign Final Team..
    public function getSingleRow_AssignFinalTeam($id) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.assign_finalteam.*");
        $this->db->from("$db1.assign_finalteam");
        $this->db->where("$db1.assign_finalteam.status", '1');
        $this->db->where("$db1.assign_finalteam.id", $id);
        $recRows = $this->db->get()->row();
        return ($recRows) ? $recRows : null;
    }

    //Save Attendance Data..
    public function addsave_attendancedata() {
        if ($_REQUEST['assign_finalteam_id'] and $_REQUEST['bdprojid'] and $_REQUEST['attenyear'] and $_REQUEST['attenmonth']) {
            $AssignfinalteamidArr = $_REQUEST['assign_finalteam_id'];
            $projID = $_REQUEST['bdprojid'];
            $year = $_REQUEST['attenyear'];
            $month = $_REQUEST['attenmonth'];

            foreach ($AssignfinalteamidArr as $afTeamID) {
                $RecAssignFnTeam = $this->getSingleRow_AssignFinalTeam($afTeamID);
                $numRows = $this->db->where(['year' => $year, 'month' => $month, 'is_active' => '1', 'project_id' => $projID, 'designation_id' => $RecAssignFnTeam->designation_id])->from("timeshet_fill")->count_all_results();
                if ($numRows < 1) {
                    //Single Row Insert into timeshet_fill Table ..
                    $insertArr = array("designation_id" => $RecAssignFnTeam->designation_id, "emp_id" => $RecAssignFnTeam->empname,
                        "project_id" => $projID,
                        "fill_date" => date("Y-m-d"),
                        "year" => $year,
                        "month" => $month,
                        "`1`" => $_REQUEST['atten_' . $afTeamID . '_1'],
                        "`2`" => $_REQUEST['atten_' . $afTeamID . '_2'],
                        "`3`" => $_REQUEST['atten_' . $afTeamID . '_3'],
                        "`4`" => $_REQUEST['atten_' . $afTeamID . '_4'],
                        "`5`" => $_REQUEST['atten_' . $afTeamID . '_5'],
                        "`6`" => $_REQUEST['atten_' . $afTeamID . '_6'],
                        "`7`" => $_REQUEST['atten_' . $afTeamID . '_7'],
                        "`8`" => $_REQUEST['atten_' . $afTeamID . '_8'],
                        "`9`" => $_REQUEST['atten_' . $afTeamID . '_9'],
                        "`10`" => $_REQUEST['atten_' . $afTeamID . '_10'],
                        "`11`" => $_REQUEST['atten_' . $afTeamID . '_11'],
                        "`12`" => $_REQUEST['atten_' . $afTeamID . '_12'],
                        "`13`" => $_REQUEST['atten_' . $afTeamID . '_13'],
                        "`14`" => $_REQUEST['atten_' . $afTeamID . '_14'],
                        "`15`" => $_REQUEST['atten_' . $afTeamID . '_15'],
                        "`16`" => $_REQUEST['atten_' . $afTeamID . '_16'],
                        "`17`" => $_REQUEST['atten_' . $afTeamID . '_17'],
                        "`18`" => $_REQUEST['atten_' . $afTeamID . '_18'],
                        "`19`" => $_REQUEST['atten_' . $afTeamID . '_19'],
                        "`20`" => $_REQUEST['atten_' . $afTeamID . '_20'],
                        "`21`" => $_REQUEST['atten_' . $afTeamID . '_21'],
                        "`22`" => $_REQUEST['atten_' . $afTeamID . '_22'],
                        "`23`" => $_REQUEST['atten_' . $afTeamID . '_23'],
                        "`24`" => $_REQUEST['atten_' . $afTeamID . '_24'],
                        "`25`" => $_REQUEST['atten_' . $afTeamID . '_25'],
                        "`26`" => $_REQUEST['atten_' . $afTeamID . '_26'],
                        "`27`" => $_REQUEST['atten_' . $afTeamID . '_27'],
                        "`28`" => $_REQUEST['atten_' . $afTeamID . '_28'],
                        "`29`" => $_REQUEST['atten_' . $afTeamID . '_29'],
                        "`30`" => $_REQUEST['atten_' . $afTeamID . '_30'],
                        "`31`" => $_REQUEST['atten_' . $afTeamID . '_31'],
                        "entryby" => $this->session->userdata('uid'),
                        "created" => date("Y-m-d"));
                    $this->db->insert("timeshet_fill", $insertArr);
                }
            }
            $this->session->set_flashdata('msg', "Attendance Record Details Insert Successfully.");
        }
        redirect(base_url("accountdept/addview_attendance/" . $projID . "/" . $year . "/" . $month));
    }

}
