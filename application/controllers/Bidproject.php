<?php

/* Database connection start */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Bidproject extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('Manmonth_model');
        $this->load->model('mastermodel');
        $this->load->model('Bidproject_model', 'bidproject');
        $this->load->model('Front_model');
        $this->load->model('SecondDB_model');
        $this->load->model('Bidprojecteoi_model');
        $this->load->model('Bidprojectfq_model');
        $this->load->model('Bidprojectrfp_model');
        if (!($this->session->userdata('loginid'))) {
            redirect(base_url());
        }
        $bduserid = $this->session->userdata('loginid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";
        if ($actual_link == "http://bd.cegtechno.com") {
           if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):
            else:
                redirect(base_url('welcome/logout'));
            endif;
        }
    }

    //Dashbord After Login..
    public function index() {
        $title = 'Submitted Project';
        $sectorArr = $this->Front_model->GetActiveSector();
        $secId = '0';
        $proposalManager = $this->Front_model->allprojectProposalManager();
        //All Country..
        $CountryrecArr = $this->mastermodel->allcountry();
        $stateDetails = $this->mastermodel->StateByCounID('100');
        $this->load->view('bidproject/tender_view', compact('title', 'secId', 'sectorArr', 'proposalManager', 'CountryrecArr', 'stateDetails'));
    }
 
    // Project Display
    public function newProjectAll() {
        $list = $this->bidproject->get_datatables();
        //echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $view1 = '';
        $view2 = '';
        $view3 = '';
        $awardLink = '';
        //print_r($list);
        foreach ($list as $togoproject) {
            if ($togoproject->project_status == 0) {

                $statusAlert = '<div class="alert alert-warning"><i class="fa fa-info"></i> Awaiting</div>';
                $status = 'Awaiting';
                $awardLink = '';
                $editlink = '&nbsp&nbsp<a href="' . base_url('bidproject/submited_cegexp/' . $togoproject->fld_id . "/" . strtolower($status)) . '" class="btn btn-success btn-sm"><i id="faction" title="Edit Project" class="glyphicon glyphicon-edit icon-white"></i></a>&nbsp';
            } else if ($togoproject->project_status == 1) {
                
                $statusAlert = '<div class="alert alert-success"><i class="fa fa-info"></i> Won</div>';
                $status = 'Won';
                $editlink = '';
                $awardLink = '<a href="' . base_url('bidproject/awardproject?pn=' . $togoproject->fld_id) . '" class="btn btn-info" target="_blank">Award Project</a>';
            } else if ($togoproject->project_status == 2) {
                
                $statusAlert = '<div class="alert alert-danger"><i class="fa fa-info"></i> Loose</div>';
                $status = 'Loose';
                $awardLink = '';
                $editlink = '&nbsp&nbsp<a href="' . base_url('bidproject/submited_cegexp/' . $togoproject->fld_id . "/" . strtolower($status)) . '" class="btn btn-success btn-sm"><i id="faction" title="Edit Project" class="glyphicon glyphicon-edit icon-white"></i></a>&nbsp';
            } else if ($togoproject->project_status == 3) {
                
                $statusAlert = '<div class="alert alert-warning"><i class="fa fa-info"></i> Cancel</div>';
                $status = 'Cancel';
                $awardLink = '';
                $editlink = '&nbsp&nbsp<a href="' . base_url('bidproject/submited_cegexp/' . $togoproject->fld_id . "/" . strtolower($status)) . '" class="btn btn-success btn-sm"><i id="faction" title="Edit Project" class="glyphicon glyphicon-edit icon-white"></i></a>&nbsp';
            }

            if ($bdRole == 1) {
                $view1 = $statusAlert;
            } else if ($bdRole == 2) {
                $view2 = $statusAlert;
            } else {
                if ($togoproject->project_status == 0) {
                    $status1 = '<a class="btn btn-user mt-4" style="cursor: pointer;" onclick="assignpm(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#changestatus">' . $statusAlert . '</a>';
                } else {
                    $status1 = $statusAlert;
                }
                $view3 = $status1 . '&nbsp; &nbsp;' . $awardLink;
            }
            $detail = $togoproject->TenderDetails;
            $user = $this->SecondDB_model->getUserByID($togoproject->assign_to);
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ($togoproject->Expiry_Date == "Refer Document") ? "Null " : date("d-m-Y", strtotime($togoproject->Expiry_Date));
            $row[] = $togoproject->TenderDetails . '<hr><p><span style="float:left;" class="highlight">' . $togoproject->generated_tenderid . '</span><span style="float:right;" class="highlight">' . $user->userfullname . '</span></p>';
            $row[] = $togoproject->Location;
            $row[] = $togoproject->Organization;
            $row[] = $view1 . $view2 . $view3 ;
            $row[] = $editlink.'<div style="cursor:pointer" title="Add Reminder" data-toggle="modal" data-target="#clientmodel" onclick="editclientmodel(' . "'" . $togoproject->fld_id . "'" . ')" class="btn btn-danger btn-sm"><i class="fa fa-bell"></i></div>'. '&nbsp;&nbsp;<a onclick="datechangeproj(' . "'" . $togoproject->fld_id . "','" . date("Y-m-d", strtotime($togoproject->Expiry_Date)) . "'" . ')" data-toggle="modal" data-target="#changedate" class="btn btn-warning btn-sm "> <i class="fa fa-hourglass-end"></i> </a>';
            $data[] = $row;
        }

        /* foreach ($list as $togoproject) {
          if ($togoproject->project_status == 0) {
          $status = 'Awaiting';
          $awardLink = '';
          $editlink = '&nbsp&nbsp<a href="' . base_url('bidproject/submited_cegexp/' . $togoproject->project_id . "/" . strtolower($status)) . '"><i id="faction" title="Edit Project" class="glyphicon glyphicon-edit icon-white"></i></a>&nbsp';
          } else if ($togoproject->project_status == 1) {
          $status = 'Won';
          $editlink = '';
          $awardLink = '<a href="' . base_url('bidproject/awardproject?pn=' . $togoproject->project_id) . '" class="btn btn-info" target="_blank">Award Project</a>';
          } else if ($togoproject->project_status == 2) {
          $status = 'Loose';
          $awardLink = '';
          $editlink = '&nbsp&nbsp<a href="' . base_url('bidproject/submited_cegexp/' . $togoproject->project_id . "/" . strtolower($status)) . '"><i id="faction" title="Edit Project" class="glyphicon glyphicon-edit icon-white"></i></a>&nbsp';
          } else if ($togoproject->project_status == 3) {
          $status = 'Cancel';
          $awardLink = '';
          $editlink = '';
          }

          if ($bdRole == 1) {
          $view1 = $status;
          } else if ($bdRole == 2) {
          $view2 = $status;
          } else {
          if ($togoproject->project_status == 0) {
          $status1 = '<a class="btn btn-user" style="cursor: pointer;" onclick="assignpm(' . "'" . $togoproject->project_id . "'" . ')" data-toggle="modal" data-target="#changestatus">' . $status . '</a>';
          } else {
          $status1 = $status;
          }
          $view3 = $status1 . '&nbsp; &nbsp;' . $awardLink;
          }
          $detail = $togoproject->TenderDetails;
          $user = $this->SecondDB_model->getUserByID($togoproject->assign_to);
          $no++;
          $row = array();
          $row[] = $no;
          $row[] = ($togoproject->Expiry_Date == "Refer Document") ? "Null " : date("d-m-Y", strtotime($togoproject->Expiry_Date)) . '<a onclick="datechangeproj(' . "'" . $togoproject->project_id . "','" . date("Y-m-d", strtotime($togoproject->Expiry_Date)) . "'" . ')" data-toggle="modal" data-target="#changedate"> Edit </a>';
          $row[] = $togoproject->TenderDetails . '<hr><p><span style="float:left;">' . $togoproject->generated_tenderid . '</span><span style="float:right; color:green;">' . $user->userfullname . '</span></p>';
          $row[] = $togoproject->Location;
          $row[] = $togoproject->Organization;
          $row[] = $view1 . $view2 . $view3 . $editlink;
          $data[] = $row;
          } */
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->bidproject->count_all(),
            "recordsFiltered" => $this->bidproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

// ###############################################//
    /*     * *************code by durgesh (13-09-2019)****************** */
// ###############################################//
    // ###############################################//
    /*     * *************code by durgesh (13-09-2019)****************** */
// ###############################################//	
    //EOI Project
    public function submittedeoiproject() {
        $title = 'EOI Submitted Project';
        $sectorArr = $this->Front_model->GetActiveSector();
		// print_r($sectorArr); die;
        $secId = '0';
        $proposalManager = $this->Front_model->allprojectProposalManager();
		// print_r($proposalManager); die;
        //All Country..
        $CountryrecArr = $this->mastermodel->allcountry();
        // print_r($CountryrecArr); die;
		$stateDetails = $this->mastermodel->StateByCounID('100');
        $this->load->view('bidproject/eoi_view', compact('title', 'secId', 'sectorArr', 'proposalManager', 'CountryrecArr', 'stateDetails'));
    }

    // Project Display
    public function eoiProjectAll() {
        $list = $this->Bidprojecteoi_model->get_datatables();
        // echo '<pre>'; print_r($this->db->last_query()); die;
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $view1 = '';
        $view2 = '';
        $view3 = '';
        $awardLink = '';
        //echo '<pre>'; print_r($list);
        foreach ($list as $togoproject) {
            if ($togoproject->project_status == 0) {
                $status = 'Awaiting';
                $statusAlert = '<div class="alert alert-warning"><i class="fa fa-info"></i> Awaiting </div>';

                $awardLink = '';

                $editlink = '&nbsp&nbsp<a href="' . base_url('bidproject/submited_cegexp/' . $togoproject->fld_id . "/" . strtolower($status)) . '" class="btn btn-info btn-sm" ><i class="fa fa-edit"></i></a>&nbsp';
            } else if ($togoproject->project_status == 1) {
                $status = 'Won';
                $statusAlert = '<div class="alert alert-success"><i class="fa fa-info"></i> Won </div>';

                $editlink = '';


                


                $awardLink = '<a href="' . base_url('bidproject/awardproject?pn=' . $togoproject->fld_id) . '" class="btn btn-info" target="_blank" class="btn btn-success btn-sm" title="Award Project"><i class="fa fa-tasks"></i></a>';
            } else if ($togoproject->project_status == 2) {
                $status = 'Loose';

                $statusAlert = '<div class="alert alert-danger"><i class="fa fa-info"></i> Loose </div>';

                $awardLink = '';


  


                $editlink = '&nbsp&nbsp<a href="' . base_url('bidproject/submited_cegexp/' . $togoproject->fld_id . "/" . strtolower($status)) . '" title="Edit Project"><i class="fa fa-edit"></i></a>&nbsp';
            } else if ($togoproject->project_status == 3) {
                $status = 'Cancel';
                $statusAlert = '<div class="alert alert-danger"><i class="fa fa-info"></i> Cancel </div>';
                $awardLink = '';
                $editlink = '';
            }

            if ($bdRole == 1) {
                $view1 = $statusAlert;
            } else if ($bdRole == 2) {
                $view2 = $statusAlert;
            } else {
                if ($togoproject->project_status == 0) {
                    $status1 = '<a  style="cursor: pointer;" onclick="assignpm(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#changestatus">' . $statusAlert . '</a>';
                } else {
                    $status1 = $statusAlert;
                }
                $view3 = $status1 . '&nbsp; &nbsp;' . $awardLink;
            }
            $user = $this->SecondDB_model->getUserByID($togoproject->assign_to);
            $detail = $togoproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ($togoproject->Expiry_Date == "Refer Document") ? "Null " : date("d-m-Y", strtotime($togoproject->Expiry_Date));
            $row[] = $togoproject->TenderDetails . '<hr><p ><span style="float:left;" class="highlight pull-left">' . $togoproject->generated_tenderid . '</span><span style="float:right; color:green;" class="highlight pull-right">' . $user->userfullname . '</span></p>';
            $row[] = $togoproject->Location;
            $row[] = $togoproject->Organization;
            $row[] = $view1 . $view2 . $view3 ;




            $row[] = '&nbsp; <div style="cursor:pointer" class="btn btn-danger btn-sm" title="Add Reminder" data-toggle="modal" data-target="#clientmodel" onclick="editclientmodel(' . "'" . $togoproject->fld_id . "'" . ')"   ><i class="fa fa-bell"></i></div>'. $editlink. '<a onclick="datechangeproj(' . "'" . $togoproject->fld_id . "','" . date("Y-m-d", strtotime($togoproject->Expiry_Date)) . "'" . ')" data-toggle="modal" style="color:green;" data-target="#changedate" title="Change Date" class="btn btn-warning btn-sm">   <i class="fa fa-calendar"></i> </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Bidprojecteoi_model->count_filtered(),
            "recordsFiltered" => $this->Bidprojecteoi_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    // ###############################################//
    /*     * *************code by durgesh (13-09-2019)****************** */
// ###############################################//
    // ###############################################//
    /*     * *************code by durgesh (13-09-2019)****************** */
// ###############################################//	
    //RFP Project
    public function submittedrfpproject() {
        $title = 'RFP Submitted Project';
        $sectorArr = $this->Front_model->GetActiveSector();
        $secId = '0';
        $proposalManager = $this->Front_model->allprojectProposalManager();
        //All Country..
        $CountryrecArr = $this->mastermodel->allcountry();
        $stateDetails = $this->mastermodel->StateByCounID('100');
        $this->load->view('bidproject/rfp_view', compact('title', 'secId', 'sectorArr', 'proposalManager', 'CountryrecArr', 'stateDetails'));
    }
 
    public function rfpProjectAll() {
        $list = $this->Bidprojectrfp_model->get_datatables();
        // echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $view1 = '';
        $view2 = '';
        $view3 = '';
        $awardLink = '';
        //echo '<pre>'; print_r($list);
        foreach ($list as $togoproject) {
            if (($togoproject->project_status == '0') and ( $togoproject->project_status != '')) {
                $status = 'Awaiting';
                $statusAlert = '<span class="alert alert-warning"><i class="fa fa-info"></i> Awaiting </span>';
                $awardLink = '';
               
            } else if ($togoproject->project_status == 1) {
                $status = 'Won';
                $statusAlert = '<span class="alert alert-success"><i class="fa fa-info"></i> Won </span>';
                $editlink = '';
                // <a title="Award Project" href="' . base_url('bidproject/awardproject?pn=' . $togoproject->fld_id) . '" class="btn btn-success btn-sm" target="_blank"><i class="fa fa-trophy"></i></a>
                $awardLink = '
                ';
            } else if ($togoproject->project_status == 2) {
                $statusAlert = '<span class="alert alert-danger"><i class="fa fa-info"></i> Loose </span>';
                $status = 'Loose';
                
                $awardLink = '';
                
            } else if ($togoproject->project_status == 3) {
              
                $statusAlert = '<span class="alert alert-danger"><i class="fa fa-info"></i> Cancel </span>';
                $status = 'Cancel';
                $awardLink = '';
                $editlink = '';
            }

            if ($bdRole == 1) {
                $view1 = $statusAlert;
            } else if ($bdRole == 2) {
                $view2 = $statusAlert;
            } else {
                if ($togoproject->project_status == 0) {
                    $status1 = '<a class="btn btn-user" style="cursor: pointer;" onclick="assignpm(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#changestatus">' . $statusAlert . '</a>';
                } else {
                    $status1 = $statusAlert;
                }
                $view3 = $status1;
            }

             $editlink = '<a class="btn btn-info btn-sm" href="' . base_url('bidproject/submited_cegexp/' . $togoproject->fld_id . "/" . strtolower($status)) . '"><i id="faction" title="Edit Project" class="fa fa-edit"></i></a>';
            $user = $this->SecondDB_model->getUserByID($togoproject->assign_to);
            $detail = $togoproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ($togoproject->Expiry_Date == "Refer Document") ? "Null " : date("d-m-Y", strtotime($togoproject->Expiry_Date));
            $row[] = $togoproject->TenderDetails . '<hr> <div style="width: 100%; margin-bottom: 2px" class="highlight">' . $togoproject->generated_tenderid . '</div> <div style="width: 100%"  class="highlight">' . $user->userfullname . '</div>';
            $row[] = $togoproject->Location;
            $row[] = $togoproject->Organization;
            $row[] = $view1 . $view2 . $view3;
            $row[] =  $awardLink.$editlink.'&nbsp; <div style="cursor:pointer" title="Add Reminder" data-toggle="modal" data-target="#clientmodel" onclick="editclientmodel(' . "'" . $togoproject->fld_id . "'" . ')" class="btn btn-danger btn-sm"><i class="fa fa-bell"></i></div>'. '&nbsp;&nbsp;<a onclick="datechangeproj(' . "'" . $togoproject->fld_id . "','" . date("Y-m-d", strtotime($togoproject->Expiry_Date)) . "'" . ')" data-toggle="modal" data-target="#changedate" class="btn btn-warning btn-sm"> <i class="fa fa-hourglass-end"></i> </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Bidprojectrfp_model->count_filtered(),
            "recordsFiltered" => $this->Bidprojectrfp_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    //###############################################//
    /***************code by durgesh (13-09-2019)*******************/
    //###############################################//	

    //FQ Project
    public function submittedfqproject() {
        $title = 'FQ Submitted Project';
        
        $sectorArr = $this->Front_model->GetActiveSector();
        $secId = '0';
        $proposalManager = $this->Front_model->allprojectProposalManager();
        
        //All Country..
        $CountryrecArr = $this->mastermodel->allcountry();
        $stateDetails = $this->mastermodel->StateByCounID('100');
        $this->load->view('bidproject/fq_view', compact('title', 'secId', 'sectorArr', 'proposalManager', 'CountryrecArr', 'stateDetails'));
    }

    // Project Display
    public function fqProjectAll() {
        $list = $this->Bidprojectfq_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $view1 = '';
        $view2 = '';
        $view3 = '';
        $awardLink = '';
        //echo '<pre>'; print_r($list);
        foreach ($list as $togoproject) {
            if ($togoproject->project_status == 0) {
                $status = 'Awaiting';
                $awardLink = '';
                $editlink = '&nbsp&nbsp<a href="' . base_url('bidproject/submited_cegexp/' . $togoproject->fld_id . "/" . strtolower($status)) . '"><i id="faction" title="Edit Project" class="glyphicon glyphicon-edit icon-white"></i></a>&nbsp';
            } else if ($togoproject->project_status == 1) {
                $status = 'Won';
                $editlink = '';
                $awardLink = '<a title="Award Project" href="' . base_url('bidproject/awardproject?pn=' . $togoproject->fld_id) . '" class="btn btn-success btn-sm" target="_blank"><i class="fa fa-trophy"></i></a>';
            } else if ($togoproject->project_status == 2) {
                $status = 'Loose';
                $awardLink = '';
                $editlink = '&nbsp&nbsp<a href="' . base_url('bidproject/submited_cegexp/' . $togoproject->fld_id . "/" . strtolower($status)) . '"><i id="faction" title="Edit Project" class="glyphicon glyphicon-edit icon-white"></i></a>&nbsp';
            } else if ($togoproject->project_status == 3) {
                $status = 'Cancel';
                $awardLink = '';
                $editlink = '';
            }

            if ($bdRole == 1) {
                $view1 = $status;
            } else if ($bdRole == 2) {
                $view2 = $status;
            } else {
                if ($togoproject->project_status == 0) {
                    $status1 = '<a class="btn btn-user" style="cursor: pointer;" onclick="assignpm(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#changestatus">' . $status . '</a>';
                } else {
                    $status1 = $status;
                }
                $view3 = $status1;
            }
            $user = $this->SecondDB_model->getUserByID($togoproject->assign_to);
            $detail = $togoproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ($togoproject->Expiry_Date == "Refer Document") ? "Null " : date("d-m-Y", strtotime($togoproject->Expiry_Date)) . '<a onclick="datechangeproj(' . "'" . $togoproject->fld_id . "','" . date("Y-m-d", strtotime($togoproject->Expiry_Date)) . "'" . ')" data-toggle="modal" data-target="#changedate"> Edit </a>';
            $row[] = $togoproject->TenderDetails . '<hr><p><span style="float:left;">' . $togoproject->generated_tenderid . '</span><span style="float:right; color:green;">' . $user->userfullname . '</span></p>';
            $row[] = $togoproject->Location;
            $row[] = $togoproject->Organization;
            $row[] = $view1 . $view2 . $view3;
            $row[] = $awardLink.'&nbsp; <div style="cursor:pointer" class="btn btn-danger btn-sm" title="Add Reminder" data-toggle="modal" data-target="#clientmodel" onclick="editclientmodel(' . "'" . $togoproject->fld_id . "'" . ')"><i class="fa fa-bell"></i></div>'. $editlink;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Bidprojectfq_model->count_all(),
            "recordsFiltered" => $this->Bidprojectfq_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    // ###############################################//
    /*     * *************code by durgesh (13-09-2019)****************** */
// ###############################################//
    // ###############################################//
    /*     * *************code by durgesh (13-09-2019)****************** */
// ###############################################//	
    //Link View Details Popup Open Ash..
    public function projurlview() {
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('bd_tenderdetail', array('Tender_url'), array('fld_id' => $projId));
        if ($Rec) {
            $RowDataArr = $Rec->row();
        }
        $hyperlink = ($RowDataArr->Tender_url) ? $RowDataArr->Tender_url : 'http://www.cegindia.com/';
        $this->load->view('welcome_link', compact('projId', 'hyperlink'));
    }

    //Visibility Status
    public function visibilityStatus($copeName, $actid) {
        return $this->Front_model->updateRecord('bd_tenderdetail', array('visible_scope' => $copeName), array('fld_id' => $actid));
    }

    //Comment Showe
    public function commentset() {
        $tndrid = $_REQUEST['tndrid'];
        if (isset($tndrid)) {
            $Rec = $this->Front_model->selectRecordOrderByASC('comment_on_project', array('*'), array('is_active' => '1', 'project_id' => $tndrid));
            if ($Rec) {
                $data['allcomments'] = $Rec->result();
            }
            $this->load->view('comment_view', $data);
        }
    }

    //Comment Submit..
    public function commentsubmit() {
        $projId = $_REQUEST['projId'];
        $comnt = $_REQUEST['comnt'];
        if ($comnt):
            $insertArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $projId, 'sectorId' => '0', 'commentText' => $comnt);
            $Rec = $this->Front_model->insertRecord('comment_on_project', $insertArr);
            $this->notified($projId, 'A new Comment on Tender.');
        endif;
        return $Rec;
    }

    //Submit Project Concern Details..
    public function projectconcerndetails() {
        //Get Project Id..
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('project_concern_details', array('concern_details'), array('project_id' => $projId));
        if ($Rec != '') {
            $data = $Rec->result();
        } else {
            $data[0] = '';
        }
        $this->load->view('welcome_message', compact('projId', 'data'));
    }

    //For No Go Project set..
    public function nogosubmit() {
        if ($_REQUEST['actid']) {
            $inserArr = array('user_id' => $this->session->userdata('loginid'), 'project_id' => $_REQUEST['actid'], 'actionIP' => get_client_ip());
            $Respon = $this->Front_model->insertRecord('proj_nogo_for_user', $inserArr);
        }
        if ($Respon > 0):
            $this->visibilityStatus('No_Go_project', $_REQUEST['actid']);
            $this->notified($_REQUEST['actid'], 'Tender Status Changed as No Go.');
            $this->session->set_flashdata('msg', "Project Status  Changed No Go . ");
            redirect(base_url('/bidproject'));
        endif;
    }

    public function notified($tendrId, $notifData) {
        $rowArrData = NotifiedUserIDs($this->session->userdata('loginid'));
        foreach ($rowArrData as $rowRec) {
            $insertArr = array('user_to_notify' => $rowRec, 'user_who_fired_event' => $this->session->userdata('loginid'),
                'event_id_project' => $tendrId, 'notification_data' => $notifData);
            $Record = $this->Front_model->insertRecord('notification', $insertArr);
        }
    }

    public function awardproject() {
        $title = 'Submitted Project |  Award Project Detail';
        $assigntoArr = $this->db->query('select bd.fld_id,bd.TenderDetails,gt.generated_tenderid from bd_tenderdetail as bd left join tender_generated_id as gt on bd.fld_id=gt.project_id where bd.fld_id=' . $_REQUEST['pn']);
        $dataArr = $assigntoArr->result();
        $this->load->view('/bidproject/award_view', compact('dataArr', 'title'));
    }

    public function tenderteam_view() {
        //print_r($_REQUEST['pn']); die;
        $this->db->select('a.*,b.designation_name,c.TenderDetails,d.generated_tenderid');
        $this->db->from('team_assign_member as a');
        $this->db->join('designation_master_requisition as b', 'a.designation_id = b.fld_id', 'left');
        $this->db->join('bd_tenderdetail as c', 'a.project_id = c.fld_id', 'left');
        $this->db->join('tender_generated_id as d', 'a.project_id = d.project_id', 'left');
        $this->db->where('a.project_id', $_REQUEST['pn']);
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
        } else {
            return false;
        }
        $userData = $this->Front_model->GetAllUserRec();
        $DesignationArr = $this->Front_model->allActiveDesignation();
        $this->load->view('/bidproject/tenderteam_view', compact('resulstArr', 'DesignationArr', 'userData'));
    }

    public function negotiationteam_view() {
        //print_r($_REQUEST['pn']); die;
        $this->db->select('a.*,b.designation_name,c.TenderDetails,d.generated_tenderid');
        $this->db->from('team_negotiation_member as a');
        $this->db->join('designation_master_requisition as b', 'a.designation_id = b.fld_id', 'left');
        $this->db->join('bd_tenderdetail as c', 'a.project_id = c.fld_id', 'left');
        $this->db->join('tender_generated_id as d', 'a.project_id = d.project_id', 'left');
        $this->db->where('a.project_id', $_REQUEST['pn']);
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
        } else {
            return false;
        }
        $userData = $this->Front_model->GetAllUserRec();
        $DesignationArr = $this->Front_model->allActiveDesignation();
        $this->load->view('/bidproject/negotiationteam_view', compact('resulstArr', 'DesignationArr', 'userData'));
    }

    public function contractteam_view() {
        //print_r($_REQUEST['pn']); die;
        $this->db->select('a.*,b.designation_name,c.TenderDetails,d.generated_tenderid');
        $this->db->from('team_contract_member as a');
        $this->db->join('designation_master_requisition as b', 'a.designation_id = b.fld_id', 'left');
        $this->db->join('bd_tenderdetail as c', 'a.project_id = c.fld_id', 'left');
        $this->db->join('tender_generated_id as d', 'a.project_id = d.project_id', 'left');
        $this->db->where('a.project_id', $_REQUEST['pn']);
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
        } else {
            return false;
        }
        $userData = $this->Front_model->GetAllUserRec();
        $DesignationArr = $this->Front_model->allActiveDesignation();
        $this->load->view('/bidproject/contractteam_view', compact('resulstArr', 'DesignationArr', 'userData'));
    }

    public function saveteamdata() {
        $data = $_REQUEST;
        //echo '<pre>'; print_r($data); die;
        for ($i = 0; $count = count($data['team_id']), $i < $count; $i++) {
            $updaterecords = array('age_limit' => $data['age_limit'][$i],
                'man_months' => $data['man_months'][$i],
                'construction_months' => $data['construction_months'][$i],
                'development_months' => $data['development_months'][$i],
                'om_months' => $data['om_months'][$i],
                'user_id' => $data['username'][$i],
                'otheruser_name' => $data['other'][$i],
                'emailaddress' => $data['emailaddress'][$i],
                'contactnumber' => $data['contactnumber'][$i],
                'marks' => $data['marks'][$i],
                'firm' => $data['firm'][$i],
                'certificate' => $data['certificate'][$i],
                'undertaking' => $data['undertaking'][$i],
                'salary' => $data['salary'][$i],
                'remarks' => $data['remarks'][$i],
            );
            $where = array('id' => $data['team_id'][$i]);
            $updateID = $this->Front_model->updateRecord('team_negotiation_member', $updaterecords, $where);
        }
        if ($data['url_chk'] == '1') {
            redirect(base_url('/bidproject/negotiationteam_view?pn=' . $data['project_id']));
        }
    }

    public function locadata() {
        $data = $_REQUEST;
        $bdRole = $this->Front_model->bd_rolecheck();
        if (loginUserDept() == 14) {
            $cruID = $this->session->userdata('loginid');
            $updates = array('lock_cru' => 1, 'lock_by_cru' => $cruID);
            $wheres = array('id' => $data['id']);
            $updateIDs = $this->Front_model->updateRecord('team_negotiation_member', $updates, $wheres);
        } else {
            if ($bdRole == 1) {
                $cruID = $this->session->userdata('loginid');
                $updates = array('lock_manager' => 1, 'lock_by_manager' => $cruID);
                $wheres = array('id' => $data['id']);
                $updateIDs = $this->Front_model->updateRecord('team_negotiation_member', $updates, $wheres);
            }//elseif($bdRole == 2){
            //}
            else {
                $cruID = $this->session->userdata('loginid');
                $updates = array('lock_mgmt' => 1, 'lock_by_mgmt' => $cruID);
                $wheres = array('id' => $data['id']);
                $updateIDs = $this->Front_model->updateRecord('team_negotiation_member', $updates, $wheres);
            }
        }
        if (!empty($data['id'])) {
            $this->db->select('a.*,b.designation_name,c.TenderDetails,d.generated_tenderid');
            $this->db->from('team_negotiation_member as a');
            $this->db->join('designation_master_requisition as b', 'a.designation_id = b.fld_id', 'left');
            $this->db->join('bd_tenderdetail as c', 'a.project_id = c.fld_id', 'left');
            $this->db->join('tender_generated_id as d', 'a.project_id = d.project_id', 'left');
            $this->db->where('a.id', $data['id']);
            $query = $this->db->get();
            if ($query->num_rows() != 0) {
                $resulstArr = $query->result_array();
                //echo '<pre>'; print_r($resulstArr);
                foreach ($resulstArr as $rowRec) {
                    $insertArr = array('team_assignmember_id' => $rowRec['id'],
                        'project_id' => $rowRec['project_id'],
                        'project_no' => $rowRec['project_no'],
                        'proposal_mngr_id' => $rowRec['proposal_mngr_id'],
                        'designation_id' => $rowRec['designation_id'],
                        'age_limit' => $rowRec['age_limit'],
                        'man_months' => $rowRec['man_months'],
                        'construction_months' => $rowRec['construction_months'],
                        'development_months' => $rowRec['development_months'],
                        'om_months' => $rowRec['om_months'],
                        'user_id' => $rowRec['user_id'],
                        'otheruser_name' => $rowRec['otheruser_name'],
                        'emailaddress' => $rowRec['emailaddress'],
                        'contactnumber' => $rowRec['contactnumber'],
                        'marks' => $rowRec['marks'],
                        'firm' => $rowRec['firm'],
                        'certificate' => $rowRec['certificate'],
                        'undertaking' => $rowRec['undertaking'],
                        'salary' => $rowRec['salary'],
                        'remarks' => $rowRec['remarks'],
                        'req_status' => $rowRec['req_status'],
                        'created' => $rowRec['created'],
                        'modified' => $rowRec['modified'],
                    );
                    $Record = $this->Front_model->insertRecord('team_contract_member', $insertArr);
                }
            }
        }
        exit;
    }

    public function contractsaveteamdata() {
        $data = $_REQUEST;
        //echo '<pre>'; print_r($data); die;
        for ($i = 0; $count = count($data['team_id']), $i < $count; $i++) {
            $updaterecords = array('age_limit' => $data['age_limit'][$i],
                'man_months' => $data['man_months'][$i],
                'construction_months' => $data['construction_months'][$i],
                'development_months' => $data['development_months'][$i],
                'om_months' => $data['om_months'][$i],
                'user_id' => $data['username'][$i],
                'otheruser_name' => $data['other'][$i],
                'emailaddress' => $data['emailaddress'][$i],
                'contactnumber' => $data['contactnumber'][$i],
                'marks' => $data['marks'][$i],
                'firm' => $data['firm'][$i],
                'certificate' => $data['certificate'][$i],
                'undertaking' => $data['undertaking'][$i],
                'salary' => $data['salary'][$i],
                'remarks' => $data['remarks'][$i],
            );
            $where = array('id' => $data['team_id'][$i]);
            $updateID = $this->Front_model->updateRecord('team_contract_member', $updaterecords, $where);
        }
        if ($data['url_chk'] == '1') {
            redirect(base_url('/bidproject/contractteam_view?pn=' . $data['project_id']));
        }
    }

    public function contractlocadata() {
        $data = $_REQUEST;
        $bdRole = $this->Front_model->bd_rolecheck();
        if (loginUserDept() == 14) {
            $cruID = $this->session->userdata('loginid');
            $updates = array('lock_cru' => 1, 'lock_by_cru' => $cruID);
            $wheres = array('id' => $data['id']);
            $updateIDs = $this->Front_model->updateRecord('team_contract_member', $updates, $wheres);
        } else {
            if ($bdRole == 1) {
                $cruID = $this->session->userdata('loginid');
                $updates = array('lock_manager' => 1, 'lock_by_manager' => $cruID);
                $wheres = array('id' => $data['id']);
                $updateIDs = $this->Front_model->updateRecord('team_contract_member', $updates, $wheres);
            }//elseif($bdRole == 2){
            //}
            else {
                $cruID = $this->session->userdata('loginid');
                $updates = array('lock_mgmt' => 1, 'lock_by_mgmt' => $cruID);
                $wheres = array('id' => $data['id']);
                $updateIDs = $this->Front_model->updateRecord('team_contract_member', $updates, $wheres);
            }
        }

        exit;
    }

    //Code By Asheesh...
    public function ajax_proj_detailsById() {
        $projId = $_REQUEST['projId'];
        $listPrj = $this->bidproject->get_bidprojects($projId);
        echo json_encode($listPrj);
        exit;
    }

    //Code By Asheesh...
    public function ajaxasheesh() {
        //$projId = $_REQUEST['projId'];
        $projId = 2780;
        $listPrj = $this->bidproject->get_bidprojects($projId);
        echo "<pre>";
        print_r($listPrj);
        die;
    }

    //Submited To CEG Exp..
    /* public function submited_cegexp() {
      $projstatus = '';
      $cegexpID = $this->uri->segment(3);
      @$projstatus = $this->uri->segment(4);
      $recordArr = $this->bidproject->get_bidprojects($cegexpID);
      $projcode = $recordArr->generated_tenderid;
      $ProjrecArr = $this->mastermodel->SelectRecordFld('ceg_exp', array('project_code' => $projcode));
      if (count($ProjrecArr) < 1) {
      $insertedArr = array('project_name' => $recordArr->TenderDetails, 'client' => '', 'project_code' => $projcode, 'proj_status' => $projstatus, 'project_id' => $cegexpID);
      $resp = $this->mastermodel->InsertMasterData($insertedArr, 'ceg_exp');
      // echo @$projstatus."Insrt".$ProjrecArr[0]->fld_id; die;
      // redirect(base_url('bidproject'));
      if ((@$projstatus == 'loose') or ( @$projstatus == 'awaiting')) {
      redirect(base_url('company/cegexpedit/' . $ProjrecArr[0]->fld_id));
      } else {
      redirect(base_url('bidproject'));
      }
      }
      //Update Ceg EXP..
      if (count($ProjrecArr) > 0) {
      $updArr = array('proj_status' => $projstatus, 'project_id' => $cegexpID);
      $respupd = $this->mastermodel->UpdateRecords('ceg_exp', array('project_code' => $projcode), $updArr);
      // echo @$projstatus."UPD".$ProjrecArr[0]->fld_id; die;
      if ((@$projstatus == 'loose')or ( @$projstatus == 'awaiting')) {
      redirect(base_url('company/cegexpedit/' . $ProjrecArr[0]->fld_id));
      } else {
      redirect(base_url('bidproject'));
      }
      }
      redirect(base_url('bidproject'));
      } */


    public function submited_cegexp() {
        $projstatus = '';
        $cegexpID = $this->uri->segment(3);
        @$projstatus = $this->uri->segment(4);
        $recordArr = $this->bidproject->get_bidprojects($cegexpID);
        $ProjrecArr = $this->mastermodel->SelectRecordFldNew('bdceg_exp', array('project_id' => $cegexpID));
        if (count($ProjrecArr) < 1) {
            $insertedArr = array('project_id' => $cegexpID);
            $resp = $this->mastermodel->InsertMasterData($insertedArr, 'bdceg_exp');
        }

        $ProjrecArr1 = $this->mastermodel->SelectRecordFldNew('bdcegexp_proj_summery', array('project_id' => $cegexpID));
        if (count($ProjrecArr1) < 1) {
            $insertedArr1 = array('project_id' => $cegexpID);
            $resp1 = $this->mastermodel->InsertMasterData($insertedArr1, 'bdcegexp_proj_summery');
        }

        redirect(base_url('company/cegexpedit/' . $cegexpID));
        //Update Ceg EXP..
        /* if (count($ProjrecArr) > 0) {
          $updArr = array('proj_status' => $projstatus, 'project_id' => $cegexpID);
          $respupd = $this->mastermodel->UpdateRecords('ceg_exp', array('project_code' => $projcode), $updArr);
          // echo @$projstatus."UPD".$ProjrecArr[0]->fld_id; die;
          if ((@$projstatus == 'loose')or ( @$projstatus == 'awaiting')) {
          redirect(base_url('company/cegexpedit/' . $ProjrecArr[0]->fld_id));
          } else {
          redirect(base_url('bidproject'));
          }
          }
          redirect(base_url('bidproject')); */
    }

    //Update Submitted Exp Date...
    public function changeprojdate() {
        $respupd = $this->mastermodel->UpdateRecords(' bd_tenderdetail', array('fld_id' => $_REQUEST['updproject_id']), array('Expiry_Date' => $_REQUEST['upd_date']));
        //$this->session->set_flashdata('msg', 'Exp. Date Updated successfully');
        //redirect(base_url('bidproject'));

        $message = "Exp. Date Updated successfully";
        $output = array(
        
            "msg" => $message,
        );
        echo json_encode($output);
    }

    public function reminderinfo() {
        if ($_REQUEST['reminder_date'] && $_REQUEST['reminder_subject'] && $_REQUEST['reminder_message']) {
            $_REQUEST['entryby'] = $this->session->userdata('loginid');
            $res = $this->Front_model->insertRecord('reminderinfo', $_REQUEST);
        }
        if ($res):
            $data['arr'] = $_REQUEST;
            $data['mailby'] = $this->SecondDB_model->getUserByID($this->session->userdata('loginid'));
            $data['tenderdetail'] = $this->mastermodel->SelectRecordSingle('bd_tenderdetail', array('fld_id' => $_REQUEST['project_id']));
            $msgDetails = $this->load->view('email_template/reminder_email', $data, true);
            $emailsAddress = $this->Front_model->ReminderEmail();
            $emailArrStr77 = 'bdcru@cegindia.com';
            //sendMail("$emailArrStr77", $_REQUEST['reminder_subject'], $msgDetails);
            
            //$this->session->set_flashdata('msg', "Reminder Successfully Added.");

            $message = "Reminder Successfully Added.";
        else:
            //$this->session->set_flashdata('msg', "Something Went Wrong.");
            $message = "Something Went Wrong.";
        endif;
        //redirect(base_url('/bidproject'));

        $output = array(
        
            "msg" => $message,
        );
        echo json_encode($output);
    }

    public function ajax_reminderinfo() {
        $client_Arr = $this->mastermodel->SelectRecord('reminderinfo', array('project_id' => $_REQUEST['project_id']));
        if ($client_Arr) {
            echo json_encode($client_Arr);
        } else {
            return false;
        }
    }

    public function ajax_reminderdel() {
        $id = $this->uri->segment(3);
        if ($id) {
            $this->db->delete('reminderinfo', array('id' => $id));
        }
        redirect(base_url('/bidproject'));
    }

    public function reminderCorn() {
        $beforedate = date("Y-m-d", strtotime('-2 days'));
        $currdate = date("Y-m-d");
        $this->db->select('*');
        $this->db->from('reminderinfo');
        $where = "(reminder_date >= '$beforedate' AND reminder_date <= '$currdate')";
        $this->db->where($where);
        $res = $this->db->get()->result();
        foreach ($res as $val) {
            $data['arr'] = array(
                'reminder_date' => $val->reminder_date,
                'reminder_subject' => $val->reminder_subject,
                'reminder_message' => $val->reminder_message
            );
            $data['mailby'] = $this->SecondDB_model->getUserByID($val->entryby);
            $data['tenderdetail'] = $this->mastermodel->SelectRecordSingle('bd_tenderdetail', array('fld_id' => $val->project_id));
            $msgDetails = $this->load->view('email_template/reminder_email', $data, true);
            $emailsAddress = $this->Front_model->ReminderEmail();
            //$emailArrStr77 = implode(",", $emailsAddress);
            $emailArrStr77 = 'bdcru@cegindia.com';
            sendMail("$emailArrStr77", $val->reminder_subject, $msgDetails);
            //sendMail("jitendra00752@gmail.com", $val->reminder_subject, $msgDetails);
            echo $val->project_id . '<br/>';
        }
    }

    //Project New Report With Compt Code By asheesh..
    public function bidnewreport() {
        $title = 'Report Project With Competitor ';
        $sectorArr = $this->Front_model->GetActiveSector();
        $secId = '0';
        $proposalManager = $this->Front_model->allprojectProposalManager();
        //All Country..
        $CountryrecArr = $this->mastermodel->allcountry();
        $stateDetails = $this->mastermodel->StateByCounID('100');
        $this->load->view('bidproject/bidnewreport_view', compact('title', 'secId', 'sectorArr', 'proposalManager', 'CountryrecArr', 'stateDetails'));
    }

    //Code By Asheesh 12-12-2018..
    // Project Display
    public function ajaxnewprojectreport() {
        $list = $this->bidproject->get_datatables();
        //echo '<pre>'; print_r($this->db->last_query()); die;
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $view1 = '';
        $view2 = '';
        $view3 = '';
        $awardLink = '';
        //print_r($list);



        foreach ($list as $togoproject) {

            if ($togoproject->project_status == 0) {
                $statusAlert = '<div class="alert alert-warning"><i class="fa fa-info"></i> Awaiting</div>';

                $status = 'Awaiting';
                $awardLink = '';
            } else if ($togoproject->project_status == 1) {
                $statusAlert = '<div class="alert alert-success"><i class="fa fa-info"></i> Won</div>';
                $status = 'Won';
                $awardLink = 'Award Project';
            } else if ($togoproject->project_status == 2) {
                $statusAlert = '<div class="alert alert-danger"><i class="fa fa-info"></i> Loose</div>';
                $status = 'Loose';
                $awardLink = '';
            } else if ($togoproject->project_status == 3) {
                $statusAlert = '<div class="alert alert-warning"><i class="fa fa-info"></i> Cancel</div>';
                $status = 'Cancel';
                $awardLink = '';
            }
            if ($bdRole == 1) {
                $view1 = $statusAlert;
            } else if ($bdRole == 2) {
                $view2 = $statusAlert;
            } else {
                if ($togoproject->project_status == 0) {
                    $status1 = '' . $statusAlert . '';
                } else {
                    $status1 = $statusAlert;
                }
                $view3 = $status1 . '&nbsp; &nbsp;' . $awardLink;
            }

            $detail = $togoproject->TenderDetails;
            $user = $this->SecondDB_model->getUserByID($togoproject->assign_to);
            $ActID = $togoproject->fld_id;

            // echo '<pre>'; print_r($list); die;
            $myRecArr = $this->getcomptdetailrank($ActID);
            $resultStrng = '';
            $resultStrng2 = '';
            $winnerD = '';


            //echo '<pre>'; print_r($list); die;

            if ($myRecArr):
                foreach ($myRecArr as $rowR) {
                    $rowR['compname'];
                    $rowR['ranker'];
                    $resultStrng .= "<small>Company :" . $rowR['compname'] . " , Rank :" . $rowR['ranker'] . "</small><hr>";
                    $resultStrng2 .= "<small>Company :" . $rowR['compname'] . "</small><hr>";

                    if ($rowR['ranker'] == '1') {
                        $winnerD = "<small>" . $rowR['compname'] . "</small>";
                    }
                }
            endif;
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = ($togoproject->Expiry_Date == "Refer Document") ? "Null " : date("d-m-Y", strtotime($togoproject->Expiry_Date)) . '';
            $row[] = $togoproject->TenderDetails . '<hr> <div   class="highlight mb-2">' . $togoproject->generated_tenderid . '</div><div   class="highlight">' . $user->userfullname . '</div>';
            $row[] = $togoproject->Location;
            $row[] = $togoproject->Organization;
            $row[] = $view1 . $view2 . $view3;

            if ($togoproject->project_status == 1) {
                $row[] = $resultStrng;
                $row[] = $winnerD;
            }
            if ($togoproject->project_status == 2) {
                $row[] = $resultStrng;
                $row[] = $winnerD;
            }
            if ($togoproject->project_status == 3) {
                $row[] = '';
                $row[] = '';
            }
            if ($togoproject->project_status == 0) {
                $row[] = $resultStrng2;
                $row[] = '';
            }

            $data[] = $row;
        }


         // print_r($this->bidproject->count_filtered()); die;


        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->bidproject->count_filtered(),
            "recordsFiltered" => $this->bidproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }



public function projectreport() {
        $title = 'Project Report';
        $tmProject = $this->Front_model->getAllProject();
        $employee = $this->Front_model->getAllemployees();
        $this->db->select('fld_id,designation_name');
        $this->db->from('designation_master');
        $this->db->where('is_active', '1');
        $designation = $this->db->get()->result_object();
        //echo '<pre>'; print_r($employee); die;
        $this->load->view('projectplanning/projectreport_view', compact('designation', 'title', 'tmProject', 'employee'));
    }




      public function educationalreport() {
        $data['error'] = '';
        $data['title'] = "Educational Report";
        $this->load->view('projectplanning/educationalreport_view', $data);
    }
    public function getcomptdetailrank($recID) {
        $recArr = $this->GetComptDetailsById($recID);
        $cegcompcomp = $recArr['cegcompcomp'];
        //start Rank Code
        $temrArr = array();
        foreach ($cegcompcomp as $comptrow) {
            $totRec7 = ($comptrow->technical_marks + $comptrow->financial_marks);
            $temrArr[$comptrow->fld_id] = number_format($totRec7, 2);
        }
        arsort($temrArr);
        $finalRank = array();
        $nnmms = 1;
        foreach ($temrArr as $kky => $rwrank):
            $finalRank[$kky] = $nnmms;
            $nnmms++;
        endforeach;
        //Close..
        $returnArr = array();
        $ssnn = 0;
        if ($cegcompcomp):
            foreach ($cegcompcomp as $comptrow) {
                $arrayD = array();
                $compName = $this->mastermodel->GetCompNameById($comptrow->compt_comp_id);
                $rannK = $finalRank[$comptrow->fld_id];
                $arrayD['compname'] = $compName;
                $arrayD['ranker'] = $rannK;
                array_push($returnArr, $arrayD);
            }
        endif;
        return $returnArr;
    }

    public function GetComptDetailsById($cegexpID) {
        $projstatus = '';
        $compnameArr = array();
        if ($cegexpID) {
            // Find Data
            $data['bddetail'] = $this->mastermodel->SelectRecordSingle('bd_tenderdetail', array('fld_id' => $cegexpID));
            $data['project_status'] = $this->mastermodel->SelectRecordSingle('bdproject_status', array('project_id' => $cegexpID));
            //bdcegexp data
            $this->db->select('a.*,c.service_name,d.generated_tenderid');
            $this->db->from('bdceg_exp as a');
            $this->db->join('main_services as c', 'a.service_id = c.id', 'left');
            $this->db->join('tender_generated_id as d', 'a.project_id = d.project_id', 'left');
            $this->db->where('a.project_id', $cegexpID);

            $resultdata = $this->db->get()->result();
            $data['expres'] = $resultdata[0];
            $data['projdetail'] = $this->mastermodel->SelectRecordSingle('project_description', array('project_id' => $cegexpID));
            //$country_id = $data['expres']->countries_id;
            $country_id = $data['projdetail']->country_id;
            $bid_securitytype = $data['projdetail']->bid_securitytype;
            $sectors = $data['projdetail']->sector_id;
            $service_id = $data['projdetail']->service_id;
            $data['cegcompcomp'] = $this->mastermodel->SelectRecordFldNew('cegexp_competitor_comp', array('project_id' => $cegexpID, 'status' => '1'));



            return $data;
        }
    }

    //Reload For Final Marks...
    //Score Reload And Update Financial.. .
    public function reloadscore_finan() {
        $projID = $this->uri->segment(3);
        $recArr = $this->mastermodel->SelectRecordFld('cegexp_competitor_comp', array('project_id' => $projID, 'status' => '1'));
        $getminVal = $this->mastermodel->getMinVal('cegexp_competitor_comp', 'financial_score', array('project_id' => $projID, 'status' => '1'));
        if ($recArr):
            foreach ($recArr as $rowdt) {
                $recUpd = 0;
                $rrrFinal = 0;
                if (($rowdt->finan_weightage) and ( $rowdt->financial_score)) {
                    $recUpd = ($getminVal / $rowdt->financial_score);
                    $rrrFinal = $recUpd * $rowdt->finan_weightage;
                    $this->mastermodel->UpdateRecords('cegexp_competitor_comp', array('fld_id' => $rowdt->fld_id), array('financial_marks' => $rrrFinal));
                }
            }
        endif;
        $this->session->set_flashdata('msg', 'Financial Score Updated successfully');
        redirect(base_url('company/cegexpedit/' . $projID));
    }

    //code by durgesh(12-09-2019) for get jv record\
    public function GetJvCompanyList($jv_ids) {
        //echo '<pre>';
        //print_r($jv_ids);
        //die;
        $this->db->select('company_name');
        $this->db->from('main_company');
        $this->db->where('status', '1');
        $this->db->where_in('fld_id', $jv_ids);
        $jv_company_result = $this->db->get()->result();
        $D3 = '';
        if ($jv_company_result) {
            foreach ($jv_company_result as $jv_row) {
                $D3 .= '<span class="btn btn-danger" style="margin-left:20px;">' . $jv_row->company_name . ' <b style="color:#222;">(JV)</b></span>' . "<br><br>";
            }
            return $D3;
        }
    }

    // ###############################################//
    /*     * *************code by durgesh (13-09-2019)****************** */
// ###############################################//
    public function GetAssocCompanyList($assoc_ids) {
        $this->db->select('company_name');
        $this->db->from('main_company');
        $this->db->where('status', '1');
        $this->db->where_in('fld_id', $assoc_ids);
        $assoc_company_result = $this->db->get()->result();
        $D3 = '';
        if ($assoc_company_result) {
            foreach ($assoc_company_result as $assoc_row) {
                $D3 .= '<span class="btn btn-danger" style="margin-left:20px;">' . $assoc_row->company_name . ' <b style="color:#222;">(ASSOC)</b></span>' . "<br><br>";
            }
            return $D3;
        }
    }

    // ###############################################//
    /*     * *************code by durgesh (13-09-2019)****************** */
// ###############################################//
    public function GetLastComment($projid) {
        $this->db->select('commentText');
        $this->db->from('comment_on_project');
        $this->db->where(array('is_active' => '1', 'project_id' => $projid));
        $this->db->order_by('fld_id', 'DESC');
        $resultcmt = $this->db->get()->row();
        if ($resultcmt) {
            return ($resultcmt) ? $resultcmt : '';
        }
    }

    // ###############################################//
    /***************code by durgesh (13-09-2019)*******************/
// ###############################################//	
    //New Function By Asheesh 14-09-2019...
    //Ajax For Show If Exist..
    public function showjvrecord($projId) {
        //$projId = '59104';
        $Rec = $this->Front_model->selectRecord('jv_companyrecords', array('*'), array('status' => '1', 'project_id' => $projId));
        if ($Rec) {
            $recData = $Rec->result();
            $LC = '';
            $jv = '';
            $as = '';
            $lead_companes = explode(",", $recData[0]->lead_companes);
            $joint_ventures = explode(",", $recData[0]->joint_ventures);
            $associate_company = explode(",", $recData[0]->associate_company);
            $entry_by = $recData[0]->entry_by;

            foreach ($lead_companes as $r12) {
				if($r12){
                $LC .= '<a href="'.base_url("company/addcompany_extra?compnm=$r12").'" style="color:green;">'.$this->mastermodel->GetCompNameById($r12) . "</a> <b>(Lead)</b><br/>";
                }
			}
		   foreach ($joint_ventures as $r11) {
				if($r11){
                $jv .= '<<a href="'.base_url("company/addcompany_extra?compnm=$r11").'" style="color:green;">'.$this->mastermodel->GetCompNameById($r11) . "</a> <b>(JV)</b><br/>";
                }
		   }
            foreach ($associate_company as $r13) {
				if($r13){
                $as .= '<a href="'.base_url("company/addcompany_extra?compnm=$r13").'" style="color:green;">'.$this->mastermodel->GetCompNameById($r13) . "</a> <b>(Associate)</b><br/>";
			   }
			}
            $returnVar = '';
            if (($LC)) {
                $returnVar .= '<div style="width: 200px" class="highlight mt-1">' . $LC . '</div>';
            }
            if (($jv)) {
                $returnVar .= '<div style="width: 200px" class="highlight mt-1">' . $jv . '</div>';
            }
            if (($as)) {
                $returnVar .= '<div style="width: 200px" class="highlight mt-1">' . $as . '</div>';
            }
            $returnVar .= '';
            return $returnVar;
        } else {
            return null;
        }
    }

    //Code Change 14-09-2019...
    public function newProjectnewAll() {
        $list = $this->bidproject->get_datatables();
        // echo '<pre>';
        // print_r($list);
        // die;
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $view1 = '';
        $view2 = '';
        $view3 = '';
        $awardLink = '';

        foreach ($list as $togoproject) {
            if ($togoproject->project_status == 0) {
                $statusAlert = '<div class="alert alert-warning"><i class="fa fa-info"></i> Awaiting</div>';
              
                $status = 'Awaiting';
                $awardLink = '';
                //<i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>
                $editlink = '&nbsp&nbsp<a href="' . base_url('bidproject/submited_cegexp/' . $togoproject->fld_id . "/" . strtolower($status)) . '"  class="btn btn-info btn-sm"><i id="faction" title="Edit Project" class="glyphicon glyphicon-edit icon-white"></i></a>&nbsp';
            } else if ($togoproject->project_status == 1) {
                $statusAlert = '<div class="alert alert-success"><i class="fa fa-info"></i> Won</div>';
               
                $status = 'Won';
                $editlink = '';
                // <i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>
                $awardLink = '&nbsp&nbsp<a href="' . base_url('bidproject/awardproject?pn=' . $togoproject->fld_id) . '" class="btn btn-info" target="_blank">Award Project</a>';
            } else if ($togoproject->project_status == 2) {
                $statusAlert = '<div class="alert alert-danger"><i class="fa fa-info"></i> Loose</div>';
             
                $status = 'Loose';
                $awardLink = '';
                // <i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>
                $editlink = '&nbsp&nbsp<a href="' . base_url('bidproject/submited_cegexp/' . $togoproject->fld_id . "/" . strtolower($status)) . '"  class="btn btn-info btn-sm"><i id="faction" title="Edit Project" class="glyphicon glyphicon-edit icon-white"></i></a>&nbsp';
            } else if ($togoproject->project_status == 3) {
                $statusAlert = '<div class="alert alert-warning"><i class="fa fa-info"></i> Cancel</div>';
                $status = 'Cancel';
                $awardLink = '';
                $editlink = '';
            }

            if ($bdRole == 1) {
                $view1 = $statusAlert;
            } else if ($bdRole == 2) {
                $view2 = $statusAlert;
            } else {
                if ($togoproject->project_status == 0) {
                    $status1 = '<a class="btn btn-user" style="cursor: pointer;" onclick="assignpm(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#changestatus">' . $statusAlert . '</a>';
                } else {
                    $status1 = $statusAlert;
                }
                $view3 = $status1 . '&nbsp; &nbsp;' . $awardLink;
            }
            $user = $this->SecondDB_model->getUserByID($togoproject->assign_to);
            $lastcmt = $this->GetLastComment($togoproject->fld_id);

            $detail = $togoproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ($togoproject->Expiry_Date == "Refer Document") ? "Null " : date("d-m-Y", strtotime($togoproject->Expiry_Date));
            $row[] = $togoproject->TenderDetails . '<hr><p><span style="float:left;" class="highlight">' . $togoproject->generated_tenderid . '</span><span style="float:right; "  class="highlight">' . $user->userfullname . '</span></p>';
            $row[] = ($togoproject->country_name) ? $togoproject->country_name : "";
            $row[] = ($togoproject->state_name) ? $togoproject->state_name : "";
            $row[] = ($togoproject->sectName) ? $togoproject->sectName : "";
            $row[] = ($togoproject->service_name) ? $togoproject->service_name : "";
            $row[] = $this->showjvrecord($togoproject->project_id);
			//$com_id= $this->GetAllCompetitorList($togoproject->project_id);
			//$company_name = $this->db->get_where('main_company', array('fld_id'=>$com_id->compt_comp_id,'status'=>'1'))->row();
			//if($company_name->company_name) {
			//$row[] = '<span class="btn btn-success btn-sm">'.$company_name->company_name.'</span>';
		    //}
			//else{
			 // $row[]= '';
			//}
			$row[] = $this->GetAllCompetitorList($togoproject->project_id);
            $row[] = ($togoproject->Organization) ? $togoproject->Organization : "";
            $row[] = ($togoproject->client_name) ? $togoproject->client_name : '';
			if(($togoproject->client_position) or ($togoproject->client_contact)){
            $row[] =  $togoproject->client_position.' ('.$togoproject->client_contact.')';
			}
			else{
			 $row[] = '';	
			}
           // $row[] = ($togoproject->client_position) ? $togoproject->client_position : '';
            $row[] = '<span style="color:green;">' . $lastcmt->commentText . '</span>';
            $row[] = $view1 . $view2 . $view3 . $editlink. '<a onclick="datechangeproj(' . "'" . $togoproject->fld_id . "','" . date("Y-m-d", strtotime($togoproject->Expiry_Date)) . "'" . ')" data-toggle="modal" data-target="#changedate" class="btn btn-warning btn-sm "> <i class="fa fa-hourglass-end"></i> </a>';

            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->bidproject->count_all(),
            "recordsFiltered" => $this->bidproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function eoiProjectnewAll() {
        $list = $this->Bidprojecteoi_model->get_datatables();
        // echo '<pre>';
        // print_r($list);
        // die;
        //echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $view1 = '';
        $view2 = '';
        $view3 = '';
        $awardLink = '';
        //echo '<pre>'; print_r($list);
        foreach ($list as $togoproject) {
            if ($togoproject->project_status == 0) {
                $status = 'Awaiting';

                $statusAlert = '<div class="alert alert-warning"><i class="fa fa-info"></i> Awaiting </div>';
                
               // <i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>
                $awardLink = '';




                $editlink = '&nbsp&nbsp<a href="' . base_url('bidproject/submited_cegexp/' . $togoproject->fld_id . "/" . strtolower($status)) . '" title="Edit Project" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>&nbsp';
            } else if ($togoproject->project_status == 1) {
                $status = 'Won';
                 $statusAlert = '<div class="alert alert-success"><i class="fa fa-info"></i> Won </div>';
                
                $editlink = '';
              


                $awardLink = '&nbsp&nbsp<a href="' . base_url('bidproject/awardproject?pn=' . $togoproject->fld_id) . '" class="btn btn-success" target="_blank" title="Award Project"><i class="fa fa-tasks"></i></a>';
            } else if ($togoproject->project_status == 2) {
                $status = 'Loose';

                $statusAlert = '<div class="alert alert-danger"><i class="fa fa-info"></i> Loose </div>';
                $awardLink = '';

                 

                $editlink = '&nbsp&nbsp<a href="' . base_url('bidproject/submited_cegexp/' . $togoproject->fld_id . "/" . strtolower($status)) . '" class="btn btn-info btn-sm" title="Edit Project"><i class="fa fa-edit"></i></a>&nbsp';
            } else if ($togoproject->project_status == 3) {
                $status = 'Cancel';

                $statusAlert = '<div class="alert alert-danger"><i class="fa fa-info"></i> Cancel </div>';
                $awardLink = '';
                $editlink = '';
            }

            if ($bdRole == 1) {
                $view1 = $statusAlert;
            } else if ($bdRole == 2) {
                $view2 = $statusAlert;
            } else {




                if ($togoproject->project_status == 0) {
                    $status1 = '<a   style="cursor: pointer;" onclick="assignpm(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#changestatus">' . $statusAlert . '</a>';
                } else {
                    $status1 = $statusAlert;
                }
                $view3 = $status1 . '&nbsp; &nbsp;' . $awardLink;
            }
            $user = $this->SecondDB_model->getUserByID($togoproject->assign_to);
			// print_r($user); die;
			// echo $togoproject->fld_id; die;
            $lastcmt = $this->GetLastComment($togoproject->fld_id);
			// print_r($lastcmt); die;
            $detail = $togoproject->TenderDetails;
			// echo $detail; exit;
            $no++;
            $row = array();
   //          $row[] = $no;
   //          $row[] = ($togoproject->Expiry_Date == "Refer Document") ? "Null " : date("d-m-Y", strtotime($togoproject->Expiry_Date));
   //          $row[] = $togoproject->TenderDetails . '<hr><p><span style="float:left;" class="highlight pull-left">' . $togoproject->generated_tenderid . '</span><span style="float:right;" class="highlight pull-right">' . $user->userfullname . '</span></p>';
   //          $row[] = ($togoproject->country_name) ? $togoproject->country_name : "";
   //          $row[] = ($togoproject->state_name) ? $togoproject->state_name : "";
   //          $row[] = ($togoproject->sectName) ? $togoproject->sectName : "";
   //          $row[] = ($togoproject->service_name) ? $togoproject->service_name : "";
   //          $row[] = $this->showjvrecord($togoproject->project_id);
			// //$com_id= $this->GetAllCompetitorList($togoproject->project_id);
			// //$company_name = $this->db->get_where('main_company', array('fld_id'=>$com_id->compt_comp_id,'status'=>'1'))->row();
			// //if($company_name->company_name) {
			// //$row[] = '<span class="btn btn-success btn-sm">'.$company_name->company_name.'</span>';
		 //    //}
			// //else{
			//  // $row[]= '';
			// //}
			// $row[] = $this->GetAllCompetitorList($togoproject->project_id);
   //          $row[] = $togoproject->Organization;
   //          $row[] = ($togoproject->client_name) ? $togoproject->client_name : '';
			// if(($togoproject->client_position) or ($togoproject->client_contact)){
   //          $row[] =  $togoproject->client_position.' ('.$togoproject->client_contact.')';
			// }
			// else{
			//  $row[] = '';	
			// }
   //          //$row[] = ($togoproject->client_contact) ? $togoproject->client_contact : '';
   //         // $row[] = ($togoproject->client_position) ? $togoproject->client_position : '';
   //          $row[] = '<span style="color:green;">' . $lastcmt->commentText . '</span>';



            $row[] = $no;
            $row[] = ($togoproject->Expiry_Date == "Refer Document") ? "Null " : date("d-m-Y", strtotime($togoproject->Expiry_Date));
            $row[] = $togoproject->TenderDetails;

            $row[] = '<span style="float:left; width: auto !important" class="highlight">' . $togoproject->generated_tenderid . '</span>';
            $row[] = '<span style="float:left; width: auto !important"  class="highlight">' . $user->userfullname . '</span>';
            //submission date
            $row[] =  "";



            $row[] = ($togoproject->country_name) ? $togoproject->country_name : "";
            $row[] = ($togoproject->state_name) ? $togoproject->state_name : "";
            $row[] = ($togoproject->sectName) ? $togoproject->sectName : "";
            $row[] = ($togoproject->service_name) ? $togoproject->service_name : "";
            $row[] = $this->showjvrecord($togoproject->project_id);
            //$com_id= $this->GetAllCompetitorList($togoproject->project_id);
            //$company_name = $this->db->get_where('main_company', array('fld_id'=>$com_id->compt_comp_id,'status'=>'1'))->row();
            //if($company_name->company_name) {
            //$row[] = '<span class="btn btn-success btn-sm">'.$company_name->company_name.'</span>';
           // }
            //else{
            //  $row[]= '';
            //}
            $row[] = $this->GetAllCompetitorList($togoproject->project_id);
            $row[] = ($togoproject->client_name) ? $togoproject->client_name : ''; //$togoproject->Organization;

            $row[] = ($togoproject->client_contact) ? $togoproject->client_contact : '';  //($togoproject->client_name) ? $togoproject->client_name : '';
            // if(($togoproject->client_position) || ($togoproject->client_contact)){
   //          $row[] =  $togoproject->client_position.' ('.$togoproject->client_contact.')';
            // }
            // else{
            //  $row[] = '';    
            // }


            $row[] = ($togoproject->client_address) ? $togoproject->client_address : '';

            

          

           $row[] = ($togoproject->bid_validity_date) ? date("d-m-Y", strtotime($togoproject->bid_validity_date)) : "";

            $row[] = $view1 . $view2 . $view3 . $editlink. '<a  class="btn btn-warning btn-sm"onclick="datechangeproj(' . "'" . $togoproject->fld_id . "','" . date("Y-m-d", strtotime($togoproject->Expiry_Date)) . "'" . ')" data-toggle="modal" data-target="#changedate" title="Change Date"> <i class="fa fa-calendar"></i> </a>';




  

            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Bidprojecteoi_model->count_filtered(),
            "recordsFiltered" => $this->Bidprojecteoi_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function rfpProjectnewAll() {
        $list = $this->Bidprojectrfp_model->get_datatables();
        // echo '<pre>';
        // print_r($list);
        // die;
        //echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $view1 = '';
        $view2 = '';
        $view3 = '';
        $awardLink = '';
        //echo '<pre>'; print_r($list);
        foreach ($list as $togoproject) {
            if ($togoproject->project_status == 0) {
                $statusAlert = '<div class="alert alert-warning"><i class="fa fa-info"></i> Awaiting</div>';
                $status = 'Awaiting';
                $awardLink = '';
                // <i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>
                
            } else if ($togoproject->project_status == 1) {
                $statusAlert = '<span class="alert alert-success"><i class="fa fa-info"></i> Won</span>';
                $status = 'Won';
                $editlink = '';
                // <i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>
                // $awardLink = '&nbsp&nbsp<a href="' . base_url('bidproject/awardproject?pn=' . $togoproject->fld_id) . '" class="btn btn-info" target="_blank">Award Project</a>';
                $awardLink = '';
            } else if ($togoproject->project_status == 2) {
                $statusAlert = '<div class="alert alert-danger"><i class="fa fa-info"></i> Loose</div>';
                $status = 'Loose';
                $awardLink = '';
                // <i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>
                
            } else if ($togoproject->project_status == 3) {
                $statusAlert = '<div class="alert alert-danger"><i class="fa fa-info"></i> Cancel</div>';
                $status = 'Cancel';
                $awardLink = '';
                $editlink = '';
            }

            if ($bdRole == 1) {
                $view1 = $statusAlert;
            } else if ($bdRole == 2) {
                $view2 = $statusAlert;
            } else {
                if ($togoproject->project_status == 0) {
                    $status1 = '<a class="btn btn-user mt-3 ml-0 pl-0" style="cursor: pointer;" onclick="assignpm(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#changestatus">' . $statusAlert . '</a>';
                } else {
                    $status1 = $statusAlert;
                }
                $view3 = $status1;
            }


            $editlink = '&nbsp&nbsp<a href="' . base_url('bidproject/submited_cegexp/' . $togoproject->fld_id . "/" . strtolower($status)) . '" class="btn btn-info btn-sm"><i id="faction" title="Edit Project" class="glyphicon glyphicon-edit icon-white"></i></a>&nbsp';

            
            $user = $this->SecondDB_model->getUserByID($togoproject->assign_to);
            $lastcmt = $this->GetLastComment($togoproject->fld_id);

            $detail = $togoproject->TenderDetails;
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = ($togoproject->Expiry_Date == "Refer Document") ? "Null " : date("d-m-Y", strtotime($togoproject->Expiry_Date));
            $row[] = $togoproject->TenderDetails;

            $row[] = '<span style="float:left; width: auto !important" class="highlight">' . $togoproject->generated_tenderid . '</span>';
            $row[] = '<span style="float:left; width: auto !important"  class="highlight">' . $user->userfullname . '</span>';
            //submission date
            $row[] =  "";



            $row[] = ($togoproject->country_name) ? $togoproject->country_name : "";
            $row[] = ($togoproject->state_name) ? $togoproject->state_name : "";
            $row[] = ($togoproject->sectName) ? $togoproject->sectName : "";
            $row[] = ($togoproject->service_name) ? $togoproject->service_name : "";
            $row[] = $this->showjvrecord($togoproject->project_id);
			//$com_id= $this->GetAllCompetitorList($togoproject->project_id);
			//$company_name = $this->db->get_where('main_company', array('fld_id'=>$com_id->compt_comp_id,'status'=>'1'))->row();
			//if($company_name->company_name) {
			//$row[] = '<span class="btn btn-success btn-sm">'.$company_name->company_name.'</span>';
		   // }
			//else{
			//  $row[]= '';
			//}
			$row[] = $this->GetAllCompetitorList($togoproject->project_id);
            $row[] = ($togoproject->client_name) ? $togoproject->client_name : ''; //$togoproject->Organization;

            $row[] = ($togoproject->client_contact) ? $togoproject->client_contact : '';  //($togoproject->client_name) ? $togoproject->client_name : '';
			// if(($togoproject->client_position) || ($togoproject->client_contact)){
   //          $row[] =  $togoproject->client_position.' ('.$togoproject->client_contact.')';
			// }
			// else{
			//  $row[] = '';	
			// }


            $row[] = ($togoproject->client_address) ? $togoproject->client_address : '';

            

          

           $row[] = ($togoproject->bid_validity_date) ? date("d-m-Y", strtotime($togoproject->bid_validity_date)) : "";

            //$row[] = ($togoproject->client_contact) ? $togoproject->client_contact : '';
            //$row[] = ($togoproject->client_position) ? $togoproject->client_position : '';
            // $row[] = '<span class="highlight">' . $lastcmt->commentText . '</span>';
            $row[] = $view1 . $view2 . $view3 ."". $editlink.  '&nbsp; &nbsp;' . $awardLink. '<a onclick="datechangeproj(' . "'" . $togoproject->fld_id . "','" . date("Y-m-d", strtotime($togoproject->Expiry_Date)) . "'" . ')" data-toggle="modal" data-target="#changedate" class="btn btn-warning btn-sm"> <i class="fa fa-hourglass-end"></i> </a>';

            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Bidprojectrfp_model->count_filtered(),
            "recordsFiltered" => $this->Bidprojectrfp_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function fqProjectnewAll() {
        $list = $this->Bidprojectfq_model->get_datatables();
        // echo '<pre>';
        // print_r($list);
        // die;
        //echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $view1 = '';
        $view2 = '';
        $view3 = '';
        $awardLink = '';
        //echo '<pre>'; print_r($list);
        foreach ($list as $togoproject) {
            if ($togoproject->project_status == 0) {
                $status = 'Awaiting';
                $awardLink = '';
                $editlink = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>&nbsp&nbsp<a href="' . base_url('bidproject/submited_cegexp/' . $togoproject->fld_id . "/" . strtolower($status)) . '"><i id="faction" title="Edit Project" class="glyphicon glyphicon-edit icon-white"></i></a>&nbsp';
            } else if ($togoproject->project_status == 1) {
                $status = 'Won';
                $editlink = '';
                $awardLink = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>&nbsp&nbsp<a href="' . base_url('bidproject/awardproject?pn=' . $togoproject->fld_id) . '" class="btn btn-info" target="_blank">Award Project</a>';
            } else if ($togoproject->project_status == 2) {
                $status = 'Loose';
                $awardLink = '';
                $editlink = '<i id="faction" title="Comment" onclick="gocomment(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-comment icon-white commentcolor"></i>&nbsp&nbsp<a href="' . base_url('bidproject/submited_cegexp/' . $togoproject->fld_id . "/" . strtolower($status)) . '"><i id="faction" title="Edit Project" class="glyphicon glyphicon-edit icon-white"></i></a>&nbsp';
            } else if ($togoproject->project_status == 3) {
                $status = 'Cancel';
                $awardLink = '';
                $editlink = '';
            }

            if ($bdRole == 1) {
                $view1 = $status;
            } else if ($bdRole == 2) {
                $view2 = $status;
            } else {
                if ($togoproject->project_status == 0) {
                    $status1 = '<a class="btn btn-user" style="cursor: pointer;" onclick="assignpm(' . "'" . $togoproject->fld_id . "'" . ')" data-toggle="modal" data-target="#changestatus">' . $status . '</a>';
                } else {
                    $status1 = $status;
                }
                $view3 = $status1 . '&nbsp; &nbsp;' . $awardLink;
            }
            $user = $this->SecondDB_model->getUserByID($togoproject->assign_to);
            $lastcmt = $this->GetLastComment($togoproject->fld_id);

            $detail = $togoproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ($togoproject->Expiry_Date == "Refer Document") ? "Null " : date("d-m-Y", strtotime($togoproject->Expiry_Date)) . '<a onclick="datechangeproj(' . "'" . $togoproject->fld_id . "','" . date("Y-m-d", strtotime($togoproject->Expiry_Date)) . "'" . ')" data-toggle="modal" data-target="#changedate"> Edit </a>';
            $row[] = $togoproject->TenderDetails . '<hr><p><span style="float:left;">' . $togoproject->generated_tenderid . '</span><span style="float:right; color:green;">' . $user->userfullname . '</span></p>';
            $row[] = ($togoproject->country_name) ? $togoproject->country_name : "";
            $row[] = ($togoproject->state_name) ? $togoproject->state_name : "";
            $row[] = ($togoproject->sectName) ? $togoproject->sectName : "";
            $row[] = ($togoproject->service_name) ? $togoproject->service_name : "";
            $row[] = $this->showjvrecord($togoproject->project_id);
			//$com_id= $this->GetCompetitorCompanyName($togoproject->project_id);
			//$company_name = $this->db->get_where('main_company', array('fld_id'=>$com_id->compt_comp_id,'status'=>'1'))->row();
			//if($company_name->company_name) {
			//$row[] = '<span class="btn btn-success btn-sm">'.$company_name->company_name.'</span>';
		   // }
			//else{
			//  $row[]= '';
			//}
			$row[] = $this->GetAllCompetitorList($togoproject->project_id);
            $row[] = $togoproject->Organization;

            $row[] = ($togoproject->client_name) ? $togoproject->client_name : '';
			if(($togoproject->client_position) or ($togoproject->client_contact)){
            $row[] =  $togoproject->client_position.' ('.$togoproject->client_contact.')';
			}
			else{
			 $row[] = '';	
			}
            //$row[] = ($togoproject->client_contact) ? $togoproject->client_contact : '';
            //$row[] = ($togoproject->client_position) ? $togoproject->client_position : '';
            $row[] = '<span style="color:green;">' . $lastcmt->commentText . '</span>';
            $row[] = $view1 . $view2 . $view3 . $editlink;

            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Bidprojectfq_model->count_all(),
            "recordsFiltered" => $this->Bidprojectfq_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
	
	// code by dugesh(30-09-2019)
	public function GetCompetitorCompanyName($projid){
		$this->db->select('compt_comp_id');
		$this->db->from('cegexp_competitor_comp');
		$this->db->where(array('project_id'=>$projid,'status'=>'1'));
		$resultArr = $this->db->get()->row();
		if($resultArr){
			return ($resultArr)? $resultArr:'';
		}
	}
	
	//code by durgesh for get all competitor
    public function GetAllCompetitorList($projId){
        $Rec = $this->Front_model->selectRecord('cegexp_competitor_comp', array('*'), array('status' => '1', 'project_id' => $projId));
        if ($Rec) {
            $recData = $Rec->result();
            $LC = '';
            foreach ($recData as $r11) {
                $LC .= '<a href="'.base_url("company/addcompany_extra?compnm=$r11->compt_comp_id").'" style="color:green;">'.$this->mastermodel->GetCompNameById($r11->compt_comp_id) . "</a><b>(Comp.)</b><br/><br/>";
            }
            $returnVar = '';
            if (($LC) and ( $LC != " , ")) {
                $returnVar .= '<div style="width: 200px" class="highlight mt-1">' . $LC . '</div>';
            }
            $returnVar .= '';
            return $returnVar;
        } else {
            return null;
        }
        
    }

}
