<?php
/* Database connection start This Controller Create By Asheesh */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Projectdetail extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Front_model');
        $this->load->model('mastermodel');
        $this->load->model('Projectdetail_model');
        $this->load->helper(array('form', 'url','user_helper'));
        $this->load->library('session', 'form_validation');
        if (!($this->session->userdata('uid'))) {
            redirect(base_url());
        }
    }
	
	public function projectData(){
		$tmProject = $this->Front_model->getAllProject();
		$data = $this->Projectdetail_model->get_datatables();
		//echo '<pre>'; print_r($data); die;
		$this->load->view('projectdetail/project_view',compact('data','tmProject'));
	}
	
	public function saveproject(){
		$Arr = $_REQUEST;
		$count = $this->mastermodel->count_rows('project_detail', array('projectbd_id' => $_REQUEST['projectbd_id']));
		if ($count < 1):
			$arr = array('projectbd_id'=>$Arr['projectbd_id'],'cegproject_id'=>$Arr['cegproject_id'],'cegexp_id'=>$Arr['cegexp_id'],'project_status'=>$Arr['project_status'],'total_mm'=>$Arr['total_mm'],'status'=>1,'is_active'=>1);
			$this->mastermodel->InsertMasterData($arr,'project_detail');
		else:
			$arr = array('project_status'=>$Arr['project_status'],'total_mm'=>$Arr['total_mm'],'status'=>1,'is_active'=>1);
			$where = array('projectbd_id'=>$Arr['projectbd_id']);
			$respon = $this->mastermodel->UpdateRecords('project_detail',$where,$arr);
		endif;
		echo '1'; die;
	}
	
	public function getManmonth(){
		$id = $_REQUEST['id'];
		$query = $this->db->query('select totalmm from tm_projects where id='.$id);
		$res = $query->result_array();
		$arr = array('total_mm'=>$res[0]['totalmm']);
		echo json_encode($arr);
	}
}
