<?php

/* Database connection start */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Cru extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Mastermodel');
        $this->load->model('Cru_model', 'cru');
        $this->load->model('Front_model');
        if (!($this->session->userdata('loginid'))) {
            redirect(base_url());
        }
        $this->db2 = $this->load->database('another_db', TRUE);
        $db2 = $this->db2->database;

        $bduserid = $this->session->userdata('loginid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";
        if ($actual_link == "http://bd.cegtechno.com") {
           if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):
            else:
                redirect(base_url('welcome/logout'));
            endif;
        }
    }

    public function GetAllGenratedCodes() {
        $query = $this->db->query("SELECT * FROM tender_generated_id GROUP BY generated_tenderid ORDER BY generated_tenderid ASC");
        return $query->result();
    }

//Dashbord After Login..
    public function index() {
        $title = 'CRU-Modules';
        $sectorArr = $this->Front_model->GetActiveSector();
        //All Project For clone Asheesh/...
        $this->db->select('a.project_id,b.generated_tenderid');
        $this->db->from('team_assign_member as a');
        $this->db->join('tender_generated_id as b', 'a.project_id = b.project_id', 'LEFT');
        $this->db->where('a.project_id', $_REQUEST['pn']);
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
        }
        $tenderGenCode = $this->GetAllGenratedCodes();
        $secId = '0';
        $this->load->view('cru/tender_view', compact('title', 'secId', 'sectorArr', 'tenderGenCode'));
    }

// Project Display
    public function newProjectAll() {
        $list = $this->cru->get_datatables();
        //echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $view = '';
        $action = '';
        foreach ($list as $cru) {
            $chkemp = $this->cru->checkempexist($cru->project_id);
            if ($chkemp < 1) {
                $chkempclass = 'chkempclassred';
            } else {
                $chkempclass = 'chkempclassreds';
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '<span class="' . $chkempclass . '">' . $cru->TenderDetails . '</span>';
            $row[] = $cru->generated_tenderid;
            $row[] = $cru->Location;
            $row[] = $cru->Organization;
            $row[] = '<a href="' . base_url('cru/cru_view?pn=' . $cru->project_id) . '" class="btn btn-info" target="_blank">View Team</a>';
            $row[] = '<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" onclick="setnotimportant(' . "'" . $cru->project_id . "','" . $cru->generated_tenderid . "'" . ')" class="btn btn-info" title="Team Clone">T-Clone</a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->cru->count_all(),
            "recordsFiltered" => $this->cru->count_filtered(),
            "data" => $data);
        echo json_encode($output);
    }

    public function notified($tendrId, $notifData) {
        $rowArrData = NotifiedUserIDs($this->session->userdata('loginid'));
        foreach ($rowArrData as $rowRec) {
            $insertArr = array('user_to_notify' => $rowRec, 'user_who_fired_event' => $this->session->userdata('loginid'),
                'event_id_project' => $tendrId, 'notification_data' => $notifData);
            $Record = $this->Front_model->insertRecord('notification', $insertArr);
        }
    }

    public function userDatasOther() {
        $userData = $this->db->query('select * from team_req_otheremp where status="1" and fld_id=' . $_POST['userIDOther']);
        $user = $userData->result();
        $response = array($user[0]->emp_email, $user[0]->emp_contact);
        if ($response) {
            echo json_encode($response);
        }
    }

    public function saveteamdata() {
        $data = $_REQUEST;
        for ($i = 0; $count = count($data['team_id']), $i < $count; $i++) {
            $updaterecords = array('age_limit' => $data['age_limit'][$i],
                'man_months' => $data['man_months'][$i],
                'construction_months' => $data['construction_months'][$i],
                'development_months' => $data['development_months'][$i],
                'om_months' => $data['om_months'][$i],
                'user_id' => $data['username'][$i],
                'otheruser_name' => $data['other'][$i],
                'emailaddress' => $data['emailaddress'][$i],
                'contactnumber' => $data['contactnumber'][$i],
                'marks' => $data['marks'][$i],
                'firm' => $data['firm'][$i],
                'certificate' => $data['certificate'][$i],
                'undertaking' => $data['undertaking'][$i],
                'salary' => $data['salary'][$i],
                'remarks' => $data['remarks'][$i],
            );
            $where = array('id' => $data['team_id'][$i]);
            $updateID = $this->Front_model->updateRecord('team_assign_member', $updaterecords, $where);
        }
        if ($data['url_chk'] == '1') {
            $this->session->set_flashdata('msg', "Record Save and Updated. ");
            redirect(base_url('/cru/cru_view?pn=' . $data['project_id']));
        }
    }

    public function locadata() {
        $data = $_REQUEST;
        $bdRole = $this->Front_model->bd_rolecheck();
        if (loginUserDept() == 14) {
            $cruID = $this->session->userdata('loginid');
            $updates = array('lock_cru' => 1, 'lock_by_cru' => $cruID);
            $wheres = array('id' => $data['id']);
            $updateIDs = $this->Front_model->updateRecord('team_assign_member', $updates, $wheres);
        } else {
            if ($bdRole == 1) {
                $cruID = $this->session->userdata('loginid');
                $updates = array('lock_manager' => 1, 'lock_by_manager' => $cruID);
                $wheres = array('id' => $data['id']);
                $updateIDs = $this->Front_model->updateRecord('team_assign_member', $updates, $wheres);
            }//elseif($bdRole == 2){
//}
            else {
                $cruID = $this->session->userdata('loginid');
                $updates = array('lock_mgmt' => 1, 'lock_by_mgmt' => $cruID);
                $wheres = array('id' => $data['id']);
                $updateIDs = $this->Front_model->updateRecord('team_assign_member', $updates, $wheres);
            }
        }
        if (!empty($data['id'])) {
            $this->db->select('a.*,b.designation_name,c.TenderDetails,d.generated_tenderid');
            $this->db->from('team_assign_member as a');
            $this->db->join('designation_master_requisition as b', 'a.designation_id = b.fld_id', 'left');
            $this->db->join('bd_tenderdetail as c', 'a.project_id = c.fld_id', 'left');
            $this->db->join('tender_generated_id as d', 'a.project_id = d.project_id', 'left');
            $this->db->where('a.id', $data['id']);

            $query = $this->db->get();
            if ($query->num_rows() != 0) {
                $resulstArr = $query->result_array();
//echo '<pre>'; print_r($resulstArr);
                foreach ($resulstArr as $rowRec) {
                    $insertArr = array('team_assignmember_id' => $rowRec['id'],
                        'project_id' => $rowRec['project_id'],
                        'project_no' => $rowRec['project_no'],
                        'proposal_mngr_id' => $rowRec['proposal_mngr_id'],
                        'designation_id' => $rowRec['designation_id'],
                        'age_limit' => $rowRec['age_limit'],
                        'man_months' => $rowRec['man_months'],
                        'construction_months' => $rowRec['construction_months'],
                        'development_months' => $rowRec['development_months'],
                        'om_months' => $rowRec['om_months'],
                        'user_id' => $rowRec['user_id'],
                        'otheruser_name' => $rowRec['otheruser_name'],
                        'emailaddress' => $rowRec['emailaddress'],
                        'contactnumber' => $rowRec['contactnumber'],
                        'marks' => $rowRec['marks'],
                        'firm' => $rowRec['firm'],
                        'certificate' => $rowRec['certificate'],
                        'undertaking' => $rowRec['undertaking'],
                        'salary' => $rowRec['salary'],
                        'remarks' => $rowRec['remarks'],
                        'req_status' => $rowRec['req_status'],
                        'created' => $rowRec['created'],
                        'modified' => $rowRec['modified'],
                    );
                    $Record = $this->Front_model->insertRecord('team_negotiation_member', $insertArr);
                }
            }
        }
        exit;
    }

    //New Code By Asheesh For Cru..
    public function cru_view() {
        $this->db->select('a.*,b.designation_name,c.TenderDetails,d.generated_tenderid');
        $this->db->from('team_assign_member as a');
        $this->db->join('designation_master_requisition as b', 'a.designation_id = b.fld_id', 'left');
        $this->db->join('bd_tenderdetail as c', 'a.project_id = c.fld_id', 'left');
        $this->db->join('tender_generated_id as d', 'a.project_id = d.project_id', 'left');
        $this->db->where('a.project_id', $_REQUEST['pn']);
        $this->db->where('b.is_active', '1');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
        } else {
            $this->session->set_flashdata('errmsg', "No Any Team Assign for This Project");
            redirect(base_url('cru'));
        }
        $loginRoleEmp = $this->Mastermodel->SelectRecordSingle('bd_role', array('user_id' => $this->session->userdata('loginid')));
        $tqRecArr = $this->Mastermodel->SelectRecord('team_assign_member', array('status' => '1', 'project_id' => $_REQUEST['pn']));
        $userData = $this->Front_model->GetAllUserRec();
        $DesignationArr = $this->Front_model->allActiveDesignation();
        $this->load->view('/cru/copytest', compact('resulstArr', 'DesignationArr', 'userData', 'tqRecArr', 'loginRoleEmp'));
        // $this->load->view('/cru/cru_view_ash', compact('resulstArr', 'DesignationArr', 'userData', 'tqRecArr', 'loginRoleEmp'));
    }

//export Complete Project
    public function exportTenderTeam() {
        if (!empty($_REQUEST['project_id'])) {

            $resulstArr = $this->Mastermodel->SelectRecord('team_assign_member', array('status' => '1', 'project_id' => $_REQUEST['project_id']));
            if ($resulstArr) {
                $filename = 'tenderteam' . $_REQUEST['project_id'] . '_' . date('Y-m-d') . ".csv";
                $fp = fopen('php://output', 'w');
                $fields7 = array(strip_tags($row->project_id));
                $lineData = array(ProjNameById($resulstArr[0]->project_id));
                fputcsv($fp, $lineData, ',');
                $fields = array('Sr.No.', 'Designation', 'Name', 'Email', 'Contact', 'Firm', 'Age Limit', 'Certifcate', 'Undertaking', 'Salary', 'Remarks CRU', 'Remarks BD', 'Marks', 'Total Man Months', 'Weightage Marks RFP');
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="' . $filename . '";');
                // fputcsv($fp, $fields7, ',');
                fputcsv($fp, $fields, ',');
                $i = 1;

                foreach ($resulstArr as $row) {
                    $empNM = '';
                    $empNM2 = '';
                    $empName = '';
                    if ($row->empname):
                        $empNM = UserNameById($row->empname);
                    else:
                        $empNM2 = UserOtherNameById($row->empnameother);
                    endif;
                    $empName = $empNM . " " . $empNM2;
                    $desin = DesignationNameById($row->designation_id);
                    if ($row->certificate == 1) {
                        $certificate = 'Yes';
                    } elseif ($row->project_id == 0) {
                        $certificate = 'No';
                    } else {
                        $certificate = '';
                    }
                    if ($row->undertaking == 1) {
                        $undertaking = 'Yes';
                    } elseif ($row->undertaking == 0) {
                        $undertaking = 'No';
                    } else {
                        $undertaking = '';
                    }
                    $lineData = array($i, $desin, $empName, $row->emailaddress, $row->contactnumber, $row->firm, $row->age_limit, $certificate, $undertaking, $row->salary, $row->remarks_cru, $row->remarks_bd, $row->marks, $row->man_months, $row->weightage_marks_rfp);
                    $i++;
                    fputcsv($fp, $lineData, ',');
                }
            }




//            if ($tqRecArr) {
//                $filename = ' Team Requisition ' . $_REQUEST['project_id'] . '_' . date('Y-m-d') . ".csv";
//                $fields = array('Sr.No.', 'Designation', 'Name', 'Email', 'Contact', 'Marks', 'Firm', 'Age Limit', 'Total Man Months', 'Weightage Marks RFP', 'Certifcate', 'Undertaking', 'Salary', 'Remarks CRU', 'Remarks BD');
//                header('Content-Type: text/csv');
//                header('Content-Disposition: attachment; filename="' . $filename . '";');
//                fputcsv($fp, $fields7, ',');
//                fputcsv($fp, $fields, ',');
//              
//                
//                foreach($tqRecArr as $rwdata){
//                      $lineData = array($i, $row['project_id'], $row['project_id'], $row['project_id'], $row['project_id'], $row['project_id'], $row['project_id'], $row['project_id'], $row['project_id'], $row['project_id'], $row['project_id'], $row['project_id'], $row['project_id'], $row['project_id'], $row['project_id']);
//                      fputcsv($fp, $lineData, ',');
//                }
//                
//                
//            }
//            if ($query->num_rows() != 0) {
//                $resulstArr = $query->result_array();
//                $filename = 'tenderteam' . $_REQUEST['project_id'] . '_' . date('Y-m-d') . ".csv";
//                $fp = fopen('php://output', 'w');
//                $fields7 = array(strip_tags($resulstArr[0]['TenderDetails']));
//                $fields = array('Sr.No.', 'Designation', 'Name', 'Email', 'Contact', 'Marks', 'Firm', 'Age Limit', 'Total Man Months', 'Weightage Marks RFP', 'Certifcate', 'Undertaking', 'Salary', 'Remarks CRU', 'Remarks BD');
//                header('Content-Type: text/csv');
//                header('Content-Disposition: attachment; filename="' . $filename . '";');
//                fputcsv($fp, $fields7, ',');
//                fputcsv($fp, $fields, ',');
//                $i = 1;
//                foreach ($resulstArr as $row) {
//                    if ($row['empname']):
//                        $empNM = UserNameById($row['empname']);
//                    else:
//                        $empNM2 = UserOtherNameById($row['empnameother']);
//                    endif;
//                    $row['empname'] = $empNM . " " . $empNM2;
//
//                    if ($row['certificate'] == 1) {
//                        $certificate = 'Yes';
//                    } elseif ($row['certificate'] == 1) {
//                        $certificate = 'No';
//                    } else {
//                        $certificate = '';
//                    }
//                    if ($row['undertaking'] == 1) {
//                        $undertaking = 'Yes';
//                    } elseif ($row['undertaking'] == 1) {
//                        $undertaking = 'No';
//                    } else {
//                        $undertaking = '';
//                    }
//                    $lineData = array($i, $row['designation_name'], $row['empname'], $row['emailaddress'], $row['contactnumber'], $row['marks'], $row['firm'], $row['age_limit'], $row['man_months'], $row['weightage_marks_rfp'], $certificate, $undertaking, $row['salary'], $row['remarks_cru'], $row['remarks_bd']);
//                    $i++;
//                    fputcsv($fp, $lineData, ',');
//                }
//            }
        }
    }

    //Ash New Code Excel Exp... 
//    public function exportTenderTeam_n() {
//        $this->db->select('a.*,b.designation_name,c.TenderDetails,d.generated_tenderid');
//        $this->db->from('team_assign_member as a');
//        $this->db->join('designation_master_requisition as b', 'a.designation_id = b.fld_id', 'left');
//        $this->db->join('bd_tenderdetails as c', 'a.project_id = c.fld_id', 'left');
//        $this->db->join('tender_generated_id as d', 'a.project_id = d.project_id', 'left');
//        $this->db->where('a.project_id', $_REQUEST['pn']);
//        $query = $this->db->get();
//        if ($query->num_rows() != 0) {
//            $resulstArr = $query->result_array();
//        }
//
//        $loginRoleEmp = $this->Mastermodel->SelectRecordSingle('bd_role', array('user_id' => $this->session->userdata('loginid')));
//        $tqRecArr = $this->Mastermodel->SelectRecord('team_assign_member', array('status' => '1', 'project_id' => $_REQUEST['pn']));
//        $userData = $this->Front_model->GetAllUserRec();
//        $DesignationArr = $this->Front_model->allActiveDesignation();
//    }
//Save Seprate other Code By Asheesh..
    public function saveotherempdata() {
        $this->form_validation->set_rules('oemail', 'Email', 'required|valid_email|is_unique[team_req_otheremp.emp_email]');
        $this->form_validation->set_rules('ocontact', 'Contact No', 'required|is_unique[team_req_otheremp.emp_contact]');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('msg', "Email OR Contact already registered");
            redirect(base_url('cru/cru_view?pn=' . $this->input->post('project_id')));
        } else {
            $inserArr = array(
                'emp_name' => $this->input->post('oname'),
                'emp_email' => $this->input->post('oemail'),
                'emp_contact' => $this->input->post('ocontact'),
                'entry_by' => $this->session->userdata('loginid'));
            $recc = $this->Mastermodel->InsertMasterData($inserArr, 'team_req_otheremp');
            if ($recc):
                $this->session->set_flashdata('msg', "Other Employee Detail Added Success ");
                redirect(base_url('cru/cru_view?pn=' . $this->input->post('project_id')));
            else:
                redirect(base_url('cru/cru_view?pn=' . $this->input->post('project_id')));
            endif;
        }
    }

//Save Team Req Code By Asheesh..
    public function save_cruteam_req() {
        $myRecArr = array();
        $myRecArr['entry_by'] = $this->session->userdata('loginid');
        $myRecArr = $this->input->post();
        $fldid = array_pop($myRecArr);

        $recc = $this->Mastermodel->UpdateRecords('team_assign_member', array('id' => $fldid), $myRecArr);

        if ($recc):
//History Added For Save..
            $arrHist = array('project_id' => $myRecArr['project_id'], 'action_id' => $recc, 'user_id' => $this->session->userdata('loginid'), 'notif_msg' => ' Team Requisition Emp Details Add', 'details' => json_encode($this->input->post()));
            $this->Mastermodel->InsertMasterData($arrHist, 'team_requ_history');
            $this->session->set_flashdata('msg', " Team Requisition / Team Details Detail Added Success ");
            redirect(base_url('cru/cru_view?pn=' . $this->input->post('project_id')));
        else:
            redirect(base_url('cru/cru_view?pn=' . $this->input->post('project_id')));
        endif;
    }

//Ajax Code For Get TQ Req Get By Id  Asheesh..
    public function tqDetailsByID() {
        $tqid = $_REQUEST['edtid'];
        $tqRecArr = $this->Mastermodel->SelectRecordSingle('team_assign_member', array('status' => '1', 'id' => $tqid));
        $empNM = '';
        $empNM2 = '';
        if ($tqRecArr->empname):
            $empNM = UserNameById($tqRecArr->empname);
        else:
            $empNM2 = UserOtherNameById($tqRecArr->empnameother);
        endif;
        $tqRecArr->empname = $empNM . " " . $empNM2;
        echo json_encode($tqRecArr);
        exit();
    }

    public function tqReqDetailsByID($tqid) {
        $tqRecArr = $this->Mastermodel->SelectRecordSingle('team_assign_member', array('status' => '1', 'id' => $tqid));
        $empNM = '';
        $empNM2 = '';
        if ($tqRecArr->empname):
            $empNM = UserNameById($tqRecArr->empname);
        else:
            $empNM2 = UserOtherNameById($tqRecArr->empnameother);
        endif;
        $tqRecArr->empname = $empNM . " " . $empNM2;
        return $tqRecArr;
    }

//Update Team Req Data
    public function updaterqdata() {
        $myUpdRecArr = $this->input->post();
        $edttid = array_pop($myUpdRecArr);
        $recc = $this->Mastermodel->UpdateRecords('team_assign_member', array('id' => $edttid), $myUpdRecArr);
        if ($recc):
//History Added For Save..
            $arrHist = array('project_id' => $this->input->post('project_id'), 'action_id' => $edttid, 'user_id' => $this->session->userdata('loginid'), 'notif_msg' => ' Team Requisition Emp Details Updated', 'details' => json_encode($this->input->post()));
            $this->Mastermodel->InsertMasterData($arrHist, 'team_requ_history');
            $this->session->set_flashdata('msg', " Team Requisition / Team Details Detail Updated Success ");
            redirect(base_url('cru/cru_view?pn=' . $this->input->post('project_id')));
        else:
            redirect(base_url('cru/cru_view?pn=' . $this->input->post('project_id')));
        endif;
    }

    

//Delete Team Req..
    public function deleteam_treq() {
        $delid = $_REQUEST['delid'];
        $reCorDarr = $this->tqReqDetailsByID($delid);
        $arrHist = array('project_id' => '', 'action_id' => $delid, 'user_id' => $this->session->userdata('loginid'), 'notif_msg' => ' Team Requisition EDeleted', 'details' => json_encode($reCorDarr));
        $this->Mastermodel->InsertMasterData($arrHist, 'team_requ_history');
//Delete Pro..
        $updateArr = array(
            'empname' => null,
            'empnameother' => null,
            'emailaddress' => null,
            'contactnumber' => null,
            'marks' => null,
            'firm' => null,
            'certificate' => null,
            'undertaking' => null,
            'salary' => null,
            'remarks_cru' => null,
            'remarks_bd' => null,
        );
        $this->Mastermodel->UpdateRecords('team_assign_member', array('id' => $delid), $updateArr);

        //$this->Mastermodel->UpdateRecords('team_assign_member', array('id' => $delid), array('status' => '0'));
        if ($recc):
//History Added For Save..
            $this->session->set_flashdata('msg', " Team Requisition / Team Deleted Success ");
            redirect(base_url('cru/cru_view?pn=' . $reCorDarr->project_id));
        else:
            redirect(base_url('cru/cru_view?pn=' . $reCorDarr->project_id));
        endif;
    }

//Lock On Own Lvl Code By Asheesh..
    public function lockbylbl() {
        $lockid = $_REQUEST['lockid'];
        $recordArr = $this->Mastermodel->SelectRecordSingle('bd_role', array('user_id' => $this->session->userdata('loginid')));
        if ($recordArr):
            echo $this->Mastermodel->UpdateRecords('team_assign_member', array('id' => $lockid), array('locks' => $recordArr->role_name));
        else:
            echo null;
        endif;
    }

//Resume Upload By Asheesh..
    public function saveupld_resume() {
        if ($_FILES['resume']['name']) {
            $configThm = $this->getUploadConfig();
            $this->load->library('upload', $configThm);
            $this->upload->initialize($configThm);
            if ($this->upload->do_upload('resume')) {
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $resResp = $this->Mastermodel->UpdateRecords('team_assign_member', array('id' => $_REQUEST['act_id']), array('resume_file' => $filename, 'extra_contact' => $_REQUEST['ocontact']));
                $this->session->set_flashdata('msg', "Resume Uploaded Success");
                redirect(base_url('cru/cru_view?pn=' . $_REQUEST['project_id']));
            } else {
                $this->session->set_flashdata('errmsg', $this->upload->display_errors());
                redirect(base_url('cru/cru_view?pn=' . $_REQUEST['project_id']));
            }
        }
    }

    protected function getUploadConfig() {
        $config = array();
        $config['upload_path'] = './uploads/resume/';
        $config['allowed_types'] = 'pdf|doc';
        $config['max_size'] = '200000'; //2MB
        $config['file_name'] = "Resume_" . time();
        return $config;
    }

    public function test() {
        $data['error'] = '';
        $this->load->view('cru/copytest', $data);
    }

    public function getDataAndInsert() {
        $query = $this->db->query("select * from team_assign group by project_id");
        $res = $query->result();
        //echo '<pre>';
        foreach ($res as $value) {
            $res = $this->Mastermodel->count_rows('team_assign_member', array('project_id' => $value->project_id));
            if ($res < 1) {
                $projectID = $value->project_id;
                $query1 = $this->db->query("select * from team_assign where project_id =" . $projectID);
                $result = $query1->result();
                foreach ($result as $val) {
                    $insertArr = array(
                        'team_assign_id' => $val->id,
                        'project_id' => $val->project_id,
                        'project_no' => $val->project_no,
                        'proposal_mngr_id' => $val->proposal_mngr_id,
                        'designation_id' => $val->designation_id,
                        'age_limit' => $val->age_limit,
                        'man_months' => $val->man_months,
                        'construction_months' => $val->construction_months,
                        'development_months' => $val->development_months,
                        'om_months' => $val->om_months,
                        'filled_by_id' => $val->filled_by_id,
                        'single_comment' => $val->single_comment,
                        'weightage_marks_rfp' => $val->weightage_marks_rfp,
                        'comment' => $val->comment,
                        'req_status' => $val->req_status,
                    );
                    $Record = $this->Front_model->insertRecord('team_assign_member', $insertArr);
                }
                echo $projectID . '<br/>';
            }
        }
    }

    //Code Edit on 23 June
    public function userDatas() {
        $userData = $this->db2->query('SELECT * FROM main_users WHERE isactive="1" and id=' . $_POST['userID']);
        $user = $userData->result();
        $response = array($user[0]->contactnumber, $user[0]->emailaddress);
        if ($response) {
            echo json_encode($response);
        }
    }

    public function ajax_clonecondition() {
        $cloneforprojid = $_REQUEST['cloneforprojid']; // P039 Proj ID
        $clonetoprojid = $_REQUEST['clonetoprojid']; // P040 Proj ID

        if (($cloneforprojid) and ( $clonetoprojid)) {
            //First Condition ... Team Postion No...
            $this->db->where(array('project_id' => $cloneforprojid, 'status' => '1'));
            $num1 = $this->db->count_all_results('team_assign_member');
            
            $this->db->where(array('project_id' => $clonetoprojid, 'status' => '1'));
            $num2 = $this->db->count_all_results('team_assign_member');
            
            if ($num1 != $num2) {
                echo json_encode(array("status" => "No of Team Diffrent", "result" => false));
                exit();
            }
        }
        //Cond 2... Check Position...
        if (($cloneforprojid) and ( $clonetoprojid)) {
            //First Condition ... Team Postion No...
            $this->db->select_sum('designation_id');
            $this->db->from('team_assign_member');
            $this->db->where(array('project_id' => $cloneforprojid, 'status' => '1'));
            $query11 = $this->db->get();
            $designum1 = $query11->row()->designation_id;

            $this->db->select_sum('designation_id');
            $this->db->from('team_assign_member');
            $this->db->where(array('project_id' => $clonetoprojid, 'status' => '1'));
            $query12 = $this->db->get();
            $designum2 = $query12->row()->designation_id;
            if ($designum1 != $designum2) {
                echo json_encode(array("status" => "Position or Designation is Diffrent", "result" => false));
                exit();
            }
        }

        //Cond 3...
        //To Employee Already Feel Team... team_assign_member Will Be Blanck...
        if (($cloneforprojid) and ( $clonetoprojid)) {
            $where1 = "((empname is  NOT NULL) OR (emailaddress is  NOT NULL) OR (contactnumber is  NOT NULL) OR (marks is  NOT NULL) OR (firm is NOT NULL) OR (certificate is NOT NULL) OR (undertaking is  NOT NULL) OR (salary is  NOT NULL))";
            $this->db->where($where1);
            $this->db->where(array('project_id' => $clonetoprojid, 'status' => '1'));
            $num3 = $this->db->count_all_results('team_assign_member');
            if ($num3 > 0) {
                echo json_encode(array("status" => "Team Requisition Details Already Filled", "result" => false));
                exit();
            } else {
                //Copy ...
                $this->clonecopypaste($cloneforprojid, $clonetoprojid);
                echo json_encode(array("status" => "Clone Proccess Success", "result" => true));
                exit();
            }
        }
        exit;
    }

    //Clone Copy Paste,,,,
    public function clonecopypaste($clonefromprojid, $clonetoprojid) {
        $teamData = $this->db->query('SELECT * FROM team_assign_member WHERE status="1" AND project_id=' . $clonefromprojid);
        $teamRecArr = $teamData->result();
        if ($teamRecArr) {
            foreach ($teamRecArr as $recrow) {
                $updetArr = array('empname' => $recrow->empname, 'empnameother' => $recrow->empnameother, 'emailaddress' => $recrow->emailaddress, 'contactnumber' => $recrow->contactnumber, 'marks' => $recrow->marks, 'firm' => $recrow->firm, 'marks' => $recrow->marks,
                    'certificate' => $recrow->certificate, 'undertaking' => $recrow->undertaking, 'salary' => $recrow->salary, 'remarks_cru' => $recrow->remarks_cru);
                $this->Mastermodel->UpdateRecords('team_assign_member', array('project_id' => $clonetoprojid, 'designation_id' => $recrow->designation_id), $updetArr);
            }
        }
        return true;
    }
    
    
    //Position Delete Delete By Cru
    public function delpositionby_cru() {
        $tassignmemberid = $_REQUEST['tassignmemberid'];
        $tassignid = $_REQUEST['tassignid'];
        $bdproj_id = $_REQUEST['bdproj_id'];
        $updateArr = array("status" => "0");
        if ($tassignmemberid and $tassignid and $bdproj_id) {
            $this->Mastermodel->UpdateRecords('team_assign', array('id' => $tassignid), $updateArr);
            $this->Mastermodel->UpdateRecords('team_assign_member', array('id' => $tassignmemberid), $updateArr);
            $arrHist = array('project_id' => $bdproj_id, 'action_id' => $tassignid, 'user_id' => $this->session->userdata('loginid'), 'notif_msg' => ' Team Requisition EDeleted', 'details' => json_encode($_REQUEST));
            $this->Mastermodel->InsertMasterData($arrHist, 'team_requ_history');
            $this->session->set_flashdata('msg', "Position Deleted Success ");
        }
        redirect(base_url("cru/cru_view?pn=" . $bdproj_id));
    }
    
    

}
