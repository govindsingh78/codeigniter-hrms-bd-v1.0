<?php

/* Database connection start This Controller Create By Asheesh */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Projectplanning extends CI_Controller {

     function __construct() {
        parent::__construct();
        //$this->load->model('Manmonth_model');
        
        $this->load->model('Bidproject_model', 'bidproject');
        
        $this->load->model('SecondDB_model');
        $this->load->model('Bidprojecteoi_model');
        $this->load->model('Bidprojectfq_model');
        $this->load->model('Bidprojectrfp_model');


        $this->load->model('Front_model');
        $this->load->model('mastermodel');
        $this->load->model('Projectplanning_model');
        $this->load->model('Planningreport_model');
        $this->load->model('Educationalreport_model', "educationalreportmodel");


        if (!($this->session->userdata('loginid'))) {
            redirect(base_url());
        }
        $bduserid = $this->session->userdata('loginid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";
        if ($actual_link == "http://bd.cegtechno.com") {
           if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):
            else:
                redirect(base_url('welcome/logout'));
            endif;
        }
    }


    
    public function projectview() {
        $title = 'Project Planning';
        $tmProject = $this->Front_model->getAllProject();
        $this->load->view('projectplanning/project_view', compact('title', 'tmProject'));
    }

    public function AllprojectData() {
        $list = $this->Projectplanning_model->get_datatables();
        //echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $value) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $value->project_name;
            $row[] = $value->client_name;
            $row[] = $value->start_date;
            $row[] = $value->end_date;
            $row[] = $value->project_category;
            $row[] = ($value->totalmm != '0') ? $value->totalmm : '';
            $row[] = '<a href="' . base_url('projectplanning/userdetail/' . $value->project_id) . '" target="_blank">View</a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Projectplanning_model->count_all(),
            "recordsFiltered" => $this->Projectplanning_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function projectreport() {
        $title = 'Project Report';
        $tmProject = $this->Front_model->getAllProject();
        $employee = $this->Front_model->getAllemployees();
        $this->db->select('fld_id,designation_name');
        $this->db->from('designation_master');
        $this->db->where('is_active', '1');
        $designation = $this->db->get()->result_object();
        //echo '<pre>'; print_r($employee); die;
        $this->load->view('projectplanning/projectreport_view', compact('designation', 'title', 'tmProject', 'employee'));
    }

    public function projectreportData() {


        $list = $this->Planningreport_model->get_datatables();

        //print_r("expression"); die;


        $data = array();
        $no = $_POST['start'];
        foreach ($list as $value) {
            $consumem = $value->cumulative_pre_mm + $value->invtotalbalmm;
            // $resultbal = $this->Front_model->getempbalancemm($value->project_id , $value->id, $value->designation_id);
            // $eotresult = $this->Front_model->getempeot($value->project_id , $value->id, $value->designation_id);
            // $toalmm = $value->man_months + $eotresult->eot_mm;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $value->project_name;
			if($value->userfullname != 'Tbn'){
            //$row[] = '<a target="_blank" style="color: cornflowerblue;" href="' . base_url('projectplanning/userprofile/' . $value->id) . '">' . $value->userfullname . '<a>';
            $row[] = $value->userfullname;
		    }
			else{
            $row[] = '';
			}
			$row[] = $value->emailaddress;
            $row[] = $value->contactnumber;
            $row[] = $value->designation_name;
            $row[] = $value->marks;
            $row[] = $value->firm;
            $row[] = $value->invtotalmm;
            $row[] = $value->invtotalmm - $consumem;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Planningreport_model->count_filtered(),
            "recordsFiltered" => $this->Planningreport_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    
    

    public function mmupdateemp() {
        $list = $this->Planningreport_model->get_datatables();
        foreach ($list as $value) {
            $resultbal = $this->Front_model->getempbalancemm($value->project_id, $value->id, $value->designation_id);
            $eotresult = $this->Front_model->getempeot($value->project_id, $value->id, $value->designation_id);
            $toalmm = $value->man_months + $eotresult->eot_mm;
            $arr = array(
                'invtotalmm' => round($toalmm, 2),
                'invtotalbalmm' => round($resultbal->balance_mm, 3)
            );
            $Where = array('project_id' => $value->project_id, 'empname' => $value->id, 'designation_id' => $value->designation_id);
            $this->mastermodel->UpdateRecords('assign_finalteam', $Where, $arr);
        }
        redirect(base_url('projectplanning/projectreport'));
    }

    public function addteam() {
        $tmProject = $this->Front_model->getAllProject();
        $employee = $this->Front_model->getAllemployees();
        $designation = $this->Front_model->allActiveDesignation();
        //echo '<pre>'; print_r($employee);
        $this->load->view('projectplanning/add_team', compact('tmProject', 'employee', 'designation'));
    }

    public function viewteam() {
        $tmProject = $this->Front_model->getAllProject();
        $id = '';
        if ($_REQUEST) {
            $id = $_REQUEST['project_id'];
            $userdata = $this->Front_model->getuserdetailByprojectID($id);
        }
        $this->load->view('projectplanning/view_team', compact('tmProject', 'userdata', 'id'));
    }

    public function getuserdetailByID() {
        $userdata = $this->Front_model->getuserdetailByID($_REQUEST['id']);
        echo json_encode($userdata);
        die;
    }

    public function saveprojectteam() {
        $Arr = $_REQUEST;
        $arr = array('cegproject_id' => $Arr['project_id'],
            'designation_id' => $Arr['designation_id'],
            'age_limit' => $Arr['age_limit'],
            'man_months' => $Arr['total_mm'],
            'used_months' => $Arr['used_mm'],
            'user_id' => $Arr['emp_id'],
            'emailaddress' => $Arr['email_id'],
            'contactnumber' => $Arr['contact_no'],
            'marks' => $Arr['marks'],
            'firm' => $Arr['firm'],
            'entry_by' => $this->session->userdata('uid')
        );
        $res = $this->mastermodel->InsertMasterData($arr, 'team_project_member');
        if ($res) {
            $this->session->set_flashdata('msg', "Added Success.");
            redirect(base_url('projectplanning/addteam'));
        } else {
            $this->session->set_flashdata('msg', "Added Not Success.");
            redirect(base_url('projectplanning/addteam'));
        }
    }

    public function savemultipleprojectteam() {
        $Arr = $_REQUEST;
        //echo '<pre>'; print_r($Arr); die;
        $count = $this->mastermodel->count_rows('team_project_member', array('cegproject_id' => $Arr['project_id']));
        if ($count < 1):
            $length = count($Arr['designation_name']);
            for ($i = 0; $i < $length; $i++) {
                $arr = array(
                    'cegproject_id' => $Arr['project_id'],
                    'designation_name' => $Arr['designation_name'][$i],
                    'designation_id' => $Arr['designation_id'][$i],
                    'employee_name' => $Arr['emp_name'][$i],
                    'age_limit' => $Arr['age_limit'][$i],
                    'man_months' => $Arr['total_mm'][$i],
                    'used_months' => $Arr['used_mm'][$i],
                    'user_id' => $Arr['emp_id'][$i],
                    'emailaddress' => $Arr['email_id'][$i],
                    'contactnumber' => $Arr['contact_no'][$i],
                    'marks' => $Arr['marks'][$i],
                    'firm' => $Arr['firm'][$i],
                    'entry_by' => $this->session->userdata('uid')
                );
                $res = $this->mastermodel->InsertMasterData($arr, 'team_project_member');
            }
        endif;
        if ($res) {
            $this->session->set_flashdata('msg', "Added Success.");
            redirect(base_url('projectplanning/viewteam'));
        } else {
            $this->session->set_flashdata('msg', "Data Not Insert.");
            redirect(base_url('projectplanning/viewteam'));
        }
    }

    public function userprofile() {
        $userID = $this->uri->segment(3);
        $data = $this->Planningreport_model->getuserInfo($userID);
		$prj_coor = $this->db->get_where('project_coordinator',array('emp_id'=>$userID,'status'=>'1'))->result();
		$title = $data['userdetail'][0]->userfullname;
       // echo '<pre>'; print_r($data); die;
        $this->load->view('projectplanning/user_view', compact('data','title','prj_coor'));
    }
    
    

    public function usercomment() {
        if ($_REQUEST['user_comment']) {
            $recArr = array('user_id' => $this->session->userdata('uid'),
                'comment_id' => $_REQUEST['comment_id'],
                'comment' => $_REQUEST['user_comment'],
            );
            $this->mastermodel->InsertMasterData($recArr, 'user_comment');
        }
        redirect(base_url('projectplanning/userprofile/' . $_REQUEST['comment_id']));
    }

    public function viewteamproject() {
        $tmProject = $this->Front_model->getAllProject();
        $id = '';
        if ($_REQUEST) {
            $id = $_REQUEST['project_id'];
            $userdata = $this->Front_model->getuserinfoByprojectID($id);
        }
        $this->load->view('projectplanning/view_teamproject', compact('tmProject', 'userdata', 'id'));
    }

    public function saveviewteamproject() {
        $Arr = $_REQUEST;
        $length = count($Arr['user_id']);
        for ($i = 0; $i < $length; $i++) {
            $arr = array(
                'key_id' => $Arr['cat_id'][$i],
                'designation_id' => $Arr['designation_id'][$i],
            );
            $Where = array('id' => $Arr['user_id'][$i]);
            $res = $this->mastermodel->UpdateRecords('assign_finalteam', $Where, $arr);
        }

        if ($res) {
            $this->session->set_flashdata('msg', "Added Success.");
            redirect(base_url('projectplanning/viewteamproject'));
        } else {
            $this->session->set_flashdata('msg', "Data Not Insert.");
            redirect(base_url('projectplanning/viewteamproject'));
        }
    }

    public function viewposition() {
        $this->db->select('*');
        $this->db->from('designation_master');
        $this->db->where('is_active', '1');
        $designation_master = $this->db->get()->result();
        $id = '';

        if (!empty($_REQUEST['designation_id'])) {
            $designation_id = $_REQUEST['designation_id'];
            foreach ($designation_id as $val) {
                $arr = array('designation_id' => $_REQUEST['project_id']);
                $Where = array('fld_id' => $val);
                $res = $this->mastermodel->UpdateRecords('designation_master_requisition', $Where, $arr);
            }
        }
        if ($_REQUEST) {
            $id = $_REQUEST['project_id'];
            $this->db->select('designation_name');
            $this->db->from('designation_master_requisition');
            $this->db->where('is_active', '1');
            $this->db->where('designation_id', $id);
            $designation_master1 = $this->db->get()->result();
        }

        if (!empty($_REQUEST['sector'])) {
            $scid = $_REQUEST['sector'];
            $this->db->select('*');
            $this->db->from('designation_master_requisition');
            $this->db->where('is_active', '1');
            $this->db->where('designation_id', NULL);
            $this->db->where('sector_id', $_REQUEST['sector']);
            $this->db->order_by('designation_name', 'ASC');
            $designation = $this->db->get()->result();
        }
        $this->db->select('*');
        $this->db->from('sector');
        $this->db->where('is_active', '1');
        $this->db->order_by('sectName', 'ASC');
        $sector = $this->db->get()->result();

        /* $this->db->select('*');
          $this->db->from('designation_master_requisition');
          $this->db->where('is_active','1');
          $this->db->where('designation_id',NULL);
          $this->db->order_by('designation_name','ASC');
          $designation = $this->db->get()->result(); */
        //$designation = $this->Front_model->getAlldesignation();
        //$this->load->view('projectplanning/add_position', compact('designation_master1','id','designation','designation_master'));
        $this->load->view('projectplanning/add_position', compact('scid', 'sector', 'designation_master1', 'id', 'designation', 'designation_master'));
    }

    public function addDesignationmaster() {
        $designation = $_REQUEST['designation'];
        if ($designation):
            $insertArr = array('designation_name' => $designation);
            $response = $this->Front_model->insertRecord('designation_master', $insertArr);
        endif;
        return $response;
    }

    public function userdetail() {
        if (!empty($this->uri->segment(3))) {
            $projectID = $this->uri->segment(3);
            $data = $this->Projectplanning_model->getuserDetailByexpid($projectID);
            $this->db->select('TenderDetails');
            $this->db->from('bd_tenderdetail');
            $this->db->where('fld_id', $projectID);
            $projName = $this->db->get()->result_object();
        }
        $this->load->view('projectplanning/project_view_detail', compact('data', 'projName'));
    }

    public function userview() {
        if (!empty($this->uri->segment(3)) && !empty($this->uri->segment(4))) {
            $projectID = $this->uri->segment(3);
            $userID = $this->uri->segment(4);
            $data = $this->Projectplanning_model->getuserDetail($projectID, $userID);

            $startDate = $this->Projectplanning_model->getProjectStartDate($projectID);
            $year = date('Y', strtotime($startDate));
            //echo '<pre>'; print_r($startDateData);
            $this->db->select('TenderDetails');
            $this->db->from('bd_tenderdetail');
            $this->db->where('fld_id', $projectID);
            $tenderdetails = $this->db->get()->result_object();

            //Get Only Years..
            $this->db->select('a.*');
            $this->db->from('siteofc_attend_invcrwn as a');
            $this->db->where(array('a.status' => '1', 'a.bd_projId' => $projectID, 'a.emp_id' => $userID));
            $this->db->group_by("a.year");
            $yearsRecArr = $this->db->get()->result();
            $this->load->view('projectplanning/project_viewdata', compact('projectID', 'userID', 'data', 'year', 'tenderdetails', 'yearsRecArr'));
        }
    }

    //###################  Intermitent Report 06-02-2019.. Code By Asheesh...
    public function itmt_report() {
        $data['error'] = '';
        $db1 = $this->db1->database;
        //2 Sentrifugo..
        $db2 = $this->db2->database;
        $this->db->select("$db2.tm_project_employees.emp_id,$db2.main_employees_summary.userfullname,$db2.main_employees_summary.department_name,$db2.main_employees_summary.position_name");
        $this->db->from("$db2.tm_project_employees");
        $this->db->join("$db2.main_employees_summary", "$db2.tm_project_employees.emp_id=$db2.main_employees_summary.user_id", "LEFT");
        $this->db->where(array("$db2.tm_project_employees.is_active" => '1', "$db2.tm_project_employees.is_intermittent" => '1'));
        // $this->db->where("($db2.tm_project_employees.emp_id='429' OR $db2.tm_project_employees.emp_id='192')", NULL, FALSE);
        $this->db->group_by("$db2.tm_project_employees.emp_id");
        $data['empRecArr'] = $this->db->get()->result();
        $this->load->view('projectplanning/itmt_report_view', $data);
    }

    // New Code By Asheesh...
    public function educationalreport() {
        $data['error'] = '';
        $data['title'] = "Educational Report";
        $this->load->view('projectplanning/educationalreport_view', $data);
    }

    public function educationalreport_ajax() {

        $list = $this->educationalreportmodel->get_datatables();
        //print_r("expression"); die;
        $data = array();
        $no = $_POST['start'];
        $currdate = date('d-m-Y');
        $CountCegExp = "";

        foreach ($list as $value) {

            $empid = $value->user_id;
//            $eduRecArr1 = $this->cegproj_edulvldetails($empid, '1');
//            $eduRecArr2 = $this->cegproj_edulvldetails($empid, '2');
//            $eduRecArr3 = $this->cegproj_edulvldetails($empid, '3');
//            $eduRecArr4 = $this->cegproj_edulvldetails($empid, '4');
//            $eduRecArr5 = $this->cegproj_edulvldetails($empid, '5');
//            $eduRecArr6 = $this->cegproj_edulvldetails($empid, '6');
//            $eduRecArr7 = $this->cegproj_edulvldetails($empid, '7');

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $value->employeeId;
			if($value->userfullname !='Tbn'){
            $row[] = $value->prefix_name . "&nbsp;" . $value->userfullname;
			}
			else{
			 $row[]	= '';
			}
            //$row[] = '<a target="_blank" style="color: cornflowerblue;" href="' . base_url('projectplanning/userprofile/' . $value->id) . '">' . $value->userfullname . '<a>';
            $row[] = $value->position_name;
            $row[] = $value->department_name;


            $selecteddate = ($value->selecteddate) ? date("d-m-Y", strtotime($value->selecteddate)) : '';
            $diff = abs(strtotime($selecteddate) - strtotime($currdate));
            $years = floor($diff / (365 * 60 * 60 * 24));
            $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
            $CountCegExp = $years . '.' . $months;
            $row[] = $CountCegExp;
            //For Other Exp..
            $row[] = $value->years_exp;


//            $row[] = ($value->educationlevelcode1) ? $value->educationlevelcode1 : '';
//            $row[] = ($value->institution_name1) ? $value->institution_name1 : '';
//            $row[] = ($value->course1) ? $value->course1 : '';
//            $row[] = ($value->spc_location1) ? $value->spc_location1 : '';
//            $row[] = ($value->specialization1) ? $value->specialization1 : '';
//            $row[] = ($value->from_date1) ? $value->from_date1 : '';
//            $row[] = ($value->percentage1) ? $value->percentage1 : '';
//
//            $row[] = ($value->educationlevelcode2) ? $value->educationlevelcode2 : '';
//            $row[] = ($value->institution_name2) ? $value->institution_name2 : '';
//            $row[] = ($value->course2) ? $value->course2 : '';
//            $row[] = ($value->spc_location2) ? $value->spc_location2 : '';
//            $row[] = ($value->specialization2) ? $value->specialization2 : '';
//            $row[] = ($value->from_date2) ? $value->from_date2 : '';
//            $row[] = ($value->percentage2) ? $value->percentage2 : '';

            $row[] = ($value->educationlevelcode3) ? $value->educationlevelcode3 : '';
            $row[] = ($value->institution_name3) ? $value->institution_name3 : '';
            $row[] = ($value->course3) ? $value->course3 : '';
            $row[] = ($value->spc_location3) ? $value->spc_location3 : '';
            $row[] = ($value->specialization3) ? $value->specialization3 : '';
            $row[] = ($value->from_date3) ? $value->from_date3 : '';
           // $row[] = ($value->percentage3) ? $value->percentage3 : '';

            $row[] = ($value->educationlevelcode4) ? $value->educationlevelcode4 : '';
            $row[] = ($value->institution_name4) ? $value->institution_name4 : '';
            $row[] = ($value->course4) ? $value->course4 : '';
            $row[] = ($value->spc_location4) ? $value->spc_location4 : '';
            $row[] = ($value->specialization4) ? $value->specialization4 : '';
            $row[] = ($value->from_date4) ? $value->from_date4 : '';
           // $row[] = ($value->percentage4) ? $value->percentage4 : '';

            $row[] = ($value->educationlevelcode5) ? $value->educationlevelcode5 : '';
            $row[] = ($value->institution_name5) ? $value->institution_name4 : '';
            $row[] = ($value->course5) ? $value->course5 : '';
            $row[] = ($value->spc_location5) ? $value->spc_location5 : '';
            $row[] = ($value->specialization5) ? $value->specialization5 : '';
            $row[] = ($value->from_date5) ? $value->from_date5 : '';
            //$row[] = ($value->percentage5) ? $value->percentage5 : '';

            //$row[] = ($value->educationlevelcode6) ? $value->educationlevelcode6 : '';
           // $row[] = ($value->institution_name6) ? $value->institution_name6 : '';
           // $row[] = ($value->course6) ? $value->course6 : '';
          //  $row[] = ($value->spc_location6) ? $value->spc_location6 : '';
           // $row[] = ($value->specialization6) ? $value->specialization6 : '';
           // $row[] = ($value->from_date6) ? $value->from_date6 : '';
           // $row[] = ($value->percentage6) ? $value->percentage6 : '';

           // $row[] = ($value->educationlevelcode7) ? $value->educationlevelcode7 : '';
           // $row[] = ($value->institution_name7) ? $value->institution_name7 : '';
           // $row[] = ($value->course7) ? $value->course7 : '';
          //  $row[] = ($value->spc_location7) ? $value->spc_location7 : '';
           // $row[] = ($value->specialization7) ? $value->specialization7 : '';
          //  $row[] = ($value->from_date7) ? $value->from_date7 : '';
           // $row[] = ($value->percentage7) ? $value->percentage7 : '';

            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->educationalreportmodel->count_all(),
            "recordsFiltered" => $this->educationalreportmodel->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    //Educational Level By userid and Lavelid..
    function cegproj_edulvldetails($empid, $edulvid) {

        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db2.main_empeducationdetails.*,$db2.main_educationlevelcode.educationlevelcode");
        $this->db->from("$db2.main_empeducationdetails");
        $this->db->join("$db2.main_educationlevelcode", "$db2.main_empeducationdetails.educationlevel=$db2.main_educationlevelcode.id", 'LEFT');
        $this->db->where(array("$db2.main_empeducationdetails.user_id" => $empid, "$db2.main_empeducationdetails.educationlevel" => $edulvid, "$db2.main_empeducationdetails.isactive" => '1'));
        $data = $this->db->get()->row();
        return ($data) ? $data : null;
    }

//    public function get_total_otherexp($empid) {
//        $getmonth = 0;
//        $db1 = $this->db1->database;
//        $db2 = $this->db2->database;
//        $this->db->select("$db2.main_empeducationdetails.*");
//        $this->db->from("$db2.main_empeducationdetails");
//        $this->db->where(array("$db2.main_empeducationdetails.user_id" => $empid, "$db2.main_empeducationdetails.isactive" => '1'));
//        $otherExpArr = $this->db->get()->result();
//
//        // $otherExpArr = $this->mastermodel->GetTableData('main_empexperiancedetails', array('user_id' => $empid, 'isactive' => '1'));
//        if ($otherExpArr):
//            foreach ($otherExpArr as $recRw) {
//                $getmonth += getDateDiffMonth($recRw->from_date, $recRw->to_date);
//                // $getmonth += 1;
//            }
//            return ($getmonth > 0) ? number_format(($getmonth / 12), 2) : 0;
//        else:
//            return 0;
//        endif;
//    }
}
