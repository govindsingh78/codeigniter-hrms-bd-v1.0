<?php

/* Database connection start */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Bgreport extends CI_Controller {

   public function __construct() {
        parent::__construct();
        $this->load->model('Bgreport_model', 'bgreportmodel');
        $this->load->model('SecondDB_model');
    }

//Another DashBoard.. Code By Asheesh 2-01-2018...
    public function bg_report() {
        $title = 'BG Report';
        $this->load->view('bg_report/bg_report', compact("title"));
    }

    public function ajax_list_bg_report() {
        $list = $this->bgreportmodel->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $comprow) {
            $projectname = $this->SecondDB_model->getProjectBYId($comprow->project_numberid);
            $record_Arr= $this->db->get_where('proj_performance_bd_details', array('bd_projid'=>$comprow->project_id))->result();
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '<p style="text-align:justify;">'.$comprow->TenderDetails.'</p><br/><br/><b>'.$projectname->project_name.'</b>';
            if($record_Arr):
                
            $row_bg_type = array();
            $row_bg_amount = array();
            $row_bg_number = array();
            $row_bg_start = array();
            $row_bg_validity = array();
            $row_bg_bank = array();
            
            $row_bg1_type = array();
            $row_bg1_amount = array();
            $row_bg1_number = array();
            $row_bg1_start = array();
            $row_bg1_validity = array();
            $row_bg1_bank = array();
            foreach($record_Arr as $bdrecord_row):
			
		    if($bdrecord_row->pbg == 'Yes'):
			
            $row_bg_type[] = $bdrecord_row->pbg;
			else:
			 $row_bg_type[] = '';
		    endif;
            
           	if($bdrecord_row->abg == 'Yes'):
			
            $row_bg1_type[] = $bdrecord_row->abg;
			else:
			 $row_bg1_type[] = '';
		    endif;
            
			if($bdrecord_row->pbg_amount):
            $row_bg_amount[] = number_format($bdrecord_row->pbg_amount, 2);
			else:
			$row_bg_amount[] = '';
			endif;
            
            if($bdrecord_row->abg_amount):
            $row_bg1_amount[] = number_format($bdrecord_row->abg_amount, 2);
			else:
			$row_bg1_amount[] = '';
			endif;
            
			if($bdrecord_row->pbg_number):
            $row_bg_number[] = $bdrecord_row->pbg_number;
			else:
			$row_bg_number[] = '';
			endif;
            
            if($bdrecord_row->abg_number):
            $row_bg1_number[] = $bdrecord_row->abg_number;
			else:
			$row_bg1_number[] = '';
			endif;
            
            if(($bdrecord_row->pbg_start) != '0000-00-00'):
            $row_bg_start[] = $bdrecord_row->pbg_start;
            else:
            $row_bg_start[] = '';
            endif;
            
            if(($bdrecord_row->abg_start) != '0000-00-00'):
            $row_bg1_start[] = $bdrecord_row->abg_start;
            else:
            $row_bg1_start[] = '';
            endif;  
            
            if(($bdrecord_row->pbg_validity) != '0000-00-00'):
            $row_bg_validity[] = $bdrecord_row->pbg_validity;
            else:
            $row_bg_validity[] = '';
            endif;
            
            if(($bdrecord_row->abg_validity) != '0000-00-00'):
            $row_bg1_validity[] = $bdrecord_row->abg_validity;
            else:
            $row_bg1_validity[] = '';
            endif;   
            
			if($bdrecord_row->pbg_bank):
            $row_bg_bank[] = $bdrecord_row->pbg_bank;
			else:
			 $row_bg_bank[] = '';
			endif;
            
			if($bdrecord_row->abg_bank):
            $row_bg1_bank[] = $bdrecord_row->abg_bank;
			else:
			 $row_bg1_bank[] = '';
			endif;
			
            endforeach;
            endif;
			
            if(($row_bg_type) OR ($row_bg1_type)):
            $row[] = '<div class="row"><div class="col-sm-12"><div class="form-group"><label>PBG</label><br/><br/>'. implode('<br/><br/>', $row_bg_type).'</div></div></div><br/><br/><br/><br/><div class="row"><div class="col-sm-12"><div class="form-group"><label>ABG</label><br/><br/>'.implode('<br/><br/>', $row_bg1_type).'</div></div></div>';
			else:
			$row[] = '';
			endif;
            
            if(($row_bg_amount) OR ($row_bg1_amount)):
            $row[] = '<div class="row"><div class="col-sm-12"><div class="form-group"><label>Amount</label><br/><br/>'.implode('<br/><br/>', $row_bg_amount).'</div></div></div><br/><br/><br/><br/><div class="row"><div class="col-sm-12"><div class="form-group"><label>Amount</label><br/><br/>'.implode('<br/><br/>', $row_bg1_amount).'</div></div></div>';
            else:
			$row[] = '';
			endif;
            
            if(($row_bg_number) OR ($row_bg1_number)):
            $row[] = '<div class="row"><div class="col-sm-12"><div class="form-group"><label>Number</label><br/><br/>'.implode('<br/><br/>', $row_bg_number).'</div></div></div><br/><br/><br/><br/><div class="row"><div class="col-sm-12"><div class="form-group"><label>Number</label><br/><br/>'.implode('<br/><br/>', $row_bg1_number).'</div></div></div>';
            else:
			$row[] = '';
			endif;
            
            if(($row_bg_start) OR ($row_bg1_start)):
            $row[] = '<div class="row"><div class="col-sm-12"><div class="form-group"><label>Start</label><br/><br/>'.implode('<br/><br/>', $row_bg_start).'</div></div></div><br/><br/><br/><br/><div class="row"><div class="col-sm-12"><div class="form-group"><label>Start</label><br/><br/>'.implode('<br/><br/>', $row_bg1_start).'</div></div></div>';
            else:
			$row[] = '';
			endif;
            
            if(($row_bg_validity) OR ($row_bg1_validity)):
            $row[] = '<div class="row"><div class="col-sm-12"><div class="form-group"><label>Validity</label><br/><br/>'.implode('<br/><br/>', $row_bg_validity).'</div></div></div><br/><br/><br/><br/><div class="row"><div class="col-sm-12"><div class="form-group"><label>Validity</label><br/><br/>'.implode('<br/><br/>', $row_bg1_validity).'</div></div></div>';
            else:
			$row[] = '';
			endif;
            
            if(($row_bg_bank) OR ($row_bg1_bank)):
            $row[] = '<div class="row"><div class="col-sm-12"><div class="form-group"><label>Bank</label><br/><br/>'.implode('<br/><br/>', $row_bg_bank).'</div></div></div><br/><br/><br/><br/><div class="row"><div class="col-sm-12"><div class="form-group"><label>Bank</label><br/><br/>'.implode('<br/><br/>', $row_bg1_bank).'</div></div></div>';
            else:
			$row[] = '';
			endif;

            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" =>  $this->bgreportmodel->count_all(),
            "recordsFiltered" => $this->bgreportmodel->count_filtered(),
            "data" => $data,
        );
//output to json format
        echo json_encode($output);
    }

}
