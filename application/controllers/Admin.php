<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
        $this->load->model('mastermodel');
        $this->load->helper('cookie');
        if (!empty($this->session->userdata('loginid'))) {
            redirect(base_url("Dashboard"));
        }
    }

    public function index() { 
        $this->load->view('admin/auth/login');
    }

    //Login Control..
    public function loginchecker() {
        $empid = $this->input->post('username');
        $empPass = $this->input->post('password');
        if ($empid == '' or $empPass == ''){
            $this->session->set_flashdata('error_msg', 'Required Field Must be Validate');
        }else{
            $LoginCredentialArr = array('username' => $this->input->post('username'), 'password' => $this->input->post('password'));
            $user_details = $this->mastermodel->login_user_action($LoginCredentialArr);
            // echo "<pre>"; print_r($user_details[0]->businessunit_id); die;
            // echo "<pre>"; print_r($user_details); die;
            if ($user_details){

                $this->session->set_userdata(array('assign_role' => $user_details[0]->emprole, 'loginid' => $user_details[0]->id, 'username' => $user_details[0]->userfullname,'businessunit_id' => $user_details[0]->businessunit_id));
                // echo "<pre>"; print_r($user_details);
               
                redirect(base_url("Dashboard"));
                
            
            }else{
                $this->session->set_flashdata('error_msg', 'Your Login Credential is Invalid.');
                redirect(base_url(""));
            }
        }
    }

}
