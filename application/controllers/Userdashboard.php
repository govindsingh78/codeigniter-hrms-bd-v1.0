<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Userdashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mastermodel');
        $this->load->library('form_validation');
        if ($this->session->userdata('loginid') == "") {
            redirect(base_url(""));
        }
    }

    public function home() {
        $data['error'] = '';
        $data['title'] = 'Dashboard';
        $id = $this->session->userdata('loginid');
        $BasicDetailRec = $this->mastermodel->GetBasicRecLoginUser();
        $data['recInOutArr'] = $this->mastermodel->GetInOutTimeDetails($BasicDetailRec->thumbcode);
         
//        echo "<pre>";
//        print_r($data['recInOutArr']);
//        die;
        
        $this->load->view('userdashboard_view', $data);
    }

    public function logout() {
        $this->session->sess_destroy();
        $this->session->set_userdata(array('loginid' => null, 'username' => null));
        redirect(base_url(""));
    }

}
