<?php
/* Database connection start */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Eoiproject extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('Manmonth_model');
        $this->load->model('Eoiproject_model', 'eoiproject');
        $this->load->model('Front_model');
        if (!($this->session->userdata('uid'))) {
            redirect(base_url());
        }
        $bduserid = $this->session->userdata('uid');
        $actual_link = "http://$_SERVER[HTTP_HOST]";
        if ($actual_link == "http://bd.cegtechno.com") {
           if (($bduserid=='190') or ( $bduserid == '211') or ( $bduserid == '296')):
            else:
                redirect(base_url('welcome/logout'));
            endif;
        }
    }

    //Dashbord After Login..
    public function index() {
        $sectorArr = $this->Front_model->GetActiveSector();
        $secId = '0';
        $compnameArr = array();
        $proposalManager = $this->Front_model->allprojectProposalManager();
        //Code By Asheesh..
        $Rec = $this->Front_model->selectRecord('main_company', array('fld_id', 'company_name'), array('status' => '1'));
        if ($Rec != '') {
            $compnameArr = $Rec->result();
        }
        $this->load->view('eoiproject/tender_view', compact('secId', 'sectorArr', 'proposalManager', 'compnameArr'));
    }

    // Project Display
    public function newProjectAll() {
        $list = $this->eoiproject->get_datatables();
        //echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $bdRole = $this->Front_model->bd_rolecheck();
        $status = '';
        //echo '<pre>'; print_r($list);
        foreach ($list as $eoiproject) {
            if ($eoiproject->visible_scope == 'Bid_project') {
                if ($eoiproject->project_status == 0) {
                    $status = 'Bid(Awaiting)';
                } else if ($eoiproject->project_status == 1) {
                    $status = 'Bid(Won)';
                } else if ($eoiproject->project_status == 2) {
                    $status = 'Bid(Loose)';
                }
            } elseif ($eoiproject->visible_scope == 'No_Submit_project') {
                $status = 'Not Submitted';
            } elseif ($eoiproject->visible_scope == 'To_Go_project') {
                $status = 'To Be Submitted';
            }
            $detail = $eoiproject->TenderDetails;
            $no++;
            $row = array();
            $row[] = $no;
            $resull = explode("/", $eoiproject->generated_tenderid);
            $row[] = ($eoiproject->Expiry_Date == "Refer Document") ? "Null" : date("d-m-Y", strtotime($eoiproject->Expiry_Date));
            $row[] = $resull[3];
            $row[] = $eoiproject->TenderDetails . '<hr><p><span style="float:left;">' . $eoiproject->generated_tenderid . '</span><span style="float:right; color:green;">' . $eoiproject->userfullname . '</span></p>';
            $row[] = $eoiproject->Location;
            $row[] = $eoiproject->Organization;
            
           
            $row[] = $status;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            //"recordsTotal" => $this->activeproject->count_all(),
            "recordsTotal" => '0',
            "recordsFiltered" => $this->eoiproject->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }


    //Link View Details Popup Open Ash..
    public function projurlview() {
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('bd_tenderdetail', array('Tender_url'), array('fld_id' => $projId));
        if ($Rec) {
            $RowDataArr = $Rec->row();
        }
        $hyperlink = ($RowDataArr->Tender_url) ? $RowDataArr->Tender_url : 'http://www.cegindia.com/';
        $this->load->view('welcome_link', compact('projId', 'hyperlink'));
    }

    //Visibility Status
    public function visibilityStatus($copeName, $actid) {
        return $this->Front_model->updateRecord('bd_tenderdetail', array('visible_scope' => $copeName), array('fld_id' => $actid));
    }

    //Comment Showe
    public function commentset() {
        $tndrid = $_REQUEST['tndrid'];
        if (isset($tndrid)) {
            $Rec = $this->Front_model->selectRecordOrderByASC('comment_on_project', array('*'), array('is_active' => '1', 'project_id' => $tndrid));
            if ($Rec) {
                $data['allcomments'] = $Rec->result();
            }
            $this->load->view('comment_view', $data);
        }
    }

    //Comment Submit..
    public function commentsubmit() {
        $projId = $_REQUEST['projId'];
        $comnt = $_REQUEST['comnt'];
        if ($comnt):
            $insertArr = array('user_id' => $this->session->userdata('uid'), 'project_id' => $projId, 'sectorId' => '0', 'commentText' => $comnt);
            $Rec = $this->Front_model->insertRecord('comment_on_project', $insertArr);
            $this->notified($projId, 'A new Comment on Tender.');
        endif;
        return $Rec;
    }

    //Submit Project Concern Details..
    public function projectconcerndetails() {
        //Get Project Id..
        $projId = $_REQUEST['projID'];
        $Rec = $this->Front_model->selectRecord('project_concern_details', array('concern_details'), array('project_id' => $projId));
        if ($Rec != '') {
            $data = $Rec->result();
        } else {
            $data[0] = '';
        }
        $this->load->view('welcome_message', compact('projId', 'data'));
    }

    //For No Go Project set..
    public function nogosubmit() {
        if ($_REQUEST['actid']) {
            $inserArr = array('user_id' => $this->session->userdata('uid'), 'project_id' => $_REQUEST['actid'], 'actionIP' => get_client_ip());
            $Respon = $this->Front_model->insertRecord('proj_nogo_for_user', $inserArr);
        }
        if ($Respon > 0):
            $this->visibilityStatus('No_Go_project', $_REQUEST['actid']);
            $this->notified($_REQUEST['actid'], 'Tender Status Changed as No Go.');
            $this->session->set_flashdata('msg', "Project Status  Changed No Go . ");
            redirect(base_url('/togoproject'));
        endif;
    }

    public function notified($tendrId, $notifData) {
        $rowArrData = NotifiedUserIDs($this->session->userdata('uid'));
        foreach ($rowArrData as $rowRec) {
            $insertArr = array('user_to_notify' => $rowRec, 'user_who_fired_event' => $this->session->userdata('uid'),
                'event_id_project' => $tendrId, 'notification_data' => $notifData);
            $Record = $this->Front_model->insertRecord('notification', $insertArr);
        }
    }

}

