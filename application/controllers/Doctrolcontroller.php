<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Doctrolcontroller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
        $this->load->model('doctrolmgmt/Doctrolmodal', 'doctrolmodal');
        $this->load->model('doctrolmgmt/Doctrollastmodal', 'Doctrollastmodal');
        $this->load->model('doctrolmgmt/Doctrollastcoordmodal', 'Doctrollastcoordmodal');
        $this->load->model('doctrolmgmt/Doctrolreportmodal', 'doctrolrepmodal');
        $this->load->model('doctrolmgmt/Doctroldocumentmodal', 'doctroldocmodal');
    }

    //code by durgesh for view project coordinator
    public function doctrol_project_mgmt() {
        $title = 'Project Coordinator Dashboard';
        $user_id = $this->session->userdata('uid');
        $count_proj = $this->get_record();
        $doc = '';
        $doc_emp = '';
        foreach ($count_proj as $count_proj_row) {
            $doc .= $count_proj_row->hrms_projid . '<br/>';
            $doc_emp .= $count_proj_row->emp_id . '<br/>';
        }
        $doc_arr = array_filter(array_unique(explode('<br/>', @$doc)));
        $doc_emp_arr = array_filter(array_unique(explode('<br/>', @$doc_emp)));
        $count_doc = $this->get_record1($doc_arr);
        $last_doc_date = $this->get_lastdaterecord();
        $last_rep_date = $this->get_lastdatereprecord();
        $count_rep = $this->get_record2($doc_arr);
        $count_today = $this->get_record3($doc_arr);
        $this->load->view('doctrolmanage/doctrol_proj_coord', compact("title", "count_proj", 'count_doc', 'count_rep', 'count_today','last_doc_date','last_rep_date'));
    }
    
     //code by durgesh for view project coordinator
    public function doctrol_project_last_upload_date_report() {
        $title = 'Last Uplaod Date Of Document/Report';
        $this->load->view('doctrolmanage/doctrol_proj_last_upload_date', compact("title"));
    }

    //code by durgesh for ajax list of project coordinator
    public function ajax_list_doctrol_last_upload_date_report() {
        $list = $this->Doctrollastmodal->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $comprow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $comprow->project_name;
            $last_date_letter = $this->Doctrollastmodal->get_lastdaterecordByprojid($comprow->id);
            $last_date_report = $this->Doctrollastmodal->get_lastdatereprecordByprojid($comprow->id);
            $countcurrentrecordletter = $this->Doctrollastmodal->count_record_current_date($comprow->id);
            $countcurrentrecordreport = $this->Doctrollastmodal->count_record_report_current_date($comprow->id);
            $row[] = ($last_date_letter['entry_date'])?$last_date_letter['entry_date'] : '';
            $row[] = ($last_date_report['entry_date']) ? $last_date_report['entry_date'] : '';
            $row[] = $countcurrentrecordletter;
            $row[] = $countcurrentrecordreport;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Doctrollastmodal->count_all(),
            "recordsFiltered" => $this->Doctrollastmodal->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

 //code by durgesh for view project coordinator
    public function doctrol_project_last_upload_date_report_coord() {
        $title = 'Last Uplaod Date Of Document/Report';
        $this->load->view('doctrolmanage/doctrol_proj_last_upload_date_coord', compact("title"));
    }

    //code by durgesh for ajax list of project coordinator
    public function ajax_list_doctrol_last_upload_date_report_coord() {
        $list = $this->Doctrollastcoordmodal->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $comprow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $comprow->project_name;
            $last_date_letter = $this->Doctrollastcoordmodal->get_lastdaterecordByprojid($comprow->id);
            $last_date_report = $this->Doctrollastcoordmodal->get_lastdatereprecordByprojid($comprow->id);
            $countcurrentrecordletter = $this->Doctrollastcoordmodal->count_record_current_date($comprow->id);
            $countcurrentrecordreport = $this->Doctrollastcoordmodal->count_record_report_current_date($comprow->id);
            $row[] = ($last_date_letter['entry_date'])?$last_date_letter['entry_date'] : '';
            $row[] = ($last_date_report['entry_date']) ? $last_date_report['entry_date'] : '';
            $row[] = $countcurrentrecordletter;
            $row[] = $countcurrentrecordreport;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Doctrollastcoordmodal->count_all(),
            "recordsFiltered" => $this->Doctrollastcoordmodal->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    
    

    public function get_lastdaterecord() {
        $db1 = $this->db1->database; //BD
        $db2 = $this->db2->database; //HRMS
        $this->db->select("$db1.dbo_pwd_doctrol_metadata.project_id,$db1.dbo_pwd_doctrol_metadata.entry_date");
        $this->db->from("$db1.dbo_pwd_doctrol_metadata");
        //$this->db->where_in("$db1.dbo_pwd_doctrol_metadata.project_id", $projid);
        $this->db->order_by("$db1.dbo_pwd_doctrol_metadata.entry_date", "DESC");
        $ProjArr = $this->db->get()->row();
        if ($ProjArr) {
            return ($ProjArr) ? $ProjArr : '';
        }
    }

    public function get_lastdatereprecord() {
        $db1 = $this->db1->database; //BD
        $db2 = $this->db2->database; //HRMS
        $this->db->select("$db1.dbo_doctrol_upload_report.entry_date");
        $this->db->from("$db1.dbo_doctrol_upload_report");
       // $this->db->where_in("$db1.dbo_doctrol_upload_report.rep_project_id", $projid);
        $this->db->order_by("$db1.dbo_doctrol_upload_report.entry_date", "DESC");
        $ProjArr = $this->db->get()->row();
        if ($ProjArr) {
            return ($ProjArr) ? $ProjArr : '';
        }
    }

    //code by durgesh for ajax list of project coordinator
    public function ajax_list_doctrol_proj_coord() {
        $list = $this->doctrolmodal->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $comprow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $comprow->project_name;
            $row[] = '<a href="' . base_url('dashboard/viewedit') . '/' . $comprow->bd_project_id . '" class="btn btn-danger btn-sm" target="_blank"><i title="Ceg Exp" class="glyphicon glyphicon-eye-open"></i> View</a>';
            $row[] = '<a href="' . base_url('doctrol/doctrol_document') . '/' . $comprow->id . '" class="btn btn-primary btn-sm" target="_blank"><i title="Doctrol" class="glyphicon glyphicon-eye-open"></i> View</a>';
            $row[] = '<a href="' . base_url('doctrol/doctrol_report') . '/' . $comprow->id . '" class="btn btn-success btn-sm" target="_blank"><i title="Doctrol" class="glyphicon glyphicon-eye-open"></i> View</a>';

            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->doctrolmodal->count_all(),
            "recordsFiltered" => $this->doctrolmodal->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    //code by durgesh for view project coordinator
    public function doctrol_document($id) {
        $db1 = $this->db1->database; //BD
        $db2 = $this->db2->database; //HRMS
        $title = 'Doctrol Document Report';
        $projid = $id;
        $project_name = $this->db->get_where("$db2.tm_projects", array("$db2.tm_projects.id" => $id))->row();
        $category_name = $this->db->get_where("$db1.tbl_doccategory", array("$db1.tbl_doccategory.status" => '1'))->result();
        $this->load->view('doctrolmanage/doctrol_doc_report', compact("title", "projid", "project_name", "category_name"));
    }

    //code by durgesh for view project coordinator
    public function doctrol_report($id) {
        $db1 = $this->db1->database; //BD
        $db2 = $this->db2->database; //HRMS
        $title = 'Doctrol Report';
        $projid = $id;
        $project_name = $this->db->get_where("$db2.tm_projects", array("$db2.tm_projects.id" => $id))->row();
        $category_name = $this->db->get_where("$db1.tbl_doctype", array("$db1.tbl_doctype.status" => '1'))->result();
        $this->load->view('doctrolmanage/doctrol_rep_report', compact("title", "projid", "project_name", "category_name"));
    }

    //code by durgesh for ajax list of project coordinator
    public function ajax_list_doctrol_doc_report($projid) {
        $list = $this->doctrolrepmodal->get_datatables($projid);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $comprow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $comprow->cat_name;
            $row[] = $comprow->commun_type;
            $row[] = '<a href="' . HOSTNAME . 'Doctrol/' . $comprow->project_name . '/Documents/' . $comprow->file_name . '" class="btn btn-success btn-sm" download><i title="Download" class="glyphicon glyphicon-download"></i> Download</a>';
            $row[] = $comprow->entry_date;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->doctrolrepmodal->count_all(),
            "recordsFiltered" => $this->doctrolrepmodal->count_filtered($projid),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    //code by durgesh for ajax list of project coordinator
    public function ajax_list_doctrol_rep_report($projid) {
        $list = $this->doctroldocmodal->get_datatables($projid);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $comprow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $comprow->report_typename;
            $row[] = $comprow->duration_name;
            $row[] = '<a href="' . HOSTNAME . 'Doctrol/' . $comprow->project_name . '/' . $comprow->short_name . '/' . $comprow->file_name . '" class="btn btn-success btn-sm" download><i title="Download" class="glyphicon glyphicon-download"></i> Download</a>';
            $row[] = $comprow->entry_date;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->doctroldocmodal->count_all(),
            "recordsFiltered" => $this->doctroldocmodal->count_filtered($projid),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function get_record() {
        $db1 = $this->db1->database; //BD
        $db2 = $this->db2->database; //HRMS
        $projcoordid = $this->session->userdata('uid');
        $this->db->select("$db1.project_coordinator.hrms_projid,$db1.project_coordinator.emp_id");
        $this->db->from("$db1.project_coordinator");
        // $this->db->where(array("$db1.project_coordinator.emp_id" => $projcoordid));
        $ProjArr = $this->db->get()->result();
        if ($ProjArr) {
            return ($ProjArr) ? $ProjArr : '';
        }
    }

    public function get_record1($projid) {
        $db1 = $this->db1->database; //BD
        $db2 = $this->db2->database; //HRMS
        $this->db->select("$db1.dbo_pwd_doctrol_metadata.project_id");
        $this->db->from("$db1.dbo_pwd_doctrol_metadata");
        $this->db->where_in("$db1.dbo_pwd_doctrol_metadata.project_id", $projid);
        $ProjArr = $this->db->get()->result();
        if ($ProjArr) {
            return ($ProjArr) ? $ProjArr : '';
        }
    }

    public function get_record2($projid) {
        $db1 = $this->db1->database; //BD
        $db2 = $this->db2->database; //HRMS
        $this->db->select("$db1.dbo_doctrol_upload_report.rep_project_id");
        $this->db->from("$db1.dbo_doctrol_upload_report");
        $this->db->where_in("$db1.dbo_doctrol_upload_report.rep_project_id", $projid);
        $ProjArr = $this->db->get()->result();
        if ($ProjArr) {
            return ($ProjArr) ? $ProjArr : '';
        }
    }

    public function get_record3($projid) {
        $db1 = $this->db1->database; //BD
        $db2 = $this->db2->database; //HRMS
        $this->db->select("$db1.dbo_pwd_doctrol_metadata.project_id");
        $this->db->from("$db1.dbo_pwd_doctrol_metadata");
        $this->db->where_in("$db1.dbo_pwd_doctrol_metadata.project_id", $projid);
        $this->db->where(array("$db1.dbo_pwd_doctrol_metadata.entry_date" => date('Y-m-d'), "$db1.dbo_pwd_doctrol_metadata.status" => '1'));
        $ProjArr = $this->db->get()->num_rows();
        if ($ProjArr) {
            return ($ProjArr) ? $ProjArr : '';
        }
    }

    //code by durgesh for report duration
    public function get_reportdurationlist($reptypedur_id) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($reptypedur_id) {
            $this->db->select("$db1.dbo_duration.duration_id,$db1.dbo_duration.duration_name");
            $this->db->from("$db1.dbo_duration");
            $this->db->where(array("$db1.dbo_duration.report_typeid" => $reptypedur_id, "$db1.dbo_duration.status" => '1'));
            $resultArr = $this->db->get()->result();
            echo json_encode($resultArr);
        }
        exit();
    }

}
