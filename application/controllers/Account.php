<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);
require_once APPPATH . "/third_party/excel1/vendor/autoload.php";

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Font;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Protection;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class Account extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Front_model');
        $this->load->model('SecondDB_model');
        $this->load->model('mastermodel');
        $this->load->model('Account_info');
        $this->load->model('Newreportsummary_model');
        $this->load->helper(array('form', 'url', 'user_helper'));
        $this->load->library('session', 'form_validation');
        if (!($this->session->userdata('uid'))) {
            redirect(base_url());
        }
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    public function viewinfo() {
        $tmProject = $this->Front_model->getAllProject();
        $id = '';
        if ($_REQUEST) {
            $id = $_REQUEST['project_id'];
            $userdata = $this->Front_model->getuserinfoByprojectID($id);
        }
        $this->load->view('account/view_account', compact('tmProject', 'userdata', 'id'));
    }

    public function getproject() {
        if ($_REQUEST['project_numberid']) {
            $id = $_REQUEST['project_numberid'];
            $this->db->select('b.fld_id,b.TenderDetails');
            $this->db->from('bdcegexp_proj_summery as a');
            $this->db->join('bd_tenderdetail as b', 'a.project_id = b.fld_id', 'left');
            $this->db->where('a.project_numberid', $id);
            $res = $this->db->get()->result_array();
        }
        echo json_encode($res[0]);
        die;
    }

    public function getstate() {
        if ($_REQUEST['registered_country']) {
            $countryid = $_REQUEST['registered_country'];
            $this->db->select('state_id,state_name');
            $this->db->from('states');
            $this->db->where('country_id', $countryid);
            $res = $this->db->get()->result_array();
        }
        echo json_encode($res);
        die;
    }

    public function getcity() {
        if ($_REQUEST['registered_state']) {
            $stateid = $_REQUEST['registered_state'];
            $this->db->select('city_id,city_name');
            $this->db->from('cities');
            $this->db->where('state_id', $stateid);
            $res = $this->db->get()->result_array();
        }
        echo json_encode($res);
        die;
    }

    public function addinfo() {
        $tmProject = $this->Front_model->getAllProject();
        $this->db->select('*');
        $this->db->from('countries');
        $this->db->where('status', '1');
        $country = $this->db->get()->result();
        //Get Project List..
        $this->db->select('project_numberid');
        $this->db->from('accountinfo');
        $this->db->group_by('project_numberid');
        $ProjIdesArr = $this->db->get()->result();

        $returnArr = array();
        if ($ProjIdesArr):
            foreach ($ProjIdesArr as $reCd) {
                $returnArr[] = $reCd->project_numberid;
            }
        endif;
        //Step 2//
        $returnArr2 = array();
        if ($tmProject):
            foreach ($tmProject as $reCd2) {
                if (in_array($reCd2->id, $returnArr) == "") {
                    $returnArr2[] = $reCd2;
                }
            }
        endif;
        $this->load->view('account/add_info', compact('returnArr2', 'country'));
    }

    public function saveinfo() {
        if ($_REQUEST) {
            $_REQUEST['entryby'] = $this->session->userdata('uid');
            $res = $this->mastermodel->InsertMasterData($_REQUEST, 'accountinfo');
            $_REQUEST['account_id'] = $res;
            $res1 = $this->mastermodel->InsertMasterData($_REQUEST, 'history_accountinfo');
        }
        if ($res) {
            $this->session->set_flashdata('msg', "project info save.");
            redirect(base_url('account/viewaccountinfo'));
        } else {
            $this->session->set_flashdata('msg', "project info not save.");
            redirect(base_url('account/viewaccountinfo'));
        }
    }

    public function viewaccountinfo() {
        $data['error'] = '';
        $this->load->view('account/view_list', $data);
    }

    public function accountinfo_ajax() {
        $list = $this->Account_info->get_datatables();
        //echo '<pre>'; print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $view = '';
        $action = '';
        foreach ($list as $val) {
            $res = $this->Front_model->getexcelvalue($val->project_id);
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $val->project_name;
            $row[] = $res->contract_value;
            $row[] = $res->total_cumulative;
            $row[] = $res->contract_value - $res->total_cumulative;
            $row[] = "<a title='Edit Info' href='" . base_url("account/editinfo?id=" . $val->id) . "'><span class='glyphicon glyphicon-edit'></span></a>&nbsp;&nbsp;<a title='View Details' href='" . base_url("account/adddetail?id=" . $val->id) . "'><span class='glyphicon glyphicon-eye-open'></span></a> &nbsp; <a title='Add Team' href='" . base_url("account/addteam?id=" . $val->id) . "'><span class='glyphicon glyphicon-plus'></span></a>&nbsp;<a title='Add Replacement' href='" . base_url("account/addreplacement?id=" . $val->id) . "'><span class='glyphicon glyphicon-user'></span></a>  &nbsp; <a title='EOT' href='" . base_url("account/eot?id=" . $val->id) . "'><span class='glyphicon glyphicon-time'></span></a> &nbsp; <a title='View Invoice' href='" . base_url("account/viewinvoice?id=" . $val->id) . "'><span class='glyphicon glyphicon-save'></span></a> &nbsp; <a title='Invoice type 2' href='" . base_url("account/config_invoice_typediff?id=" . $val->id) . "'><span class='glyphicon glyphicon-wrench'></span></a>";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Account_info->count_all(),
            "recordsTotal" => '0',
            "recordsFiltered" => $this->Account_info->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function viewinvoice() {
        $id = $_REQUEST['id'];
        if ($id) {
            $this->load->view('account/invoice_view', compact('id'));
        }
    }

    public function invoiceinfo_ajax() {
        $params = $columns = $totalRecords = $data = array();
        $params = $_REQUEST;
        $projArr = $this->mastermodel->GetTableData('invoicedetail', array('infoid' => $_REQUEST['project_id']));
        $srn = 0;
        foreach ($projArr as $rowData):
            $srn++;
            $row = array();
            $invoicename = $rowData->invoice_name . '/' . $rowData->invoice_no;
            $row[] = $srn;
            $row[] = $invoicename;
            $row[] = $rowData->ra_invoice;
            $row[] = date('F Y', strtotime($rowData->invoice_date . "- 1 month"));
            $row[] = '<a href="' . base_url('account/exceldown/' . $rowData->id) . '"> <span style="color:#4CAF50;" title="Download" class="glyphicon glyphicon-download-alt"></span> </a> &nbsp;|&nbsp;<a style="color:#7386D5" href="' . base_url('account/viewtax?id=' . $rowData->id) . '">V-Tax</a> &nbsp;|&nbsp;<a href="' . base_url('account/viewescalation?id=' . $rowData->id) . '">V-Escalation</a>&nbsp;|&nbsp;<a style="color:green" target="_blank" href="' . base_url('account/const_mm_invoice_pdf/' . $rowData->id) . '">Cons-Maint </a>&nbsp;|&nbsp;<a target="_blank" style="color:#1c4e7f" href="' . base_url('account/invoice_pdfwitheot/' . $rowData->id) . '"> EOT </a>&nbsp;|&nbsp;<a target="_blank" style="color:#20c997" href="' . base_url('account/invoice_normal_construc_mainten_eot/' . $rowData->id) . '">  Cons-Maint-EOT </a>|&nbsp;<a target="_blank" style="color:#03a9f4" href="' . base_url('account/invocepdf_as_excel/' . $rowData->id) . '"> 1-Excl_as_PDF </a>';
            // $row[] = '<a href="' . base_url('account/exceldown/' . $rowData->id) . '"> <span style="color:#4CAF50;" title="Download" class="glyphicon glyphicon-download-alt"></span> </a> &nbsp;|&nbsp;<a style="color:#7386D5" href="' . base_url('account/viewtax?id=' . $rowData->id) . '">Tax</a> &nbsp;|&nbsp;<a href="' . base_url('account/viewescalation?id=' . $rowData->id) . '">Escalation</a>&nbsp;|&nbsp;<a style="color:#d44a17" href="' . base_url('account/viewreceivedamount?id=' . $rowData->id) . '">Received Amount</a>&nbsp;|&nbsp;<a href="' . base_url('account/viewpercentage?id=' . $rowData->id) . '">Percentage</a>&nbsp;|&nbsp;<a style="color:green" target="_blank" href="' . base_url('account/invoice_pdf/' . $rowData->id) . '">Cons-Maint </a>&nbsp;|&nbsp;<a target="_blank" style="color:#1c4e7f" href="' . base_url('account/invoice_pdfwitheot/' . $rowData->id) . '"> EOT </a>&nbsp;|&nbsp;<a target="_blank" style="color:#20c997" href="' . base_url('account/invoice_normal_construc_mainten_eot/' . $rowData->id) . '">  Cons-Maint-EOT </a>';
            $data[] = $row;
        endforeach;
        $json_data = array(
            // "draw" => 1,
            // "data" => $data
            "draw" => $_POST['draw'],
            "recordsTotal" => '0',
            "recordsFiltered" => '0',
            "data" => $data,
        );
        echo json_encode($json_data);
        exit();
    }

    public function editinfo() {
        $id = $_REQUEST['id'];
        if ($id) {
            $Where = array('id' => $id);
            $data = $this->mastermodel->GetTableData('accountinfo', $Where);

            $registered_country = isset($data[0]->registered_country) ? $data[0]->registered_country : false;
            $registered_state = isset($data[0]->registered_state) ? $data[0]->registered_state : false;
            $country = isset($data[0]->country) ? $data[0]->country : false;
            $state = isset($data[0]->state) ? $data[0]->state : false;
            $project_id = isset($data[0]->project_id) ? $data[0]->project_id : false;

            $this->db->select('*');
            $this->db->from('states');
            $this->db->where('status', '1');
            $this->db->where('country_id', $registered_country);
            $regstate = $this->db->get()->result();

            $this->db->select('*');
            $this->db->from('cities');
            $this->db->where('status', '1');
            $this->db->where('state_id', $registered_state);
            $regcity = $this->db->get()->result();

            $this->db->select('*');
            $this->db->from('states');
            $this->db->where('status', '1');
            $this->db->where('country_id', $country);
            $statedata = $this->db->get()->result();

            $this->db->select('*');
            $this->db->from('cities');
            $this->db->where('status', '1');
            $this->db->where('state_id', $state);
            $citydata = $this->db->get()->result();

            $this->db->select('TenderDetails');
            $this->db->from('bd_tenderdetail');
            $this->db->where('fld_id', $project_id);
            $tender = $this->db->get()->result();
        }
        $tmProject = $this->Front_model->getAllProject();

        $this->db->select('*');
        $this->db->from('countries');
        $this->db->where('status', '1');
        $country = $this->db->get()->result();
        $this->load->view('account/edit_info', compact('id', 'tender', 'statedata', 'citydata', 'data', 'tmProject', 'country', 'regstate', 'regcity'));
    }

    public function saveeditinfo() {
        if ($_REQUEST) {
            $data = array(
                'registered_address' => $_REQUEST['registered_address'],
                'registered_country' => $_REQUEST['registered_country'],
                'registered_state' => $_REQUEST['registered_state'],
                'registered_city' => $_REQUEST['registered_city'],
                'pan_no' => $_REQUEST['pan_no'], 'gstn_no' => $_REQUEST['gstn_no'],
                'state_code' => $_REQUEST['state_code'], 'name' => $_REQUEST['name'],
                'address' => $_REQUEST['address'], 'country' => $_REQUEST['country'],
                'state' => $_REQUEST['state'], 'city' => $_REQUEST['city'],
                'sgst' => $_REQUEST['sgst'], 'cgst' => $_REQUEST['cgst'], 'tds' => $_REQUEST['tds'],
                'bankholdername' => $_REQUEST['bankholdername'], 'bank_name' => $_REQUEST['bank_name'],
                'bank_address' => $_REQUEST['bank_address'], 'bank_accno' => $_REQUEST['bank_accno'],
                'rtgs_code' => $_REQUEST['rtgs_code'],
                'decimal_place' => $_REQUEST['decimal_place'],
                'no_days' => $_REQUEST['no_days'],
                'accordion_team' => $_REQUEST['accordion_team'],
                'accordion_other' => $_REQUEST['accordion_other'],
                'project_start_date' => $_REQUEST['proj_start_date'],
                'client_gst_no' => $_REQUEST['client_gst_no'],
                'proj_old_or_new' => $_REQUEST['proj_old_or_new'],
                'clnt_gst_state_code' => $_REQUEST['clnt_gst_state_code'],
                'invoice_formate_type' => $_REQUEST['invoice_formate_type'],
                'proj_invoce_sector' => ($_REQUEST['proj_invoce_sector']) ? $_REQUEST['proj_invoce_sector'] : Null,
                'invoice_services' => ($_REQUEST['invoice_services']) ? $_REQUEST['invoice_services'] : Null,
                'entryby' => $this->session->userdata('uid'),
                'place_of_supply_stateid' => $_REQUEST['place_of_supply_stateid']);

            $Where = array('id' => $_REQUEST['id']);
            $this->mastermodel->UpdateRecords('accountinfo', $Where, $data);

            $data1 = array(
                'account_id' => $_REQUEST['id'],
                'project_numberid' => $_REQUEST['project_numberid'],
                'project_id' => $_REQUEST['project_id'],
                'registered_address' => $_REQUEST['registered_address'],
                'registered_country' => $_REQUEST['registered_country'],
                'registered_state' => $_REQUEST['registered_state'],
                'registered_city' => $_REQUEST['registered_city'],
                'pan_no' => $_REQUEST['pan_no'], 'gstn_no' => $_REQUEST['gstn_no'],
                'state_code' => $_REQUEST['state_code'], 'name' => $_REQUEST['name'],
                'address' => $_REQUEST['address'], 'country' => $_REQUEST['country'],
                'state' => $_REQUEST['state'], 'city' => $_REQUEST['city'],
                'sgst' => $_REQUEST['sgst'], 'cgst' => $_REQUEST['cgst'], 'tds' => $_REQUEST['tds'],
                'bankholdername' => $_REQUEST['bankholdername'], 'bank_name' => $_REQUEST['bank_name'],
                'bank_address' => $_REQUEST['bank_address'], 'bank_accno' => $_REQUEST['bank_accno'],
                'rtgs_code' => $_REQUEST['rtgs_code'],
                'decimal_place' => $_REQUEST['decimal_place'],
                'no_days' => $_REQUEST['no_days'],
                'accordion_team' => $_REQUEST['accordion_team'],
                'accordion_other' => $_REQUEST['accordion_other'],
                'entryby' => $this->session->userdata('uid')
            );
            $res1 = $this->mastermodel->InsertMasterData($data1, 'history_accountinfo');
        }
        redirect(base_url('account/editinfo?id=' . $_REQUEST['id']));
    }

    public function adddetail() {
        $id = $_REQUEST['id'];
        if ($id) {
            $Where = array('id' => $id);
            $data = $this->mastermodel->GetTableData('accountinfo', $Where);
        }
        $userdata = $this->SecondDB_model->GetAllUserRec();
        //echo '<pre>'; print_r($data); die;
        $this->load->view('account/add_detail', compact('data', 'userdata'));
    }

    public function savedetail() {
        if ($_REQUEST) {
            $project_numberid = $_REQUEST['project_numberid'];
            $invoice_no = $_REQUEST['invoice_no'];
            $arr = array(
                'project_numberid' => $_REQUEST['project_numberid'],
                'infoid' => $_REQUEST['infoid'],
                'invoice_name' => $_REQUEST['invoice_name'],
                'invoice_no' => $_REQUEST['invoice_no'],
                'ra_invoice' => $_REQUEST['ra_invoice'],
                'invoice_date' => date('Y-m-d', strtotime($_REQUEST['invoice_date']))
            );
            $Where = array('project_numberid' => $project_numberid, 'invoice_no' => $invoice_no);
            $data = $this->mastermodel->count_rows('invoicedetail', $Where);
            //echo '<pre>'; print_r($_REQUEST);
            if ($data < 1) {
                $res = $this->mastermodel->InsertMasterData($arr, 'invoicedetail');
            } else {
                $Where = array('project_numberid' => $_REQUEST['project_numberid'], 'invoice_no' => $_REQUEST['invoice_no']);
                $this->mastermodel->UpdateRecords('invoicedetail', $Where, $arr);
            }

            $datas = array('invoice_no' => $_REQUEST['invoice_no']);
            $Wheres = array('project_numberid' => $_REQUEST['project_numberid']);
            $this->mastermodel->UpdateRecords('accountinfo', $Wheres, $datas);
        }
    }

    public function saveteam() {
        $Arr = $_REQUEST;
        $length = count($Arr['tableid']);

        if ($Arr['save'] == 'save') {
            for ($i = 0; $i < $length; $i++) {
                $arr = array(
                    //'empname'=>$Arr['emp_id'][$i],
                    'man_months' => $Arr['man_months'][$i],
                    'balancemm' => $Arr['balancemm'][$i],
                    'cumulative_pre_mm' => $Arr['cumulative_pre_mm'][$i],
                    'cumulative_pre_amount' => $Arr['cumulative_pre_amount'][$i],
                    'rate' => $Arr['rate'][$i],
                    //'account_lock'=> 1,
                    'account_entryby' => $this->session->userdata('uid'),
                    'account_date' => date('Y-m-d')
                );
                $Where = array('id' => $Arr['tableid'][$i]);
                $res = $this->mastermodel->UpdateRecords('assign_finalteam', $Where, $arr);
            }
        } else {
            for ($i = 0; $i < $length; $i++) {
                $arr = array(
                    //'empname'=>$Arr['emp_id'][$i],
                    'man_months' => $Arr['man_months'][$i],
                    'balancemm' => $Arr['balancemm'][$i],
                    'cumulative_pre_mm' => $Arr['cumulative_pre_mm'][$i],
                    'cumulative_pre_amount' => $Arr['cumulative_pre_amount'][$i],
                    'rate' => $Arr['rate'][$i],
                    'account_lock' => 1,
                    'account_entryby' => $this->session->userdata('uid'),
                    'account_date' => date('Y-m-d')
                );
                $Where = array('id' => $Arr['tableid'][$i]);
                $res = $this->mastermodel->UpdateRecords('assign_finalteam', $Where, $arr);
            }
        }

        if ($res) {
            $this->session->set_flashdata('msg', "Added Success.");
            redirect(base_url('account/addteam?id=' . $Arr['teamid']));
        } else {
            $this->session->set_flashdata('msg', "Data Not Insert.");
            redirect(base_url('account/addteam?id' . $Arr['teamid']));
        }
    }

    public function addteam() {
        $id = $_REQUEST['id'];
        if ($id) {
            $Where = array('id' => $id);
            $data = $this->mastermodel->GetTableData('accountinfo', $Where);
        }
        $projectid = $data[0]->project_numberid;
        $userdata = $this->SecondDB_model->GetAllUserRecByprojectID($projectid);
        $userres = $this->SecondDB_model->GetAllUserRec();
        $this->db->select('a.id,a.designation_id,b.designation_name');
        $this->db->from('assign_finalteam as a');
        $this->db->join('designation_master_requisition as b', 'a.designation_id = b.fld_id', 'right');
        $this->db->where('a.project_id', $data[0]->project_id);
        $designation = $this->db->get()->result();
        $this->load->view('account/add_team', compact('data', 'userdata', 'userres', 'designation'));
    }

    public function getdojbyID() {
        $userID = $_REQUEST['userID'];
        if ($userID) {
            $this->db->select('date_of_joining');
            $this->db->from('main_employees_summary');
            $this->db->where('user_id', $userID);
            $res = $this->db->get()->result_array();
            $resdata = isset($res[0]) ? $res[0] : false;
        }
        echo json_encode($resdata);
        die;
    }

    public function getemployeename() {
        $id = $_REQUEST['designation_id'];
        if ($id) {
            $username = $this->Front_model->getemployeename($id);
        }
        echo json_encode($username);
        die;
    }

    public function editsaveteam() {
        $data = array('empname' => $_REQUEST['emp_id']);
        $Where = array('id' => $_REQUEST['designation_id']);
        $res = $this->mastermodel->UpdateRecords('assign_finalteam', $Where, $data);
        if ($res) {
            $this->session->set_flashdata('msg', "Added Success.");
            redirect(base_url('account/addteam?id=' . $_REQUEST['teamid']));
        } else {
            $this->session->set_flashdata('msg', "Data Not Insert.");
            redirect(base_url('account/addteam?id' . $_REQUEST['teamid']));
        }
    }

    public function addreplacement() {
        $id = $_REQUEST['id'];
        if ($id) {
            $Where = array('id' => $id);
            $data = $this->mastermodel->GetTableData('accountinfo', $Where);
        }
        $projectid = $data[0]->project_numberid;
        $userdata = $this->SecondDB_model->GetAllUserRecByprojectID($projectid);
        $userres = $this->SecondDB_model->GetAllUserRec();
        $this->db->select('a.id,a.designation_id,b.designation_name');
        $this->db->from('assign_finalteam as a');
        $this->db->join('designation_master_requisition as b', 'a.designation_id = b.fld_id', 'right');
        $this->db->where('a.project_id', $data[0]->project_id);
        $designation = $this->db->get()->result();
        $this->load->view('account/add_replacement', compact('data', 'userdata', 'userres', 'designation'));
    }

    public function editreplacementteam() {
        //echo '<pre>'; print_r($_REQUEST); die;
        $id = $_REQUEST['designation_id'];
        if ($id) {
            $this->db->select('*');
            $this->db->from('assign_finalteam');
            $this->db->where('id', $id);
            $res = $this->db->get()->result();

            if ($res) {
                $data = array('replacement_reducationrate' => $_REQUEST['replacement_reducationrate']);
                $Where = array('id' => $_REQUEST['designation_id']);
                $result = $this->mastermodel->UpdateRecords('assign_finalteam', $Where, $data);
            }
        }
        if ($result) {
            $this->session->set_flashdata('msg', "Added Success.");
            redirect(base_url('account/addreplacement?id=' . $_REQUEST['teamid']));
        } else {
            $this->session->set_flashdata('msg', "Data Not Insert.");
            redirect(base_url('account/addreplacement?id' . $_REQUEST['teamid']));
        }
    }

    public function eot() {
        $id = $_REQUEST['id'];
        if ($id) {
            $Where = array('id' => $id);
            $data = $this->mastermodel->GetTableData('accountinfo', $Where);
        }
        $project_id = $data[0]->project_id;
        //echo '<pre>'; print_r($data); die;
        $this->load->view('account/add_eot', compact('data', 'userdata'));
    }

    public function checkteam() {
        $data = array(
            'project_id' => $_REQUEST['project_id'],
            'submission_date' => $_REQUEST['submission_date'],
            'approval_date' => $_REQUEST['approval_date'],
            'entryby' => $this->session->userdata('uid')
        );
        $Where = array('project_id' => $_REQUEST['project_id'], 'statusid' => 0);
        $count = $this->mastermodel->count_rows('acceot_date', $Where);
        if ($count < 1) {
            $res = $this->mastermodel->InsertMasterData($data, 'acceot_date');
        } else {
            if (!empty($_REQUEST['approval_date'])) {
                $arr = array(
                    'approval_date' => $_REQUEST['approval_date'],
                    'statusid' => 1,
                    'entryby' => $this->session->userdata('uid')
                );
                $Where = array('id' => $_REQUEST['acceot_id']);
                $res = $this->mastermodel->UpdateRecords('acceot_date', $Where, $arr);
            }
        }

        if ($res) {
            $this->session->set_flashdata('msg', "Added Success.");
            redirect(base_url('account/eot?id=' . $_REQUEST['teamid']));
        } else {
            $this->session->set_flashdata('msg', "Data Not Insert.");
            redirect(base_url('account/eot?id=' . $_REQUEST['teamid']));
        }
    }

    public function eotsave() {
        //  echo '<pre>'; print_r($_REQUEST); die;


        $Arr = $_REQUEST;
        $length = count($Arr['detail_id']);

        if ($Arr['save'] == 'save') {
            for ($i = 0; $i < $length; $i++) {
                $arr = array(
                    'project_id' => $Arr['project_id'],
                    'eot_id' => $Arr['eot_id'],
                    'masterdetail_id' => $Arr['detail_id'][$i],
                    'eot_mm' => $Arr['eotmm'][$i],
                    'entryby' => $this->session->userdata('uid')
                );

                $Where = array('id' => $Arr['eotdetail_id'][$i]);
                $data = $this->mastermodel->count_rows('eot_detail', $Where);
                if ($data < 1) {
                    $res = $this->mastermodel->InsertMasterData($arr, 'eot_detail');
                } else {
                    $Wheres = array('id' => $Arr['eotdetail_id'][$i]);
                    $res = $this->mastermodel->UpdateRecords('eot_detail', $Wheres, $arr);
                }
            }
        } else {
            for ($i = 0; $i < $length; $i++) {
                $arr = array(
                    'project_id' => $Arr['project_id'],
                    'eot_id' => $Arr['eot_id'],
                    'masterdetail_id' => $Arr['detail_id'][$i],
                    'eot_mm' => $Arr['eotmm'][$i],
                    'eot_lock' => 1,
                    'entryby' => $this->session->userdata('uid')
                );

                $Where = array('id' => $Arr['eotdetail_id'][$i]);
                $data = $this->mastermodel->count_rows('eot_detail', $Where);
                if ($data < 1) {
                    $res = $this->mastermodel->InsertMasterData($arr, 'eot_detail');
                } else {
                    $Wheres = array('id' => $Arr['eotdetail_id'][$i]);
                    $this->mastermodel->UpdateRecords('eot_detail', $Wheres, $arr);
                }
            }
        }

        if ($res) {
            $this->session->set_flashdata('msg', "Added Success.");
            redirect(base_url('account/eot?id=' . $Arr['teamid']));
        } else {
            $this->session->set_flashdata('msg', "Data Not Insert.");
            redirect(base_url('account/eot?id=' . $Arr['teamid']));
        }
    }

    public function eotsaveteam() {
        //echo '<pre>'; print_r($_REQUEST); die;
        $Arr = $_REQUEST;
        $length = count($Arr['assign_id']);

        if ($Arr['save'] == 'save') {
            for ($i = 0; $i < $length; $i++) {
                $arr = array(
                    'project_id' => $Arr['project_id'],
                    'eot_id' => $Arr['eot_id'],
                    'designation_id' => $Arr['designation_id'][$i],
                    'emp_id' => $Arr['empname'][$i],
                    'assign_id' => $Arr['assign_id'][$i],
                    'eot_mm' => $Arr['eot_mm'][$i],
                    'entryby' => $this->session->userdata('uid')
                );

                $Where = array('id' => $Arr['eotteam_id'][$i]);
                $data = $this->mastermodel->count_rows('saveeotteam', $Where);
                if ($data < 1) {
                    $res = $this->mastermodel->InsertMasterData($arr, 'saveeotteam');
                } else {
                    $Wheres = array('id' => $Arr['eotteam_id'][$i]);
                    $res = $this->mastermodel->UpdateRecords('saveeotteam', $Wheres, $arr);
                }
            }
        } else {
            for ($i = 0; $i < $length; $i++) {
                $arr = array(
                    'project_id' => $Arr['project_id'],
                    'eot_id' => $Arr['eot_id'],
                    'designation_id' => $Arr['designation_id'][$i],
                    'emp_id' => $Arr['empname'][$i],
                    'assign_id' => $Arr['assign_id'][$i],
                    'eot_mm' => $Arr['eot_mm'][$i],
                    'eot_lock' => 1,
                    'entryby' => $this->session->userdata('uid')
                );

                $Where = array('id' => $Arr['eotteam_id'][$i]);
                $data = $this->mastermodel->count_rows('saveeotteam', $Where);
                if ($data < 1) {
                    $res = $this->mastermodel->InsertMasterData($arr, 'saveeotteam');
                } else {
                    $Wheres = array('id' => $Arr['eotteam_id'][$i]);
                    $this->mastermodel->UpdateRecords('saveeotteam', $Wheres, $arr);
                }
            }
        }

        if ($res) {
            $this->session->set_flashdata('msg', "Added Success.");
            redirect(base_url('account/eot?id=' . $Arr['teamid']));
        } else {
            $this->session->set_flashdata('msg', "Data Not Insert.");
            redirect(base_url('account/eot?id=' . $Arr['teamid']));
        }
    }

    public function monthdata() {
        $id = $_REQUEST['id'];
        if ($id) {
            $Where = array('id' => $id);
            $monthdata = $this->mastermodel->GetTableData('acc_monthdate', $Where);
            if ($monthdata[0]->info_id) {
                $Wheres = array('id' => $monthdata[0]->info_id);
                $data = $this->mastermodel->GetTableData('accountinfo', $Wheres);
            }
        }
        $this->load->view('account/add_month', compact('data', 'monthdata'));
    }

    public function monthsaveteam() {
        //echo '<pre>'; print_r($_REQUEST); die;
        $Arr = $_REQUEST;
        $length = count($Arr['assign_id']);
        if ($length) {
            if ($Arr['save'] == 'save') {
                for ($i = 0; $i < $length; $i++) {
                    $arr = array(
                        'month_id' => $Arr['month_id'],
                        'assign_id' => $Arr['assign_id'][$i],
                        'emp_id' => $Arr['emp_id'][$i],
                        'designation_id' => $Arr['designation_id'][$i],
                        'user_month' => $Arr['man_months'][$i],
                        'cumulative_pre_mm' => $Arr['cumulative_pre_mm'][$i],
                        'cumulative_pre_amount' => $Arr['cumulative_pre_amount'][$i],
                        'entryby' => $this->session->userdata('uid')
                    );

                    $Where = array('id' => $Arr['eotteam_id'][$i]);
                    $data = $this->mastermodel->count_rows('acc_monthdetail', $Where);

                    if ($data < 1) {
                        $res = $this->mastermodel->InsertMasterData($arr, 'acc_monthdetail');
                    } else {
                        $Wheres = array('id' => $Arr['eotteam_id'][$i]);
                        $res = $this->mastermodel->UpdateRecords('acc_monthdetail', $Wheres, $arr);
                    }
                }
            } else {
                for ($i = 0; $i < $length; $i++) {
                    $arr = array(
                        'month_id' => $Arr['month_id'],
                        'emp_id' => $Arr['emp_id'][$i],
                        'designation_id' => $Arr['designation_id'][$i],
                        'assign_id' => $Arr['assign_id'][$i],
                        'user_month' => $Arr['man_months'][$i],
                        'cumulative_pre_mm' => $Arr['cumulative_pre_mm'][$i],
                        'cumulative_pre_amount' => $Arr['cumulative_pre_amount'][$i],
                        'eot_lock' => 1,
                        'entryby' => $this->session->userdata('uid')
                    );

                    $Where = array('id' => $Arr['eotteam_id'][$i]);
                    $data = $this->mastermodel->count_rows('acc_monthdetail', $Where);

                    if ($data < 1) {
                        $res = $this->mastermodel->InsertMasterData($arr, 'acc_monthdetail');
                    } else {
                        $Wheres = array('id' => $Arr['eotteam_id'][$i]);
                        $this->mastermodel->UpdateRecords('acc_monthdetail', $Wheres, $arr);
                    }
                }
            }
        }
        if ($res) {
            $this->session->set_flashdata('msg', "Added Success.");
            redirect(base_url('account/monthdata?id=' . $Arr['month_id']));
        } else {
            $this->session->set_flashdata('msg', "Data Not Insert.");
            redirect(base_url('account/monthdata?id=' . $Arr['month_id']));
        }
    }

    public function monthsavedate() {
        //echo '<pre>'; print_r($_REQUEST); die;
        if ($_REQUEST) {
            $date_month = $_REQUEST['date_month'];
            $Arr = $_REQUEST;
            $monthyear = date('Y-m', strtotime($date_month));
            $firstyear = $monthyear . '-01';
            $lastyear = $monthyear . '-31';
            $arr = array(
                'date_month' => $Arr['date_month'],
                'invoice_id' => $Arr['invoice_id'],
                'project_id' => $Arr['project_id'],
                'info_id' => $Arr['ids'],
                'entryby' => $this->session->userdata('uid')
            );

            //$Where = array('project_id'=>$Arr['project_id'],'invoice_id'=>$Arr['invoice_id'],'id'=>$Arr['monthID']);
            $Where = array('project_id' => $Arr['project_id'], 'invoice_id' => $Arr['invoice_id']);
            $where_date = "(date_month >='$firstyear' AND date_month <= '$lastyear')";
            $this->db->select('*');
            $this->db->from('acc_monthdate');
            $this->db->where($Where);
            $this->db->where($where_date);
            $data = $this->db->count_all_results();
            if ($data < 1) {
                $res = $this->mastermodel->InsertMasterData($arr, 'acc_monthdate');
            } else {
                //$Wheres = array('project_id'=>$Arr['project_id'],'invoice_id'=>$Arr['invoice_id'],'id'=>$Arr['monthID']);
                $Wheres = array('project_id' => $Arr['project_id'], 'invoice_id' => $Arr['invoice_id']);
                $res = $this->mastermodel->UpdateRecords('acc_monthdate', $Wheres, $arr);
                $res = $Arr['monthID'];
            }
        }
        if ($res) {
            $this->session->set_flashdata('msg', "Added Success.");
            redirect(base_url('account/monthdata?id=' . $res));
        } else {
            $this->session->set_flashdata('msg', "Data Not Insert.");
            redirect(base_url('account/monthdata?id=' . $res));
        }
    }

    public function othermonthsavedate() {
        if ($_REQUEST) {
            $date_month = $_REQUEST['date_month'];
            $Arr = $_REQUEST;
            $monthyear = date('Y-m', strtotime($date_month));
            $firstyear = $monthyear . '-01';
            $lastyear = $monthyear . '-31';
            $arr = array(
                'date_month' => $Arr['date_month'],
                'invoice_id' => $Arr['invoice_id'],
                'project_id' => $Arr['project_id'],
                'info_id' => $Arr['ids'],
                'entryby' => $this->session->userdata('uid')
            );

            $Where = array('project_id' => $Arr['project_id'], 'invoice_id' => $Arr['invoice_id'], 'id' => $Arr['monthID']);
            $where_date = "(date_month >='$firstyear' AND date_month <= '$lastyear')";
            $this->db->select('*');
            $this->db->from('acc_othermonthdate');
            $this->db->where($Where);
            $this->db->where($where_date);
            $data = $this->db->count_all_results();
            if ($data < 1) {
                $res = $this->mastermodel->InsertMasterData($arr, 'acc_othermonthdate');
            } else {
                $Wheres = array('project_id' => $Arr['project_id'], 'invoice_id' => $Arr['invoice_id'], 'id' => $Arr['monthID']);
                $res = $this->mastermodel->UpdateRecords('acc_othermonthdate', $Wheres, $arr);
                $res = $Arr['monthID'];
            }
        }
        if ($res) {
            $this->session->set_flashdata('msg', "Added Success.");
            redirect(base_url('account/otherinfo?id=' . $res));
        } else {
            $this->session->set_flashdata('msg', "Data Not Insert.");
            redirect(base_url('account/otherinfo?id=' . $res));
        }
    }

    public function otherinfo() {
        $id = $_REQUEST['id'];
        if ($id) {
            $Where = array('id' => $id);
            $monthdata = $this->mastermodel->GetTableData('acc_othermonthdate', $Where);
            if ($monthdata[0]->info_id) {
                $Wheres = array('id' => $monthdata[0]->info_id);
                $data = $this->mastermodel->GetTableData('accountinfo', $Wheres);
            }
        }
        $this->load->view('account/add_othermonth', compact('data', 'monthdata'));
    }

    public function curothermonthsavedate() {
        if ($_REQUEST) {
            //echo '<pre>'; print_r($_REQUEST);
            $date_month = $_REQUEST['date_month'];
            $Arr = $_REQUEST;
            $monthyear = date('Y-m', strtotime($date_month));
            $firstyear = $monthyear . '-01';
            $lastyear = $monthyear . '-31';
            $arr = array(
                'date_month' => $Arr['date_month'],
                'invoice_id' => $Arr['invoice_id'],
                'project_id' => $Arr['project_id'],
                'info_id' => $Arr['ids'],
                'entryby' => $this->session->userdata('uid')
            );
            $Where = array('project_id' => $Arr['project_id'], 'invoice_id' => $Arr['invoice_id'], 'id' => $Arr['monthID']);
            $where_date = "(date_month >='$firstyear' AND date_month <= '$lastyear')";
            $this->db->select('*');
            $this->db->from('acc_othermonthdate');
            $this->db->where($Where);
            $this->db->where($where_date);
            $data = $this->db->count_all_results();

            if ($data < 1) {
                $res = $this->mastermodel->InsertMasterData($arr, 'acc_othermonthdate');
            } else {
                $Wheres = array('project_id' => $Arr['project_id'], 'invoice_id' => $Arr['invoice_id'], 'id' => $Arr['monthID']);
                $res = $this->mastermodel->UpdateRecords('acc_othermonthdate', $Wheres, $arr);
                $res = $Arr['monthID'];
            }
        }
        if ($res) {
            $this->session->set_flashdata('msg', "Added Success.");
            redirect(base_url('account/curotherinfo?id=' . $res));
        } else {
            $this->session->set_flashdata('msg', "Data Not Insert.");
            redirect(base_url('account/curotherinfo?id=' . $res));
        }
    }

    public function curotherinfo() {
        $id = $_REQUEST['id'];
        if ($id) {
            $Where = array('id' => $id);
            $monthdata = $this->mastermodel->GetTableData('acc_othermonthdate', $Where);
            if ($monthdata[0]->info_id) {
                $Wheres = array('id' => $monthdata[0]->info_id);
                $data = $this->mastermodel->GetTableData('accountinfo', $Wheres);
            }
        }
        $this->load->view('account/add_curothermonth', compact('data', 'monthdata'));
    }

    public function othermonthsaveteam() {
        //echo '<pre>'; print_r($_REQUEST); die;
        $Arr = $_REQUEST;
        $length = count($Arr['detail_id']);
        if ($length) {
            if ($Arr['save'] == 'save') {
                for ($i = 0; $i < $length; $i++) {
                    $arr = array(
                        'month_id' => $Arr['month_id'],
                        'masterdetail_id' => $Arr['detail_id'][$i],
                        'other_mm' => $Arr['eotmm'][$i],
                        'cumulative_pre_mm' => $Arr['cumulative_pre_mm'][$i],
                        'cumulative_pre_amount' => $Arr['cumulative_pre_amount'][$i],
                        'entryby' => $this->session->userdata('uid')
                    );

                    $Where = array('id' => $Arr['eotdetail_id'][$i]);
                    $data = $this->mastermodel->count_rows('acc_othermonthdetail', $Where);

                    if ($data < 1) {
                        $res = $this->mastermodel->InsertMasterData($arr, 'acc_othermonthdetail');
                    } else {
                        $Wheres = array('id' => $Arr['eotdetail_id'][$i]);
                        $res = $this->mastermodel->UpdateRecords('acc_othermonthdetail', $Wheres, $arr);
                    }
                }
            } else {
                for ($i = 0; $i < $length; $i++) {
                    $arr = array(
                        'month_id' => $Arr['month_id'],
                        'masterdetail_id' => $Arr['detail_id'][$i],
                        'other_mm' => $Arr['eotmm'][$i],
                        'cumulative_pre_mm' => $Arr['cumulative_pre_mm'][$i],
                        'cumulative_pre_amount' => $Arr['cumulative_pre_amount'][$i],
                        'eot_lock' => 1,
                        'entryby' => $this->session->userdata('uid')
                    );
                    $Where = array('id' => $Arr['eotdetail_id'][$i]);
                    $data = $this->mastermodel->count_rows('acc_othermonthdetail', $Where);
                    if ($data < 1) {
                        $res = $this->mastermodel->InsertMasterData($arr, 'acc_othermonthdetail');
                    } else {
                        $Wheres = array('id' => $Arr['eotdetail_id'][$i]);
                        $this->mastermodel->UpdateRecords('acc_othermonthdetail', $Wheres, $arr);
                    }
                }
            }
        }
        if ($res) {
            $this->session->set_flashdata('msg', "Added Success.");
            redirect(base_url('account/otherinfo?id=' . $Arr['month_id']));
        } else {
            $this->session->set_flashdata('msg', "Data Not Insert.");
            redirect(base_url('account/otherinfo?id=' . $Arr['month_id']));
        }
    }

    public function cruothermonthsaveteam() {
        //echo '<pre>'; print_r($_REQUEST); die;
        $Arr = $_REQUEST;
        $length = count($Arr['detail_id']);
        if ($length) {
            if ($Arr['save'] == 'save') {
                for ($i = 0; $i < $length; $i++) {
                    $cumulative_pre_mm = $Arr['cumulative_pre_mm'][$i] + $Arr['eotmm'][$i];
                    $currentrate = $Arr['eotmm'][$i] * $Arr['fix_rate'][$i];
                    $cumulative_pre_amount = $Arr['cumulative_pre_amount'][$i] + $currentrate;

                    $arr = array(
                        'month_id' => $Arr['month_id'],
                        'masterdetail_id' => $Arr['detail_id'][$i],
                        'other_mm' => $Arr['eotmm'][$i],
                        'cumulative_pre_mm' => $cumulative_pre_mm,
                        'cumulative_pre_amount' => $cumulative_pre_amount,
                        'entryby' => $this->session->userdata('uid')
                    );

                    $Where = array('id' => $Arr['eotdetail_id'][$i]);
                    $data = $this->mastermodel->count_rows('acc_othermonthdetail', $Where);

                    if ($data < 1) {
                        $res = $this->mastermodel->InsertMasterData($arr, 'acc_othermonthdetail');
                    } else {
                        $Wheres = array('id' => $Arr['eotdetail_id'][$i]);
                        $res = $this->mastermodel->UpdateRecords('acc_othermonthdetail', $Wheres, $arr);
                    }
                }
            } else {
                for ($i = 0; $i < $length; $i++) {
                    // $cumulative_pre_mm = $Arr['cumulative_pre_mm'][$i] + $Arr['eotmm'][$i];
                    // $cumulative_pre_amount = $Arr['cumulative_pre_amount'][$i] * $cumulative_pre_mm;
                    $cumulative_pre_mm = $Arr['cumulative_pre_mm'][$i] + $Arr['eotmm'][$i];
                    $currentrate = $Arr['eotmm'][$i] * $Arr['fix_rate'][$i];
                    $cumulative_pre_amount = $Arr['cumulative_pre_amount'][$i] + $currentrate;
                    $arr = array(
                        'month_id' => $Arr['month_id'],
                        'masterdetail_id' => $Arr['detail_id'][$i],
                        'other_mm' => $Arr['eotmm'][$i],
                        'cumulative_pre_mm' => $cumulative_pre_mm,
                        'cumulative_pre_amount' => $cumulative_pre_amount,
                        'eot_lock' => 1,
                        'entryby' => $this->session->userdata('uid')
                    );

                    $Where = array('id' => $Arr['eotdetail_id'][$i]);
                    $data = $this->mastermodel->count_rows('acc_othermonthdetail', $Where);
                    if ($data < 1) {
                        $res = $this->mastermodel->InsertMasterData($arr, 'acc_othermonthdetail');
                    } else {
                        $Wheres = array('id' => $Arr['eotdetail_id'][$i]);
                        $this->mastermodel->UpdateRecords('acc_othermonthdetail', $Wheres, $arr);
                    }
                }
            }
        }
        if ($res) {
            $this->session->set_flashdata('msg', "Added Success.");
            redirect(base_url('account/curotherinfo?id=' . $Arr['month_id']));
        } else {
            $this->session->set_flashdata('msg', "Data Not Insert.");
            redirect(base_url('account/curotherinfo?id=' . $Arr['month_id']));
        }
    }

    public function getHoliday($startDate, $endDate) {
        $this->db->select('holiday_date,name');
        $this->db->from('siteoffice_holiday');
        $where_date = "(holiday_date >='$startDate' AND holiday_date <= '$endDate')";
        $this->db->where($where_date);
        $arr = array('is_active' => '1');
        $this->db->where($arr);
        $res = $this->db->get()->result_array();

        $step = '+1 day';
        $format = 'Y-m-d';
        $dates = array();
        $current = strtotime($startDate);
        $endDate = strtotime($endDate);
        while ($current <= $endDate) {
            if (date("D", $current) == "Sun")
                $dates[] = array('holiday_date' => date($format, $current), 'name' => 'Sunday');
            $current = strtotime($step, $current);
        }
        $result = array_merge($res, $dates);
        return isset($result) ? $result : '';
    }

    public function savetax() {
        if ($_REQUEST) {
            $tax_month = date('n', strtotime($_REQUEST['date_month']));
            $tax_year = date('Y', strtotime($_REQUEST['date_month']));
            $data = array(
                'tax_name' => $_REQUEST['tax_name'],
                'percentage' => $_REQUEST['percentage'],
                'date_month' => $_REQUEST['date_month'],
                'tax_month' => $tax_month,
                'tax_year' => $tax_year,
                'entryby' => $this->session->userdata('uid')
            );
            $res1 = $this->mastermodel->InsertMasterData($data, 'tax_master');
        }
        redirect(base_url('account/viewaccountinfo'));
    }

    public function viewtax() {
        $id = $_REQUEST['id'];
        if ($id) {
            $Where = array('id' => $id);
            $infoid = $this->mastermodel->GetTableData('invoicedetail', $Where);
            $Wheres = array('id' => $infoid[0]->infoid);
            $data = $this->mastermodel->GetTableData('accountinfo', $Wheres);
            $project_id = isset($data[0]->project_id) ? $data[0]->project_id : false;
        }
        $this->db->select('*');
        $this->db->from('tax_master');
        $this->db->where('status', '1');
        $taxres = $this->db->get()->result_array();
        $this->load->view('account/viewtaxdetail', compact('id', 'project_id', 'taxres', 'data'));
    }

    public function viewescalation() {
        $id = $_REQUEST['id'];
        if ($id) {
            $Where = array('id' => $id);
            $infoid = $this->mastermodel->GetTableData('invoicedetail', $Where);

            $inID = $infoid[0]->infoid;
            $this->db->select('*');
            $this->db->from('invoicedetail');
            $wheres = "(infoid = $inID and id < $id)";
            $this->db->where($wheres);
            $this->db->order_by('id', 'desc');
            $this->db->limit(1);
            $previnvoice = $this->db->get()->row_array();

            $Wheres = array('id' => $infoid[0]->infoid);
            $data = $this->mastermodel->GetTableData('accountinfo', $Wheres);
            $project_id = isset($data[0]->project_id) ? $data[0]->project_id : false;
        }

        $this->db->select('*');
        $this->db->from('escalation_tax');
        $this->db->where('project_id', $project_id);
        $this->db->where('invoice_id', $id);
        $curtaxres = $this->db->get()->result_array();

        $this->db->select('*');
        $this->db->from('escalation_tax');
        $this->db->where('project_id', $project_id);
        $this->db->where('invoice_id', $previnvoice['id']);
        $prevtaxres = $this->db->get()->result_array();

        if (!empty($curtaxres)) {
            $taxres = $curtaxres;
        } else {
            $taxres = array();
            foreach ($prevtaxres as $val) {
                $val['esc_pretotal'] = $val['totalamount'];
                $taxres[] = $val;
            }
        }
        //echo '<pre>'; print_r($taxres); die;
        $this->load->view('account/viewescalation', compact('id', 'project_id', 'taxres', 'data'));
    }

    public function viewattdence() {
        //redirect(base_url('accountdept/accdept_attendance'));
        $data['data'] = '';
        $this->load->view('account/viewattdence', $data);
    }

    public function viewmonthdate() {
        $data = $_REQUEST;
        $this->session->set_userdata(array('date_month' => $data['date_month']));
        $this->session->userdata('date_month');
        redirect(base_url('account/viewattdence'));
    }

    public function viewproject_ajax_list() {
        $project = $this->Front_model->getproject();
        $srn = 0;
        foreach ($project as $rowData):
            $srn++;
            $row = array();
            $row[] = $srn;
            $row[] = $rowData->project_name;
            $row[] = '<li title="Weekly Attendance View" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#attdencemodel" onclick="viewattdencemodel(' . "'" . $rowData->project_id . "'" . ')"><i class="ace-icon glyphicon glyphicon-eye-open"></i></li> <a href="' . base_url('account/viewemployeeattd/' . $rowData->project_id) . '"><li title="Monthly Attendance View" class="btn btn-warning btn-xs"><i class="ace-icon glyphicon  glyphicon-calendar  bigger-110 icon-only"></i></li></a><li title="Feel Weekly Attendance" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#attdencemodelchk" onclick="viewattdencemodeldata(' . "'" . $rowData->project_id . "'" . ')"><i class="ace-icon glyphicon glyphicon-eye-open"></i></li> ';
            $data[] = $row;
        endforeach;
        $json_data = array(
            "draw" => 1,
            "data" => $data
        );
        echo json_encode($json_data);
        exit();
    }

    public function viewemployeeattd() {
        $project_id = $this->uri->segment(3);
        if ($project_id) {
            $projectname = $this->Front_model->getprojectname($project_id);
            $staffemployeename = $this->Front_model->getemployee_withreplcond($project_id, 3);
            $keyemployeename = $this->Front_model->getemployee_withreplcond($project_id, 1);
            $subemployeename = $this->Front_model->getemployee_withreplcond($project_id, 2);
            $this->load->view('account/showattdence', compact('project_id', 'projectname', 'keyemployeename', 'subemployeename', 'staffemployeename'));
        }
    }

    public function getattendance() {
        if ($_REQUEST) {
            $data = $_REQUEST;
            $projectname = $this->Front_model->getprojectname($data['project_id']);
            $keyemployeename = $this->Front_model->getemployee_withreplcond($data['project_id'], 1);
            $subemployeename = $this->Front_model->getemployee_withreplcond($data['project_id'], 2);
            $staffemployeename = $this->Front_model->getemployee_withreplcond($data['project_id'], 3);
            if (!empty($_REQUEST['emp_id'])) {
                $Arr = $_REQUEST;
                $this->Front_model->saveinfotime($Arr);
            }
            $this->load->view('account/attendancedata', compact('data', 'projectname', 'staffemployeename', 'subemployeename', 'keyemployeename'));
        } else {
            redirect(base_url('account/viewattdence'));
        }
    }

    public function getattendancedaata() {
        //echo '<pre>'; print_r($_REQUEST); die;
        if ($_REQUEST) {
            $data = $_REQUEST;
            $arrDate = explode(',', $data['dates']);
            if (count($arrDate) <= 8) {
                $projectname = $this->Front_model->getprojectname($data['project_ids']);
                $keyemployeename = $this->Front_model->getemployee_withreplcond($data['project_ids'], 1);
                $subemployeename = $this->Front_model->getemployee_withreplcond($data['project_ids'], 2);
                $staffemployeename = $this->Front_model->getemployee_withreplcond($data['project_ids'], 3);

                // echo '<pre>'; print_r($staffemployeename); die;
                //Check Replc Effected Date Will Be Greater Than Selected Date.
                $newRecArray = array_merge($keyemployeename, $subemployeename, $staffemployeename);
                $reTurnArr = array();
                if ($newRecArray) {
                    foreach ($newRecArray as $dtRow) {
                        if ($dtRow->replacement_effacted_date):
//                            foreach ($arrDate as $DateRow) {
//                                if (strtotime($DateRow) < strtotime($dtRow->replacement_effacted_date)) {
//                                    $this->session->set_flashdata('errormsg', 'Selected Date will be Greater or Equal Than Replacement Effacted Date');
//                                    redirect(base_url('account/viewattdence'));
//                                }
//                            }
                        endif;
                    }
                }
                //Close Condition Replc Effected Date Will Be Greater Than Selected Date.
                if (!empty($data['emp_id'])) {
                    $Arr = $data;
                    $res = $this->Front_model->saveinfotimeattd($Arr);
                    unset($data['project_id']);
                    unset($data['emp_id']);
                    unset($data['designation_id']);
                    unset($data['date_month']);
                    unset($data['attend']);
                }
                $this->load->view('account/viewattendancedata', compact('data', 'projectname', 'staffemployeename', 'subemployeename', 'keyemployeename'));
            } else {
                $this->session->set_flashdata('errormsg', 'Please select only 7 days Max');
                redirect(base_url('account/viewattdence'));
            }
        } else {
            redirect(base_url('account/viewattdence'));
        }
    }

//    public function getattendancedaata() {
//      //  echo '<pre>'; print_r($_REQUEST); die;
//        if ($_REQUEST) {
//            $data = $_REQUEST;
//            $arrDate = explode(',', $data['dates']);
//            if (count($arrDate) <= 8) {
//                $projectname = $this->Front_model->getprojectname($data['project_ids']);
//                $keyemployeename = $this->Front_model->getemployee_withreplcond($data['project_ids'], 1);
//                $subemployeename = $this->Front_model->getemployee_withreplcond($data['project_ids'], 2);
//                $staffemployeename = $this->Front_model->getemployee_withreplcond($data['project_ids'], 3);
//
//                // echo '<pre>'; print_r($staffemployeename); die;
//                //Check Replc Effected Date Will Be Greater Than Selected Date.
//                $newRecArray = array_merge($keyemployeename, $subemployeename, $staffemployeename);
//                $reTurnArr = array();
//                if ($newRecArray) {
//                    foreach ($newRecArray as $dtRow) {
//                        if ($dtRow->replacement_effacted_date):
////                            foreach ($arrDate as $DateRow) {
////                                if (strtotime($DateRow) < strtotime($dtRow->replacement_effacted_date)) {
////                                    $this->session->set_flashdata('errormsg', 'Selected Date will be Greater or Equal Than Replacement Effacted Date');
////                                    redirect(base_url('account/viewattdence'));
////                                }
////                            }
//                        endif;
//                    }
//                }
//                //Close Condition Replc Effected Date Will Be Greater Than Selected Date.
//                if (!empty($data['emp_id'])) {
//                    $Arr = $data;
//                    $res = $this->Front_model->saveinfotimeattd($Arr);
//                    unset($data['project_id']);
//                    unset($data['emp_id']);
//                    unset($data['designation_id']);
//                    unset($data['date_month']);
//                    unset($data['attend']);
//                }
//                $this->load->view('account/viewattendancedata', compact('data', 'projectname', 'staffemployeename', 'subemployeename', 'keyemployeename'));
//            } else {
//                $this->session->set_flashdata('errormsg', 'Please select only 7 days Max');
//                redirect(base_url('account/viewattdence'));
//            }
//        } else {
//            redirect(base_url('account/viewattdence'));
//        }
//    }

    public function getattendancedata() {
        if (!empty($_REQUEST['project_id'])) {
            $Arr = $_REQUEST;
            $this->Front_model->saveinfotimeattd($Arr);
        }
        redirect(base_url('account/viewattdence'));
    }

    //Ash Coide For New Report..
    public function newreportsummary() {
        $data['error'] = '';
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.tm_projects.id,$db2.tm_projects.project_name");
        $this->db->from("$db2.tm_projects");
        $this->db->where(array("$db2.tm_projects.is_active" => '1'));
        $this->db->where("($db2.tm_projects.project_category='ie' OR $db2.tm_projects.project_category='ae')", NULL, FALSE);
        $this->db->order_by("$db2.tm_projects.project_name", 'ASC');
        $data['ProjRec'] = $this->db->get()->result();
        $this->load->view('account/newreportsummary_view', $data);
    }

    //Ajax For Data Table Asheesh...
    public function newreportsummary_ajax() {
        $list = $this->Newreportsummary_model->get_datatables();
//        echo '<pre>';
//        print_r($list);
//        die;

        foreach ($list as $val) {
            $no++;
            $row = array();
            $row[] = 'ddd';
            $row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = '';
            $data[] = $row;

            $json_data = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => '0',
                "recordsFiltered" => '0',
                "data" => $data);
        }
        echo json_encode($json_data);
        exit;

//        foreach ($list as $val) {
//            $no++;
//            $row = array();
//            $row[] = $no;
//            $row[] = "<a target='_blank' href='" . base_url('projectplanning/userdetail/157300') . "'>" . $val->project_name . "</a>";
//            $row[] = $val->invoice_date;
//            $row[] = number_format($val->contract_value, 2);
//            $row[] = number_format($val->total_cumulative, 2);
//            $row[] = number_format($val->remuneration, 2);
//            $row[] = number_format($val->reimbursable, 2);
//            $row[] = '';
//            $row[] = number_format($val->current_month_bill, 2);
//            $row[] = number_format($val->balance, 2);
//            $data[] = $row;
//        }
//        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->Newreportsummary_model->count_all(),
//            "recordsTotal" => '0',
//            "recordsFiltered" => $this->Newreportsummary_model->count_filtered(),
//            "data" => $data,
//        );
        //output to json format
        // echo json_encode($output);
    }

    public function viewreceivedamount() {
        $id = $_REQUEST['id'];
        if ($id) {
            $Where = array('id' => $id);
            $infoid = $this->mastermodel->GetTableData('invoicedetail', $Where);
            $Wheres = array('id' => $infoid[0]->infoid);
            $data = $this->mastermodel->GetTableData('accountinfo', $Wheres);
            $project_id = isset($data[0]->project_id) ? $data[0]->project_id : false;
        }
        $this->db->select('*');
        $this->db->from('acc_receivedamount');
        $this->db->where('project_id', $project_id);
        $this->db->where('invoice_id', $id);
        $res = $this->db->get()->result_array();
        $this->load->view('account/viewreceivedamount', compact('id', 'project_id', 'res', 'data'));
    }

    public function savereceivedamt() {
        if ($_REQUEST['update'] == '1') {
            $length = count($_REQUEST['countdata']);
            $project_id = $_REQUEST['project_id'];
            $invoice_id = $_REQUEST['invoice_id'];
            for ($i = 0; $i < $length; $i++) {
                $ids = $_REQUEST['ids'][$i];
                $this->db->select('*');
                $this->db->from('acc_receivedamount');
                $this->db->where('id', $ids);
                $this->db->where('project_id', $project_id);
                $this->db->where('invoice_id', $invoice_id);
                $taxres = $this->db->get()->row_array();

                if (empty($taxres)) {
                    $data = array(
                        'project_id' => $_REQUEST['project_id'],
                        'invoice_id' => $_REQUEST['invoice_id'],
                        'manpower' => $_REQUEST['manpower'][$i],
                        'reimbursable' => $_REQUEST['reimbursable'][$i],
                        'taxs' => $_REQUEST['taxs'][$i],
                        'reason' => $_REQUEST['reason'][$i],
                        'entryby' => $this->session->userdata('uid')
                    );
                    $res1 = $this->mastermodel->InsertMasterData($data, 'acc_receivedamount');
                } else {
                    $datas = array(
                        'project_id' => $_REQUEST['project_id'],
                        'invoice_id' => $_REQUEST['invoice_id'],
                        'manpower' => $_REQUEST['manpower'][$i],
                        'reimbursable' => $_REQUEST['reimbursable'][$i],
                        'taxs' => $_REQUEST['taxs'][$i],
                        'reason' => $_REQUEST['reason'][$i],
                        'entryby' => $this->session->userdata('uid')
                    );
                    $Where = array('project_id' => $project_id, 'invoice_id' => $invoice_id, 'id' => $ids);
                    $this->mastermodel->UpdateRecords('acc_receivedamount', $Where, $datas);
                }
            }
        } else {
            $data = array(
                'project_id' => $_REQUEST['project_id'],
                'invoice_id' => $_REQUEST['invoice_id'],
                'manpower' => $_REQUEST['manpower'],
                'reimbursable' => $_REQUEST['reimbursable'],
                'taxs' => $_REQUEST['taxs'],
                'reason' => $_REQUEST['reason'],
                'entryby' => $this->session->userdata('uid')
            );
            $res1 = $this->mastermodel->InsertMasterData($data, 'acc_receivedamount');
        }

        redirect(base_url('account/viewinvoice?id=' . $_REQUEST['accteam_id']));
    }

    public function viewpercentage() {
        $id = $_REQUEST['id'];
        if ($id) {
            $Where = array('id' => $id);
            $infoid = $this->mastermodel->GetTableData('invoicedetail', $Where);

            $Wheres = array('id' => $infoid[0]->infoid);
            $data = $this->mastermodel->GetTableData('accountinfo', $Wheres);
            $project_id = isset($data[0]->project_id) ? $data[0]->project_id : false;

            $invoiceID = $id;
            $invoicedata = $this->Front_model->getaccountdetailforexcel($invoiceID);
            $decimalplace = $invoicedata->decimal_place;
            $no_days = $invoicedata->no_days;

            $currentdate = date('Y-m-d', strtotime($invoicedata->invoice_date . "-1 month"));
            $cruyears = date('Y', strtotime($currentdate));
            $crumonths = date('n', strtotime($currentdate));
            $crumonthschk = date('m', strtotime($currentdate));
            $d = cal_days_in_month(CAL_GREGORIAN, $crumonths, $cruyears);

            $startDate = date('Y-m-d', strtotime($cruyears . '-' . $crumonthschk . '-01'));
            $endDate = date('Y-m-d', strtotime($cruyears . '-' . $crumonthschk . "-$d"));

            if (!empty($no_days)) {
                $divideno_days = $no_days;
            } else {
                $divideno_days = cal_days_in_month(CAL_GREGORIAN, $crumonths, $cruyears);
            }

            $keyteamdata = $this->Front_model->getexceldataproject_withreplc($invoicedata->project_id, $invoicedata->project_numberid, $invoicedata->invoiceno, 1);
            $subteamdata = $this->Front_model->getexceldataproject_withreplc($invoicedata->project_id, $invoicedata->project_numberid, $invoicedata->invoiceno, 2);
            $adminteamdata = $this->Front_model->getexceldataproject_withreplc($invoicedata->project_id, $invoicedata->project_numberid, $invoicedata->invoiceno, 3);

            $adminID = count($adminteamdata);
            $keyID = count($keyteamdata);
            $subID = count($subteamdata);

            $summerytotalmmamountkeys = 0;
            for ($i = 0; $i < $keyID; $i++) {
                $intermittent = $this->Front_model->getintermittent($invoicedata->project_id, $keyteamdata[$i]->emp_id);
                if (!empty($intermittent)) {
                    $manmonthscrukey = $this->Front_model->getintermittentattd($invoicedata->project_id, $keyteamdata[$i]->emp_id, $keyteamdata[$i]->designation_id, $crumonths, $cruyears);
                    if (($manmonthscrukey->present) > 0) {
                        $mmcountprekey = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                    } else {
                        $mmcountprekey = 0;
                    }
                } else {
                    $timemonth = $this->Front_model->gettimesheetdetail($invoicedata->project_id, $keyteamdata[$i]->designation_id, $keyteamdata[$i]->emp_id, $crumonths, $cruyears);
                    if (($timemonth->leave + $timemonth->absent) > 0) {
                        $mmcountprekey = round((1 - ($timemonth->leave + $timemonth->absent) / $divideno_days), $decimalplace);
                        if ($mmcountprekey < 0) {
                            $mmcountprekey = 0;
                        }
                    } else {
                        if (($timemonth->present) > 0) {
                            $mmcountprekey = 1;
                        } else {
                            $mmcountprekey = 0;
                        }
                    }
                }
                if ($keyteamdata[$i]->rate > 0) {
                    $replacement_reducationrate = $keyteamdata[$i]->rate - (($keyteamdata[$i]->rate * $keyteamdata[$i]->replacement_reducationrate) / 100);
                } else {
                    $replacement_reducationrate = $keyteamdata[$i]->rate;
                }
                $mmamountkey = $mmcountprekey * $replacement_reducationrate;
                $summerytotalmmamountkeys += $mmamountkey;
            }
            $summerytotalmmamountkeyssub = 0;
            for ($i = 0; $i < $subID; $i++) {
                $intermittent = $this->Front_model->getintermittent($invoicedata->project_id, $subteamdata[$i]->emp_id);
                if (!empty($intermittent)) {
                    $manmonthscrukey = $this->Front_model->getintermittentattd($invoicedata->project_id, $subteamdata[$i]->emp_id, $subteamdata[$i]->designation_id, $crumonths, $cruyears);
                    if (($manmonthscrukey->present) > 0) {
                        $mmcountpresub = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                    } else {
                        $mmcountpresub = 0;
                    }
                } else {
                    $timemonthsub = $this->Front_model->gettimesheetdetail($invoicedata->project_id, $subteamdata[$i]->designation_id, $subteamdata[$i]->emp_id, $crumonths, $cruyears);
                    if (($timemonthsub->leave + $timemonthsub->absent) > 0) {
                        $mmcountpresub = round((1 - ($timemonthsub->leave + $timemonthsub->absent) / $divideno_days), $decimalplace);
                        if ($mmcountpresub < 0) {
                            $mmcountpresub = 0;
                        }
                    } else {
                        if (($timemonthsub->present) > 0) {
                            $mmcountpresub = 1;
                        } else {
                            $mmcountpresub = 0;
                        }
                    }
                }
                if ($subteamdata[$i]->rate > 0) {
                    $replacement_reducationrate = $subteamdata[$i]->rate - (($subteamdata[$i]->rate * $subteamdata[$i]->replacement_reducationrate) / 100);
                } else {
                    $replacement_reducationrate = $subteamdata[$i]->rate;
                }
                $mmamountsub = $mmcountpresub * $replacement_reducationrate;
                $summerytotalmmamountkeyssub += $mmamountsub;
            }

            $summerytotalmmamountadmim = 0;
            for ($i = 0; $i < $adminID; $i++) {
                $intermittent = $this->Front_model->getintermittent($invoicedata->project_id, $adminteamdata[$i]->emp_id);
                if (!empty($intermittent)) {

                    $manmonthscrukey = $this->Front_model->getintermittentattd($invoicedata->project_id, $adminteamdata[$i]->emp_id, $adminteamdata[$i]->designation_id, $crumonths, $cruyears);
                    if (($manmonthscrukey->present) > 0) {
                        $mmcountpreadmin = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                    } else {
                        $mmcountpreadmin = 0;
                    }
                } else {
                    $timemonth = $this->Front_model->gettimesheetdetail($invoicedata->project_id, $adminteamdata[$i]->designation_id, $adminteamdata[$i]->emp_id, $crumonths, $cruyears);
                    if (($timemonth->leave + $timemonth->absent) > 0) {
                        $mmcountpreadmin = round((1 - ($timemonth->leave + $timemonth->absent) / $divideno_days), $decimalplace);
                        if ($mmcountpreadmin < 0) {
                            $mmcountpreadmin = 0;
                        }
                    } else {
                        if (($timemonth->present) > 0) {
                            $mmcountpreadmin = 1;
                        } else {
                            $mmcountpreadmin = 0;
                        }
                    }
                }
                if ($adminteamdata[$i]->rate > 0) {
                    $replacement_reducationrate = $adminteamdata[$i]->rate - (($adminteamdata[$i]->rate * $adminteamdata[$i]->replacement_reducationrate) / 100);
                } else {
                    $replacement_reducationrate = $adminteamdata[$i]->rate;
                }
                $mmamountadmin = $mmcountpreadmin * $replacement_reducationrate;
                $summerytotalmmamountadmim += $mmamountadmin;
            }
            $remprof = $summerytotalmmamountkeys + $summerytotalmmamountkeyssub;
            $supportingstaff = $summerytotalmmamountadmim;
        }
        $this->load->view('account/viewpercentage', compact('id', 'project_id', 'taxres', 'data', 'remprof', 'supportingstaff'));
    }

    public function savepercentage() {
        if ($_REQUEST) {
            // echo '<pre>'; print_r($_REQUEST); die;
            $length = count($_REQUEST['percentagecount']);
            $project_id = $_REQUEST['project_id'];
            $invoice_id = $_REQUEST['invoice_id'];
            $totalmmcum = 0;
            $totalmmamount = 0;
            for ($i = 0; $i < $length; $i++) {
                $ids = $_REQUEST['ids'][$i];
                $this->db->select('*');
                $this->db->from('acc_percentage');
                $this->db->where('id', $ids);
                $this->db->where('project_id', $project_id);
                $this->db->where('invoice_id', $invoice_id);
                $perres = $this->db->get()->row_array();
                if (empty($perres)) {
                    $totalmmcum = $_REQUEST['cumpremm'][$i] + $_REQUEST['curmm'][$i];
                    $totalmmamount = $_REQUEST['cumpreamt'][$i] + $_REQUEST['cumamt'][$i];
                    $data = array('project_id' => $_REQUEST['project_id'],
                        'invoice_id' => $_REQUEST['invoice_id'],
                        'type_id' => $_REQUEST['type_id'][$i],
                        'cumpremm' => $_REQUEST['cumpremm'][$i],
                        'cumpreamt' => $_REQUEST['cumpreamt'][$i],
                        'curmm' => $_REQUEST['curmm'][$i],
                        'cumamt' => $_REQUEST['cumamt'][$i],
                        'totalmmcum' => $totalmmcum,
                        'totalmmamount' => $totalmmamount,
                        'entryby' => $this->session->userdata('uid')
                    );
                    $res1 = $this->mastermodel->InsertMasterData($data, 'acc_percentage');
                } else {
                    $totalmmcum = $_REQUEST['cumpremm'][$i] + $_REQUEST['curmm'][$i];
                    $totalmmamount = $_REQUEST['cumpreamt'][$i] + $_REQUEST['cumamt'][$i];
                    $datas = array('cumpremm' => $_REQUEST['cumpremm'][$i],
                        'cumpreamt' => $_REQUEST['cumpreamt'][$i],
                        'curmm' => $_REQUEST['curmm'][$i],
                        'cumamt' => $_REQUEST['cumamt'][$i],
                        'totalmmcum' => $totalmmcum,
                        'totalmmamount' => $totalmmamount,
                        'entryby' => $this->session->userdata('uid')
                    );
                    $Where = array('project_id' => $project_id, 'invoice_id' => $invoice_id, 'id' => $ids);
                    $this->mastermodel->UpdateRecords('acc_percentage', $Where, $datas);
                }
            }
        }
        redirect(base_url('account/viewinvoice?id=' . $_REQUEST['accteam_id']));
    }

    public function savetaxinfo() {
        if ($_REQUEST) {
            //echo '<pre>'; print_r($_REQUEST); die;
            $length = count($_REQUEST['percentagecount']);
            $project_id = $_REQUEST['project_id'];
            $invoice_id = $_REQUEST['invoice_id'];
            $currenttaxtotal = 0;
            $percentage = 0;
            $totaltaxcum = 0;
            foreach ($_REQUEST['tax_id'] as $reCoRd) {
                $tAxId = $reCoRd;
                $sTaTus = $_REQUEST['status' . $reCoRd];
                $ContractAmount = $_REQUEST['contractamount' . $reCoRd];
                $AmOuNt = $_REQUEST['amount' . $reCoRd];
                // echo $project_id . "-->" . $invoice_id . "-->" . $tAxId . "-->" . $sTaTus . "-->" . $ContractAmount . "-=>" . $AmOuNt . "<br>";
                $wHeReArr = array('project_id' => $project_id, 'invoice_id' => $invoice_id, 'tax_id' => $tAxId);
                $this->db->select('id');
                $this->db->from('project_tax');
                $this->db->where($wHeReArr);
                $NumROWtaxres = $this->db->get()->num_rows();

                if ($NumROWtaxres < 1) {
                    //Insert Record to project_tax.
                    //Get Current Tax Total Amount And Total Tax Cum..
                    $this->db->select('*');
                    $this->db->from('tax_master');
                    $this->db->where('id', $reCoRd);
                    $this->db->where('status', 1);
                    $taxmaster = $this->db->get()->row_array();
                    $percentage = $taxmaster['percentage'];

                    $this->db->select('*');
                    $this->db->from('escalation_tax');
                    $this->db->where('project_id', $project_id);
                    $this->db->where('invoice_id', $invoice_id);
                    $this->db->order_by('id', 'desc');
                    $this->db->limit(1);
                    $escalation = $this->db->get()->row_array();

                    if ($sTaTus == 1) {
                        $currenttaxtotal = round(($escalation['invoicetotal'] * $percentage) / 100);
                        $totaltaxcum = $AmOuNt + $currenttaxtotal;
                    } else {
                        $totaltaxcum = $AmOuNt;
                    }

                    $InsertdataArr = array('project_id' => $project_id, 'invoice_id' => $invoice_id, 'tax_id' => $tAxId,
                        'status' => $sTaTus,
                        'contractamount' => $ContractAmount,
                        'amount' => $AmOuNt,
                        'currenttaxtotal' => $currenttaxtotal,
                        'totaltaxcum' => $totaltaxcum,
                        'entryby' => $this->session->userdata('uid'));
                    $res1 = $this->mastermodel->InsertMasterData($InsertdataArr, 'project_tax');
                } else {
                    $this->db->select('*');
                    $this->db->from('tax_master');
                    $this->db->where('id', $reCoRd);
                    $this->db->where('status', 1);
                    $taxmaster = $this->db->get()->row_array();
                    $percentage = $taxmaster['percentage'];

                    $this->db->select('*');
                    $this->db->from('escalation_tax');
                    $this->db->where('project_id', $project_id);
                    $this->db->where('invoice_id', $invoice_id);
                    $this->db->order_by('id', 'desc');
                    $this->db->limit(1);
                    $escalation = $this->db->get()->row_array();

                    // echo $escalation['invoicetotal']; die;
                    if ($sTaTus == 1) {
                        $currenttaxtotal = round(($escalation['invoicetotal'] * $percentage) / 100);
                        $totaltaxcum = $AmOuNt + $currenttaxtotal;
                    } else {
                        $totaltaxcum = $AmOuNt;
                    }

                    //Update Record to project_tax.
                    $datasUpd = array('status' => $sTaTus, 'contractamount' => $ContractAmount, 'amount' => $AmOuNt, 'currenttaxtotal' => $currenttaxtotal,
                        'totaltaxcum' => $totaltaxcum);
                    $this->mastermodel->UpdateRecords('project_tax', $wHeReArr, $datasUpd);
                }
            }
        }
        redirect(base_url('account/viewinvoice?id=' . $_REQUEST['accteam_id']));
    }

    public function numberconvert($number) {
        $no = round($number);
        $point = round($number - $no, 2) * 100;
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();
        $words = array('0' => '', '1' => 'one', '2' => 'two',
            '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
            '7' => 'seven', '8' => 'eight', '9' => 'nine',
            '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
            '13' => 'thirteen', '14' => 'fourteen',
            '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
            '18' => 'eighteen', '19' => 'nineteen', '20' => 'twenty',
            '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
            '60' => 'sixty', '70' => 'seventy',
            '80' => 'eighty', '90' => 'ninety');
        $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
        while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number] .
                        " " . $digits[$counter] . $plural . " " . $hundred :
                        $words[floor($number / 10) * 10]
                        . " " . $words[$number % 10] . " "
                        . $digits[$counter] . $plural . " " . $hundred;
            } else
                $str[] = null;
        }
        $str = array_reverse($str);
        $result = implode('', $str);
        $points = ($point) ?
                "." . $words[$point / 10] . " " .
                $words[$point = $point % 10] : '';
        $recReturn = "( " . $result . "Rupees Only )";
        return ucwords($recReturn);
    }

    public Function getprojStartDateByProjID($proj_id) {
        $this->db->select('project_start_date');
        $this->db->from('accountinfo');
        $this->db->where(array('project_id' => $proj_id));
        $invoicecum = $this->db->get()->row();
        return ($invoicecum) ? date("d-m-Y", strtotime($invoicecum->project_start_date)) : date("d-m-Y");
    }

    public Function GetAc_IFSCByProjId($proj_id) {
        $this->db->select('bank_accno,rtgs_code,client_gst_no,clnt_gst_state_code');
        $this->db->from('accountinfo');
        $this->db->where(array('project_id' => $proj_id));
        $AcDetailArr = $this->db->get()->row();
        return ($AcDetailArr) ? $AcDetailArr : null;
    }

    public Function GetStateNameById($state_id) {
        $this->db->SELECT('state_name');
        $this->db->FROM('states');
        $this->db->WHERE(array('state_id' => $state_id));
        $StateDetailArr = $this->db->get()->row();
        return ($StateDetailArr) ? $StateDetailArr->state_name : null;
    }

    public function lastDateInvc($invoice_id) {
        $this->db->select('invexc.invoice_date');
        $this->db->from('invoicedetail  as invexc');
        $this->db->where(array('id' => $invoice_id));
        $InvoiceExcl = $this->db->get()->row_array();
        $invcDateRec = $InvoiceExcl['invoice_date'];
        $prev_month_ts = date("d-m-Y", strtotime("$invcDateRec -1 month"));
        $lastdateRc = date('t', strtotime($prev_month_ts));
        $RecArr = array("lastdateRec" => $lastdateRc, "monthYear" => $prev_month_ts);
        return ($RecArr) ? $RecArr : null;
    }

    public function createDateRangeArray($strDateFrom, $strDateTo) {
        $aryRange = array();
        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));
        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                array_push($aryRange, date('Y-m', $iDateFrom));
            }
        }
        $result = array_unique($aryRange);
        return $result;
    }

    public function GetLastId_escalation_tax($project_id, $invoice_id) {
        $this->db->select('h.id');
        $this->db->from('escalation_tax as h');
        $this->db->where('h.project_id', $project_id);
        $this->db->where('h.invoice_id', $invoice_id);
        $this->db->order_by('h.id', "DESC");
        $this->db->limit('1');
        $RecIDD = $this->db->get()->row();
        return ($RecIDD->id) ? $RecIDD->id : null;
    }

    //New Function By Asheesh Full Date List Array... 29-05-2019
    public function GetDateRangeArray($strDateFrom, $strDateTo) {
        $aryRange = array();
        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));
        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                array_push($aryRange, date('Y-m-d', $iDateFrom));
            }
        }
        $result = array_unique($aryRange);
        return $result;
    }

    //Clear Data Invoice Lock..
    public function savecleardata() {

        if ($_REQUEST) {
            $keyteamdata = getAssignuserinproject($_REQUEST['project_ids'], 1);
            $subteamdata = getAssignuserinproject($_REQUEST['project_ids'], 2);
            $adminteamdata = getAssignuserinproject($_REQUEST['project_ids'], 3);

            $this->db->select('*');
            $this->db->from('invoicedetail');
            $this->db->where('project_numberid', $_REQUEST['project_numberids']);
            $this->db->where('infoid', $_REQUEST['clear_id']);
            $this->db->order_by('id', 'desc');
            $resinvoicedata = $this->db->get()->row_array();

            $currentdate = date('Y-m-d', strtotime($resinvoicedata['invoice_date'] . "-1 month"));
            $cruyears = date('Y', strtotime($currentdate));
            $crumonths = date('n', strtotime($currentdate));
            $prefirst = $cruyears . '-' . $crumonths . '-01';
            $prelast = $cruyears . '-' . $crumonths . '-31';
            $this->db->select('*');
            $this->db->from('invoicedetail');
            $this->db->where('project_numberid', $_REQUEST['project_numberids']);
            $this->db->where('infoid', $_REQUEST['clear_id']);
            $where_date = "(invoice_date >='$prefirst' AND invoice_date <= '$prelast')";
            $this->db->where($where_date);
            $this->db->order_by('id', 'desc');
            $respreviousdata = $this->db->get()->row_array();
            $this->db->select('*');
            $this->db->from('accountinfo');
            $this->db->where('id', $_REQUEST['clear_id']);
            $invoicedec = $this->db->get()->row_array();
            $decimalplace = $invoicedec['decimal_place'];
            $no_days = $invoicedec['no_days'];
            if (!empty($no_days)) {
                $divideno_days = $no_days;
            } else {
                $divideno_days = cal_days_in_month(CAL_GREGORIAN, $crumonths, $cruyears);
            }

            //Typer 1..
            if ($keyteamdata) {
                foreach ($keyteamdata as $keyval) {
                    //echo '<pre>'; print_r($keyval);
                    $wheredatachk = array(
                        'project_id' => $_REQUEST['project_ids'],
                        'projectno_id' => $_REQUEST['project_numberids'],
                        'invoiceno_id' => $respreviousdata['invoice_no'],
                        'designation_id' => $keyval->designation_id,
                        'emp_id' => $keyval->empname
                    );
                    $this->db->select('*');
                    $this->db->from('invoicesave');
                    $this->db->where($wheredatachk);
                    $invoicechk = $this->db->get()->row_array();
                    if (!empty($invoicechk)) {
                        $cumulative_pre_mm = $invoicechk['totalcumulativemm'];
                        $cumulative_pre_amount = $invoicechk['totalcumulativeamount'];
                    } else {
                        $cumulative_pre_mm = $keyval->cumulative_pre_mm;
                        $cumulative_pre_amount = $keyval->cumulative_pre_amount;
                    }
                    $wheredata = array(
                        'project_id' => $_REQUEST['project_ids'],
                        'projectno_id' => $_REQUEST['project_numberids'],
                        'invoiceno_id' => $resinvoicedata['invoice_no'],
                        'designation_id' => $keyval->designation_id,
                        'emp_id' => $keyval->empname
                    );

                    $this->db->select('totalcumulativemm,totalcumulativeamount');
                    $this->db->from('invoicesave');
                    $this->db->where($wheredata);
                    $invoicecum = $this->db->get()->row_array();

                    if (!empty($invoicecum)) {
                        $totalcumulativemm = $invoicecum['totalcumulativemm'];
                        $totalcumulativeamount = $invoicecum['totalcumulativeamount'];
                    } else {
                        $intermittent = getintermittent($_REQUEST['project_ids'], $keyval->empname);
                        //echo '<pre>'; print_r($intermittent);
                        if (!empty($intermittent)) {
                            $manmonthscrukey = getintermittentattd($_REQUEST['project_ids'], $keyval->empname, $keyval->designation_id, $crumonths, $cruyears);
                            //echo '<pre>'; print_r($manmonthscrukey);
                            //$totalnoofdays = cal_days_in_month(CAL_GREGORIAN,$crumonths,$cruyears);
                            $totalnoofdays = 30;
                            if (($manmonthscrukey->present) > 0) {
                                //$mmcountscrukey = round((1- ($totalnoofdays - $manmonthscrukey->present) / 30),$decimalplace);
                                $mmcountscrukey = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                            } else {
                                $mmcountscrukey = 0;
                            }
                        } else {
                            $manmonthscrukey = getuseattd($_REQUEST['project_ids'], $keyval->empname, $keyval->designation_id, $crumonths, $cruyears);
                            if (($manmonthscrukey->leave + $manmonthscrukey->absent) > 0) {
                                $mmcountscrukey = round((1 - ($manmonthscrukey->leave + $manmonthscrukey->absent) / $divideno_days), $decimalplace);
                                if ($mmcountscrukey < 0) {
                                    $mmcountscrukey = 0;
                                }
                            } else {
                                if (($manmonthscrukey->present) > 0) {
                                    $mmcountscrukey = 1;
                                } else {
                                    $mmcountscrukey = 0;
                                }
                            }
                        }
                        $totalcumulativemm = $cumulative_pre_mm + $mmcountscrukey;
                        if ($keyval->replacement_reducationrate > 0) {
                            $replacement_reducationrate = $keyval->rate - (($keyval->rate * $keyval->replacement_reducationrate) / 100);
                        } else {
                            $replacement_reducationrate = $keyval->rate;
                        }
                        //$ratered = $keyval->rate * $replacement_reducationrate;
                        $mmamountkey = $mmcountscrukey * $replacement_reducationrate;
                        $totalcumulativeamount = $cumulative_pre_amount + $mmamountkey;
                    }

                    //Insert Record Into Invoice Save Key 1;
                    $arr = array('info_id' => $_REQUEST['clear_id'],
                        'invoiceno_id' => $_REQUEST['invoice_no_id'],
                        'project_id' => $_REQUEST['project_ids'],
                        'projectno_id' => $_REQUEST['project_numberids'],
                        'designation_id' => $keyval->designation_id,
                        'emp_id' => $keyval->empname,
                        'srno' => $keyval->srno,
                        'key_id' => 1,
                        'date_of_joining' => $keyval->date_of_joining,
                        'userfullname' => $keyval->userfullname,
                        'designation_name' => $keyval->designation_name,
                        'mm' => $keyval->man_months,
                        'balance_mm' => $keyval->balancemm,
                        'rate' => $keyval->rate,
                        'replacement_reducationrate' => $keyval->replacement_reducationrate,
                        'cumulativemm' => $cumulative_pre_mm,
                        'cumulativeamount' => $cumulative_pre_amount,
                        'totalcumulativemm' => $totalcumulativemm,
                        'totalcumulativeamount' => $totalcumulativeamount,
                        'entryby' => $this->session->userdata('uid'),
                        'created' => date('Y-m-d')
                    );

                    $Where = array(
                        'info_id' => $_REQUEST['clear_id'],
                        'invoiceno_id' => $_REQUEST['invoice_no_id'],
                        'project_id' => $_REQUEST['project_ids'],
                        'projectno_id' => $_REQUEST['project_numberids'],
                        'designation_id' => $keyval->designation_id,
                        'emp_id' => $keyval->empname,
                        'key_id' => 1
                    );



                    $data = $this->mastermodel->count_rows('invoicesave', $Where);

                    if ($data < 1) {
                        $res = $this->mastermodel->InsertMasterData($arr, 'invoicesave');
                    } else {
                        //Update Record Into Invoice Save Key 1;
                        //10 Jan Asheesh Code... calculating mm from attandance
                        $intermittent = getintermittent($_REQUEST['project_ids'], $keyval->empname);
                        if (!empty($intermittent)) {
                            $manmonthscrukey = getintermittentattd($_REQUEST['project_ids'], $keyval->empname, $keyval->designation_id, $crumonths, $cruyears);
                            $totalnoofdays = 30;
                            if (($manmonthscrukey->present) > 0) {
                                $mmcountscrukey = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                            } else {
                                $mmcountscrukey = 0;
                            }
                        } else {
                            $manmonthscrukey = getuseattd($_REQUEST['project_ids'], $keyval->empname, $keyval->designation_id, $crumonths, $cruyears);
                            if (($manmonthscrukey->leave + $manmonthscrukey->absent) > 0) {
                                $mmcountscrukey = round((1 - ($manmonthscrukey->leave + $manmonthscrukey->absent) / $divideno_days), $decimalplace);
                                if ($mmcountscrukey < 0) {
                                    $mmcountscrukey = 0;
                                }
                            } else {
                                if (($manmonthscrukey->present) > 0) {
                                    $mmcountscrukey = 1;
                                } else {
                                    $mmcountscrukey = 0;
                                }
                            }
                        }
                        $totalcumulativemm = $cumulative_pre_mm + $mmcountscrukey;
                        if ($keyval->replacement_reducationrate > 0) {
                            $replacement_reducationrate = $keyval->rate - (($keyval->rate * $keyval->replacement_reducationrate) / 100);
                        } else {
                            $replacement_reducationrate = $keyval->rate;
                        }
                        $mmamountkey = $mmcountscrukey * $replacement_reducationrate;
                        $totalcumulativeamount = $cumulative_pre_amount + $mmamountkey;
                        $updatearr = array(
                            'mm' => $keyval->man_months,
                            'balance_mm' => $keyval->balancemm,
                            'rate' => $keyval->rate,
                            'srno' => $keyval->srno,
                            'replacement_reducationrate' => $keyval->replacement_reducationrate,
                            'cumulativemm' => $cumulative_pre_mm,
                            'cumulativeamount' => $cumulative_pre_amount,
                            'totalcumulativemm' => $totalcumulativemm,
                            'totalcumulativeamount' => $totalcumulativeamount,
                            'modified' => date('Y-m-d')
                        );
                        $res = $this->mastermodel->UpdateRecords('invoicesave', $Where, $updatearr);
                    }
                    //Inser Or Update ash 12-
                }
            }

            if ($subteamdata) {
                foreach ($subteamdata as $subval) {
                    $wheredatachk = array(
                        'project_id' => $_REQUEST['project_ids'],
                        'projectno_id' => $_REQUEST['project_numberids'],
                        'invoiceno_id' => $respreviousdata['invoice_no'],
                        'designation_id' => $subval->designation_id,
                        'emp_id' => $subval->empname
                    );

                    $this->db->select('*');
                    $this->db->from('invoicesave');
                    $this->db->where($wheredatachk);
                    $invoicechk = $this->db->get()->row_array();

                    if (!empty($invoicechk)) {
                        $cumulative_pre_mm = $invoicechk['totalcumulativemm'];
                        $cumulative_pre_amount = $invoicechk['totalcumulativeamount'];
                    } else {
                        //Bug Asheesh 05-02-2019..
                        $cumulative_pre_mm = $subval->cumulative_pre_mm;
                        $cumulative_pre_amount = $subval->cumulative_pre_amount;
                    }
                    $wheredata = array(
                        'project_id' => $_REQUEST['project_ids'],
                        'projectno_id' => $_REQUEST['project_numberids'],
                        'invoiceno_id' => $resinvoicedata['invoice_no'],
                        'designation_id' => $subval->designation_id,
                        'emp_id' => $subval->empname
                    );
                    $this->db->select('totalcumulativemm,totalcumulativeamount');
                    $this->db->from('invoicesave');
                    $this->db->where($wheredata);
                    $invoicecum = $this->db->get()->row_array();
                    if (!empty($invoicecum)) {
                        $totalcumulativemm = $invoicecum['totalcumulativemm'];
                        $totalcumulativeamount = $invoicecum['totalcumulativeamount'];
                    } else {
                        $intermittent = getintermittent($_REQUEST['project_ids'], $subval->empname);
                        if (!empty($intermittent)) {
                            $manmonthscrukey = getintermittentattd($_REQUEST['project_ids'], $subval->empname, $subval->designation_id, $crumonths, $cruyears);
                            $totalnoofdays = 30;
                            if (($manmonthscrukey->present) > 0) {
                                $mmcountscrukey = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                            } else {
                                $mmcountscrukey = 0;
                            }
                        } else {
                            $manmonthscrukey = getuseattd($_REQUEST['project_ids'], $subval->empname, $subval->designation_id, $crumonths, $cruyears);
                            if (($manmonthscrukey->leave + $manmonthscrukey->absent) > 0) {
                                $mmcountscrukey = round((1 - ($manmonthscrukey->leave + $manmonthscrukey->absent) / $divideno_days), $decimalplace);
                                if ($mmcountscrukey < 0) {
                                    $mmcountscrukey = 0;
                                }
                            } else {
                                if (($manmonthscrukey->present) > 0) {
                                    $mmcountscrukey = 1;
                                } else {
                                    $mmcountscrukey = 0;
                                }
                            }
                        }
                        $totalcumulativemm = $cumulative_pre_mm + $mmcountscrukey;
                        if ($subval->replacement_reducationrate > 0) {
                            $replacement_reducationrate = $subval->rate - (($subval->rate * $subval->replacement_reducationrate) / 100);
                        } else {
                            $replacement_reducationrate = $subval->rate;
                        }
                        //$ratered = $subval->rate * $replacement_reducationrate;
                        $mmamountkey = $mmcountscrukey * $replacement_reducationrate;
                        $totalcumulativeamount = $cumulative_pre_amount + $mmamountkey;
                    }
                    $arr = array('info_id' => $_REQUEST['clear_id'],
                        'invoiceno_id' => $_REQUEST['invoice_no_id'],
                        'project_id' => $_REQUEST['project_ids'],
                        'projectno_id' => $_REQUEST['project_numberids'],
                        'designation_id' => $subval->designation_id,
                        'emp_id' => $subval->empname,
                        'srno' => $subval->srno,
                        'key_id' => 2,
                        'date_of_joining' => $subval->date_of_joining,
                        'userfullname' => $subval->userfullname,
                        'designation_name' => $subval->designation_name,
                        'mm' => $subval->man_months,
                        'balance_mm' => $subval->balancemm,
                        'rate' => $subval->rate,
                        'replacement_reducationrate' => $subval->replacement_reducationrate,
                        'cumulativemm' => $cumulative_pre_mm,
                        'cumulativeamount' => $cumulative_pre_amount,
                        'totalcumulativemm' => $totalcumulativemm,
                        'totalcumulativeamount' => $totalcumulativeamount,
                        'entryby' => $this->session->userdata('uid'),
                        'created' => date('Y-m-d')
                    );
                    $Where = array('info_id' => $_REQUEST['clear_id'],
                        'invoiceno_id' => $_REQUEST['invoice_no_id'],
                        'project_id' => $_REQUEST['project_ids'],
                        'projectno_id' => $_REQUEST['project_numberids'],
                        'designation_id' => $subval->designation_id,
                        'emp_id' => $subval->empname,
                        'key_id' => 2
                    );
                    $data = $this->mastermodel->count_rows('invoicesave', $Where);
                    if ($data < 1) {
                        $res = $this->mastermodel->InsertMasterData($arr, 'invoicesave');
                    } else {
                        //10 Jan 2018 Code For 2nd Tab... Code By Asheesh
                        $intermittent = getintermittent($_REQUEST['project_ids'], $subval->empname);
                        if (!empty($intermittent)) {
                            $manmonthscrukey = getintermittentattd($_REQUEST['project_ids'], $subval->empname, $subval->designation_id, $crumonths, $cruyears);
                            $totalnoofdays = 30;
                            if (($manmonthscrukey->present) > 0) {
                                $mmcountscrukey = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                            } else {
                                $mmcountscrukey = 0;
                            }
                        } else {
                            $manmonthscrukey = getuseattd($_REQUEST['project_ids'], $subval->empname, $subval->designation_id, $crumonths, $cruyears);
                            if (($manmonthscrukey->leave + $manmonthscrukey->absent) > 0) {
                                $mmcountscrukey = round((1 - ($manmonthscrukey->leave + $manmonthscrukey->absent) / $divideno_days), $decimalplace);
                                if ($mmcountscrukey < 0) {
                                    $mmcountscrukey = 0;
                                }
                            } else {
                                if (($manmonthscrukey->present) > 0) {
                                    $mmcountscrukey = 1;
                                } else {
                                    $mmcountscrukey = 0;
                                }
                            }
                        }
                        $totalcumulativemm = $cumulative_pre_mm + $mmcountscrukey;
                        if ($subval->replacement_reducationrate > 0) {
                            $replacement_reducationrate = $subval->rate - (($subval->rate * $subval->replacement_reducationrate) / 100);
                        } else {
                            $replacement_reducationrate = $subval->rate;
                        }
                        $mmamountkey = $mmcountscrukey * $replacement_reducationrate;
                        $totalcumulativeamount = $cumulative_pre_amount + $mmamountkey;
                        $updatearr = array(
                            'mm' => $subval->man_months,
                            'balance_mm' => $subval->balancemm,
                            'rate' => $subval->rate,
                            'srno' => $subval->srno,
                            'replacement_reducationrate' => $subval->replacement_reducationrate,
                            'cumulativemm' => $cumulative_pre_mm,
                            'cumulativeamount' => $cumulative_pre_amount,
                            'totalcumulativemm' => $totalcumulativemm,
                            'totalcumulativeamount' => $totalcumulativeamount,
                            'modified' => date('Y-m-d')
                        );
                        $res = $this->mastermodel->UpdateRecords('invoicesave', $Where, $updatearr);
                    }
                }
            }

            if ($adminteamdata) {
                foreach ($adminteamdata as $adminval) {
                    $wheredatachk = array(
                        'project_id' => $_REQUEST['project_ids'],
                        'projectno_id' => $_REQUEST['project_numberids'],
                        'invoiceno_id' => $respreviousdata['invoice_no'],
                        'designation_id' => $adminval->designation_id,
                        'emp_id' => $adminval->empname
                    );
                    $this->db->select('*');
                    $this->db->from('invoicesave');
                    $this->db->where($wheredatachk);
                    $invoicechk = $this->db->get()->row_array();
                    if (!empty($invoicechk)) {
                        $cumulative_pre_mm = $invoicechk['totalcumulativemm'];
                        $cumulative_pre_amount = $invoicechk['totalcumulativeamount'];
                    } else {
                        $cumulative_pre_mm = $adminval->cumulative_pre_mm;
                        $cumulative_pre_amount = $adminval->cumulative_pre_amount;
                    }
                    $wheredata = array(
                        'project_id' => $_REQUEST['project_ids'],
                        'projectno_id' => $_REQUEST['project_numberids'],
                        'invoiceno_id' => $resinvoicedata['invoice_no'],
                        'designation_id' => $adminval->designation_id,
                        'emp_id' => $adminval->empname
                    );
                    $this->db->select('totalcumulativemm,totalcumulativeamount');
                    $this->db->from('invoicesave');
                    $this->db->where($wheredata);
                    $invoicecum = $this->db->get()->row_array();

                    if (!empty($invoicecum)) {
                        $totalcumulativemm = $invoicecum['totalcumulativemm'];
                        $totalcumulativeamount = $invoicecum['totalcumulativeamount'];
                    } else {
                        $intermittent = getintermittent($_REQUEST['project_ids'], $adminval->empname);
                        if (!empty($intermittent)) {
                            $manmonthscrukey = getintermittentattd($_REQUEST['project_ids'], $adminval->empname, $adminval->designation_id, $crumonths, $cruyears);
                            //$totalnoofdays = cal_days_in_month(CAL_GREGORIAN,$crumonths,$cruyears);
                            $totalnoofdays = 30;
                            if (($manmonthscrukey->present) > 0) {
                                //$mmcountscrukey = round((1- ($totalnoofdays - $manmonthscrukey->present) / 30),$decimalplace);
                                $mmcountscrukey = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                            } else {
                                $mmcountscrukey = 0;
                            }
                        } else {
                            $manmonthscrukey = getuseattd($_REQUEST['project_ids'], $adminval->empname, $adminval->designation_id, $crumonths, $cruyears);
                            if (($manmonthscrukey->leave + $manmonthscrukey->absent) > 0) {
                                $mmcountscrukey = round((1 - ($manmonthscrukey->leave + $manmonthscrukey->absent) / $divideno_days), $decimalplace);
                                if ($mmcountscrukey < 0) {
                                    $mmcountscrukey = 0;
                                }
                            } else {
                                if (($manmonthscrukey->present) > 0) {
                                    $mmcountscrukey = 1;
                                } else {
                                    $mmcountscrukey = 0;
                                }
                            }
                        }
                        $totalcumulativemm = $cumulative_pre_mm + $mmcountscrukey;
                        if ($adminval->replacement_reducationrate > 0) {
                            $replacement_reducationrate = $adminval->rate - (($adminval->rate * $adminval->replacement_reducationrate) / 100);
                        } else {
                            $replacement_reducationrate = $adminval->rate;
                        }
                        //$ratered = $adminval->rate * $replacement_reducationrate;
                        $mmamountkey = $mmcountscrukey * $replacement_reducationrate;
                        $totalcumulativeamount = $cumulative_pre_amount + $mmamountkey;
                    }

                    $arr = array('info_id' => $_REQUEST['clear_id'],
                        'invoiceno_id' => $_REQUEST['invoice_no_id'],
                        'project_id' => $_REQUEST['project_ids'],
                        'projectno_id' => $_REQUEST['project_numberids'],
                        'designation_id' => $adminval->designation_id,
                        'emp_id' => $adminval->empname,
                        'srno' => $adminval->srno,
                        'key_id' => 3,
                        'date_of_joining' => $adminval->date_of_joining,
                        'userfullname' => $adminval->userfullname,
                        'designation_name' => $adminval->designation_name,
                        'mm' => $adminval->man_months,
                        'balance_mm' => $adminval->balancemm,
                        'rate' => $adminval->rate,
                        'replacement_reducationrate' => $adminval->replacement_reducationrate,
                        'cumulativemm' => $cumulative_pre_mm,
                        'cumulativeamount' => $cumulative_pre_amount,
                        'totalcumulativemm' => $totalcumulativemm,
                        'totalcumulativeamount' => $totalcumulativeamount,
                        'entryby' => $this->session->userdata('uid'),
                        'created' => date('Y-m-d')
                    );

                    $Where = array(
                        'info_id' => $_REQUEST['clear_id'],
                        'invoiceno_id' => $_REQUEST['invoice_no_id'],
                        'project_id' => $_REQUEST['project_ids'],
                        'projectno_id' => $_REQUEST['project_numberids'],
                        'designation_id' => $adminval->designation_id,
                        'emp_id' => $adminval->empname,
                        'key_id' => 3
                    );
                    $data = $this->mastermodel->count_rows('invoicesave', $Where);

                    if ($data < 1) {
                        $res = $this->mastermodel->InsertMasterData($arr, 'invoicesave');
                    } else {
                        //Code For 3nd Tab... 10 Jan Code By Asheesh..
                        $intermittent = getintermittent($_REQUEST['project_ids'], $adminval->empname);
                        if (!empty($intermittent)) {
                            $manmonthscrukey = getintermittentattd($_REQUEST['project_ids'], $adminval->empname, $adminval->designation_id, $crumonths, $cruyears);
                            $totalnoofdays = 30;
                            if (($manmonthscrukey->present) > 0) {
                                $mmcountscrukey = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                            } else {
                                $mmcountscrukey = 0;
                            }
                        } else {
                            $manmonthscrukey = getuseattd($_REQUEST['project_ids'], $adminval->empname, $adminval->designation_id, $crumonths, $cruyears);
                            if (($manmonthscrukey->leave + $manmonthscrukey->absent) > 0) {
                                $mmcountscrukey = round((1 - ($manmonthscrukey->leave + $manmonthscrukey->absent) / $divideno_days), $decimalplace);
                                if ($mmcountscrukey < 0) {
                                    $mmcountscrukey = 0;
                                }
                            } else {
                                if (($manmonthscrukey->present) > 0) {
                                    $mmcountscrukey = 1;
                                } else {
                                    $mmcountscrukey = 0;
                                }
                            }
                        }
                        $totalcumulativemm = $cumulative_pre_mm + $mmcountscrukey;
                        if ($adminval->replacement_reducationrate > 0) {
                            $replacement_reducationrate = $adminval->rate - (($adminval->rate * $adminval->replacement_reducationrate) / 100);
                        } else {
                            $replacement_reducationrate = $adminval->rate;
                        }
                        $mmamountkey = $mmcountscrukey * $replacement_reducationrate;
                        $totalcumulativeamount = $cumulative_pre_amount + $mmamountkey;

                        $updatearr = array(
                            'mm' => $adminval->man_months,
                            'balance_mm' => $adminval->balancemm,
                            'rate' => $adminval->rate,
                            'srno' => $adminval->srno,
                            'replacement_reducationrate' => $adminval->replacement_reducationrate,
                            'cumulativemm' => $cumulative_pre_mm,
                            'cumulativeamount' => $cumulative_pre_amount,
                            'totalcumulativemm' => $totalcumulativemm,
                            'totalcumulativeamount' => $totalcumulativeamount,
                            'modified' => date('Y-m-d')
                        );
                        $res = $this->mastermodel->UpdateRecords('invoicesave', $Where, $updatearr);
                    }
                }
            }

            $eot = $this->mastermodel->geteotdetailLastID($_REQUEST['project_ids']);
            $geteotemployeekey = $this->Front_model->geteotemployee($_REQUEST['project_ids'], $eot->id, 1);
            $geteotemployeesub = $this->Front_model->geteotemployee($_REQUEST['project_ids'], $eot->id, 2);
            $geteotemployeestaff = $this->Front_model->geteotemployee($_REQUEST['project_ids'], $eot->id, 3);
            //echo '<pre>'; print_r($geteotemployeekey); die;
            if ($geteotemployeekey) {
                foreach ($geteotemployeekey as $eotkeyval) {
                    $arr = array(
                        'eot_id' => $eotkeyval->id,
                        'eot_mm' => $eotkeyval->eot_mm
                    );
                    $Where = array('info_id' => $_REQUEST['clear_id'],
                        'invoiceno_id' => $_REQUEST['invoice_no_id'],
                        'project_id' => $_REQUEST['project_ids'],
                        'projectno_id' => $_REQUEST['project_numberids'],
                        'designation_id' => $eotkeyval->designation_id,
                        'emp_id' => $eotkeyval->emp_id,
                        'key_id' => 1);
                    $data = $this->mastermodel->count_rows('invoicesave', $Where);

                    if ($data < 1) {
                        $res = $this->mastermodel->InsertMasterData($arr, 'invoicesave');
                    } else {
                        $updatearr = array(
                            'eot_id' => $eotkeyval->id,
                            'eot_mm' => $eotkeyval->eot_mm
                        );
                        $res = $this->mastermodel->UpdateRecords('invoicesave', $Where, $updatearr);
                    }
                }
            }

            if ($geteotemployeesub) {
                foreach ($geteotemployeesub as $eotsubval) {
                    $arr = array(
                        'eot_id' => $eotsubval->id,
                        'eot_mm' => $eotsubval->eot_mm
                    );

                    $Where = array(
                        'info_id' => $_REQUEST['clear_id'],
                        'invoiceno_id' => $_REQUEST['invoice_no_id'],
                        'project_id' => $_REQUEST['project_ids'],
                        'projectno_id' => $_REQUEST['project_numberids'],
                        'designation_id' => $eotsubval->designation_id,
                        'emp_id' => $eotsubval->emp_id,
                        'key_id' => 2
                    );
                    $data = $this->mastermodel->count_rows('invoicesave', $Where);

                    if ($data < 1) {
                        $res = $this->mastermodel->InsertMasterData($arr, 'invoicesave');
                    } else {
                        $updatearr = array(
                            'eot_id' => $eotsubval->id,
                            'eot_mm' => $eotsubval->eot_mm
                        );
                        $res = $this->mastermodel->UpdateRecords('invoicesave', $Where, $updatearr);
                    }
                }
            }

            if ($geteotemployeestaff) {
                foreach ($geteotemployeestaff as $eotadminval) {
                    $arr = array(
                        'eot_id' => $eotadminval->id,
                        'eot_mm' => $eotadminval->eot_mm
                    );
                    $Where = array(
                        'info_id' => $_REQUEST['clear_id'],
                        'invoiceno_id' => $_REQUEST['invoice_no_id'],
                        'project_id' => $_REQUEST['project_ids'],
                        'projectno_id' => $_REQUEST['project_numberids'],
                        'designation_id' => $eotadminval->designation_id,
                        'emp_id' => $eotadminval->emp_id,
                        'key_id' => 3
                    );
                    $data = $this->mastermodel->count_rows('invoicesave', $Where);
                    if ($data < 1) {
                        $res = $this->mastermodel->InsertMasterData($arr, 'invoicesave');
                    } else {
                        $updatearr = array(
                            'eot_id' => $eotadminval->id,
                            'eot_mm' => $eotadminval->eot_mm
                        );
                        $res = $this->mastermodel->UpdateRecords('invoicesave', $Where, $updatearr);
                    }
                }
            }
        }

        if ($_REQUEST['finish'] == 'Finish') {
            if ($_REQUEST['clear_id']) {
                $data = array('invoice_no' => NULL);
                $Where = array('id' => $_REQUEST['clear_id']);
                $clear_id = $_REQUEST['clear_id'];
                $invoice_no_id = $_REQUEST['invoice_no_id'];
                $project_numberids = $_REQUEST['project_numberids'];
                $project_ids = $_REQUEST['project_ids'];
                // $retResp = $this->getInvoiceMonthlyEntry_detail($clear_id, $invoice_no_id, $project_numberids, $project_ids);
                $this->mastermodel->UpdateRecords('accountinfo', $Where, $data);
            }
        }
        redirect(base_url('account/viewaccountinfo'));
    }

//script for update and insert the JV/Lead/Assoc(20-09-2019)INTO 'jv_cegexp' Table
    // public function JvLeadAssocScript() {
    // $this->db->select('cegexpid,project_id,base_comp_id,lead_comp_id,joint_venture,asso_comp');
    // $this->db->from('jv_cegexp');
    // $this->db->where('status', '1');
    // $result = $this->db->get()->result();
    //echo '<pre>';
    //print_r($result);
    //die;
    // if($result) {
    // foreach ($result as $jvleadassoc) {
    // $cegecpid = $jvleadassoc->cegexpid;
    // $proj_id = $jvleadassoc->project_id;
    // $lead_id = $jvleadassoc->lead_comp_id;
    // $jv_id = $jvleadassoc->joint_venture;
    // $assoc_id = $jvleadassoc->asso_comp;
    // $updateArr = array(
    // 'lead_companes' => $jvleadassoc->lead_comp_id,
    // 'joint_ventures' => $jvleadassoc->joint_venture,
    // 'associate_company' => $jvleadassoc->asso_comp,
    // 'cegexp_id' => $jvleadassoc->cegexpid,
    // 'entry_by' => $this->session->userdata('uid')
    // ); 
    // $InsertArr = array(
    // 'lead_companes' => $jvleadassoc->lead_comp_id,
    // 'joint_ventures' => $jvleadassoc->joint_venture,
    // 'associate_company' => $jvleadassoc->asso_comp,
    // 'project_id' => $jvleadassoc->project_id,
    // 'cegexp_id' => $jvleadassoc->cegexpid,
    // 'entry_by' => $this->session->userdata('uid')
    // ); 
    // $whereArr = array('status' => '1', 'project_id' => $proj_id);        
    // if($proj_id AND $cegecpid AND $lead_id AND $jv_id AND $assoc_id):
    // $upaterowArr = $this->mastermodel->UpdateRecords('jv_companyrecords', $whereArr, $updateArr);
    // else :
    // $insertrowArr = $this->mastermodel->InsertMasterData($InsertArr, 'jv_companyrecords');
    // endif;
    // }
    // }
    // }	
    //Script For Privious Invoice Entry INTO monthwise_invc_saveupd_afterlock...
    public function invoicescript() {

        $this->db->select('a.*');
        $this->db->from('invoicedetail as a');
        $this->db->where("a.invoice_no >", '0');
        $this->db->where("a.ra_invoice >", '0');
        // $this->db->where("a.project_numberid", '127');
        $this->db->order_by("a.id", 'ASC');
        $RecData = $this->db->get()->result();

        if ($RecData) {
            foreach ($RecData as $recD) {
                $clear_id = $recD->infoid;
                $invoice_no_id = $recD->invoice_no;
                $project_numberids = $recD->project_numberid;
                $project_ids = $this->Front_model->getprojectid_tstobd($project_numberids);

                echo $project_ids . "==>" . $recD->id . "<br>";

                if ($clear_id and $invoice_no_id and $project_numberids and $recD->invoice_name):
                    $respp = $this->getInvoiceMonthlyEntry_detail($clear_id, $invoice_no_id, $project_numberids, $project_ids);
                    echo $project_ids . "==>" . $respp . "<br>";
                endif;
            }
        }
    }

//    Get Invoice Monthly Entry..
    // public function getInvoiceMonthlyEntry_detail($clear_id, $invoice_no_id, $project_numberids, $project_ids) {
    //$clear_id = 23;
    //$invoice_no_id = 44;
    //$project_numberids = 127;
    //project_ids = 89167;
    //Extra Code By Asheesh..
    // $this->db->select('a.ra_invoice,a.invoice_date,b.decimal_place,b.no_days');
    // $this->db->from('invoicedetail as a');
    // $this->db->join("accountinfo as b", "b.project_numberid=a.project_numberid", "LEFT");
    // $this->db->where(array("a.project_numberid" => $project_numberids, "a.invoice_no" => $invoice_no_id, "a.infoid" => $clear_id));
    // $invoice = $this->db->get()->result();
    // $currentdate = date('Y-m-d', strtotime($invoice[0]->invoice_date . "-1 month"));
    // $cruyears = date('Y', strtotime($currentdate));
    // $crumonths = date('n', strtotime($currentdate));
    // $getcrukey = getcurrentmonth($project_ids, 1);
    // $getcrusub = getcurrentmonth($project_ids, 2);
    // $getcrustaff = getcurrentmonth($project_ids, 3);
    //Set Defaoult Decimal Place.
    // $decimalplace = $invoice[0]->decimal_place;
    // if ($decimalplace == "") {
    // $decimalplace = 15;
    // }
    // Set Default Days.
    // if ($invoice->no_days) {
    // $totalnoofdays = $invoice[0]->no_days;
    // } else {
    // $totalnoofdays = 30;
    // }
    // 1 Key Professional..
    // if ($getcrukey) {
    // foreach ($getcrukey as $vals1) {
    // $mmcountscrukey = 0;
    // $intermittent = getintermittent($project_ids, $vals1->empname);
    // if (!empty($intermittent)) {
    // $manmonthscrukey = getintermittentattd($project_ids, $vals1->empname, $vals1->designation_id, $crumonths, $cruyears);
    // if (($manmonthscrukey->present) > 0) {
    // $mmcountscrukey = round($manmonthscrukey->present / $totalnoofdays, $decimalplace);
    // } else {
    // $mmcountscrukey = 0;
    // }
    // } else {
    // $manmonthscrukey = getuseattd($project_ids, $vals1->empname, $vals1->designation_id, $crumonths, $cruyears);
    // if (($manmonthscrukey->leave + $manmonthscrukey->absent) > 0) {
    // $mmcountscrukey = (1 - ($manmonthscrukey->leave + $manmonthscrukey->absent) / $totalnoofdays);
    // if ($mmcountscrukey < 0) {
    // $mmcountscrukey = 0;
    // }
    // } else {
    // if (($manmonthscrukey->present) > 0) {
    // $mmcountscrukey = 1;
    // } else {
    // $mmcountscrukey = 0;
    // }
    // }
    // }
    // if ($vals1->rate > 0) {
    // $replacement_reducationrate = $vals1->rate - (($vals1->rate * $vals1->replacement_reducationrate) / 100);
    // } else {
    // $replacement_reducationrate = $vals1->rate;
    // }
    // $mmamountkey = $mmcountscrukey * $replacement_reducationrate;
    //  Get Balancde MM Code By Asheesh..
    //Next 4 Balance..
    // $balancekey = $vals1->man_months - ($vals1->cumulative_pre_mm + $mmcountscrukey);
    //Total MM
    // $totalmmkey = $vals1->man_months;
############# insert Update Query.. ##################
    // $InvcInsertArr1 = array("project_id" => $project_ids,
    // "project_numberid" => $project_numberids,
    // "invoice_no_id" => $invoice_no_id,
    // "ra_invoice_no" => $invoice[0]->ra_invoice,
    // "year" => $cruyears,
    // "month" => $crumonths,
    // "empl_id" => $vals1->empname,
    // "designation_id" => $vals1->designation_id,
    // "key_type" => "1",
    // "billed_mm" => ($mmcountscrukey) ? $mmcountscrukey : '',
    // "billed_amount" => ($mmamountkey) ? ($mmamountkey) : '',
    // "balance_mm" => ($balancekey) ? $balancekey : '',
    // "total_mm" => ($totalmmkey) ? $totalmmkey : '',
    // "entry_by" => $this->session->userdata('uid'));
    // $UpDdataArr1 = array("billed_mm" => ($mmcountscrukey) ? $mmcountscrukey : '',
    // "billed_amount" => ($mmamountkey) ? ($mmamountkey) : '',
    // "balance_mm" => ($balancekey) ? round($balancekey, 2) : '',
    // "total_mm" => ($totalmmkey) ? round($totalmmkey, 2) : '',
    // "entry_by" => $this->session->userdata('uid'));
    // $WhereArr1 = array("project_id" => $project_ids,
    // "project_numberid" => $project_numberids,
    // "ra_invoice_no" => $invoice[0]->ra_invoice,
    // "year" => $cruyears,
    // "month" => $crumonths,
    // "empl_id" => $vals1->empname,
    // "designation_id" => $vals1->designation_id,
    // "key_type" => "1");
    // Check Exist or Not..
    // $this->db->select('b.fld_id');
    // $this->db->from('monthwise_invc_saveupd_afterlock as b');
    // $this->db->where(array("b.project_id" => $project_ids, "b.project_numberid" => $project_numberids, "b.ra_invoice_no" => $invoice[0]->ra_invoice, "b.year" => $cruyears, "b.month" => $crumonths, "b.empl_id" => $vals1->empname, "b.designation_id" => $vals1->designation_id, "b.key_type" => "1"));
    // $invc_SummDetail = $this->db->get()->row();
    // if ($invc_SummDetail->fld_id):
    //  Update Query..
    // $this->mastermodel->UpdateRecords('monthwise_invc_saveupd_afterlock', $WhereArr1, $UpDdataArr1);
    // else:
    //  Insert Query..
    // $this->mastermodel->InsertMasterData($InvcInsertArr1, 'monthwise_invc_saveupd_afterlock');
    // endif;
    // }
    // }
    // ################# Sub Professional ##########################
    // if ($getcrusub) {
    // foreach ($getcrusub as $vals2) {
    // $mmcountscrusub = 0;
    // $intermittent = getintermittent($project_ids, $vals2->empname);
    // if (!empty($intermittent)) {
    // $manmonthscrusub = getintermittentattd($project_ids, $vals2->empname, $vals2->designation_id, $crumonths, $cruyears);
    // if (($manmonthscrusub->present) > 0) {
    // $mmcountscrusub = round($manmonthscrusub->present / $totalnoofdays, $decimalplace);
    // } else {
    // $mmcountscrusub = 0;
    // }
    // } else {
    // $manmonthscrusub = getuseattd($project_ids, $vals2->empname, $vals2->designation_id, $crumonths, $cruyears);
    // if (($manmonthscrusub->leave + $manmonthscrusub->absent) > 0) {
    // $mmcountscrusub = (1 - ($manmonthscrusub->leave + $manmonthscrusub->absent) / $totalnoofdays);
    // if ($mmcountscrusub < 0) {
    // $mmcountscrusub = 0;
    // }
    // } else {
    // if (($manmonthscrusub->present) > 0) {
    // $mmcountscrusub = 1;
    // } else {
    // $mmcountscrusub = 0;
    // }
    // }
    // }
    // if ($vals2->rate > 0) {
    // $replacement_reducationrate = $vals2->rate - (($vals2->rate * $vals2->replacement_reducationrate) / 100);
    // } else {
    // $replacement_reducationrate = $vals2->rate;
    // }
    // $mmamountsub = $mmcountscrusub * $replacement_reducationrate;
    //  Get Balancde MM Code By Asheesh..
    //Next 4 Balance..
    // $balancesub = $vals2->man_months - ($vals2->cumulative_pre_mm + $mmcountscrusub);
    //Total MM
    // $totalmmsub = $vals2->man_months;
    // $InvcInsertArr2 = array("project_id" => $project_ids,
    // "project_numberid" => $project_numberids,
    // "invoice_no_id" => $invoice_no_id,
    // "ra_invoice_no" => $invoice[0]->ra_invoice,
    // "year" => $cruyears,
    // "month" => $crumonths,
    // "empl_id" => $vals2->empname,
    // "designation_id" => $vals2->designation_id,
    // "key_type" => "2",
    // "billed_mm" => ($mmcountscrusub) ? $mmcountscrusub : '0',
    // "billed_amount" => ($mmamountsub) ? ($mmamountsub) : '',
    // "balance_mm" => ($balancesub) ? $balancesub : '',
    // "total_mm" => ($totalmmsub) ? $totalmmsub : '',
    // "entry_by" => $this->session->userdata('uid'));
    // $UpDdataArr2 = array("billed_mm" => ($mmcountscrusub) ? $mmcountscrusub : '',
    // "billed_amount" => ($mmamountsub) ? ($mmamountsub) : '',
    // "balance_mm" => ($balancesub) ? round($balancesub, 2) : '',
    // "total_mm" => ($totalmmsub) ? round($totalmmsub, 2) : '',
    // "entry_by" => $this->session->userdata('uid'));
    // $WhereArr2 = array("project_id" => $project_ids,
    // "project_numberid" => $project_numberids,
    // "ra_invoice_no" => $invoice[0]->ra_invoice,
    // "year" => $cruyears,
    // "month" => $crumonths,
    // "empl_id" => $vals2->empname,
    // "designation_id" => $vals2->designation_id,
    // "key_type" => "2");
    // Check Exist or Not..
    // $this->db->select('b.fld_id');
    // $this->db->from('monthwise_invc_saveupd_afterlock as b');
    // $this->db->where(array("b.project_id" => $project_ids, "b.project_numberid" => $project_numberids, "b.ra_invoice_no" => $invoice[0]->ra_invoice, "b.year" => $cruyears, "b.month" => $crumonths, "b.empl_id" => $vals2->empname, "b.designation_id" => $vals2->designation_id, "b.key_type" => "2"));
    // $invc_SummDetail = $this->db->get()->row();
    // if ($invc_SummDetail->fld_id):
    // Update Query..
    // $this->mastermodel->UpdateRecords('monthwise_invc_saveupd_afterlock', $WhereArr2, $UpDdataArr2);
    // else:
    //Insert Query..
    // $this->mastermodel->InsertMasterData($InvcInsertArr2, 'monthwise_invc_saveupd_afterlock');
    // endif;
    // }
    // }
    // ################# Admin Staff Professional ##########################
    // if ($getcrustaff) {
    // foreach ($getcrustaff as $vals3) {
    // $mmcountscruadmin = 0;
    // $intermittent = getintermittent($project_ids, $vals3->empname);
    // if (!empty($intermittent)) {
    // $manmonthscruadmin = getintermittentattd($project_ids, $vals3->empname, $vals3->designation_id, $crumonths, $cruyears);
    // if (($manmonthscruadmin->present) > 0) {
    // $mmcountscruadmin = round($manmonthscruadmin->present / $totalnoofdays, $decimalplace);
    // } else {
    // $mmcountscruadmin = 0;
    // }
    // } else {
    // $manmonthscruadmin = getuseattd($project_ids, $vals3->empname, $vals3->designation_id, $crumonths, $cruyears);
    // if (($manmonthscruadmin->leave + $manmonthscruadmin->absent) > 0) {
    // $mmcountscruadmin = (1 - ($manmonthscruadmin->leave + $manmonthscruadmin->absent) / $totalnoofdays);
    // if ($mmcountscruadmin < 0) {
    // $mmcountscruadmin = 0;
    // }
    // } else {
    // if (($manmonthscruadmin->present) > 0) {
    // $mmcountscruadmin = 1;
    // } else {
    // $mmcountscruadmin = 0;
    // }
    // }
    // }
    // if ($vals3->rate > 0) {
    // $replacement_reducationrate = $vals3->rate - (($vals3->rate * $vals3->replacement_reducationrate) / 100);
    // } else {
    // $replacement_reducationrate = $vals3->rate;
    // }
    // $mmamountadmin = $mmcountscruadmin * $replacement_reducationrate;
    //  Get Balancde MM Code By Asheesh..
    //Next 4 Balance..
    // $balanceadmin = $vals3->man_months - ($vals3->cumulative_pre_mm + $mmcountscruadmin);
    // Total MM
    // $totalmmadmin = $vals3->man_months;
    // $InvcInsertArr3 = array("project_id" => $project_ids,
    // "project_numberid" => $project_numberids,
    // "invoice_no_id" => $invoice_no_id,
    // "ra_invoice_no" => $invoice[0]->ra_invoice,
    // "year" => $cruyears,
    // "month" => $crumonths,
    // "empl_id" => $vals3->empname,
    // "designation_id" => $vals3->designation_id,
    // "key_type" => "3",
    // "billed_mm" => ($mmcountscruadmin) ? $mmcountscruadmin : '0',
    // "billed_amount" => ($mmamountadmin) ? ($mmamountadmin) : '',
    // "balance_mm" => ($balanceadmin) ? $balanceadmin : '',
    // "total_mm" => ($totalmmadmin) ? $totalmmadmin : '',
    // "entry_by" => $this->session->userdata('uid'));
    // $UpDdataArr3 = array("billed_mm" => ($mmcountscruadmin) ? $mmcountscruadmin : '',
    // "billed_amount" => ($mmamountadmin) ? ($mmamountadmin) : '',
    // "balance_mm" => ($balanceadmin) ? round($balanceadmin, 2) : '',
    // "total_mm" => ($totalmmadmin) ? round($totalmmadmin, 2) : '',
    // "entry_by" => $this->session->userdata('uid'));
    // $WhereArr3 = array("project_id" => $project_ids,
    // "project_numberid" => $project_numberids,
    // "ra_invoice_no" => $invoice[0]->ra_invoice,
    // "year" => $cruyears,
    // "month" => $crumonths,
    // "empl_id" => $vals3->empname,
    // "designation_id" => $vals3->designation_id,
    // "key_type" => "3");
    //Check Exist or Not..
    // $this->db->select('b.fld_id');
    // $this->db->from('monthwise_invc_saveupd_afterlock as b');
    // $this->db->where(array("b.project_id" => $project_ids, "b.project_numberid" => $project_numberids, "b.ra_invoice_no" => $invoice[0]->ra_invoice, "b.year" => $cruyears, "b.month" => $crumonths, "b.empl_id" => $vals3->empname, "b.designation_id" => $vals3->designation_id, "b.key_type" => "3"));
    // $invc_SummDetail = $this->db->get()->row();
    // if ($invc_SummDetail->fld_id):
    //Update Query..
    // $this->mastermodel->UpdateRecords('monthwise_invc_saveupd_afterlock', $WhereArr3, $UpDdataArr3);
    // else:
    // Insert Query..
    // $this->mastermodel->InsertMasterData($InvcInsertArr3, 'monthwise_invc_saveupd_afterlock');
    // endif;
    // }
    // }
    // return true;
    // }


    public function monthlycrn() {
        $this->db->select('a.*');
        $this->db->from('invoicedetail as a');
        $this->db->where("a.invoice_no >", '0');
        $this->db->where("a.ra_invoice >", '0');
        $ResultData = $this->db->get()->result();
        if ($ResultData) {
            foreach ($ResultData as $reCd) {

                $projTsNoID = $reCd->project_numberid;
                $invc_noID = $reCd->invoice_no;
                if ($projTsNoID and $invc_noID):
                    $this->livecrownscript($projTsNoID, $invc_noID);
                endif;
                //echo '<pre>';
                //print_r($reCd);
                // echo '<hr>';
            }
        }
    }

    //Test Crown..
    public function livecrownscript($projTsNoID, $invc_noID) {
        $this->db->select('a.*');
        $this->db->from('invoicedetail as a');
        $this->db->where("a.invoice_no >", '0');
        $this->db->where("a.ra_invoice >", '0');
        $this->db->where("a.project_numberid", $projTsNoID);
        $this->db->where("a.invoice_no", $invc_noID);
        $this->db->order_by("a.id", 'ASC');
        $RecData = $this->db->get()->row();

        $invoicedata = $this->Front_model->getaccountdetailforexcel($RecData->id);

        // print_r($RecData).$RecData->id; die;

        $projectname = $this->Front_model->getprojectname($invoicedata->project_id);

        $decimalplace = $invoicedata->decimal_place;
        $no_days = $invoicedata->no_days;
        $invoicedate = date('jS F Y', strtotime($invoicedata->invoice_date));

        $previousdate = date('Y-m-d', strtotime($invoicedata->invoice_date . "-2 month"));
        $preyears = date('Y', strtotime($previousdate));
        $premonths = date('n', strtotime($previousdate));
        $currentdate = date('Y-m-d', strtotime($invoicedata->invoice_date . "-1 month"));
        $currentinvoicedate = date('F Y', strtotime($currentdate));
        $cruyears = date('Y', strtotime($currentdate));
        $crumonths = date('n', strtotime($currentdate));
        $crumonthschk = date('m', strtotime($currentdate));
        $d = cal_days_in_month(CAL_GREGORIAN, $crumonths, $cruyears);
        $startDate = date('Y-m-d', strtotime($cruyears . '-' . $crumonthschk . '-01'));
        $endDate = date('Y-m-d', strtotime($cruyears . '-' . $crumonthschk . "-$d"));
        if (!empty($no_days)) {
            $divideno_days = $no_days;
        } else {
            $divideno_days = cal_days_in_month(CAL_GREGORIAN, $crumonths, $cruyears);
        }
        $holidayArr = $this->getHoliday($startDate, $endDate);
        $taxinvoice = $invoicedata->invoice_name . '/' . $invoicedata->invoiceno;

        $keyteamdata = $this->Front_model->getexceldataproject_withreplc($invoicedata->project_id, $invoicedata->project_numberid, $invoicedata->invoiceno, 1);
        $subteamdata = $this->Front_model->getexceldataproject_withreplc($invoicedata->project_id, $invoicedata->project_numberid, $invoicedata->invoiceno, 2);
        $adminteamdata = $this->Front_model->getexceldataproject_withreplc($invoicedata->project_id, $invoicedata->project_numberid, $invoicedata->invoiceno, 3);
        $keyID = count($keyteamdata);

        $escalation = $this->Front_model->getescalation($invoicedata->project_id, $invoiceID);
        $projtax = $this->Front_model->getprojecttax($invoicedata->project_id, $invoiceID);
        // echo '<pre>'; print_r($escalation); die;

        $keyID = count($keyteamdata);
        $subID = count($subteamdata);
        $adminID = count($adminteamdata);

        //Code For 1 KeyProffesional..
        for ($i = 0; $i < $keyID; $i++) {
            $intermittent = $this->Front_model->getintermittent($invoicedata->project_id, $keyteamdata[$i]->emp_id);
            if (!empty($intermittent)) {
                $manmonthscrukey = $this->Front_model->getintermittentattd($invoicedata->project_id, $keyteamdata[$i]->emp_id, $keyteamdata[$i]->designation_id, $crumonths, $cruyears);
                $totalnoofdays = 30;
                if (($manmonthscrukey->present) > 0) {
                    $mmcountprekey = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                } else {
                    $mmcountprekey = 0;
                }
            } else {
                $timemonth = $this->Front_model->gettimesheetdetail($invoicedata->project_id, $keyteamdata[$i]->designation_id, $keyteamdata[$i]->emp_id, $crumonths, $cruyears);
                if (($timemonth->leave + $timemonth->absent) > 0) {
                    $mmcountprekey = round((1 - ($timemonth->leave + $timemonth->absent) / $divideno_days), $decimalplace);
                    if ($mmcountprekey < 0) {
                        $mmcountprekey = 0;
                    }
                } else {
                    if (($timemonth->present) > 0) {
                        $mmcountprekey = 1;
                    } else {
                        $mmcountprekey = 0;
                    }
                }
            }
            if ($keyteamdata[$i]->rate > 0) {
                $replacement_reducationrate = $keyteamdata[$i]->rate - (($keyteamdata[$i]->rate * $keyteamdata[$i]->replacement_reducationrate) / 100);
            } else {
                $replacement_reducationrate = $keyteamdata[$i]->rate;
            }

            $mmamountkey = $mmcountprekey * $replacement_reducationrate;
            $amount = $keyteamdata[$i]->rate * $keyteamdata[$i]->mm;
            $designation_name = $keyteamdata[$i]->designation;
            $userfullname = $keyteamdata[$i]->username;
            $rate = $keyteamdata[$i]->rate;
            $man_months = $keyteamdata[$i]->mm;
            $eot_mm = $keyteamdata[$i]->eot_mm;
            $rcv_mm = $keyteamdata[$i]->eot_mm + $man_months;
            $eotamount = $keyteamdata[$i]->eot_mm * $keyteamdata[$i]->rate;
            $rcvamount = $rcv_mm * $keyteamdata[$i]->rate;
            $cumulativemm = $keyteamdata[$i]->cumulativemm;
            $Ddesignationid = $keyteamdata[$i]->designation_id;
            if ($cumulativemm < 0) {
                $cumulativemm = 0;
            }
            $cumulativeamount = $keyteamdata[$i]->cumulativeamount;
            if ($cumulativeamount < 0) {
                $cumulativeamount = 0;
            }
            $totalcumulativemm = $cumulativemm + $mmcountprekey;
            $totalcumulativeamount = $mmamountkey + $cumulativeamount;
            $keybalancemm = $man_months + $eot_mm - $totalcumulativemm;
            $keybalanceamount = $amount + $eotamount - $totalcumulativeamount;
            $balanceamt += $keybalanceamount;
            //############# insert Update Query.. ##################
            $InvcInsertArr1 = array("project_id" => $invoicedata->project_id,
                "project_numberid" => $invoicedata->project_numberid,
                "invoice_no_id" => $invoicedata->invoiceno,
                "ra_invoice_no" => $invoicedata->ra_invoice,
                "year" => $cruyears,
                "month" => $crumonths,
                "empl_id" => $keyteamdata[$i]->emp_id,
                "designation_id" => $keyteamdata[$i]->designation_id,
                "key_type" => "1",
                "billed_mm" => ($mmcountprekey) ? $mmcountprekey : '',
                "billed_amount" => ($mmamountkey) ? ($mmamountkey) : '',
                "balance_mm" => ($keybalancemm) ? $keybalancemm : '',
                "total_mm" => ($man_months) ? $man_months : '',
                "balance_amount" => ($keybalanceamount) ? $keybalanceamount : "",
                "entry_by" => $this->session->userdata('uid'));

            // echo '<pre>'; print_r($InvcInsertArr1); die;


            $UpDdataArr1 = array("billed_mm" => ($mmcountscrukey) ? $mmcountscrukey : '',
                "billed_amount" => ($mmamountkey) ? ($mmamountkey) : '',
                "balance_mm" => ($balancekey) ? round($balancekey, 2) : '',
                "total_mm" => ($totalmmkey) ? round($totalmmkey, 2) : '',
                "balance_amount" => ($keybalanceamount) ? $keybalanceamount : "",
                "entry_by" => $this->session->userdata('uid'));
            $WhereArr1 = array("project_id" => $invoicedata->project_id,
                "project_numberid" => $invoicedata->project_numberid,
                "ra_invoice_no" => $invoicedata->ra_invoice,
                "year" => $cruyears,
                "month" => $crumonths,
                "empl_id" => $keyteamdata[$i]->emp_id,
                "designation_id" => $keyteamdata[$i]->designation_id,
                "key_type" => "1");
            //Check Exist or Not..
            $this->db->select('b.fld_id');
            $this->db->from('monthwise_invc_saveupd_afterlock as b');
            $this->db->where(array("b.project_id" => $invoicedata->project_id, "b.project_numberid" => $invoicedata->project_numberid, "b.ra_invoice_no" => $invoicedata->ra_invoice, "b.year" => $cruyears, "b.month" => $crumonths, "b.empl_id" => $keyteamdata[$i]->emp_id, "b.designation_id" => $keyteamdata[$i]->designation_id, "b.key_type" => "1"));
            $invc_SummDetail = $this->db->get()->row();
            if ($invc_SummDetail->fld_id):
                //Update Query..
                $this->mastermodel->UpdateRecords('monthwise_invc_saveupd_afterlock', $WhereArr1, $UpDdataArr1);
            else:
                //Insert Query..
                if ($keyteamdata[$i]->emp_id):
                    $this->mastermodel->InsertMasterData($InvcInsertArr1, 'monthwise_invc_saveupd_afterlock');
                endif;
            endif;

            echo $userfullname . "<br>";
        }


        //Code For 2 SubProffesional..
        for ($i = 0; $i < $subID; $i++) {
            $intermittent = $this->Front_model->getintermittent($invoicedata->project_id, $subteamdata[$i]->emp_id);
            if (!empty($intermittent)) {
                $manmonthscrukey = $this->Front_model->getintermittentattd($invoicedata->project_id, $subteamdata[$i]->emp_id, $subteamdata[$i]->designation_id, $crumonths, $cruyears);
                $totalnoofdays = 30;
                if (($manmonthscrukey->present) > 0) {
                    $mmcountpresub = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                } else {
                    $mmcountpresub = 0;
                }
            } else {
                $timemonthsub = $this->Front_model->gettimesheetdetail($invoicedata->project_id, $subteamdata[$i]->designation_id, $subteamdata[$i]->emp_id, $crumonths, $cruyears);
                if (($timemonthsub->leave + $timemonthsub->absent) > 0) {
                    $mmcountpresub = round((1 - ($timemonthsub->leave + $timemonthsub->absent) / $divideno_days), $decimalplace);
                    if ($mmcountpresub < 0) {
                        $mmcountpresub = 0;
                    }
                } else {
                    if (($timemonthsub->present) > 0) {
                        $mmcountpresub = 1;
                    } else {
                        $mmcountpresub = 0;
                    }
                }
            }

            if ($subteamdata[$i]->rate > 0) {
                $replacement_reducationrate = $subteamdata[$i]->rate - (($subteamdata[$i]->rate * $subteamdata[$i]->replacement_reducationrate) / 100);
            } else {
                $replacement_reducationrate = $subteamdata[$i]->rate;
            }
            $mmamountsub = $mmcountpresub * $replacement_reducationrate;
            $amount = $subteamdata[$i]->rate * $subteamdata[$i]->mm;
            $designation_name = $subteamdata[$i]->designation;
            $userfullname = $subteamdata[$i]->username;
            $rate = $subteamdata[$i]->rate;
            $man_months = $subteamdata[$i]->mm;
            $eot_mm = $subteamdata[$i]->eot_mm;
            $rcv_mm = $subteamdata[$i]->eot_mm + $man_months;
            $eotamount = $subteamdata[$i]->eot_mm * $subteamdata[$i]->rate;
            $rcvamount = $rcv_mm * $subteamdata[$i]->rate;
            $cumulativemm = $subteamdata[$i]->cumulativemm;
            if ($cumulativemm < 0) {
                $cumulativemm = 0;
            }
            $cumulativeamount = $subteamdata[$i]->cumulativeamount;
            if ($cumulativeamount < 0) {
                $cumulativeamount = 0;
            }
            $totalcumulativemm = $cumulativemm + $mmcountpresub;
            $totalcumulativeamount = $mmamountsub + $cumulativeamount;

            $keybalancemm = $man_months + $eot_mm - $totalcumulativemm;
            $keybalanceamount = $amount + $eotamount - $totalcumulativeamount;
            $balanceamt += $keybalanceamount;

            // echo 'A' . "$jj" . 'B' . "$designation_name" . 'C' . "$userfullname" . 'D' . "$rate" . 'E' . "$man_months" . 'F' . "$amount" . 'G' . "$eot_mm" . 'H' . "$eotamount" . 'I' . "$rcv_mm" . 'J' . "$rcvamount" . 'K' . "$cumulativemm" . 'L' . "$cumulativeamount" . 'M' . "$mmcountpresub" . 'N' . "$mmamountsub" . 'O' . "$totalcumulativemm" . 'P' . "$totalcumulativeamount" . 'Q : - ' . "$keybalancemm" . "<hr>";

            $InvcInsertArr2 = array("project_id" => $invoicedata->project_id,
                "project_numberid" => $invoicedata->project_numberid,
                "invoice_no_id" => $invoicedata->invoiceno,
                "ra_invoice_no" => $invoicedata->ra_invoice,
                "year" => $cruyears,
                "month" => $crumonths,
                "empl_id" => $subteamdata[$i]->emp_id,
                "designation_id" => $subteamdata[$i]->designation_id,
                "key_type" => "2",
                "billed_mm" => ($mmcountpresub) ? $mmcountpresub : '',
                "billed_amount" => ($mmamountsub) ? ($mmamountsub) : '',
                "balance_mm" => ($keybalancemm) ? $keybalancemm : '',
                "total_mm" => ($man_months) ? $man_months : '',
                "balance_amount" => ($keybalanceamount) ? $keybalanceamount : "",
                "entry_by" => $this->session->userdata('uid'));

            $UpDdataArr2 = array(
                "billed_mm" => ($mmcountpresub) ? $mmcountpresub : '',
                "billed_amount" => ($mmamountsub) ? ($mmamountsub) : '',
                "balance_mm" => ($keybalancemm) ? $keybalancemm : '',
                "total_mm" => ($man_months) ? $man_months : '',
                "balance_amount" => ($keybalanceamount) ? $keybalanceamount : "",
                "entry_by" => $this->session->userdata('uid'));
            $WhereArr2 = array("project_id" => $invoicedata->project_id,
                "project_numberid" => $invoicedata->project_numberid,
                "ra_invoice_no" => $invoicedata->ra_invoice,
                "year" => $cruyears,
                "month" => $crumonths,
                "empl_id" => $subteamdata[$i]->emp_id,
                "designation_id" => $subteamdata[$i]->designation_id,
                "key_type" => "2");
            //Check Exist or Not..
            $this->db->select('b.fld_id');
            $this->db->from('monthwise_invc_saveupd_afterlock as b');
            $this->db->where(array("b.project_id" => $invoicedata->project_id, "b.project_numberid" => $invoicedata->project_numberid, "b.ra_invoice_no" => $invoicedata->ra_invoice, "b.year" => $cruyears, "b.month" => $crumonths, "b.empl_id" => $subteamdata[$i]->emp_id, "b.designation_id" => $subteamdata[$i]->designation_id, "b.key_type" => "2"));
            $invc_SummDetail = $this->db->get()->row();
            if ($invc_SummDetail->fld_id):
                //Update Query..
                $this->mastermodel->UpdateRecords('monthwise_invc_saveupd_afterlock', $WhereArr2, $UpDdataArr2);
            else:
                //Insert Query..
                if ($subteamdata[$i]->emp_id) {
                    $this->mastermodel->InsertMasterData($InvcInsertArr2, 'monthwise_invc_saveupd_afterlock');
                }
            endif;
            echo $userfullname . "<br>";
        }
        //Code For 2 Admin Staff..
        for ($i = 0; $i < $adminID; $i++) {
            $intermittent = $this->Front_model->getintermittent($invoicedata->project_id, $adminteamdata[$i]->emp_id);
            if (!empty($intermittent)) {
                $manmonthscrukey = $this->Front_model->getintermittentattd($invoicedata->project_id, $adminteamdata[$i]->emp_id, $adminteamdata[$i]->designation_id, $crumonths, $cruyears);
                $totalnoofdays = 30;
                if (($manmonthscrukey->present) > 0) {
                    $mmcountpreadmin = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                } else {
                    $mmcountpreadmin = 0;
                }
            } else {
                $timemonth = $this->Front_model->gettimesheetdetail($invoicedata->project_id, $adminteamdata[$i]->designation_id, $adminteamdata[$i]->emp_id, $crumonths, $cruyears);
                if (($timemonth->leave + $timemonth->absent) > 0) {
                    $mmcountpreadmin = round((1 - ($timemonth->leave + $timemonth->absent) / $divideno_days), $decimalplace);
                    if ($mmcountpreadmin < 0) {
                        $mmcountpreadmin = 0;
                    }
                } else {
                    if (($timemonth->present) > 0) {
                        $mmcountpreadmin = 1;
                    } else {
                        $mmcountpreadmin = 0;
                    }
                }
            }

            if ($adminteamdata[$i]->rate > 0) {
                $replacement_reducationrate = $adminteamdata[$i]->rate - (($adminteamdata[$i]->rate * $adminteamdata[$i]->replacement_reducationrate) / 100);
            } else {
                $replacement_reducationrate = $adminteamdata[$i]->rate;
            }
            $mmamountkey = $mmcountpreadmin * $replacement_reducationrate;
            $amount = $adminteamdata[$i]->rate * $adminteamdata[$i]->mm;
            $designation_name = $adminteamdata[$i]->designation;
            $userfullname = $adminteamdata[$i]->username;
            $rate = $adminteamdata[$i]->rate;
            $man_months = $adminteamdata[$i]->mm;
            $eot_mm = $adminteamdata[$i]->eot_mm;
            $rcv_mm = $adminteamdata[$i]->eot_mm + $man_months;
            $eotamount = $adminteamdata[$i]->eot_mm * $adminteamdata[$i]->rate;
            $rcvamount = $rcv_mm * $adminteamdata[$i]->rate;
            $cumulativemm = $adminteamdata[$i]->cumulativemm;
            if ($cumulativemm < 0) {
                $cumulativemm = 0;
            }
            $cumulativeamount = $adminteamdata[$i]->cumulativeamount;
            if ($cumulativeamount < 0) {
                $cumulativeamount = 0;
            }

            $totalcumulativemm = $cumulativemm + $mmcountpreadmin;
            $totalcumulativeamount = $mmamountkey + $cumulativeamount;
            $keybalancemm = $man_months + $eot_mm - $totalcumulativemm;
            $keybalanceamount = $amount + $eotamount - $totalcumulativeamount;
            $balanceamt += $keybalanceamount;

            $InvcInsertArr3 = array("project_id" => $invoicedata->project_id,
                "project_numberid" => $invoicedata->project_numberid,
                "invoice_no_id" => $invoicedata->invoiceno,
                "ra_invoice_no" => $invoicedata->ra_invoice,
                "year" => $cruyears,
                "month" => $crumonths,
                "empl_id" => $adminteamdata[$i]->emp_id,
                "designation_id" => $adminteamdata[$i]->designation_id,
                "key_type" => "3",
                "billed_mm" => ($mmcountpreadmin) ? $mmcountpreadmin : '',
                "billed_amount" => ($mmamountkey) ? ($mmamountkey) : '',
                "balance_mm" => ($keybalancemm) ? $keybalancemm : '',
                "total_mm" => ($man_months) ? $man_months : '',
                "balance_amount" => ($keybalanceamount) ? $keybalanceamount : "",
                "entry_by" => $this->session->userdata('uid'));

            $UpDdataArr3 = array(
                "billed_mm" => ($mmcountpreadmin) ? $mmcountpreadmin : '',
                "billed_amount" => ($mmamountkey) ? ($mmamountkey) : '',
                "balance_mm" => ($keybalancemm) ? $keybalancemm : '',
                "total_mm" => ($man_months) ? $man_months : '',
                "balance_amount" => ($keybalanceamount) ? $keybalanceamount : "",
                "entry_by" => $this->session->userdata('uid'));
            $WhereArr3 = array("project_id" => $invoicedata->project_id,
                "project_numberid" => $invoicedata->project_numberid,
                "ra_invoice_no" => $invoicedata->ra_invoice,
                "year" => $cruyears,
                "month" => $crumonths,
                "empl_id" => $adminteamdata[$i]->emp_id,
                "designation_id" => $adminteamdata[$i]->designation_id,
                "key_type" => "3");
            //Check Exist or Not..
            $this->db->select('b.fld_id');
            $this->db->from('monthwise_invc_saveupd_afterlock as b');
            $this->db->where(array("b.project_id" => $invoicedata->project_id, "b.project_numberid" => $invoicedata->project_numberid, "b.ra_invoice_no" => $invoicedata->ra_invoice, "b.year" => $cruyears, "b.month" => $crumonths, "b.empl_id" => $adminteamdata[$i]->emp_id, "b.designation_id" => $adminteamdata[$i]->designation_id, "b.key_type" => "3"));
            $invc_SummDetail = $this->db->get()->row();
            if ($invc_SummDetail->fld_id):
                //Update Query..
                $this->mastermodel->UpdateRecords('monthwise_invc_saveupd_afterlock', $WhereArr3, $UpDdataArr3);
            else:
                //Insert Query..
                if ($adminteamdata[$i]->emp_id) {
                    $this->mastermodel->InsertMasterData($InvcInsertArr3, 'monthwise_invc_saveupd_afterlock');
                }
            endif;
            echo $userfullname . "<br>";
        }
    }

    //17-05-2019.. Asheesh
    public function exceldown() {
        $invoiceID = $this->uri->segment(3);
        //$this->load->library('excel');
        //$invoiceID = 1;
        $invoicedata = $this->Front_model->getaccountdetailforexcel($invoiceID);
        $projectname = $this->Front_model->getprojectname($invoicedata->project_id);
        $decimalplace = $invoicedata->decimal_place;
        $no_days = $invoicedata->no_days;

        $invoicedate = date('jS F Y', strtotime($invoicedata->invoice_date));
        //$selectdate =  date('Y-m-d',strtotime('-1 month',strtotime($invoicedata->invoice_date)));
        //$year = date('Y', strtotime($selectdate));
        //$month = date('n', strtotime($selectdate));

        $previousdate = date('Y-m-d', strtotime($invoicedata->invoice_date . "-2 month"));
        $preyears = date('Y', strtotime($previousdate));
        $premonths = date('n', strtotime($previousdate));

        $currentdate = date('Y-m-d', strtotime($invoicedata->invoice_date . "-1 month"));
        $currentinvoicedate = date('F Y', strtotime($currentdate));
        $cruyears = date('Y', strtotime($currentdate));
        $crumonths = date('n', strtotime($currentdate));
        $crumonthschk = date('m', strtotime($currentdate));
        $d = cal_days_in_month(CAL_GREGORIAN, $crumonths, $cruyears);

        $startDate = date('Y-m-d', strtotime($cruyears . '-' . $crumonthschk . '-01'));
        $endDate = date('Y-m-d', strtotime($cruyears . '-' . $crumonthschk . "-$d"));

        if (!empty($no_days)) {
            $divideno_days = $no_days;
        } else {
            $divideno_days = cal_days_in_month(CAL_GREGORIAN, $crumonths, $cruyears);
        }

        $holidayArr = $this->getHoliday($startDate, $endDate);
        $taxinvoice = $invoicedata->invoice_name . '/' . $invoicedata->invoiceno;

        $keyteamdata = $this->Front_model->getexceldataproject_withreplc($invoicedata->project_id, $invoicedata->project_numberid, $invoicedata->invoiceno, 1);
        $subteamdata = $this->Front_model->getexceldataproject_withreplc($invoicedata->project_id, $invoicedata->project_numberid, $invoicedata->invoiceno, 2);
        $adminteamdata = $this->Front_model->getexceldataproject_withreplc($invoicedata->project_id, $invoicedata->project_numberid, $invoicedata->invoiceno, 3);


        $escalation = $this->Front_model->getescalation($invoicedata->project_id, $invoiceID);
        $projtax = $this->Front_model->getprojecttax($invoicedata->project_id, $invoiceID);
        // echo '<pre>'; print_r($escalation); die;
        $adminID = count($adminteamdata);
        $keyID = count($keyteamdata);
        $subID = count($subteamdata);

        $transportation = gettransportation($invoicedata->project_id, 1);
        $travel_site = gettransportation($invoicedata->project_id, 2);
        $office_rent = gettransportation($invoicedata->project_id, 3);
        $office_supplies = gettransportation($invoicedata->project_id, 4);
        $office_furniture = gettransportation($invoicedata->project_id, 5);
        $office_equ = gettransportation($invoicedata->project_id, 6);
        $reports = gettransportation($invoicedata->project_id, 7);
        $survey_equ = gettransportation($invoicedata->project_id, 8);
        $road_survey_equ = gettransportation($invoicedata->project_id, 9);
        $contingencies = gettransportation($invoicedata->project_id, 10);

        $transID = count($transportation);
        $travelID = count($travel_site);
        $officerentID = count($office_rent);
        $suppliesID = count($office_supplies);
        $furnitureID = count($office_furniture);
        $equipID = count($office_equ);
        $reportID = count($reports);
        $survey_equID = count($survey_equ);
        $road_survey_equID = count($road_survey_equ);
        $contingenciesID = count($contingencies);

        $escalationID = count($escalation);
        $projtaxID = count($projtax);

        $summrytotalcontractkey = 0;
        $summerytotaleotamountkey = 0;
        $summerytotalmmamountkeys = 0;
        $summerytotalcumulativeamountskey = 0;
        $summerytotalcumulativeamountfull = 0;
        $summerytotalkeybalanceamount = 0;

        for ($i = 0; $i < $keyID; $i++) {
            $intermittent = $this->Front_model->getintermittent($invoicedata->project_id, $keyteamdata[$i]->emp_id);
            if (!empty($intermittent)) {
                $manmonthscrukey = $this->Front_model->getintermittentattd($invoicedata->project_id, $keyteamdata[$i]->emp_id, $keyteamdata[$i]->designation_id, $crumonths, $cruyears);
                if (($manmonthscrukey->present) > 0) {
                    $mmcountprekey = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                } else {
                    $mmcountprekey = 0;
                }
            } else {
                $timemonth = $this->Front_model->gettimesheetdetail($invoicedata->project_id, $keyteamdata[$i]->designation_id, $keyteamdata[$i]->emp_id, $crumonths, $cruyears);
                if (($timemonth->leave + $timemonth->absent) > 0) {
                    $mmcountprekey = round((1 - ($timemonth->leave + $timemonth->absent) / $divideno_days), $decimalplace);
                    if ($mmcountprekey < 0) {
                        $mmcountprekey = 0;
                    }
                } else {
                    if (($timemonth->present) > 0) {
                        $mmcountprekey = 1;
                    } else {
                        $mmcountprekey = 0;
                    }
                }
            }

            if ($keyteamdata[$i]->rate > 0) {
                $replacement_reducationrate = $keyteamdata[$i]->rate - (($keyteamdata[$i]->rate * $keyteamdata[$i]->replacement_reducationrate) / 100);
            } else {
                $replacement_reducationrate = $keyteamdata[$i]->rate;
            }

            $mmamountkey = $mmcountprekey * $replacement_reducationrate;
            $amount = $keyteamdata[$i]->rate * $keyteamdata[$i]->mm;
            $eotamount = $keyteamdata[$i]->eot_mm * $keyteamdata[$i]->rate;
            $cumulativeamount = $keyteamdata[$i]->cumulativeamount;
            if ($cumulativeamount < 0) {
                $cumulativeamount = 0;
            }
            $totalcumulativeamount = $mmamountkey + $cumulativeamount;
            $keybalanceamount = $amount + $eotamount - $totalcumulativeamount;

            $summrytotalcontractkey += $amount;
            $summerytotaleotamountkey += $eotamount;
            $summerytotalcumulativeamountskey += $cumulativeamount;
            $summerytotalmmamountkeys += $mmamountkey;
            $summerytotalcumulativeamountfull += $totalcumulativeamount;
            $summerytotalkeybalanceamount += $keybalanceamount;
        }

        $summerytotalcontractsub = 0;
        $summerytotaleotamountsub = 0;
        $summerytotalcumulativeamountssub = 0;
        $summerytotalmmamountkeyssub = 0;
        $summerytotalcumulativeamountfullsub = 0;
        $summerytotalkeybalanceamountsub = 0;

        for ($i = 0; $i < $subID; $i++) {
            $intermittent = $this->Front_model->getintermittent($invoicedata->project_id, $subteamdata[$i]->emp_id);
            if (!empty($intermittent)) {
                $manmonthscrukey = $this->Front_model->getintermittentattd($invoicedata->project_id, $subteamdata[$i]->emp_id, $subteamdata[$i]->designation_id, $crumonths, $cruyears);
                if (($manmonthscrukey->present) > 0) {
                    $mmcountpresub = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                } else {
                    $mmcountpresub = 0;
                }
            } else {
                $timemonthsub = $this->Front_model->gettimesheetdetail($invoicedata->project_id, $subteamdata[$i]->designation_id, $subteamdata[$i]->emp_id, $crumonths, $cruyears);
                if (($timemonthsub->leave + $timemonthsub->absent) > 0) {
                    $mmcountpresub = round((1 - ($timemonthsub->leave + $timemonthsub->absent) / $divideno_days), $decimalplace);
                    if ($mmcountpresub < 0) {
                        $mmcountpresub = 0;
                    }
                } else {
                    if (($timemonthsub->present) > 0) {
                        $mmcountpresub = 1;
                    } else {
                        $mmcountpresub = 0;
                    }
                }
            }

            if ($subteamdata[$i]->rate > 0) {
                $replacement_reducationrate = $subteamdata[$i]->rate - (($subteamdata[$i]->rate * $subteamdata[$i]->replacement_reducationrate) / 100);
            } else {
                $replacement_reducationrate = $subteamdata[$i]->rate;
            }

            $mmamountsub = $mmcountpresub * $replacement_reducationrate;
            $amount = $subteamdata[$i]->rate * $subteamdata[$i]->mm;
            $eotamount = $subteamdata[$i]->eot_mm * $subteamdata[$i]->rate;
            $cumulativeamount = $subteamdata[$i]->cumulativeamount;
            if ($cumulativeamount < 0) {
                $cumulativeamount = 0;
            }
            $totalcumulativeamount = $mmamountsub + $cumulativeamount;
            $keybalanceamount = $amount + $eotamount - $totalcumulativeamount;

            $summerytotalcontractsub += $amount;
            $summerytotaleotamountsub += $eotamount;
            $summerytotalcumulativeamountssub += $cumulativeamount;
            $summerytotalmmamountkeyssub += $mmamountsub;
            $summerytotalcumulativeamountfullsub += $totalcumulativeamount;
            $summerytotalkeybalanceamountsub += $keybalanceamount;
        }

        $totalsummrycontract = $summrytotalcontractkey + $summerytotalcontractsub;
        $totalsummryeot = $summerytotaleotamountkey + $summerytotaleotamountsub;
        $totalsummrycumulativeamount = $summerytotalcumulativeamountskey + $summerytotalcumulativeamountssub;
        $totalsummryamount = $summerytotalmmamountkeys + $summerytotalmmamountkeyssub;
        $totalsummrycumulativeamountfull = $summerytotalcumulativeamountfull + $summerytotalcumulativeamountfullsub;
        $totalsummrybalanceamount = $summerytotalkeybalanceamount + $summerytotalkeybalanceamountsub;

        $summerytotalcontract = 0;
        $summerytotaleotamount = 0;
        $summerytotalcumulativeamounts = 0;
        $summerytotalmmamountkeys = 0;
        $summerytotalcumulativeamountfull = 0;
        $summerytotalkeybalanceamount = 0;

        for ($i = 0; $i < $adminID; $i++) {
            $intermittent = $this->Front_model->getintermittent($invoicedata->project_id, $adminteamdata[$i]->emp_id);
            if (!empty($intermittent)) {
                $manmonthscrukey = $this->Front_model->getintermittentattd($invoicedata->project_id, $adminteamdata[$i]->emp_id, $adminteamdata[$i]->designation_id, $crumonths, $cruyears);
                if (($manmonthscrukey->present) > 0) {
                    $mmcountpreadmin = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                } else {
                    $mmcountpreadmin = 0;
                }
            } else {
                $timemonth = $this->Front_model->gettimesheetdetail($invoicedata->project_id, $adminteamdata[$i]->designation_id, $adminteamdata[$i]->emp_id, $crumonths, $cruyears);
                if (($timemonth->leave + $timemonth->absent) > 0) {
                    $mmcountpreadmin = round((1 - ($timemonth->leave + $timemonth->absent) / $divideno_days), $decimalplace);
                    if ($mmcountpreadmin < 0) {
                        $mmcountpreadmin = 0;
                    }
                } else {
                    if (($timemonth->present) > 0) {
                        $mmcountpreadmin = 1;
                    } else {
                        $mmcountpreadmin = 0;
                    }
                }
            }

            if ($adminteamdata[$i]->rate > 0) {
                $replacement_reducationrate = $adminteamdata[$i]->rate - (($adminteamdata[$i]->rate * $adminteamdata[$i]->replacement_reducationrate) / 100);
            } else {
                $replacement_reducationrate = $adminteamdata[$i]->rate;
            }

            $mmamountkey = $mmcountpreadmin * $replacement_reducationrate;
            $amount = $adminteamdata[$i]->rate * $adminteamdata[$i]->mm;
            $eotamount = $adminteamdata[$i]->eot_mm * $adminteamdata[$i]->rate;
            $cumulativeamount = $adminteamdata[$i]->cumulativeamount;
            if ($cumulativeamount < 0) {
                $cumulativeamount = 0;
            }

            $totalcumulativeamount = $mmamountkey + $cumulativeamount;
            $keybalanceamount = $amount + $eotamount - $totalcumulativeamount;

            $summerytotalcontract += $amount;
            $summerytotaleotamount += $eotamount;
            $summerytotalcumulativeamounts += $cumulativeamount;
            $summerytotalmmamountkeys += $mmamountkey;
            $summerytotalcumulativeamountfull += $totalcumulativeamount;
            $summerytotalkeybalanceamount += $keybalanceamount;
        }

        $summerytranstotalcontract = 0;
        $summerytranstotalcum = 0;
        $summerytranstotalcur = 0;
        $summerytranstotaltocum = 0;
        $summerytranstotalbal = 0;

        for ($i = 0; $i < $transID; $i++) {
            $transeot = $this->Front_model->getreimbursableeot($invoicedata->project_id, $transportation[$i]->id);
            $per_month = $transportation[$i]->per_month;
            $no_month = $transportation[$i]->no_month;
            $amount = str_replace(",", "", $per_month) * $no_month;

            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $transportation[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $transportation[$i]->id, $invoicedata->invoice_date);

            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month;
            $totalcuamt = $cumprevamt + $currentamount;

            $eot_mm = $transeot->eot_mm;
            $eot_amount = $per_month * $eot_mm;
            $balanceamt = $amount + $eot_amount - $totalcuamt;

            $summerytranstotalcontract += $amount;
            $summerytranstotalcum += $cumprevamt;
            $summerytranstotalcur += $currentamount;
            $summerytranstotaltocum += $totalcuamt;
            $summerytranstotalbal += $balanceamt;
        }

        $summerytraveltotalcontract = 0;
        $summerytraveltotalcum = 0;
        $summerytraveltotalcur = 0;
        $summerytraveltotaltocum = 0;
        $summerytraveltotalbal = 0;
        for ($i = 0; $i < $travelID; $i++) {
            $traveleot = $this->Front_model->getreimbursableeot($invoicedata->project_id, $travel_site[$i]->id);
            $per_month = $travel_site[$i]->per_month;
            $no_month = $travel_site[$i]->no_month;

            $amount = $per_month * $no_month;
            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $travel_site[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $travel_site[$i]->id, $invoicedata->invoice_date);

            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month;

            $eot_mm = $traveleot->eot_mm;
            $eot_amount = $per_month * $eot_mm;

            $totalcuamt = $cumprevamt + $currentamount;
            $balanceamt = $amount + $eot_amount - $totalcuamt;

            $summerytraveltotalcontract += $amount;
            $summerytraveltotalcum += $cumprevamt;
            $summerytraveltotalcur += $currentamount;
            $summerytraveltotaltocum += $totalcuamt;
            $summerytraveltotalbal += $balanceamt;
        }

        $summeryofficerenttotalcontract = 0;
        $summeryofficerenttotalcum = 0;
        $summeryofficerenttotalcur = 0;
        $summeryofficerenttotaltocums = 0;
        $summeryofficerenttotalbal = 0;
        for ($i = 0; $i < $officerentID; $i++) {
            $officerenteot = $this->Front_model->getreimbursableeot($invoicedata->project_id, $office_rent[$i]->id);
            $per_month = $office_rent[$i]->per_month;
            $no_month = $office_rent[$i]->no_month;
            $amount = $per_month * $no_month;
            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $office_rent[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $office_rent[$i]->id, $invoicedata->invoice_date);

            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month;
            $totalcuamt = $cumprevamt + $currentamount;

            $eot_mm = $officerenteot->eot_mm;
            $eot_amount = $per_month * $eot_mm;
            $balanceamt = $amount + $eot_amount - $totalcuamt;

            $summeryofficerenttotalcontract += $amount;
            $summeryofficerenttotalcum += $cumprevamt;
            $summeryofficerenttotalcur += $currentamount;
            $summeryofficerenttotaltocums += $totalcuamt;
            $summeryofficerenttotalbal += $balanceamt;
        }

        $summerysuppliestotalcontract = 0;
        $summerysuppliestotalcum = 0;
        $summerysuppliestotalcur = 0;
        $summerysuppliestotaltocum = 0;
        $summerysuppliestotalbal = 0;
        for ($i = 0; $i < $suppliesID; $i++) {
            $officesupplieseot = $this->Front_model->getreimbursableeot($invoicedata->project_id, $office_supplies[$i]->id);
            $per_month = $office_supplies[$i]->per_month;
            $no_month = $office_supplies[$i]->no_month;
            $amount = $per_month * $no_month;

            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $office_supplies[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $office_supplies[$i]->id, $invoicedata->invoice_date);

            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month;
            $totalcuamt = $cumprevamt + $currentamount;

            $eot_mm = $officesupplieseot->eot_mm;
            $eot_amount = $per_month * $eot_mm;
            $balanceamt = $amount + $eot_amount - $totalcuamt;

            $summerysuppliestotalcontract += $amount;
            $summerysuppliestotalcum += $cumprevamt;
            $summerysuppliestotalcur += $currentamount;
            $summerysuppliestotaltocum += $totalcuamt;
            $summerysuppliestotalbal += $balanceamt;
        }

        $summeryfurnituretotalcontract = 0;
        $summeryfurnituretotalcum = 0;
        $summeryfurnituretotalcur = 0;
        $summeryfurnituretotaltocum = 0;
        $summeryfurnituretotalbal = 0;

        for ($i = 0; $i < $furnitureID; $i++) {
            $officefurnitureeot = $this->Front_model->getreimbursableeot($invoicedata->project_id, $office_furniture[$i]->id);
            $per_month = $office_furniture[$i]->per_month;
            $no_month = $office_furniture[$i]->no_month;
            $qty = ($office_furniture[$i]->qty > 0) ? $office_furniture[$i]->qty : "1";
            $amount = $per_month * $no_month * $qty;
            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $office_furniture[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $office_furniture[$i]->id, $invoicedata->invoice_date);

            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;

            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month * $qty;
            $totalcuamt = $cumprevamt + $currentamount;

            $eot_mm = $officefurnitureeot->eot_mm;
            $eot_amount = $per_month * $qty * $eot_mm;
            $balanceamt = $amount + $eot_amount - $totalcuamt;

            $summeryfurnituretotalcontract += $amount;
            $summeryfurnituretotalcum += $cumprevamt;
            $summeryfurnituretotalcur += $currentamount;
            $summeryfurnituretotaltocum += $totalcuamt;
            $summeryfurnituretotalbal += $balanceamt;
        }

        $summeryequiptotalcontract = 0;
        $summeryequiptotalcum = 0;
        $summeryequiptotalcur = 0;
        $summeryequiptotaltocum = 0;
        $summeryequiptotalbal = 0;
        for ($i = 0; $i < $equipID; $i++) {
            $officeequeot = $this->Front_model->getreimbursableeot($invoicedata->project_id, $office_equ[$i]->id);
            $per_month = $office_equ[$i]->per_month;
            $no_month = $office_equ[$i]->no_month;
            $qty = ($office_equ[$i]->qty > 0) ? $office_equ[$i]->qty : "1";
            $amount = $per_month * $no_month * $qty;

            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $office_equ[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $office_equ[$i]->id, $invoicedata->invoice_date);
            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;

            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month * $qty;
            $totalcuamt = $cumprevamt + $currentamount;

            $eot_mm = $officeequeot->eot_mm;
            $eot_amount = $per_month * $qty * $eot_mm;
            $balanceamt = $amount + $eot_amount - $totalcuamt;

            $summeryequiptotalcontract += $amount;
            $summeryequiptotalcum += $cumprevamt;
            $summeryequiptotalcur += $currentamount;
            $summeryequiptotaltocum += $totalcuamt;
            $summeryequiptotalbal += $balanceamt;
        }

        $summeryreporttotalcontract = 0;
        $summeryreporttotalcum = 0;
        $summeryreporttotalcur = 0;
        $summeryreporttotaltocum = 0;
        $summeryreporttotalbal = 0;

        for ($i = 0; $i < $reportID; $i++) {
            $reportseot = $this->Front_model->getreimbursableeot($invoicedata->project_id, $reports[$i]->id);
            $qty = ($reports[$i]->qty > 0) ? $reports[$i]->qty : "1";
            $per_month = $reports[$i]->per_month;
            $no_month = $reports[$i]->no_month;
            $totalcopies = ($reports[$i]->no_month * $qty);
            $contractamt = ($totalcopies * $per_month);

            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $reports[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $reports[$i]->id, $invoicedata->invoice_date);
            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;

            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month * $qty;
            $totalcuamt = $cumprevamt + $currentamount;

            $eot_mm = $reportseot->eot_mm;
            $eot_amount = $per_month * $qty * $eot_mm;
            $balanceamt = $contractamt + $eot_amount - $totalcuamt;

            $summeryreporttotalcontract += $contractamt;
            $summeryreporttotalcum += $cumprevamt;
            $summeryreporttotalcur += $currentamount;
            $summeryreporttotaltocum += $totalcuamt;
            $summeryreporttotalbal += $balanceamt;
        }

        $summerysurvey_equtotalcontract = 0;
        $summerysurvey_equtotalcum = 0;
        $summerysurvey_equtotalcur = 0;
        $summerysurvey_equtotaltocum = 0;
        $summerysurvey_equtotalbal = 0;

        for ($i = 0; $i < $survey_equID; $i++) {
            $survey_equeot = $this->Front_model->getreimbursableeot($invoicedata->project_id, $survey_equ[$i]->id);
            $per_month = $survey_equ[$i]->per_month;
            $no_month = $survey_equ[$i]->no_month;
            $totalamt = ($no_month * $per_month);

            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $survey_equ[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $survey_equ[$i]->id, $invoicedata->invoice_date);
            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month;

            $eot_mm = $survey_equeot->eot_mm;
            $eot_amount = $per_month * $eot_mm;
            $totalcuamt = $cumprevamt + $currentamount;
            $balanceamt = $totalamt + $eot_amount - $totalcuamt;

            $summerysurvey_equtotalcontract += $totalamt;
            $summerysurvey_equtotalcum += $cumprevamt;
            $summerysurvey_equtotalcur += $currentamount;
            $summerysurvey_equtotaltocum += $totalcuamt;
            $summerysurvey_equtotalbal += $balanceamt;
        }

        $summeryroad_survey_equtotalcontract = 0;
        $summeryroad_survey_equtotalcum = 0;
        $summeryroad_survey_equtotalcur = 0;
        $summeryroad_survey_equtotaltocum = 0;
        $summeryroad_survey_equtotalbal = 0;

        for ($i = 0; $i < $road_survey_equID; $i++) {
            $per_month = $road_survey_equ[$i]->per_month;
            $no_month = $road_survey_equ[$i]->no_month;
            $qty = ($road_survey_equ[$i]->qty > 0) ? $road_survey_equ[$i]->qty : "1";
            $totalamt = ($no_month * $per_month * $qty);

            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $road_survey_equ[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $road_survey_equ[$i]->id, $invoicedata->invoice_date);
            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month;

            $totalcuamt = $cumprevamt + $currentamount;
            $balanceamt = $totalamt - $totalcuamt;

            $summeryroad_survey_equtotalcontract += $totalamt;
            $summeryroad_survey_equtotalcum += $cumprevamt;
            $summeryroad_survey_equtotalcur += $currentamount;
            $summeryroad_survey_equtotaltocum += $totalcuamt;
            $summeryroad_survey_equtotalbal += $balanceamt;
        }

        $summarycontingenciestotalcontract = 0;
        $summarycontingenciestotalcum = 0;
        $summarycontingenciestotalcur = 0;
        $summarycontingenciestotaltocum = 0;
        $summarycontingenciestotalbal = 0;

        for ($i = 0; $i < $contingenciesID; $i++) {
            $per_month = $contingencies[$i]->per_month;
            $no_month = $contingencies[$i]->no_month;
            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $contingencies[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $contingencies[$i]->id, $invoicedata->invoice_date);
            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;

            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month;

            $totalcuamt = $cumprevamt + $currentamount;
            $balanceamt = $per_month - $totalcuamt;

            $summarycontingenciestotalcontract += $per_month;
            $summarycontingenciestotalcum += $cumprevamt;
            $summarycontingenciestotalcur += $currentamount;
            $summarycontingenciestotaltocum += $totalcuamt;
            $summarycontingenciestotalbal += $balanceamt;
        }

        $summerysubtotalcontract = $totalsummrycontract + $summerytotalcontract + $summerytranstotalcontract + $summerytraveltotalcontract + $summeryofficerenttotalcontract + $summerysuppliestotalcontract + $summeryfurnituretotalcontract + $summeryequiptotalcontract + $summeryreporttotalcontract + $summerysurvey_equtotalcontract + $summeryroad_survey_equtotalcontract + $summarycontingenciestotalcontract;
        $summerysubtotalcum = $totalsummrycumulativeamount + $summerytotalcumulativeamounts + $summerytranstotalcum + $summerytraveltotalcum + $summeryofficerenttotalcum + $summerysuppliestotalcum + $summeryfurnituretotalcum + $summeryequiptotalcum + $summeryreporttotalcum + $summerysurvey_equtotalcum + $summeryroad_survey_equtotalcum + $summarycontingenciestotalcum;
        $summerysubtotalcur = $totalsummryamount + $summerytotalmmamountkeys + $summerytranstotalcur + $summerytraveltotalcur + $summeryofficerenttotalcur + $summerysuppliestotalcur + $summeryfurnituretotalcur + $summeryequiptotalcur + $summeryreporttotalcur + $summerysurvey_equtotalcur + $summeryroad_survey_equtotalcur + $summarycontingenciestotalcur;
        $summerysubtotalcumtotal = $totalsummrycumulativeamountfull + $summerytotalcumulativeamountfull + $summerysuppliestotaltocum + $summerysurvey_equtotaltocum + $summeryreporttotaltocum + $summeryequiptotaltocum + $summeryfurnituretotaltocum + $summerytranstotaltocum + $summerytraveltotaltocum + $summeryofficerenttotaltocums + $summeryroad_survey_equtotaltocum + $summarycontingenciestotaltocum;
        $summerysubtotalbalance = $totalsummrybalanceamount + $summerytotalkeybalanceamount + $summeryreporttotalbal + $summerysurvey_equtotalbal + $summeryequiptotalbal + $summeryfurnituretotalbal + $summerysuppliestotalbal + $summeryofficerenttotalbal + $summerytraveltotalbal + $summerytranstotalbal + $summeryroad_survey_equtotalbal + $summarycontingenciestotalbal;

        //Invoice Percentage (%)
        $summerysubtotalpercum = 0;
        $summerysubtotalpercur = 0;
        $summerysubtotalpercumtotal = 0;
        $summerysubtotalperbalance = 0;
        if ($invoicedata->invoice_type == 2) {
            $keysubper = checpercentage($invoicedata->project_id, $invoiceID, 1);
            $cumpreamt = $keysubper['cumpreamt'];
            $cumamt = $keysubper['cumamt'];
            $totalmmamount = $keysubper['totalmmamount'];
            $keyamt = $totalsummrycontract - $totalmmamount;

            $subsubper = checpercentage($invoicedata->project_id, $invoiceID, 2);
            $subcumpreamt = $subsubper['cumpreamt'];
            $subcumamt = $subsubper['cumamt'];
            $subtotalmmamount = $subsubper['totalmmamount'];

            $subkeyamt = $summerytotalcontract - $subtotalmmamount;
            $transsubper = checpercentage($invoicedata->project_id, $invoiceID, 3);
            $transcumpremm = $transsubper['cumpremm'];
            $transcumpreamt = $transsubper['cumpreamt'];
            $transcurmm = $transsubper['curmm'];
            $transcumamt = $transsubper['cumamt'];
            $transtotalmmcum = $transsubper['totalmmcum'];
            $transtotalmmamount = $transsubper['totalmmamount'];
            $transkeyb = (100 - $transtotalmmcum);
            $transkeyamt = $summerytranstotalcontract - $transtotalmmamount;

            $dutytrvlper = checpercentage($invoicedata->project_id, $invoiceID, 4);
            $dutytrvlcumpremm = $dutytrvlper['cumpremm'];
            $dutytrvlcumpreamt = $dutytrvlper['cumpreamt'];
            $dutytrvlcurmm = $dutytrvlper['curmm'];
            $dutytrvlcumamt = $dutytrvlper['cumamt'];
            $dutytrvltotalmmcum = $dutytrvlper['totalmmcum'];
            $dutytrvltotalmmamount = $dutytrvlper['totalmmamount'];
            $dutytrvlkeyb = (100 - $dutytrvltotalmmcum);
            $dutytrvlkeyamt = $summerytraveltotalcontract - $dutytrvltotalmmamount;

            $officerentper = checpercentage($invoicedata->project_id, $invoiceID, 5);
            $officerentcumpremm = $officerentper['cumpremm'];
            $officerentcumpreamt = $officerentper['cumpreamt'];
            $officerentcurmm = $officerentper['curmm'];
            $officerentcumamt = $officerentper['cumamt'];
            $officerenttotalmmcum = $officerentper['totalmmcum'];
            $officerenttotalmmamount = $officerentper['totalmmamount'];
            $officerentkeyb = (100 - $officerenttotalmmcum);
            $officerentkeyamt = $summeryofficerenttotalcontract - $officerenttotalmmamount;

            $officesuplper = checpercentage($invoicedata->project_id, $invoiceID, 6);
            $officesuplcumpremm = $officesuplper['cumpremm'];
            $officesuplcumpreamt = $officesuplper['cumpreamt'];
            $officesuplcurmm = $officesuplper['curmm'];
            $officesuplcumamt = $officesuplper['cumamt'];
            $officesupltotalmmcum = $officesuplper['totalmmcum'];
            $officesupltotalmmamount = $officesuplper['totalmmamount'];
            $officesuplkeyb = (100 - $officesupltotalmmcum);
            $officesuplkeyamt = $summerysuppliestotalcontract - $officesupltotalmmamount;

            $officefurnper = checpercentage($invoicedata->project_id, $invoiceID, 7);
            $officefurncumpremm = $officefurnper['cumpremm'];
            $officefurncumpreamt = $officefurnper['cumpreamt'];
            $officefurncurmm = $officefurnper['curmm'];
            $officefurncumamt = $officefurnper['cumamt'];
            $officefurntotalmmcum = $officefurnper['totalmmcum'];
            $officefurntotalmmamount = $officefurnper['totalmmamount'];
            $officefurnkeyb = (100 - $officefurntotalmmcum);
            $officefurnkeyamt = $summeryfurnituretotalcontract - $officefurntotalmmamount;

            $officeequiper = checpercentage($invoicedata->project_id, $invoiceID, 8);
            $officeequicumpremm = $officeequiper['cumpremm'];
            $officeequicumpreamt = $officeequiper['cumpreamt'];
            $officeequicurmm = $officeequiper['curmm'];
            $officeequicumamt = $officeequiper['cumamt'];
            $officeequitotalmmcum = $officeequiper['totalmmcum'];
            $officeequitotalmmamount = $officeequiper['totalmmamount'];
            $officeequikeyb = (100 - $officeequitotalmmcum);
            $officeequikeyamt = $summeryequiptotalcontract - $officeequitotalmmamount;

            $reportsdocper = checpercentage($invoicedata->project_id, $invoiceID, 9);
            $reportsdoccumpremm = $reportsdocper['cumpremm'];
            $reportsdoccumpreamt = $reportsdocper['cumpreamt'];
            $reportsdoccurmm = $reportsdocper['curmm'];
            $reportsdoccumamt = $reportsdocper['cumamt'];
            $reportsdoctotalmmcum = $reportsdocper['totalmmcum'];
            $reportsdoctotalmmamount = $reportsdocper['totalmmamount'];
            $reportsdockeyb = (100 - $reportsdoctotalmmcum);
            $reportsdockeyamt = $summeryreporttotalcontract - $reportsdoctotalmmamount;

            $surveyequiper = checpercentage($invoicedata->project_id, $invoiceID, 10);
            $surveyequicumpremm = $surveyequiper['cumpremm'];
            $surveyequicumpreamt = $surveyequiper['cumpreamt'];
            $surveyequicurmm = $surveyequiper['curmm'];
            $surveyequicumamt = $surveyequiper['cumamt'];
            $surveyequitotalmmcum = $surveyequiper['totalmmcum'];
            $surveyequitotalmmamount = $surveyequiper['totalmmamount'];
            $surveyequikeyb = (100 - $surveyequitotalmmcum);
            $surveyequikeyamt = $summerysurvey_equtotalcontract - $surveyequitotalmmamount;

            $roadsurveyequiper = checpercentage($invoicedata->project_id, $invoiceID, 11);
            $roadsurveyeqcumpremm = $roadsurveyequiper['cumpremm'];
            $roadsurveyeqcumpreamt = $roadsurveyequiper['cumpreamt'];
            $roadsurveyeqcurmm = $roadsurveyequiper['curmm'];
            $roadsurveyeqcumamt = $roadsurveyequiper['cumamt'];
            $roadsurveyeqtotalmmcum = $roadsurveyequiper['totalmmcum'];
            $roadsurveyeqtotalmmamount = $roadsurveyequiper['totalmmamount'];
            $roadsurveyeqkeyb = (100 - $roadsurveyeqtotalmmcum);
            $roadsurveyeqkeyamt = $summeryroad_survey_equtotalcontract - $roadsurveyeqtotalmmamount;

            $contper = checpercentage($invoicedata->project_id, $invoiceID, 12);
            $contcumpremm = $contper['cumpremm'];
            $contcumpreamt = $contper['cumpreamt'];
            $contcurmm = $contper['curmm'];
            $contcumamt = $contper['cumamt'];
            $conttotalmmcum = $contper['totalmmcum'];
            $conttotalmmamount = $contper['totalmmamount'];
            $contkeyb = (100 - $conttotalmmcum);
            $contkeyamt = $summarycontingenciestotalcontract - $conttotalmmamount;


            $summerysubtotalpercum = $cumpreamt + $subcumpreamt + $transcumpreamt + $dutytrvlcumpreamt + $officerentcumpreamt + $officesuplcumpreamt + $officefurncumpreamt + $officeequicumpreamt + $reportsdoccumpreamt + $surveyequicumpreamt + $roadsurveyeqcumpreamt + $contcumpreamt;
            $summerysubtotalpercur = $contcumamt + $roadsurveyeqcumamt + $surveyequicumamt + $reportsdoccumamt + $officeequicumamt + $cumamt + $subcumamt + $transcumamt + $dutytrvlcumamt + $officerentcumamt + $officesuplcumamt + $officefurncumamt;
            $summerysubtotalpercumtotal = $conttotalmmamount + $roadsurveyeqtotalmmamount + $surveyequitotalmmamount + $reportsdoctotalmmamount + $officeequitotalmmamount + $officefurntotalmmamount + $totalmmamount + $subtotalmmamount + $transtotalmmamount + $dutytrvltotalmmamount + $officerenttotalmmamount + $officesupltotalmmamount;
            $summerysubtotalperbalance = $contkeyamt + $roadsurveyeqkeyamt + $surveyequikeyamt + $reportsdockeyamt + $officeequikeyamt + $officefurnkeyamt + $keyamt + $subkeyamt + $transkeyamt + $dutytrvlkeyamt + $officerentkeyamt + $officesuplkeyamt;

            for ($i = 0; $i < $escalationID; $i++) {
                $perccurtotals += $escalation[$i]->perccurtotal;
            }
            $totalcurs = $perccurtotals + $summerysubtotalpercur;
        } else {
            for ($i = 0; $i < $escalationID; $i++) {
                $perccurtotals += $escalation[$i]->perccurtotal;
            }
            $totalcurs = $perccurtotals + $summerysubtotalcur;
        }


        //Sheet 1 Code Edited By Asheesh...
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $spreadsheet->getActiveSheet()->setCellValue('A1', 'GST Registered Address');
        $spreadsheet->getActiveSheet()->setCellValue('A2', "$invoicedata->registered_address");
        $spreadsheet->getActiveSheet()->getColumnDimension('A2')->setWidth(20);
        $spreadsheet->getActiveSheet()->mergeCells('A2:B3');

        //Get Custom Code By Asheesh For Get Ac/And IFSC Code...
        $ArrAc_IFSCproj = $this->GetAc_IFSCByProjId($invoicedata->project_id);

        $spreadsheet->getActiveSheet()->setCellValue('A4', 'TAX INVOICE');
        $spreadsheet->getActiveSheet()->mergeCells('A4:D4');

        $spreadsheet->getActiveSheet()->setCellValue('A5', 'Invoice No.: ');
        $spreadsheet->getActiveSheet()->setCellValue('B5', "$taxinvoice");

        $spreadsheet->getActiveSheet()->setCellValue('C5', "Date : $invoicedate");
        $spreadsheet->getActiveSheet()->mergeCells('C5:D5');

        $spreadsheet->getActiveSheet()->setCellValue('A6', 'PAN No.:');
        $spreadsheet->getActiveSheet()->setCellValue('B6', "$invoicedata->pan_no");

        //12-03-2019.. place_supply_stateName Code By Ash
        $place_supply_stateName = '';
        $place_supply_stateName = $this->GetStateNameById($invoicedata->place_of_supply_stateid);

        $spreadsheet->getActiveSheet()->setCellValue('C6', "Place of Supply - $place_supply_stateName ");
        $spreadsheet->getActiveSheet()->mergeCells('C6:D6');

        $spreadsheet->getActiveSheet()->setCellValue('A7', 'GSTN No.');
        $spreadsheet->getActiveSheet()->setCellValue('B7', "$invoicedata->gstn_no");
        $spreadsheet->getActiveSheet()->setCellValue('C7', "State Code - $invoicedata->state_code");

        $spreadsheet->getActiveSheet()->setCellValue('D7', "RA Invoice No.: $invoicedata->ra_invoice");

        $spreadsheet->getActiveSheet()->setCellValue('A8', '');
        $spreadsheet->getActiveSheet()->mergeCells('A8:D8');

        $spreadsheet->getActiveSheet()->setCellValue('A9', 'Name :');
        $spreadsheet->getActiveSheet()->setCellValue('B9', "$invoicedata->name");
        $spreadsheet->getActiveSheet()->getStyle("B9")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->setCellValue('A10', 'Address :');
        $spreadsheet->getActiveSheet()->setCellValue('B10', "$invoicedata->address");
        $spreadsheet->getActiveSheet()->getStyle('B10', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);

        //$spreadsheet->getActiveSheet()->setCellValue('A11', 'GSTN No.');
        //$spreadsheet->getActiveSheet()->setCellValue('B11', "$ArrAc_IFSCproj->client_gst_no");
        //$spreadsheet->getActiveSheet()->setCellValue('D11', "State Code : $ArrAc_IFSCproj->clnt_gst_state_code ");
        $spreadsheet->getActiveSheet()->mergeCells('B11:D13');
        $spreadsheet->getActiveSheet()->getStyle('B11')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        //$spreadsheet->getActiveSheet()->setCellValue('A13', 'Project');
        //$spreadsheet->getActiveSheet()->mergeCells('B12:D12');
        //$spreadsheet->getActiveSheet()->mergeCells('B11:D13');
        $spreadsheet->getActiveSheet()->mergeCells('A11:A13');

        $spreadsheet->getActiveSheet()->getStyle('B11', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);

        //Excel Formating Code By Asheesh 07-02-2019.
        //GST & State code formating by Rahul 15-02-2019
        $spreadsheet->getActiveSheet()->mergeCells('C10:D10');
        $spreadsheet->getActiveSheet()->setCellValue('C10', "GSTN No. : $ArrAc_IFSCproj->client_gst_no \rState Code : $ArrAc_IFSCproj->clnt_gst_state_code");
        $spreadsheet->getActiveSheet()->getStyle('C10', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('C10')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A11')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A2')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle("A9:D10")->applyFromArray(
                [
                    'font' => [
                    //'bold' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );

        $spreadsheet->getActiveSheet()->getStyle("A25:B28")->applyFromArray(
                [
                    'font' => [
                    //'bold' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );

        $spreadsheet->getActiveSheet()->getStyle("B25")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                ]
        );


//Row Background Color -- Code Rahul 15 Feb Sheet 1
        $spreadsheet->getActiveSheet()->getStyle('A15:D15')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('CCCCCC');
        $spreadsheet->getActiveSheet()->setCellValue('A11', 'Project :');
        $ProjTNaMe = strip_tags($invoicedata->TenderDetails);
        $spreadsheet->getActiveSheet()->setCellValue('B11', "$ProjTNaMe");
        //$spreadsheet->getActiveSheet()->mergeCells('B11:D13');
        $spreadsheet->getActiveSheet()->getRowDimension('11')->setRowHeight(60);
        //$spreadsheet->getActiveSheet()->getStyle('B13', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->setCellValue('A14', '');
        $spreadsheet->getActiveSheet()->mergeCells('A14:D14');

        $spreadsheet->getActiveSheet()->setCellValue('A15', 'S.No.');
        $spreadsheet->getActiveSheet()->mergeCells('A15:A16');

        $spreadsheet->getActiveSheet()->setCellValue('B15', 'Description');
        $spreadsheet->getActiveSheet()->mergeCells('B15:B16');

        $spreadsheet->getActiveSheet()->setCellValue('C15', 'Service Accounting Code (SAC)');
        $spreadsheet->getActiveSheet()->mergeCells('C15:C16');
        // $spreadsheet->getActiveSheet()->getStyle('C15:C16', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getRowDimension('15')->setRowHeight(50);
        //  $spreadsheet->getActiveSheet()->getColumnDimension('C15')->setWidth(1050);
        $spreadsheet->getActiveSheet()->getColumnDimensionByColumn('C15')->setWidth(100);

        $spreadsheet->getActiveSheet()->setCellValue('D15', "Amount\n(₹)");
        $spreadsheet->getActiveSheet()->getStyle('D15', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->mergeCells('D15:D16');
        //$spreadsheet->getActiveSheet()->setCellValue('D16', 'Rs.');

        $spreadsheet->getActiveSheet()->setCellValue('A17', '1');
        $spreadsheet->getActiveSheet()->setCellValue('B17', "Consultancy Fee for the Month of : $currentinvoicedate");
        $spreadsheet->getActiveSheet()->setCellValue('D17', "$totalcurs");
        $spreadsheet->getActiveSheet()->getStyle('D20')->getNumberFormat()->setFormatCode('#,##0');
        /* $spreadsheet->getActiveSheet()->setCellValue('A18', '2');
          $spreadsheet->getActiveSheet()->setCellValue('B18', "Add : CGST @ $invoicedata->cgst  %");
          $spreadsheet->getActiveSheet()->setCellValue('D18', '138353');

          $spreadsheet->getActiveSheet()->setCellValue('A19', '3');
          $spreadsheet->getActiveSheet()->setCellValue('B19', "Add : SGST @ $invoicedata->sgst %");
          $spreadsheet->getActiveSheet()->setCellValue('D19', '138353'); */
        $is = 0;
        $ij = 2;
        $taxs1 = 0;
        $staxj = 18;
        for ($i = 0; $i < $projtaxID; $i++) {
            $status = $projtax[$is]->status;
            if ($status == 1) {
                $tax_name = $projtax[$i]->tax_name;
                $currenttaxtotal = $projtax[$i]->currenttaxtotal;
                $spreadsheet->getActiveSheet()->setCellValue("A$staxj", "$ij");
                $spreadsheet->getActiveSheet()->setCellValue("B$staxj", "Add : $tax_name");
                $spreadsheet->getActiveSheet()->setCellValue("D$staxj", "$currenttaxtotal");
                // $spreadsheet->getActiveSheet()->setCellValue('B20', 'Chachi420');
                $staxj++;
                $ij++;
                $taxs1 += $currenttaxtotal;
            }
            $is++;
        }

        $firsttotal = $taxs1 + $totalcurs;

        $spreadsheet->getActiveSheet()->setCellValue("C17", "998331");
        $spreadsheet->getActiveSheet()->mergeCells('C17:C19');

        $spreadsheet->getActiveSheet()->setCellValue('A20', '');
        $spreadsheet->getActiveSheet()->setCellValue('B20', 'TOTAL(Rs.)');
        $spreadsheet->getActiveSheet()->setCellValue('C20', '');
        $spreadsheet->getActiveSheet()->setCellValue('D20', "$firsttotal");

        $spreadsheet->getActiveSheet()->setCellValue('A21', '');
        $spreadsheet->getActiveSheet()->mergeCells('A21:D21');

        //Rupeees in word...
        $inNumber_firsttotal = $this->numberconvert($firsttotal);
        $spreadsheet->getActiveSheet()->setCellValue('A22', "$inNumber_firsttotal");

        $spreadsheet->getActiveSheet()->mergeCells('A22:D22');

        $spreadsheet->getActiveSheet()->setCellValue('A23', "M/s Consulting Engineers Group Ltd.  has obtained  Lower Deduction certificate for the financial year 2019-2020 (AY 2020-21). The rate of TDS on which approval has been given by Income Tax Department is @ 5.00%. The certificate can be download as per following procedure:- log in through Traces  and  follow the below procedure. Procedure for generating Lower Deduction  certificate  u/s 197: ");
        $spreadsheet->getActiveSheet()->mergeCells('A23:D23');

        $spreadsheet->getActiveSheet()->setCellValue('A24', 'Please issue RTGS/DD/ Cheque in favour of');
        //$spreadsheet->getActiveSheet()->mergeCells('A24:B24');

        $spreadsheet->getActiveSheet()->setCellValue('B25', "$invoicedata->bankholdername");
        //$spreadsheet->getActiveSheet()->mergeCells('C24:D24');

        $spreadsheet->getActiveSheet()->setCellValue('A25', 'Name');
        // $spreadsheet->getActiveSheet()->mergeCells('A25:B25');
        $spreadsheet->getActiveSheet()->setCellValue('A26', "Bank\nBranch & Address");
        $spreadsheet->getActiveSheet()->setCellValue('A27', 'Account Number');
        //$spreadsheet->getActiveSheet()->mergeCells('A26:B26');

        $spreadsheet->getActiveSheet()->setCellValue('A28', 'IFS Code');
        //$spreadsheet->getActiveSheet()->mergeCells('A27:B27');
        //Custom Bream Str Code Asheesh...
        $strAdd1 = explode(",", $invoicedata->bank_address);

        $spreadsheet->getActiveSheet()->setCellValue('B26', "$invoicedata->bank_name \n $strAdd1[0] ,$strAdd1[1] \n$strAdd1[2]");
        //$spreadsheet->getActiveSheet()->mergeCells('B25:C25');
        $spreadsheet->getActiveSheet()->setCellValue('D25', "");
        $spreadsheet->getActiveSheet()->mergeCells('C25:D25');
        //$spreadsheet->getActiveSheet()->mergeRows('C25:C26');

        $spreadsheet->getActiveSheet()->setCellValue('B27', "$ArrAc_IFSCproj->bank_accno");
        $spreadsheet->getActiveSheet()->mergeCells('C26:D26');

        $spreadsheet->getActiveSheet()->setCellValue('B28', "$ArrAc_IFSCproj->rtgs_code");
        $spreadsheet->getActiveSheet()->mergeCells('C27:D27');

//        $spreadsheet->getActiveSheet()->setCellValue('A32', 'Kindly, deduct TDS @5% from Invoice (copy of Lower Tax Deduction Certificate No.: 1217AA484C dated 8/5/2017 issued by Office of Assistant / Deputy Commissioner of Income Tax.');
//        $spreadsheet->getActiveSheet()->mergeCells('A32:D33');
        // Set column widths
        // $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        // $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(25);

        $spreadsheet->getActiveSheet()->getStyle('A4:D4')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle("A4:D4")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A4:D7")->applyFromArray(
                [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A11:D11")->applyFromArray(
                [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A12:D23")->applyFromArray(
                [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A1:D29")->applyFromArray(
                [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ]
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A12:A12")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("D7")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ]
                ]
        );


        $spreadsheet->getActiveSheet()->getStyle("A15:D15")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A20:D20")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ]
                ]
        );

        $spreadsheet->getActiveSheet()->getStyle("A22")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A24")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ]
                ]
        );

        $spreadsheet->getActiveSheet()->getStyle("A25:D28")->applyFromArray(
                [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ]
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A25:B25")->applyFromArray(
                [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ]
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A26:B26")->applyFromArray(
                [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ]
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A27:B27")->applyFromArray(
                [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ]
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A28:B28")->applyFromArray(
                [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ]
                    ]
                ]
        );
        //Custom Code For 1st Sheet.... Excel Formating Sheet 1.
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(46);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);

        $spreadsheet->getActiveSheet()->getStyle('A17:A20')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A2', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A10:B10')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A13')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('B13')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A15:D15')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        // $spreadsheet->getActiveSheet()->getStyle('B15:D15')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A26')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('C17')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('C17')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('B26')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('C15', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A23', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A32', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('B25', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A26', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A23', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('B26', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D17:D19')->getNumberFormat()->setFormatCode('#,##0');

        //$spreadsheet->getActiveSheet()->getRowDimension('A2')->setRowHeight(100);
        $spreadsheet->getActiveSheet()->getRowDimension('2')->setRowHeight(45);
        $spreadsheet->getActiveSheet()->getStyle('A13')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getRowDimension('32')->setRowHeight(40);
        $spreadsheet->getActiveSheet()->getRowDimension('26')->setRowHeight(70);
        $spreadsheet->getActiveSheet()->getRowDimension('23')->setRowHeight(30);
        $spreadsheet->getActiveSheet()->getStyle('A25:C15')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('C25', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->mergeCells('C9:D9');
//$spreadsheet->->getActiveSheet()->removeRow(13);
// $spreadsheet->getActiveSheet()->mergeCells('B11:D13');

        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
        //$objPHPExcel->getActiveSheet()->getPageSetup()->setPrintArea('A1:D29');
        $spreadsheet->getActiveSheet()->getPageMargins()->setTop(2.2);
        $spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.5);
        $spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.5);
        $spreadsheet->getActiveSheet()->getPageMargins()->setBottom(0.5);

        // Rename first worksheet
        $spreadsheet->getActiveSheet()->setTitle('Invoice Letter');


/////////////////////////////////////////Start of Sheet2///////////////////////////////////////////////////////////////////////////////////////
        // Create a new worksheet, after the default sheet

        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(1);

        if ($invoicedata->invoice_type == 2) {
            $spreadsheet->getActiveSheet()->setCellValue('A1', "$invoicedata->TenderDetails");
            $spreadsheet->getActiveSheet()->mergeCells('A1:L1');
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(20);

            //Custom Code By Asheesh.. For Project Start Date 08-02-2019.
            $getprojStartDateByProjID = $this->getprojStartDateByProjID($invoicedata->project_id);
            $spreadsheet->getActiveSheet()->setCellValue('A2', "Date of Start  : $getprojStartDateByProjID");
            $spreadsheet->getActiveSheet()->mergeCells('A2:B2');

            //Custom Code By Asheesh For Date Formate.. 08-02-2019
            $DDatestring = "$invoicedate first day of last month";
            $eVdate = date("d-m-Y", strtotime($DDatestring));
            //2 Two..
            $DDatestring2 = "$eVdate last day of this month";
            $eVdate2 = date("d-m-Y", strtotime($DDatestring2));
            $spreadsheet->getActiveSheet()->setCellValue('C2', "Duration Claimed in this Bill : $eVdate to $eVdate2");
            $spreadsheet->getActiveSheet()->mergeCells('C2:J2');

            $spreadsheet->getActiveSheet()->setCellValue('K2', "Dated $invoicedate");
            $spreadsheet->getActiveSheet()->mergeCells('K2:L2');

            $spreadsheet->getActiveSheet()->setCellValue('A3', "GST No.: $invoicedata->gstn_no");
            $spreadsheet->getActiveSheet()->mergeCells('A3:B3');

            $spreadsheet->getActiveSheet()->setCellValue('C3', "INVOICE No.: $invoicedata->ra_invoice");
            $spreadsheet->getActiveSheet()->mergeCells('C3:J3');

            $spreadsheet->getActiveSheet()->setCellValue('K3', "PAN No.: $invoicedata->pan_no");
            $spreadsheet->getActiveSheet()->mergeCells('K3:L3');

            $spreadsheet->getActiveSheet()->setCellValue('A4', "S.N.");
            $spreadsheet->getActiveSheet()->mergeCells('A4:A5');

            $spreadsheet->getActiveSheet()->setCellValue('B4', "Description");
            $spreadsheet->getActiveSheet()->mergeCells('B4:B5');

            $spreadsheet->getActiveSheet()->setCellValue('C4', 'Contract Value');
            $spreadsheet->getActiveSheet()->mergeCells('C4:D4');
            $spreadsheet->getActiveSheet()->setCellValue('C5', ' % of Work');
            $spreadsheet->getActiveSheet()->setCellValue('D5', 'Amount');

            $spreadsheet->getActiveSheet()->setCellValue('E4', 'Cumulative up to Previous Bill ');
            $spreadsheet->getActiveSheet()->mergeCells('E4:F4');
            $spreadsheet->getActiveSheet()->setCellValue('E5', ' % of Work');
            $spreadsheet->getActiveSheet()->setCellValue('F5', 'Amount');

            $spreadsheet->getActiveSheet()->setCellValue('G4', 'Current Month');
            $spreadsheet->getActiveSheet()->mergeCells('G4:H4');
            $spreadsheet->getActiveSheet()->setCellValue('G5', ' % of Work');
            $spreadsheet->getActiveSheet()->setCellValue('H5', 'Amount');

            $spreadsheet->getActiveSheet()->setCellValue('I4', 'Cumulative Balance');
            $spreadsheet->getActiveSheet()->mergeCells('I4:J4');
            $spreadsheet->getActiveSheet()->setCellValue('I5', ' % of Work');
            $spreadsheet->getActiveSheet()->setCellValue('J5', 'Amount');

            $spreadsheet->getActiveSheet()->setCellValue('K4', 'Balance');
            $spreadsheet->getActiveSheet()->mergeCells('K4:L4');
            $spreadsheet->getActiveSheet()->setCellValue('K5', ' % of Work');
            $spreadsheet->getActiveSheet()->setCellValue('L5', 'Amount');

            //12 ... Rows..
            $spreadsheet->getActiveSheet()->setCellValue('A6', "1");
            $spreadsheet->getActiveSheet()->setCellValue('B6', "Remuneration of Local Professional Staff");
            $spreadsheet->getActiveSheet()->setCellValue('C6', "");
            $spreadsheet->getActiveSheet()->setCellValue('D6', "$totalsummrycontract");
            $spreadsheet->getActiveSheet()->setCellValue('E6', "");
            // $spreadsheet->getActiveSheet()->setCellValue('F6', "$cumpreamt");
            //Code Comment by ash 18-01-2019.. For Padi Dahod isuee
            $spreadsheet->getActiveSheet()->setCellValue('F6', "$totalsummrycumulativeamount");

            $spreadsheet->getActiveSheet()->setCellValue('G6', "");
            $spreadsheet->getActiveSheet()->setCellValue('H6', "$cumamt");
            $spreadsheet->getActiveSheet()->setCellValue('I6', "");
            $spreadsheet->getActiveSheet()->setCellValue('J6', "$totalmmamount");
            $spreadsheet->getActiveSheet()->setCellValue('K6', "");
            $spreadsheet->getActiveSheet()->setCellValue('L6', "$keyamt");

            $spreadsheet->getActiveSheet()->setCellValue('A7', "2");
            $spreadsheet->getActiveSheet()->setCellValue('B7', "Supporting Staff");
            $spreadsheet->getActiveSheet()->setCellValue('C7', "");
            $spreadsheet->getActiveSheet()->setCellValue('D7', "$summerytotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('E7', "");
            //ash 21-01 code By Asheesh...
            // $spreadsheet->getActiveSheet()->setCellValue('F7', "$summerytotalcumulativeamounts");
            //Comment By Asheesh For Percentage Issue..
            $spreadsheet->getActiveSheet()->setCellValue('F7', "$subcumpreamt");
            $spreadsheet->getActiveSheet()->setCellValue('G7', "");
            $spreadsheet->getActiveSheet()->setCellValue('H7', "$subcumamt");
            $spreadsheet->getActiveSheet()->setCellValue('I7', "");
            $spreadsheet->getActiveSheet()->setCellValue('J7', "$subtotalmmamount");
            $spreadsheet->getActiveSheet()->setCellValue('K7', "");
            $spreadsheet->getActiveSheet()->setCellValue('L7', "$subkeyamt");

            $spreadsheet->getActiveSheet()->setCellValue('A8', "3");
            $spreadsheet->getActiveSheet()->setCellValue('B8', "Transportation");
            $spreadsheet->getActiveSheet()->setCellValue('C8', "100%");
            $spreadsheet->getActiveSheet()->setCellValue('D8', "$summerytranstotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('E8', "$transcumpremm%");
            $spreadsheet->getActiveSheet()->setCellValue('F8', "$transcumpreamt");
            $spreadsheet->getActiveSheet()->setCellValue('G8', "$transcurmm%");
            $spreadsheet->getActiveSheet()->setCellValue('H8', "$transcumamt");
            $spreadsheet->getActiveSheet()->setCellValue('I8', "$transtotalmmcum%");
            $spreadsheet->getActiveSheet()->setCellValue('J8', "$transtotalmmamount");
            $spreadsheet->getActiveSheet()->setCellValue('K8', "$transkeyb%");
            $spreadsheet->getActiveSheet()->setCellValue('L8', "$transkeyamt");

            $spreadsheet->getActiveSheet()->setCellValue('A9', "4");
            $spreadsheet->getActiveSheet()->setCellValue('B9', "Duty Travel to Site");
            $spreadsheet->getActiveSheet()->setCellValue('C9', "100%");
            $spreadsheet->getActiveSheet()->setCellValue('D9', "$summerytraveltotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('E9', "$dutytrvlcumpremm%");
            $spreadsheet->getActiveSheet()->setCellValue('F9', "$dutytrvlcumpreamt");
            $spreadsheet->getActiveSheet()->setCellValue('G9', "$dutytrvlcurmm%");
            $spreadsheet->getActiveSheet()->setCellValue('H9', "$dutytrvlcumamt");
            $spreadsheet->getActiveSheet()->setCellValue('I9', "$dutytrvltotalmmcum%");
            $spreadsheet->getActiveSheet()->setCellValue('J9', "$dutytrvltotalmmamount");
            $spreadsheet->getActiveSheet()->setCellValue('K9', "$dutytrvlkeyb%");
            $spreadsheet->getActiveSheet()->setCellValue('L9', "$dutytrvlkeyamt");

            $spreadsheet->getActiveSheet()->setCellValue('A10', "5");
            $spreadsheet->getActiveSheet()->setCellValue('B10', "Office Rent");
            $spreadsheet->getActiveSheet()->setCellValue('C10', "100%");
            $spreadsheet->getActiveSheet()->setCellValue('D10', "$summeryofficerenttotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('E10', "$officerentcumpremm%");
            $spreadsheet->getActiveSheet()->setCellValue('F10', "$officerentcumpreamt");
            $spreadsheet->getActiveSheet()->setCellValue('G10', "$officerentcurmm%");
            $spreadsheet->getActiveSheet()->setCellValue('H10', "$officerentcumamt");
            $spreadsheet->getActiveSheet()->setCellValue('I10', "$officerenttotalmmcum%");
            $spreadsheet->getActiveSheet()->setCellValue('J10', "$officerenttotalmmamount");
            $spreadsheet->getActiveSheet()->setCellValue('K10', "$officerentkeyb%");
            $spreadsheet->getActiveSheet()->setCellValue('L10', "$officerentkeyamt");

            $spreadsheet->getActiveSheet()->setCellValue('A11', "6");
            $spreadsheet->getActiveSheet()->setCellValue('B11', "Office Supplies, Utilities & Communication");
            $spreadsheet->getActiveSheet()->setCellValue('C11', "100%");
            $spreadsheet->getActiveSheet()->setCellValue('D11', "$summerysuppliestotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('E11', "$officesuplcumpremm%");
            $spreadsheet->getActiveSheet()->setCellValue('F11', "$officesuplcumpreamt");
            $spreadsheet->getActiveSheet()->setCellValue('G11', "$officesuplcurmm%");
            $spreadsheet->getActiveSheet()->setCellValue('H11', "$officesuplcumamt");
            $spreadsheet->getActiveSheet()->setCellValue('I11', "$officesupltotalmmcum%");
            $spreadsheet->getActiveSheet()->setCellValue('J11', "$officesupltotalmmamount");
            $spreadsheet->getActiveSheet()->setCellValue('K11', "$officesuplkeyb%");
            $spreadsheet->getActiveSheet()->setCellValue('L11', "$officesuplkeyamt");

            $spreadsheet->getActiveSheet()->setCellValue('A12', "7");
            $spreadsheet->getActiveSheet()->setCellValue('B12', "Office Furniture & Equipment");
            $spreadsheet->getActiveSheet()->setCellValue('C12', "100%");
            $spreadsheet->getActiveSheet()->setCellValue('D12', "$summeryfurnituretotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('E12', "$officefurncumpremm%");
            $spreadsheet->getActiveSheet()->setCellValue('F12', "$officefurncumpreamt");
            $spreadsheet->getActiveSheet()->setCellValue('G12', "$officefurncurmm%");
            $spreadsheet->getActiveSheet()->setCellValue('H12', "$officefurncumamt");
            $spreadsheet->getActiveSheet()->setCellValue('I12', "$officefurntotalmmcum%");
            $spreadsheet->getActiveSheet()->setCellValue('J12', "$officefurntotalmmamount");
            $spreadsheet->getActiveSheet()->setCellValue('K12', "$officefurnkeyb%");
            $spreadsheet->getActiveSheet()->setCellValue('L12', "$officefurnkeyamt");

            $spreadsheet->getActiveSheet()->setCellValue('A13', "8");
            $spreadsheet->getActiveSheet()->setCellValue('B13', "Office Equipment");
            $spreadsheet->getActiveSheet()->setCellValue('C13', "100%");
            $spreadsheet->getActiveSheet()->setCellValue('D13', "$summeryequiptotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('E13', "$officeequicumpremm%");
            $spreadsheet->getActiveSheet()->setCellValue('F13', "$officeequicumpreamt");
            $spreadsheet->getActiveSheet()->setCellValue('G13', "$officeequicurmm%");
            $spreadsheet->getActiveSheet()->setCellValue('H13', "$officeequicumamt");
            $spreadsheet->getActiveSheet()->setCellValue('I13', "$officeequitotalmmcum%");
            $spreadsheet->getActiveSheet()->setCellValue('J13', "$officeequitotalmmamount");
            $spreadsheet->getActiveSheet()->setCellValue('K13', "$officeequikeyb%");
            $spreadsheet->getActiveSheet()->setCellValue('L13', "$officeequikeyamt");

            $spreadsheet->getActiveSheet()->setCellValue('A14', "9");
            $spreadsheet->getActiveSheet()->setCellValue('B14', "Reports & Document Printing");
            $spreadsheet->getActiveSheet()->setCellValue('C14', "100%");
            $spreadsheet->getActiveSheet()->setCellValue('D14', "$summeryreporttotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('E14', "$reportsdoccumpremm%");
            $spreadsheet->getActiveSheet()->setCellValue('F14', "$reportsdoccumpreamt");
            $spreadsheet->getActiveSheet()->setCellValue('G14', "$reportsdoccurmm%");
            $spreadsheet->getActiveSheet()->setCellValue('H14', "$reportsdoccumamt");
            $spreadsheet->getActiveSheet()->setCellValue('I14', "$reportsdoctotalmmcum%");
            $spreadsheet->getActiveSheet()->setCellValue('J14', "$reportsdoctotalmmamount");
            $spreadsheet->getActiveSheet()->setCellValue('K14', "$reportsdockeyb%");
            $spreadsheet->getActiveSheet()->setCellValue('L14', "$reportsdockeyamt");

            $spreadsheet->getActiveSheet()->setCellValue('A15', "10");
            $spreadsheet->getActiveSheet()->setCellValue('B15', "Survey Equipment with Survey Party and Vehicle");
            $spreadsheet->getActiveSheet()->setCellValue('C15', "100%");
            $spreadsheet->getActiveSheet()->setCellValue('D15', "$summerysurvey_equtotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('E15', "$surveyequicumpremm%");
            $spreadsheet->getActiveSheet()->setCellValue('F15', "$surveyequicumpreamt");
            $spreadsheet->getActiveSheet()->setCellValue('G15', "$surveyequicurmm%");
            $spreadsheet->getActiveSheet()->setCellValue('H15', "$surveyequicumamt");
            $spreadsheet->getActiveSheet()->setCellValue('I15', "$surveyequitotalmmcum%");
            $spreadsheet->getActiveSheet()->setCellValue('J15', "$surveyequitotalmmamount");
            $spreadsheet->getActiveSheet()->setCellValue('K15', "$surveyequikeyb%");
            $spreadsheet->getActiveSheet()->setCellValue('L15', "$surveyequikeyamt");

            $spreadsheet->getActiveSheet()->setCellValue('A16', "11");
            $spreadsheet->getActiveSheet()->setCellValue('B16', "Road Survey Equipment");
            $spreadsheet->getActiveSheet()->setCellValue('C16', "100%");
            $spreadsheet->getActiveSheet()->setCellValue('D16', "$summeryroad_survey_equtotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('E16', "$roadsurveyeqcumpremm%");
            $spreadsheet->getActiveSheet()->setCellValue('F16', "$roadsurveyeqcumpreamt");
            $spreadsheet->getActiveSheet()->setCellValue('G16', "$roadsurveyeqcurmm%");
            $spreadsheet->getActiveSheet()->setCellValue('H16', "$roadsurveyeqcumamt");
            $spreadsheet->getActiveSheet()->setCellValue('I16', "$roadsurveyeqtotalmmcum%");
            $spreadsheet->getActiveSheet()->setCellValue('J16', "$roadsurveyeqtotalmmamount");
            $spreadsheet->getActiveSheet()->setCellValue('K16', "$roadsurveyeqkeyb%");
            $spreadsheet->getActiveSheet()->setCellValue('L16', "$roadsurveyeqkeyamt");

            $spreadsheet->getActiveSheet()->setCellValue('A17', "12");
            $spreadsheet->getActiveSheet()->setCellValue('B17', "Contingencies");
            $spreadsheet->getActiveSheet()->setCellValue('C17', "100%");
            $spreadsheet->getActiveSheet()->setCellValue('D17', "$summarycontingenciestotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('E17', "$contcumpremm%");
            $spreadsheet->getActiveSheet()->setCellValue('F17', "$contcumpreamt");
            $spreadsheet->getActiveSheet()->setCellValue('G17', "$contcurmm%");
            $spreadsheet->getActiveSheet()->setCellValue('H17', "$contcumamt");
            $spreadsheet->getActiveSheet()->setCellValue('I17', "$conttotalmmcum%");
            $spreadsheet->getActiveSheet()->setCellValue('J17', "$conttotalmmamount");
            $spreadsheet->getActiveSheet()->setCellValue('K17', "$contkeyb%");
            $spreadsheet->getActiveSheet()->setCellValue('L17', "$contkeyamt");

            //08-02-2018. code asheesh
            $sumValofTot = ($contcumpreamt + $roadsurveyeqcumpreamt + $surveyequicumpreamt + $reportsdoccumpreamt + $officeequicumpreamt + $officefurncumpreamt + $officesuplcumpreamt + $officerentcumpreamt + $dutytrvlcumpreamt + $transcumpreamt + $summerytotalcumulativeamounts + $totalsummrycumulativeamount);

            $spreadsheet->getActiveSheet()->setCellValue('A18', "");
            $spreadsheet->getActiveSheet()->setCellValue('B18', "Sub Total : (1 to 12)");
            $spreadsheet->getActiveSheet()->setCellValue('C18', "");
            $spreadsheet->getActiveSheet()->setCellValue('D18', "$summerysubtotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('E18', "");
            $spreadsheet->getActiveSheet()->setCellValue('F18', "$sumValofTot");
            $spreadsheet->getActiveSheet()->setCellValue('G18', "");
            $spreadsheet->getActiveSheet()->setCellValue('H18', "$summerysubtotalpercur");
            $spreadsheet->getActiveSheet()->setCellValue('I18', "");
            $spreadsheet->getActiveSheet()->setCellValue('J18', "$summerysubtotalpercumtotal");
            $spreadsheet->getActiveSheet()->setCellValue('K18', "");
            $spreadsheet->getActiveSheet()->setCellValue('L18', "$summerysubtotalperbalance");

            $esj = 19;
            $escpertotal = 0;
            $esccurtotal = 0;
            $esctotal = 0;
            for ($i = 0; $i < $escalationID; $i++) {
                $escalation_name = $escalation[$i]->escalation_name;
                $esc_pretotal = $escalation[$i]->esc_pretotal;
                //Code By Asheesh 09-02-2019.
//                if ($escalation[$i]->esc_percentage > 0) {
//                    $esc_pretotal = ($sumValofTot * $escalation[$i]->esc_percentage / 100);
//                }
//                Percentage (%) Invoice Esscalation
                $perccurtotal = $escalation[$i]->perccurtotal;
                $totalamount = $escalation[$i]->totalamount;
                $spreadsheet->getActiveSheet()->setCellValue("A$esj", "Add :");
                $spreadsheet->getActiveSheet()->setCellValue("B$esj", "$escalation_name");
                $spreadsheet->getActiveSheet()->setCellValue("C$esj", "-");
                $spreadsheet->getActiveSheet()->setCellValue("D$esj", "-");
                $spreadsheet->getActiveSheet()->setCellValue("E$esj", "-");
                $spreadsheet->getActiveSheet()->setCellValue("F$esj", "$esc_pretotal");
                $spreadsheet->getActiveSheet()->setCellValue("G$esj", "-");
                $spreadsheet->getActiveSheet()->setCellValue("H$esj", "$perccurtotal");
                $spreadsheet->getActiveSheet()->setCellValue("I$esj", "-");
                $spreadsheet->getActiveSheet()->setCellValue("J$esj", "$totalamount");
                $spreadsheet->getActiveSheet()->setCellValue("K$esj", "");
                $spreadsheet->getActiveSheet()->setCellValue("L$esj", "");
                $esj++;

                $escpertotal += $esc_pretotal;
                $esccurtotal += $perccurtotal;
                $esctotal += $totalamount;
            }
            $nextesj = $esj;
            //Code Edited By Asheesh 09-02-2019.
            $totalcu = $escpertotal + $sumValofTot;
            $totalcur = $perccurtotal + $summerysubtotalpercur;
            $totalamts = $totalamount + $summerysubtotalpercumtotal;

            $spreadsheet->getActiveSheet()->setCellValue("A$nextesj", "");
            $spreadsheet->getActiveSheet()->setCellValue("B$nextesj", "Total (Rs.)");
            $spreadsheet->getActiveSheet()->setCellValue("C$nextesj", "-");
            $spreadsheet->getActiveSheet()->setCellValue("D$nextesj", "-");
            $spreadsheet->getActiveSheet()->setCellValue("E$nextesj", "-");
            $spreadsheet->getActiveSheet()->setCellValue("F$nextesj", "$totalcu");
            $spreadsheet->getActiveSheet()->setCellValue("G$nextesj", "-");
            $spreadsheet->getActiveSheet()->setCellValue("H$nextesj", "$totalcur");
            $spreadsheet->getActiveSheet()->setCellValue("I$nextesj", "-");
            $spreadsheet->getActiveSheet()->setCellValue("J$nextesj", "$totalamts");
            $spreadsheet->getActiveSheet()->setCellValue("K$nextesj", "-");
            $spreadsheet->getActiveSheet()->setCellValue("L$nextesj", "-");
            $spreadsheet->getActiveSheet()->getStyle("A$nextesj:L$nextesj")->applyFromArray(
                    [
                        'font' => [
                            'bold' => true,
                        ]
                    ]
            );

            $taxj = $nextesj + 1;
            $tax1 = 0;
            $tax2 = 0;
            $tax3 = 0;
            $tax4 = 0;
            for ($i = 0; $i < $projtaxID; $i++) {
                $tax_name = $projtax[$i]->tax_name;
                $contractamount = $projtax[$i]->contractamount;
                $amount = $projtax[$i]->amount;
                $currenttaxtotal = $projtax[$i]->currenttaxtotal;
                $totaltaxcum = $projtax[$i]->totaltaxcum;

                $spreadsheet->getActiveSheet()->setCellValue("A$taxj", "Add : ");
                $spreadsheet->getActiveSheet()->setCellValue("B$taxj", "$tax_name");
                $spreadsheet->getActiveSheet()->setCellValue("C$taxj", "");
                $spreadsheet->getActiveSheet()->setCellValue("D$taxj", "$contractamount");
                $spreadsheet->getActiveSheet()->setCellValue("E$taxj", "");
                $spreadsheet->getActiveSheet()->setCellValue("F$taxj", "$amount");
                $spreadsheet->getActiveSheet()->setCellValue("G$taxj", "");
                $spreadsheet->getActiveSheet()->setCellValue("H$taxj", "$currenttaxtotal");
                $spreadsheet->getActiveSheet()->setCellValue("I$taxj", "");
                $spreadsheet->getActiveSheet()->setCellValue("J$taxj", "$totaltaxcum");
                $spreadsheet->getActiveSheet()->setCellValue("K$taxj", "");
                $spreadsheet->getActiveSheet()->setCellValue("L$taxj", "");
                $taxj++;

                $tax1 += $contractamount;
                $tax2 += $amount;
                $tax3 += $currenttaxtotal;
                $tax4 += $totaltaxcum;
            }

            $grandtotalcontract = $summerysubtotalcontract + $tax1;
            $grandtotalcumpre = $totalcu + $tax2;
            $grandtotalcur = $totalcur + $tax3;
            $grandtotalcum = $totalamts + $tax4;

            $spreadsheet->getActiveSheet()->setCellValue("A$taxj", "");
            $spreadsheet->getActiveSheet()->setCellValue("B$taxj", "Grand Total  (Rs.)");
            $spreadsheet->getActiveSheet()->setCellValue("C$taxj", "-");
            $spreadsheet->getActiveSheet()->setCellValue("D$taxj", "$grandtotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue("E$taxj", "-");
            $spreadsheet->getActiveSheet()->setCellValue("F$taxj", "$grandtotalcumpre");
            $spreadsheet->getActiveSheet()->setCellValue("G$taxj", "-");
            $spreadsheet->getActiveSheet()->setCellValue("H$taxj", "$grandtotalcur");
            $spreadsheet->getActiveSheet()->setCellValue("I$taxj", "-");
            $spreadsheet->getActiveSheet()->setCellValue("J$taxj", "$grandtotalcum");
            $spreadsheet->getActiveSheet()->setCellValue("K$taxj", "-");
            $spreadsheet->getActiveSheet()->setCellValue("L$taxj", "-");

            $spreadsheet->getActiveSheet()->getStyle('A1:L5')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $spreadsheet->getActiveSheet()->getStyle("A1:L5")->applyFromArray(
                    [
                        'font' => [
                            'bold' => true,
                        ],
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => Border::BORDER_THIN,
                                'color' => ['argb' => 'FF000000'],
                            ],
                        ]
                    ]
            );
            $spreadsheet->getActiveSheet()->getStyle("A$taxj:L$taxj")->applyFromArray(
                    [
                        'font' => [
                            'bold' => true,
                        ]
                    ]
            );

            $spreadsheet->getActiveSheet()->getStyle("A7:A$taxj")->applyFromArray(
                    [
                        'font' => [
                            'bold' => true,
                        ]
                    ]
            );

            $spreadsheet->getActiveSheet()->getStyle("A18:L18")->applyFromArray(
                    [
                        'font' => [
                            'bold' => true,
                        ]
                    ]
            );

            $spreadsheet->getActiveSheet()->getStyle("A1:L$taxj")->applyFromArray(
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => Border::BORDER_THIN,
                                'color' => ['argb' => 'FF000000'],
                            ],
                        ]
                    ]
            );
        } else {
            $spreadsheet->getActiveSheet()->setCellValue('A1', 'INDEPENDENT ENGINEER SERVICES');
            $spreadsheet->getActiveSheet()->mergeCells('A1:G1');
            //$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);

            $spreadsheet->getActiveSheet()->setCellValue('A2', "$invoicedata->TenderDetails");
            $spreadsheet->getActiveSheet()->mergeCells('A2:G2');

            //Custom Code By Asheesh.. For Project Start Date 08-02-2019.
            $getprojStartDateByProjID = $this->getprojStartDateByProjID($invoicedata->project_id);

            $spreadsheet->getActiveSheet()->setCellValue('A3', "Date of Start  : $getprojStartDateByProjID");
            $spreadsheet->getActiveSheet()->mergeCells('A3:B3');

            //Custom Code By Asheesh For Date Formate.. 08-02-2019
            $DDatestring = "$invoicedate first day of last month";
            $eVdate = date("d-m-Y", strtotime($DDatestring));
            //2 Two..
            $DDatestring2 = "$eVdate last day of this month";
            $eVdate2 = date("d-m-Y", strtotime($DDatestring2));

            $spreadsheet->getActiveSheet()->setCellValue('C3', "Duration Claimed in this Bill : $eVdate to $eVdate2");
            $spreadsheet->getActiveSheet()->mergeCells('C3:E3');

            $spreadsheet->getActiveSheet()->setCellValue('F3', "Dated $invoicedate");
            $spreadsheet->getActiveSheet()->mergeCells('F3:G3');

            $spreadsheet->getActiveSheet()->setCellValue('A4', "GST No.: $invoicedata->gstn_no");
            $spreadsheet->getActiveSheet()->mergeCells('A4:B4');

            $spreadsheet->getActiveSheet()->setCellValue('C4', "RA INVOICE No.: $invoicedata->ra_invoice");
            $spreadsheet->getActiveSheet()->mergeCells('C4:E4');

            $spreadsheet->getActiveSheet()->setCellValue('F4', "PAN No.: $invoicedata->pan_no");
            $spreadsheet->getActiveSheet()->mergeCells('F4:G4');

            $spreadsheet->getActiveSheet()->setCellValue('A5', "S.N.");
            $spreadsheet->getActiveSheet()->mergeCells('A5:A6');

            $spreadsheet->getActiveSheet()->setCellValue('B5', "Description");
            $spreadsheet->getActiveSheet()->mergeCells('B5:B6');

            $spreadsheet->getActiveSheet()->setCellValue('C5', "Contract Value");
            $spreadsheet->getActiveSheet()->setCellValue('C6', "Rs.");

            $spreadsheet->getActiveSheet()->setCellValue('D5', "Cumulative up to Previous Bill");
            $spreadsheet->getActiveSheet()->setCellValue('D6', "Rs.");

            $spreadsheet->getActiveSheet()->setCellValue('E5', "Current Month Bill");
            $spreadsheet->getActiveSheet()->setCellValue('E6', "Rs.");

            $spreadsheet->getActiveSheet()->setCellValue('F5', "Total Cumulative");
            $spreadsheet->getActiveSheet()->setCellValue('F6', "Rs.");

            $spreadsheet->getActiveSheet()->setCellValue('G5', "Balance");
            $spreadsheet->getActiveSheet()->setCellValue('G6', "Rs.");

            $spreadsheet->getActiveSheet()->setCellValue('A7', "1");
            $spreadsheet->getActiveSheet()->setCellValue('B7', "Remuneration of Local Professional Staff");
            $spreadsheet->getActiveSheet()->setCellValue('C7', "$totalsummrycontract");
            $spreadsheet->getActiveSheet()->setCellValue('D7', "$totalsummrycumulativeamount");
            $spreadsheet->getActiveSheet()->setCellValue('E7', "$totalsummryamount");
            $spreadsheet->getActiveSheet()->setCellValue('F7', "$totalsummrycumulativeamountfull");
            $spreadsheet->getActiveSheet()->setCellValue('G7', "$totalsummrybalanceamount");

            // echo $totalsummrycumulativeamountfull; die;
            //Ash1..
            $inserTr = array('invoice_id' => $invoiceID, 'srpos_no' => '1', 'project_id' => $invoicedata->project_id, 'description' => 'Remuneration of Local Professional Staff',
                'contract_value' => $totalsummrycontract, 'cumulative_up_previous_bill' => $totalsummrycumulativeamount,
                'current_month_bill' => $totalsummryamount,
                'total_cumulative' => $totalsummrycumulativeamountfull,
                'balance' => $totalsummrybalanceamount, 'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '1', 'project_id' => $invoicedata->project_id));
            $num1 = $this->db->count_all_results('invoice_after_xlsdownload');
            if ($num1 < 1) {
                $this->db->insert('invoice_after_xlsdownload', $inserTr);
            } else {
                $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '1', 'project_id' => $invoicedata->project_id));
                $this->db->update('invoice_after_xlsdownload', array('description' => 'Remuneration of Local Professional Staff', 'contract_value' => $totalsummrycontract, 'cumulative_up_previous_bill' => $totalsummrycumulativeamount, 'current_month_bill' => $totalsummryamount, 'total_cumulative' => $totalsummrycumulativeamountfull, 'balance' => $totalsummrybalanceamount));
            }
            //Close Ash1 Code..

            $spreadsheet->getActiveSheet()->setCellValue('A8', "2");
            $spreadsheet->getActiveSheet()->setCellValue('B8', "Supporting Staff");
            $spreadsheet->getActiveSheet()->setCellValue('C8', "$summerytotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('D8', "$summerytotalcumulativeamounts");
            $spreadsheet->getActiveSheet()->setCellValue('E8', "$summerytotalmmamountkeys");
            $spreadsheet->getActiveSheet()->setCellValue('F8', "$summerytotalcumulativeamountfull");
            $spreadsheet->getActiveSheet()->setCellValue('G8', "$summerytotalkeybalanceamount");

            //Ash2..
            $inserTr = array('invoice_id' => $invoiceID, 'srpos_no' => '2', 'project_id' => $invoicedata->project_id, 'description' => 'Supporting Staff',
                'contract_value' => $summerytotalcontract, 'cumulative_up_previous_bill' => $summerytotalcumulativeamounts,
                'current_month_bill' => $summerytotalmmamountkeys, 'total_cumulative' => $summerytotalcumulativeamountfull, 'balance' => $summerytotalkeybalanceamount, 'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '2', 'project_id' => $invoicedata->project_id));
            $num1 = $this->db->count_all_results('invoice_after_xlsdownload');
            if ($num1 < 1) {
                $this->db->insert('invoice_after_xlsdownload', $inserTr);
            } else {
                $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '2', 'project_id' => $invoicedata->project_id));
                $this->db->update('invoice_after_xlsdownload', array(
                    'description' => 'Supporting Staff',
                    'contract_value' => $summerytotalcontract,
                    'cumulative_up_previous_bill' => $summerytotalcumulativeamounts,
                    'current_month_bill' => $summerytotalmmamountkeys,
                    'total_cumulative' => $summerytotalcumulativeamountfull,
                    'balance' => $summerytotalkeybalanceamount,
                    'entry_by' => $this->session->userdata('uid')));
            }
            //Close Ash2 Code..

            $spreadsheet->getActiveSheet()->setCellValue('A9', "3");
            $spreadsheet->getActiveSheet()->setCellValue('B9', "Transportation");
            $spreadsheet->getActiveSheet()->setCellValue('C9', "$summerytranstotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('D9', "$summerytranstotalcum");
            $spreadsheet->getActiveSheet()->setCellValue('E9', "$summerytranstotalcur");
            $spreadsheet->getActiveSheet()->setCellValue('F9', "$summerytranstotaltocum");
            $spreadsheet->getActiveSheet()->setCellValue('G9', "$summerytranstotalbal");

            //Ash3..
            $inserTr = array('invoice_id' => $invoiceID, 'srpos_no' => '3', 'project_id' => $invoicedata->project_id, 'description' => 'Transportation',
                'contract_value' => $summerytranstotalcontract,
                'cumulative_up_previous_bill' => $summerytranstotalcum,
                'current_month_bill' => $summerytranstotalcur,
                'total_cumulative' => $summerytranstotaltocum,
                'balance' => $summerytranstotalbal,
                'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '3', 'project_id' => $invoicedata->project_id));
            $num1 = $this->db->count_all_results('invoice_after_xlsdownload');
            if ($num1 < 1) {
                $this->db->insert('invoice_after_xlsdownload', $inserTr);
            } else {
                $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '3', 'project_id' => $invoicedata->project_id));
                $this->db->update('invoice_after_xlsdownload', array('description' => 'Transportation', 'contract_value' => $summerytranstotalcontract,
                    'cumulative_up_previous_bill' => $summerytranstotalcum,
                    'current_month_bill' => $summerytranstotalcur,
                    'total_cumulative' => $summerytranstotaltocum,
                    'balance' => $summerytranstotalbal,
                    'entry_by' => $this->session->userdata('uid')));
            }
            //Close Ash3 Code..

            $spreadsheet->getActiveSheet()->setCellValue('A10', "4");
            $spreadsheet->getActiveSheet()->setCellValue('B10', "Duty Travel to Site");
            $spreadsheet->getActiveSheet()->setCellValue('C10', "$summerytraveltotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('D10', "$summerytraveltotalcum");
            $spreadsheet->getActiveSheet()->setCellValue('E10', "$summerytraveltotalcur");
            $spreadsheet->getActiveSheet()->setCellValue('F10', "$summerytraveltotaltocum");
            $spreadsheet->getActiveSheet()->setCellValue('G10', "$summerytraveltotalbal");

            //Ash4..
            $inserTr = array('invoice_id' => $invoiceID, 'srpos_no' => '4', 'project_id' => $invoicedata->project_id, 'description' => 'Duty Travel to Site',
                'contract_value' => $summerytraveltotalcontract,
                'cumulative_up_previous_bill' => $summerytraveltotalcum,
                'current_month_bill' => $summerytraveltotalcur,
                'total_cumulative' => $summerytraveltotaltocum,
                'balance' => $summerytraveltotalbal,
                'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '4', 'project_id' => $invoicedata->project_id));
            $num1 = $this->db->count_all_results('invoice_after_xlsdownload');
            if ($num1 < 1) {
                $this->db->insert('invoice_after_xlsdownload', $inserTr);
            } else {
                $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '4', 'project_id' => $invoicedata->project_id));
                $this->db->update('invoice_after_xlsdownload', array(
                    'description' => 'Duty Travel to Site',
                    'contract_value' => $summerytraveltotalcontract,
                    'cumulative_up_previous_bill' => $summerytraveltotalcum,
                    'current_month_bill' => $summerytraveltotalcur,
                    'total_cumulative' => $summerytraveltotaltocum,
                    'balance' => $summerytraveltotalbal,
                    'entry_by' => $this->session->userdata('uid')));
            }
            //Close Ash4 Code..

            $spreadsheet->getActiveSheet()->setCellValue('A11', "5");
            $spreadsheet->getActiveSheet()->setCellValue('B11', "Office Rent ");
            $spreadsheet->getActiveSheet()->setCellValue('C11', "$summeryofficerenttotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('D11', "$summeryofficerenttotalcum");
            $spreadsheet->getActiveSheet()->setCellValue('E11', "$summeryofficerenttotalcur");
            $spreadsheet->getActiveSheet()->setCellValue('F11', "$summeryofficerenttotaltocums");
            $spreadsheet->getActiveSheet()->setCellValue('G11', "$summeryofficerenttotalbal");

            //Ash5..
            $inserTr = array('invoice_id' => $invoiceID, 'srpos_no' => '5', 'project_id' => $invoicedata->project_id, 'description' => 'Office Rent',
                'contract_value' => $summeryofficerenttotalcontract,
                'cumulative_up_previous_bill' => $summeryofficerenttotalcum,
                'current_month_bill' => $summeryofficerenttotalcur,
                'total_cumulative' => $summeryofficerenttotaltocums,
                'balance' => $summeryofficerenttotalbal,
                'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '5', 'project_id' => $invoicedata->project_id));
            $num1 = $this->db->count_all_results('invoice_after_xlsdownload');
            if ($num1 < 1) {
                $this->db->insert('invoice_after_xlsdownload', $inserTr);
            } else {
                $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '5', 'project_id' => $invoicedata->project_id));
                $this->db->update('invoice_after_xlsdownload', array(
                    'description' => 'Office Rent',
                    'contract_value' => $summeryofficerenttotalcontract,
                    'cumulative_up_previous_bill' => $summeryofficerenttotalcum,
                    'current_month_bill' => $summeryofficerenttotalcur,
                    'total_cumulative' => $summeryofficerenttotaltocums,
                    'balance' => $summeryofficerenttotalbal,
                    'entry_by' => $this->session->userdata('uid')));
            }
            //Close Ash5 Code..

            $spreadsheet->getActiveSheet()->setCellValue('A12', "6");
            $spreadsheet->getActiveSheet()->setCellValue('B12', "Office Supplies, Utilities & Communication");
            $spreadsheet->getActiveSheet()->setCellValue('C12', "$summerysuppliestotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('D12', "$summerysuppliestotalcum");
            $spreadsheet->getActiveSheet()->setCellValue('E12', "$summerysuppliestotalcur");
            $spreadsheet->getActiveSheet()->setCellValue('F12', "$summerysuppliestotaltocum");
            $spreadsheet->getActiveSheet()->setCellValue('G12', "$summerysuppliestotalbal");

            //Ash6..
            $inserTr = array('invoice_id' => $invoiceID, 'srpos_no' => '6', 'project_id' => $invoicedata->project_id, 'description' => 'Office Supplies, Utilities & Communication',
                'contract_value' => $summerysuppliestotalcontract,
                'cumulative_up_previous_bill' => $summerysuppliestotalcum,
                'current_month_bill' => $summerysuppliestotalcur,
                'total_cumulative' => $summerysuppliestotaltocum,
                'balance' => $summerysuppliestotalbal,
                'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '6', 'project_id' => $invoicedata->project_id));
            $num1 = $this->db->count_all_results('invoice_after_xlsdownload');
            if ($num1 < 1) {
                $this->db->insert('invoice_after_xlsdownload', $inserTr);
            } else {
                $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '6', 'project_id' => $invoicedata->project_id));
                $this->db->update('invoice_after_xlsdownload', array(
                    'description' => 'Office Supplies, Utilities & Communication',
                    'contract_value' => $summerysuppliestotalcontract,
                    'cumulative_up_previous_bill' => $summerysuppliestotalcum,
                    'current_month_bill' => $summerysuppliestotalcur,
                    'total_cumulative' => $summerysuppliestotaltocum,
                    'balance' => $summerysuppliestotalbal,
                    'entry_by' => $this->session->userdata('uid')));
            }
            //Close Ash6 Code..
            $spreadsheet->getActiveSheet()->setCellValue('A13', "7");
            $spreadsheet->getActiveSheet()->setCellValue('B13', "Office Furniture & Equipment");
            $spreadsheet->getActiveSheet()->setCellValue('C13', "$summeryfurnituretotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('D13', "$summeryfurnituretotalcum");
            $spreadsheet->getActiveSheet()->setCellValue('E13', "$summeryfurnituretotalcur");
            $spreadsheet->getActiveSheet()->setCellValue('F13', "$summeryfurnituretotaltocum");
            $spreadsheet->getActiveSheet()->setCellValue('G13', "$summeryfurnituretotalbal");

            //Ash7..
            $inserTr = array('invoice_id' => $invoiceID, 'srpos_no' => '7', 'project_id' => $invoicedata->project_id, 'description' => 'Office Furniture & Equipment',
                'contract_value' => $summeryfurnituretotalcontract,
                'cumulative_up_previous_bill' => $summeryfurnituretotalcum,
                'current_month_bill' => $summeryfurnituretotalcur,
                'total_cumulative' => $summeryfurnituretotaltocum,
                'balance' => $summeryfurnituretotalbal,
                'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '7', 'project_id' => $invoicedata->project_id));
            $num1 = $this->db->count_all_results('invoice_after_xlsdownload');
            if ($num1 < 1) {
                $this->db->insert('invoice_after_xlsdownload', $inserTr);
            } else {
                $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '7', 'project_id' => $invoicedata->project_id));
                $this->db->update('invoice_after_xlsdownload', array(
                    'description' => 'Office Furniture & Equipment',
                    'contract_value' => $summeryfurnituretotalcontract,
                    'cumulative_up_previous_bill' => $summeryfurnituretotalcum,
                    'current_month_bill' => $summeryfurnituretotalcur,
                    'total_cumulative' => $summeryfurnituretotaltocum,
                    'balance' => $summeryfurnituretotalbal,
                    'entry_by' => $this->session->userdata('uid')));
            }
            //Close Ash7 Code...

            $spreadsheet->getActiveSheet()->setCellValue('A14', "8");
            $spreadsheet->getActiveSheet()->setCellValue('B14', "Office Equipment");
            $spreadsheet->getActiveSheet()->setCellValue('C14', "$summeryequiptotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('D14', "$summeryequiptotalcum");
            $spreadsheet->getActiveSheet()->setCellValue('E14', "$summeryequiptotalcur");
            $spreadsheet->getActiveSheet()->setCellValue('F14', "$summeryequiptotaltocum");
            $spreadsheet->getActiveSheet()->setCellValue('G14', "$summeryequiptotalbal");

            //Ash8..
            $inserTr = array('invoice_id' => $invoiceID, 'srpos_no' => '8', 'project_id' => $invoicedata->project_id, 'description' => 'Office Equipment',
                'contract_value' => $summeryequiptotalcontract,
                'cumulative_up_previous_bill' => $summeryequiptotalcum,
                'current_month_bill' => $summeryequiptotalcur,
                'total_cumulative' => $summeryequiptotaltocum,
                'balance' => $summeryequiptotalbal,
                'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '8', 'project_id' => $invoicedata->project_id));
            $num1 = $this->db->count_all_results('invoice_after_xlsdownload');
            if ($num1 < 1) {
                $this->db->insert('invoice_after_xlsdownload', $inserTr);
            } else {
                $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '8', 'project_id' => $invoicedata->project_id));
                $this->db->update('invoice_after_xlsdownload', array(
                    'description' => 'Office Equipment',
                    'contract_value' => $summeryequiptotalcontract,
                    'cumulative_up_previous_bill' => $summeryequiptotalcum,
                    'current_month_bill' => $summeryequiptotalcur,
                    'total_cumulative' => $summeryequiptotaltocum,
                    'balance' => $summeryequiptotalbal,
                    'entry_by' => $this->session->userdata('uid')));
            }
            //Close Ash8 Code...

            $spreadsheet->getActiveSheet()->setCellValue('A15', "9");
            $spreadsheet->getActiveSheet()->setCellValue('B15', "Reports & Document Printing");
            $spreadsheet->getActiveSheet()->setCellValue('C15', "$summeryreporttotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('D15', "$summeryreporttotalcum");
            $spreadsheet->getActiveSheet()->setCellValue('E15', "$summeryreporttotalcur");
            $spreadsheet->getActiveSheet()->setCellValue('F15', "$summeryreporttotaltocum");
            $spreadsheet->getActiveSheet()->setCellValue('G15', "$summeryreporttotalbal");

            //Ash9..
            $inserTr = array('invoice_id' => $invoiceID, 'srpos_no' => '9', 'project_id' => $invoicedata->project_id, 'description' => 'Reports & Document Printing',
                'contract_value' => $summeryreporttotalcontract,
                'cumulative_up_previous_bill' => $summeryreporttotalcum,
                'current_month_bill' => $summeryequiptotalcur,
                'total_cumulative' => $summeryreporttotaltocum,
                'balance' => $summeryreporttotalbal,
                'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '9', 'project_id' => $invoicedata->project_id));
            $num1 = $this->db->count_all_results('invoice_after_xlsdownload');
            if ($num1 < 1) {
                $this->db->insert('invoice_after_xlsdownload', $inserTr);
            } else {
                $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '9', 'project_id' => $invoicedata->project_id));
                $this->db->update('invoice_after_xlsdownload', array(
                    'description' => 'Reports & Document Printing',
                    'contract_value' => $summeryreporttotalcontract,
                    'cumulative_up_previous_bill' => $summeryreporttotalcum,
                    'current_month_bill' => $summeryreporttotalcur,
                    'total_cumulative' => $summeryreporttotaltocum,
                    'balance' => $summeryreporttotalbal,
                    'entry_by' => $this->session->userdata('uid')));
            }
            //Close Ash9 Code...

            $spreadsheet->getActiveSheet()->setCellValue('A16', "10");
            $spreadsheet->getActiveSheet()->setCellValue('B16', "Survey Equipment with Survey Party and Vehicle");
            $spreadsheet->getActiveSheet()->setCellValue('C16', "$summerysurvey_equtotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('D16', "$summerysurvey_equtotalcum");
            $spreadsheet->getActiveSheet()->setCellValue('E16', "$summerysurvey_equtotalcur");
            $spreadsheet->getActiveSheet()->setCellValue('F16', "$summerysurvey_equtotaltocum");
            $spreadsheet->getActiveSheet()->setCellValue('G16', "$summerysurvey_equtotalbal");

            //Ash10..
            $inserTr = array('invoice_id' => $invoiceID, 'srpos_no' => '10', 'project_id' => $invoicedata->project_id, 'description' => 'Survey Equipment with Survey Party and Vehicle',
                'contract_value' => $summerysurvey_equtotalcontract,
                'cumulative_up_previous_bill' => $summerysurvey_equtotalcum,
                'current_month_bill' => $summerysurvey_equtotalcur,
                'total_cumulative' => $summerysurvey_equtotaltocum,
                'balance' => $summerysurvey_equtotalbal,
                'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '10', 'project_id' => $invoicedata->project_id));
            $num1 = $this->db->count_all_results('invoice_after_xlsdownload');
            if ($num1 < 1) {
                $this->db->insert('invoice_after_xlsdownload', $inserTr);
            } else {
                $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '10', 'project_id' => $invoicedata->project_id));
                $this->db->update('invoice_after_xlsdownload', array(
                    'description' => 'Survey Equipment with Survey Party and Vehicle',
                    'contract_value' => $summerysurvey_equtotalcontract,
                    'cumulative_up_previous_bill' => $summerysurvey_equtotalcum,
                    'current_month_bill' => $summerysurvey_equtotalcur,
                    'total_cumulative' => $summerysurvey_equtotaltocum,
                    'balance' => $summerysurvey_equtotalbal,
                    'entry_by' => $this->session->userdata('uid')));
            }
            //Close Ash10 Code...

            $spreadsheet->getActiveSheet()->setCellValue('A17', "11");
            $spreadsheet->getActiveSheet()->setCellValue('B17', "Road Survey Equipment");
            $spreadsheet->getActiveSheet()->setCellValue('C17', "$summeryroad_survey_equtotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('D17', "$summeryroad_survey_equtotalcum");
            $spreadsheet->getActiveSheet()->setCellValue('E17', "$summeryroad_survey_equtotalcur");
            $spreadsheet->getActiveSheet()->setCellValue('F17', "$summeryroad_survey_equtotaltocum");
            $spreadsheet->getActiveSheet()->setCellValue('G17', "$summeryroad_survey_equtotalbal");

            //Ash11..
            $inserTr = array('invoice_id' => $invoiceID, 'srpos_no' => '11', 'project_id' => $invoicedata->project_id, 'description' => 'Road Survey Equipment',
                'contract_value' => $summeryroad_survey_equtotalcontract,
                'cumulative_up_previous_bill' => $summeryroad_survey_equtotalcum,
                'current_month_bill' => $summeryroad_survey_equtotalcur,
                'total_cumulative' => $summeryroad_survey_equtotaltocum,
                'balance' => $summeryroad_survey_equtotalbal,
                'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '11', 'project_id' => $invoicedata->project_id));
            $num1 = $this->db->count_all_results('invoice_after_xlsdownload');
            if ($num1 < 1) {
                $this->db->insert('invoice_after_xlsdownload', $inserTr);
            } else {
                $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '11', 'project_id' => $invoicedata->project_id));
                $this->db->update('invoice_after_xlsdownload', array(
                    'description' => 'Road Survey Equipment',
                    'contract_value' => $summeryroad_survey_equtotalcontract,
                    'cumulative_up_previous_bill' => $summeryroad_survey_equtotalcum,
                    'current_month_bill' => $summeryroad_survey_equtotalcur,
                    'total_cumulative' => $summeryroad_survey_equtotaltocum,
                    'balance' => $summeryroad_survey_equtotalbal,
                    'entry_by' => $this->session->userdata('uid')));
            }
            //Close Ash11 Code...

            $spreadsheet->getActiveSheet()->setCellValue('A18', "12");
            $spreadsheet->getActiveSheet()->setCellValue('B18', "Contingencies");
            $spreadsheet->getActiveSheet()->setCellValue('C18', "$summarycontingenciestotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('D18', "$summarycontingenciestotalcum");
            $spreadsheet->getActiveSheet()->setCellValue('E18', "$summarycontingenciestotalcur");
            $spreadsheet->getActiveSheet()->setCellValue('F18', "$summarycontingenciestotaltocum");
            $spreadsheet->getActiveSheet()->setCellValue('G18', "$summarycontingenciestotalbal");

            //Ash12..
            $inserTr = array('invoice_id' => $invoiceID, 'srpos_no' => '12', 'project_id' => $invoicedata->project_id, 'description' => 'Contingencies',
                'contract_value' => $summarycontingenciestotalcontract,
                'cumulative_up_previous_bill' => $summarycontingenciestotalcum,
                'current_month_bill' => $summarycontingenciestotalcur,
                'total_cumulative' => $summarycontingenciestotaltocum,
                'balance' => $summarycontingenciestotalbal,
                'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '12', 'project_id' => $invoicedata->project_id));
            $num1 = $this->db->count_all_results('invoice_after_xlsdownload');
            if ($num1 < 1) {
                $this->db->insert('invoice_after_xlsdownload', $inserTr);
            } else {
                $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => '12', 'project_id' => $invoicedata->project_id));
                $this->db->update('invoice_after_xlsdownload', array(
                    'description' => 'Contingencies',
                    'contract_value' => $summarycontingenciestotalcontract,
                    'cumulative_up_previous_bill' => $summarycontingenciestotalcum,
                    'current_month_bill' => $summarycontingenciestotalcur,
                    'total_cumulative' => $summarycontingenciestotaltocum,
                    'balance' => $summarycontingenciestotalbal,
                    'entry_by' => $this->session->userdata('uid')));
            }
            //Close Ash12 Code...
            $spreadsheet->getActiveSheet()->setCellValue('A19', "");
            $spreadsheet->getActiveSheet()->setCellValue('B19', "Sub Total : (1 to 12)");
            $spreadsheet->getActiveSheet()->setCellValue('C19', "$summerysubtotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue('D19', "$summerysubtotalcum");
            $spreadsheet->getActiveSheet()->setCellValue('E19', "$summerysubtotalcur");
            $spreadsheet->getActiveSheet()->setCellValue('F19', "$summerysubtotalcumtotal");
            $spreadsheet->getActiveSheet()->setCellValue('G19', "$summerysubtotalbalance");
            //Ash Sub Total..
            $inserTr = array('invoice_id' => $invoiceID, 'srpos_no' => 'sub_total', 'project_id' => $invoicedata->project_id, 'description' => 'Sub Total : (I to X)',
                'contract_value' => $summerysubtotalcontract,
                'cumulative_up_previous_bill' => $summerysubtotalcum,
                'current_month_bill' => $summerysubtotalcur,
                'total_cumulative' => $summerysubtotalcumtotal,
                'balance' => $summerysubtotalbalance,
                'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => 'sub_total', 'project_id' => $invoicedata->project_id));
            $num1 = $this->db->count_all_results('invoice_after_xlsdownload');
            if ($num1 < 1) {
                $this->db->insert('invoice_after_xlsdownload', $inserTr);
            } else {
                $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => 'sub_total', 'project_id' => $invoicedata->project_id));
                $this->db->update('invoice_after_xlsdownload', array(
                    'description' => 'Sub Total : (I to X)',
                    'contract_value' => $summerysubtotalcontract,
                    'cumulative_up_previous_bill' => $summerysubtotalcum,
                    'current_month_bill' => $summerysubtotalcur,
                    'total_cumulative' => $summerysubtotalcumtotal,
                    'balance' => $summerysubtotalbalance,
                    'entry_by' => $this->session->userdata('uid')));
            }


            //CloseSub Total. Code...
            $esj = 20;
            $escpertotal = 0;
            $esccurtotal = 0;
            $esctotal = 0;
            //echo '<pre>'; print_r($escalation); die;
            for ($i = 0; $i < $escalationID; $i++) {
                $escalation_name = $escalation[$i]->escalation_name;
                $esc_pretotal = $escalation[$i]->esc_pretotal;
                $perccurtotal = $escalation[$i]->perccurtotal;
                $totalamount = $escalation[$i]->totalamount;

                $spreadsheet->getActiveSheet()->setCellValue("A$esj", "Add : ");
                $spreadsheet->getActiveSheet()->setCellValue("B$esj", "$escalation_name");
                $spreadsheet->getActiveSheet()->setCellValue("C$esj", "-");
                $spreadsheet->getActiveSheet()->setCellValue("D$esj", "$esc_pretotal");
                $spreadsheet->getActiveSheet()->setCellValue("E$esj", "$perccurtotal");
                $spreadsheet->getActiveSheet()->setCellValue("F$esj", "$totalamount");
                $spreadsheet->getActiveSheet()->setCellValue("G$esj", "");
                $esj++;

                $escpertotal += $esc_pretotal;
                $esccurtotal += $perccurtotal;
                $esctotal += $totalamount;
            }


            $nextesj = $esj;
            $totalcu = $escpertotal + $summerysubtotalcum;
            //code Comment By Asheesh 28-05-2019.
            // $totalcur = $perccurtotal + $summerysubtotalcur;
            $totalcur = $esccurtotal + $summerysubtotalcur;
            $totalamts = $esctotal + $summerysubtotalcumtotal;

            //echo  $esccurtotal."-->>".$perccurtotal."-->".$summerysubtotalcur; die;

            $spreadsheet->getActiveSheet()->setCellValue("A$nextesj", "");
            $spreadsheet->getActiveSheet()->setCellValue("B$nextesj", "Total (Rs.)");
            $spreadsheet->getActiveSheet()->setCellValue("C$nextesj", "$summerysubtotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue("D$nextesj", "$totalcu");
            $spreadsheet->getActiveSheet()->setCellValue("E$nextesj", "$totalcur");
            $spreadsheet->getActiveSheet()->setCellValue("F$nextesj", "$totalamts");
            $spreadsheet->getActiveSheet()->setCellValue("G$nextesj", "-");

            //Total (Rs.) Ash Sub Total...
            $inserTr = array('invoice_id' => $invoiceID, 'srpos_no' => 'total_rs', 'project_id' => $invoicedata->project_id, 'description' => 'Total (Rs.)',
                'contract_value' => '0',
                'cumulative_up_previous_bill' => $totalcu,
                'current_month_bill' => $totalcur,
                'total_cumulative' => $totalamts,
                'balance' => '0',
                'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => 'total_rs', 'project_id' => $invoicedata->project_id));
            $num1 = $this->db->count_all_results('invoice_after_xlsdownload');
            if ($num1 < 1) {
                $this->db->insert('invoice_after_xlsdownload', $inserTr);
            } else {
                $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => 'total_rs', 'project_id' => $invoicedata->project_id));
                $this->db->update('invoice_after_xlsdownload', array(
                    'description' => 'Total (Rs.)',
                    'contract_value' => '0',
                    'cumulative_up_previous_bill' => $totalcu,
                    'current_month_bill' => $totalcur,
                    'total_cumulative' => $totalamts,
                    'balance' => '0',
                    'entry_by' => $this->session->userdata('uid')));
            }
            //Total (Rs.) CloseSub Total. Code...

            $spreadsheet->getActiveSheet()->getStyle("A$nextesj:G$nextesj")->applyFromArray(
                    [
                        'font' => [
                            'bold' => true,
                        ]
                    ]
            );

            $taxj = $nextesj + 1;
            $tax1 = 0;
            $tax2 = 0;
            $tax3 = 0;
            $tax4 = 0;


            for ($i = 0; $i < $projtaxID; $i++) {
                $tax_name = $projtax[$i]->tax_name;
                $contractamount = $projtax[$i]->contractamount;
                $amount = $projtax[$i]->amount;
                $currenttaxtotal = $projtax[$i]->currenttaxtotal;
                $totaltaxcum = $projtax[$i]->totaltaxcum;

                $spreadsheet->getActiveSheet()->setCellValue("A$taxj", "Add :");
                $spreadsheet->getActiveSheet()->setCellValue("B$taxj", "$tax_name");
                $spreadsheet->getActiveSheet()->setCellValue("C$taxj", "$contractamount");
                $spreadsheet->getActiveSheet()->setCellValue("D$taxj", "$amount");
                //Txt..
                $spreadsheet->getActiveSheet()->setCellValue("E$taxj", "$currenttaxtotal");
                $spreadsheet->getActiveSheet()->setCellValue("F$taxj", "$totaltaxcum");
                $spreadsheet->getActiveSheet()->setCellValue("G$taxj", "");
                $taxj++;

                $tax1 += $contractamount;
                $tax2 += $amount;
                $tax3 += $currenttaxtotal;
                $tax4 += $totaltaxcum;

                //Code Start 12-08-2019 "tax_after_xlsdownload" Code By Asheesh..
                $inserTaxr = array('invoice_id' => $invoiceID, 'project_id' => $projtax[$i]->project_id, 'tax_master_id' => $projtax[$i]->tax_id, 'current_month_tax' => $currenttaxtotal, 'entry_by' => $this->session->userdata('uid'));
                $this->db->where(array('invoice_id' => $invoiceID, 'project_id' => $projtax[$i]->project_id, 'status' => '1', 'tax_master_id' => $projtax[$i]->tax_id));
                $Taxnum = $this->db->count_all_results('tax_after_xlsdownload');
                if ($Taxnum < 1) {
                    $this->db->insert('tax_after_xlsdownload', $inserTaxr);
                } else {
                    $this->db->where(array('invoice_id' => $invoiceID, 'project_id' => $projtax[$i]->project_id, 'status' => '1', 'tax_master_id' => $projtax[$i]->tax_id));
                    $this->db->update('tax_after_xlsdownload', array('current_month_tax' => $currenttaxtotal, 'entry_by' => $this->session->userdata('uid')));
                }
                //Close Start 12-08-2019 "tax_after_xlsdownload" Code By Asheesh..
            }





            $grandtotalcontract = $summerysubtotalcontract + $tax1;
            $grandtotalcumpre = $totalcu + $tax2;
            $grandtotalcur = $totalcur + $tax3;
            $grandtotalcum = $totalamts + $tax4;

            $spreadsheet->getActiveSheet()->setCellValue("A$taxj", "");
            $spreadsheet->getActiveSheet()->setCellValue("B$taxj", "Grand Total  (Rs.)");
            $spreadsheet->getActiveSheet()->setCellValue("C$taxj", "$grandtotalcontract");
            $spreadsheet->getActiveSheet()->setCellValue("D$taxj", "$grandtotalcumpre");
            $spreadsheet->getActiveSheet()->setCellValue("E$taxj", "$grandtotalcur");
            $spreadsheet->getActiveSheet()->setCellValue("F$taxj", "$grandtotalcum");
            $spreadsheet->getActiveSheet()->setCellValue("G$taxj", "-");

            //Grand Total  (Rs.) Ash Sub Total...
            $inserTr = array('invoice_id' => $invoiceID, 'srpos_no' => 'grand_total_rs', 'project_id' => $invoicedata->project_id, 'description' => 'Grand Total  (Rs.)',
                'contract_value' => $grandtotalcontract,
                'cumulative_up_previous_bill' => $grandtotalcumpre,
                'current_month_bill' => $grandtotalcur,
                'total_cumulative' => $grandtotalcum,
                'balance' => '0',
                'entry_by' => $this->session->userdata('uid'));
            $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => 'grand_total_rs', 'project_id' => $invoicedata->project_id));
            $num1 = $this->db->count_all_results('invoice_after_xlsdownload');
            if ($num1 < 1) {
                $this->db->insert('invoice_after_xlsdownload', $inserTr);
            } else {
                $this->db->where(array('invoice_id' => $invoiceID, 'srpos_no' => 'grand_total_rs', 'project_id' => $invoicedata->project_id));
                $this->db->update('invoice_after_xlsdownload', array(
                    'description' => 'Grand Total  (Rs.)',
                    'contract_value' => $grandtotalcontract,
                    'cumulative_up_previous_bill' => $grandtotalcumpre,
                    'current_month_bill' => $grandtotalcur,
                    'total_cumulative' => $grandtotalcum,
                    'balance' => '0',
                    'entry_by' => $this->session->userdata('uid')));
            }
            //Grand Total  (Rs.) CloseSub Total. Code...

            $spreadsheet->getActiveSheet()->getStyle('A1:G6')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $spreadsheet->getActiveSheet()->getStyle("A1:G6")->applyFromArray(
                    [
                        'font' => [
                            'bold' => true,
                        ],
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => Border::BORDER_THIN,
                                'color' => ['argb' => 'FF000000'],
                            ],
                        ]
                    ]
            );
            $spreadsheet->getActiveSheet()->getStyle("A$taxj:G$taxj")->applyFromArray(
                    [
                        'font' => [
                            'bold' => true,
                        ]
                    ]
            );

            $spreadsheet->getActiveSheet()->getStyle("A7:A$taxj")->applyFromArray(
                    [
                        'font' => [
                            'bold' => true,
                        ]
                    ]
            );

            $spreadsheet->getActiveSheet()->getStyle("A19:G19")->applyFromArray(
                    [
                        'font' => [
                            'bold' => true,
                        ]
                    ]
            );

            $spreadsheet->getActiveSheet()->getStyle("A1:G$taxj")->applyFromArray(
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => Border::BORDER_THIN,
                                'color' => ['argb' => 'FF000000'],
                            ],
                        ]
                    ]
            );
            //Sheet 2 Custom Code By Asheesh 07-02-2019... eXCEL fORMATING
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(45);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
            $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18);
            $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(17);
            $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(15);
            $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(15);
            $spreadsheet->getActiveSheet()->getStyle('A20:G20')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
            $spreadsheet->getActiveSheet()->getStyle('B20', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
            $spreadsheet->getActiveSheet()->getStyle('A2', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
            $spreadsheet->getActiveSheet()->getStyle('E5', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
            $spreadsheet->getActiveSheet()->getStyle('F5', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
            $spreadsheet->getActiveSheet()->getRowDimension('2')->setRowHeight(40);
            $spreadsheet->getActiveSheet()->getStyle('A7:A32')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $spreadsheet->getActiveSheet()->getStyle('A2')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
            $spreadsheet->getActiveSheet()->getStyle('A5')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
            $spreadsheet->getActiveSheet()->getStyle('B5')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
            $spreadsheet->getActiveSheet()->getStyle('D5', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);

            $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
            // $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
            // $spreadsheet->getActiveSheet()->getPageMargins()->setTop(1);
            //$spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.75);
            //$spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.75);
            //$spreadsheet->getActiveSheet()->getPageMargins()->setBottom(1);
            // $spreadsheet->getActiveSheet()->getPageSetup()->setFitToPage(true);
            // $spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
            //$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);
            //Excel Number Formate Code..

            $spreadsheet->getActiveSheet()->getStyle('C7:C22')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
            $spreadsheet->getActiveSheet()->getStyle('D7:D22')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
            $spreadsheet->getActiveSheet()->getStyle('E7:E22')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
            $spreadsheet->getActiveSheet()->getStyle('F7:F22')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
            $spreadsheet->getActiveSheet()->getStyle('G7:G22')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);

            $spreadsheet->getActiveSheet()->getStyle('C7:C22')->getNumberFormat()->setFormatCode('#,##0');
            $spreadsheet->getActiveSheet()->getStyle('D7:D22')->getNumberFormat()->setFormatCode('#,##0');
            $spreadsheet->getActiveSheet()->getStyle('E7:E22')->getNumberFormat()->setFormatCode('#,##0');
            $spreadsheet->getActiveSheet()->getStyle('F7:F22')->getNumberFormat()->setFormatCode('#,##0');
            $spreadsheet->getActiveSheet()->getStyle('G7:G22')->getNumberFormat()->setFormatCode('#,##0');
            $spreadsheet->getActiveSheet()->getStyle('C22:G33')->getNumberFormat()->setFormatCode('#,##0');
        }

        // Rename second worksheet
        $spreadsheet->getActiveSheet()->setTitle('Summary');


////////////////////////////////////////////Excel formating Sheet 3 Code Start../////////////////////////////////////////////////////////////
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet

        $spreadsheet->setActiveSheetIndex(0);
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(2);
        $spreadsheet->getActiveSheet()->setCellValue('A1', '(1) Remuneration for Local Professional Staff ASHE');
        $spreadsheet->getActiveSheet()->mergeCells('A1:R1');

        $spreadsheet->getActiveSheet()->setCellValue('A2', '');
        $spreadsheet->getActiveSheet()->mergeCells('A2:R2');

        $spreadsheet->getActiveSheet()->setCellValue('A3', 'S.No.');
        $spreadsheet->getActiveSheet()->mergeCells('A3:A4');

        $spreadsheet->getActiveSheet()->setCellValue('B3', 'Position');
        $spreadsheet->getActiveSheet()->mergeCells('B3:B4');

        $spreadsheet->getActiveSheet()->setCellValue('C3', 'Name');
        $spreadsheet->getActiveSheet()->mergeCells('C3:C4');

        $spreadsheet->getActiveSheet()->setCellValue('D3', 'Rate  (INR)');
        $spreadsheet->getActiveSheet()->mergeCells('D3:D4');

        $spreadsheet->getActiveSheet()->setCellValue('E3', 'Contract Value');
        $spreadsheet->getActiveSheet()->mergeCells('E3:F3');
        $spreadsheet->getActiveSheet()->setCellValue('E4', ' Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('F4', 'Amount (INR)');

        //Code Edited By Asheesh IF EOT Not for Project 4 Cols Hide Excell Formate..
        $spreadsheet->getActiveSheet()->setCellValue('G3', 'Total EOT');
        $spreadsheet->getActiveSheet()->mergeCells('G3:H3');
        $spreadsheet->getActiveSheet()->setCellValue('G4', ' Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('H4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('I3', 'Revised  Contract Value ');
        $spreadsheet->getActiveSheet()->mergeCells('I3:J3');
        $spreadsheet->getActiveSheet()->setCellValue('I4', ' Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('J4', 'Amount (INR)');
        //Code Edited By Asheesh IF EOT Not for Project 4 Cols Hide Excell Formate..

        $spreadsheet->getActiveSheet()->setCellValue('K3', 'Cumulative up to Previous Bill');
        $spreadsheet->getActiveSheet()->mergeCells('K3:L3');
        $spreadsheet->getActiveSheet()->setCellValue('K4', ' Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('L4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('M3', 'Current Month Bill');
        $spreadsheet->getActiveSheet()->mergeCells('M3:N3');
        $spreadsheet->getActiveSheet()->setCellValue('M4', ' Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('N4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('O3', 'Total Cumulative');
        $spreadsheet->getActiveSheet()->mergeCells('O3:P3');
        $spreadsheet->getActiveSheet()->setCellValue('O4', ' Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('P4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('Q3', 'Balance');
        $spreadsheet->getActiveSheet()->mergeCells('Q3:R3');
        $spreadsheet->getActiveSheet()->setCellValue('Q4', ' Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('R4', 'Amount (INR)');
        $spreadsheet->getActiveSheet()->setCellValue('A5', 'A');
        $spreadsheet->getActiveSheet()->setCellValue('B5', ' Remuneration of Key Personnel (Professional Staff) :');
        $spreadsheet->getActiveSheet()->mergeCells('B5:R5');

        $spreadsheet->getActiveSheet()->getStyle('A3:R5')->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:R1')->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:R1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A3:R5')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $cellval = '6';
        $j = 1;
        $totalcontract = 0;
        $totalcumulativepre = 0;
        $currmonth = 0;
        $totalcumulativeamt = 0;
        $totaleotamount = 0;
        $totalrcvamount = 0;
        $balanceamt = 0;
        $totalmmamountkeys = 0;
        $totalcumulativeamounts = 0;
        $totalcumulativeamountfull = 0;
        $totalkeybalanceamount = 0;

        for ($i = 0; $i < $keyID; $i++) {
            //  echo '<pre>'; print_r($keyteamdata[$i]);  
            $intermittent = $this->Front_model->getintermittent($invoicedata->project_id, $keyteamdata[$i]->emp_id);
            if (!empty($intermittent)) {
                $manmonthscrukey = $this->Front_model->getintermittentattd($invoicedata->project_id, $keyteamdata[$i]->emp_id, $keyteamdata[$i]->designation_id, $crumonths, $cruyears);
                //echo '<pre>'; print_r($manmonthscrukey);
                //$totalnoofdays = cal_days_in_month(CAL_GREGORIAN,$crumonths,$cruyears);
                $totalnoofdays = 30;
                if (($manmonthscrukey->present) > 0) {
                    //$mmcountprekey = round((1- ($totalnoofdays - $manmonthscrukey->present) / 30),$decimalplace);
                    $mmcountprekey = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                } else {
                    $mmcountprekey = 0;
                }
            } else {
                $timemonth = $this->Front_model->gettimesheetdetail($invoicedata->project_id, $keyteamdata[$i]->designation_id, $keyteamdata[$i]->emp_id, $crumonths, $cruyears);
                // $mmcountprekey1 = round((1- ($timemonth->leave + $timemonth->absent) / 30),$decimalplace);
                // if(($mmcountprekey1) > 0 ){
                // $mmcountprekey = $mmcountprekey1;
                // }
                if (($timemonth->leave + $timemonth->absent) > 0) {
                    $mmcountprekey = round((1 - ($timemonth->leave + $timemonth->absent) / $divideno_days), $decimalplace);
                    if ($mmcountprekey < 0) {
                        $mmcountprekey = 0;
                    }
                } else {
                    if (($timemonth->present) > 0) {
                        $mmcountprekey = 1;
                    } else {
                        $mmcountprekey = 0;
                    }
                }
            }
            //Rahul Replacement Condition 6-Nov-2019
            if ($keyteamdata[$i]->rate > 0) {
                $replacement_reducationrate = $keyteamdata[$i]->rate - (($keyteamdata[$i]->rate * $keyteamdata[$i]->replacement_reducationrate) / 100);
            } else {
                $replacement_reducationrate = $keyteamdata[$i]->rate;
            }
            $mmamountkey = $mmcountprekey * $replacement_reducationrate;
            $amount = $keyteamdata[$i]->rate * $keyteamdata[$i]->mm;
            $designation_name = $keyteamdata[$i]->designation;
            $userfullname = $keyteamdata[$i]->username;
            $rate = $keyteamdata[$i]->rate;
            $man_months = $keyteamdata[$i]->mm;
            $eot_mm = $keyteamdata[$i]->eot_mm;
            $rcv_mm = $keyteamdata[$i]->eot_mm + $man_months;
            $eotamount = $keyteamdata[$i]->eot_mm * $keyteamdata[$i]->rate;
            $rcvamount = $rcv_mm * $keyteamdata[$i]->rate;
            //05-02-2019.
            $cumulativemm = $keyteamdata[$i]->cumulativemm;
            //11-02-2019..
            $Ddesignationid = $keyteamdata[$i]->designation_id;

            if ($cumulativemm < 0) {
                $cumulativemm = 0;
            }
            $cumulativeamount = $keyteamdata[$i]->cumulativeamount;
            if ($cumulativeamount < 0) {
                $cumulativeamount = 0;
            }
            $totalcumulativemm = $cumulativemm + $mmcountprekey;


            $totalcumulativeamount = $mmamountkey + $cumulativeamount;
            $keybalancemm = $man_months + $eot_mm - $totalcumulativemm;
            $keybalanceamount = $amount + $eotamount - $totalcumulativeamount;
            $balanceamt += $keybalanceamount;
            $totalcontract += $amount;
            $totaleotamount += $eotamount;
            $totalrcvamount += $rcvamount;
            $totalcumulativeamounts += $cumulativeamount;
            $totalmmamountkeys += $mmamountkey;
            $totalcumulativeamountfull += $totalcumulativeamount;
            $totalkeybalanceamount += $keybalanceamount;
//11-02-2019..
            //   $designation_name= trim($designation_name);

            $spreadsheet->getActiveSheet()->setCellValue('A' . $cellval, "$j");
            $spreadsheet->getActiveSheet()->setCellValue('B' . $cellval, "$designation_name");
            $spreadsheet->getActiveSheet()->setCellValue('C' . $cellval, "$userfullname");
            $spreadsheet->getActiveSheet()->setCellValue('D' . $cellval, "$rate");
            $spreadsheet->getActiveSheet()->setCellValue('E' . $cellval, "$man_months");
            $spreadsheet->getActiveSheet()->setCellValue('F' . $cellval, "$amount");
            //04-02-2019.. HRMS..
            $spreadsheet->getActiveSheet()->setCellValue('G' . $cellval, "$eot_mm");
            $spreadsheet->getActiveSheet()->setCellValue('H' . $cellval, "$eotamount");
            $spreadsheet->getActiveSheet()->setCellValue('I' . $cellval, "$rcv_mm");
            $spreadsheet->getActiveSheet()->setCellValue('J' . $cellval, "$rcvamount");
            $spreadsheet->getActiveSheet()->setCellValue('K' . $cellval, "$cumulativemm");
            $spreadsheet->getActiveSheet()->setCellValue('L' . $cellval, "$cumulativeamount");
            $spreadsheet->getActiveSheet()->setCellValue('M' . $cellval, "$mmcountprekey");
            $spreadsheet->getActiveSheet()->setCellValue('N' . $cellval, "$mmamountkey");
            $spreadsheet->getActiveSheet()->setCellValue('O' . $cellval, "$totalcumulativemm");
            $spreadsheet->getActiveSheet()->setCellValue('P' . $cellval, "$totalcumulativeamount");
            $spreadsheet->getActiveSheet()->setCellValue('Q' . $cellval, "$keybalancemm");
            $spreadsheet->getActiveSheet()->setCellValue('R' . $cellval, "$keybalanceamount");
            $cellval = $cellval + 1;
            $j++;
        }


        //die;
        //$spreadsheet->getActiveSheet()->setCellValue('A'.$cellval, '2');
        $spreadsheet->getActiveSheet()->setCellValue('A' . $cellval, 'Sub-Total for Key Professional Staff (a)');
        $spreadsheet->getActiveSheet()->mergeCells("A$cellval:C$cellval");
        // $spreadsheet->getActiveSheet()->getStyle("A$cellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->setCellValue("F$cellval", "$totalcontract");
//         $spreadsheet->getActiveSheet()->setCellValue("F$cellval", "$totalcontract");
        $spreadsheet->getActiveSheet()->getStyle("F$cellval")->getNumberFormat()->setFormatCode('#,##0');

        $spreadsheet->getActiveSheet()->setCellValue("H$cellval", "$totaleotamount");
        $spreadsheet->getActiveSheet()->getStyle("H$cellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->setCellValue("J$cellval", "$totalrcvamount");
        $spreadsheet->getActiveSheet()->getStyle("J$cellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->setCellValue("L$cellval", "$totalcumulativeamounts");
        $spreadsheet->getActiveSheet()->getStyle("L$cellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->setCellValue("N$cellval", "$totalmmamountkeys");
        $spreadsheet->getActiveSheet()->getStyle("N$cellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->setCellValue("P$cellval", "$totalcumulativeamountfull");
        $spreadsheet->getActiveSheet()->getStyle("P$cellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->setCellValue("R$cellval", "$totalkeybalanceamount");
        $spreadsheet->getActiveSheet()->getStyle("R$cellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("A$cellval:R$cellval")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );

        $blnkcellval = $cellval + 1;
        $nxtcellval = $cellval + 2;

        $spreadsheet->getActiveSheet()->setCellValue('A' . $blnkcellval, '');
        $spreadsheet->getActiveSheet()->mergeCells("A$blnkcellval:R$blnkcellval");

        $spreadsheet->getActiveSheet()->setCellValue('A' . $nxtcellval, 'B');
        $spreadsheet->getActiveSheet()->setCellValue('B' . $nxtcellval, 'Remuneration of Sub Professional Staff :');
        $spreadsheet->getActiveSheet()->mergeCells("B$nxtcellval:R$nxtcellval");

        $spreadsheet->getActiveSheet()->getStyle("A$nxtcellval:R$nxtcellval")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    // 'size' => '10',
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );

        $secondcellval = $cellval + 3;
        $jj = 1;
        $totalcontract2 = 0;
        $totaleotamount2 = 0;
        $totalrcvamount2 = 0;
        $totalcumulativeamounts2 = 0;
        $totalmmamountkeys2 = 0;
        $totalcumulativepre2 = 0;
        $totalcumulativeamountfull2 = 0;
        $totalkeybalanceamount2 = 0;
        $currmonth2 = 0;
        $totalcumulativeamt2 = 0;
        $balanceamt2 = 0;
        for ($i = 0; $i < $subID; $i++) {
            $intermittent = $this->Front_model->getintermittent($invoicedata->project_id, $subteamdata[$i]->emp_id);
            if (!empty($intermittent)) {
                $manmonthscrukey = $this->Front_model->getintermittentattd($invoicedata->project_id, $subteamdata[$i]->emp_id, $subteamdata[$i]->designation_id, $crumonths, $cruyears);
                //$totalnoofdays = cal_days_in_month(CAL_GREGORIAN,$crumonths,$cruyears);
                $totalnoofdays = 30;
                if (($manmonthscrukey->present) > 0) {
                    //$mmcountpresub = round((1- ($totalnoofdays - $manmonthscrukey->present) / 30),$decimalplace);
                    $mmcountpresub = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                } else {
                    $mmcountpresub = 0;
                }
            } else {
                $timemonthsub = $this->Front_model->gettimesheetdetail($invoicedata->project_id, $subteamdata[$i]->designation_id, $subteamdata[$i]->emp_id, $crumonths, $cruyears);
                // $mmcountpresub1 = round((1- ($timemonthsub->leave + $timemonthsub->absent) / 30),$decimalplace);
                // if(($mmcountpresub1) > 0 ){
                // $mmcountpresub = $mmcountpresub1;
                // }
                if (($timemonthsub->leave + $timemonthsub->absent) > 0) {
                    $mmcountpresub = round((1 - ($timemonthsub->leave + $timemonthsub->absent) / $divideno_days), $decimalplace);
                    if ($mmcountpresub < 0) {
                        $mmcountpresub = 0;
                    }
                } else {
                    if (($timemonthsub->present) > 0) {
                        $mmcountpresub = 1;
                    } else {
                        $mmcountpresub = 0;
                    }
                }
            }

            if ($subteamdata[$i]->rate > 0) {
                $replacement_reducationrate = $subteamdata[$i]->rate - (($subteamdata[$i]->rate * $subteamdata[$i]->replacement_reducationrate) / 100);
            } else {
                $replacement_reducationrate = $subteamdata[$i]->rate;
            }
            $mmamountsub = $mmcountpresub * $replacement_reducationrate;
            //$mmamountsub = $mmcountpresub * $subteamdata[$i]->rate;
            //echo '<pre>'; print_r($subteamdata[$i]);
            $amount = $subteamdata[$i]->rate * $subteamdata[$i]->mm;
            $designation_name = $subteamdata[$i]->designation;
            $userfullname = $subteamdata[$i]->username;
            $rate = $subteamdata[$i]->rate;
            $man_months = $subteamdata[$i]->mm;
            $eot_mm = $subteamdata[$i]->eot_mm;
            $rcv_mm = $subteamdata[$i]->eot_mm + $man_months;
            $eotamount = $subteamdata[$i]->eot_mm * $subteamdata[$i]->rate;
            $rcvamount = $rcv_mm * $subteamdata[$i]->rate;
            $cumulativemm = $subteamdata[$i]->cumulativemm;
            if ($cumulativemm < 0) {
                $cumulativemm = 0;
            }
            $cumulativeamount = $subteamdata[$i]->cumulativeamount;
            if ($cumulativeamount < 0) {
                $cumulativeamount = 0;
            }

            $totalcumulativemm = $cumulativemm + $mmcountpresub;
            $totalcumulativeamount = $mmamountsub + $cumulativeamount;

            $keybalancemm = $man_months + $eot_mm - $totalcumulativemm;
            $keybalanceamount = $amount + $eotamount - $totalcumulativeamount;
            $balanceamt += $keybalanceamount;

            $totalcontract2 += $amount;
            $totaleotamount2 += $eotamount;
            $totalrcvamount2 += $rcvamount;
            $totalcumulativeamounts2 += $cumulativeamount;
            $totalmmamountkeys2 += $mmamountsub;
            $totalcumulativeamountfull2 += $totalcumulativeamount;
            $totalkeybalanceamount2 += $keybalanceamount;

            $spreadsheet->getActiveSheet()->setCellValue('A' . $secondcellval, "$jj");
            $spreadsheet->getActiveSheet()->setCellValue('B' . $secondcellval, "$designation_name");
            $spreadsheet->getActiveSheet()->setCellValue('C' . $secondcellval, "$userfullname");
            $spreadsheet->getActiveSheet()->setCellValue('D' . $secondcellval, "$rate");
            $spreadsheet->getActiveSheet()->setCellValue('E' . $secondcellval, "$man_months");
            $spreadsheet->getActiveSheet()->setCellValue('F' . $secondcellval, "$amount");
            $spreadsheet->getActiveSheet()->setCellValue('G' . $secondcellval, "$eot_mm");
            $spreadsheet->getActiveSheet()->setCellValue('H' . $secondcellval, "$eotamount");
            $spreadsheet->getActiveSheet()->setCellValue('I' . $secondcellval, "$rcv_mm");
            $spreadsheet->getActiveSheet()->setCellValue('J' . $secondcellval, "$rcvamount");
            $spreadsheet->getActiveSheet()->setCellValue('K' . $secondcellval, "$cumulativemm");
            $spreadsheet->getActiveSheet()->setCellValue('L' . $secondcellval, "$cumulativeamount");
            $spreadsheet->getActiveSheet()->setCellValue('M' . $secondcellval, "$mmcountpresub");
            $spreadsheet->getActiveSheet()->setCellValue('N' . $secondcellval, "$mmamountsub");
            $spreadsheet->getActiveSheet()->setCellValue('O' . $secondcellval, "$totalcumulativemm");
            $spreadsheet->getActiveSheet()->setCellValue('P' . $secondcellval, "$totalcumulativeamount");
            $spreadsheet->getActiveSheet()->setCellValue('Q' . $secondcellval, "$keybalancemm");
            $spreadsheet->getActiveSheet()->setCellValue('R' . $secondcellval, "$keybalanceamount");
            $secondcellval = $secondcellval + 1;
            $jj++;
        }
        //die;
        $spreadsheet->getActiveSheet()->setCellValue('A' . $secondcellval, 'Sub-Total for Sub Professional Staff (b)');
        $spreadsheet->getActiveSheet()->mergeCells("A$secondcellval:C$secondcellval");
        $spreadsheet->getActiveSheet()->setCellValue("F$secondcellval", "$totalcontract2");
        $spreadsheet->getActiveSheet()->setCellValue("H$secondcellval", "$totaleotamount2");
        $spreadsheet->getActiveSheet()->setCellValue("J$secondcellval", "$totalrcvamount2");
        $spreadsheet->getActiveSheet()->setCellValue("L$secondcellval", "$totalcumulativeamounts2");
        $spreadsheet->getActiveSheet()->setCellValue("N$secondcellval", "$totalmmamountkeys2");
        $spreadsheet->getActiveSheet()->setCellValue("P$secondcellval", "$totalcumulativeamountfull2");
        $spreadsheet->getActiveSheet()->setCellValue("R$secondcellval", "$totalkeybalanceamount2");
        $spreadsheet->getActiveSheet()->getStyle("F$secondcellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("H$secondcellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("J$secondcellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("L$secondcellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("N$secondcellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("P$secondcellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("R$secondcellval")->getNumberFormat()->setFormatCode('#,##0');



        $spreadsheet->getActiveSheet()->getStyle("A$secondcellval:R$secondcellval")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );

        $lastcellval = $secondcellval + 1;
        $lasttotalcontract = $totalcontract + $totalcontract2;
        $lasttotaleot = $totaleotamount + $totaleotamount2;
        $lasttotalrcv = $totalrcvamount + $totalrcvamount2;
        $lastcumulativeamounts = $totalcumulativeamounts + $totalcumulativeamounts2;
        $lasttotalamt = $totalmmamountkeys + $totalmmamountkeys2;
        $lastcumulativeamount = $totalcumulativeamountfull + $totalcumulativeamountfull2;
        $lastkeybalanceamount = $totalkeybalanceamount + $totalkeybalanceamount2;

        $spreadsheet->getActiveSheet()->setCellValue('A' . $lastcellval, 'Total for Key & Sub Professional Staff (a+b)');
        $spreadsheet->getActiveSheet()->mergeCells("A$lastcellval:C$lastcellval");
        $spreadsheet->getActiveSheet()->setCellValue("F$lastcellval", "$lasttotalcontract");
        $spreadsheet->getActiveSheet()->setCellValue("H$lastcellval", "$lasttotaleot");
        $spreadsheet->getActiveSheet()->setCellValue("J$lastcellval", "$lasttotalrcv");
        $spreadsheet->getActiveSheet()->setCellValue("L$lastcellval", "$lastcumulativeamounts");
        $spreadsheet->getActiveSheet()->setCellValue("N$lastcellval", "$lasttotalamt");
        $spreadsheet->getActiveSheet()->setCellValue("P$lastcellval", "$lastcumulativeamount");
        $spreadsheet->getActiveSheet()->setCellValue("R$lastcellval", "$lastkeybalanceamount");

        $spreadsheet->getActiveSheet()->getStyle("F$lastcellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("H$lastcellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("J$lastcellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("L$lastcellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("N$lastcellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("P$lastcellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("R$lastcellval")->getNumberFormat()->setFormatCode('#,##0');


        $spreadsheet->getActiveSheet()->getStyle("A$lastcellval:R$lastcellval")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A1:R$lastcellval")->applyFromArray(
                [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ]
                    ]
                ]
        );

        //Excel Formating Sheet 3 Custom Code Start,,..
        //
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);

        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(11);

        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(11);
        $spreadsheet->getActiveSheet()->getRowDimension('3')->setRowHeight(35);
        $spreadsheet->getActiveSheet()->getStyle('A3:O3')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A3:O3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);


        $spreadsheet->getActiveSheet()->getStyle('E4:R4', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D3:Q3', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('B6:B19', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('B23:B52', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('C23:C52', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('C6:C19', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A6:O54')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
//Repeat Row at header
        $spreadsheet->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(3, 4);
        $spreadsheet->getActiveSheet()->getStyle('B5')->getAlignment()->setHorizontal(HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getStyle('B6:B52')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('M6:M56')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('N6:N56')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('O6:O56')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);


        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
        $spreadsheet->getActiveSheet()->getPageMargins()->setTop(0.5);
        $spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.5);
        $spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.5);
        $spreadsheet->getActiveSheet()->getPageMargins()->setBottom(0.5);

        //@@@@ Remove Eot Cond Section ..
        //$spreadsheet->getActiveSheet()->removeColumn("G");
        //$spreadsheet->getActiveSheet()->removeColumn("G", 1);
        //$spreadsheet->getActiveSheet()->removeColumn("H");
        $spreadsheet->getActiveSheet()->getStyle('A6:A100')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A6:A100')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $spreadsheet->getActiveSheet()->getStyle('A5:Q100')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        //$spreadsheet->getActiveSheet()->getStyle("A5:A100")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        //$spreadsheet->getActiveSheet()->getStyle('D6:D100')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('D6:D100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('F6:F100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('H6:H100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('J6:J100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('L6:L100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('J6:J100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('L6:L100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('N6:N100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('P6:P100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('R6:R100')->getNumberFormat()->setFormatCode('#,##0');

        $spreadsheet->getActiveSheet()->getStyle('E6:E100')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('F6:F29')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        // $spreadsheet->getActiveSheet()->getStyle('H6:H100')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        $spreadsheet->getActiveSheet()->getStyle('G6:G100')->getNumberFormat()->setFormatCode('0.000');
        //$spreadsheet->getActiveSheet()->getStyle('I6:I29')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        // $spreadsheet->getActiveSheet()->getStyle('J6:J100')->getNumberFormat()->setFormatCode('0.000');
        //$spreadsheet->getActiveSheet()->getStyle('K6:K29')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        // $spreadsheet->getActiveSheet()->getStyle('L6:L100')->getNumberFormat()->setFormatCode('0.000');

        $spreadsheet->getActiveSheet()->getStyle('M6:M29')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('M6:M100')->getNumberFormat()->setFormatCode('0.000');

        $spreadsheet->getActiveSheet()->getStyle('Q6:Q29')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('Q6:Q100')->getNumberFormat()->setFormatCode('0.000');

        $spreadsheet->getActiveSheet()->getStyle('O6:O29')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('O6:O100')->getNumberFormat()->setFormatCode('0.000');

        $spreadsheet->getActiveSheet()->getStyle('K6:K29')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('K6:K100')->getNumberFormat()->setFormatCode('0.000');


        $spreadsheet->getActiveSheet()->getStyle('I6:I29')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('I6:I100')->getNumberFormat()->setFormatCode('0.000');


        //$spreadsheet->getActiveSheet()->getStyle('N6:N100')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('O6:O29')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        //$spreadsheet->getActiveSheet()->getStyle("F$cellval")->getNumberFormat()->setFormatCode('#,##0');



        $spreadsheet->getActiveSheet()->getStyle('N3:O3')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('N3:O3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $spreadsheet->getActiveSheet()->setTitle('(1)');
        $spreadsheet->setActiveSheetIndex(0);


/////////////////// Create a new worksheet,  sheet4///////////////////////////////////////////////
        //Excel Sheets 4 ... Code Start
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(3);
        $spreadsheet->getActiveSheet()->setCellValue('A1', '(2). SUPPORT STAFF');
        $spreadsheet->getActiveSheet()->mergeCells('A1:R1');

        $spreadsheet->getActiveSheet()->setCellValue('A2', '');
        $spreadsheet->getActiveSheet()->mergeCells('A2:R2');

        $spreadsheet->getActiveSheet()->setCellValue('A3', 'S.No.');
        $spreadsheet->getActiveSheet()->mergeCells('A3:A4');

        $spreadsheet->getActiveSheet()->setCellValue('B3', 'Position');
        $spreadsheet->getActiveSheet()->mergeCells('B3:B4');

        $spreadsheet->getActiveSheet()->setCellValue('C3', 'Name');
        $spreadsheet->getActiveSheet()->mergeCells('C3:C4');

        $spreadsheet->getActiveSheet()->setCellValue('D3', 'Rate  (INR)');
        $spreadsheet->getActiveSheet()->mergeCells('D3:D4');

        $spreadsheet->getActiveSheet()->setCellValue('E3', 'Contract Value');
        $spreadsheet->getActiveSheet()->mergeCells('E3:F3');
        $spreadsheet->getActiveSheet()->setCellValue('E4', ' Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('F4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('G3', 'Total EOT');
        $spreadsheet->getActiveSheet()->mergeCells('G3:H3');
        $spreadsheet->getActiveSheet()->setCellValue('G4', ' Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('H4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('I3', 'Revised  Contract Value ');
        $spreadsheet->getActiveSheet()->mergeCells('I3:J3');
        $spreadsheet->getActiveSheet()->setCellValue('I4', ' Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('J4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('K3', 'Cumulative up to Previous Bill');
        $spreadsheet->getActiveSheet()->mergeCells('K3:L3');
        $spreadsheet->getActiveSheet()->setCellValue('K4', ' Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('L4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('M3', 'Current Month Bill');
        $spreadsheet->getActiveSheet()->mergeCells('M3:N3');
        $spreadsheet->getActiveSheet()->setCellValue('M4', ' Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('N4', 'Amount (INR)');


        $spreadsheet->getActiveSheet()->setCellValue('O3', 'Total Cumulative');
        $spreadsheet->getActiveSheet()->mergeCells('O3:P3');
        $spreadsheet->getActiveSheet()->setCellValue('O4', ' Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('P4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('Q3', 'Balance');
        $spreadsheet->getActiveSheet()->mergeCells('Q3:R3');
        $spreadsheet->getActiveSheet()->setCellValue('Q4', ' Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('R4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->getStyle('A1:R4')->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:R4')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        //$spreadsheet->getActiveSheet()->getStyle('A3:N4')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $admincellval = '5';
        $jadminID = 1;
        $totalcontract3 = 0;
        $totalcumulativepre3 = 0;
        $currmonth3 = 0;
        $totalcumulativeamt3 = 0;
        $balanceamt3 = 0;
        $totaleotamount3 = 0;
        $totalrcvamount3 = 0;
        $totalcumulativeamounts3 = 0;
        $totalmmamountkeys3 = 0;
        $totalcumulativeamountfull3 = 0;
        $totalkeybalanceamount3 = 0;

        for ($i = 0; $i < $adminID; $i++) {
            $intermittent = $this->Front_model->getintermittent($invoicedata->project_id, $adminteamdata[$i]->emp_id);
            if (!empty($intermittent)) {

                $manmonthscrukey = $this->Front_model->getintermittentattd($invoicedata->project_id, $adminteamdata[$i]->emp_id, $adminteamdata[$i]->designation_id, $crumonths, $cruyears);
                //$totalnoofdays = cal_days_in_month(CAL_GREGORIAN,$crumonths,$cruyears);
                $totalnoofdays = 30;
                if (($manmonthscrukey->present) > 0) {
                    //$mmcountpreadmin = round((1- ($totalnoofdays - $manmonthscrukey->present) / 30),$decimalplace);
                    $mmcountpreadmin = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                } else {
                    $mmcountpreadmin = 0;
                }
            } else {
                $timemonth = $this->Front_model->gettimesheetdetail($invoicedata->project_id, $adminteamdata[$i]->designation_id, $adminteamdata[$i]->emp_id, $crumonths, $cruyears);
                // $mmcountpreadmin1 = round((1- ($timemonth->leave + $timemonth->absent) / 30),$decimalplace);
                // if(($mmcountpreadmin1) > 0 ){
                // $mmcountpreadmin = $mmcountpreadmin1;
                // }
                if (($timemonth->leave + $timemonth->absent) > 0) {
                    $mmcountpreadmin = round((1 - ($timemonth->leave + $timemonth->absent) / $divideno_days), $decimalplace);
                    if ($mmcountpreadmin < 0) {
                        $mmcountpreadmin = 0;
                    }
                } else {
                    if (($timemonth->present) > 0) {
                        $mmcountpreadmin = 1;
                    } else {
                        $mmcountpreadmin = 0;
                    }
                }
            }

            if ($adminteamdata[$i]->rate > 0) {
                $replacement_reducationrate = $adminteamdata[$i]->rate - (($adminteamdata[$i]->rate * $adminteamdata[$i]->replacement_reducationrate) / 100);
            } else {
                $replacement_reducationrate = $adminteamdata[$i]->rate;
            }
            $mmamountkey = $mmcountpreadmin * $replacement_reducationrate;
            //$mmamountkey = $mmcountpreadmin * $adminteamdata[$i]->rate;

            $amount = $adminteamdata[$i]->rate * $adminteamdata[$i]->mm;
            $designation_name = $adminteamdata[$i]->designation;
            $userfullname = $adminteamdata[$i]->username;
            $rate = $adminteamdata[$i]->rate;
            $man_months = $adminteamdata[$i]->mm;
            $eot_mm = $adminteamdata[$i]->eot_mm;
            $rcv_mm = $adminteamdata[$i]->eot_mm + $man_months;
            $eotamount = $adminteamdata[$i]->eot_mm * $adminteamdata[$i]->rate;
            $rcvamount = $rcv_mm * $adminteamdata[$i]->rate;
            $cumulativemm = $adminteamdata[$i]->cumulativemm;
            if ($cumulativemm < 0) {
                $cumulativemm = 0;
            }
            $cumulativeamount = $adminteamdata[$i]->cumulativeamount;
            if ($cumulativeamount < 0) {
                $cumulativeamount = 0;
            }

            $totalcumulativemm = $cumulativemm + $mmcountpreadmin;
            $totalcumulativeamount = $mmamountkey + $cumulativeamount;

            $keybalancemm = $man_months + $eot_mm - $totalcumulativemm;
            $keybalanceamount = $amount + $eotamount - $totalcumulativeamount;
            $balanceamt += $keybalanceamount;

            $totalcontract3 += $amount;
            $totaleotamount3 += $eotamount;
            $totalrcvamount3 += $rcvamount;
            $totalcumulativeamounts3 += $cumulativeamount;
            $totalmmamountkeys3 += $mmamountkey;
            $totalcumulativeamountfull3 += $totalcumulativeamount;
            $totalkeybalanceamount3 += $keybalanceamount;

            $spreadsheet->getActiveSheet()->setCellValue('A' . $admincellval, "$jadminID");
            $spreadsheet->getActiveSheet()->setCellValue('B' . $admincellval, "$designation_name");
            $spreadsheet->getActiveSheet()->setCellValue('C' . $admincellval, "$userfullname");
            $spreadsheet->getActiveSheet()->setCellValue('D' . $admincellval, "$rate");
            $spreadsheet->getActiveSheet()->setCellValue('E' . $admincellval, "$man_months");
            $spreadsheet->getActiveSheet()->setCellValue('F' . $admincellval, "$amount");
            $spreadsheet->getActiveSheet()->setCellValue('G' . $admincellval, "$eot_mm");
            $spreadsheet->getActiveSheet()->setCellValue('H' . $admincellval, "$eotamount");
            $spreadsheet->getActiveSheet()->setCellValue('I' . $admincellval, "$rcv_mm");
            $spreadsheet->getActiveSheet()->setCellValue('J' . $admincellval, "$rcvamount");
            $spreadsheet->getActiveSheet()->setCellValue('K' . $admincellval, "$cumulativemm");
            $spreadsheet->getActiveSheet()->setCellValue('L' . $admincellval, "$cumulativeamount");
            $spreadsheet->getActiveSheet()->setCellValue('M' . $admincellval, "$mmcountpreadmin");
            $spreadsheet->getActiveSheet()->setCellValue('N' . $admincellval, "$mmamountkey");
            $spreadsheet->getActiveSheet()->setCellValue('O' . $admincellval, "$totalcumulativemm");
            $spreadsheet->getActiveSheet()->setCellValue('P' . $admincellval, "$totalcumulativeamount");
            $spreadsheet->getActiveSheet()->setCellValue('Q' . $admincellval, "$keybalancemm");
            $spreadsheet->getActiveSheet()->setCellValue('R' . $admincellval, "$keybalanceamount");
            $admincellval = $admincellval + 1;
            $jadminID++;
        }


        $spreadsheet->getActiveSheet()->setCellValue('A' . $admincellval, 'Sub Total for Support Staff (C) ');
        $spreadsheet->getActiveSheet()->mergeCells("A$admincellval:C$admincellval");

        $spreadsheet->getActiveSheet()->setCellValue("F$admincellval", "$totalcontract3");
        $spreadsheet->getActiveSheet()->setCellValue("H$admincellval", "$totaleotamount3");
        $spreadsheet->getActiveSheet()->setCellValue("J$admincellval", "$totalrcvamount3");
        $spreadsheet->getActiveSheet()->setCellValue("L$admincellval", "$totalcumulativeamounts3");
        $spreadsheet->getActiveSheet()->setCellValue("N$admincellval", "$totalmmamountkeys3");
        $spreadsheet->getActiveSheet()->setCellValue("P$admincellval", "$totalcumulativeamountfull3");
        $spreadsheet->getActiveSheet()->setCellValue("R$admincellval", "$totalkeybalanceamount3");
        $spreadsheet->getActiveSheet()->getStyle("F$admincellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("H$admincellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("J$admincellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("L$admincellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("N$admincellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("P$admincellval")->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle("R$admincellval")->getNumberFormat()->setFormatCode('#,##0');


        $spreadsheet->getActiveSheet()->getStyle("A$admincellval:R$admincellval")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );

        $spreadsheet->getActiveSheet()->getStyle("A1:R$admincellval")->applyFromArray(
                [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ]
                    ]
                ]
        );

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(9);

        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(9);
        $spreadsheet->getActiveSheet()->getStyle('E4:R4', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D3:Q3', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('B6:B19', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('B23:B52', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('C23:C52', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('C6:C19', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A5:B52')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A5:A52')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getRowDimension('3')->setRowHeight(35);
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
        $spreadsheet->getActiveSheet()->getPageMargins()->setTop(1);
        $spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.75);
        $spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.75);
        $spreadsheet->getActiveSheet()->getPageMargins()->setBottom(1);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPrintArea('A1:R23');


        $spreadsheet->getActiveSheet()->getStyle('D5:D100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('F5:F100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('J5:J100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('L5:L100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('N5:N100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('P5:P100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('R5:R100')->getNumberFormat()->setFormatCode('#,##0');

        $spreadsheet->getActiveSheet()->getStyle('A3:R3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A3:R3')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

        // $spreadsheet->getActiveSheet()->getStyle('D5:D50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('E5:E50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('E5:E50')->getNumberFormat()->setFormatCode('0.000');
        //$spreadsheet->getActiveSheet()->getStyle('F5:F50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('G5:G50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        $spreadsheet->getActiveSheet()->getStyle('H5:H50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('I5:I50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('J5:J50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('K5:K50')->getNumberFormat()->setFormatCode('0.000');
        // $spreadsheet->getActiveSheet()->getStyle('L5:L50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('M5:M50')->getNumberFormat()->setFormatCode('0.000');
        //$spreadsheet->getActiveSheet()->getStyle('N5:N50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('O5:O50')->getNumberFormat()->setFormatCode('0.000');
        // $spreadsheet->getActiveSheet()->getStyle('P5:P50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('Q5:Q50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('R5:R50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        //4th Sheet Close Here... Start From Here Asheesh..
        $spreadsheet->getActiveSheet()->setTitle('(2)');
        $spreadsheet->setActiveSheetIndex(0);


        // Create a new worksheet, after the default sheet
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(4);
        $spreadsheet->getActiveSheet()->setCellValue('A1', '(3) Transportation (Fixed Rate on Rental Basis) ');
        $spreadsheet->getActiveSheet()->mergeCells('A1:Q1');

        $spreadsheet->getActiveSheet()->setCellValue('A2', '');
        $spreadsheet->getActiveSheet()->mergeCells('A2:Q2');

        $spreadsheet->getActiveSheet()->setCellValue('A3', 'S.No.');
        $spreadsheet->getActiveSheet()->mergeCells('A3:A4');

        $spreadsheet->getActiveSheet()->setCellValue('B3', 'Description of Vehicles');
        $spreadsheet->getActiveSheet()->mergeCells('B3:B4');

        $spreadsheet->getActiveSheet()->setCellValue('C3', 'Rate  (INR)');
        $spreadsheet->getActiveSheet()->mergeCells('C3:C4');

        $spreadsheet->getActiveSheet()->setCellValue('D3', 'Contract Value');
        $spreadsheet->getActiveSheet()->mergeCells('D3:E3');
        $spreadsheet->getActiveSheet()->setCellValue('D4', 'Qty.(No. of Vehicle - Month)');
        $spreadsheet->getActiveSheet()->setCellValue('E4', 'Amount (INR)');


        $spreadsheet->getActiveSheet()->setCellValue('F3', 'Total EOT');
        $spreadsheet->getActiveSheet()->mergeCells('F3:G3');
        $spreadsheet->getActiveSheet()->setCellValue('F4', 'Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('G4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('H3', 'Revised  Contract Value ');
        $spreadsheet->getActiveSheet()->mergeCells('H3:I3');
        $spreadsheet->getActiveSheet()->setCellValue('H4', 'Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('I4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('J3', 'Cumulative up to Previous Bill');
        $spreadsheet->getActiveSheet()->mergeCells('J3:K3');
        $spreadsheet->getActiveSheet()->setCellValue('J4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('K4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('L3', 'Current Month Bill');
        $spreadsheet->getActiveSheet()->mergeCells('L3:M3');
        $spreadsheet->getActiveSheet()->setCellValue('L4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('M4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('N3', 'Total Cumulative');
        $spreadsheet->getActiveSheet()->mergeCells('N3:O3');
        $spreadsheet->getActiveSheet()->setCellValue('N4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('O4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('P3', 'Balance');
        $spreadsheet->getActiveSheet()->mergeCells('P3:Q3');
        $spreadsheet->getActiveSheet()->setCellValue('P4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('Q4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->getStyle('A1:Q4')->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:Q4')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        //echo '<pre>'; print_r($transportation); die;
        $transcellval = '5';
        $transj = 1;
        $transtotalcontract = 0;
        $transtotalcum = 0;
        $transtotalcur = 0;
        $transtotaleot = 0;
        $transtotalrcv = 0;
        $transtotaltocum = 0;
        $transtotalbal = 0;

        for ($i = 0; $i < $transID; $i++) {
            $transeot = $this->Front_model->getreimbursableeot($invoicedata->project_id, $transportation[$i]->id);
            //echo '<pre>'; print_r($transeot);
            //$amount = $transportation[$i]->rate * $transportation[$i]->man_months;
            $description = $transportation[$i]->description;
            $per_month = $transportation[$i]->per_month;
            $no_month = $transportation[$i]->no_month;
            $amount = str_replace(",", "", $per_month) * $no_month;
            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $transportation[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $transportation[$i]->id, $invoicedata->invoice_date);
            //echo '<pre>';		 print_r($prevmonth); die;
            $cumprevmonth = $prevmonth[0]->cumulative_pre_mm;
            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month;

            $totalcummm = $cumprevmonth + $currentmmmonth;
            $totalcuamt = $cumprevamt + $currentamount;

            $eot_mm = $transeot->eot_mm;
            $rcv_mm = round($eot_mm + $no_month, 2);

            $eot_amount = $per_month * $eot_mm;
            $rcv_amount = $per_month * $rcv_mm;

            $balancemm = $no_month + $eot_mm - $totalcummm;
            $balanceamt = $amount + $eot_amount - $totalcuamt;
            $transtotalcontract += $amount;
            $transtotaleot += $eot_amount;
            $transtotalrcv += $rcv_amount;
            $transtotalcum += $cumprevamt;
            $transtotalcur += $currentamount;
            $transtotaltocum += $totalcuamt;
            $transtotalbal += $balanceamt;

            $spreadsheet->getActiveSheet()->setCellValue('A' . $transcellval, "$transj");
            $spreadsheet->getActiveSheet()->setCellValue('B' . $transcellval, "$description");
            $spreadsheet->getActiveSheet()->setCellValue('C' . $transcellval, "$per_month");
            $spreadsheet->getActiveSheet()->setCellValue('D' . $transcellval, "$no_month");
            $spreadsheet->getActiveSheet()->setCellValue('E' . $transcellval, "$amount");
            $spreadsheet->getActiveSheet()->setCellValue('F' . $transcellval, "$eot_mm");
            $spreadsheet->getActiveSheet()->setCellValue('G' . $transcellval, "$eot_amount");
            $spreadsheet->getActiveSheet()->setCellValue('H' . $transcellval, "$rcv_mm");
            $spreadsheet->getActiveSheet()->setCellValue('I' . $transcellval, "$rcv_amount");
            $spreadsheet->getActiveSheet()->setCellValue('J' . $transcellval, "$cumprevmonth");
            $spreadsheet->getActiveSheet()->setCellValue('K' . $transcellval, "$cumprevamt");
            $spreadsheet->getActiveSheet()->setCellValue('L' . $transcellval, "$currentmmmonth");
            $spreadsheet->getActiveSheet()->setCellValue('M' . $transcellval, "$currentamount");
            $spreadsheet->getActiveSheet()->setCellValue('N' . $transcellval, "$totalcummm");
            $spreadsheet->getActiveSheet()->setCellValue('O' . $transcellval, "$totalcuamt");
            $spreadsheet->getActiveSheet()->setCellValue('P' . $transcellval, "$balancemm");
            $spreadsheet->getActiveSheet()->setCellValue('Q' . $transcellval, "$balanceamt");
            $transcellval = $transcellval + 1;
            $transj++;
        }

        //$spreadsheet->getActiveSheet()->setCellValue('A'.$cellval, '2');
        $spreadsheet->getActiveSheet()->setCellValue('A' . $transcellval, 'Total');
        $spreadsheet->getActiveSheet()->mergeCells("A$transcellval:C$transcellval");
        $spreadsheet->getActiveSheet()->setCellValue("E$transcellval", "$transtotalcontract");
        $spreadsheet->getActiveSheet()->setCellValue("G$transcellval", "$transtotaleot");
        $spreadsheet->getActiveSheet()->setCellValue("I$transcellval", "$transtotalrcv");
        $spreadsheet->getActiveSheet()->setCellValue("K$transcellval", "$transtotalcum");
        $spreadsheet->getActiveSheet()->setCellValue("M$transcellval", "$transtotalcur");
        $spreadsheet->getActiveSheet()->setCellValue("O$transcellval", "$transtotaltocum");
        $spreadsheet->getActiveSheet()->setCellValue("Q$transcellval", "$transtotalbal");
        $spreadsheet->getActiveSheet()->getRowDimension($transcellval)->setRowHeight(35);
        $spreadsheet->getActiveSheet()->getStyle("A$transcellval:Q$transcellval")->getNumberFormat()->setFormatCode('#,##0');


        $spreadsheet->getActiveSheet()->getStyle("A$transcellval:Q$transcellval")->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => 'FF000000'],
                ],
            ]
                ]
        );

        $spreadsheet->getActiveSheet()->getStyle("A1:Q$transcellval")->applyFromArray(
                [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ]
                    ]
                ]
        );

        $transblnkcellval = $transcellval + 1;
        $transnxtcellvalchk = $transcellval + 3;
        $transnxtcellval = $transcellval + 4;

        $transnxtcellval1 = $transcellval + 5;
        $transnxtcellval2 = $transcellval + 6;
        $transnxtcellval3 = $transcellval + 7;
        $transnxtcellval4 = $transcellval + 8;

        $spreadsheet->getActiveSheet()->setCellValue('A' . $transblnkcellval, '');
        //$spreadsheet->getActiveSheet()->mergeCells("A$transblnkcellval:Q$transnxtcellvalchk");
        //4.. Code Section Asheesh...
        $spreadsheet->getActiveSheet()->setCellValue('A' . $transnxtcellval, '(4) Duty Travel to Site (Fixed Costs): Professional and Sub - Professional Staff');
        $spreadsheet->getActiveSheet()->mergeCells("A$transnxtcellval:Q$transnxtcellval");

        $spreadsheet->getActiveSheet()->setCellValue('A' . $transnxtcellval1, '');
        $spreadsheet->getActiveSheet()->mergeCells("A$transnxtcellval1:Q$transnxtcellval1");

        $spreadsheet->getActiveSheet()->setCellValue('A' . $transnxtcellval2, 'S.No.');
        $spreadsheet->getActiveSheet()->mergeCells("A$transnxtcellval2:A$transnxtcellval3");

        $spreadsheet->getActiveSheet()->setCellValue('B' . $transnxtcellval2, 'Trips');
        $spreadsheet->getActiveSheet()->mergeCells("B$transnxtcellval2:B$transnxtcellval3");



        $spreadsheet->getActiveSheet()->setCellValue('C' . $transnxtcellval2, 'Rate Per Month (INR)');
        $spreadsheet->getActiveSheet()->mergeCells("C$transnxtcellval2:C$transnxtcellval3");

        $spreadsheet->getActiveSheet()->setCellValue('D' . $transnxtcellval2, 'Contract Values');
        $spreadsheet->getActiveSheet()->mergeCells("D$transnxtcellval2:E$transnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('D' . $transnxtcellval3, 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('E' . $transnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('F' . $transnxtcellval2, 'Total EOT');
        $spreadsheet->getActiveSheet()->mergeCells("F$transnxtcellval2:G$transnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('F' . $transnxtcellval3, 'Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('G' . $transnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('H' . $transnxtcellval2, 'Revised  Contract Value');
        $spreadsheet->getActiveSheet()->mergeCells("H$transnxtcellval2:I$transnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('H' . $transnxtcellval3, 'Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('I' . $transnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('J' . $transnxtcellval2, 'Cumulative up to Previous Bill');
        $spreadsheet->getActiveSheet()->mergeCells("J$transnxtcellval2:K$transnxtcellval2");
        $spreadsheet->getActiveSheet()->getRowDimension($transnxtcellval2)->setRowHeight(35);
        $spreadsheet->getActiveSheet()->setCellValue('J' . $transnxtcellval3, 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('K' . $transnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('L' . $transnxtcellval2, 'Current Month Bill');
        $spreadsheet->getActiveSheet()->mergeCells("L$transnxtcellval2:M$transnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('L' . $transnxtcellval3, 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('M' . $transnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('N' . $transnxtcellval2, 'Total Cumulative');
        $spreadsheet->getActiveSheet()->mergeCells("N$transnxtcellval2:O$transnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('N' . $transnxtcellval3, 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('O' . $transnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('P' . $transnxtcellval2, 'Balance');
        $spreadsheet->getActiveSheet()->mergeCells("P$transnxtcellval2:Q$transnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('P' . $transnxtcellval3, 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('Q' . $transnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->getStyle("A$transblnkcellval:Q$transnxtcellval3")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A$transblnkcellval:Q$transnxtcellval3")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $travelcellval = $transnxtcellval4;
        $travelj = 1;
        $traveltotalcontract = 0;
        $traveltotalcum = 0;
        $traveltotalcur = 0;
        $traveltotaltocum = 0;
        $traveltotalbal = 0;
        $traveltotaleot = 0;
        $traveltotalrcv = 0;
        for ($i = 0; $i < $travelID; $i++) {
            $traveleot = $this->Front_model->getreimbursableeot($invoicedata->project_id, $travel_site[$i]->id);
            //$amount = $travel_site[$i]->rate * $travel_site[$i]->man_months;
            $description = $travel_site[$i]->description;
            $per_month = $travel_site[$i]->per_month;
            $no_month = $travel_site[$i]->no_month;

            $amount = $per_month * $no_month;
            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $travel_site[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $travel_site[$i]->id, $invoicedata->invoice_date);
            //echo '<pre>';		 print_r($prevmonth);
            $cumprevmonth = $prevmonth[0]->cumulative_pre_mm;
            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month;

            $eot_mm = $traveleot->eot_mm;
            $rcv_mm = round($eot_mm + $no_month, 2);

            $eot_amount = $per_month * $eot_mm;
            $rcv_amount = $per_month * $rcv_mm;

            $totalcummm = $cumprevmonth + $currentmmmonth;
            $totalcuamt = $cumprevamt + $currentamount;

            $balancemm = $no_month + $eot_mm - $totalcummm;
            $balanceamt = $amount + $eot_amount - $totalcuamt;

            $traveltotalcontract += $amount;
            $traveltotaleot += $eot_amount;
            $traveltotalrcv += $rcv_amount;
            $traveltotalcum += $cumprevamt;
            $traveltotalcur += $currentamount;
            $traveltotaltocum += $totalcuamt;
            $traveltotalbal += $balanceamt;

            $spreadsheet->getActiveSheet()->setCellValue('A' . $travelcellval, "$travelj");
            $spreadsheet->getActiveSheet()->setCellValue('B' . $travelcellval, "$description");

            $spreadsheet->getActiveSheet()->setCellValue('C' . $travelcellval, "$per_month");
            $spreadsheet->getActiveSheet()->setCellValue('D' . $travelcellval, "$no_month");
            $spreadsheet->getActiveSheet()->setCellValue('E' . $travelcellval, "$amount");

            $spreadsheet->getActiveSheet()->setCellValue('F' . $travelcellval, "$eot_mm");
            $spreadsheet->getActiveSheet()->setCellValue('G' . $travelcellval, "$eot_amount");
            $spreadsheet->getActiveSheet()->setCellValue('H' . $travelcellval, "$rcv_mm");
            $spreadsheet->getActiveSheet()->setCellValue('I' . $travelcellval, "$rcv_amount");

            $spreadsheet->getActiveSheet()->setCellValue('J' . $travelcellval, "$cumprevmonth");
            $spreadsheet->getActiveSheet()->setCellValue('K' . $travelcellval, "$cumprevamt");
            $spreadsheet->getActiveSheet()->setCellValue('L' . $travelcellval, "$currentmmmonth");
            $spreadsheet->getActiveSheet()->setCellValue('M' . $travelcellval, "$currentamount");
            $spreadsheet->getActiveSheet()->setCellValue('N' . $travelcellval, "$totalcummm");
            $spreadsheet->getActiveSheet()->setCellValue('O' . $travelcellval, "$totalcuamt");
            $spreadsheet->getActiveSheet()->setCellValue('P' . $travelcellval, "$balancemm");
            $spreadsheet->getActiveSheet()->setCellValue('Q' . $travelcellval, "$balanceamt");

            //$spreadsheet->getActiveSheet()->getRowDimension($travelcellval)->setRowHeight(45999);
            $travelcellval = $travelcellval + 1;
            $travelj++;
        }

        $spreadsheet->getActiveSheet()->setCellValue('A' . $travelcellval, 'Total');
        $spreadsheet->getActiveSheet()->mergeCells("A$travelcellval:C$travelcellval");
        $spreadsheet->getActiveSheet()->setCellValue("E$travelcellval", "$traveltotalcontract");
        $spreadsheet->getActiveSheet()->setCellValue("G$travelcellval", "$traveltotaleot");
        $spreadsheet->getActiveSheet()->setCellValue("I$travelcellval", "$traveltotalrcv");
        $spreadsheet->getActiveSheet()->setCellValue("K$travelcellval", "$traveltotalcum");
        $spreadsheet->getActiveSheet()->setCellValue("M$travelcellval", "$traveltotalcur");
        $spreadsheet->getActiveSheet()->setCellValue("O$travelcellval", "$traveltotaltocum");
        $spreadsheet->getActiveSheet()->setCellValue("Q$travelcellval", "$traveltotalbal");
        $spreadsheet->getActiveSheet()->getStyle("E$travelcellval:Q$travelcellval")->getNumberFormat()->setFormatCode('#,##0');

        $spreadsheet->getActiveSheet()->getStyle("A$travelcellval:Q$travelcellval")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );

        $spreadsheet->getActiveSheet()->getStyle("A$transblnkcellval:Q$travelcellval")->applyFromArray(
                [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ]
                    ]
                ]
        );

        $travelblnkcellval = $travelcellval + 1;
        $travelnxtcellvalchk = $travelcellval + 3;
        $travelnxtcellval = $travelcellval + 4;

        $travelnxtcellval1 = $travelcellval + 5;
        $travelnxtcellval2 = $travelcellval + 6;
        $travelnxtcellval3 = $travelcellval + 7;
        $travelnxtcellval4 = $travelcellval + 8;

        $spreadsheet->getActiveSheet()->setCellValue('A' . $travelblnkcellval, '');
        $spreadsheet->getActiveSheet()->mergeCells("A$travelblnkcellval:Q$travelnxtcellvalchk");

        $spreadsheet->getActiveSheet()->setCellValue('A' . $travelnxtcellval, '(5) Office Rent (Fixed Costs)');
        $spreadsheet->getActiveSheet()->mergeCells("A$travelnxtcellval:Q$travelnxtcellval");

        $spreadsheet->getActiveSheet()->setCellValue('A' . $travelnxtcellval1, '');
        $spreadsheet->getActiveSheet()->mergeCells("A$travelnxtcellval1:Q$travelnxtcellval1");

        $spreadsheet->getActiveSheet()->setCellValue('A' . $travelnxtcellval2, 'S.No.');
        $spreadsheet->getActiveSheet()->mergeCells("A$travelnxtcellval2:A$travelnxtcellval3");

        $spreadsheet->getActiveSheet()->setCellValue('B' . $travelnxtcellval2, 'Description');
        $spreadsheet->getActiveSheet()->mergeCells("B$travelnxtcellval2:B$travelnxtcellval3");

        $spreadsheet->getActiveSheet()->setCellValue('C' . $travelnxtcellval2, 'Rate Per Month (INR)');
        $spreadsheet->getActiveSheet()->mergeCells("C$travelnxtcellval2:C$travelnxtcellval3");

        $spreadsheet->getActiveSheet()->setCellValue('D' . $travelnxtcellval2, 'Contract Values');
        $spreadsheet->getActiveSheet()->mergeCells("D$travelnxtcellval2:E$travelnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('D' . $travelnxtcellval3, 'No. of Months');
        $spreadsheet->getActiveSheet()->setCellValue('E' . $travelnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('F' . $travelnxtcellval2, 'Total EOT');
        $spreadsheet->getActiveSheet()->mergeCells("F$travelnxtcellval2:G$travelnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('F' . $travelnxtcellval3, 'Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('G' . $travelnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('H' . $travelnxtcellval2, 'Revised  Contract Value');
        $spreadsheet->getActiveSheet()->mergeCells("H$travelnxtcellval2:I$travelnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('H' . $travelnxtcellval3, 'Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('I' . $travelnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('J' . $travelnxtcellval2, 'Cumulative up to Previous Bill');
        $spreadsheet->getActiveSheet()->mergeCells("J$travelnxtcellval2:K$travelnxtcellval2");

        $spreadsheet->getActiveSheet()->getRowDimension($travelnxtcellval2)->setRowHeight(35);
        $spreadsheet->getActiveSheet()->setCellValue('J' . $travelnxtcellval3, 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('K' . $travelnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('L' . $travelnxtcellval2, 'Current Month Bill');
        $spreadsheet->getActiveSheet()->mergeCells("L$travelnxtcellval2:M$travelnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('L' . $travelnxtcellval3, 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('M' . $travelnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('N' . $travelnxtcellval2, 'Total Cumulative');
        $spreadsheet->getActiveSheet()->mergeCells("N$travelnxtcellval2:O$travelnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('N' . $travelnxtcellval3, 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('O' . $travelnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('P' . $travelnxtcellval2, 'Balance');
        $spreadsheet->getActiveSheet()->mergeCells("P$travelnxtcellval2:Q$travelnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('P' . $travelnxtcellval3, 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('Q' . $travelnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->getStyle("A$travelblnkcellval:Q$travelnxtcellval3")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A$travelblnkcellval:Q$travelnxtcellval3")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $officerentcellval = $travelnxtcellval4;
        $officerentj = 1;
        $officerenttotalcontract = 0;
        $officerenttotalcum = 0;
        $officerenttotalcur = 0;
        $officerenttotaltocum = 0;
        $officerenttotalbal = 0;
        $officerenttotaleot = 0;
        $officerenttotalrcv = 0;
        for ($i = 0; $i < $officerentID; $i++) {
            $officerenteot = $this->Front_model->getreimbursableeot($invoicedata->project_id, $office_rent[$i]->id);
            //$amount = $travel_site[$i]->rate * $travel_site[$i]->man_months;
            $description = $office_rent[$i]->description;
            $per_month = $office_rent[$i]->per_month;
            $no_month = $office_rent[$i]->no_month;

            $amount = $per_month * $no_month;
            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $office_rent[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $office_rent[$i]->id, $invoicedata->invoice_date);

            $cumprevmonth = $prevmonth[0]->cumulative_pre_mm;
            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month;

            $eot_mm = $officerenteot->eot_mm;
            $rcv_mm = round($eot_mm + $no_month, 2);

            $eot_amount = $per_month * $eot_mm;
            $rcv_amount = $per_month * $rcv_mm;

            $totalcummm = $cumprevmonth + $currentmmmonth;
            $totalcuamt = $cumprevamt + $currentamount;

            $balancemm = $no_month + $eot_mm - $totalcummm;
            $balanceamt = $amount + $eot_amount - $totalcuamt;

            $officerenttotalcontract += $amount;
            $officerenttotaleot += $eot_amount;
            $officerenttotalrcv += $rcv_amount;
            $officerenttotalcum += $cumprevamt;
            $officerenttotalcur += $currentamount;
            $officerenttotaltocum += $totalcuamt;
            $officerenttotalbal += $balanceamt;

            $spreadsheet->getActiveSheet()->setCellValue('A' . $officerentcellval, "$officerentj");
            $spreadsheet->getActiveSheet()->setCellValue('B' . $officerentcellval, "$description");
            $spreadsheet->getActiveSheet()->setCellValue('C' . $officerentcellval, "$per_month");
            $spreadsheet->getActiveSheet()->setCellValue('D' . $officerentcellval, "$no_month");
            $spreadsheet->getActiveSheet()->setCellValue('E' . $officerentcellval, "$amount");

            $spreadsheet->getActiveSheet()->setCellValue('F' . $officerentcellval, "$eot_mm");
            $spreadsheet->getActiveSheet()->setCellValue('G' . $officerentcellval, "$eot_amount");
            $spreadsheet->getActiveSheet()->setCellValue('H' . $officerentcellval, "$rcv_mm");
            $spreadsheet->getActiveSheet()->setCellValue('I' . $officerentcellval, "$rcv_amount");

            $spreadsheet->getActiveSheet()->setCellValue('J' . $officerentcellval, "$cumprevmonth");
            $spreadsheet->getActiveSheet()->setCellValue('K' . $officerentcellval, "$cumprevamt");
            $spreadsheet->getActiveSheet()->setCellValue('L' . $officerentcellval, "$currentmmmonth");
            $spreadsheet->getActiveSheet()->setCellValue('M' . $officerentcellval, "$currentamount");
            $spreadsheet->getActiveSheet()->setCellValue('N' . $officerentcellval, "$totalcummm");
            $spreadsheet->getActiveSheet()->setCellValue('O' . $officerentcellval, "$totalcuamt");
            $spreadsheet->getActiveSheet()->setCellValue('P' . $officerentcellval, "$balancemm");
            $spreadsheet->getActiveSheet()->setCellValue('Q' . $officerentcellval, "$balanceamt");
            $officerentcellval = $officerentcellval + 1;
            $officerentj++;
        }

        $spreadsheet->getActiveSheet()->setCellValue('A' . $officerentcellval, 'Total');
        $spreadsheet->getActiveSheet()->mergeCells("A$officerentcellval:C$officerentcellval");
        $spreadsheet->getActiveSheet()->setCellValue("E$officerentcellval", "$officerenttotalcontract");
        $spreadsheet->getActiveSheet()->setCellValue("G$officerentcellval", "$officerenttotaleot");
        $spreadsheet->getActiveSheet()->setCellValue("I$officerentcellval", "$officerenttotalrcv");
        $spreadsheet->getActiveSheet()->setCellValue("K$officerentcellval", "$officerenttotalcum");
        $spreadsheet->getActiveSheet()->setCellValue("M$officerentcellval", "$officerenttotalcur");
        $spreadsheet->getActiveSheet()->setCellValue("O$officerentcellval", "$officerenttotaltocum");
        $spreadsheet->getActiveSheet()->setCellValue("Q$officerentcellval", "$officerenttotalbal");
        $spreadsheet->getActiveSheet()->getStyle("A$officerentcellval:Q$officerentcellval")->getNumberFormat()->setFormatCode('#,##0');


        $spreadsheet->getActiveSheet()->getStyle("A$officerentcellval:Q$officerentcellval")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );

        $spreadsheet->getActiveSheet()->getStyle("A$travelblnkcellval:Q$officerentcellval")->applyFromArray([
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => 'FF000000'],
                ]
            ]
                ]
        );

        $suppliesblnkcellval = $officerentcellval + 1;
        $suppliesnxtcellvalchk = $officerentcellval + 3;
        $suppliesnxtcellval = $officerentcellval + 4;

        $suppliesnxtcellval1 = $officerentcellval + 5;
        $suppliesnxtcellval2 = $officerentcellval + 6;
        $suppliesnxtcellval3 = $officerentcellval + 7;
        $suppliesnxtcellval4 = $officerentcellval + 8;

        $spreadsheet->getActiveSheet()->setCellValue('A' . $suppliesblnkcellval, '');
        $spreadsheet->getActiveSheet()->mergeCells("A$suppliesblnkcellval:Q$suppliesnxtcellvalchk");

        $spreadsheet->getActiveSheet()->setCellValue('A' . $suppliesnxtcellval, '(6) Office Supplies, Utilities and Communication (Fixed Costs)');
        $spreadsheet->getActiveSheet()->mergeCells("A$suppliesnxtcellval:Q$suppliesnxtcellval");

        $spreadsheet->getActiveSheet()->setCellValue('A' . $suppliesnxtcellval1, '');
        $spreadsheet->getActiveSheet()->mergeCells("A$suppliesnxtcellval1:Q$suppliesnxtcellval1");

        $spreadsheet->getActiveSheet()->setCellValue('A' . $suppliesnxtcellval2, 'S.No.');
        $spreadsheet->getActiveSheet()->mergeCells("A$suppliesnxtcellval2:A$suppliesnxtcellval3");

        $spreadsheet->getActiveSheet()->setCellValue('B' . $suppliesnxtcellval2, 'Item');
        $spreadsheet->getActiveSheet()->mergeCells("B$suppliesnxtcellval2:B$suppliesnxtcellval3");

        $spreadsheet->getActiveSheet()->setCellValue('C' . $suppliesnxtcellval2, 'Months');
        $spreadsheet->getActiveSheet()->mergeCells("C$suppliesnxtcellval2:C$suppliesnxtcellval3");

        $spreadsheet->getActiveSheet()->setCellValue('D' . $suppliesnxtcellval2, 'Contract Values');
        $spreadsheet->getActiveSheet()->mergeCells("D$suppliesnxtcellval2:E$suppliesnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('D' . $suppliesnxtcellval3, 'Monthly Rate');
        $spreadsheet->getActiveSheet()->setCellValue('E' . $suppliesnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('F' . $suppliesnxtcellval2, 'Total EOT');
        $spreadsheet->getActiveSheet()->mergeCells("F$suppliesnxtcellval2:G$suppliesnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('F' . $suppliesnxtcellval3, 'Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('G' . $suppliesnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('H' . $suppliesnxtcellval2, 'Revised  Contract Value');
        $spreadsheet->getActiveSheet()->mergeCells("H$suppliesnxtcellval2:I$suppliesnxtcellval2");
        $spreadsheet->getActiveSheet()->getRowDimension($suppliesnxtcellval2)->setRowHeight(35);

        $spreadsheet->getActiveSheet()->setCellValue('H' . $suppliesnxtcellval3, 'Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('I' . $suppliesnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('J' . $suppliesnxtcellval2, 'Cumulative up to Previous Bill');
        $spreadsheet->getActiveSheet()->mergeCells("J$suppliesnxtcellval2:K$suppliesnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('J' . $suppliesnxtcellval3, 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('K' . $suppliesnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('L' . $suppliesnxtcellval2, 'Current Month Bill');
        $spreadsheet->getActiveSheet()->mergeCells("L$suppliesnxtcellval2:M$suppliesnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('L' . $suppliesnxtcellval3, 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('M' . $suppliesnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('N' . $suppliesnxtcellval2, 'Total Cumulative');
        $spreadsheet->getActiveSheet()->mergeCells("N$suppliesnxtcellval2:O$suppliesnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('N' . $suppliesnxtcellval3, 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('O' . $suppliesnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('P' . $suppliesnxtcellval2, 'Balance');
        $spreadsheet->getActiveSheet()->mergeCells("P$suppliesnxtcellval2:Q$suppliesnxtcellval2");
        $spreadsheet->getActiveSheet()->setCellValue('P' . $suppliesnxtcellval3, 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('Q' . $suppliesnxtcellval3, 'Amount (INR)');

        $spreadsheet->getActiveSheet()->getStyle("A$suppliesblnkcellval:Q$suppliesnxtcellval3")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A$suppliesblnkcellval:Q$suppliesnxtcellval3")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $suppliescellval = $suppliesnxtcellval4;
        $suppliesj = 1;
        $suppliestotalcontract = 0;
        $suppliestotalcum = 0;
        $suppliestotalcur = 0;
        $suppliestotaltocum = 0;
        $suppliestotalbal = 0;
        $suppliestotaleot = 0;
        $suppliestotalrcv = 0;
        for ($i = 0; $i < $suppliesID; $i++) {
            $officesupplieseot = $this->Front_model->getreimbursableeot($invoicedata->project_id, $office_supplies[$i]->id);
            //$amount = $travel_site[$i]->rate * $travel_site[$i]->man_months;
            $description = $office_supplies[$i]->description;
            $per_month = $office_supplies[$i]->per_month;
            $no_month = $office_supplies[$i]->no_month;


            $amount = $per_month * $no_month;
            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $office_supplies[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $office_supplies[$i]->id, $invoicedata->invoice_date);

            $cumprevmonth = $prevmonth[0]->cumulative_pre_mm;
            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month;

            $eot_mm = $officesupplieseot->eot_mm;
            $rcv_mm = round($eot_mm + $no_month, 2);

            $eot_amount = $per_month * $eot_mm;
            $rcv_amount = $per_month * $rcv_mm;

            $totalcummm = $cumprevmonth + $currentmmmonth;
            $totalcuamt = $cumprevamt + $currentamount;
            $balancemm = $no_month + $eot_mm - $totalcummm;
            $balanceamt = $amount + $eot_amount - $totalcuamt;

            $suppliestotalcontract += $amount;
            $suppliestotaleot += $eot_amount;
            $suppliestotalrcv += $rcv_amount;
            $suppliestotalcum += $cumprevamt;
            $suppliestotalcur += $currentamount;
            $suppliestotaltocum += $totalcuamt;
            $suppliestotalbal += $balanceamt;

            $spreadsheet->getActiveSheet()->setCellValue('A' . $suppliescellval, "$suppliesj");
            $spreadsheet->getActiveSheet()->setCellValue('B' . $suppliescellval, "$description");
            $spreadsheet->getActiveSheet()->setCellValue('C' . $suppliescellval, "$per_month");
            $spreadsheet->getActiveSheet()->setCellValue('D' . $suppliescellval, "$no_month");
            $spreadsheet->getActiveSheet()->setCellValue('E' . $suppliescellval, "$amount");

            $spreadsheet->getActiveSheet()->setCellValue('F' . $suppliescellval, "$eot_mm");
            $spreadsheet->getActiveSheet()->setCellValue('G' . $suppliescellval, "$eot_amount");
            $spreadsheet->getActiveSheet()->setCellValue('H' . $suppliescellval, "$rcv_mm");
            $spreadsheet->getActiveSheet()->setCellValue('I' . $suppliescellval, "$rcv_amount");

            $spreadsheet->getActiveSheet()->setCellValue('J' . $suppliescellval, "$cumprevmonth");
            $spreadsheet->getActiveSheet()->setCellValue('K' . $suppliescellval, "$cumprevamt");
            $spreadsheet->getActiveSheet()->setCellValue('L' . $suppliescellval, "$currentmmmonth");
            $spreadsheet->getActiveSheet()->setCellValue('M' . $suppliescellval, "$currentamount");
            $spreadsheet->getActiveSheet()->setCellValue('N' . $suppliescellval, "$totalcummm");
            $spreadsheet->getActiveSheet()->setCellValue('O' . $suppliescellval, "$totalcuamt");
            $spreadsheet->getActiveSheet()->setCellValue('P' . $suppliescellval, "$balancemm");
            $spreadsheet->getActiveSheet()->setCellValue('Q' . $suppliescellval, "$balanceamt");
            $suppliescellval = $suppliescellval + 1;
            $suppliesj++;
        }

        $spreadsheet->getActiveSheet()->setCellValue('A' . $suppliescellval, 'Total');
        $spreadsheet->getActiveSheet()->mergeCells("A$suppliescellval:C$suppliescellval");
        $spreadsheet->getActiveSheet()->setCellValue("E$suppliescellval", "$suppliestotalcontract");

        $spreadsheet->getActiveSheet()->setCellValue("G$suppliescellval", "$suppliestotaleot");
        $spreadsheet->getActiveSheet()->setCellValue("I$suppliescellval", "$suppliestotalrcv");

        $spreadsheet->getActiveSheet()->setCellValue("K$suppliescellval", "$suppliestotalcum");
        $spreadsheet->getActiveSheet()->setCellValue("M$suppliescellval", "$suppliestotalcur");
        $spreadsheet->getActiveSheet()->setCellValue("O$suppliescellval", "$suppliestotaltocum");
        $spreadsheet->getActiveSheet()->setCellValue("Q$suppliescellval", "$suppliestotalbal");
        $spreadsheet->getActiveSheet()->getStyle("A$suppliescellval:Q$suppliescellval")->getNumberFormat()->setFormatCode('#,##0');


        $spreadsheet->getActiveSheet()->getStyle("A$suppliescellval:Q$suppliescellval")->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => 'FF000000'],
                ],
            ]
                ]
        );

        $spreadsheet->getActiveSheet()->getStyle("A$suppliesblnkcellval:Q$suppliescellval")->applyFromArray(
                [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ]
                    ]
                ]
        );

        $spreadsheet->getActiveSheet()->getStyle("A$transblnkcellval:Q$transnxtcellvalchk")->applyFromArray(
                [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_NONE,
                            'color' => ['rgb' => 'FFFFFF'],
                        ]
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A$travelblnkcellval:Q$travelnxtcellvalchk")->applyFromArray(
                [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_NONE,
                            'color' => ['rgb' => 'FFFFFF'],
                        ]
                    ]
                ]
        );
        //$spreadsheet->getActiveSheet()->mergeCells("A$travelblnkcellval:Q$travelnxtcellvalchk");
        //A$suppliesblnkcellval:Q$suppliesnxtcellvalchk

        $spreadsheet->getActiveSheet()->getStyle("A$suppliesblnkcellval:Q$suppliesnxtcellvalchk")->applyFromArray(
                [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_NONE,
                            'color' => ['rgb' => 'FFFFFF'],
                        ]
                    ]
                ]
        );











        // Custom Style Sheet V ..
        ///////////////////////////////////////////////////////
        //$spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(10);
//
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(9);
        $spreadsheet->getActiveSheet()->getStyle('A3:Q100', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D4:Q4', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getRowDimension('3')->setRowHeight(35);
        $spreadsheet->getActiveSheet()->getStyle('B5:B40', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->getStyle('A20:P20', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D21:Q21', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A5:Q100')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle("A5:A100")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet->getActiveSheet()->getPageMargins()->setTop(1);
        $spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.75);
        $spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.75);
        $spreadsheet->getActiveSheet()->getPageMargins()->setBottom(1);

        ////////////////////////////////////////////
        $spreadsheet->getActiveSheet()->getStyle('C5:C100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('E5:E100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('I5:I100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('K5:K100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('M5:M100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('O5:O100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('Q5:Q100')->getNumberFormat()->setFormatCode('#,##0');

        $spreadsheet->getActiveSheet()->getStyle('A3:R3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A3:R3')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);


        ///////////////////////////////////////////
        //Number Formate Code Excel ...
        //$spreadsheet->getActiveSheet()->getStyle('C5:C100')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        // $spreadsheet->getActiveSheet()->getStyle('C5:C100')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        $spreadsheet->getActiveSheet()->getStyle('D5:D50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('E5:E50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('F5:F50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        $spreadsheet->getActiveSheet()->getStyle('G5:G50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('H5:H50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('I5:I50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('J5:J50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('K5:K50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('L5:L50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('M5:M50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('N5:N50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('O5:O50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('P5:P50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('Q5:Q50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);

        $spreadsheet->getActiveSheet()->getPageSetup()->setPrintArea('A1:Q50');



        //echo '<pre>';	 print_r($transportation); die;
        $spreadsheet->getActiveSheet()->setTitle('(3_4_5_6)');
        $spreadsheet->setActiveSheetIndex(0);

        /////////////////////////////////////////////Sheet 5 /////////////////////////////////////////
        // Create a new worksheet, after the default sheet
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(5);

        $spreadsheet->getActiveSheet()->setCellValue('A1', '(7) Office Furniture and Equipment (Rental Fix Rate)');
        $spreadsheet->getActiveSheet()->mergeCells('A1:S1');

        $spreadsheet->getActiveSheet()->setCellValue('A2', '');
        $spreadsheet->getActiveSheet()->mergeCells('A2:S2');

        $spreadsheet->getActiveSheet()->setCellValue('A3', 'S.No.');
        $spreadsheet->getActiveSheet()->mergeCells('A3:A4');

        $spreadsheet->getActiveSheet()->setCellValue('B3', 'Description');
        $spreadsheet->getActiveSheet()->mergeCells('B3:B4');

        $spreadsheet->getActiveSheet()->setCellValue('C3', 'Unit');
        $spreadsheet->getActiveSheet()->mergeCells('C3:C4');

        $spreadsheet->getActiveSheet()->setCellValue('D3', 'Period in Months');
        $spreadsheet->getActiveSheet()->mergeCells('D3:D4');

        $spreadsheet->getActiveSheet()->setCellValue('E3', 'Rate / Month(Rs.)');
        $spreadsheet->getActiveSheet()->mergeCells('E3:E4');

        $spreadsheet->getActiveSheet()->setCellValue('F3', 'Contract Value');
        $spreadsheet->getActiveSheet()->mergeCells('F3:G3');
        $spreadsheet->getActiveSheet()->setCellValue('F4', 'Quantity');
        $spreadsheet->getActiveSheet()->setCellValue('G4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('H3', 'Total EOT');
        $spreadsheet->getActiveSheet()->mergeCells('H3:I3');
        $spreadsheet->getActiveSheet()->setCellValue('H4', 'Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('I4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('J3', 'Revised  Contract Value');
        $spreadsheet->getActiveSheet()->mergeCells('J3:K3');
        $spreadsheet->getActiveSheet()->setCellValue('J4', 'Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('K4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('L3', 'Cumulative Up to Previous Bill');
        $spreadsheet->getActiveSheet()->mergeCells('L3:M3');
        $spreadsheet->getActiveSheet()->setCellValue('L4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('M4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('N3', 'Current Month Bill');
        $spreadsheet->getActiveSheet()->mergeCells('N3:O3');
        $spreadsheet->getActiveSheet()->setCellValue('N4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('O4', 'Amount (INR)');


        $spreadsheet->getActiveSheet()->setCellValue('P3', 'Total Cumulative');
        $spreadsheet->getActiveSheet()->mergeCells('P3:Q3');
        $spreadsheet->getActiveSheet()->setCellValue('P4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('Q4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('R3', 'Balance');
        $spreadsheet->getActiveSheet()->mergeCells('R3:S3');
        $spreadsheet->getActiveSheet()->setCellValue('R4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('S4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->getStyle("A1:S4")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A1:S4")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $furniturecellval = '5';
        $furniturej = 1;
        $furnituretotalcontract = 0;
        $furnituretotalcum = 0;
        $furnituretotalcur = 0;
        $furnituretotaltocum = 0;
        $furnituretotalbal = 0;
        $furnituretotaleot = 0;
        $furnituretotalrcv = 0;

        for ($i = 0; $i < $furnitureID; $i++) {
            $officefurnitureeot = $this->Front_model->getreimbursableeot($invoicedata->project_id, $office_furniture[$i]->id);
            //$amount = $office_furniture[$i]->rate * $office_furniture[$i]->man_months;
            $description = $office_furniture[$i]->description;
            $unit = $office_furniture[$i]->unit;
            $per_month = $office_furniture[$i]->per_month;
            $no_month = $office_furniture[$i]->no_month;
            $qty = ($office_furniture[$i]->qty > 0) ? $office_furniture[$i]->qty : "";


            $amount = $per_month * $no_month * $qty;
            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $office_furniture[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $office_furniture[$i]->id, $invoicedata->invoice_date);

            //echo '<pre>'; print_r($currmonth); die;

            $cumprevmonth = $prevmonth[0]->cumulative_pre_mm;
            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;
            //$cumprevamt = ($per_month * $qty * $prevmonth[0]->cumulative_pre_amount);
            //$cumprevamt = ($per_month * $qty * $cumprevmonth);

            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month * $qty;

            $eot_mm = $officefurnitureeot->eot_mm;
            $rcv_mm = round($eot_mm + $no_month, 2);

            $eot_amount = $per_month * $qty * $eot_mm;
            $rcv_amount = $per_month * $qty * $rcv_mm;

            $totalcummm = $cumprevmonth + $currentmmmonth;
            $totalcuamt = $cumprevamt + $currentamount;

            $balancemm = $no_month + $eot_mm - $totalcummm;
            $balanceamt = $amount + $eot_amount - $totalcuamt;

            $furnituretotalcontract += $amount;
            $furnituretotaleot += $eot_amount;
            $furnituretotalrcv += $rcv_amount;
            $furnituretotalcum += $cumprevamt;
            $furnituretotalcur += $currentamount;
            $furnituretotaltocum += $totalcuamt;
            $furnituretotalbal += $balanceamt;


            $spreadsheet->getActiveSheet()->setCellValue('A' . $furniturecellval, "$furniturej");
            $spreadsheet->getActiveSheet()->setCellValue('B' . $furniturecellval, "$description");
            $spreadsheet->getActiveSheet()->setCellValue('C' . $furniturecellval, "$unit");
            $spreadsheet->getActiveSheet()->setCellValue('D' . $furniturecellval, "$no_month");
            $spreadsheet->getActiveSheet()->setCellValue('E' . $furniturecellval, "$per_month");
            $spreadsheet->getActiveSheet()->setCellValue('F' . $furniturecellval, "$qty");
            $spreadsheet->getActiveSheet()->setCellValue('G' . $furniturecellval, "$amount");

            $spreadsheet->getActiveSheet()->setCellValue('H' . $furniturecellval, "$eot_mm");
            $spreadsheet->getActiveSheet()->setCellValue('I' . $furniturecellval, "$eot_amount");
            $spreadsheet->getActiveSheet()->setCellValue('J' . $furniturecellval, "$rcv_mm");
            $spreadsheet->getActiveSheet()->setCellValue('K' . $furniturecellval, "$rcv_amount");

            $spreadsheet->getActiveSheet()->setCellValue('L' . $furniturecellval, "$cumprevmonth");
            $spreadsheet->getActiveSheet()->setCellValue('M' . $furniturecellval, "$cumprevamt");
            $spreadsheet->getActiveSheet()->setCellValue('N' . $furniturecellval, "$currentmmmonth");
            $spreadsheet->getActiveSheet()->setCellValue('O' . $furniturecellval, "$currentamount");
            $spreadsheet->getActiveSheet()->setCellValue('P' . $furniturecellval, "$totalcummm");
            $spreadsheet->getActiveSheet()->setCellValue('Q' . $furniturecellval, "$totalcuamt");
            $spreadsheet->getActiveSheet()->setCellValue('R' . $furniturecellval, "$balancemm");
            $spreadsheet->getActiveSheet()->setCellValue('S' . $furniturecellval, "$balanceamt");
            $furniturecellval = $furniturecellval + 1;
            $furniturej++;
        }

        //$spreadsheet->getActiveSheet()->setCellValue('A'.$cellval, '2');
        $spreadsheet->getActiveSheet()->setCellValue('A' . $furniturecellval, 'Total');
        $spreadsheet->getActiveSheet()->mergeCells("A$furniturecellval:C$furniturecellval");
        $spreadsheet->getActiveSheet()->setCellValue("G$furniturecellval", "$furnituretotalcontract");

        $spreadsheet->getActiveSheet()->setCellValue("I$furniturecellval", "$furnituretotaleot");
        $spreadsheet->getActiveSheet()->setCellValue("K$furniturecellval", "$furnituretotalrcv");

        $spreadsheet->getActiveSheet()->setCellValue("M$furniturecellval", "$furnituretotalcum");
        $spreadsheet->getActiveSheet()->setCellValue("O$furniturecellval", "$furnituretotalcur");
        $spreadsheet->getActiveSheet()->setCellValue("Q$furniturecellval", "$furnituretotaltocum");
        $spreadsheet->getActiveSheet()->setCellValue("S$furniturecellval", "$furnituretotalbal");
        $spreadsheet->getActiveSheet()->getStyle("A$furniturecellval:S$furniturecellval")->getNumberFormat()->setFormatCode('#,##0');




        $spreadsheet->getActiveSheet()->getStyle("A$furniturecellval:S$furniturecellval")->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'borders' => ['allBorders' => ['borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => 'FF000000'],
                ],
            ]
                ]
        );

        $spreadsheet->getActiveSheet()->getStyle("A1:S$furniturecellval")->applyFromArray(
                [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ]
                    ]
                ]
        );

        // Custom Style Sheet V ..
        ///////////////////////////////////////////////////////
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(10);
//
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(9);
        $spreadsheet->getActiveSheet()->getStyle('A3:P3', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D4:S4', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A5:S100')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle("A5:A100")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $spreadsheet->getActiveSheet()->getStyle('A20:P20', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D21:Q21', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('B5:B19', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getRowDimension(3)->setRowHeight(35);
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
        $spreadsheet->getActiveSheet()->getPageMargins()->setTop(1);
        $spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.75);
        $spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.75);
        $spreadsheet->getActiveSheet()->getPageMargins()->setBottom(1);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPrintArea('A1:S21');

        //Number Formate Code Excel ...
///////////////////////////////////////////////////////////////////
        $spreadsheet->getActiveSheet()->getStyle('A3:S3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A3:S3')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('E5:E100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('G5:G100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('K5:K100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('M5:M100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('O5:O100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('Q5:Q100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('S5:S100')->getNumberFormat()->setFormatCode('#,##0');

///////////////////////////////////////////////////////////////////
        $spreadsheet->getActiveSheet()->getStyle('C5:C50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('D5:D50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('E5:E50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('F5:F50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('G5:G50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('H5:H50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        $spreadsheet->getActiveSheet()->getStyle('I5:I50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('J5:J50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('K5:K50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('L5:L50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('M5:M50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('N5:N50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('O5:O50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('P5:P50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('Q5:Q50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('R5:R50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('S5:Q50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);

        $spreadsheet->getActiveSheet()->setTitle('(7)');
        $spreadsheet->setActiveSheetIndex(0);

        // Create a new worksheet, after the default sheet
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(6);

        $spreadsheet->getActiveSheet()->setCellValue('A1', '(8) Office Equipment (Rental / Hire)');
        $spreadsheet->getActiveSheet()->mergeCells('A1:S1');

        $spreadsheet->getActiveSheet()->setCellValue('A2', '');
        $spreadsheet->getActiveSheet()->mergeCells('A2:S2');

        $spreadsheet->getActiveSheet()->setCellValue('A3', 'S.No.');
        $spreadsheet->getActiveSheet()->mergeCells('A3:A4');

        $spreadsheet->getActiveSheet()->setCellValue('B3', 'Description');
        $spreadsheet->getActiveSheet()->mergeCells('B3:B4');

        $spreadsheet->getActiveSheet()->setCellValue('C3', 'Unit');
        $spreadsheet->getActiveSheet()->mergeCells('C3:C4');

        $spreadsheet->getActiveSheet()->setCellValue('D3', 'Period in Months');
        $spreadsheet->getActiveSheet()->mergeCells('D3:D4');

        $spreadsheet->getActiveSheet()->setCellValue('E3', 'Rate / Month(Rs.)');
        $spreadsheet->getActiveSheet()->mergeCells('E3:E4');

        $spreadsheet->getActiveSheet()->setCellValue('F3', 'Contract Value');
        $spreadsheet->getActiveSheet()->mergeCells('F3:G3');
        $spreadsheet->getActiveSheet()->setCellValue('F4', 'Quantity');
        $spreadsheet->getActiveSheet()->setCellValue('G4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('H3', 'Total EOT');
        $spreadsheet->getActiveSheet()->mergeCells('H3:I3');
        $spreadsheet->getActiveSheet()->setCellValue('H4', 'Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('I4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('J3', 'Revised  Contract Value');
        $spreadsheet->getActiveSheet()->mergeCells('J3:K3');
        $spreadsheet->getActiveSheet()->setCellValue('J4', 'Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('K4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('L3', 'Cumulative Up to Previous Bill');
        $spreadsheet->getActiveSheet()->mergeCells('L3:M3');
        $spreadsheet->getActiveSheet()->setCellValue('L4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('M4', 'Amount (INR)');


        $spreadsheet->getActiveSheet()->setCellValue('N3', 'Current Month Bill');
        $spreadsheet->getActiveSheet()->mergeCells('N3:O3');
        $spreadsheet->getActiveSheet()->setCellValue('N4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('O4', 'Amount (INR)');


        $spreadsheet->getActiveSheet()->setCellValue('P3', 'Total Cumulative');
        $spreadsheet->getActiveSheet()->mergeCells('P3:Q3');
        $spreadsheet->getActiveSheet()->setCellValue('P4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('Q4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('R3', 'Balance');
        $spreadsheet->getActiveSheet()->mergeCells('R3:S3');
        $spreadsheet->getActiveSheet()->setCellValue('R4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('S4', 'Amount (INR)');



//        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
//        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
//        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
//        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
//        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
//        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
//        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
//        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
//        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
//        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
//        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
//        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
//        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
//        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
//        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
//        $spreadsheet->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
//        $spreadsheet->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);

        $spreadsheet->getActiveSheet()->getStyle("A1:S4")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A1:S4")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $equipcellval = '6';
        $equipj = 1;
        $equiptotalcontract = 0;
        $equiptotalcum = 0;
        $equiptotalcur = 0;
        $equiptotaltocum = 0;
        $equiptotalbal = 0;
        $equiptotaleot = 0;
        $equiptotalrcv = 0;
        for ($i = 0; $i < $equipID; $i++) {
            //$amount = $office_furniture[$i]->rate * $office_furniture[$i]->man_months;
            $officeequeot = $this->Front_model->getreimbursableeot($invoicedata->project_id, $office_equ[$i]->id);
            $description = $office_equ[$i]->description;
            $unit = $office_equ[$i]->unit;
            $per_month = $office_equ[$i]->per_month;
            $no_month = $office_equ[$i]->no_month;

            $qty = ($office_equ[$i]->qty > 0) ? $office_equ[$i]->qty : "1";
            $amount = $per_month * $no_month * $qty;
            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $office_equ[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $office_equ[$i]->id, $invoicedata->invoice_date);
            //echo '<pre>'; print_r($currmonth); die;
            $eot_mm = $officeequeot->eot_mm;
            $rcv_mm = round($eot_mm + $no_month, 2);

            $eot_amount = $per_month * $qty * $eot_mm;
            $rcv_amount = $per_month * $qty * $rcv_mm;
            $cumprevmonth = $prevmonth[0]->cumulative_pre_mm;
            //$cumprevamt = ($per_month * $qty * $prevmonth[0]->cumulative_pre_amount);
            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month * $qty;

            $totalcummm = $cumprevmonth + $currentmmmonth;
            $totalcuamt = $cumprevamt + $currentamount;

            $balancemm = $no_month + $eot_mm - $totalcummm;
            $balanceamt = $amount + $eot_amount - $totalcuamt;

            $equiptotalcontract += $amount;
            $equiptotaleot += $eot_amount;
            $equiptotalrcv += $rcv_amount;
            $equiptotalcum += $cumprevamt;
            $equiptotalcur += $currentamount;
            $equiptotaltocum += $totalcuamt;
            $equiptotalbal += $balanceamt;

            $spreadsheet->getActiveSheet()->setCellValue('A' . $equipcellval, "$equipj");
            $spreadsheet->getActiveSheet()->setCellValue('B' . $equipcellval, "$description");
            $spreadsheet->getActiveSheet()->setCellValue('C' . $equipcellval, "$unit");
            $spreadsheet->getActiveSheet()->setCellValue('D' . $equipcellval, "$no_month");
            $spreadsheet->getActiveSheet()->setCellValue('E' . $equipcellval, "$per_month");
            $spreadsheet->getActiveSheet()->setCellValue('F' . $equipcellval, "$qty");
            $spreadsheet->getActiveSheet()->setCellValue('G' . $equipcellval, "$amount");

            $spreadsheet->getActiveSheet()->setCellValue('H' . $equipcellval, "$eot_mm");
            $spreadsheet->getActiveSheet()->setCellValue('I' . $equipcellval, "$eot_amount");
            $spreadsheet->getActiveSheet()->setCellValue('J' . $equipcellval, "$rcv_mm");
            $spreadsheet->getActiveSheet()->setCellValue('K' . $equipcellval, "$rcv_amount");

            $spreadsheet->getActiveSheet()->setCellValue('L' . $equipcellval, "$cumprevmonth");
            $spreadsheet->getActiveSheet()->setCellValue('M' . $equipcellval, "$cumprevamt");
            $spreadsheet->getActiveSheet()->setCellValue('N' . $equipcellval, "$currentmmmonth");
            $spreadsheet->getActiveSheet()->setCellValue('O' . $equipcellval, "$currentamount");
            $spreadsheet->getActiveSheet()->setCellValue('P' . $equipcellval, "$totalcummm");
            $spreadsheet->getActiveSheet()->setCellValue('Q' . $equipcellval, "$totalcuamt");
            $spreadsheet->getActiveSheet()->setCellValue('R' . $equipcellval, "$balancemm");
            $spreadsheet->getActiveSheet()->setCellValue('S' . $equipcellval, "$balanceamt");
            $equipcellval = $equipcellval + 1;
            $equipj++;
        }

        //$spreadsheet->getActiveSheet()->setCellValue('A'.$cellval, '2');
        $spreadsheet->getActiveSheet()->setCellValue('A' . $equipcellval, 'Total');
        $spreadsheet->getActiveSheet()->mergeCells("A$equipcellval:C$equipcellval");
        $spreadsheet->getActiveSheet()->setCellValue("G$equipcellval", "$equiptotalcontract");

        $spreadsheet->getActiveSheet()->setCellValue("I$equipcellval", "$equiptotaleot");
        $spreadsheet->getActiveSheet()->setCellValue("K$equipcellval", "$equiptotalrcv");

        $spreadsheet->getActiveSheet()->setCellValue("M$equipcellval", "$equiptotalcum");
        $spreadsheet->getActiveSheet()->setCellValue("O$equipcellval", "$equiptotalcur");
        $spreadsheet->getActiveSheet()->setCellValue("Q$equipcellval", "$equiptotaltocum");
        $spreadsheet->getActiveSheet()->setCellValue("S$equipcellval", "$equiptotalbal");
        $spreadsheet->getActiveSheet()->getStyle("A$equipcellval:S$equipcellval")->getNumberFormat()->setFormatCode('#,##0');


        $spreadsheet->getActiveSheet()->getStyle("A$equipcellval:S$equipcellval")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A1:S$equipcellval")->applyFromArray(
                [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ]
                    ]
                ]
        );

        //Code For Excel Formated VIII (B).
        //Number Formate Code Excel ...

        $spreadsheet->getActiveSheet()->getStyle('G5:G100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('I5:I100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('M5:M100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('K5:K100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('O5:O100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('P5:P100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('S5:S100')->getNumberFormat()->setFormatCode('#,##0');

        $spreadsheet->getActiveSheet()->getStyle('A3:S3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A3:S3')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);


        $spreadsheet->getActiveSheet()->getStyle('C5:C50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('D5:D50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        $spreadsheet->getActiveSheet()->getStyle('E5:E50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('F5:F50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('G5:G50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('H5:H50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('I5:I50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('J5:J50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('K5:K50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('L5:L50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //  $spreadsheet->getActiveSheet()->getStyle('M5:M50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('N5:N50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('O5:O50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('P5:P50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('Q5:Q50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('R5:R50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('S5:S50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        //////////////////////////
        // Custom Style Sheet V ..
        ///////////////////////////////////////////////////////
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(10);
//
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(9);
        $spreadsheet->getActiveSheet()->getStyle('A3:P3', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D4:S4', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A5:S100')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle("A5:A100")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getRowDimension(3)->setRowHeight(35);
        $spreadsheet->getActiveSheet()->getStyle('A20:P20', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D21:Q21', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('B5:B19', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
        $spreadsheet->getActiveSheet()->getPageMargins()->setTop(1);
        $spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.75);
        $spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.75);
        $spreadsheet->getActiveSheet()->getPageMargins()->setBottom(1);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPrintArea('A1:S21');

        $spreadsheet->getActiveSheet()->setTitle('(8)');
        $spreadsheet->setActiveSheetIndex(0);
        // Create a new worksheet, after the default sheet
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(7);

        //echo '<pre>'; print_r($reports); die;

        $spreadsheet->getActiveSheet()->setCellValue('A1', '(9) Reports and Document Printing ');
        $spreadsheet->getActiveSheet()->mergeCells('A1:S1');

        $spreadsheet->getActiveSheet()->setCellValue('A2', '');
        $spreadsheet->getActiveSheet()->mergeCells('A2:S2');

        $spreadsheet->getActiveSheet()->setCellValue('A3', 'S.No.');
        $spreadsheet->getActiveSheet()->mergeCells('A3:A4');

        $spreadsheet->getActiveSheet()->setCellValue('B3', 'Description');
        $spreadsheet->getActiveSheet()->mergeCells('B3:B4');

        $spreadsheet->getActiveSheet()->setCellValue('C3', 'No. of Reports');
        $spreadsheet->getActiveSheet()->mergeCells('C3:C4');

        $spreadsheet->getActiveSheet()->setCellValue('D3', 'No. of Copies per Report ');
        $spreadsheet->getActiveSheet()->mergeCells('D3:D4');

        $spreadsheet->getActiveSheet()->setCellValue('E3', 'Contract Value');
        $spreadsheet->getActiveSheet()->mergeCells('E3:G3');
        $spreadsheet->getActiveSheet()->setCellValue('E4', 'Total Nos. of Copies');
        $spreadsheet->getActiveSheet()->setCellValue('F4', 'Rate per Copy ');
        $spreadsheet->getActiveSheet()->setCellValue('G4', 'Amount (Rs.)');

        $spreadsheet->getActiveSheet()->setCellValue('H3', 'Total EOT');
        $spreadsheet->getActiveSheet()->mergeCells('H3:I3');
        $spreadsheet->getActiveSheet()->setCellValue('H4', 'Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('I4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('J3', 'Revised  Contract Value');
        $spreadsheet->getActiveSheet()->mergeCells('J3:K3');
        $spreadsheet->getActiveSheet()->setCellValue('J4', 'Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('K4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('L3', 'Cumulative Up to Previous Bill');
        $spreadsheet->getActiveSheet()->mergeCells('L3:M3');
        $spreadsheet->getActiveSheet()->setCellValue('L4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('M4', 'Amount (INR)');


        $spreadsheet->getActiveSheet()->setCellValue('N3', 'Current Month Bill');
        $spreadsheet->getActiveSheet()->mergeCells('N3:O3');
        $spreadsheet->getActiveSheet()->setCellValue('N4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('O4', 'Amount (INR)');


        $spreadsheet->getActiveSheet()->setCellValue('P3', 'Total Cumulative');
        $spreadsheet->getActiveSheet()->mergeCells('P3:Q3');
        $spreadsheet->getActiveSheet()->setCellValue('P4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('Q4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('R3', 'Balance');
        $spreadsheet->getActiveSheet()->mergeCells('R3:S3');
        $spreadsheet->getActiveSheet()->setCellValue('R4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('S4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->getStyle("A1:S4")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A1:S4")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        //echo '<pre>'; print_r($office_furniture); die;

        $reportcellval = '5';
        $reportj = 1;
        $reporttotalcontract = 0;
        $reporttotalcum = 0;
        $reporttotalcur = 0;
        $reporttotaltocum = 0;
        $reporttotalbal = 0;
        $reporttotaleot = 0;
        $reporttotalrcv = 0;

        for ($i = 0; $i < $reportID; $i++) {
            $reportseot = $this->Front_model->getreimbursableeot($invoicedata->project_id, $reports[$i]->id);
            //$amount = $office_furniture[$i]->rate * $office_furniture[$i]->man_months;
            $description = $reports[$i]->description;
            $qty = ($reports[$i]->qty > 0) ? $reports[$i]->qty : "1";
            $per_month = $reports[$i]->per_month;
            $no_month = $reports[$i]->no_month;
            $totalcopies = ($reports[$i]->no_month * $qty);
            $contractamt = ($totalcopies * $per_month);


            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $reports[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $reports[$i]->id, $invoicedata->invoice_date);

            //echo '<pre>'; print_r($currmonth); die;

            $eot_mm = $reportseot->eot_mm;
            $rcv_mm = round($eot_mm + $no_month, 2);

            $eot_amount = $per_month * $qty * $eot_mm;
            $rcv_amount = $per_month * $qty * $rcv_mm;

            $cumprevmonth = $prevmonth[0]->cumulative_pre_mm;
            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;

            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month * $qty;

            $totalcummm = $cumprevmonth + $currentmmmonth;
            $totalcuamt = $cumprevamt + $currentamount;

            $balancemm = $no_month + $eot_mm - $totalcummm;
            $balanceamt = $contractamt + $eot_amount - $totalcuamt;

            $reporttotalcontract += $contractamt;
            $reporttotaleot += $eot_amount;
            $reporttotalrcv += $rcv_amount;
            $reporttotalcum += $cumprevamt;
            $reporttotalcur += $currentamount;
            $reporttotaltocum += $totalcuamt;
            $reporttotalbal += $balanceamt;


            $spreadsheet->getActiveSheet()->setCellValue('A' . $reportcellval, "$reportj");
            $spreadsheet->getActiveSheet()->setCellValue('B' . $reportcellval, "$description");
            $spreadsheet->getActiveSheet()->setCellValue('C' . $reportcellval, "$no_month");
            $spreadsheet->getActiveSheet()->setCellValue('D' . $reportcellval, "$qty");
            $spreadsheet->getActiveSheet()->setCellValue('E' . $reportcellval, "$totalcopies");
            $spreadsheet->getActiveSheet()->setCellValue('F' . $reportcellval, "$per_month");
            $spreadsheet->getActiveSheet()->setCellValue('G' . $reportcellval, "$contractamt");

            $spreadsheet->getActiveSheet()->setCellValue('H' . $reportcellval, "$eot_mm");
            $spreadsheet->getActiveSheet()->setCellValue('I' . $reportcellval, "$eot_amount");
            $spreadsheet->getActiveSheet()->setCellValue('J' . $reportcellval, "$rcv_mm");
            $spreadsheet->getActiveSheet()->setCellValue('K' . $reportcellval, "$rcv_amount");

            $spreadsheet->getActiveSheet()->setCellValue('L' . $reportcellval, "$cumprevmonth");
            $spreadsheet->getActiveSheet()->setCellValue('M' . $reportcellval, "$cumprevamt");
            $spreadsheet->getActiveSheet()->setCellValue('N' . $reportcellval, "$currentmmmonth");
            $spreadsheet->getActiveSheet()->setCellValue('O' . $reportcellval, "$currentamount");
            $spreadsheet->getActiveSheet()->setCellValue('P' . $reportcellval, "$totalcummm");
            $spreadsheet->getActiveSheet()->setCellValue('Q' . $reportcellval, "$totalcuamt");
            $spreadsheet->getActiveSheet()->setCellValue('R' . $reportcellval, "$balancemm");
            $spreadsheet->getActiveSheet()->setCellValue('S' . $reportcellval, "$balanceamt");
            $reportcellval = $reportcellval + 1;
            $reportj++;
        }

        //$spreadsheet->getActiveSheet()->setCellValue('A'.$cellval, '2');
        $spreadsheet->getActiveSheet()->setCellValue('A' . $reportcellval, 'Total');
        $spreadsheet->getActiveSheet()->mergeCells("A$reportcellval:C$reportcellval");
        $spreadsheet->getActiveSheet()->setCellValue("G$reportcellval", "$reporttotalcontract");

        $spreadsheet->getActiveSheet()->setCellValue("I$reportcellval", "$reporttotaleot");
        $spreadsheet->getActiveSheet()->setCellValue("K$reportcellval", "$reporttotalrcv");

        $spreadsheet->getActiveSheet()->setCellValue("M$reportcellval", "$reporttotalcum");
        $spreadsheet->getActiveSheet()->setCellValue("O$reportcellval", "$reporttotalcur");
        $spreadsheet->getActiveSheet()->setCellValue("Q$reportcellval", "$reporttotaltocum");
        $spreadsheet->getActiveSheet()->setCellValue("S$reportcellval", "$reporttotalbal");
        $spreadsheet->getActiveSheet()->getStyle("A$reportcellval:S$reportcellval")->getNumberFormat()->setFormatCode('#,##0');

        $spreadsheet->getActiveSheet()->getStyle("A$reportcellval:S$reportcellval")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );

        $spreadsheet->getActiveSheet()->getStyle("A1:S$reportcellval")->applyFromArray(
                [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ]
                    ]
                ]
        );

        // Custom Style Sheet VIII ..
        ///////////////////////////////////////////////////////
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(10);
//
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(9);
        $spreadsheet->getActiveSheet()->getRowDimension(3)->setRowHeight(35);
        $spreadsheet->getActiveSheet()->getStyle('A3:P3', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D4:S4', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A20:P20', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D21:Q21', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('B5:B19', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A5:Q100')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle("A5:A100")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        //$spreadsheet->getActiveSheet()->getStyle('A5:Q100')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        //Code For Excel Formated VIII (B).
        //Number Formate Code Excel ...

        $spreadsheet->getActiveSheet()->getStyle('A3:S3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A3:S3')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

        $spreadsheet->getActiveSheet()->getStyle('G5:G100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('K5:K100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('M5:M100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('O5:O100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('Q5:Q100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('S5:S100')->getNumberFormat()->setFormatCode('#,##0');



        $spreadsheet->getActiveSheet()->getStyle('C5:C50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('D5:D50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('E5:E50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('F5:F50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        // $spreadsheet->getActiveSheet()->getStyle('G5:G50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('H5:H50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('I5:I50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('J5:J50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('K5:K50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('L5:L50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('M5:M50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('N5:N50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('O5:O50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('P5:P50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('Q5:Q50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('R5:R50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('S5:S50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);



        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet->getActiveSheet()->getPageMargins()->setTop(1);
        $spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.75);
        $spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.75);
        $spreadsheet->getActiveSheet()->getPageMargins()->setBottom(1);

        $spreadsheet->getActiveSheet()->getPageSetup()->setPrintArea('A1:S21');
        $spreadsheet->getActiveSheet()->setTitle('(9)');
        $spreadsheet->setActiveSheetIndex(0);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Create a new worksheet, after the default sheet
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(8);


        $survey_equ = gettransportation($invoicedata->project_id, 8);
        //echo '<pre>'; print_r($survey_equ); die;

        $spreadsheet->getActiveSheet()->setCellValue('A1', '(10) Survey Equipment with Survey Party and Vehicle etc. Complete');
        $spreadsheet->getActiveSheet()->mergeCells('A1:Q1');

        $spreadsheet->getActiveSheet()->setCellValue('A2', '');
        $spreadsheet->getActiveSheet()->mergeCells('A2:Q2');

        $spreadsheet->getActiveSheet()->setCellValue('A3', 'S.No.');
        $spreadsheet->getActiveSheet()->mergeCells('A3:A4');

        $spreadsheet->getActiveSheet()->setCellValue('B3', 'Description');
        $spreadsheet->getActiveSheet()->mergeCells('B3:B4');

        $spreadsheet->getActiveSheet()->setCellValue('C3', 'Nos. of Months');
        $spreadsheet->getActiveSheet()->mergeCells('C3:C4');

        $spreadsheet->getActiveSheet()->setCellValue('D3', 'Rate / Month (INR)');
        $spreadsheet->getActiveSheet()->mergeCells('D3:D4');

        $spreadsheet->getActiveSheet()->setCellValue('E3', 'Contract Value');
        $spreadsheet->getActiveSheet()->setCellValue('E4', 'Amount (Rs.)');

        $spreadsheet->getActiveSheet()->setCellValue('F3', 'Total EOT');
        $spreadsheet->getActiveSheet()->mergeCells('F3:G3');
        $spreadsheet->getActiveSheet()->setCellValue('F4', 'Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('G4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('H3', 'Revised  Contract Value');
        $spreadsheet->getActiveSheet()->mergeCells('H3:I3');
        $spreadsheet->getActiveSheet()->setCellValue('H4', 'Total Man Month');
        $spreadsheet->getActiveSheet()->setCellValue('I4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('J3', 'Cumulative Up to Previous Bill');
        $spreadsheet->getActiveSheet()->mergeCells('J3:K3');
        $spreadsheet->getActiveSheet()->setCellValue('J4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('K4', 'Amount (INR)');


        $spreadsheet->getActiveSheet()->setCellValue('L3', 'Current Month Bill');
        $spreadsheet->getActiveSheet()->mergeCells('L3:M3');
        $spreadsheet->getActiveSheet()->setCellValue('L4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('M4', 'Amount (INR)');


        $spreadsheet->getActiveSheet()->setCellValue('N3', 'Total Cumulative');
        $spreadsheet->getActiveSheet()->mergeCells('N3:O3');
        $spreadsheet->getActiveSheet()->setCellValue('N4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('O4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('P3', 'Balance');
        $spreadsheet->getActiveSheet()->mergeCells('P3:Q3');
        $spreadsheet->getActiveSheet()->setCellValue('P4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('Q4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->getStyle("A1:Q4")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A1:Q4")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        //echo '<pre>'; print_r($office_furniture); die;

        $survey_equID = count($survey_equ);
        $survey_equcellval = '5';
        $survey_equj = 1;
        $survey_equtotalcontract = 0;
        $survey_equtotalcum = 0;
        $survey_equtotalcur = 0;
        $survey_equtotaltocum = 0;
        $survey_equtotalbal = 0;
        $survey_equtotaleot = 0;
        $survey_equtotalrcv = 0;

        for ($i = 0; $i < $survey_equID; $i++) {
            $survey_equeot = $this->Front_model->getreimbursableeot($invoicedata->project_id, $survey_equ[$i]->id);
            //$amount = $office_furniture[$i]->rate * $office_furniture[$i]->man_months;
            $description = $survey_equ[$i]->description;
            $per_month = $survey_equ[$i]->per_month;
            $no_month = $survey_equ[$i]->no_month;
            $totalamt = ($no_month * $per_month);
            //$transtotalcontract += $amount;


            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $survey_equ[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $survey_equ[$i]->id, $invoicedata->invoice_date);

            $cumprevmonth = $prevmonth[0]->cumulative_pre_mm;
            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;
            //$cumprevamt = $per_month * $cumprevmonth;

            $eot_mm = $survey_equeot->eot_mm;
            $rcv_mm = round($eot_mm + $no_month, 2);

            $eot_amount = $per_month * $eot_mm;
            $rcv_amount = $per_month * $rcv_mm;


            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month;

            $totalcummm = $cumprevmonth + $currentmmmonth;
            $totalcuamt = $cumprevamt + $currentamount;

            $balancemm = $no_month + $eot_mm - $totalcummm;
            $balanceamt = $totalamt + $eot_amount - $totalcuamt;

            $survey_equtotalcontract += $totalamt;
            $survey_equtotaleot += $eot_amount;
            $survey_equtotalrcv += $rcv_amount;
            $survey_equtotalcum += $cumprevamt;
            $survey_equtotalcur += $currentamount;
            $survey_equtotaltocum += $totalcuamt;
            $survey_equtotalbal += $balanceamt;

            $spreadsheet->getActiveSheet()->setCellValue('A' . $survey_equcellval, "$survey_equj");
            $spreadsheet->getActiveSheet()->setCellValue('B' . $survey_equcellval, "$description");
            $spreadsheet->getActiveSheet()->setCellValue('C' . $survey_equcellval, "$no_month");
            $spreadsheet->getActiveSheet()->setCellValue('D' . $survey_equcellval, "$per_month");
            $spreadsheet->getActiveSheet()->setCellValue('E' . $survey_equcellval, "$totalamt");

            $spreadsheet->getActiveSheet()->setCellValue('F' . $survey_equcellval, "$eot_mm");
            $spreadsheet->getActiveSheet()->setCellValue('G' . $survey_equcellval, "$eot_amount");
            $spreadsheet->getActiveSheet()->setCellValue('H' . $survey_equcellval, "$rcv_mm");
            $spreadsheet->getActiveSheet()->setCellValue('I' . $survey_equcellval, "$rcv_amount");

            $spreadsheet->getActiveSheet()->setCellValue('J' . $survey_equcellval, "$cumprevmonth");
            $spreadsheet->getActiveSheet()->setCellValue('K' . $survey_equcellval, "$cumprevamt");
            $spreadsheet->getActiveSheet()->setCellValue('L' . $survey_equcellval, "$currentmmmonth");
            $spreadsheet->getActiveSheet()->setCellValue('M' . $survey_equcellval, "$currentamount");
            $spreadsheet->getActiveSheet()->setCellValue('N' . $survey_equcellval, "$totalcummm");
            $spreadsheet->getActiveSheet()->setCellValue('O' . $survey_equcellval, "$totalcuamt");
            $spreadsheet->getActiveSheet()->setCellValue('P' . $survey_equcellval, "$balancemm");
            $spreadsheet->getActiveSheet()->setCellValue('Q' . $survey_equcellval, "$balanceamt");
            $survey_equcellval = $survey_equcellval + 1;
            $survey_equj++;
        }

        //$spreadsheet->getActiveSheet()->setCellValue('A'.$cellval, '2');
        $spreadsheet->getActiveSheet()->setCellValue('A' . $survey_equcellval, 'Total');
        $spreadsheet->getActiveSheet()->mergeCells("A$survey_equcellval:C$survey_equcellval");
        $spreadsheet->getActiveSheet()->setCellValue("E$survey_equcellval", "$survey_equtotalcontract");
        $spreadsheet->getActiveSheet()->setCellValue("G$survey_equcellval", "$survey_equtotaleot");
        $spreadsheet->getActiveSheet()->setCellValue("I$survey_equcellval", "$survey_equtotalrcv");

        $spreadsheet->getActiveSheet()->setCellValue("K$survey_equcellval", "$survey_equtotalcum");
        $spreadsheet->getActiveSheet()->setCellValue("M$survey_equcellval", "$survey_equtotalcur");
        $spreadsheet->getActiveSheet()->setCellValue("O$survey_equcellval", "$survey_equtotaltocum");
        $spreadsheet->getActiveSheet()->setCellValue("Q$survey_equcellval", "$survey_equtotalbal");

        $spreadsheet->getActiveSheet()->getStyle("A$survey_equcellval:Q$survey_equcellval")->getNumberFormat()->setFormatCode('#,##0');

        $spreadsheet->getActiveSheet()->getStyle("A$survey_equcellval:Q$survey_equcellval")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );

        $spreadsheet->getActiveSheet()->getStyle("A1:Q$survey_equcellval")->applyFromArray(
                [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ]
                    ]
                ]
        );

        // Custom Style Sheet IX ..
        ///////////////////////////////////////////////////////
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(10);
//
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(9);
        $spreadsheet->getActiveSheet()->getStyle('A3:P3', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D4:S4', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);


        $spreadsheet->getActiveSheet()->getStyle('A20:P20', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D21:Q21', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);


        $spreadsheet->getActiveSheet()->getStyle('B5:B19', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->getStyle('A5:Q100')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle("A5:A100")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet->getActiveSheet()->getPageMargins()->setTop(1);
        $spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.75);
        $spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.75);
        $spreadsheet->getActiveSheet()->getPageMargins()->setBottom(1);

        $spreadsheet->getActiveSheet()->getPageSetup()->setPrintArea('A1:Q21');

        //Code For Excel Formated VIII (B).
        //Number Formate Code Excel ...
        $spreadsheet->getActiveSheet()->getStyle('A3:Q3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A3:Q3')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

        $spreadsheet->getActiveSheet()->getStyle('D5:D100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('E5:E100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('I5:I100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('K5:K100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('M5:M100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('O5:O100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('Q5:Q100')->getNumberFormat()->setFormatCode('#,##0');



        $spreadsheet->getActiveSheet()->getStyle('C5:C50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('D5:D50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        //$spreadsheet->getActiveSheet()->getStyle('E5:E50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('F5:F50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('G5:G50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('H5:H50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('I5:I50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('J5:J50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('K5:K50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('L5:L50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('M5:M50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('N5:N50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('O5:O50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('P5:P50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('Q5:Q50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('R5:R50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('S5:S50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);

        $spreadsheet->getActiveSheet()->setTitle('(10)');
        $spreadsheet->setActiveSheetIndex(0);

        // Create a new worksheet, after the default sheet
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(9);


        $road_survey_equ = gettransportation($invoicedata->project_id, 9);
        //echo '<pre>'; print_r($road_survey_equ); die;

        $spreadsheet->getActiveSheet()->setCellValue('A1', '(11) Road Survey Equipment');
        $spreadsheet->getActiveSheet()->mergeCells('A1:N1');

        $spreadsheet->getActiveSheet()->setCellValue('A2', '');
        $spreadsheet->getActiveSheet()->mergeCells('A2:N2');

        $spreadsheet->getActiveSheet()->setCellValue('A3', 'S.No.');
        $spreadsheet->getActiveSheet()->mergeCells('A3:A4');

        $spreadsheet->getActiveSheet()->setCellValue('B3', 'Description');
        $spreadsheet->getActiveSheet()->mergeCells('B3:B4');

        $spreadsheet->getActiveSheet()->setCellValue('C3', 'KM');
        $spreadsheet->getActiveSheet()->mergeCells('C3:C4');

        $spreadsheet->getActiveSheet()->setCellValue('D3', 'Rate');
        $spreadsheet->getActiveSheet()->mergeCells('D3:D4');

        $spreadsheet->getActiveSheet()->setCellValue('E3', 'Total No.');
        $spreadsheet->getActiveSheet()->mergeCells('E3:E4');

        $spreadsheet->getActiveSheet()->setCellValue('F3', 'Contract Value');
        $spreadsheet->getActiveSheet()->setCellValue('F4', 'Amount (Rs.)');

        $spreadsheet->getActiveSheet()->setCellValue('G3', 'Cumulative Up to Previous Bill');
        $spreadsheet->getActiveSheet()->mergeCells('G3:H3');
        $spreadsheet->getActiveSheet()->setCellValue('G4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('H4', 'Amount (INR)');


        $spreadsheet->getActiveSheet()->setCellValue('I3', 'Current Month Bill');
        $spreadsheet->getActiveSheet()->mergeCells('I3:J3');
        $spreadsheet->getActiveSheet()->setCellValue('I4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('J4', 'Amount (INR)');


        $spreadsheet->getActiveSheet()->setCellValue('K3', 'Total Cumulative');
        $spreadsheet->getActiveSheet()->mergeCells('K3:L3');
        $spreadsheet->getActiveSheet()->setCellValue('K4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('L4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('M3', 'Balance');
        $spreadsheet->getActiveSheet()->mergeCells('M3:N3');
        $spreadsheet->getActiveSheet()->setCellValue('M4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('N4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->getStyle("A1:N4")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A1:O4")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        //echo '<pre>'; print_r($office_furniture); die;

        $road_survey_equID = count($road_survey_equ);
        $road_survey_equcellval = '5';
        $road_survey_equj = 1;
        $road_survey_equtotalcontract = 0;
        $road_survey_equtotalcum = 0;
        $road_survey_equtotalcur = 0;
        $road_survey_equtotaltocum = 0;
        $road_survey_equtotalbal = 0;

        for ($i = 0; $i < $road_survey_equID; $i++) {
            //$amount = $office_furniture[$i]->rate * $office_furniture[$i]->man_months;
            $description = $road_survey_equ[$i]->description;
            $per_month = $road_survey_equ[$i]->per_month;
            $no_month = $road_survey_equ[$i]->no_month;
            $qty = ($road_survey_equ[$i]->qty > 0) ? $road_survey_equ[$i]->qty : "1";
            $totalamt = ($no_month * $per_month * $qty);
            //$transtotalcontract += $amount;


            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $road_survey_equ[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $road_survey_equ[$i]->id, $invoicedata->invoice_date);

            $cumprevmonth = $prevmonth[0]->cumulative_pre_mm;
            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;
            //$cumprevamt = $per_month * $cumprevmonth;

            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month;

            $totalcummm = $cumprevmonth + $currentmmmonth;
            $totalcuamt = $cumprevamt + $currentamount;

            $balancemm = $qty - $totalcummm;
            $balanceamt = $totalamt - $totalcuamt;

            $road_survey_equtotalcontract += $totalamt;
            $road_survey_equtotalcum += $cumprevamt;
            $road_survey_equtotalcur += $currentamount;
            $road_survey_equtotaltocum += $totalcuamt;
            $road_survey_equtotalbal += $balanceamt;



            $spreadsheet->getActiveSheet()->setCellValue('A' . $road_survey_equcellval, "$road_survey_equj");
            $spreadsheet->getActiveSheet()->setCellValue('B' . $road_survey_equcellval, "$description");
            $spreadsheet->getActiveSheet()->setCellValue('C' . $road_survey_equcellval, "$no_month");
            $spreadsheet->getActiveSheet()->setCellValue('D' . $road_survey_equcellval, "$per_month");
            $spreadsheet->getActiveSheet()->setCellValue('E' . $road_survey_equcellval, "$qty");
            $spreadsheet->getActiveSheet()->setCellValue('F' . $road_survey_equcellval, "$totalamt");
            $spreadsheet->getActiveSheet()->setCellValue('G' . $road_survey_equcellval, "$cumprevmonth");
            $spreadsheet->getActiveSheet()->setCellValue('H' . $road_survey_equcellval, "$cumprevamt");
            $spreadsheet->getActiveSheet()->setCellValue('I' . $road_survey_equcellval, "$currentmmmonth");
            $spreadsheet->getActiveSheet()->setCellValue('J' . $road_survey_equcellval, "$currentamount");
            $spreadsheet->getActiveSheet()->setCellValue('K' . $road_survey_equcellval, "$totalcummm");
            $spreadsheet->getActiveSheet()->setCellValue('L' . $road_survey_equcellval, "$totalcuamt");
            $spreadsheet->getActiveSheet()->setCellValue('M' . $road_survey_equcellval, "$balancemm");
            $spreadsheet->getActiveSheet()->setCellValue('N' . $road_survey_equcellval, "$balanceamt");
            $road_survey_equcellval = $road_survey_equcellval + 1;
            $road_survey_equj++;
        }

        //$spreadsheet->getActiveSheet()->setCellValue('A'.$cellval, '2');
        $spreadsheet->getActiveSheet()->setCellValue('A' . $road_survey_equcellval, 'Total');
        $spreadsheet->getActiveSheet()->mergeCells("A$road_survey_equcellval:C$road_survey_equcellval");
        $spreadsheet->getActiveSheet()->setCellValue("F$road_survey_equcellval", "$road_survey_equtotalcontract");
        $spreadsheet->getActiveSheet()->setCellValue("H$road_survey_equcellval", "$road_survey_equtotalcum");
        $spreadsheet->getActiveSheet()->setCellValue("J$road_survey_equcellval", "$road_survey_equtotalcur");
        $spreadsheet->getActiveSheet()->setCellValue("L$road_survey_equcellval", "$road_survey_equtotaltocum");
        $spreadsheet->getActiveSheet()->setCellValue("N$road_survey_equcellval", "$road_survey_equtotalbal");

        $spreadsheet->getActiveSheet()->getStyle("A$road_survey_equcellval:N$road_survey_equcellval")->getNumberFormat()->setFormatCode('#,##0');

        $spreadsheet->getActiveSheet()->getStyle("A$road_survey_equcellval:N$road_survey_equcellval")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );

        $spreadsheet->getActiveSheet()->getStyle("A1:N$road_survey_equcellval")->applyFromArray(
                [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ]
                    ]
                ]
        );


        // Custom Style Sheet X ..
        ///////////////////////////////////////////////////////
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(10);
//
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(9);
        $spreadsheet->getActiveSheet()->getStyle('A3:P3', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D4:S4', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->getStyle('A5:Q100')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle("A5:A100")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A20:P20', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D21:Q21', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->getStyle('B5:B19', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A5:Q5')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);



        $spreadsheet->getActiveSheet()->getPageMargins()->setTop(1);
        $spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.75);
        $spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.75);
        $spreadsheet->getActiveSheet()->getPageMargins()->setBottom(1);

        $spreadsheet->getActiveSheet()->getPageSetup()->setPrintArea('A1:N21');
        $spreadsheet->getActiveSheet()->getStyle('H5:H100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('F5:F100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('J5:J100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('L5:L100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('N6:N100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('A3:N3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A3:N3')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

        $spreadsheet->getActiveSheet()->getStyle('C5:C50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        $spreadsheet->getActiveSheet()->getStyle('D5:D50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('E5:E50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('F5:F50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('G5:G50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('H5:H50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('I5:I50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        /// $spreadsheet->getActiveSheet()->getStyle('J5:J50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('K5:K50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('L5:L50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('M5:M50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('N5:N50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        //  $spreadsheet->getActiveSheet()->getStyle('O5:O50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('P5:P50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('Q5:Q50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('R5:R50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('S5:S50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->setTitle('(11)');
        $spreadsheet->setActiveSheetIndex(0);
        // Create a new worksheet, after the default sheet


        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(10);
        $contingencies = gettransportation($invoicedata->project_id, 10);
        //echo '<pre>'; print_r($contingencies); die;
        $spreadsheet->getActiveSheet()->setCellValue('A1', '(12) Contingencies');
        $spreadsheet->getActiveSheet()->mergeCells('A1:L1');

        $spreadsheet->getActiveSheet()->setCellValue('A2', '');
        $spreadsheet->getActiveSheet()->mergeCells('A2:L2');

        $spreadsheet->getActiveSheet()->setCellValue('A3', 'S.No.');
        $spreadsheet->getActiveSheet()->mergeCells('A3:A4');

        $spreadsheet->getActiveSheet()->setCellValue('B3', 'Description');
        $spreadsheet->getActiveSheet()->mergeCells('B3:B4');

        $spreadsheet->getActiveSheet()->setCellValue('C3', 'Qty');
        $spreadsheet->getActiveSheet()->mergeCells('C3:C4');

        $spreadsheet->getActiveSheet()->setCellValue('D3', 'Contract Value');
        $spreadsheet->getActiveSheet()->setCellValue('D4', 'Amount (Rs.)');

        $spreadsheet->getActiveSheet()->setCellValue('E3', 'Cumulative Up to Previous Bill');
        $spreadsheet->getActiveSheet()->mergeCells('E3:F3');
        $spreadsheet->getActiveSheet()->setCellValue('E4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('F4', 'Amount (INR)');


        $spreadsheet->getActiveSheet()->setCellValue('G3', 'Current Month Bill');
        $spreadsheet->getActiveSheet()->mergeCells('G3:H3');
        $spreadsheet->getActiveSheet()->setCellValue('G4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('H4', 'Amount (INR)');


        $spreadsheet->getActiveSheet()->setCellValue('I3', 'Total Cumulative');
        $spreadsheet->getActiveSheet()->mergeCells('I3:J3');
        $spreadsheet->getActiveSheet()->setCellValue('I4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('J4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->setCellValue('K3', 'Balance');
        $spreadsheet->getActiveSheet()->mergeCells('K3:L3');
        $spreadsheet->getActiveSheet()->setCellValue('K4', 'Month');
        $spreadsheet->getActiveSheet()->setCellValue('L4', 'Amount (INR)');

        $spreadsheet->getActiveSheet()->getStyle("A1:L4")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A1:L4")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        //echo '<pre>'; print_r($office_furniture); die;
        $contingenciesID = count($contingencies);
        $contingenciescellval = '5';
        $contingenciesj = 1;
        $contingenciestotalcontract = 0;
        $contingenciestotalcum = 0;
        $contingenciestotalcur = 0;
        $contingenciestotaltocum = 0;
        $contingenciestotalbal = 0;

        for ($i = 0; $i < $contingenciesID; $i++) {
            //$amount = $office_furniture[$i]->rate * $office_furniture[$i]->man_months;
            $description = $contingencies[$i]->description;
            $per_month = $contingencies[$i]->per_month;
            $no_month = $contingencies[$i]->no_month;
            //$totalamt = ($no_month * $per_month);
            //$transtotalcontract += $amount;
            $prevmonth = getotherpreviousmonth($invoicedata->project_id, $invoiceID, $contingencies[$i]->id, $invoicedata->invoice_date);
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $contingencies[$i]->id, $invoicedata->invoice_date);
            $cumprevmonth = $prevmonth[0]->cumulative_pre_mm;
            $cumprevamt = $prevmonth[0]->cumulative_pre_amount;
            //$cumprevamt = $per_month * $cumprevmonth;
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = $currentmmmonth * $per_month;
            $totalcummm = $cumprevmonth + $currentmmmonth;
            $totalcuamt = $cumprevamt + $currentamount;
            $balancemm = $no_month - $totalcummm;
            $balanceamt = $per_month - $totalcuamt;

            $contingenciestotalcontract += $per_month;
            $contingenciestotalcum += $cumprevamt;
            $contingenciestotalcur += $currentamount;
            $contingenciestotaltocum += $totalcuamt;
            $contingenciestotalbal += $balanceamt;

            $spreadsheet->getActiveSheet()->setCellValue('A' . $contingenciescellval, "$contingenciesj");
            $spreadsheet->getActiveSheet()->setCellValue('B' . $contingenciescellval, "$description");
            $spreadsheet->getActiveSheet()->setCellValue('C' . $contingenciescellval, "$no_month");
            $spreadsheet->getActiveSheet()->setCellValue('D' . $contingenciescellval, "$per_month");
            $spreadsheet->getActiveSheet()->setCellValue('E' . $contingenciescellval, "$cumprevmonth");
            $spreadsheet->getActiveSheet()->setCellValue('F' . $contingenciescellval, "$cumprevamt");
            $spreadsheet->getActiveSheet()->setCellValue('G' . $contingenciescellval, "$currentmmmonth");
            $spreadsheet->getActiveSheet()->setCellValue('H' . $contingenciescellval, "$currentamount");
            $spreadsheet->getActiveSheet()->setCellValue('I' . $contingenciescellval, "$totalcummm");
            $spreadsheet->getActiveSheet()->setCellValue('J' . $contingenciescellval, "$totalcuamt");
            $spreadsheet->getActiveSheet()->setCellValue('K' . $contingenciescellval, "$balancemm");
            $spreadsheet->getActiveSheet()->setCellValue('L' . $contingenciescellval, "$balanceamt");
            $contingenciescellval = $contingenciescellval + 1;
            $contingenciesj++;
        }

        //$spreadsheet->getActiveSheet()->setCellValue('A'.$cellval, '2');
        $spreadsheet->getActiveSheet()->setCellValue('A' . $contingenciescellval, 'Total');
        $spreadsheet->getActiveSheet()->mergeCells("A$contingenciescellval:C$contingenciescellval");
        $spreadsheet->getActiveSheet()->setCellValue("D$contingenciescellval", "$contingenciestotalcontract");
        $spreadsheet->getActiveSheet()->setCellValue("F$contingenciescellval", "$contingenciestotalcum");
        $spreadsheet->getActiveSheet()->setCellValue("H$contingenciescellval", "$contingenciestotalcur");
        $spreadsheet->getActiveSheet()->setCellValue("J$contingenciescellval", "$contingenciestotaltocum");
        $spreadsheet->getActiveSheet()->setCellValue("L$contingenciescellval", "$contingenciestotalbal");
        $spreadsheet->getActiveSheet()->getStyle("A$contingenciescellval:L$contingenciescellval")->getNumberFormat()->setFormatCode('#,##0');


        $spreadsheet->getActiveSheet()->getStyle("A$contingenciescellval:L$contingenciescellval")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ]
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A1:L$contingenciescellval")->applyFromArray(
                [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ]
                    ]
                ]
        );

        // Custom Style Sheet X ..
        ///////////////////////////////////////////////////////
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(10);
//
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(9);
        $spreadsheet->getActiveSheet()->getStyle('A3:P3', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D4:S4', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);


        $spreadsheet->getActiveSheet()->getStyle('A20:P20', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('D21:Q21', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A5:Q100')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle("A5:A100")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $spreadsheet->getActiveSheet()->getStyle('B5:B19', $spreadsheet->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A5:Q5')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet->getActiveSheet()->getPageMargins()->setTop(1);
        $spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.75);
        $spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.75);
        $spreadsheet->getActiveSheet()->getPageMargins()->setBottom(1);

        $spreadsheet->getActiveSheet()->getPageSetup()->setPrintArea('A1:L21');

        $spreadsheet->getActiveSheet()->getStyle('A3:L3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A3:L3')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);


        $spreadsheet->getActiveSheet()->getStyle('D5:D100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('F5:F100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('H5:H100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('J5:J100')->getNumberFormat()->setFormatCode('#,##0');
        $spreadsheet->getActiveSheet()->getStyle('L5:J100')->getNumberFormat()->setFormatCode('#,##0');

        $spreadsheet->getActiveSheet()->getStyle('C5:C50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        // $spreadsheet->getActiveSheet()->getStyle('D5:D50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('E5:E50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        // $spreadsheet->getActiveSheet()->getStyle('F5:F50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('G5:G50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('H5:H50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('I5:I50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('J5:J50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getStyle('K5:K50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
        //$spreadsheet->getActiveSheet()->getStyle('L5:L50')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);

        $spreadsheet->getActiveSheet()->setTitle('(12)');
        $spreadsheet->setActiveSheetIndex(0);

        // Create a new worksheet, after the default sheet
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(11);


        $spreadsheet->getActiveSheet()->setCellValue('A1', 'ATTENDENCE SHEET TYU');
        $spreadsheet->getActiveSheet()->mergeCells('A1:AK3');
        $spreadsheet->getActiveSheet()->setCellValue('A4', 'Month / Year August 2018');
        $spreadsheet->getActiveSheet()->mergeCells('A4:AK4');

        $spreadsheet->getActiveSheet()->setCellValue('A5', 'S.No.');
        $spreadsheet->getActiveSheet()->setCellValue('B5', 'Designation');
        $spreadsheet->getActiveSheet()->setCellValue('C5', 'Name');
        $spreadsheet->getActiveSheet()->setCellValue('D5', 'Origin');
        $spreadsheet->getActiveSheet()->setCellValue('E5', '1');
        $spreadsheet->getActiveSheet()->setCellValue('F5', '2');
        $spreadsheet->getActiveSheet()->setCellValue('G5', '3');
        $spreadsheet->getActiveSheet()->setCellValue('H5', '4');
        $spreadsheet->getActiveSheet()->setCellValue('I5', '5');
        $spreadsheet->getActiveSheet()->setCellValue('J5', '6');
        $spreadsheet->getActiveSheet()->setCellValue('K5', '7');
        $spreadsheet->getActiveSheet()->setCellValue('L5', '8');
        $spreadsheet->getActiveSheet()->setCellValue('M5', '9');
        $spreadsheet->getActiveSheet()->setCellValue('N5', '10');
        $spreadsheet->getActiveSheet()->setCellValue('O5', '11');
        $spreadsheet->getActiveSheet()->setCellValue('P5', '12');
        $spreadsheet->getActiveSheet()->setCellValue('Q5', '13');
        $spreadsheet->getActiveSheet()->setCellValue('R5', '14');
        $spreadsheet->getActiveSheet()->setCellValue('S5', '15');
        $spreadsheet->getActiveSheet()->setCellValue('T5', '16');
        $spreadsheet->getActiveSheet()->setCellValue('U5', '17');
        $spreadsheet->getActiveSheet()->setCellValue('V5', '18');
        $spreadsheet->getActiveSheet()->setCellValue('W5', '19');
        $spreadsheet->getActiveSheet()->setCellValue('X5', '20');
        $spreadsheet->getActiveSheet()->setCellValue('Y5', '21');
        $spreadsheet->getActiveSheet()->setCellValue('Z5', '22');
        $spreadsheet->getActiveSheet()->setCellValue('AA5', '23');
        $spreadsheet->getActiveSheet()->setCellValue('AB5', '24');
        $spreadsheet->getActiveSheet()->setCellValue('AC5', '25');
        $spreadsheet->getActiveSheet()->setCellValue('AD5', '26');
        $spreadsheet->getActiveSheet()->setCellValue('AE5', '27');
        $spreadsheet->getActiveSheet()->setCellValue('AF5', '28');
        $spreadsheet->getActiveSheet()->setCellValue('AG5', '29');
        $spreadsheet->getActiveSheet()->setCellValue('AH5', '30');
        $spreadsheet->getActiveSheet()->setCellValue('AI5', '31');
        $spreadsheet->getActiveSheet()->setCellValue('AJ5', 'Total Days');
        $spreadsheet->getActiveSheet()->setCellValue('AK5', 'Remarks');

        $spreadsheet->getActiveSheet()->getStyle("A1:AK5")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A5:AK5")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $timesheetkey = '6';
        $j = 1;

        for ($i = 0; $i < $keyID; $i++) {
            //Code Modify By Asheesh 21-05-2019
            // $keyarr = $this->mastermodel->getmonthattdence($crumonths, $cruyears, $invoicedata->project_id, $keyteamdata[$i]->emp_id, $keyteamdata[$i]->designation_id);
            $keyarr = $this->mastermodel->getmonthattdence_new($crumonths, $cruyears, $invoicedata->project_id, $keyteamdata[$i]->emp_id, $keyteamdata[$i]->designation_id);

            $designation_name = $keyteamdata[$i]->designation;
            $userfullname = $keyteamdata[$i]->username;
            $key1 = isset($keyarr['1']) ? strtoupper($keyarr['1']) : '';
            $key2 = isset($keyarr['2']) ? strtoupper($keyarr['2']) : '';
            $key3 = isset($keyarr['3']) ? strtoupper($keyarr['3']) : '';
            $key4 = isset($keyarr['4']) ? strtoupper($keyarr['4']) : '';
            $key5 = isset($keyarr['5']) ? strtoupper($keyarr['5']) : '';
            $key6 = isset($keyarr['6']) ? strtoupper($keyarr['6']) : '';
            $key7 = isset($keyarr['7']) ? strtoupper($keyarr['7']) : '';
            $key8 = isset($keyarr['8']) ? strtoupper($keyarr['8']) : '';
            $key9 = isset($keyarr['9']) ? strtoupper($keyarr['9']) : '';
            $key10 = isset($keyarr['10']) ? strtoupper($keyarr['10']) : '';
            $key11 = isset($keyarr['11']) ? strtoupper($keyarr['11']) : '';
            $key12 = isset($keyarr['12']) ? strtoupper($keyarr['12']) : '';
            $key13 = isset($keyarr['13']) ? strtoupper($keyarr['13']) : '';
            $key14 = isset($keyarr['14']) ? strtoupper($keyarr['14']) : '';
            $key15 = isset($keyarr['15']) ? strtoupper($keyarr['15']) : '';
            $key16 = isset($keyarr['16']) ? strtoupper($keyarr['16']) : '';
            $key17 = isset($keyarr['17']) ? strtoupper($keyarr['17']) : '';
            $key18 = isset($keyarr['18']) ? strtoupper($keyarr['18']) : '';
            $key19 = isset($keyarr['19']) ? strtoupper($keyarr['19']) : '';
            $key20 = isset($keyarr['20']) ? strtoupper($keyarr['20']) : '';
            $key21 = isset($keyarr['21']) ? strtoupper($keyarr['21']) : '';
            $key22 = isset($keyarr['22']) ? strtoupper($keyarr['22']) : '';
            $key23 = isset($keyarr['23']) ? strtoupper($keyarr['23']) : '';
            $key24 = isset($keyarr['24']) ? strtoupper($keyarr['24']) : '';
            $key25 = isset($keyarr['25']) ? strtoupper($keyarr['25']) : '';
            $key26 = isset($keyarr['26']) ? strtoupper($keyarr['26']) : '';
            $key27 = isset($keyarr['27']) ? strtoupper($keyarr['27']) : '';
            $key28 = isset($keyarr['28']) ? strtoupper($keyarr['28']) : '';
            $key29 = isset($keyarr['29']) ? strtoupper($keyarr['29']) : '';
            $key30 = isset($keyarr['30']) ? strtoupper($keyarr['30']) : '';
            $key31 = isset($keyarr['31']) ? strtoupper($keyarr['31']) : '';

            //echo '<pre>';
            //echo $holidayArr[$i]['holiday_date'];



            $spreadsheet->getActiveSheet()->setCellValue('A' . $timesheetkey, "$j");
            $spreadsheet->getActiveSheet()->setCellValue('B' . $timesheetkey, "$designation_name");
            $spreadsheet->getActiveSheet()->setCellValue('C' . $timesheetkey, "$userfullname");
            $spreadsheet->getActiveSheet()->setCellValue('D' . $timesheetkey, "IN");
            $spreadsheet->getActiveSheet()->setCellValue('E' . $timesheetkey, "$key1");
            $spreadsheet->getActiveSheet()->setCellValue('F' . $timesheetkey, "$key2");
            $spreadsheet->getActiveSheet()->setCellValue('G' . $timesheetkey, "$key3");
            $spreadsheet->getActiveSheet()->setCellValue('H' . $timesheetkey, "$key4");
            $spreadsheet->getActiveSheet()->setCellValue('I' . $timesheetkey, "$key5");
            $spreadsheet->getActiveSheet()->setCellValue('J' . $timesheetkey, "$key6");
            $spreadsheet->getActiveSheet()->setCellValue('K' . $timesheetkey, "$key7");
            $spreadsheet->getActiveSheet()->setCellValue('L' . $timesheetkey, "$key8");
            $spreadsheet->getActiveSheet()->setCellValue('M' . $timesheetkey, "$key9");
            $spreadsheet->getActiveSheet()->setCellValue('N' . $timesheetkey, "$key10");
            $spreadsheet->getActiveSheet()->setCellValue('O' . $timesheetkey, "$key11");
            $spreadsheet->getActiveSheet()->setCellValue('P' . $timesheetkey, "$key12");
            $spreadsheet->getActiveSheet()->setCellValue('Q' . $timesheetkey, "$key13");
            $spreadsheet->getActiveSheet()->setCellValue('R' . $timesheetkey, "$key14");
            $spreadsheet->getActiveSheet()->setCellValue('S' . $timesheetkey, "$key15");
            $spreadsheet->getActiveSheet()->setCellValue('T' . $timesheetkey, "$key16");
            $spreadsheet->getActiveSheet()->setCellValue('U' . $timesheetkey, "$key17");
            $spreadsheet->getActiveSheet()->setCellValue('V' . $timesheetkey, "$key18");
            $spreadsheet->getActiveSheet()->setCellValue('W' . $timesheetkey, "$key19");
            $spreadsheet->getActiveSheet()->setCellValue('X' . $timesheetkey, "$key20");
            $spreadsheet->getActiveSheet()->setCellValue('Y' . $timesheetkey, "$key21");
            $spreadsheet->getActiveSheet()->setCellValue('Z' . $timesheetkey, "$key22");
            $spreadsheet->getActiveSheet()->setCellValue('AA' . $timesheetkey, "$key23");
            $spreadsheet->getActiveSheet()->setCellValue('AB' . $timesheetkey, "$key24");
            $spreadsheet->getActiveSheet()->setCellValue('AC' . $timesheetkey, "$key25");
            $spreadsheet->getActiveSheet()->setCellValue('AD' . $timesheetkey, "$key26");
            $spreadsheet->getActiveSheet()->setCellValue('AE' . $timesheetkey, "$key27");
            $spreadsheet->getActiveSheet()->setCellValue('AF' . $timesheetkey, "$key28");
            $spreadsheet->getActiveSheet()->setCellValue('AG' . $timesheetkey, "$key29");
            $spreadsheet->getActiveSheet()->setCellValue('AH' . $timesheetkey, "$key30");
            $spreadsheet->getActiveSheet()->setCellValue('AI' . $timesheetkey, "$key31");
            $spreadsheet->getActiveSheet()->setCellValue('AJ' . $timesheetkey, "$totalday");
            $spreadsheet->getActiveSheet()->setCellValue('AK' . $timesheetkey, "$remark");


            $timesheetkey = $timesheetkey + 1;
            $j++;
        }
        $fillcolortimesheetkey = $timesheetkey - 1;
        for ($i = 0; $i < count($holidayArr); $i++) {
            $datefound = date('d', strtotime($holidayArr[$i]['holiday_date']));
            $holidayname = $holidayArr[$i][name];
            if ($datefound == '01') {
                $spreadsheet->getActiveSheet()->getStyle("E6:E$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("E6:E$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("E6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("E6:E$fillcolortimesheetkey");
            }
            if ($datefound == '02') {
                $spreadsheet->getActiveSheet()->getStyle("F6:F$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("F6:F$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("F6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("F6:F$fillcolortimesheetkey");
            }
            if ($datefound == '03') {
                $spreadsheet->getActiveSheet()->getStyle("G6:G$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("G6:G$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("G6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("G6:G$fillcolortimesheetkey");
            }
            if ($datefound == '04') {
                $spreadsheet->getActiveSheet()->getStyle("H6:H$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("F6:H$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("H6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("H6:H$fillcolortimesheetkey");
            }
            if ($datefound == '05') {
                $spreadsheet->getActiveSheet()->getStyle("I6:I$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("I6:I$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("I6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("I6:I$fillcolortimesheetkey");
            }
            if ($datefound == '06') {
                $spreadsheet->getActiveSheet()->getStyle("J6:J$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("J6:J$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("J6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("J6:J$fillcolortimesheetkey");
            }
            if ($datefound == '07') {
                $spreadsheet->getActiveSheet()->getStyle("K6:K$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("K6:K$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("K6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("K6:K$fillcolortimesheetkey");
            }
            if ($datefound == '08') {
                $spreadsheet->getActiveSheet()->getStyle("L6:L$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("L6:L$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("L6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("L6:L$fillcolortimesheetkey");
            }
            if ($datefound == '09') {
                $spreadsheet->getActiveSheet()->getStyle("M6:M$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("M6:M$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("M6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("M6:M$fillcolortimesheetkey");
            }
            if ($datefound == '10') {
                $spreadsheet->getActiveSheet()->getStyle("N6:N$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("N6:N$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("N6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("N6:N$fillcolortimesheetkey");
            }
            if ($datefound == '11') {
                $spreadsheet->getActiveSheet()->getStyle("O6:O$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("O6:O$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("O6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("O6:O$fillcolortimesheetkey");
            }
            if ($datefound == '12') {
                $spreadsheet->getActiveSheet()->getStyle("P6:P$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("P6:P$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("P6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("P6:P$fillcolortimesheetkey");
            }
            if ($datefound == '13') {
                $spreadsheet->getActiveSheet()->getStyle("Q6:Q$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("Q6:Q$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("Q6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("Q6:Q$fillcolortimesheetkey");
            }
            if ($datefound == '14') {
                $spreadsheet->getActiveSheet()->getStyle("R6:R$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("R6:R$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("R6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("R6:R$fillcolortimesheetkey");
            }
            if ($datefound == '15') {
                $spreadsheet->getActiveSheet()->getStyle("S6:S$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("S6:S$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("S6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("S6:S$fillcolortimesheetkey");
            }
            if ($datefound == '16') {
                $spreadsheet->getActiveSheet()->getStyle("T6:T$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("T6:T$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("T6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("T6:T$fillcolortimesheetkey");
            }
            if ($datefound == '17') {
                $spreadsheet->getActiveSheet()->getStyle("U6:U$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("U6:U$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("U6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("U6:U$fillcolortimesheetkey");
            }
            if ($datefound == '18') {
                $spreadsheet->getActiveSheet()->getStyle("V6:V$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("V6:V$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("V6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("V6:V$fillcolortimesheetkey");
            }
            if ($datefound == '19') {
                $spreadsheet->getActiveSheet()->getStyle("W6:W$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("W6:W$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("W6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("W6:W$fillcolortimesheetkey");
            }
            if ($datefound == '20') {
                $spreadsheet->getActiveSheet()->getStyle("X6:X$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("X6:X$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("X6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("X6:X$fillcolortimesheetkey");
            }
            if ($datefound == '21') {
                $spreadsheet->getActiveSheet()->getStyle("Y6:Y$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("Y6:Y$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("Y6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("Y6:Y$fillcolortimesheetkey");
            }
            if ($datefound == '22') {
                $spreadsheet->getActiveSheet()->getStyle("Z6:Z$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("Z6:Z$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("Z6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("Z6:Z$fillcolortimesheetkey");
            }
            if ($datefound == '23') {
                $spreadsheet->getActiveSheet()->getStyle("AA6:AA$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AA6:AA$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AA6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AA6:AA$fillcolortimesheetkey");
            }
            if ($datefound == '24') {
                $spreadsheet->getActiveSheet()->getStyle("AB6:AB$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AB6:AB$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AB6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AB6:AB$fillcolortimesheetkey");
            }
            if ($datefound == '25') {
                $spreadsheet->getActiveSheet()->getStyle("AC6:AC$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AC6:AC$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AC6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AC6:AC$fillcolortimesheetkey");
            }
            if ($datefound == '26') {
                $spreadsheet->getActiveSheet()->getStyle("AD6:AD$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AD6:AD$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AD6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AD6:AD$fillcolortimesheetkey");
            }
            if ($datefound == '27') {
                $spreadsheet->getActiveSheet()->getStyle("AE6:AE$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AE6:AE$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AE6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AE6:AE$fillcolortimesheetkey");
            }
            if ($datefound == '28') {
                $spreadsheet->getActiveSheet()->getStyle("AF6:AF$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AF6:AF$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AF6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AF6:AF$fillcolortimesheetkey");
            }
            if ($datefound == '29') {
                $spreadsheet->getActiveSheet()->getStyle("AG6:AG$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AG6:AG$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AG6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AG6:AG$fillcolortimesheetkey");
            }
            if ($datefound == '30') {
                $spreadsheet->getActiveSheet()->getStyle("AH6:AH$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AH6:AH$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AH6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AH6:AH$fillcolortimesheetkey");
            }
            if ($datefound == '31') {
                $spreadsheet->getActiveSheet()->getStyle("AI6:AI$fillcolortimesheetkey")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AI6:AI$fillcolortimesheetkey")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AI6", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AI6:AI$fillcolortimesheetkey");
            }
        }

        //die;
        $spreadsheet->getActiveSheet()->setCellValue("A$timesheetkey", "Key Words");
        $spreadsheet->getActiveSheet()->mergeCells("A$timesheetkey:B$timesheetkey");
        $spreadsheet->getActiveSheet()->setCellValue("C$timesheetkey", "Origin=Consultaning Engineers Group");

        $nxttimesheetkey = $timesheetkey + 1;
        $spreadsheet->getActiveSheet()->setCellValue("A$nxttimesheetkey", "* Use this space as appropriate ");
        $spreadsheet->getActiveSheet()->mergeCells("A$nxttimesheetkey:B$nxttimesheetkey");
        $spreadsheet->getActiveSheet()->setCellValue("C$nxttimesheetkey", "P = Present");

        $blnktimesheetkey = $timesheetkey + 2;
        $afterblnktimesheetkeychk = $timesheetkey + 3;
        $afterblnktimesheetkeys = $afterblnktimesheetkeychk + 2;
        $afterblnktimesheetkeyss = $afterblnktimesheetkeys + 1;
        $afterblnktimesheetkey = $afterblnktimesheetkeyss + 1;

        $spreadsheet->getActiveSheet()->setCellValue('A' . $blnktimesheetkey, '');
        $spreadsheet->getActiveSheet()->mergeCells("A$blnktimesheetkey:AK$blnktimesheetkey");

        $spreadsheet->getActiveSheet()->setCellValue("A$afterblnktimesheetkeychk", 'ATTENDENCE SHEET');
        $spreadsheet->getActiveSheet()->mergeCells("A$afterblnktimesheetkeychk:AK$afterblnktimesheetkeys");

        $spreadsheet->getActiveSheet()->setCellValue("A$afterblnktimesheetkeyss", 'Month / Year August 2018');
        $spreadsheet->getActiveSheet()->mergeCells("A$afterblnktimesheetkeyss:AK$afterblnktimesheetkeyss");

        $spreadsheet->getActiveSheet()->setCellValue("A$afterblnktimesheetkey", 'S.No.');
        $spreadsheet->getActiveSheet()->setCellValue("B$afterblnktimesheetkey", 'Designation');
        $spreadsheet->getActiveSheet()->setCellValue("C$afterblnktimesheetkey", 'Name');
        $spreadsheet->getActiveSheet()->setCellValue("D$afterblnktimesheetkey", 'Origin');
        $spreadsheet->getActiveSheet()->setCellValue("E$afterblnktimesheetkey", '1');
        $spreadsheet->getActiveSheet()->setCellValue("F$afterblnktimesheetkey", '2');
        $spreadsheet->getActiveSheet()->setCellValue("G$afterblnktimesheetkey", '3');
        $spreadsheet->getActiveSheet()->setCellValue("H$afterblnktimesheetkey", '4');
        $spreadsheet->getActiveSheet()->setCellValue("I$afterblnktimesheetkey", '5');
        $spreadsheet->getActiveSheet()->setCellValue("J$afterblnktimesheetkey", '6');
        $spreadsheet->getActiveSheet()->setCellValue("K$afterblnktimesheetkey", '7');
        $spreadsheet->getActiveSheet()->setCellValue("L$afterblnktimesheetkey", '8');
        $spreadsheet->getActiveSheet()->setCellValue("M$afterblnktimesheetkey", '9');
        $spreadsheet->getActiveSheet()->setCellValue("N$afterblnktimesheetkey", '10');
        $spreadsheet->getActiveSheet()->setCellValue("O$afterblnktimesheetkey", '11');
        $spreadsheet->getActiveSheet()->setCellValue("P$afterblnktimesheetkey", '12');
        $spreadsheet->getActiveSheet()->setCellValue("Q$afterblnktimesheetkey", '13');
        $spreadsheet->getActiveSheet()->setCellValue("R$afterblnktimesheetkey", '14');
        $spreadsheet->getActiveSheet()->setCellValue("S$afterblnktimesheetkey", '15');
        $spreadsheet->getActiveSheet()->setCellValue("T$afterblnktimesheetkey", '16');
        $spreadsheet->getActiveSheet()->setCellValue("U$afterblnktimesheetkey", '17');
        $spreadsheet->getActiveSheet()->setCellValue("V$afterblnktimesheetkey", '18');
        $spreadsheet->getActiveSheet()->setCellValue("W$afterblnktimesheetkey", '19');
        $spreadsheet->getActiveSheet()->setCellValue("X$afterblnktimesheetkey", '20');
        $spreadsheet->getActiveSheet()->setCellValue("Y$afterblnktimesheetkey", '21');
        $spreadsheet->getActiveSheet()->setCellValue("Z$afterblnktimesheetkey", '22');
        $spreadsheet->getActiveSheet()->setCellValue("AA$afterblnktimesheetkey", '23');
        $spreadsheet->getActiveSheet()->setCellValue("AB$afterblnktimesheetkey", '24');
        $spreadsheet->getActiveSheet()->setCellValue("AC$afterblnktimesheetkey", '25');
        $spreadsheet->getActiveSheet()->setCellValue("AD$afterblnktimesheetkey", '26');
        $spreadsheet->getActiveSheet()->setCellValue("AE$afterblnktimesheetkey", '27');
        $spreadsheet->getActiveSheet()->setCellValue("AF$afterblnktimesheetkey", '28');
        $spreadsheet->getActiveSheet()->setCellValue("AG$afterblnktimesheetkey", '29');
        $spreadsheet->getActiveSheet()->setCellValue("AH$afterblnktimesheetkey", '30');
        $spreadsheet->getActiveSheet()->setCellValue("AI$afterblnktimesheetkey", '31');
        $spreadsheet->getActiveSheet()->setCellValue("AJ$afterblnktimesheetkey", 'Total Days');
        $spreadsheet->getActiveSheet()->setCellValue("AK$afterblnktimesheetkey", 'Remarks');

        $spreadsheet->getActiveSheet()->getStyle("A$afterblnktimesheetkeychk:AK$afterblnktimesheetkey")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A$afterblnktimesheetkey:AK$afterblnktimesheetkey")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $timesheetsub = $afterblnktimesheetkey + 1;
        $colortimesheetsub = $afterblnktimesheetkey + 1;
        $j = 1;

        for ($i = 0; $i < $subID; $i++) {
            $keyarr = $this->mastermodel->getmonthattdence_new($crumonths, $cruyears, $invoicedata->project_id, $subteamdata[$i]->emp_id, $subteamdata[$i]->designation_id);
            $designation_name = $subteamdata[$i]->designation;
            $userfullname = $subteamdata[$i]->username;
            $key1 = isset($keyarr['1']) ? strtoupper($keyarr['1']) : '';
            $key2 = isset($keyarr['2']) ? strtoupper($keyarr['2']) : '';
            $key3 = isset($keyarr['3']) ? strtoupper($keyarr['3']) : '';
            $key4 = isset($keyarr['4']) ? strtoupper($keyarr['4']) : '';
            $key5 = isset($keyarr['5']) ? strtoupper($keyarr['5']) : '';
            $key6 = isset($keyarr['6']) ? strtoupper($keyarr['6']) : '';
            $key7 = isset($keyarr['7']) ? strtoupper($keyarr['7']) : '';
            $key8 = isset($keyarr['8']) ? strtoupper($keyarr['8']) : '';
            $key9 = isset($keyarr['9']) ? strtoupper($keyarr['9']) : '';
            $key10 = isset($keyarr['10']) ? strtoupper($keyarr['10']) : '';
            $key11 = isset($keyarr['11']) ? strtoupper($keyarr['11']) : '';
            $key12 = isset($keyarr['12']) ? strtoupper($keyarr['12']) : '';
            $key13 = isset($keyarr['13']) ? strtoupper($keyarr['13']) : '';
            $key14 = isset($keyarr['14']) ? strtoupper($keyarr['14']) : '';
            $key15 = isset($keyarr['15']) ? strtoupper($keyarr['15']) : '';
            $key16 = isset($keyarr['16']) ? strtoupper($keyarr['16']) : '';
            $key17 = isset($keyarr['17']) ? strtoupper($keyarr['17']) : '';
            $key18 = isset($keyarr['18']) ? strtoupper($keyarr['18']) : '';
            $key19 = isset($keyarr['19']) ? strtoupper($keyarr['19']) : '';
            $key20 = isset($keyarr['20']) ? strtoupper($keyarr['20']) : '';
            $key21 = isset($keyarr['21']) ? strtoupper($keyarr['21']) : '';
            $key22 = isset($keyarr['22']) ? strtoupper($keyarr['22']) : '';
            $key23 = isset($keyarr['23']) ? strtoupper($keyarr['23']) : '';
            $key24 = isset($keyarr['24']) ? strtoupper($keyarr['24']) : '';
            $key25 = isset($keyarr['25']) ? strtoupper($keyarr['25']) : '';
            $key26 = isset($keyarr['26']) ? strtoupper($keyarr['26']) : '';
            $key27 = isset($keyarr['27']) ? strtoupper($keyarr['27']) : '';
            $key28 = isset($keyarr['28']) ? strtoupper($keyarr['28']) : '';
            $key29 = isset($keyarr['29']) ? strtoupper($keyarr['29']) : '';
            $key30 = isset($keyarr['30']) ? strtoupper($keyarr['30']) : '';
            $key31 = isset($keyarr['31']) ? strtoupper($keyarr['31']) : '';

            $spreadsheet->getActiveSheet()->setCellValue('A' . $timesheetsub, "$j");
            $spreadsheet->getActiveSheet()->setCellValue('B' . $timesheetsub, "$designation_name");
            $spreadsheet->getActiveSheet()->setCellValue('C' . $timesheetsub, "$userfullname");
            $spreadsheet->getActiveSheet()->setCellValue('D' . $timesheetsub, "IN");
            $spreadsheet->getActiveSheet()->setCellValue('E' . $timesheetsub, "$key1");
            $spreadsheet->getActiveSheet()->setCellValue('F' . $timesheetsub, "$key2");
            $spreadsheet->getActiveSheet()->setCellValue('G' . $timesheetsub, "$key3");
            $spreadsheet->getActiveSheet()->setCellValue('H' . $timesheetsub, "$key4");
            $spreadsheet->getActiveSheet()->setCellValue('I' . $timesheetsub, "$key5");
            $spreadsheet->getActiveSheet()->setCellValue('J' . $timesheetsub, "$key6");
            $spreadsheet->getActiveSheet()->setCellValue('K' . $timesheetsub, "$key7");
            $spreadsheet->getActiveSheet()->setCellValue('L' . $timesheetsub, "$key8");
            $spreadsheet->getActiveSheet()->setCellValue('M' . $timesheetsub, "$key9");
            $spreadsheet->getActiveSheet()->setCellValue('N' . $timesheetsub, "$key10");
            $spreadsheet->getActiveSheet()->setCellValue('O' . $timesheetsub, "$key11");
            $spreadsheet->getActiveSheet()->setCellValue('P' . $timesheetsub, "$key12");
            $spreadsheet->getActiveSheet()->setCellValue('Q' . $timesheetsub, "$key13");
            $spreadsheet->getActiveSheet()->setCellValue('R' . $timesheetsub, "$key14");
            $spreadsheet->getActiveSheet()->setCellValue('S' . $timesheetsub, "$key15");
            $spreadsheet->getActiveSheet()->setCellValue('T' . $timesheetsub, "$key16");
            $spreadsheet->getActiveSheet()->setCellValue('U' . $timesheetsub, "$key17");
            $spreadsheet->getActiveSheet()->setCellValue('V' . $timesheetsub, "$key18");
            $spreadsheet->getActiveSheet()->setCellValue('W' . $timesheetsub, "$key19");
            $spreadsheet->getActiveSheet()->setCellValue('X' . $timesheetsub, "$key20");
            $spreadsheet->getActiveSheet()->setCellValue('Y' . $timesheetsub, "$key21");
            $spreadsheet->getActiveSheet()->setCellValue('Z' . $timesheetsub, "$key22");
            $spreadsheet->getActiveSheet()->setCellValue('AA' . $timesheetsub, "$key23");
            $spreadsheet->getActiveSheet()->setCellValue('AB' . $timesheetsub, "$key24");
            $spreadsheet->getActiveSheet()->setCellValue('AC' . $timesheetsub, "$key25");
            $spreadsheet->getActiveSheet()->setCellValue('AD' . $timesheetsub, "$key26");
            $spreadsheet->getActiveSheet()->setCellValue('AE' . $timesheetsub, "$key27");
            $spreadsheet->getActiveSheet()->setCellValue('AF' . $timesheetsub, "$key28");
            $spreadsheet->getActiveSheet()->setCellValue('AG' . $timesheetsub, "$key29");
            $spreadsheet->getActiveSheet()->setCellValue('AH' . $timesheetsub, "$key30");
            $spreadsheet->getActiveSheet()->setCellValue('AI' . $timesheetsub, "$key31");
            $spreadsheet->getActiveSheet()->setCellValue('AJ' . $timesheetsub, "$totalday");
            $spreadsheet->getActiveSheet()->setCellValue('AK' . $timesheetsub, "$remark");

            $timesheetsub = $timesheetsub + 1;
            $j++;
        }

        $fillcolortimesheetsub = $timesheetsub - 1;
        for ($i = 0; $i < count($holidayArr); $i++) {
            $datefound = date('d', strtotime($holidayArr[$i]['holiday_date']));
            $holidayname = $holidayArr[$i][name];
            if ($datefound == '01') {
                $spreadsheet->getActiveSheet()->getStyle("E$colortimesheetsub:E$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("E$colortimesheetsub:E$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("E$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("E$colortimesheetsub:E$fillcolortimesheetsub");
            }
            if ($datefound == '02') {
                $spreadsheet->getActiveSheet()->getStyle("F$colortimesheetsub:F$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("F$colortimesheetsub:F$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("F$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("F$colortimesheetsub:F$fillcolortimesheetsub");
            }
            if ($datefound == '03') {
                $spreadsheet->getActiveSheet()->getStyle("G$colortimesheetsub:G$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("G$colortimesheetsub:G$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("G$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("G$colortimesheetsub:G$fillcolortimesheetsub");
            }
            if ($datefound == '04') {
                $spreadsheet->getActiveSheet()->getStyle("H$colortimesheetsub:H$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("F$colortimesheetsub:H$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("H$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("H$colortimesheetsub:H$fillcolortimesheetsub");
            }
            if ($datefound == '05') {
                $spreadsheet->getActiveSheet()->getStyle("I$colortimesheetsub:I$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("I$colortimesheetsub:I$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("I$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("I$colortimesheetsub:I$fillcolortimesheetsub");
            }
            if ($datefound == '06') {
                $spreadsheet->getActiveSheet()->getStyle("J$colortimesheetsub:J$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("J$colortimesheetsub:J$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("J$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("J$colortimesheetsub:J$fillcolortimesheetsub");
            }
            if ($datefound == '07') {
                $spreadsheet->getActiveSheet()->getStyle("K$colortimesheetsub:K$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("K$colortimesheetsub:K$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("K$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("K$colortimesheetsub:K$fillcolortimesheetsub");
            }
            if ($datefound == '08') {
                $spreadsheet->getActiveSheet()->getStyle("L$colortimesheetsub:L$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("L$colortimesheetsub:L$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("L$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("L$colortimesheetsub:L$fillcolortimesheetsub");
            }
            if ($datefound == '09') {
                $spreadsheet->getActiveSheet()->getStyle("M$colortimesheetsub:M$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("M$colortimesheetsub:M$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("M$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("M$colortimesheetsub:M$fillcolortimesheetsub");
            }
            if ($datefound == '10') {
                $spreadsheet->getActiveSheet()->getStyle("N$colortimesheetsub:N$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("N$colortimesheetsub:N$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("N$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("N$colortimesheetsub:N$fillcolortimesheetsub");
            }
            if ($datefound == '11') {
                $spreadsheet->getActiveSheet()->getStyle("O$colortimesheetsub:O$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("O$colortimesheetsub:O$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("O$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("O$colortimesheetsub:O$fillcolortimesheetsub");
            }
            if ($datefound == '12') {
                $spreadsheet->getActiveSheet()->getStyle("P$colortimesheetsub:P$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("P$colortimesheetsub:P$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("P$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("P$colortimesheetsub:P$fillcolortimesheetsub");
            }
            if ($datefound == '13') {
                $spreadsheet->getActiveSheet()->getStyle("Q$colortimesheetsub:Q$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("Q$colortimesheetsub:Q$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("Q$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("Q$colortimesheetsub:Q$fillcolortimesheetsub");
            }
            if ($datefound == '14') {
                $spreadsheet->getActiveSheet()->getStyle("R$colortimesheetsub:R$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("R$colortimesheetsub:R$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("R$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("R$colortimesheetsub:R$fillcolortimesheetsub");
            }
            if ($datefound == '15') {
                $spreadsheet->getActiveSheet()->getStyle("S$colortimesheetsub:S$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("S$colortimesheetsub:S$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("S$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("S$colortimesheetsub:S$fillcolortimesheetsub");
            }
            if ($datefound == '16') {
                $spreadsheet->getActiveSheet()->getStyle("T$colortimesheetsub:T$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("T$colortimesheetsub:T$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("T$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("T$colortimesheetsub:T$fillcolortimesheetsub");
            }
            if ($datefound == '17') {
                $spreadsheet->getActiveSheet()->getStyle("U$colortimesheetsub:U$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("U$colortimesheetsub:U$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("U$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("U$colortimesheetsub:U$fillcolortimesheetsub");
            }
            if ($datefound == '18') {
                $spreadsheet->getActiveSheet()->getStyle("V$colortimesheetsub:V$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("V$colortimesheetsub:V$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("V$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("V$colortimesheetsub:V$fillcolortimesheetsub");
            }
            if ($datefound == '19') {
                $spreadsheet->getActiveSheet()->getStyle("W$colortimesheetsub:W$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("W$colortimesheetsub:W$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("W$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("W$colortimesheetsub:W$fillcolortimesheetsub");
            }
            if ($datefound == '20') {
                $spreadsheet->getActiveSheet()->getStyle("X$colortimesheetsub:X$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("X$colortimesheetsub:X$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("X$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("X$colortimesheetsub:X$fillcolortimesheetsub");
            }
            if ($datefound == '21') {
                $spreadsheet->getActiveSheet()->getStyle("Y$colortimesheetsub:Y$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("Y$colortimesheetsub:Y$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("Y$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("Y$colortimesheetsub:Y$fillcolortimesheetsub");
            }
            if ($datefound == '22') {
                $spreadsheet->getActiveSheet()->getStyle("Z$colortimesheetsub:Z$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("Z$colortimesheetsub:Z$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("Z$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("Z$colortimesheetsub:Z$fillcolortimesheetsub");
            }
            if ($datefound == '23') {
                $spreadsheet->getActiveSheet()->getStyle("AA$colortimesheetsub:AA$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AA$colortimesheetsub:AA$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AA$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AA$colortimesheetsub:AA$fillcolortimesheetsub");
            }
            if ($datefound == '24') {
                $spreadsheet->getActiveSheet()->getStyle("AB$colortimesheetsub:AB$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AB$colortimesheetsub:AB$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AB$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AB$colortimesheetsub:AB$fillcolortimesheetsub");
            }
            if ($datefound == '25') {
                $spreadsheet->getActiveSheet()->getStyle("AC$colortimesheetsub:AC$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AC$colortimesheetsub:AC$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AC$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AC$colortimesheetsub:AC$fillcolortimesheetsub");
            }
            if ($datefound == '26') {
                $spreadsheet->getActiveSheet()->getStyle("AD$colortimesheetsub:AD$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AD$colortimesheetsub:AD$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AD$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AD$colortimesheetsub:AD$fillcolortimesheetsub");
            }
            if ($datefound == '27') {
                $spreadsheet->getActiveSheet()->getStyle("AE$colortimesheetsub:AE$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AE$colortimesheetsub:AE$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AE$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AE$colortimesheetsub:AE$fillcolortimesheetsub");
            }
            if ($datefound == '28') {
                $spreadsheet->getActiveSheet()->getStyle("AF$colortimesheetsub:AF$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AF$colortimesheetsub:AF$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AF$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AF$colortimesheetsub:AF$fillcolortimesheetsub");
            }
            if ($datefound == '29') {
                $spreadsheet->getActiveSheet()->getStyle("AG$colortimesheetsub:AG$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AG$colortimesheetsub:AG$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AG$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AG$colortimesheetsub:AG$fillcolortimesheetsub");
            }
            if ($datefound == '30') {
                $spreadsheet->getActiveSheet()->getStyle("AH$colortimesheetsub:AH$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AH$colortimesheetsub:AH$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AH$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AH$colortimesheetsub:AH$fillcolortimesheetsub");
            }
            if ($datefound == '31') {
                $spreadsheet->getActiveSheet()->getStyle("AI$colortimesheetsub:AI$fillcolortimesheetsub")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AI$colortimesheetsub:AI$fillcolortimesheetsub")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AI$colortimesheetsub", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AI$colortimesheetsub:AI$fillcolortimesheetsub");
            }
        }

        $spreadsheet->getActiveSheet()->setCellValue("A$timesheetsub", "Key Words");
        $spreadsheet->getActiveSheet()->mergeCells("A$timesheetsub:B$timesheetsub");
        $spreadsheet->getActiveSheet()->setCellValue("C$timesheetsub", "Origin=Consultaning Engineers Group");

        $nxttimesheetsub = $timesheetsub + 1;
        $spreadsheet->getActiveSheet()->setCellValue("A$nxttimesheetsub", "* Use this space as appropriate ");
        $spreadsheet->getActiveSheet()->mergeCells("A$nxttimesheetsub:B$nxttimesheetsub");
        $spreadsheet->getActiveSheet()->setCellValue("C$nxttimesheetsub", "P = Present");

        $blnktimesheetsub = $timesheetsub + 2;
        $afterblnktimesheetsubchk = $timesheetsub + 3;
        $afterblnktimesheetsubs = $afterblnktimesheetsubchk + 2;
        $afterblnktimesheetsubss = $afterblnktimesheetsubs + 1;
        $afterblnktimesheetsub = $afterblnktimesheetsubss + 1;
        $spreadsheet->getActiveSheet()->setCellValue('A' . $blnktimesheetsub, '');
        $spreadsheet->getActiveSheet()->mergeCells("A$blnktimesheetsub:AK$blnktimesheetsub");

        $spreadsheet->getActiveSheet()->setCellValue("A$afterblnktimesheetsubchk", 'ATTENDENCE SHEET');
        $spreadsheet->getActiveSheet()->mergeCells("A$afterblnktimesheetsubchk:AK$afterblnktimesheetsubs");

        $spreadsheet->getActiveSheet()->setCellValue("A$afterblnktimesheetsubss", 'Month / Year August 2018');
        $spreadsheet->getActiveSheet()->mergeCells("A$afterblnktimesheetsubss:AK$afterblnktimesheetsubss");

        $spreadsheet->getActiveSheet()->setCellValue("A$afterblnktimesheetsub", 'S.No.');
        $spreadsheet->getActiveSheet()->setCellValue("B$afterblnktimesheetsub", 'Designation');
        $spreadsheet->getActiveSheet()->setCellValue("C$afterblnktimesheetsub", 'Name');
        $spreadsheet->getActiveSheet()->setCellValue("D$afterblnktimesheetsub", 'Origin');
        $spreadsheet->getActiveSheet()->setCellValue("E$afterblnktimesheetsub", '1');
        $spreadsheet->getActiveSheet()->setCellValue("F$afterblnktimesheetsub", '2');
        $spreadsheet->getActiveSheet()->setCellValue("G$afterblnktimesheetsub", '3');
        $spreadsheet->getActiveSheet()->setCellValue("H$afterblnktimesheetsub", '4');
        $spreadsheet->getActiveSheet()->setCellValue("I$afterblnktimesheetsub", '5');
        $spreadsheet->getActiveSheet()->setCellValue("J$afterblnktimesheetsub", '6');
        $spreadsheet->getActiveSheet()->setCellValue("K$afterblnktimesheetsub", '7');
        $spreadsheet->getActiveSheet()->setCellValue("L$afterblnktimesheetsub", '8');
        $spreadsheet->getActiveSheet()->setCellValue("M$afterblnktimesheetsub", '9');
        $spreadsheet->getActiveSheet()->setCellValue("N$afterblnktimesheetsub", '10');
        $spreadsheet->getActiveSheet()->setCellValue("O$afterblnktimesheetsub", '11');
        $spreadsheet->getActiveSheet()->setCellValue("P$afterblnktimesheetsub", '12');
        $spreadsheet->getActiveSheet()->setCellValue("Q$afterblnktimesheetsub", '13');
        $spreadsheet->getActiveSheet()->setCellValue("R$afterblnktimesheetsub", '14');
        $spreadsheet->getActiveSheet()->setCellValue("S$afterblnktimesheetsub", '15');
        $spreadsheet->getActiveSheet()->setCellValue("T$afterblnktimesheetsub", '16');
        $spreadsheet->getActiveSheet()->setCellValue("U$afterblnktimesheetsub", '17');
        $spreadsheet->getActiveSheet()->setCellValue("V$afterblnktimesheetsub", '18');
        $spreadsheet->getActiveSheet()->setCellValue("W$afterblnktimesheetsub", '19');
        $spreadsheet->getActiveSheet()->setCellValue("X$afterblnktimesheetsub", '20');
        $spreadsheet->getActiveSheet()->setCellValue("Y$afterblnktimesheetsub", '21');
        $spreadsheet->getActiveSheet()->setCellValue("Z$afterblnktimesheetsub", '22');
        $spreadsheet->getActiveSheet()->setCellValue("AA$afterblnktimesheetsub", '23');
        $spreadsheet->getActiveSheet()->setCellValue("AB$afterblnktimesheetsub", '24');
        $spreadsheet->getActiveSheet()->setCellValue("AC$afterblnktimesheetsub", '25');
        $spreadsheet->getActiveSheet()->setCellValue("AD$afterblnktimesheetsub", '26');
        $spreadsheet->getActiveSheet()->setCellValue("AE$afterblnktimesheetsub", '27');
        $spreadsheet->getActiveSheet()->setCellValue("AF$afterblnktimesheetsub", '28');
        $spreadsheet->getActiveSheet()->setCellValue("AG$afterblnktimesheetsub", '29');
        $spreadsheet->getActiveSheet()->setCellValue("AH$afterblnktimesheetsub", '30');
        $spreadsheet->getActiveSheet()->setCellValue("AI$afterblnktimesheetsub", '31');
        $spreadsheet->getActiveSheet()->setCellValue("AJ$afterblnktimesheetsub", 'Total Days');
        $spreadsheet->getActiveSheet()->setCellValue("AK$afterblnktimesheetsub", 'Remarks');

        $spreadsheet->getActiveSheet()->getStyle("A$afterblnktimesheetsubchk:AK$afterblnktimesheetsub")->applyFromArray(
                [
                    'font' => [
                        'bold' => true,
                    ],
                ]
        );
        $spreadsheet->getActiveSheet()->getStyle("A$afterblnktimesheetsub:AK$afterblnktimesheetsub")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $timesheetadmin = $afterblnktimesheetsub + 1;
        $colortimesheetadmin = $afterblnktimesheetsub + 1;
        $j = 1;

        for ($i = 0; $i < $adminID; $i++) {
            $keyarr = $this->mastermodel->getmonthattdence_new($crumonths, $cruyears, $invoicedata->project_id, $adminteamdata[$i]->emp_id, $adminteamdata[$i]->designation_id);
            $designation_name = $adminteamdata[$i]->designation;
            $userfullname = $adminteamdata[$i]->username;
            $key1 = isset($keyarr['1']) ? strtoupper($keyarr['1']) : '';
            $key2 = isset($keyarr['2']) ? strtoupper($keyarr['2']) : '';
            $key3 = isset($keyarr['3']) ? strtoupper($keyarr['3']) : '';
            $key4 = isset($keyarr['4']) ? strtoupper($keyarr['4']) : '';
            $key5 = isset($keyarr['5']) ? strtoupper($keyarr['5']) : '';
            $key6 = isset($keyarr['6']) ? strtoupper($keyarr['6']) : '';
            $key7 = isset($keyarr['7']) ? strtoupper($keyarr['7']) : '';
            $key8 = isset($keyarr['8']) ? strtoupper($keyarr['8']) : '';
            $key9 = isset($keyarr['9']) ? strtoupper($keyarr['9']) : '';
            $key10 = isset($keyarr['10']) ? strtoupper($keyarr['10']) : '';
            $key11 = isset($keyarr['11']) ? strtoupper($keyarr['11']) : '';
            $key12 = isset($keyarr['12']) ? strtoupper($keyarr['12']) : '';
            $key13 = isset($keyarr['13']) ? strtoupper($keyarr['13']) : '';
            $key14 = isset($keyarr['14']) ? strtoupper($keyarr['14']) : '';
            $key15 = isset($keyarr['15']) ? strtoupper($keyarr['15']) : '';
            $key16 = isset($keyarr['16']) ? strtoupper($keyarr['16']) : '';
            $key17 = isset($keyarr['17']) ? strtoupper($keyarr['17']) : '';
            $key18 = isset($keyarr['18']) ? strtoupper($keyarr['18']) : '';
            $key19 = isset($keyarr['19']) ? strtoupper($keyarr['19']) : '';
            $key20 = isset($keyarr['20']) ? strtoupper($keyarr['20']) : '';
            $key21 = isset($keyarr['21']) ? strtoupper($keyarr['21']) : '';
            $key22 = isset($keyarr['22']) ? strtoupper($keyarr['22']) : '';
            $key23 = isset($keyarr['23']) ? strtoupper($keyarr['23']) : '';
            $key24 = isset($keyarr['24']) ? strtoupper($keyarr['24']) : '';
            $key25 = isset($keyarr['25']) ? strtoupper($keyarr['25']) : '';
            $key26 = isset($keyarr['26']) ? strtoupper($keyarr['26']) : '';
            $key27 = isset($keyarr['27']) ? strtoupper($keyarr['27']) : '';
            $key28 = isset($keyarr['28']) ? strtoupper($keyarr['28']) : '';
            $key29 = isset($keyarr['29']) ? strtoupper($keyarr['29']) : '';
            $key30 = isset($keyarr['30']) ? strtoupper($keyarr['30']) : '';
            $key31 = isset($keyarr['31']) ? strtoupper($keyarr['31']) : '';

            $spreadsheet->getActiveSheet()->setCellValue('A' . $timesheetadmin, "$j");
            $spreadsheet->getActiveSheet()->setCellValue('B' . $timesheetadmin, "$designation_name");
            $spreadsheet->getActiveSheet()->setCellValue('C' . $timesheetadmin, "$userfullname");
            $spreadsheet->getActiveSheet()->setCellValue('D' . $timesheetadmin, "IN");
            $spreadsheet->getActiveSheet()->setCellValue('E' . $timesheetadmin, "$key1");
            $spreadsheet->getActiveSheet()->setCellValue('F' . $timesheetadmin, "$key2");
            $spreadsheet->getActiveSheet()->setCellValue('G' . $timesheetadmin, "$key3");
            $spreadsheet->getActiveSheet()->setCellValue('H' . $timesheetadmin, "$key4");
            $spreadsheet->getActiveSheet()->setCellValue('I' . $timesheetadmin, "$key5");
            $spreadsheet->getActiveSheet()->setCellValue('J' . $timesheetadmin, "$key6");
            $spreadsheet->getActiveSheet()->setCellValue('K' . $timesheetadmin, "$key7");
            $spreadsheet->getActiveSheet()->setCellValue('L' . $timesheetadmin, "$key8");
            $spreadsheet->getActiveSheet()->setCellValue('M' . $timesheetadmin, "$key9");
            $spreadsheet->getActiveSheet()->setCellValue('N' . $timesheetadmin, "$key10");
            $spreadsheet->getActiveSheet()->setCellValue('O' . $timesheetadmin, "$key11");
            $spreadsheet->getActiveSheet()->setCellValue('P' . $timesheetadmin, "$key12");
            $spreadsheet->getActiveSheet()->setCellValue('Q' . $timesheetadmin, "$key13");
            $spreadsheet->getActiveSheet()->setCellValue('R' . $timesheetadmin, "$key14");
            $spreadsheet->getActiveSheet()->setCellValue('S' . $timesheetadmin, "$key15");
            $spreadsheet->getActiveSheet()->setCellValue('T' . $timesheetadmin, "$key16");
            $spreadsheet->getActiveSheet()->setCellValue('U' . $timesheetadmin, "$key17");
            $spreadsheet->getActiveSheet()->setCellValue('V' . $timesheetadmin, "$key18");
            $spreadsheet->getActiveSheet()->setCellValue('W' . $timesheetadmin, "$key19");
            $spreadsheet->getActiveSheet()->setCellValue('X' . $timesheetadmin, "$key20");
            $spreadsheet->getActiveSheet()->setCellValue('Y' . $timesheetadmin, "$key21");
            $spreadsheet->getActiveSheet()->setCellValue('Z' . $timesheetadmin, "$key22");
            $spreadsheet->getActiveSheet()->setCellValue('AA' . $timesheetadmin, "$key23");
            $spreadsheet->getActiveSheet()->setCellValue('AB' . $timesheetadmin, "$key24");
            $spreadsheet->getActiveSheet()->setCellValue('AC' . $timesheetadmin, "$key25");
            $spreadsheet->getActiveSheet()->setCellValue('AD' . $timesheetadmin, "$key26");
            $spreadsheet->getActiveSheet()->setCellValue('AE' . $timesheetadmin, "$key27");
            $spreadsheet->getActiveSheet()->setCellValue('AF' . $timesheetadmin, "$key28");
            $spreadsheet->getActiveSheet()->setCellValue('AG' . $timesheetadmin, "$key29");
            $spreadsheet->getActiveSheet()->setCellValue('AH' . $timesheetadmin, "$key30");
            $spreadsheet->getActiveSheet()->setCellValue('AI' . $timesheetadmin, "$key31");
            $spreadsheet->getActiveSheet()->setCellValue('AJ' . $timesheetadmin, "$totalday");
            $spreadsheet->getActiveSheet()->setCellValue('AK' . $timesheetadmin, "$remark");
            $timesheetadmin = $timesheetadmin + 1;
            $j++;
        }

        $fillcolortimesheetadmin = $timesheetadmin - 1;
        for ($i = 0; $i < count($holidayArr); $i++) {
            $datefound = date('d', strtotime($holidayArr[$i]['holiday_date']));
            $holidayname = $holidayArr[$i][name];
            if ($datefound == '01') {
                $spreadsheet->getActiveSheet()->getStyle("E$colortimesheetadmin:E$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("E$colortimesheetadmin:E$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("E$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("E$colortimesheetadmin:E$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("E$colortimesheetadmin:E$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("E$colortimesheetadmin:E$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("E$colortimesheetadmin:E$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '02') {
                $spreadsheet->getActiveSheet()->getStyle("F$colortimesheetadmin:F$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("F$colortimesheetadmin:F$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("F$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("F$colortimesheetadmin:F$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("F$colortimesheetadmin:F$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("F$colortimesheetadmin:F$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("F$colortimesheetadmin:F$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '03') {
                $spreadsheet->getActiveSheet()->getStyle("G$colortimesheetadmin:G$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("G$colortimesheetadmin:G$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("G$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("G$colortimesheetadmin:G$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("G$colortimesheetadmin:G$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("G$colortimesheetadmin:G$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("G$colortimesheetadmin:G$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '04') {
                $spreadsheet->getActiveSheet()->getStyle("H$colortimesheetadmin:H$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("F$colortimesheetadmin:H$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("H$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("H$colortimesheetadmin:H$fillcolortimesheetadmin");
                //Rotate text and align center Rahul - 18-2 90
                $spreadsheet->getActiveSheet()->getStyle("H$colortimesheetadmin:H$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("H$colortimesheetadmin:H$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("H$colortimesheetadmin:H$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '05') {
                $spreadsheet->getActiveSheet()->getStyle("I$colortimesheetadmin:I$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("I$colortimesheetadmin:I$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("I$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("I$colortimesheetadmin:I$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("I$colortimesheetadmin:I$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("I$colortimesheetadmin:I$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("I$colortimesheetadmin:I$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '06') {
                $spreadsheet->getActiveSheet()->getStyle("J$colortimesheetadmin:J$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("J$colortimesheetadmin:J$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("J$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("J$colortimesheetadmin:J$fillcolortimesheetadmin");
                $spreadsheet->getActiveSheet()->getStyle("J$colortimesheetadmin:J$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("J$colortimesheetadmin:J$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("J$colortimesheetadmin:J$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '07') {
                $spreadsheet->getActiveSheet()->getStyle("K$colortimesheetadmin:K$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("K$colortimesheetadmin:K$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("K$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("K$colortimesheetadmin:K$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("K$colortimesheetadmin:K$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("K$colortimesheetadmin:K$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("K$colortimesheetadmin:K$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '08') {
                $spreadsheet->getActiveSheet()->getStyle("L$colortimesheetadmin:L$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("L$colortimesheetadmin:L$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("L$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("L$colortimesheetadmin:L$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("L$colortimesheetadmin:L$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("L$colortimesheetadmin:L$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("L$colortimesheetadmin:L$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '09') {
                $spreadsheet->getActiveSheet()->getStyle("M$colortimesheetadmin:M$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("M$colortimesheetadmin:M$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("M$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("M$colortimesheetadmin:M$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("M$colortimesheetadmin:M$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("M$colortimesheetadmin:M$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("M$colortimesheetadmin:M$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '10') {
                $spreadsheet->getActiveSheet()->getStyle("N$colortimesheetadmin:N$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("N$colortimesheetadmin:N$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("N$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("N$colortimesheetadmin:N$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("N$colortimesheetadmin:N$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("N$colortimesheetadmin:N$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("N$colortimesheetadmin:N$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '11') {
                $spreadsheet->getActiveSheet()->getStyle("O$colortimesheetadmin:O$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("O$colortimesheetadmin:O$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("O$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("O$colortimesheetadmin:O$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("O$colortimesheetadmin:O$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("O$colortimesheetadmin:O$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("O$colortimesheetadmin:O$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '12') {
                $spreadsheet->getActiveSheet()->getStyle("P$colortimesheetadmin:P$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("P$colortimesheetadmin:P$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("P$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("P$colortimesheetadmin:P$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("P$colortimesheetadmin:P$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("P$colortimesheetadmin:P$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("P$colortimesheetadmin:P$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '13') {
                $spreadsheet->getActiveSheet()->getStyle("Q$colortimesheetadmin:Q$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("Q$colortimesheetadmin:Q$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("Q$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("Q$colortimesheetadmin:Q$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("Q$colortimesheetadmin:Q$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("Q$colortimesheetadmin:Q$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("Q$colortimesheetadmin:Q$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '14') {
                $spreadsheet->getActiveSheet()->getStyle("R$colortimesheetadmin:R$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("R$colortimesheetadmin:R$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("R$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("R$colortimesheetadmin:R$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("R$colortimesheetadmin:R$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("R$colortimesheetadmin:R$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("R$colortimesheetadmin:R$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '15') {
                $spreadsheet->getActiveSheet()->getStyle("S$colortimesheetadmin:S$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("S$colortimesheetadmin:S$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("S$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("S$colortimesheetadmin:S$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("S$colortimesheetadmin:S$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("S$colortimesheetadmin:S$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("S$colortimesheetadmin:S$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '16') {
                $spreadsheet->getActiveSheet()->getStyle("T$colortimesheetadmin:T$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("T$colortimesheetadmin:T$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("T$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("T$colortimesheetadmin:T$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("T$colortimesheetadmin:T$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("T$colortimesheetadmin:T$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("T$colortimesheetadmin:T$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '17') {
                $spreadsheet->getActiveSheet()->getStyle("U$colortimesheetadmin:U$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("U$colortimesheetadmin:U$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("U$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("U$colortimesheetadmin:U$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("U$colortimesheetadmin:U$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("U$colortimesheetadmin:U$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("U$colortimesheetadmin:U$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '18') {
                $spreadsheet->getActiveSheet()->getStyle("V$colortimesheetadmin:V$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("V$colortimesheetadmin:V$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("V$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("V$colortimesheetadmin:V$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("V$colortimesheetadmin:V$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("V$colortimesheetadmin:V$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("V$colortimesheetadmin:V$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '19') {
                $spreadsheet->getActiveSheet()->getStyle("W$colortimesheetadmin:W$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("W$colortimesheetadmin:W$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("W$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("W$colortimesheetadmin:W$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("W$colortimesheetadmin:W$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("W$colortimesheetadmin:W$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("W$colortimesheetadmin:W$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '20') {
                $spreadsheet->getActiveSheet()->getStyle("X$colortimesheetadmin:X$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("X$colortimesheetadmin:X$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("X$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("X$colortimesheetadmin:X$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("X$colortimesheetadmin:X$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("X$colortimesheetadmin:X$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("X$colortimesheetadmin:X$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '21') {
                $spreadsheet->getActiveSheet()->getStyle("Y$colortimesheetadmin:Y$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("Y$colortimesheetadmin:Y$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("Y$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("Y$colortimesheetadmin:Y$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("Y$colortimesheetadmin:Y$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("Y$colortimesheetadmin:Y$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("Y$colortimesheetadmin:Y$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '22') {
                $spreadsheet->getActiveSheet()->getStyle("Z$colortimesheetadmin:Z$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("Z$colortimesheetadmin:Z$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("Z$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("Z$colortimesheetadmin:Z$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("Z$colortimesheetadmin:Z$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("Z$colortimesheetadmin:Z$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("Z$colortimesheetadmin:Z$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '23') {
                $spreadsheet->getActiveSheet()->getStyle("AA$colortimesheetadmin:AA$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AA$colortimesheetadmin:AA$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AA$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AA$colortimesheetadmin:AA$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("AA$colortimesheetadmin:AA$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("AA$colortimesheetadmin:AA$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("AA$colortimesheetadmin:AA$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '24') {
                $spreadsheet->getActiveSheet()->getStyle("AB$colortimesheetadmin:AB$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AB$colortimesheetadmin:AB$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AB$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AB$colortimesheetadmin:AB$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("AB$colortimesheetadmin:AB$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("AB$colortimesheetadmin:AB$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("AB$colortimesheetadmin:AB$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '25') {
                $spreadsheet->getActiveSheet()->getStyle("AC$colortimesheetadmin:AC$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AC$colortimesheetadmin:AC$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AC$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AC$colortimesheetadmin:AC$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("AC$colortimesheetadmin:AC$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("AC$colortimesheetadmin:AC$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("AC$colortimesheetadmin:AC$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '26') {
                $spreadsheet->getActiveSheet()->getStyle("AD$colortimesheetadmin:AD$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AD$colortimesheetadmin:AD$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AD$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AD$colortimesheetadmin:AD$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("AD$colortimesheetadmin:AD$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("AD$colortimesheetadmin:AD$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("AD$colortimesheetadmin:AD$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '27') {
                $spreadsheet->getActiveSheet()->getStyle("AE$colortimesheetadmin:AE$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AE$colortimesheetadmin:AE$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AE$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AE$colortimesheetadmin:AE$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("AE$colortimesheetadmin:AE$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("AE$colortimesheetadmin:AE$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("AE$colortimesheetadmin:AE$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '28') {
                $spreadsheet->getActiveSheet()->getStyle("AF$colortimesheetadmin:AF$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AF$colortimesheetadmin:AF$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AF$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AF$colortimesheetadmin:AF$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("AF$colortimesheetadmin:AF$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("AF$colortimesheetadmin:AF$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("AF$colortimesheetadmin:AF$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '29') {
                $spreadsheet->getActiveSheet()->getStyle("AG$colortimesheetadmin:AG$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AG$colortimesheetadmin:AG$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AG$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AG$colortimesheetadmin:AG$fillcolortimesheetadmin");
            }
            if ($datefound == '30') {
                $spreadsheet->getActiveSheet()->getStyle("AH$colortimesheetadmin:AH$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AH$colortimesheetadmin:AH$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AH$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AH$colortimesheetadmin:AH$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("AH$colortimesheetadmin:AH$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("AH$colortimesheetadmin:AH$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("AH$colortimesheetadmin:A$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
            if ($datefound == '31') {
                $spreadsheet->getActiveSheet()->getStyle("AI$colortimesheetadmin:AI$fillcolortimesheetadmin")->getFill()->setFillType(Fill::FILL_SOLID);
                $spreadsheet->getActiveSheet()->getStyle("AI$colortimesheetadmin:AI$fillcolortimesheetadmin")->getFill()->getStartColor()->setARGB('FF808080');
                $spreadsheet->getActiveSheet()->setCellValue("AI$colortimesheetadmin", "$holidayname");
                $spreadsheet->getActiveSheet()->mergeCells("AI$colortimesheetadmin:AI$fillcolortimesheetadmin");

                $spreadsheet->getActiveSheet()->getStyle("AI$colortimesheetadmin:AI$fillcolortimesheetadmin")->getAlignment()->setTextRotation(90);
                $spreadsheet->getActiveSheet()->getStyle("AI$colortimesheetadmin:AI$fillcolortimesheetadmin")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $spreadsheet->getActiveSheet()->getStyle("AI$colortimesheetadmin:AI$fillcolortimesheetadmin")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            }
        }

        //Excell Formated Code By Asheesh 09-02-2019.
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(39);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(39);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(9);

        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('S')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('T')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('U')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('V')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('W')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('X')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('Y')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('Z')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('AA')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('AB')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('AC')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('AD')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('AE')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('AF')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('AG')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('AH')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('AI')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('AJ')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('AK')->setWidth(11);

        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet->getActiveSheet()->setTitle('Time Sheet');
        $spreadsheet->setActiveSheetIndex(0);

        //$file = $invoicedata->project_id.'_'.$invoiceID;
        $file = $projectname[0]->project_name;
        $filename = './uploads/account/' . $file;

        $writer = new Xlsx($spreadsheet);
        $writer->save($filename . '.xlsx');

        $this->load->helper('download');
        $file = './uploads/account/' . $file . '.xlsx';
        //$newfile = $invoicedata->project_id.'.xlsx';
        $newfile = $projectname[0]->project_name . '.xlsx';
        //download file from directory
        force_download($newfile, file_get_contents($file));
        redirect(base_url('account/viewinvoice?id=' . $invoiceID));
    }

    //code by durgesh
    public function generateinvoice() {
        $title = 'Generate Next Invoice';
        $id = $_REQUEST['id'];
        if ($id) {
            $Where = array('id' => $id);
            $proj_name = $this->mastermodel->GetTableData('accountinfo', $Where);
        }
        $userdata = $this->SecondDB_model->GetAllUserRec();
        $this->load->view('account/invoice_newview', compact('proj_name', 'id', 'title'));
    }

    //code by durgesh
    public function generateinvoiceinfo_ajax() {
        $params = $columns = $totalRecords = $data = array();
        $params = $_REQUEST;
        $projArr = $this->mastermodel->GetTableData('invoicedetail', array('infoid' => $_REQUEST['project_id']));
        $srn = 0;
        foreach ($projArr as $rowData):
            $srn++;
            $row = array();
            $invoicename = $rowData->invoice_name . '/' . $rowData->invoice_no;
            $row[] = $srn;
            $row[] = $invoicename;
            $row[] = $rowData->ra_invoice;
            $row[] = date('F Y', strtotime($rowData->invoice_date . "- 1 month"));
            $row[] = '<a href="' . base_url('account/exceldown/' . $rowData->id) . '" class="btn btn-info btn-sm"><span style="cursor:pointer;color:#fff;" title="Invoice Download" class="glyphicon glyphicon-eye-open"></span></a>';
            //&nbsp;&nbsp;<a href="' . base_url('account/viewtax?id=' . $rowData->id) . '" class="btn btn-info btn-sm"><span style="cursor:pointer;color:#fff;" title="View Tax" class="glyphicon glyphicon-eye-open"></span></a> &nbsp;&nbsp;<a href="' . base_url('account/viewescalation?id=' . $rowData->id) . '" class="btn btn-info btn-sm"><span style="cursor:pointer;color:#fff;" title="View Escalation" class="glyphicon glyphicon-eye-open"></span></a>&nbsp;&nbsp;<a href="' . base_url('account/viewreceivedamount?id=' . $rowData->id) . '" class="btn btn-info btn-sm"><span style="cursor:pointer;color:#fff;" title="View Received Amount" class="glyphicon glyphicon-eye-open"></span></a>&nbsp;&nbsp;<a href="' . base_url('account/viewpercentage?id=' . $rowData->id) . '" class="btn btn-info btn-sm"><span style="cursor:pointer;color:#fff;" title="View Percentage" class="glyphicon glyphicon-eye-open"></span></a>';
            $data[] = $row;
        endforeach;
        $json_data = array(
            // "draw" => 1,
            // "data" => $data
            "draw" => $_POST['draw'],
            "recordsTotal" => '0',
            "recordsFiltered" => '0',
            "data" => $data,
        );
        echo json_encode($json_data);
        exit();
    }

    // #################################### 27-09-2019 ##############################################
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    public function config_invoice_typediff() {
        $id = $_REQUEST['id'];
        $loginID = $this->session->userdata('uid');
        if ($id) {
            $Where = array('id' => $id);
            $data = $this->mastermodel->GetTableData('accountinfo', $Where);
        }
        if ($data[0]->invoice_formate_type == "1") {
            redirect(base_url("account/viewaccountinfo"));
        }
        $userdata = $this->SecondDB_model->GetAllUserRec();
        $this->load->view('account/configinvc_typediff_view', compact('data', 'userdata'));
    }

    // save Update .. Config Invoice...
    public function saveupd_config_invc_type() {
        $chkBoxActArr = $_REQUEST['key_act'];
        if ($chkBoxActArr) {
            foreach ($chkBoxActArr as $rRowID) {
                $man_month = $_REQUEST['man_month_' . $rRowID];
                $rate = $_REQUEST['rate_' . $rRowID];
                $totamount = $_REQUEST['totamount_' . $rRowID];
                $constmm = $_REQUEST['constmm_' . $rRowID];
                $constrate = $_REQUEST['constrate_' . $rRowID];
                $maintmm = $_REQUEST['maintmm_' . $rRowID];
                $maintrate = $_REQUEST['maintrate_' . $rRowID];

                if ($man_month and $rate and $totamount):
                    $UpdArr = array("construction_months" => $constmm,
                        "construction_rate" => ($constrate) ? $constrate : "0",
                        "maintenance_months" => ($maintmm) ? $maintmm : "0",
                        "maintenance_rate" => ($maintrate) ? $maintrate : "0");
                    $Where = array('id' => $rRowID);
                    $return = $this->mastermodel->UpdateRecords('assign_finalteam', $Where, $UpdArr);
                endif;
            }
        }
        redirect(base_url("account/config_invoice_typediff?id=" . $_REQUEST['teamid']));
    }

    //Save or Upd Reimbursable..
    public function saveupd_config_reimbursable() {
        $chkBoxActArr = $_REQUEST['reimb_act'];
        if ($chkBoxActArr) {
            foreach ($chkBoxActArr as $rRowID) {
                $constmm = $_REQUEST['constmm_' . $rRowID];
                $constrate = $_REQUEST['constrate_' . $rRowID];
                $maintmm = $_REQUEST['maintmm_' . $rRowID];
                $maintrate = $_REQUEST['maintrate_' . $rRowID];

                if ($constmm or $constrate or $maintmm or $maintrate):
                    $UpdArr = array("construction_months" => ($constmm) ? $constmm : "",
                        "construction_rate" => ($constrate) ? $constrate : "0",
                        "maintenance_months" => ($maintmm) ? $maintmm : "0",
                        "maintenance_rate" => ($maintrate) ? $maintrate : "0");
                    $Where = array('id' => $rRowID);
                    $return = $this->mastermodel->UpdateRecords('acc_msterdetail', $Where, $UpdArr);
                endif;
            }
        }
        redirect(base_url("account/config_invoice_typediff?id=" . $_REQUEST['teamid']));
    }

    public function ajax_recsinglerow_assignfinalteam() {
        $row_id = $_REQUEST['row_id'];
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($row_id) {
            $this->db->SELECT("$db1.assign_finalteam.project_id,$db1.assign_finalteam.designation_id,$db1.assign_finalteam.empname,$db1.assign_finalteam.maintenance_rate,$db1.assign_finalteam.construction_rate,$db1.assign_finalteam.rate");
            $this->db->FROM("$db1.assign_finalteam");
            $this->db->where(array("$db1.assign_finalteam.id" => $row_id, "$db1.assign_finalteam.status" => '1'));
            $recdArr = $this->db->get()->row();
        }
        echo ($recdArr) ? json_encode($recdArr) : "";
    }

    public function rate_vieweditupdatesave() {
        $EotTblID = $_REQUEST['eot_tblid'];
        $eotUpdatedRate = $_REQUEST['ratecols'];
        $Segmentid = $_REQUEST['Segmentid'];
        if ($EotTblID and $eotUpdatedRate) {
            //  echo "<pre>".$eotUpdatedRate; print_r($_REQUEST); die;
            $resp = $this->mastermodel->UpdateRecords('saveeotteam', array("id" => $EotTblID), array("eot_rate" => $eotUpdatedRate));
            //  echo $resp; die;
            $this->session->set_flashdata('msg', "EOT Record Updated Success.");
        }
        redirect(base_url("/account/eot?id=" . $Segmentid));
    }

    //Reimbursable Rate Update Save ..
    public function reimburEotRateSaveUpdate() {
        if ($_REQUEST['ratecols'] and $_REQUEST['reimbeot_tblid']) {
            $this->mastermodel->UpdateRecords('eot_detail', ["id" => $_REQUEST['reimbeot_tblid']], ["eot_rate" => $_REQUEST['ratecols']]);
            $this->session->set_flashdata('msg', "EOT Rate Updated Success.");
        }
        redirect(base_url("account/eot?id=" . $_REQUEST['Segmentid']));
    }

    //05-11-2019.. Code By Asheesh For Edit Update Designation..
    public function editupdteam_designation($bdprojID) {
        $this->load->model('Accountdept_model', 'accdeptmodel');
        if ($bdprojID) {
            $data['bdproject_id'] = $bdprojID;
            $data['title'] = "Edit-Team Designation";
            $projectname = $this->Front_model->getprojectname($bdprojID);
            $data['ProjectName'] = ($projectname) ? $projectname[0]->project_name : "";
            //Get All Reimbursable List..
            $data['AllDesignCategArr'] = $this->accdeptmodel->GetAllDesignCategAndTeamByProjID($bdprojID);
        }
        $this->load->view('accountdept/editupdteam_designation_view', $data);
    }

    //Save Update Proceess..
    public function saveupdtdesignation() {
        $this->load->model('Accountdept_model', 'accdeptmodel');
        if ($_REQUEST['asn_fnteamid']) {
            $asnFinalTeamID = $_REQUEST['asn_fnteamid'];
            $newDesignID = $_REQUEST['newdesign_' . $asnFinalTeamID];
            $assignFinalRec = $this->accdeptmodel->getAssignFinalSingleRow($asnFinalTeamID);

            if (($assignFinalRec) and ( $newDesignID != $assignFinalRec->designation_id)) {
                // Team Design Change History.
                $HistInsertArr = array("project_id" => $assignFinalRec->project_id, "ts_projid" => $assignFinalRec->project_numberid, "old_desigid" => $assignFinalRec->designation_id, "new_desigid" => $newDesignID, "assign_fn_temaid" => $asnFinalTeamID, "entry_upd_by" => $this->session->userdata('uid'));
                $this->db->insert("team_design_change_history", $HistInsertArr);
                //Update .. Multiple Table..
                $this->db->set('b.designation_id', $newDesignID);
                $this->db->where(["b.project_id" => $assignFinalRec->project_id, "b.designation_id" => $assignFinalRec->designation_id, "b.emp_id" => $assignFinalRec->empname]);
                $this->db->update("invoicesave as b");
                //Tabl..2
                $this->db->set('c.designation_id', $newDesignID);
                $this->db->where(["c.is_active" => "1", "c.project_id" => $assignFinalRec->project_id, "c.designation_id" => $assignFinalRec->designation_id, "c.emp_id" => $assignFinalRec->empname]);
                $this->db->update("timeshet_fill as c");
                //Tbl..3
                $this->db->set('d.designation_id', $newDesignID);
                $this->db->where(["d.project_id" => $assignFinalRec->project_id, "d.designation_id" => $assignFinalRec->designation_id, "d.empname" => $assignFinalRec->empname]);
                $this->db->update("replacementteam as d");
                // Tbl 4..
                $this->db->set('e.designation_id', $newDesignID);
                $this->db->where(["e.project_id" => $assignFinalRec->project_id, "e.designation_id" => $assignFinalRec->designation_id, "e.emp_id" => $assignFinalRec->empname]);
                $this->db->update("saveeotteam as e");
                //Layer .. 2
                $this->db->set('a.designation_id', $newDesignID);
                $this->db->where(["a.id" => $asnFinalTeamID]);
                $this->db->update("assign_finalteam as a");
                $this->session->set_flashdata('msg', "Team Designation Updated Success");
            }
        }
        redirect(base_url("account/editupdteam_designation/" . $assignFinalRec->project_id));
    }

    //Function Edited By Asheesh 13-05-2019...
    public function saveescalation() {
        $invoiceID = $_REQUEST['invoice_id'];
        $invoicedata = $this->Front_model->getaccountdetailforexcel($invoiceID);
        $decimalplace = $invoicedata->decimal_place;
        $no_days = $invoicedata->no_days;
        $curtotal = 0;

        $previousdate = date('Y-m-d', strtotime($invoicedata->invoice_date . "-2 month"));
        $preyears = date('Y', strtotime($previousdate));
        $premonths = date('n', strtotime($previousdate));

        $currentdate = date('Y-m-d', strtotime($invoicedata->invoice_date . "-1 month"));
        $cruyears = date('Y', strtotime($currentdate));
        $crumonths = date('n', strtotime($currentdate));
        $crumonthschk = date('m', strtotime($currentdate));
        $d = cal_days_in_month(CAL_GREGORIAN, $crumonths, $cruyears);

        $startDate = date('Y-m-d', strtotime($cruyears . '-' . $crumonthschk . '-01'));
        $endDate = date('Y-m-d', strtotime($cruyears . '-' . $crumonthschk . "-$d"));

        if (!empty($no_days)) {
            $divideno_days = $no_days;
        } else {
            $divideno_days = cal_days_in_month(CAL_GREGORIAN, $crumonths, $cruyears);
        }

        $keyteamdata = $this->Front_model->getexceldataproject_withreplc($invoicedata->project_id, $invoicedata->project_numberid, $invoicedata->invoiceno, 1);
        $subteamdata = $this->Front_model->getexceldataproject_withreplc($invoicedata->project_id, $invoicedata->project_numberid, $invoicedata->invoiceno, 2);
        $adminteamdata = $this->Front_model->getexceldataproject_withreplc($invoicedata->project_id, $invoicedata->project_numberid, $invoicedata->invoiceno, 3);

        $adminID = count($adminteamdata);
        $keyID = count($keyteamdata);
        $subID = count($subteamdata);

        $transportation = gettransportation($invoicedata->project_id, 1);
        $travel_site = gettransportation($invoicedata->project_id, 2);
        $office_rent = gettransportation($invoicedata->project_id, 3);
        $office_supplies = gettransportation($invoicedata->project_id, 4);
        $office_furniture = gettransportation($invoicedata->project_id, 5);
        $office_equ = gettransportation($invoicedata->project_id, 6);
        $reports = gettransportation($invoicedata->project_id, 7);
        $survey_equ = gettransportation($invoicedata->project_id, 8);
        $road_survey_equ = gettransportation($invoicedata->project_id, 9);
        $contingencies = gettransportation($invoicedata->project_id, 10);

        $transID = count($transportation);
        $travelID = count($travel_site);
        $officerentID = count($office_rent);
        $suppliesID = count($office_supplies);
        $furnitureID = count($office_furniture);
        $equipID = count($office_equ);
        $reportID = count($reports);
        $survey_equID = count($survey_equ);
        $road_survey_equID = count($road_survey_equ);
        $contingenciesID = count($contingencies);

        $summerytotalmmamountkeys = 0;
        for ($i = 0; $i < $keyID; $i++) {
            $intermittent = $this->Front_model->getintermittent($invoicedata->project_id, $keyteamdata[$i]->emp_id);
            if (!empty($intermittent)) {
                $manmonthscrukey = $this->Front_model->getintermittentattd($invoicedata->project_id, $keyteamdata[$i]->emp_id, $keyteamdata[$i]->designation_id, $crumonths, $cruyears);
                if (($manmonthscrukey->present) > 0) {
                    $mmcountprekey = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                } else {
                    $mmcountprekey = 0;
                }
            } else {
                $timemonth = $this->Front_model->gettimesheetdetail($invoicedata->project_id, $keyteamdata[$i]->designation_id, $keyteamdata[$i]->emp_id, $crumonths, $cruyears);
                if (($timemonth->leave + $timemonth->absent) > 0) {
                    $mmcountprekey = round((1 - ($timemonth->leave + $timemonth->absent) / $divideno_days), $decimalplace);
                    if ($mmcountprekey < 0) {
                        $mmcountprekey = 0;
                    }
                } else {
                    if (($timemonth->present) > 0) {
                        $mmcountprekey = 1;
                    } else {
                        $mmcountprekey = 0;
                    }
                }
            }
            if ($keyteamdata[$i]->rate > 0) {
                $replacement_reducationrate = $keyteamdata[$i]->rate - (($keyteamdata[$i]->rate * $keyteamdata[$i]->replacement_reducationrate) / 100);
            } else {
                $replacement_reducationrate = $keyteamdata[$i]->rate;
            }
            $mmamountkey = $mmcountprekey * $replacement_reducationrate;
            $summerytotalmmamountkeys += $mmamountkey;
        }

        $summerytotalmmamountkeyssub = 0;
        for ($i = 0; $i < $subID; $i++) {
            $intermittent = $this->Front_model->getintermittent($invoicedata->project_id, $subteamdata[$i]->emp_id);
            if (!empty($intermittent)) {
                $manmonthscrukey = $this->Front_model->getintermittentattd($invoicedata->project_id, $subteamdata[$i]->emp_id, $subteamdata[$i]->designation_id, $crumonths, $cruyears);
                if (($manmonthscrukey->present) > 0) {
                    $mmcountpresub = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                } else {
                    $mmcountpresub = 0;
                }
            } else {
                $timemonthsub = $this->Front_model->gettimesheetdetail($invoicedata->project_id, $subteamdata[$i]->designation_id, $subteamdata[$i]->emp_id, $crumonths, $cruyears);
                if (($timemonthsub->leave + $timemonthsub->absent) > 0) {
                    $mmcountpresub = round((1 - ($timemonthsub->leave + $timemonthsub->absent) / $divideno_days), $decimalplace);
                    // echo $timemonthsub->leave ." + ". $timemonthsub->absent ."<br>". $divideno_days."<br>";

                    if ($mmcountpresub < 0) {
                        $mmcountpresub = 0;
                    }
                } else {
                    if (($timemonthsub->present) > 0) {
                        $mmcountpresub = 1;
                    } else {
                        $mmcountpresub = 0;
                    }
                }
            }
            if ($subteamdata[$i]->rate > 0) {
                $replacement_reducationrate = $subteamdata[$i]->rate - (($subteamdata[$i]->rate * $subteamdata[$i]->replacement_reducationrate) / 100);
            } else {
                $replacement_reducationrate = $subteamdata[$i]->rate;
            }
            $mmamountsub = $mmcountpresub * $replacement_reducationrate;
            //echo $mmamountsub." = ".$mmcountpresub ." X ". $replacement_reducationrate."<br>";
            $summerytotalmmamountkeyssub += $mmamountsub;
        }
//die;
        $summerytotalmmamountadmim = 0;
        for ($i = 0; $i < $adminID; $i++) {
            $intermittent = $this->Front_model->getintermittent($invoicedata->project_id, $adminteamdata[$i]->emp_id);
            if (!empty($intermittent)) {

                $manmonthscrukey = $this->Front_model->getintermittentattd($invoicedata->project_id, $adminteamdata[$i]->emp_id, $adminteamdata[$i]->designation_id, $crumonths, $cruyears);
                if (($manmonthscrukey->present) > 0) {
                    $mmcountpreadmin = round(($manmonthscrukey->present / $divideno_days), $decimalplace);
                } else {
                    $mmcountpreadmin = 0;
                }
            } else {
                $timemonth = $this->Front_model->gettimesheetdetail($invoicedata->project_id, $adminteamdata[$i]->designation_id, $adminteamdata[$i]->emp_id, $crumonths, $cruyears);
                if (($timemonth->leave + $timemonth->absent) > 0) {
                    $mmcountpreadmin = round((1 - ($timemonth->leave + $timemonth->absent) / $divideno_days), $decimalplace);
                    if ($mmcountpreadmin < 0) {
                        $mmcountpreadmin = 0;
                    }
                } else {
                    if (($timemonth->present) > 0) {
                        $mmcountpreadmin = 1;
                    } else {
                        $mmcountpreadmin = 0;
                    }
                }
            }
            if ($adminteamdata[$i]->rate > 0) {
                $replacement_reducationrate = $adminteamdata[$i]->rate - (($adminteamdata[$i]->rate * $adminteamdata[$i]->replacement_reducationrate) / 100);
            } else {
                $replacement_reducationrate = $adminteamdata[$i]->rate;
            }
            $mmamountadmin = $mmcountpreadmin * $replacement_reducationrate;
            $summerytotalmmamountadmim += $mmamountadmin;
        }


        //ReimburSable Calculation Start..
        //3..
        $summerytranstotalcur = 0;
        for ($i = 0; $i < $transID; $i++) {
            $per_month = 0;
            $currmonth = 0;
            $currentmmmonth = 0;
            $currentamount = 0;
            //Code For Jind Proj ..
            if ($invoicedata->project_id == "89169") {
                $per_month = $transportation[$i]->construction_rate;
            } else {
                $per_month = $transportation[$i]->per_month;
            }
            $qty = ($transportation[$i]->qty > 0) ? $transportation[$i]->qty : "1";
            $unit = ($transportation[$i]->unit > 0) ? $transportation[$i]->unit : "1";
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $transportation[$i]->id, $invoicedata->invoice_date);
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = ($currentmmmonth * $per_month * $qty * $unit);
            $summerytranstotalcur += $currentamount;
        }


        //4.
        $summerytraveltotalcur = 0;
        for ($i = 0; $i < $travelID; $i++) {
            $per_month = 0;
            $per_month = $travel_site[$i]->per_month;
            $qty = ($travel_site[$i]->qty > 0) ? $travel_site[$i]->qty : "1";
            $unit = ($travel_site[$i]->unit > 0) ? $travel_site[$i]->unit : "1";
            $currmonth = 0;
            $currentmmmonth = 0;
            $currentamount = 0;
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $travel_site[$i]->id, $invoicedata->invoice_date);
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = ($currentmmmonth * $per_month * $qty * $unit);
            $summerytraveltotalcur += $currentamount;
        }

        //5..
        $summeryofficerenttotalcur = 0;
        for ($i = 0; $i < $officerentID; $i++) {
            $per_month = 0;
            $currmonth = 0;
            $currentmmmonth = 0;
            $currentamount = 0;
            $per_month = $office_rent[$i]->per_month;
            $qty = ($office_rent[$i]->qty > 0) ? $office_rent[$i]->qty : "1";
            $unit = ($office_rent[$i]->unit > 0) ? $office_rent[$i]->unit : "1";
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $office_rent[$i]->id, $invoicedata->invoice_date);
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = ($currentmmmonth * $per_month * $qty * $unit);
            $summeryofficerenttotalcur += $currentamount;
        }

        //6..
        $summerysuppliestotalcur = 0;
        for ($i = 0; $i < $suppliesID; $i++) {
            $per_month = 0;
            $currmonth = 0;
            $currentmmmonth = 0;
            $currentamount = 0;
            $per_month = $office_supplies[$i]->per_month;
            $qty = ($office_supplies[$i]->qty > 0) ? $office_supplies[$i]->qty : "1";
            $unit = ($office_supplies[$i]->unit > 0) ? $office_supplies[$i]->unit : "1";
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $office_supplies[$i]->id, $invoicedata->invoice_date);
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = ($currentmmmonth * $per_month * $qty * $unit);
            $summerysuppliestotalcur += $currentamount;
        }

        //7..
        $summeryfurnituretotalcur = 0;
        for ($i = 0; $i < $furnitureID; $i++) {
            $per_month = 0;
            $currmonth = 0;
            $currentmmmonth = 0;
            $currentamount = 0;
            $per_month = $office_furniture[$i]->per_month;
            $qty = ($office_furniture[$i]->qty > 0) ? $office_furniture[$i]->qty : "1";
            $unit = ($office_furniture[$i]->unit > 0) ? $office_furniture[$i]->unit : "1";
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $office_furniture[$i]->id, $invoicedata->invoice_date);
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = ($currentmmmonth * $per_month * $qty * $unit);
            $summeryfurnituretotalcur += $currentamount;
        }
        //8..
        $summeryequiptotalcur = 0;
        for ($i = 0; $i < $equipID; $i++) {
            $per_month = 0;
            $per_month = $office_equ[$i]->per_month;
            $qty = ($office_equ[$i]->qty > 0) ? $office_equ[$i]->qty : "1";
            $unit = ($office_equ[$i]->unit > 0) ? $office_equ[$i]->unit : "1";
            $currmonth = 0;
            $currentmmmonth = 0;
            $currentamount = 0;
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $office_equ[$i]->id, $invoicedata->invoice_date);
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = ($currentmmmonth * $per_month * $qty * $unit);
            $summeryequiptotalcur += $currentamount;
        }

        //9..
        $summeryreporttotalcur = 0;
        for ($i = 0; $i < $reportID; $i++) {
            $per_month = 0;
            $currmonth = 0;
            $currentmmmonth = 0;
            $currentamount = 0;
            $qty = ($reports[$i]->qty > 0) ? $reports[$i]->qty : "1";
            $unit = ($reports[$i]->unit > 0) ? $reports[$i]->unit : "1";
            $per_month = $reports[$i]->per_month;
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $reports[$i]->id, $invoicedata->invoice_date);
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = ($currentmmmonth * $per_month * $qty * $unit);
            // echo ($currentmmmonth . " * " . $per_month . " * " . $qty . " * " . $unit) . "<hr>";

            $summeryreporttotalcur += $currentamount;
        }

        // die;
        //10..
        $summerysurvey_equtotalcur = 0;
        for ($i = 0; $i < $survey_equID; $i++) {
            $per_month = 0;
            $currmonth = 0;
            $currentmmmonth = 0;
            $currentamount = 0;
            $per_month = $survey_equ[$i]->per_month;
            $qty = ($survey_equ[$i]->qty > 0) ? $survey_equ[$i]->qty : "1";
            $unit = ($survey_equ[$i]->unit > 0) ? $survey_equ[$i]->unit : "1";
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $survey_equ[$i]->id, $invoicedata->invoice_date);
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = ($currentmmmonth * $per_month * $qty * $unit);
            $summerysurvey_equtotalcur += $currentamount;
        }

        //11..
        $summeryroad_survey_equtotalcur = 0;
        for ($i = 0; $i < $road_survey_equID; $i++) {
            $per_month = 0;
            $per_month = $road_survey_equ[$i]->per_month;
            $qty = ($road_survey_equ[$i]->qty > 0) ? $road_survey_equ[$i]->qty : "1";
            $unit = ($road_survey_equ[$i]->unit > 0) ? $road_survey_equ[$i]->unit : "1";
            $currmonth = 0;
            $currentmmmonth = 0;
            $currentamount = 0;
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $road_survey_equ[$i]->id, $invoicedata->invoice_date);
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = ($currentmmmonth * $per_month * $qty * $unit);
            $summeryroad_survey_equtotalcur += $currentamount;
        }

        //12..
        $summarycontingenciestotalcur = 0;
        for ($i = 0; $i < $contingenciesID; $i++) {
            $per_month = 0;
            $per_month = $contingencies[$i]->per_month;
            $qty = ($contingencies[$i]->qty > 0) ? $contingencies[$i]->qty : "1";
            $unit = ($contingencies[$i]->unit > 0) ? $contingencies[$i]->unit : "1";
            $currmonth = 0;
            $currentmmmonth = 0;
            $currentamount = 0;
            $currmonth = getothercurrentmonth($invoicedata->project_id, $invoiceID, $contingencies[$i]->id, $invoicedata->invoice_date);
            $currentmmmonth = round(($currmonth[0]->other_mm), $decimalplace);
            $currentamount = ($currentmmmonth * $per_month * $qty * $unit);
            $summarycontingenciestotalcur += $currentamount;
        }
        $summerysubtotalcur = $summerytranstotalcur + $summerytraveltotalcur + $summeryofficerenttotalcur + $summerysuppliestotalcur + $summeryfurnituretotalcur + $summeryequiptotalcur + $summeryreporttotalcur + $summerysurvey_equtotalcur + $summeryroad_survey_equtotalcur + $summarycontingenciestotalcur;
        $totalcurmonth = $summerytotalmmamountkeys + $summerytotalmmamountkeyssub + $summerytotalmmamountadmim;
        //echo $summerytranstotalcur . " + " . $summerytraveltotalcur . " + " . $summeryofficerenttotalcur . " + " . $summerysuppliestotalcur . " + " . $summeryfurnituretotalcur . " + " . $summeryequiptotalcur . " + " . $summeryreporttotalcur . " + " . $summerysurvey_equtotalcur . " + " . $summeryroad_survey_equtotalcur . " + " . $summarycontingenciestotalcur;
        //echo $summerytotalmmamountkeys . " + " . $summerytotalmmamountkeyssub . " + " . $summerytotalmmamountadmim;
        //die;

        if ($_REQUEST['update'] == '1') {
            // echo '<pre>'; print_r($_REQUEST); die;
            $length = count($_REQUEST['countdata']);
            $project_id = $_REQUEST['project_id'];
            $invoice_id = $_REQUEST['invoice_id'];

            $exclsVal = 0;

            for ($i = 0; $i < $length; $i++) {
                $ids = $_REQUEST['ids'][$i];
                $this->db->select('*');
                $this->db->from('escalation_tax');
                $this->db->where('id', $ids);
                $this->db->where('project_id', $project_id);
                $this->db->where('invoice_id', $invoice_id);
                $taxres = $this->db->get()->row_array();

                $status_type = $_REQUEST['status_type'][$i];
                $percentage = $_REQUEST['percentage'][$i];

                $start_date2019 = $_REQUEST['start_date'][$i];
                $end_date2019 = $_REQUEST['end_date'][$i];
                //Get Last Date of invoide month
                $RecArrDate = $this->lastDateInvc($invoice_id);
                // print_r($RecArrDate['monthYear']);
                // Array ( [lastdateRec] => 28 [monthYear] => 07-02-2019 )
                $InvcMonthYear = date("Y-m", strtotime($RecArrDate['monthYear']));
                $strEndDateRangArr = $this->createDateRangeArray($start_date2019, $end_date2019);

                //29-05-2019.. Logic Set For Exclation..
                // $a_date = "2016-02-23";
                $FirstDayofInvcMonth = $InvcMonthYear . "-" . "01";
                $LastDayofInvcMonth = date("Y-m-t", strtotime($FirstDayofInvcMonth));
                $InvcDateRangeArr = $this->GetDateRangeArray($start_date2019, $end_date2019);

                $ThisInvcDateRangeArr = $this->GetDateRangeArray($FirstDayofInvcMonth, $LastDayofInvcMonth);
                $resultmatchDateArr = array_intersect($InvcDateRangeArr, $ThisInvcDateRangeArr);

                $totalDayCountInvcMonthYear = count($ThisInvcDateRangeArr);
                $totalnoofDateMatch = count($resultmatchDateArr);
                //Close Code By Asheesh..

                if (in_array($InvcMonthYear, $strEndDateRangArr)) {
                    $ArrfIrstVal = array_shift(array_values($strEndDateRangArr));
                    $ArrEndVal = end($strEndDateRangArr);
                    if (($InvcMonthYear == $ArrfIrstVal) or ( $InvcMonthYear == $ArrEndVal)) {
                        
                    }
                } else {
                    //echo "No";
                }

                if ($status_type == 1) {
                    $curtotal = $totalcurmonth;
                    $perccurtotal = round(($curtotal * $percentage) / 100);
                    $perDayVal = ($perccurtotal / $totalDayCountInvcMonthYear);
                    $totalamount_perccurtotal = ($perDayVal * $totalnoofDateMatch);
                    $totalamount = round($_REQUEST['pertotal'][$i] + $totalamount_perccurtotal);
                } elseif ($status_type == 2) {
                    $curtotal = $summerysubtotalcur;
                    $perccurtotal = round(($curtotal * $percentage) / 100);
                    $perDayVal = ($perccurtotal / $totalDayCountInvcMonthYear);
                    $totalamount_perccurtotal = ($perDayVal * $totalnoofDateMatch);
                    $totalamount = round($_REQUEST['pertotal'][$i] + $totalamount_perccurtotal);
                } else {
                    $curtotal = $totalcurmonth + $summerysubtotalcur;
                    $perccurtotal = round(($curtotal * $percentage) / 100);
                    $perDayVal = ($perccurtotal / $totalDayCountInvcMonthYear);
                    $totalamount_perccurtotal = ($perDayVal * $totalnoofDateMatch);
                    $totalamount = round($_REQUEST['pertotal'][$i] + $totalamount_perccurtotal);
                }
                if (!empty($totalamount_perccurtotal)) {
                    $subtotal = $totalcurmonth + $summerysubtotalcur;
                    $total = $totalamount_perccurtotal + $subtotal;
                }
                // Rahul 31-01-2019
                else {
                    $subtotal = $totalcurmonth + $summerysubtotalcur;
                    $total = $subtotal;
                }
                if (empty($taxres)) {
                    $data = array(
                        'project_id' => $_REQUEST['project_id'],
                        'invoice_id' => $_REQUEST['invoice_id'],
                        'escalation_name' => $_REQUEST['escalation'][$i],
                        'esc_percentage' => $_REQUEST['percentage'][$i],
                        'esc_pretotal' => $_REQUEST['pertotal'][$i],
                        'curtotal' => $curtotal,
                        'perccurtotal' => $totalamount_perccurtotal,
                        'totalamount' => $totalamount,
                        'invoicesubtotal' => $subtotal,
                        'invoicetotal' => $total,
                        'status_type' => $_REQUEST['status_type'][$i],
                        'start_date' => $_REQUEST['start_date'][$i],
                        'end_date' => $_REQUEST['end_date'][$i],
                        'entryby' => $this->session->userdata('uid')
                    );
                    $res1 = $this->mastermodel->InsertMasterData($data, 'escalation_tax');
                } else {
                    $Upddatas = array('escalation_name' => $_REQUEST['escalation'][$i],
                        'esc_percentage' => $_REQUEST['percentage'][$i],
                        'esc_pretotal' => $_REQUEST['pertotal'][$i],
                        'curtotal' => $curtotal,
                        'perccurtotal' => $totalamount_perccurtotal,
                        'totalamount' => $totalamount,
                        'invoicesubtotal' => $subtotal,
                        'invoicetotal' => $total,
                        'status_type' => $_REQUEST['status_type'][$i],
                        'start_date' => $_REQUEST['start_date'][$i],
                        'end_date' => $_REQUEST['end_date'][$i],
                        'entryby' => $this->session->userdata('uid')
                    );

//                    echo "<pre>";
//                    print_r($Upddatas);
//                    die;


                    $Where = array('project_id' => $project_id, 'invoice_id' => $invoice_id, 'id' => $ids);
                    $this->mastermodel->UpdateRecords('escalation_tax', $Where, $Upddatas);
                    $exclsVal += $totalamount_perccurtotal;
                }
            }
            //Get Last Id of escalation_tax Table.. Asheesh..
            $exclID = $this->GetLastId_escalation_tax($project_id, $invoice_id);
            $realValLastExcls = $subtotal + $exclsVal;
            // echo $realValLastExcls; die;
            if ($exclID):
                $this->db->where('id', $exclID);
                $this->db->update("escalation_tax", array('invoicetotal' => $realValLastExcls));
            endif;
            //Close Bug Exclation Code... 30-05-2019 ash..
        } else {
            $status_type = $_REQUEST['status_type'];
            $percentage = $_REQUEST['percentage'];
            $curtotal = 0;

//                $FirstDayofInvcMonth = $InvcMonthYear . "-" . "01";
//                $LastDayofInvcMonth = date("Y-m-t", strtotime($FirstDayofInvcMonth));
//                $InvcDateRangeArr = $this->GetDateRangeArray($start_date2019, $end_date2019);
//
//                $ThisInvcDateRangeArr = $this->GetDateRangeArray($FirstDayofInvcMonth, $LastDayofInvcMonth);
//                $resultmatchDateArr = array_intersect($InvcDateRangeArr, $ThisInvcDateRangeArr);
//
//                $totalDayCountInvcMonthYear = count($ThisInvcDateRangeArr);
//                $totalnoofDateMatch = count($resultmatchDateArr);
            //Close Code By Asheesh..

            if ($status_type == 1) {
                $curtotal = $totalcurmonth;
                $perccurtotal = round(($curtotal * $percentage) / 100);
                $totalamount = round($_REQUEST['pertotal'] + $perccurtotal);
            } elseif ($status_type == 2) {
                $curtotal = $summerysubtotalcur;
                $perccurtotal = round(($curtotal * $percentage) / 100);
                $totalamount = round($_REQUEST['pertotal'] + $perccurtotal);
            } else {
                $curtotal = $totalcurmonth + $summerysubtotalcur;
                $perccurtotal = round(($curtotal * $percentage) / 100);
                $totalamount = round($_REQUEST['pertotal'] + $perccurtotal);
            }
            if (!empty($perccurtotal)) {
                $subtotal = $totalcurmonth + $summerysubtotalcur;
                $total = $perccurtotal + $subtotal;
            }

            $data = array('project_id' => $_REQUEST['project_id'],
                'invoice_id' => $_REQUEST['invoice_id'],
                'escalation_name' => $_REQUEST['escalation'],
                'esc_percentage' => $_REQUEST['percentage'],
                'esc_pretotal' => $_REQUEST['pertotal'],
                'curtotal' => $curtotal,
                'perccurtotal' => $perccurtotal,
                'totalamount' => $totalamount,
                'invoicesubtotal' => $subtotal,
                'invoicetotal' => $total,
                'status_type' => $_REQUEST['status_type'],
                'start_date' => $_REQUEST['start_date'],
                'end_date' => $_REQUEST['end_date'],
                'entryby' => $this->session->userdata('uid')
            );
            $res1 = $this->mastermodel->InsertMasterData($data, 'escalation_tax');
        }
        redirect(base_url('account/viewinvoice?id=' . $_REQUEST['accteam_id']));
    }

    //Invoice PDF of With EOT .. Normal Formate - 1
    public function invoice_pdfwitheot($invoiceID) {
        error_reporting(0);
        $this->load->library('mp_pdf');
        $this->load->model('Accountdept_model', 'accdeptmodel');
        // $invoiceID = 227;
        $invoicedata = $this->Front_model->getaccountdetailforexcel($invoiceID);
        $projectname = $this->Front_model->getprojectname($invoicedata->project_id);
        $data['invoicedata'] = $invoicedata;

        //Get All Reimbursable List..
        $data['ReimbursableList'] = $this->accdeptmodel->GetReimbursable_Byproj($invoicedata->project_id);
        //Get Key Professional Staff & Sub Professional Staff Total RateAmount.. Code By Asheesh
        $data['KeyProfSubProfStaffTotRateAmnt'] = $this->accdeptmodel->GetKeyProf_SubProfStaffTotRateAmnt($invoicedata->project_id);
        //Get Admin Staff or Supporting Staff...
        $data['AdminSupportingStaffRateAmnt'] = $this->accdeptmodel->GetStaffRateMMAmnt_Total($invoicedata->project_id, "3");
        //Cumulative up to Previous Bill of Key Prof & Sub Prof..
        $data['CumulativePrevAmnt'] = $this->accdeptmodel->GetKeyProf_SubProf_CumulativuptoPrevAmnt($invoicedata->project_id, $invoicedata->invoiceno);
        //Cumulative up to Previous Bill of Admin Staff or Supporting Staff..

        $data['CumulativeAdminSupportingStaff'] = $this->accdeptmodel->GetAdminSupportingStaffCumulativAmnt($invoicedata->project_id, $invoicedata->invoiceno);
        //Get All Designation Category..
        $data['AllDesignCategArr'] = $this->accdeptmodel->GetAllDesignCategAndTeamByProjID($invoicedata->project_id);
        //Escalation Get...
        $data['EscalationRecArr'] = $this->accdeptmodel->GetEscalationRecArr($invoicedata->project_id, $invoiceID);
        // Start Tax Code .....
        $data['TaxesRecArr'] = $this->accdeptmodel->GetTaxesRecArr($invoicedata->project_id, $invoiceID);
        $first_page = $this->load->view('invoice/normalwitheot/first_page', $data, true);

        //Start Static Condition For Reimbursable 7-8 MergeViews..
        if ($invoicedata->project_id == "89224" or $invoicedata->project_id == "89222" or $invoicedata->project_id == "89179") {
            $second_page = $this->load->view('invoice/normalwitheot/second_page_hide_6id', $data, true);
        } else {
            $second_page = $this->load->view('invoice/normalwitheot/second_page', $data, true);
        }
        //Close Static Condition For Reimbursable 7-8 MergeViews..

        $remuneration = $this->load->view('invoice/normalwitheot/remuneration', $data, true);
        $support = $this->load->view('invoice/normalwitheot/support', $data, true);

        $transportation_duty = $this->load->view('invoice/normalwitheot/transportation_duty', $data, true);
        $officerent_supplies = $this->load->view('invoice/normalwitheot/officerent_supplies', $data, true);
        $office_furniture_equipment_report_document = $this->load->view('invoice/normalwitheot/office_furniture_equipment_report_document', $data, true);
        $road_survey = $this->load->view('invoice/normalwitheot/road_survey', $data, true);
        $survey_equipment = $this->load->view('invoice/normalwitheot/survey_equipment', $data, true);
        $contingencies = $this->load->view('invoice/normalwitheot/contingencies', $data, true);
        $attendance = $this->load->view('invoice/normalwitheot/attendance', $data, true);
        $pdfFilePath = "invoice_report.pdf";

        $margin_left = 4;
        $margin_right = 4;
        $margin_top = 5;
        $margin_bottom = 3;

        $this->mp_pdf->pdf->mPDF('', '', '', '', $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer, '');
        $this->mp_pdf->pdf->mirrorMargins = 1;
        ob_clean();
        ob_flush();

        $this->mp_pdf->pdf->WriteHTML($first_page);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($second_page);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($remuneration);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($support);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($transportation_duty);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($officerent_supplies);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($office_furniture_equipment_report_document);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($road_survey);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($survey_equipment);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($contingencies);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($attendance);

        $this->mp_pdf->pdf->Output($pdfFilePath, "I");

        ob_end_flush();
        ob_end_clean();
    }

    //Invoice PDF of With (Normal+Construction+Maint) Formate - 1
    public function invoice_normal_construc_mainten_eot($invoiceID) {
        error_reporting(0);
        $this->load->library('mp_pdf');
        $this->load->model('Accountdept_model', 'accdeptmodel');
        // $invoiceID = 219;
        $invoicedata = $this->Front_model->getaccountdetailforexcel($invoiceID);
        $projectname = $this->Front_model->getprojectname($invoicedata->project_id);
        $data['invoicedata'] = $invoicedata;
        //Get All Reimbursable List..
        $data['ReimbursableList'] = $this->accdeptmodel->GetReimbursable_Byproj($invoicedata->project_id);
        //Get Key Professional Staff & Sub Professional Staff Total RateAmount.. Code By Asheesh
        $data['KeyProfSubProfStaffTotRateAmnt'] = $this->accdeptmodel->GetKeyProf_SubProfStaffTotRateAmnt($invoicedata->project_id);
        //Get Admin Staff or Supporting Staff...
        $data['AdminSupportingStaffRateAmnt'] = $this->accdeptmodel->GetStaffRateMMAmnt_Total($invoicedata->project_id, "3");
        //Cumulative up to Previous Bill of Key Prof & Sub Prof..
        $data['CumulativePrevAmnt'] = $this->accdeptmodel->GetKeyProf_SubProf_CumulativuptoPrevAmnt($invoicedata->project_id, $invoicedata->invoiceno);
        //Cumulative up to Previous Bill of Admin Staff or Supporting Staff..
        $data['CumulativeAdminSupportingStaff'] = $this->accdeptmodel->GetAdminSupportingStaffCumulativAmnt($invoicedata->project_id, $invoicedata->invoiceno);
        //Get All Designation Category..
        $data['AllDesignCategArr'] = $this->accdeptmodel->GetAllDesignCategAndTeamByProjID($invoicedata->project_id);
        //Escalation Get...
        $data['EscalationRecArr'] = $this->accdeptmodel->GetEscalationRecArr($invoicedata->project_id, $invoiceID);
        // Start Tax Code .....
        $data['TaxesRecArr'] = $this->accdeptmodel->GetTaxesRecArr($invoicedata->project_id, $invoiceID);
        //Construction Tot Rate..
        $data['KeyProfSubProfStaffTotRateAmnt']['RateAmountTot'] = ($data['KeyProfSubProfStaffTotRateAmnt']['maintenance_rateTot'] + $data['KeyProfSubProfStaffTotRateAmnt']['construction_rateTot']);
        $data['AdminSupportingStaffRateAmnt']['RateAmountTot'] = ($data['AdminSupportingStaffRateAmnt']['maintenance_rateTot'] + $data['AdminSupportingStaffRateAmnt']['construction_rateTot']);
        // Tot Rate..
        $data['KeyProfSubProfStaffTotRateAmnt']['MMTot'] = ($data['KeyProfSubProfStaffTotRateAmnt']['maintenance_monthsTot'] + $data['KeyProfSubProfStaffTotRateAmnt']['construction_monthsTot']);
        $data['AdminSupportingStaffRateAmnt']['MMTot'] = ($data['AdminSupportingStaffRateAmnt']['maintenance_monthsTot'] + $data['AdminSupportingStaffRateAmnt']['construction_monthsTot']);

        $first_page = $this->load->view('invoice/invoice_normal_construc_mainten_eot/first_page', $data, true);
        //Second Page..
        $second_page = $this->load->view('invoice/invoice_normal_construc_mainten_eot/second_page', $data, true);
        //Start Static Condition For Reimbursable 7-8 MergeViews..
//        if ($invoicedata->project_id == "89224" or $invoicedata->project_id == "89222") {
//            $second_page = $this->load->view('invoice/normalwitheot/second_page_hide_6id', $data, true);
//        } else {
//            $second_page = $this->load->view('invoice/normalwitheot/second_page', $data, true);
//        }
        //Close Static Condition For Reimbursable 7-8 MergeViews..


        $remuneration = $this->load->view('invoice/invoice_normal_construc_mainten_eot/remuneration', $data, true);
        $support = $this->load->view('invoice/invoice_normal_construc_mainten_eot/support', $data, true);
        $transportation_duty = $this->load->view('invoice/invoice_normal_construc_mainten_eot/transportation_duty', $data, true);
        $officerent_supplies = $this->load->view('invoice/invoice_normal_construc_mainten_eot/officerent_supplies', $data, true);
        $office_furniture_equipment_report_document = $this->load->view('invoice/invoice_normal_construc_mainten_eot/office_furniture_equipment_report_document', $data, true);
        $road_survey = $this->load->view('invoice/invoice_normal_construc_mainten_eot/road_survey', $data, true);
        $survey_equipment = $this->load->view('invoice/invoice_normal_construc_mainten_eot/survey_equipment', $data, true);
        $contingencies = $this->load->view('invoice/invoice_normal_construc_mainten_eot/contingencies', $data, true);
        $attendance = $this->load->view('invoice/invoice_normal_construc_mainten_eot/attendance', $data, true);
        $pdfFilePath = "invoice_report.pdf";

        $margin_left = 4;
        $margin_right = 4;
        $margin_top = 5;
        $margin_bottom = 3;

        $this->mp_pdf->pdf->mPDF('', '', '', '', $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer, '');
        $this->mp_pdf->pdf->mirrorMargins = 1;
        ob_clean();
        ob_flush();

        $this->mp_pdf->pdf->WriteHTML($first_page);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($second_page);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($remuneration);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($support);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($transportation_duty);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($officerent_supplies);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($office_furniture_equipment_report_document);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($road_survey);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($survey_equipment);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($contingencies);

        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($attendance);
        $this->mp_pdf->pdf->Output($pdfFilePath, "I");

        ob_end_flush();
        ob_end_clean();
    }

    //Invoice PDF of Construction - Maintenance .. Formate - 2
    public function const_mm_invoice_pdf($invoiceID) {
        error_reporting(0);
        $this->load->library('mp_pdf');
        $this->load->model('Accountdept_model', 'accdeptmodel');
        // $invoiceID = 219;
        $invoicedata = $this->Front_model->getaccountdetailforexcel($invoiceID);
        $projectname = $this->Front_model->getprojectname($invoicedata->project_id);
        $data['invoicedata'] = $invoicedata;
        //Get All Reimbursable List..
        $data['ReimbursableList'] = $this->accdeptmodel->GetReimbursable_Byproj($invoicedata->project_id);
        //Get Key Professional Staff & Sub Professional Staff Total RateAmount.. Code By Asheesh
        $data['KeyProfSubProfStaffTotRateAmnt'] = $this->accdeptmodel->GetKeyProf_SubProfStaffTotRateAmnt($invoicedata->project_id);
        //Get Admin Staff or Supporting Staff...
        $data['AdminSupportingStaffRateAmnt'] = $this->accdeptmodel->GetStaffRateMMAmnt_Total($invoicedata->project_id, "3");
        //Cumulative up to Previous Bill of Key Prof & Sub Prof..
        $data['CumulativePrevAmnt'] = $this->accdeptmodel->GetKeyProf_SubProf_CumulativuptoPrevAmnt($invoicedata->project_id, $invoicedata->invoiceno);
        //Cumulative up to Previous Bill of Admin Staff or Supporting Staff..
        $data['CumulativeAdminSupportingStaff'] = $this->accdeptmodel->GetAdminSupportingStaffCumulativAmnt($invoicedata->project_id, $invoicedata->invoiceno);
        //Get All Designation Category..
        $data['AllDesignCategArr'] = $this->accdeptmodel->GetAllDesignCategAndTeamByProjID($invoicedata->project_id);
        //Escalation Get...
        $data['EscalationRecArr'] = $this->accdeptmodel->GetEscalationRecArr($invoicedata->project_id, $invoiceID);
        // Start Tax Code .....
        $data['TaxesRecArr'] = $this->accdeptmodel->GetTaxesRecArr($invoicedata->project_id, $invoiceID);

        $firstpage = $this->load->view('invoice/construc_mainten/firstpage', $data, true);
        $secondpage = $this->load->view('invoice/construc_mainten/secondpage', $data, true);
        $remuneration = $this->load->view('invoice/construc_mainten/remuneration', $data, true);
        $support = $this->load->view('invoice/construc_mainten/support', $data, true);

        if ($invoicedata->project_id == "89169") {
            $transportation_duty = $this->load->view('invoice/construc_mainten/transportation_dutyf2', $data, true);
        } else {
            $transportation_duty = $this->load->view('invoice/construc_mainten/transportation_duty', $data, true);
        }

        $office_supplies = $this->load->view('invoice/construc_mainten/office_supplies', $data, true);
        $office_furniture = $this->load->view('invoice/construc_mainten/office_furniture', $data, true);
        $road_survey = $this->load->view('invoice/construc_mainten/road_survey', $data, true);
        $contingency = $this->load->view('invoice/construc_mainten/contingency', $data, true);
        $attendance = $this->load->view('invoice/construc_mainten/attendance', $data, true);
        $pdfFilePath = "invoice_report.pdf";

        $margin_left = 4;
        $margin_right = 4;
        $margin_top = 5;
        $margin_bottom = 3;

        $this->mp_pdf->pdf->mPDF('', '', '', '', $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer, '');
        $this->mp_pdf->pdf->mirrorMargins = 1;
        ob_clean();
        ob_flush();
        $this->mp_pdf->pdf->WriteHTML($firstpage);
        $this->mp_pdf->pdf->AddPage('L');

        $this->mp_pdf->pdf->WriteHTML($secondpage);
        $this->mp_pdf->pdf->AddPage('L');

        $this->mp_pdf->pdf->WriteHTML($remuneration);
        $this->mp_pdf->pdf->AddPage('L');

        $this->mp_pdf->pdf->WriteHTML($support);
        $this->mp_pdf->pdf->AddPage('L');

        $this->mp_pdf->pdf->WriteHTML($transportation_duty);
        $this->mp_pdf->pdf->AddPage('L');

        $this->mp_pdf->pdf->WriteHTML($office_supplies);
        $this->mp_pdf->pdf->AddPage('L');

        $this->mp_pdf->pdf->WriteHTML($office_furniture);
        $this->mp_pdf->pdf->AddPage('L');

        $this->mp_pdf->pdf->WriteHTML($road_survey);
        $this->mp_pdf->pdf->AddPage('L');

        $this->mp_pdf->pdf->WriteHTML($contingency);
        $this->mp_pdf->pdf->AddPage('L');

        $this->mp_pdf->pdf->WriteHTML($attendance);

        $this->mp_pdf->pdf->Output($pdfFilePath, "I");

        ob_end_flush();
        ob_end_clean();
    }

    //Invoice Normal Invoce PDf as Excel..
    public function invocepdf_as_excel($invoiceID) {
        error_reporting(0);
        $this->load->library('mp_pdf');
        $this->load->model('Accountdept_model', 'accdeptmodel');
        $invoicedata = $this->Front_model->getaccountdetailforexcel($invoiceID);
        $projectname = $this->Front_model->getprojectname($invoicedata->project_id);
        $data['invoicedata'] = $invoicedata;
        //Get All Reimbursable List..
        $data['ReimbursableList'] = $this->accdeptmodel->GetReimbursable_Byproj($invoicedata->project_id);

        $typeIdArr = array();
        $TransportationRecArr = null;
        if ($data['ReimbursableList']) {
            foreach ($data['ReimbursableList'] as $rowRecd) {
                if ($rowRecd->reim_type_id):
                    $typeIdArr[] = $rowRecd->reim_type_id;
                endif;
            }
        }

        //Get Key Professional Staff & Sub Professional Staff Total RateAmount.. Code By Asheesh
        $data['KeyProfSubProfStaffTotRateAmnt'] = $this->accdeptmodel->GetKeyProf_SubProfStaffTotRateAmnt($invoicedata->project_id);
        $data['KeyProfStaffTotRateAmnt'] = $this->accdeptmodel->GetKeyProf_staffTotRateAmnt($invoicedata->project_id);
        $data['SubProfStaffTotRateAmnt'] = $this->accdeptmodel->GetSubProf_staffTotRateAmnt($invoicedata->project_id);
        //Get Admin Staff or Supporting Staff...
        $data['AdminSupportingStaffRateAmnt'] = $this->accdeptmodel->GetStaffRateMMAmnt_Total($invoicedata->project_id, "3");
        //Cumulative up to Previous Bill of Key Prof & Sub Prof..
        $data['CumulativePrevAmnt'] = $this->accdeptmodel->GetKeyProf_SubProf_CumulativuptoPrevAmnt($invoicedata->project_id, $invoicedata->invoiceno);
        //Cumulative up to Previous Bill of Admin Staff or Supporting Staff..
        $data['CumulativePrevAmnt_key'] = $this->accdeptmodel->GetKeyProf_CumulativuptoPrevAmnt($invoicedata->project_id, $invoicedata->invoiceno);
        $data['CumulativePrevAmnt_sub'] = $this->accdeptmodel->GetSubProf_CumulativuptoPrevAmnt($invoicedata->project_id, $invoicedata->invoiceno);

        $data['CumulativeAdminSupportingStaff'] = $this->accdeptmodel->GetAdminSupportingStaffCumulativAmnt($invoicedata->project_id, $invoicedata->invoiceno);
        //Get All Designation Category..
        $data['AllDesignCategArr'] = $this->accdeptmodel->GetAllDesignCategAndTeamByProjID($invoicedata->project_id);
        //Escalation Get...
        $data['EscalationRecArr'] = $this->accdeptmodel->GetEscalationRecArr($invoicedata->project_id, $invoiceID);
        // Start Tax Code .....
        $data['TaxesRecArr'] = $this->accdeptmodel->GetTaxesRecArr($invoicedata->project_id, $invoiceID);

//        echo "<pre>";
//        print_r($data['ReimbursableList']);
//        die;

        $data['Global_Invoice_Id'] = $invoiceID;
        $first_page = $this->load->view('invoice/invoce_as_excel/first_page', $data, true);

        if (($invoicedata->project_id == "101119") or ( $invoicedata->project_id == "93164")):
            $second_page = $this->load->view('invoice/invoce_as_excel/second_page_condition', $data, true);
        else:
            $second_page = $this->load->view('invoice/invoce_as_excel/second_page', $data, true);
        endif;

        $remuneration = $this->load->view('invoice/invoce_as_excel/remuneration', $data, true);
        $support = $this->load->view('invoice/invoce_as_excel/support', $data, true);

        //##### TransPortation & Duty Travel #######
        if ((in_array("1", $typeIdArr)) or ( in_array("2", $typeIdArr))) {
            $transportation_duty = $this->load->view('invoice/invoce_as_excel/transportation_duty', $data, true);
        }
        //##### Section Office Rent & Office Supplies, Utilities and Communication #######
        if ((in_array("3", $typeIdArr)) or ( in_array("4", $typeIdArr))) {
            $officerent_supplies = $this->load->view('invoice/invoce_as_excel/officerent_supplies', $data, true);
        }
        //##### Office Furniture and Equipment,Office Equipment (Rental / Hire) & Reports and Document Printing #######
        if ((in_array("5", $typeIdArr)) or ( in_array("6", $typeIdArr)) or ( in_array("7", $typeIdArr))) {
            $office_furniture_equipment_report_document = $this->load->view('invoice/invoce_as_excel/office_furniture_equipment_report_document', $data, true);
        }
        //##### Survey Equipment ###################
        if (in_array("8", $typeIdArr)) {
            $survey_equipment = $this->load->view('invoice/invoce_as_excel/survey_equipment', $data, true);
        }
        //###### Road Survey Equipment #############
        if (in_array("9", $typeIdArr)) {
            $road_survey = $this->load->view('invoice/invoce_as_excel/road_survey', $data, true);
        }
        //###### Contingencies #############
        if (in_array("10", $typeIdArr)) {
            $contingencies = $this->load->view('invoice/invoce_as_excel/contingencies', $data, true);
        }

        $attendance = $this->load->view('invoice/invoce_as_excel/attendance', $data, true);
        $pdfFilePath = "invoice_report.pdf";

        $margin_left = 4;
        $margin_right = 4;
        $margin_top = 5;
        $margin_bottom = 3;

        $this->mp_pdf->pdf->mPDF('', '', '', '', $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer, '');
        $this->mp_pdf->pdf->mirrorMargins = 1;
        ob_clean();
        ob_flush();

        $this->mp_pdf->pdf->WriteHTML($first_page);
        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($second_page);
        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($remuneration);
        $this->mp_pdf->pdf->AddPage('L');
        $this->mp_pdf->pdf->WriteHTML($support);
        $this->mp_pdf->pdf->AddPage('L');

        //##### TransPortation & Duty Travel #######
        if ((in_array("1", $typeIdArr)) or ( in_array("2", $typeIdArr))) {
            $this->mp_pdf->pdf->WriteHTML($transportation_duty);
            $this->mp_pdf->pdf->AddPage('L');
        }
        //##### Section Office Rent & Office Supplies, Utilities and Communication #######
        if ((in_array("3", $typeIdArr)) or ( in_array("4", $typeIdArr))) {
            $this->mp_pdf->pdf->WriteHTML($officerent_supplies);
            $this->mp_pdf->pdf->AddPage('L');
        }
        //##### Office Furniture and Equipment,Office Equipment (Rental / Hire) & Reports and Document Printing #######
        if ((in_array("5", $typeIdArr)) or ( in_array("6", $typeIdArr)) or ( in_array("7", $typeIdArr))) {
            $this->mp_pdf->pdf->WriteHTML($office_furniture_equipment_report_document);
            $this->mp_pdf->pdf->AddPage('L');
        }
        //##### Survey Equipment ###################
        if (in_array("8", $typeIdArr)) {
            $this->mp_pdf->pdf->WriteHTML($survey_equipment);
            $this->mp_pdf->pdf->AddPage('L');
        }
        //###### Road Survey Equipment #############
        if (in_array("9", $typeIdArr)) {
            $this->mp_pdf->pdf->WriteHTML($road_survey);
            $this->mp_pdf->pdf->AddPage('L');
        }
        //###### Contingencies #############
        if (in_array("10", $typeIdArr)) {
            $this->mp_pdf->pdf->WriteHTML($contingencies);
            $this->mp_pdf->pdf->AddPage('L');
        }

        $this->mp_pdf->pdf->WriteHTML($attendance);
        $this->mp_pdf->pdf->Output($pdfFilePath, "I");
        ob_end_flush();
        ob_end_clean();
    }

}
