<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Account_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mastermodel');
        $this->load->library('form_validation');
        if (($this->session->userdata('loginid') == "") or ( $this->session->userdata('assign_role') == "")) {
            redirect(base_url(""));
        }
    }

    public function mypayslip() {
        $id = $this->session->userdata('loginid');
        $data['loginUserRecd'] = $this->mastermodel->GetBasicRecLoginUser();
        $data['title'] = "My Pay Slips";
        if (@$_REQUEST['selectedyear']) {
            $data['selyear'] = $_REQUEST['selectedyear'];
            $data['empPayslipDetailArr'] = $this->mastermodel->GetEmpPayslipsRecByuserID($data['loginUserRecd']->payroll_with_name, $data['selyear']);
        } else {
            $dateYear = date("Y");
            $data['selyear'] = date("Y");
            $data['empPayslipDetailArr'] = $this->mastermodel->GetEmpPayslipsRecByuserID($data['loginUserRecd']->payroll_with_name, $dateYear);
        }
        $this->load->view("account-section/payslips_view", $data);
    }

    //My Salary Details..
    public function mysalarydetails() {
        $id = $this->session->userdata('loginid');
        $data['loginUserRecd'] = $this->mastermodel->GetBasicRecLoginUser();
        $data['title'] = "My Salary Details";
        $data['empSalDetailArr'] = $this->mastermodel->GetEmpSalaryDetailRecByID($id);
        $this->load->view("account-section/mysalarydetails_view", $data);
    }

    //Form 16 / ITR..
    public function myformitr() {
        $data['error'] = '';
        $id = $this->session->userdata('loginid');
        $data['ItrFormDetailsRecArr'] = $this->mastermodel->GetItrFormDetailsRecByID($id);
        $data['title'] = "My ITR Form Details";
        $this->load->view("account-section/itrform_view", $data);
    }

    //Data Table Demo ..
    public function datatable() {
        $data['error'] = '';
        $this->load->view("demodatatable_view", $data);
    }

}
