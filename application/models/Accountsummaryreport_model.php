<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Accountsummaryreport_model extends CI_Model {

    var $table = 'invoice_after_xlsdownload as a';
    // var $state_table = 'states as b';
    //  var $city_table = 'cities as c';
    // var $tenderdetail_table = 'bd_tenderdetail as d';
    //var $column_order = array(null, 'company_name', 'company_details', 'contact_no', 'email_id'); //set column field database for datatable orderable
    //var $column_search = array('company_name', 'company_details', 'contact_no', 'email_id'); //set column field database for datatable searchable 
    var $order = array('fld_id' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    private function _get_datatables_query() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($this->input->post('filterproject')) {

            if ($this->input->post('filterproject')) {
                $this->db->where("$db2.tm_projects.id", $this->input->post('filterproject'));
            }
            if ($this->input->post('filteryear')) {
                $this->db->like("$db1.invoicedetail.invoice_date", $this->input->post('filteryear'));
            }
            if ($this->input->post('filteryear') and $this->input->post('filtermonth')) {
                $YeAr = $this->input->post('filteryear');
                $MoNtH = $this->input->post('filtermonth');
                if ($MoNtH == '12') {
                    $MoNtH = 1;
                    $YeAr = ($YeAr + 1);
                }
                if ($MoNtH != '12') {
                    $MoNtH = ($MoNtH + 1);
                }
                $filterRes = $YeAr . "-" . str_pad($MoNtH, 2, "0", STR_PAD_LEFT);
                $this->db->like("$db1.invoicedetail.invoice_date", $filterRes);
            }

            $this->db->select("$db1.invoice_after_xlsdownload.*,$db2.tm_projects.project_name,$db1.invoicedetail.id,$db1.invoicedetail.invoice_date");
            $this->db->from("$db1.invoice_after_xlsdownload");
            $this->db->join("$db1.accountinfo", "$db1.invoice_after_xlsdownload.project_id = $db1.accountinfo.project_id", 'LEFT');
            $this->db->join("$db2.tm_projects", "$db1.accountinfo.project_numberid = $db2.tm_projects.id", 'LEFT');
            $this->db->join("$db1.invoicedetail", "$db1.invoice_after_xlsdownload.invoice_id = $db1.invoicedetail.id", 'LEFT');
            //$this->db->join("$db1.escalation_tax", "$db1.invoice_after_xlsdownload.invoice_id = $db1.escalation_tax.invoice_id", 'LEFT');
			$this->db->where(array("$db1.invoice_after_xlsdownload.srpos_no" => 'sub_total'));

            $i = 0;
            foreach ($this->column_search as $item) { // loop column 
                if ($_POST['search']['value']) {
                    if ($i === 0) {
                        $this->db->group_start();
                        $this->db->like($item, $_POST['search']['value']);
                    } else {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }
                    if (count($this->column_search) - 1 == $i) //last loop
                        $this->db->group_end();
                }
                $i++;
            }
            if (isset($_POST['order'])) { // here order processing
                $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            } else if (isset($this->order)) {
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        $ResultRec = $query->result();
        $recordArr = array();
        if ($ResultRec) {
            foreach ($ResultRec as $recR) {
                $projId = $recR->project_id;
                $inVcId = $recR->invoice_id;
                $invCdAtE = $recR->invoice_date;
                $recR->remuneration = $this->getRemunerationByProjID($projId, $inVcId);
                $recR->reimbursable = $this->getReimbursableByProjID($projId, $inVcId);
				$recR->escalaration = $this->getescalarionByProjID($projId, $inVcId);
                $recR->taxtotalinvcwise = $this->getTaxByProjInvcID($projId, $inVcId);
                $recR->invoice_date = date("Y-m", strtotime($invCdAtE . ' - 1 month'));
                array_push($recordArr, $recR);
            }
        }
        return ($ResultRec)?$ResultRec:'';
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
       // return '0';
    }

    public function getRemunerationByProjID($projId, $inVcId) {
        $db1 = $this->db1->database;
        $this->db->select_sum("$db1.invoice_after_xlsdownload.current_month_bill");
        $this->db->from("$db1.invoice_after_xlsdownload");
        $this->db->where(array("$db1.invoice_after_xlsdownload.project_id" => $projId, "$db1.invoice_after_xlsdownload.invoice_id" => $inVcId));
        $this->db->where("($db1.invoice_after_xlsdownload.srpos_no='1' OR $db1.invoice_after_xlsdownload.srpos_no='2')", NULL, FALSE);
        $query = $this->db->get();
        $ResultRecord = $query->row();
        return ($ResultRecord) ? $ResultRecord->current_month_bill : '0';
    }

    public function getReimbursableByProjID($projId, $inVcId) {
        $db1 = $this->db1->database;
        $this->db->select_sum("$db1.invoice_after_xlsdownload.current_month_bill");
        $this->db->from("$db1.invoice_after_xlsdownload");
        $this->db->where(array("$db1.invoice_after_xlsdownload.project_id" => $projId, "$db1.invoice_after_xlsdownload.invoice_id" => $inVcId));
        $this->db->where("($db1.invoice_after_xlsdownload.srpos_no='3' OR $db1.invoice_after_xlsdownload.srpos_no='4' OR $db1.invoice_after_xlsdownload.srpos_no='5' OR $db1.invoice_after_xlsdownload.srpos_no='6' OR $db1.invoice_after_xlsdownload.srpos_no='7' OR $db1.invoice_after_xlsdownload.srpos_no='8' OR $db1.invoice_after_xlsdownload.srpos_no='9' OR $db1.invoice_after_xlsdownload.srpos_no='10' OR $db1.invoice_after_xlsdownload.srpos_no='11' OR $db1.invoice_after_xlsdownload.srpos_no='12')", NULL, FALSE);
        $query = $this->db->get();
        $ResultRecord = $query->row();
        return ($ResultRecord) ? $ResultRecord->current_month_bill : '0';
    }
    
    
    //Get All Tax Code By Asheesh 12-08-2019..
    public function getTaxByProjInvcID($projId, $inVcId){
        $db1 = $this->db1->database;
        $this->db->select_sum("$db1.tax_after_xlsdownload.current_month_tax");
        $this->db->from("$db1.tax_after_xlsdownload");
        $this->db->where(array("$db1.tax_after_xlsdownload.status" => "1","$db1.tax_after_xlsdownload.project_id" => $projId, "$db1.tax_after_xlsdownload.invoice_id" => $inVcId));
        $query = $this->db->get();
        $ResultRecord = $query->row();
        return ($ResultRecord) ? $ResultRecord->current_month_tax : '0';  
    }
    
    //code by durgesh for get esclaration
	public function getescalarionByProjID($projId, $inVcId) {
	$db1 = $this->db1->database;
	$this->db->select_sum("$db1.escalation_tax.	perccurtotal");
	$this->db->from("$db1.escalation_tax");
	$this->db->where(array("$db1.escalation_tax.project_id" => $projId, "$db1.escalation_tax.invoice_id" => $inVcId));
	$query = $this->db->get();
	$ResultRecord = $query->row();
	return ($ResultRecord) ? $ResultRecord->perccurtotal : '0';
    }
    
    

}

