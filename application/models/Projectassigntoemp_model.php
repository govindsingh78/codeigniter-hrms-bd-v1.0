<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Projectassigntoemp_model extends CI_Model {

    var $table = 'emp_otherofficial_data as a';

    // var $state_table = 'states as b';
    //  var $city_table = 'cities as c';
    // var $tenderdetail_table = 'bd_tenderdetail as d';
    //var $column_order = array(null, 'company_name', 'company_details', 'contact_no', 'email_id'); //set column field database for datatable orderable
    //var $column_search = array('company_name', 'company_details', 'contact_no', 'email_id'); //set column field database for datatable searchable 
    // var $order = array('fld_id' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    private function _get_datatables_query() {
        $db2 = $this->db2->database;

        $this->db->select("$db2.emp_otherofficial_data.user_id,$db2.emp_otherofficial_data.on_project,$db2.main_employees_summary.userfullname,$db2.main_employees_summary.businessunit_name,$db2.main_employees_summary.department_name");
        $this->db->from("$db2.emp_otherofficial_data");
        $this->db->join("$db2.main_employees_summary", "$db2.emp_otherofficial_data.user_id = $db2.main_employees_summary.user_id", 'LEFT');
        $this->db->where(array("$db2.emp_otherofficial_data.status" => '1'));
        $this->db->where("$db2.emp_otherofficial_data.on_project!=", "");
        $this->db->where("$db2.emp_otherofficial_data.on_project!=", "0");

        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        $ResultRec = $query->result();
        $recordArr = array();
        if ($ResultRec) {
            foreach ($ResultRec as $recR) {
                $projIdes = $recR->on_project;
                $projIdesArr = explode(",", $projIdes);
                $recR->projlist = $this->getOnProjectList($projIdesArr);
                array_push($recordArr, $recR);
            }
        }
        return $ResultRec;
    }

    function count_filtered() {
        $db2 = $this->db2->database;
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $db2 = $this->db2->database;
        $this->db->from("$db2.emp_otherofficial_data");
        $this->db->where(array("$db2.emp_otherofficial_data.status" => '1'));
        $this->db->where("$db2.emp_otherofficial_data.on_project!=", "");
        $this->db->where("$db2.emp_otherofficial_data.on_project!=", "0");
        return $this->db->count_all_results();
        return '0';
    }

    public function getOnProjectList($projIdesArr) {
        $db2 = $this->db2->database;
        $this->db->SELECT("$db2.tm_projects.project_name");
        $this->db->FROM("$db2.tm_projects");
        $this->db->where_in("$db2.tm_projects.id", $projIdesArr);
        $resultArr = $this->db->get()->result();
        $projNm = '';
        if ($resultArr) {
            foreach ($resultArr as $ErecR) {

                if (count($resultArr) > 1) {
                    $projNm .= $ErecR->project_name . " , ";
                } else {
                    $projNm .= $ErecR->project_name;
                }
            }
        }
        return $projNm;
    }

}
