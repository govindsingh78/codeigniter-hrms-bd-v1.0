<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mastermodel extends CI_Model {

    private $table_name = 'cegth_clients';   // Client  

    public function __construct() {
        parent::__construct();
        $this->load->database();

        //Code By Asheesh For Multiple DB Connect..
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    //Insert All Master
    public function InsertMasterData($recArr, $tablename) {
        if ($recArr && $tablename):
            $this->db->insert($tablename, $recArr);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        else:
            return false;
        endif;
    }

    //Get Table Data Record.
    public function GetTableData($cegth_table, $Where) {
        $this->db->select('*');
        $this->db->from($cegth_table);
        $this->db->order_by("id", "DESC");
        $this->db->where($Where);
        $recArr = $this->db->get()->result();
        return $recArr;
    }

    //Select All Country..
    public function allcountry() {
        $this->db->select('*');
        $this->db->from('countries');
        $this->db->order_by("country_name", "ASC");
        $this->db->where(array('status' => '1'));
        $recArr = $this->db->get()->result();
        return $recArr;
    }

    //State Details By Id
    public function StateByCounID($cid) {
        $this->db->select(array('state_id', 'state_name'));
        $this->db->from('states');
        $this->db->order_by("state_name", "ASC");
        $this->db->where(array('status' => '1', 'country_id' => $cid));
        $recArr = $this->db->get()->result();
        return $recArr;
    }

    //New Data Get
    public function GetTableData_new($cegth_table, $Where) {
        $this->db->select(array('state_id', 'state_name'));
        $this->db->from($cegth_table);
        $this->db->order_by("state_name", "ASC");
        $this->db->where($Where);
        foreach ($this->db->get()->result() as $label) {
            $form_titles[$label->state_id] = $label->state_name;
        }
        return ($form_titles) ? $form_titles : false;
    }

    //Select Recored From..
    public function SelectRecord($cegth_table, $Where, $orderfield = '', $ordtype = '') {
        $this->db->select('*');
        $this->db->from($cegth_table);
        //$this->db->order_by($orderBy);
        $this->db->where($Where);
        $this->db->order_by($orderfield, $ordtype);
        return $this->db->get()->result();
    }

    //Select Recored From..
    public function SelectRecordC($cegth_table, $Where) {
        $this->db->select('*');
        $this->db->from($cegth_table);
        $this->db->order_by("country_name", "ASC");
        $this->db->where($Where);
        return $this->db->get()->result();
    }

    //Select Recored From..
    public function SelectRecordS($cegth_table, $Where) {
        $this->db->select('*');
        $this->db->from($cegth_table);
        $this->db->order_by("state_name", "ASC");
        $this->db->where($Where);
        return $this->db->get()->result();
    }

    //Select Recored From..
    public function SelectRecordFld($cegth_table, $Where, $orderfield = '', $ordtype = '') {
        $this->db->select('*');
        $this->db->from($cegth_table);
        $this->db->order_by($orderfield, $ordtype);
        $this->db->where($Where);
        return $this->db->get()->result();
    }

    //Select Recored From..
    public function SelectRecordSingle($cegth_table, $Where) {
        $this->db->select('*');
        $this->db->from($cegth_table);
        $this->db->where($Where);
        $resp = $this->db->get()->result();
        return ($resp) ? $resp[0] : false;
    }

    //Update Master Record..
    public function UpdateRecords($cegth_table, $Where, $data) {
        $this->db->where($Where);
        return $this->db->update($cegth_table, $data);
        // $insert_id = $this->db->insert_id();
        // return $insert_id;
    }

    public function count_rows($cegth_table, $Where) {
        $this->db->select('*');
        $this->db->from($cegth_table);
        $this->db->where($Where);
        $rec = $this->db->count_all_results();
        return ($rec) ? $rec : '0';
    }

    //For Drop Down Project..
    public function TableData_Project_DropDown($cegth_table, $Where) {
        $this->db->select(array('id', 'project_name'));
        $this->db->from($cegth_table);
        $this->db->order_by("project_name", "ASC");
        $this->db->where($Where);
        foreach ($this->db->get()->result() as $label) {
            $form_titles[$label->id] = $label->project_name;
        }
        return ($form_titles) ? $form_titles : false;
    }

    //check login
    public function login_user_action($LoginCredentialArr) {
        $Where = array('employeeId' => $LoginCredentialArr['username'], 'emppassword' => md5($LoginCredentialArr['password']), 'isactive' => '1');
        $this->db2->select('*');
        $this->db2->from('main_users');
        $this->db2->where($Where);
        $recArr = $this->db2->get()->result();
        if ($recArr):
            return $recArr;
        else:
            return false;
        endif;
    }

    //Count All
    public function count_all() {
        $this->db->from($this->table);
        $this->db->where('status', '1');
        return $this->db->count_all_results();
    }

    //Count Row By Cond..
    public function count_allByCond($table, $where) {
        $this->db->from($table);
        $this->db->where($where);
        return $this->db->count_all_results();
    }

    //Segment For Drop Down..
    public function SegmentDropDown($projid) {
        $this->db->select(array('id', 'segment_name', 'segment_code'));
        $this->db->from('cegth_segment_ofproject');
        $this->db->where(array('project_id' => $projid, 'status' => '1'));
        $resultArr = $this->db->get()->result();
        if ($resultArr):
            return $resultArr;
        else:
            return false;
        endif;
    }

    //Resource For Drop Down..
    public function ResourcesDropDown($projid) {
        $this->db->select(array('id', 'employee_ids'));
        $this->db->from('cegth_assign_resources_project');
        $this->db->where(array('project_id' => $projid, 'status' => '1'));
        $resultArr = $this->db->get()->result();
        if ($resultArr):
            return $resultArr[0];
        else:
            return false;
        endif;
    }

    //Get Company Name By ID ..
    public function GetCompNameById($compID) {
        $this->db->select(array('company_name'));
        $this->db->from('main_company');
        $this->db->where(array('status' => '1', 'fld_id' => $compID));
        $RecArr = $this->db->get()->result();
        return ucwords(strtolower($RecArr[0]->company_name));
    }

    public function GetCompNameDetailById($compID, $cegexpID) {
        $this->db->select(array('company_name'));
        $this->db->from('main_company');
        $this->db->where(array('status' => '1', 'fld_id' => $compID));
        $RecArr = $this->db->get()->result();

        $this->db->select('lead_comp_id,joint_venture,asso_comp');
        $this->db->from('jv_cegexp');
        $this->db->where(array('status' => '1', 'project_id' => $cegexpID, 'base_comp_id' => $compID));
        $RecArr1 = $this->db->get()->result();
        $RecArr2 = array();
        if (!empty($RecArr1)) {
            $compID = $RecArr1[0]->lead_comp_id;
            $id = explode(',', $compID);
            //print_r($id);
            $this->db->select('company_name');
            $this->db->from('main_company');
            $this->db->where('status', '1');
            $this->db->where_in('fld_id', $id);
            $RecArr2 = $this->db->get()->result();

            $compID1 = $RecArr1[0]->joint_venture;

            $id1 = explode(',', $compID1);
            $this->db->select('company_name');
            $this->db->from('main_company');
            $this->db->where('status', '1');
            $this->db->where_in('fld_id', $id1);
            $RecArr3 = $this->db->get()->result();

            $compID2 = $RecArr1[0]->asso_comp;
            $id2 = explode(',', $compID2);
            $this->db->select('company_name');
            $this->db->from('main_company');
            $this->db->where('status', '1');
            $this->db->where_in('fld_id', $id2);
            $RecArr4 = $this->db->get()->result();
        }

        if (!empty($RecArr2)) {
            foreach ($RecArr2 as $val) {
                $lead[] = $val->company_name . '<b>(Lead)</b>';
            }
            $lead = implode(',', $lead);
        } else {
            $lead = ucwords(strtolower($RecArr[0]->company_name));
        }

        if (!empty($RecArr3)) {
            foreach ($RecArr3 as $val) {
                $joint[] = $val->company_name . '<b>(JV)</b>';
            }
            $joint = implode(',', $joint);
        }

        if (!empty($RecArr4)) {
            foreach ($RecArr4 as $val) {
                $assoc[] = $val->company_name . '<b>(AC)</b>';
            }
            $assoc = implode(',', $assoc);
        }

        return $lead . '<br/>' . $joint . '<br/>' . $assoc;
    }

    //Sector Name By Id..
    public function GetSectNameById($secID) {

        if ($secID == 'all') {
            return "All Sectors";
        }

        $this->db->select(array('sector_name'));
        $this->db->from('designation_sector');
        $this->db->where(array('status' => '1', 'id' => $secID));
        $RecArr = $this->db->get()->result();
        return ucwords(strtolower($RecArr[0]->sector_name));
    }

    //Sector Name By Id id will be seprate with commas...  
    public function GetSectNameByIdC($secIDArr) {
        $IdsecArr = explode(",", $secIDArr);
        if ($IdsecArr):
            foreach ($IdsecArr as $rrow) {
                $recReturn .= $this->GetSectNameById($rrow);
                if ($recReturn) {
                    $recReturn .= ",";
                }
            }
        endif;
        return rtrim($recReturn, ',');
    }

    //Get Country Name By Id..
    function countNameById($counId) {
        $this->db->select(array('country_name'));
        $this->db->from('countries');
        $this->db->where(array('status' => '1', 'country_id' => $counId));
        $RecArr = $this->db->get()->result();
        return ucwords(strtolower($RecArr[0]->country_name));
    }

    //Field Name By Id or Nature of Comp By Id..
    function GetFieldNameById($idd) {
        $this->db->select(array('field_name'));
        $this->db->from('comp_field_master');
        $this->db->where(array('status' => '1', 'fld_id' => $idd));
        $RecArr = $this->db->get()->result();
        return ucwords(strtolower($RecArr[0]->field_name));
    }

    //Sector Name By Id id will be seprate with commas...  
    public function GetFieldNameC($contArr) {
        $IdsecArr = explode(",", $contArr);
        if ($IdsecArr):
            foreach ($IdsecArr as $rrow) {
                $recReturn .= $this->GetFieldNameById($rrow) . " , ";
            }
        endif;
        return $recReturn;
    }

    //Sector Name By Id id will be seprate with commas...  
    public function GetOperatingC($contArr) {
        $IdsecArr = explode(",", $contArr);
        if ($IdsecArr):
            foreach ($IdsecArr as $rrow) {
                $recReturn .= $this->countNameById($rrow) . " , ";
            }
        endif;
        return $recReturn;
    }

    //Get State Name By Id..
    function GetStateNameById($stateId) {
        $this->db->select(array('state_name'));
        $this->db->from('states');
        $this->db->where(array('status' => '1', 'state_id' => $stateId));
        $RecArr = $this->db->get()->result();
        return ucwords(strtolower($RecArr[0]->state_name));
    }

    //Get Name By Field of Company company comp_field_master
    public function GetnatureofCompanyById($fieldID) {
        $this->db->select(array('field_name'));
        $this->db->from('comp_field_master');
        $this->db->where(array('status' => '1', 'fld_id' => $fieldID));
        $RecArr = $this->db->get()->result();
        return ucwords(strtolower($RecArr[0]->field_name));
    }

    //Delete Master Rec
    public function DeleteRowRecord($table, $delWhere) {
        $this->db->where($delWhere);
        return $this->db->delete($table);
    }

    //Get Min Val
    public function getMinVal($table, $field, $where) {
        $this->db->select_min($field);
        $this->db->where($where);
        $query = $this->db->get($table);
        $result = $query->result_array();
        $val = $result[0][$field];
        return $val;
    }

    //Tender Genrate Id By Project Id...
    public function ProjIdByGenId($genratedId) {
        if ($genratedId):
            $this->db->select(array('project_id'));
            $this->db->from('tender_generated_id');
            $this->db->where(array('generated_tenderid' => $genratedId));
            $this->db->limit('1');
            $RecArr = $this->db->get()->result();
            return ucwords(strtolower($RecArr[0]->project_id));
        else:
            return false;
        endif;
    }

    //Get user Email By Id..
    public function UserEmailById($empId) {
        $catnm = $this->SelectRecordFld('main_users', array('fld_id' => $empId));
        if ($catnm) {
            //$userdata = $catnm->first_row();
            $res = $catnm[0]->emailaddress;
            return $res;
        } else {
            return false;
        }
    }

    //Top Management And Prop Management.
    public function prop_manager_topmanageIDs() {
        $this->db->select('*');
        $this->db->from('bd_role');
        $this->db->where("(role_id='7' OR role_id='1')", NULL, FALSE);
        $idsRecArr = $this->db->get()->result();

        $resp = array();
        if ($idsRecArr) {
            foreach ($idsRecArr as $rw):
                $singleArr = array('userid' => $rw->user_id, 'userName' => UserNameById($rw->user_id), 'useremail' => $this->UserEmailById($rw->user_id));
                array_push($resp, $singleArr);
            endforeach;
        }
        return $resp;
    }

    //Top Management And Prop Management.
    public function prop_manager_AllIDs() {
        $this->db->select('*');
        $this->db->from('bd_role');
        $this->db->where("(role_id='1' OR role_id='3' OR role_id='4')", NULL, FALSE);
        $idsRecArr = $this->db->get()->result();
        $resp = array();
        if ($idsRecArr) {
            foreach ($idsRecArr as $rw):
                $user = UserNameById($rw->user_id);
                $singleArr = array('userid' => $rw->user_id, 'useremail' => $user->ofcialemail);
                array_push($resp, $singleArr);
            endforeach;
        }
        return $resp;
    }

    public function GetRfpByPm($pid, $designID) {
        $this->db->select('*');
        $this->db->from('team_assign');
        $this->db->where("project_id", $pid);
        $this->db->where("designation_id", $designID);
        $recArr = $this->db->get()->row();
        return ($recArr) ? $recArr->weightage_marks_rfp : false;
    }

    //Get Bid Security Type By Id..
    function getBidSecurityById($id) {
        $this->db->select('security_type');
        $this->db->from('bidsecurity_type');
        $this->db->where(array('is_active' => '1', 'id' => $id));
        $RecArr = $this->db->get()->result();
        return ucwords(strtolower($RecArr[0]->security_type));
    }

    public function count_rowscegpds($id) {
        $this->db->select('*');
        $this->db->from('ceg_pds_detail');
        $Where = array('ceg_exp_id' => $id);
        $this->db->where($Where);
        $rec = $this->db->count_all_results();
        return ($rec) ? $rec : '0';
    }

    public function getProjectData() {
        $current_years = date("Y");
        $this->db->select('a.*,b.generated_tenderid,b.generate_type,c.assign_to,e.project_status');
        $this->db->from('bd_tenderdetail as a');
        $this->db->join('bdproject_status as e', 'a.fld_id = e.project_id', 'left');
        $this->db->join('tender_generated_id as b', 'a.fld_id = b.project_id', 'left');
        $this->db->join('assign_project_manager as c', 'a.fld_id = c.project_id', 'left');
        $this->db->join('bdtender_scope as f', "a.fld_id = f.project_id", "left");
        $start_date = $current_years . '-01-01';
        //$where_date = "(b.entrydate >='$start_date')";
        $where_date = "(a.Expiry_Date >='$start_date')";
        $this->db->where($where_date);
        $this->db->where('f.visible_scope', 'Bid_project');

        $result = $this->db->get()->result();
        return isset($result) ? $result : false;
    }

    //Get Name By Field of Country by hocountry_id
    public function GetcountryByHOId($country_id) {
        $this->db->select('country_name');
        $this->db->from('countries');
        $this->db->where(array('status' => '1', 'country_id' => $country_id));
        $RecArr = $this->db->get()->result();
        return ucwords(strtolower($RecArr[0]->country_name));
    }

    public function geteotdetailLastID($projectid) {
        $this->db->select("*");
        $this->db->from("acceot_date");
        $this->db->where("project_id", $projectid);
        $this->db->where("statusid", 1);
        $this->db->order_by("id", 'desc');
        $this->db->limit(1);

        $result = $this->db->get()->result_object();
        return isset($result[0]) ? $result[0] : false;
    }

    public function getmonthattdence($month, $year, $project_id, $empid, $designation_id) {

        //chk Rec.. Count Code By Asheesh..



        $this->db->select("*");
        $this->db->from("timeshet_fill");
        $this->db->where("timeshet_fill.emp_id", $empid);
        $this->db->where("timeshet_fill.designation_id", $designation_id);
        $this->db->where("timeshet_fill.project_id", $project_id);
        $this->db->where("timeshet_fill.year", $year);
        $this->db->where("timeshet_fill.month", $month);
        $recArr = $this->db->get()->result_array();
        return isset($recArr[0]) ? $recArr[0] : false;
    }

    public function GetStaffByProjID($project_id, $keyid) {
        $db2 = $this->db1->database;
        $db1 = $this->db2->database;
        $this->db->select("$db2.assign_finalteam.project_id,$db2.assign_finalteam.empname,$db2.assign_finalteam.id,$db2.assign_finalteam.srno,$db2.assign_finalteam.designation_id,$db2.assign_finalteam.empname,$db2.assign_finalteam.project_id,$db1.main_employees_summary.user_id,$db1.main_employees_summary.userfullname,$db2.designation_master_requisition.designation_name,$db2.assign_finalteam.man_months,$db2.assign_finalteam.rate");
        $this->db->from("$db2.assign_finalteam");
        $this->db->join("$db1.main_employees_summary", "$db2.assign_finalteam.empname = $db1.main_employees_summary.user_id", 'left');
        $this->db->join("$db2.designation_master_requisition", "$db2.assign_finalteam.designation_id = $db2.designation_master_requisition.fld_id", 'left');

        $this->db->where("$db2.assign_finalteam.project_id", $project_id);
        $this->db->where("$db2.assign_finalteam.key_id", $keyid);
        $this->db->order_by("$db2.assign_finalteam.srno", 'asc');
        $recArr = $this->db->get()->result_array();
        $pluginArr = array();
        if ($recArr) {
            foreach ($recArr as $Rowarr) {
                $projID = $Rowarr['project_id'];
                $empId = $Rowarr['empname'];
                $Rowarr['BalanceMm'] = $this->Gettotalcumulativemm($projID, $empId);
                array_push($pluginArr, $Rowarr);
            }
        }
        return ($pluginArr) ? $pluginArr : '';
    }

    //2019 Code By Asheesh..
    public function GetStaffByProjID2($cegexpID, $catTyper) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->SELECT("$db1.assign_finalteam.project_id,$db1.assign_finalteam.empname,$db1.assign_finalteam.empnameother,$db1.assign_finalteam.rate,$db1.assign_finalteam.man_months,$db1.designation_master_requisition.designation_name,$db2.main_employees_summary.userfullname,$db1.team_req_otheremp.emp_name,$db1.replacementteam.replacement_id");
        $this->db->from("$db1.assign_finalteam");
        $this->db->where("$db1.assign_finalteam.project_id", $cegexpID);
        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id=$db1.designation_master_requisition.fld_id", "LEFT");
        $this->db->join("$db2.main_employees_summary", "$db1.assign_finalteam.empname=$db2.main_employees_summary.user_id", "LEFT");
        $this->db->join("$db1.team_req_otheremp", "($db1.assign_finalteam.empnameother=$db1.team_req_otheremp.fld_id AND $db1.assign_finalteam.empnameother!='')", "LEFT");
        $this->db->join("$db1.replacementteam", "($db1.replacementteam.replacement_id=$db1.assign_finalteam.id AND $db1.replacementteam.status='1')", "LEFT");
        $this->db->where(array("$db1.assign_finalteam.key_id" => $catTyper, "$db1.assign_finalteam.status" => "1", "$db1.assign_finalteam.teamstatus" => "1", "$db1.designation_master_requisition.is_active" => "1"));
        $this->db->order_by("$db1.assign_finalteam.srno", "ASC");
        $recArr = $this->db->get()->result_array();

        $pluginArr = array();
        if ($recArr) {
            foreach ($recArr as $Rowarr) {
                $projID = $Rowarr['project_id'];
                $empId = $Rowarr['empname'];
                $Rowarr['BalanceMm'] = $this->Gettotalcumulativemm($projID, $empId);
                array_push($pluginArr, $Rowarr);
            }
        }

        return ($pluginArr) ? $pluginArr : null;
    }

    public function Gettotalcumulativemm($projID, $empId) {
        //Get Lastest invc details for blnv mm...
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->SELECT("$db1.invoicesave.*");
        $this->db->FROM("$db1.invoicesave");
        $this->db->WHERE(array("$db1.invoicesave.project_id" => $projID, "$db1.invoicesave.emp_id" => $empId));
        $this->db->ORDER_BY("$db1.invoicesave.id", "DESC");
        $recRowArr = $this->db->get()->row();
        $BalanceMm = null;
        if ($recRowArr) {
            $BalanceMm = ($recRowArr->mm - $recRowArr->totalcumulativemm);
        }
        return $BalanceMm;
    }

    public function GetAllRecCountArr() {
        $db2 = $this->db2->database;
        $this->db->select("$db2.countries.*");
        $this->db->from("$db2.countries");
        $this->db->order_by("$db2.countries.country_name", "ASC");
        $this->db->where("$db2.countries.status", "1");
        return $this->db->get()->result();
    }

    public function GetAllRecStatesByCid($counID) {
        $db2 = $this->db2->database;
        $this->db->select("$db2.states.*");
        $this->db->from("$db2.states");
        $this->db->order_by("$db2.states.state_name", "ASC");
        $this->db->where("$db2.states.status", "1");
        $this->db->where("$db2.states.country_id", $counID);
        return $this->db->get()->result();
    }

    public function SelectRecordCity($StateId) {
        $db2 = $this->db2->database;
        $this->db->SELECT("$db2.cities.*");
        $this->db->FROM("$db2.cities");
        $this->db->order_by("$db2.cities.city_name", "ASC");
        $this->db->where("$db2.cities.status", "1");
        $this->db->where("$db2.cities.state_id", $StateId);
        return $this->db->get()->result();
    }

    public function getmonthattdence_new($month, $year, $project_id, $empid, $designation_id) {

        //chk Rec.. Count Code By Asheesh..
        $this->db->select("a.id");
        $this->db->from("timeshet_fill as a");
        $this->db->where(array("a.designation_id" => $designation_id, "a.project_id" => $project_id, "a.year" => $year, "a.month" => $month, "a.is_active" => "1"));
        $NumRowCount = $this->db->get()->num_rows();

        //If Single Emp.. in Month..
        if (($NumRowCount < 2) and ( $NumRowCount != "0")) {
            $this->db->select("*");
            $this->db->from("timeshet_fill");
            $this->db->where("timeshet_fill.emp_id", $empid);
            $this->db->where("timeshet_fill.designation_id", $designation_id);
            $this->db->where("timeshet_fill.project_id", $project_id);
            $this->db->where("timeshet_fill.year", $year);
            $this->db->where("timeshet_fill.month", $month);
            $recArr = $this->db->get()->result_array();
            return isset($recArr[0]) ? $recArr[0] : false;
        }
        //if Multiple Emp.. Month..
        if (($NumRowCount > 1) and ( $NumRowCount != "0")) {
            $this->db->select("*");
            $this->db->from("timeshet_fill");
            //$this->db->where("timeshet_fill.emp_id", $empid);
            $this->db->where("timeshet_fill.designation_id", $designation_id);
            $this->db->where("timeshet_fill.project_id", $project_id);
            $this->db->where("timeshet_fill.year", $year);
            $this->db->where("timeshet_fill.month", $month);
            $ResprecArr = $this->db->get()->result_array();

            //Extra Condition For Replace Ment
            //1..
            if ($ResprecArr[0]['1']) {
                $Atten1 = $ResprecArr[0]['1'];
            } else if ($ResprecArr[1]['1']) {
                $Atten1 = $ResprecArr[1]['1'];
            } else if ($ResprecArr[2]['1']) {
                $Atten1 = $ResprecArr[2]['1'];
            } else {
                $Atten1 = '';
            }
            //2..
            if ($ResprecArr[0]['2']) {
                $Atten2 = $ResprecArr[0]['2'];
            } else if ($ResprecArr[1]['2']) {
                $Atten2 = $ResprecArr[1]['2'];
            } else if ($ResprecArr[2]['2']) {
                $Atten2 = $ResprecArr[2]['2'];
            } else {
                $Atten2 = '';
            }
            //3..
            if ($ResprecArr[0]['3']) {
                $Atten3 = $ResprecArr[0]['3'];
            } else if ($ResprecArr[1]['3']) {
                $Atten3 = $ResprecArr[1]['3'];
            } else if ($ResprecArr[2]['3']) {
                $Atten3 = $ResprecArr[2]['3'];
            } else {
                $Atten3 = '';
            }
            //4..
            if ($ResprecArr[0]['4']) {
                $Atten4 = $ResprecArr[0]['4'];
            } else if ($ResprecArr[1]['4']) {
                $Atten4 = $ResprecArr[1]['4'];
            } else if ($ResprecArr[2]['4']) {
                $Atten4 = $ResprecArr[2]['4'];
            } else {
                $Atten4 = '';
            }
            //5..
            if ($ResprecArr[0]['5']) {
                $Atten5 = $ResprecArr[0]['5'];
            } else if ($ResprecArr[1]['5']) {
                $Atten5 = $ResprecArr[1]['5'];
            } else if ($ResprecArr[2]['5']) {
                $Atten5 = $ResprecArr[2]['5'];
            } else {
                $Atten5 = '';
            }
            //6..
            if ($ResprecArr[0]['6']) {
                $Atten6 = $ResprecArr[0]['6'];
            } else if ($ResprecArr[1]['6']) {
                $Atten6 = $ResprecArr[1]['6'];
            } else if ($ResprecArr[2]['6']) {
                $Atten6 = $ResprecArr[2]['6'];
            } else {
                $Atten6 = '';
            }
            //7..
            if ($ResprecArr[0]['7']) {
                $Atten7 = $ResprecArr[0]['7'];
            } else if ($ResprecArr[1]['7']) {
                $Atten7 = $ResprecArr[1]['7'];
            } else if ($ResprecArr[2]['7']) {
                $Atten7 = $ResprecArr[2]['7'];
            } else {
                $Atten7 = '';
            }
            //8..
            if ($ResprecArr[0]['8']) {
                $Atten8 = $ResprecArr[0]['8'];
            } else if ($ResprecArr[1]['8']) {
                $Atten8 = $ResprecArr[1]['8'];
            } else if ($ResprecArr[2]['8']) {
                $Atten8 = $ResprecArr[2]['8'];
            } else {
                $Atten8 = '';
            }
            //9..
            if ($ResprecArr[0]['9']) {
                $Atten9 = $ResprecArr[0]['9'];
            } else if ($ResprecArr[1]['9']) {
                $Atten9 = $ResprecArr[1]['9'];
            } else if ($ResprecArr[2]['9']) {
                $Atten9 = $ResprecArr[2]['9'];
            } else {
                $Atten9 = '';
            }
            //10..
            if ($ResprecArr[0]['10']) {
                $Atten10 = $ResprecArr[0]['10'];
            } else if ($ResprecArr[1]['10']) {
                $Atten10 = $ResprecArr[1]['10'];
            } else if ($ResprecArr[2]['10']) {
                $Atten10 = $ResprecArr[2]['10'];
            } else {
                $Atten10 = '';
            }
            //11..
            if ($ResprecArr[0]['11']) {
                $Atten11 = $ResprecArr[0]['11'];
            } else if ($ResprecArr[1]['11']) {
                $Atten11 = $ResprecArr[1]['11'];
            } else if ($ResprecArr[2]['11']) {
                $Atten11 = $ResprecArr[2]['11'];
            } else {
                $Atten11 = '';
            }
            //12..
            if ($ResprecArr[0]['12']) {
                $Atten12 = $ResprecArr[0]['12'];
            } else if ($ResprecArr[1]['12']) {
                $Atten12 = $ResprecArr[1]['12'];
            } else if ($ResprecArr[2]['12']) {
                $Atten12 = $ResprecArr[2]['12'];
            } else {
                $Atten12 = '';
            }
            //13..
            if ($ResprecArr[0]['13']) {
                $Atten13 = $ResprecArr[0]['13'];
            } else if ($ResprecArr[1]['13']) {
                $Atten13 = $ResprecArr[1]['13'];
            } else if ($ResprecArr[2]['13']) {
                $Atten13 = $ResprecArr[2]['13'];
            } else {
                $Atten13 = '';
            }
            //14..
            if ($ResprecArr[0]['14']) {
                $Atten14 = $ResprecArr[0]['14'];
            } else if ($ResprecArr[1]['14']) {
                $Atten14 = $ResprecArr[1]['14'];
            } else if ($ResprecArr[2]['14']) {
                $Atten14 = $ResprecArr[2]['14'];
            } else {
                $Atten14 = '';
            }
            //15..
            if ($ResprecArr[0]['15']) {
                $Atten15 = $ResprecArr[0]['15'];
            } else if ($ResprecArr[1]['15']) {
                $Atten15 = $ResprecArr[1]['15'];
            } else if ($ResprecArr[2]['15']) {
                $Atten15 = $ResprecArr[2]['15'];
            } else {
                $Atten15 = '';
            }
            //16..
            if ($ResprecArr[0]['16']) {
                $Atten16 = $ResprecArr[0]['16'];
            } else if ($ResprecArr[1]['16']) {
                $Atten16 = $ResprecArr[1]['16'];
            } else if ($ResprecArr[2]['16']) {
                $Atten16 = $ResprecArr[2]['16'];
            } else {
                $Atten16 = '';
            }
            //17..
            if ($ResprecArr[0]['17']) {
                $Atten17 = $ResprecArr[0]['17'];
            } else if ($ResprecArr[1]['17']) {
                $Atten17 = $ResprecArr[1]['17'];
            } else if ($ResprecArr[2]['17']) {
                $Atten17 = $ResprecArr[2]['17'];
            } else {
                $Atten17 = '';
            }
            //18..
            if ($ResprecArr[0]['18']) {
                $Atten18 = $ResprecArr[0]['18'];
            } else if ($ResprecArr[1]['18']) {
                $Atten18 = $ResprecArr[1]['18'];
            } else if ($ResprecArr[2]['18']) {
                $Atten18 = $ResprecArr[2]['18'];
            } else {
                $Atten18 = '';
            }
            //19..
            if ($ResprecArr[0]['19']) {
                $Atten19 = $ResprecArr[0]['19'];
            } else if ($ResprecArr[1]['19']) {
                $Atten19 = $ResprecArr[1]['19'];
            } else if ($ResprecArr[2]['19']) {
                $Atten19 = $ResprecArr[2]['19'];
            } else {
                $Atten19 = '';
            }
            //20..
            if ($ResprecArr[0]['20']) {
                $Atten20 = $ResprecArr[0]['20'];
            } else if ($ResprecArr[1]['20']) {
                $Atten20 = $ResprecArr[1]['20'];
            } else if ($ResprecArr[2]['20']) {
                $Atten20 = $ResprecArr[2]['20'];
            } else {
                $Atten20 = '';
            }
            //21..
            if ($ResprecArr[0]['21']) {
                $Atten21 = $ResprecArr[0]['21'];
            } else if ($ResprecArr[1]['21']) {
                $Atten21 = $ResprecArr[1]['21'];
            } else if ($ResprecArr[2]['21']) {
                $Atten21 = $ResprecArr[2]['21'];
            } else {
                $Atten21 = '';
            }
            //22..
            if ($ResprecArr[0]['22']) {
                $Atten22 = $ResprecArr[0]['22'];
            } else if ($ResprecArr[1]['22']) {
                $Atten22 = $ResprecArr[1]['22'];
            } else if ($ResprecArr[2]['22']) {
                $Atten22 = $ResprecArr[2]['22'];
            } else {
                $Atten22 = '';
            }
            //23..
            if ($ResprecArr[0]['23']) {
                $Atten23 = $ResprecArr[0]['23'];
            } else if ($ResprecArr[1]['23']) {
                $Atten23 = $ResprecArr[1]['23'];
            } else if ($ResprecArr[2]['23']) {
                $Atten23 = $ResprecArr[2]['23'];
            } else {
                $Atten23 = '';
            }
            //24..
            if ($ResprecArr[0]['24']) {
                $Atten24 = $ResprecArr[0]['24'];
            } else if ($ResprecArr[1]['24']) {
                $Atten24 = $ResprecArr[1]['24'];
            } else if ($ResprecArr[2]['24']) {
                $Atten24 = $ResprecArr[2]['24'];
            } else {
                $Atten24 = '';
            }
            //25..
            if ($ResprecArr[0]['25']) {
                $Atten25 = $ResprecArr[0]['25'];
            } else if ($ResprecArr[1]['25']) {
                $Atten25 = $ResprecArr[1]['25'];
            } else if ($ResprecArr[2]['25']) {
                $Atten25 = $ResprecArr[2]['25'];
            } else {
                $Atten25 = '';
            }
            //26..
            if ($ResprecArr[0]['26']) {
                $Atten26 = $ResprecArr[0]['26'];
            } else if ($ResprecArr[1]['26']) {
                $Atten26 = $ResprecArr[1]['26'];
            } else if ($ResprecArr[2]['26']) {
                $Atten26 = $ResprecArr[2]['26'];
            } else {
                $Atten26 = '';
            }
            //27..
            if ($ResprecArr[0]['27']) {
                $Atten27 = $ResprecArr[0]['27'];
            } else if ($ResprecArr[1]['27']) {
                $Atten27 = $ResprecArr[1]['27'];
            } else if ($ResprecArr[2]['27']) {
                $Atten27 = $ResprecArr[2]['27'];
            } else {
                $Atten27 = '';
            }
            //28..
            if ($ResprecArr[0]['28']) {
                $Atten28 = $ResprecArr[0]['28'];
            } else if ($ResprecArr[1]['28']) {
                $Atten28 = $ResprecArr[1]['28'];
            } else if ($ResprecArr[2]['28']) {
                $Atten28 = $ResprecArr[2]['28'];
            } else {
                $Atten28 = '';
            }
            //29..
            if ($ResprecArr[0]['29']) {
                $Atten29 = $ResprecArr[0]['29'];
            } else if ($ResprecArr[1]['29']) {
                $Atten29 = $ResprecArr[1]['29'];
            } else if ($ResprecArr[2]['29']) {
                $Atten29 = $ResprecArr[2]['29'];
            } else {
                $Atten29 = '';
            }
            //30..
            if ($ResprecArr[0]['30']) {
                $Atten30 = $ResprecArr[0]['30'];
            } else if ($ResprecArr[1]['30']) {
                $Atten30 = $ResprecArr[1]['30'];
            } else if ($ResprecArr[2]['30']) {
                $Atten30 = $ResprecArr[2]['30'];
            } else {
                $Atten30 = '';
            }
            //31..
            if ($ResprecArr[0]['31']) {
                $Atten31 = $ResprecArr[0]['31'];
            } else if ($ResprecArr[1]['31']) {
                $Atten31 = $ResprecArr[1]['31'];
            } else if ($ResprecArr[2]['31']) {
                $Atten31 = $ResprecArr[2]['31'];
            } else {
                $Atten31 = '';
            }

            $ResprecArr[0]['1'] = $Atten1;
            $ResprecArr[0]['2'] = $Atten2;
            $ResprecArr[0]['3'] = $Atten3;
            $ResprecArr[0]['4'] = $Atten4;
            $ResprecArr[0]['5'] = $Atten5;
            $ResprecArr[0]['6'] = $Atten6;
            $ResprecArr[0]['7'] = $Atten7;
            $ResprecArr[0]['8'] = $Atten8;
            $ResprecArr[0]['9'] = $Atten9;
            $ResprecArr[0]['10'] = $Atten10;
            $ResprecArr[0]['11'] = $Atten11;
            $ResprecArr[0]['12'] = $Atten12;
            $ResprecArr[0]['13'] = $Atten13;
            $ResprecArr[0]['14'] = $Atten14;
            $ResprecArr[0]['15'] = $Atten15;
            $ResprecArr[0]['16'] = $Atten16;
            $ResprecArr[0]['17'] = $Atten17;
            $ResprecArr[0]['18'] = $Atten18;
            $ResprecArr[0]['19'] = $Atten19;
            $ResprecArr[0]['20'] = $Atten20;
            $ResprecArr[0]['21'] = $Atten21;
            $ResprecArr[0]['22'] = $Atten22;
            $ResprecArr[0]['23'] = $Atten23;
            $ResprecArr[0]['24'] = $Atten24;
            $ResprecArr[0]['25'] = $Atten25;
            $ResprecArr[0]['26'] = $Atten26;
            $ResprecArr[0]['27'] = $Atten27;
            $ResprecArr[0]['28'] = $Atten28;
            $ResprecArr[0]['29'] = $Atten29;
            $ResprecArr[0]['30'] = $Atten30;
            $ResprecArr[0]['31'] = $Atten31;
            return isset($ResprecArr[0]) ? $ResprecArr[0] : false;
        }
    }

   

}

?>