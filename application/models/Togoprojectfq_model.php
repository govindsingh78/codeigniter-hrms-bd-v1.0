<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Togoprojectfq_model extends CI_Model {

    var $table = 'bd_tenderdetail as a';
    var $genertedid_table = 'tender_generated_id as b';
    var $projectmanager_table = 'assign_project_manager as c';
    //var $users_table = 'main_users as d';
    var $scope_table = 'bdtender_scope as e';
    var $column_order = array(null, 'a.TenderDetails', 'a.Location', 'a.Organization', 'a.keyword_phase');
    var $column_search = array('b.generated_tenderid', 'a.keyword_phase', 'a.TenderDetails', 'a.Location', 'a.Organization');
    var $order = array('a.Created_Date' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {
        if ($this->input->post('sectorinput')) {
            $this->db->like('a.Sector_IDs', $this->input->post('sectorinput'));
        }

        if ($this->input->post('financial_year')) {
            $financial_year = $this->input->post('financial_year');
            $start_date = $financial_year . '-04-01';
            $end_date = ($financial_year + 1) . '-03-31';
            $where_date = "(a.Expiry_Date>='$start_date' AND a.Expiry_Date <= '$end_date')";
            
             
            $this->db->where($where_date);
        } 
		// else {
            // $checkdate = date("d-m-Y");
            // $current_years = date("Y");
            // $current_years_chk = '01-04-' . $current_years;

            // if ($current_years_chk <= $checkdate) {
                // $start_date = $current_years . '-04-01';
                // $end_date = ($current_years + 1) . '-03-31';
                // $where_date = "(a.Expiry_Date>='$start_date' AND a.Expiry_Date <= '$end_date')";
                // $this->db->where($where_date);
            // } else {
                // $start_date = ($current_years - 1) . '-04-01';
                // $end_date = $current_years . '-03-31';
                // $where_date = "(a.Expiry_Date>='$start_date' AND a.Expiry_Date <= '$end_date')";
                // $this->db->where($where_date);
            // }
        // }

        $businessID = $this->session->userdata('businessunit_id');
        $this->db->select('a.*,b.generated_tenderid,b.generate_type,c.assign_to,fqam.fld_id as fqamountid');
        $this->db->from($this->table);
        $this->db->join($this->genertedid_table, 'a.fld_id = b.project_id', 'left');
        $this->db->join($this->projectmanager_table, 'a.fld_id = c.project_id', 'left');
        //$this->db->join($this->users_table, 'd.fld_id = c.assign_to', 'left');
        //Code For FQ Amount Details Code By Asheesh 12-02-2019.
        $this->db->join("fqamount_details as fqam", "a.fld_id = fqam.project_id_bd", 'LEFT');
        $this->db->join($this->scope_table, "a.fld_id = e.project_id", "left");
        $this->db->where('e.visible_scope', 'To_Go_project');
        $this->db->where('e.businessunit_id', $businessID);
        $this->db->where('b.generate_type', 'FQ');
        // $this->db->where('a.is_active', '1');
        // $this->db->where('a.visible_scope', 'To_Go_project');
        $i = 0;

        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        //$this->db->from($this->table);
        // return $this->db->count_all_results();
        return '0';
    }

}
