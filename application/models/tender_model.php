<?php

Class Tender_model extends CI_Model {

     function __construct() {
        // Call the Model constructor
        parent::__construct();
        //$this->load->database();
        $this->db1 = $this->load->database('another_db', TRUE);
        $this->db2 = $this->load->database('online', TRUE);
    }

    // function tenderDetailAdd($table, $data) {
    function insertRecord($table, $data) {
       
        $add = $this->db->insert($table, $data);
         
        if ($add) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function tender_list($table) {
        $this->db->select('*');
        $this->db->from($table);
        //$this -> db -> where('bd_deadlinedate>="'.date("Y-m-d").'"');
//        $this -> db -> where($where);

        $query = $this->db->get();
        return $query->result();
    }

	function tender($table, $id) {
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where('is_active', '1');
			$this->db->where('id', $id);

			$query = $this->db->get();
			return $query->result();
		}
		
	function selectRecord($table, $select, $where) {
        $this->db->select($select);
        $this->db->from($table);
        $this->db->where($where);
        // $this->db->where('id', $id);

        return $this->db->get();
        // return $query->result();
    }

    function check_unique_user_name($tender_name) {
        //echo "rtrt"; echo($table."=>".$user_name); die;
        $this->db->select('bd_TENDERdetails');
        $this->db->from('bd_tm_tenderdetails');
        $this->db->where('bd_TENDERdetails', $tender_name);

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /* function tenderUpdateById($id){
      $this -> db -> select('*');
      $this -> db -> from('bd_sector_master');
      $this -> db -> where('is_active', '1');
      $this -> db -> where('id', $id);

      $query = $this -> db -> get();
      return $query->result();
      } */

    function tenderUpdateById($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('bd_tm_tenderdetails', $data);
        return true;
    }

    function deleteById($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('bd_tm_tenderdetails', $data);
        return true;
    }

    public function insertTndr($arrRec) {

        $data = $this->insertTndrArr($arrRec);

        $resp = $this->db->insert('bd_tm_tenderdetails', $data);
        $last_id = $this->db->insert_id();
//        $last_id = '3312';
        $tndr = $this->db->select('*')
                ->where('id', $last_id)
                ->get('bd_tm_tenderdetails');
        $res = $tndr->result();
//        echo '<pre>';
//        print_r($res); die;
        $dtl = $res['0']->bd_TENDERdetails;
//        echo $dtl;
        $keyword = $this->db->select('*')
                ->get('bd_subsector_master');
        $rslt = $keyword->result();
        $dataArr = array('keywrd', 'sector');

        foreach ($rslt as $subsec) {
            $key = $subsec->subsectname;
            if (strpos($dtl, $key) !== false) {
                $dataArr['keywrd'][] = $subsec->id;
                if (!in_array($subsec->sectorid, $dataArr['sector'])) {
                    $dataArr['sector'][] = $subsec->sectorid;
                }
            }
        }
//        echo '<pre>'; print_r($dataArr); 
        $key = implode(',', $dataArr['keywrd']);
        $sec = implode(',', $dataArr['sector']);
//         echo $key."///".$sec; 
//         die;
        $data = array(
            'bd_sector_dependency_key' => $sec,
            'bd_keyword_dependency_01' => $key
        );
        $this->db->where('id', $last_id);
        $this->db->update('bd_tm_tenderdetails', $data);
        return $resp;
    }

    public function insertTndrArr($arrRec) {
        $InsertArr = array(
            'emp_id' => $arrRec['empid'],
            'bd_TENDERdetails' => $arrRec['tendername'],
            'bd_deadlinedate' => $arrRec['deadline'],
            'link_url' => $arrRec['linkurl'],
            'bd_csv_tndrvalue' => $arrRec['tendervalue'],
            'bd_location' => $arrRec['location'],
            'bd_organization' => $arrRec['organisation']
        );
        return $InsertArr;
    }

    public function chkTenderIdExistance($tndrId) {
        $this->db->select('fld_id');
        $this->db->from('bd_TENDERdetails');
        $this->db->where('tender24_ID', $tndrId);
        $this->db->where('is_active', '1');
        $this->db->limit(1);

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    
    public function alltenderidArr() {
        $this->db->select('tender24_ID');
        $this->db->from('bd_tenderdetail');
        $this->db->where('is_active', '1');
        $this->db->where('tender24_ID!=',"");
        $result = $this->db->get();

        $resQuery = array();
        if ($result->num_rows() > 0):
            foreach ($result->result() as $rec_row):
                array_push($resQuery, $rec_row->tender24_ID);
            endforeach;
            return $resQuery;
        else:
            return $resQuery;
        endif;
    }

    function updateRecord($table, $data, $where) {
        $this->db->where($where);
        $this->db->update($table, $data);
        //$result = $this->db->get()->result;
        
       
			
        return TRUE;
    }
    
    /* function keyword_list(){
      $this->db->select('km.name as key,km.id as kid,sm.name as sec');
      $this->db->from('bd_keyword_master as km');
      $this->db->join('bd_sector_master as sm', 'km.sector_id = sm.id', 'left');
      //$this->db->where('km.is_active=1');
      //$this->db->where('sm.is_active=1');
      $query = $this->db->get();
      //echo $this->db->last_query(); die;
      return $query->result();
      } */
}
