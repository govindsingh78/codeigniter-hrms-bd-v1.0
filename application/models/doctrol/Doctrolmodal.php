<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Doctrolmodal extends CI_Model {

    var $table = 'project_coordinator as a';
    var $column_order = array(null, 'a.emp_id', ); //set column field database for datatable orderable
    var $column_search = array('a.emp_id'); //set column field database for datatable searchable 
   // var $order = array("a.fld_id" => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();

        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    private function _get_datatables_query() {

        $db1 = $this->db1->database; //BD
        $db2 = $this->db2->database; //HRMS
        $projcoordid = $this->session->userdata('uid');
        $this->db->select("$db2.tm_projects.id,$db2.tm_projects.project_name,$db1.project_coordinator.bd_project_id");
        $this->db->from("$db1.project_coordinator");
        //$this->db->join("$db2.main_employees_summary", "$db2.main_employees_summary.user_id=$db1.project_coordinator.emp_id", 'left');
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id = $db1.project_coordinator.hrms_projid", 'left');
        $this->db->where("$db1.project_coordinator.emp_id", $projcoordid);
        $this->db->group_by("$db1.project_coordinator.hrms_projid");
        //$this->db->select('a.project_id,c.TenderDetails,b.project_numberid,pc.emp_id as emp_idcoodg,eng.emp_id as engempid, e.pbg_amount,e.pbg_number,e.pbg_start,e.pbg_validity,e.pbg_bank,e.abg_amount,e.abg_number,e.abg_start,e.abg_validity,e.abg_bank,e.pbg,e.abg');
        //$this->db->from($this->table);
       // $this->db->join($this->table_datail, 'a.project_id = c.fld_id', 'left');
       // $this->db->join($this->table_summery, 'a.project_id = b.project_id', 'left');
	//$this->db->join("$db2.tm_projects", "b.project_numberid = $db2.tm_projects.id", 'inner');
      //  $this->db->join($this->table_bdproject_status, 'a.project_id = d.project_id', 'left');
       // $this->db->join($this->table_bd_details, 'a.project_id = e.bd_projid', 'inner');
       // $this->db->join("project_coordinator as pc", 'a.project_id = pc.bd_project_id', 'left');
       // $this->db->join("project_assign_engnr as eng", 'a.project_id = eng.bd_project_id', 'left');
       // $this->db->where("b.project_numberid!=", "");
       // $this->db->where("($db2.tm_projects.project_category='ie' OR $db2.tm_projects.project_category='ae' OR $db2.tm_projects.project_category='pmc')", NULL, FALSE);
	//$this->db->group_by('a.project_id');

        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
         $this->db->from($this->table);
         return $this->db->count_all_results();

    }

}
