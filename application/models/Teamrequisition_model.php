<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Teamrequisition_model extends CI_Model {

    var $table = 'assign_project_manager as a';
    var $tenderdetails_table = 'bd_tenderdetail as b';
    var $togo_table = 'teamreq_togo_project as c';
    var $genertedid_table = 'tender_generated_id as d';
    var $scope_table = 'bdtender_scope as f';
    //var $users_table = 'main_users as e';
    var $column_order = array(null, 'b.TenderDetails', 'b.visible_scope', 'b.Location', 'b.Organization', 'b.keyword_phase'); //set column field database for datatable orderable
    var $column_search = array('d.generated_tenderid', 'b.keyword_phase', 'b.TenderDetails', 'b.Location', 'b.Organization'); //set column field database for datatable searchable 
    var $order = array('a.fld_id' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {
        if ($this->input->post('project_name')) {
            $this->db->where('d.generate_type', $this->input->post('project_name'));
        }
        if ($this->input->post('sectorinput')) {
            $this->db->like('b.Sector_IDs', $this->input->post('sectorinput'));
        }
        //Filter Code By Asheesh..
        if ($this->input->post('tndr_status')) {
            if ($this->input->post('tndr_status') == '3'):
                $this->db->like('a.proj_status', '3');
            else:
                $this->db->like('f.visible_scope', $this->input->post('tndr_status'));
            endif;
        }

        if ($this->input->post('national_intern') == 1 || $this->input->post('national_intern') == 2 || $this->input->post('national_intern') == 3) {
          $this->db->like('national_intern', $this->input->post('national_intern'));
        }

        $bdRole = $this->Front_model->bd_rolecheck();
        $pmid = $this->session->userdata('uid');
        $businessID = $this->session->userdata('businessunit_id');
        /*  if ($bdRole == 1) {
          $this->db->select('a.assign_to,a.assign_by,a.proj_status,a.filled_by,a.is_active,b.*,d.generated_tenderid');
          $this->db->from($this->table);
          $this->db->join($this->tenderdetails_table, 'a.project_id = b.fld_id', 'left');
          $this->db->join($this->scope_table, 'a.project_id = f.project_id', 'left');
          $this->db->join($this->togo_table, 'a.project_id = c.project_id', 'left');
          $this->db->join($this->genertedid_table, 'a.project_id = d.project_id', 'left');
          //$this->db->join($this->users_table, 'a.assign_to = e.fld_id', 'left');
          $this->db->where('c.is_active', '1');
          // $this->db->where('a.assign_to', $pmid);
          } else { */
        $this->db->select('a.assign_to,a.assign_by,a.proj_status,a.filled_by,a.is_active,b.*,d.generated_tenderid');
        $this->db->from($this->table);
        $this->db->join($this->tenderdetails_table, 'a.project_id = b.fld_id', 'left');
        $this->db->join($this->scope_table, 'a.project_id = f.project_id', 'left');
        $this->db->join($this->togo_table, 'a.project_id = c.project_id', 'left');
        $this->db->join($this->genertedid_table, 'a.project_id = d.project_id', 'left');
        //$this->db->join($this->users_table, 'a.assign_to = e.fld_id', 'left');
        $this->db->where('f.businessunit_id', $businessID);
        $this->db->where('c.is_active', '1');
        $this->db->where('b.fld_id is NOT NULL', NULL, FALSE);
        //}
        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function _get_datatables_query_new() {
        $bdRole = $this->Front_model->bd_rolecheck();
        $pmid = $this->session->userdata('uid');

        $this->db->select('a.*,d.generate_type');
        $this->db->from($this->table);
        $this->db->join($this->tenderdetails_table, 'a.project_id = b.fld_id', 'left');
        $this->db->join($this->togo_table, 'a.project_id = c.project_id', 'left');
        $this->db->join($this->genertedid_table, 'a.project_id = d.project_id', 'left');
        //$this->db->join($this->users_table, 'a.assign_to = e.fld_id', 'left');
        //$this->db->where('a.is_active', '1');
        $where2 = '(generate_type="E" or generate_type = "FQ")';
        $this->db->where($where2);
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        
        return $query->result();
        
    }

    function get_datatables_new() {
        $this->_get_datatables_query_new();
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        //$this->db->from($this->table);
        //return $this->db->count_all_results();
        return '0';
    }

    public function bd_rolecheck() {
        $pmid = $this->session->userdata('uid');
        $query = $this->db->query('select role_id from bd_role where user_id= ' . $pmid);
        $result = $query->result();
        if (!empty($result)) {
            return $result[0]->role_id;
        } else {
            return false;
        }
    }

}
