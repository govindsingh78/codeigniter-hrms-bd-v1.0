<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Projectplanning_model extends CI_Model {

    var $table = 'tm_projects as a';
    var $table_client = 'tm_clients as b';
    var $table_project_detail = 'bdcegexp_proj_summery as c';

    // var $column_order = array(null, 'project_name', 'service', 'client', 'start_year', 'funding', 'sector', 'state'); //set column field database for datatable orderable
    //var $column_search = array("$db2.tm_projects.project_name","$db2.tm_clients.client_name"); //set column field database for datatable searchable 
    //var $order = array('a.id' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->db1 = $this->load->database('online', TRUE);
        //$db1 = $this->db;
        //Load another database
        $this->db2 = $this->load->database('another_db', TRUE);
        //$db2 = $this->db2;
    }

    private function _get_datatables_query($recArr) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($this->input->post('projectinput')) {
            $this->db->where("$db2.tm_projects.id", $this->input->post('projectinput'));
        }
        if ($this->input->post('max_mm')) {
            $mm = $this->input->post('max_mm');
            $where = "($db1.project_detail.total_mm <='$mm')";
            $this->db->where($where);
        }
        $this->db->select("$db2.tm_projects.project_name,$db2.tm_projects.start_date,$db2.tm_projects.end_date,$db2.tm_projects.project_category,$db2.tm_projects.totalmm,$db2.tm_clients.client_name,$db1.bdcegexp_proj_summery.project_numberid,$db1.bdcegexp_proj_summery.project_id");
        $this->db->from("$db2.tm_projects");
        $this->db->join("$db2.tm_clients", "$db2.tm_projects.client_id = $db2.tm_clients.id", 'left');
        $this->db->join("$db1.bdcegexp_proj_summery", "$db2.tm_projects.id = $db1.bdcegexp_proj_summery.project_numberid", 'right');
        $this->db->where("$db2.tm_projects.is_active", '1');
        $this->db->order_by("$db2.tm_projects.project_name", "ASC");

        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        if ($this->input->post()) {
            $recArr = $this->input->post();
        }
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        // $this->db->from($this->table);
        //  return $this->db->count_all_results();
        return '0';
    }

//    public function getuserDetailByexpid($id) {
//
//        $db1 = $this->db1->database;
//        $db2 = $this->db2->database;
//
//        $this->db->select("$db1.assign_finalteam.*,$db2.main_users.userfullname,$db1.designation_master_requisition.designation_name as finaldesignation_name,$db2.tm_projects.project_name,$db2.tm_project_employees.is_intermittent,$db1.saveeotteam.eot_mm");
//        $this->db->from("$db1.assign_finalteam");
//        $this->db->join("$db2.main_users", "$db1.assign_finalteam.empname = $db2.main_users.id", 'left');
//        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id = $db1.designation_master_requisition.fld_id", 'LEFT');
//        $this->db->join("$db1.bdcegexp_proj_summery", "$db1.assign_finalteam.project_id = $db1.bdcegexp_proj_summery.project_id", 'LEFT');
//        $this->db->join("$db2.tm_projects", "$db1.bdcegexp_proj_summery.project_numberid = $db2.tm_projects.id", 'LEFT');
//        $this->db->join("$db2.tm_project_employees", "($db1.assign_finalteam.empname = $db2.tm_project_employees.emp_id AND $db2.tm_projects.id=$db2.tm_project_employees.project_id)", 'LEFT');
//        $this->db->join("$db1.saveeotteam", "($db1.assign_finalteam.empname = $db1.saveeotteam.emp_id and $db1.saveeotteam.project_id=$db1.assign_finalteam.project_id)", 'LEFT');
//        // $this->db->join("$db1.invoicesave", "($db1.assign_finalteam.empname = $db1.invoicesave.emp_id and $db1.invoicesave.project_id=$db1.assign_finalteam.project_id)", 'LEFT');
//
//        $this->db->where("$db1.assign_finalteam.status", '1');
//        $this->db->where("$db1.assign_finalteam.project_id", $id);
//        $result = $this->db->get()->result_object();
//
//        $reTurmArr = array();
//        if ($result) {
//            foreach ($result as $rowR) {
//                $projID = $rowR->project_id;
//                $empId = $rowR->empname;
//                $rowR->totalcumulativemm = $this->getlatest_totalcumulativemm($projID, $empId);
//                $reTurmArr[] = $rowR;
//            }
//        }
//        return isset($reTurmArr) ? $reTurmArr : false;
//    }






    public function getProjectStartDate($id) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.tm_projects.start_date");
        $this->db->from("$db1.bdcegexp_proj_summery");
        $this->db->join("$db2.tm_projects", "$db1.bdcegexp_proj_summery.project_numberid = $db2.tm_projects.id", 'left');
        $this->db->where("$db2.tm_projects.is_active", '1');
        $this->db->where("$db1.bdcegexp_proj_summery.project_id", $id);
        $result = $this->db->get()->result_object();
        return isset($result) ? $result[0]->start_date : false;
    }

    public function getuserDetail($projectID, $userID) {
        $this->db->select("*");
        $this->db->from("finalteamdata");
        $this->db->where("project_id", $projectID);
        $this->db->where("emp_id", $userID);
        $result = $this->db->get()->result_object();
        return isset($result) ? $result : false;
    }

    //Code Edited By Asheesh 22-12-2018.
    public function getuserDetailByexpid($id) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
 

        $this->db->select("$db1.assign_finalteam.*,$db2.main_users.userfullname,$db1.designation_master_requisition.designation_name as finaldesignation_name,$db2.tm_projects.project_name,$db2.tm_project_employees.is_intermittent,$db1.saveeotteam.eot_mm");
        $this->db->from("$db1.assign_finalteam");
        $this->db->join("$db2.main_users", "$db1.assign_finalteam.empname = $db2.main_users.id", 'left');
        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id = $db1.designation_master_requisition.fld_id", 'LEFT');
        $this->db->join("$db1.bdcegexp_proj_summery", "$db1.assign_finalteam.project_id = $db1.bdcegexp_proj_summery.project_id", 'LEFT');
        $this->db->join("$db2.tm_projects", "$db1.bdcegexp_proj_summery.project_numberid = $db2.tm_projects.id", 'LEFT');
        $this->db->join("$db2.tm_project_employees", "($db1.assign_finalteam.empname = $db2.tm_project_employees.emp_id AND $db2.tm_projects.id=$db2.tm_project_employees.project_id)", 'LEFT');
        $this->db->join("$db1.saveeotteam", "($db1.assign_finalteam.empname = $db1.saveeotteam.emp_id and $db1.saveeotteam.project_id=$db1.assign_finalteam.project_id)", 'LEFT');
        // $this->db->join("$db1.invoicesave", "($db1.assign_finalteam.empname = $db1.invoicesave.emp_id and $db1.invoicesave.project_id=$db1.assign_finalteam.project_id)", 'LEFT');

        $this->db->where("$db1.assign_finalteam.status", '1');
        $this->db->where("$db1.assign_finalteam.project_id", $id);
        $result = $this->db->get()->result_object();

        $reTurmArr = array();
        if ($result) {
            foreach ($result as $rowR) {
                $projID = $rowR->project_id;
                $empId = $rowR->empname;
                $rowR->totalcumulativemm = $this->getlatest_totalcumulativemm($projID, $empId);
                $reTurmArr[] = $rowR;
            }
        }
        return isset($reTurmArr) ? $reTurmArr : false;
    }

    public function getuserDetailByexpidNew($id) {
        $db1 = $this->db2->database;
        $db2 = $this->db1->database;
 

        $this->db->select("$db1.assign_finalteam.*,$db2.main_users.userfullname,$db1.designation_master_requisition.designation_name as finaldesignation_name,$db2.tm_projects.project_name,$db2.tm_project_employees.is_intermittent,$db1.saveeotteam.eot_mm");
        $this->db->from("$db1.assign_finalteam");
        $this->db->join("$db2.main_users", "$db1.assign_finalteam.empname = $db2.main_users.id", 'left');
        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id = $db1.designation_master_requisition.fld_id", 'LEFT');
        $this->db->join("$db1.bdcegexp_proj_summery", "$db1.assign_finalteam.project_id = $db1.bdcegexp_proj_summery.project_id", 'LEFT');
        $this->db->join("$db2.tm_projects", "$db1.bdcegexp_proj_summery.project_numberid = $db2.tm_projects.id", 'LEFT');
        $this->db->join("$db2.tm_project_employees", "($db1.assign_finalteam.empname = $db2.tm_project_employees.emp_id AND $db2.tm_projects.id=$db2.tm_project_employees.project_id)", 'LEFT');
        $this->db->join("$db1.saveeotteam", "($db1.assign_finalteam.empname = $db1.saveeotteam.emp_id and $db1.saveeotteam.project_id=$db1.assign_finalteam.project_id)", 'LEFT');
        // $this->db->join("$db1.invoicesave", "($db1.assign_finalteam.empname = $db1.invoicesave.emp_id and $db1.invoicesave.project_id=$db1.assign_finalteam.project_id)", 'LEFT');

        $this->db->where("$db1.assign_finalteam.status", '1');
        $this->db->where("$db1.assign_finalteam.project_id", $id);
        $result = $this->db->get()->result_object();

        $reTurmArr = array();
        if ($result) {
            foreach ($result as $rowR) {
                $projID = $rowR->project_id;
                $empId = $rowR->empname;
                $rowR->totalcumulativemm = $this->getlatest_totalcumulativemm($projID, $empId);
                $reTurmArr[] = $rowR;
            }
        }
        return isset($reTurmArr) ? $reTurmArr : false;
    }

    //Get Latest Single Total Cumulative mm New Code By Asheesh 
    public function getlatest_totalcumulativemm($projID, $empId) {
        $db1 = $this->db1->database;
        $this->db->select("$db1.invoicesave.totalcumulativemm");
        $this->db->from("$db1.invoicesave");
        $this->db->where("$db1.invoicesave.project_id", $projID);
        $this->db->where("$db1.invoicesave.emp_id", $empId);
        $this->db->group_by("$db1.invoicesave.id", 'DESC');
        $result = $this->db->get()->row();
        return $result->totalcumulativemm;
    }

}
