<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pdfsort_model extends CI_Model {

    var $table = 'ceg_exp';
    var $column_order = array(null, 'project_name', 'service', 'client', 'start_year', 'funding', 'sector', 'state'); //set column field database for datatable orderable
    var $column_search = array('project_name', 'service', 'client', 'start_year', 'funding', 'sector', 'state'); //set column field database for datatable searchable 
    var $order = array('fld_id' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query($recArr) {

        if (!empty($this->input->post('service_cond')) == '1') {
            $service_cond = ' AND ';
        } elseif (!empty($this->input->post('service_cond')) == '2') {
            $service_cond = ' OR ';
        } else {
            $service_cond = '';
        }

        if (!empty($this->input->post('client_cond')) == '1') {
            $client_cond = ' AND ';
        } elseif (!empty($this->input->post('client_cond')) == '2') {
            $client_cond = ' OR ';
        } else {
            $client_cond = '';
        }

        if (!empty($this->input->post('year_cond')) == '1') {
            $year_cond = ' AND ';
        } elseif (!empty($this->input->post('year_cond')) == '2') {
            $year_cond = ' OR ';
        } else {
            $year_cond = '';
        }

        if (!empty($this->input->post('sector_cond')) == '1') {
            $sector_cond = ' AND ';
        } elseif (!empty($this->input->post('sector_cond')) == '2') {
            $sector_cond = ' OR ';
        } else {
            $sector_cond = '';
        }


        if (!empty($this->input->post('serviceinput'))) {
            $serviceinput = " service LIKE " . '"%' . $this->input->post('serviceinput') . '%"';
        } else {
            $serviceinput = '';
        }

        if (!empty($this->input->post('clientnput'))) {
            $clientnput = " client LIKE " . '"%' . $this->input->post('clientnput') . '%"';
        } else {
            $clientnput = '';
        }

        if (!empty($this->input->post('yearinput'))) {
            $yearinput = " start_year LIKE " . '"%' . $this->input->post('yearinput') . '%"';
        } else {
            $yearinput = '';
        }

        if (!empty($this->input->post('sectorinput'))) {
            $sectorinput = " sector LIKE " . '"%' . $this->input->post('sectorinput') . '%"';
        } else {
            $sectorinput = '';
        }

        if (!empty($this->input->post('stateinput'))) {
            $stateinput = " state LIKE " . '"%' . $this->input->post('stateinput') . '%"';
            //$this->db->where($where);
        } else {
            $stateinput = '';
        }
        if (!empty($service_cond) != '0' || !empty($client_cond) != '0' || !empty($year_cond) != '0' || !empty($sector_cond) != '0' ||
                !empty($serviceinput) || !empty($clientnput) || !empty($yearinput) || !empty($sectorinput) || !empty($stateinput)) {
            $wheres = $serviceinput . $service_cond . $clientnput . $client_cond . $yearinput . $year_cond . $sectorinput . $sector_cond . $stateinput;
            $this->db->where($wheres);
        }
        /* if(!empty($this->input->post('serviceinput'))){
          $serviceinput = " service LIKE ".'"%'.$this->input->post('serviceinput').'%"';
          $this->db->where($serviceinput);
          } */
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where(array('status' => '1', 'proj_status' => 'won'));

        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        if ($this->input->post()) {
            $recArr = $this->input->post();
        }
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        // $this->db->from($this->table);
        //  return $this->db->count_all_results();
        return '0';
    }

}
