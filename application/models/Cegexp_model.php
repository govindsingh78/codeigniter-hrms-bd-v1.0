<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cegexp_model extends CI_Model {

    var $table = 'bdceg_exp as a';
    var $table_summery = 'bdcegexp_proj_summery as b';
    var $table_datail = 'bd_tenderdetail as c';
    var $table_sector = 'sector as d';
    var $table_states = 'states as e';
    var $table_bdproject_status = 'bdproject_status as f';
    var $table_main_services = 'main_services as g';
    var $table_project_description = 'project_description as h';
    var $column_order = array(null, 'c.TenderDetails', 'a.service', 'a.client', 'a.start_year', 'a.funding', 'a.sector', 'a.state'); //set column field database for datatable orderable
    var $column_search = array('c.TenderDetails', 'a.service', 'a.client', 'a.start_year', 'a.funding', 'a.sector', 'a.state'); //set column field database for datatable searchable 
    var $order = array('a.fld_id' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query($recArr) {

        if (!empty($this->input->post('date_cond')) == '1') {
            $date_cond = ' AND ';
        } elseif (!empty($this->input->post('date_cond')) == '2') {
            $date_cond = ' OR ';
        } else {
            $date_cond = '';
        }

        if (!empty($this->input->post('service_cond')) == '1') {
            $service_cond = ' AND ';
        } elseif (!empty($this->input->post('service_cond')) == '2') {
            $service_cond = ' OR ';
        } else {
            $service_cond = '';
        }

        if (!empty($this->input->post('client_cond')) == '1') {
            $client_cond = ' AND ';
        } elseif (!empty($this->input->post('client_cond')) == '2') {
            $client_cond = ' OR ';
        } else {
            $client_cond = '';
        }

        if (!empty($this->input->post('year_cond')) == '1') {
            $year_cond = ' AND ';
        } elseif (!empty($this->input->post('year_cond')) == '2') {
            $year_cond = ' OR ';
        } else {
            $year_cond = '';
        }

        if (!empty($this->input->post('sector_cond')) == '1') {
            $sector_cond = ' AND ';
        } elseif (!empty($this->input->post('sector_cond')) == '2') {
            $sector_cond = ' OR ';
        } else {
            $sector_cond = '';
        }

        if (!empty($this->input->post('state_cond')) == '1') {
            $state_cond = ' AND ';
        } elseif (!empty($this->input->post('state_cond')) == '2') {
            $state_cond = ' OR ';
        } else {
            $state_cond = '';
        }

        if (!empty($this->input->post('cost_cond')) == '1') {
            $cost_cond = ' AND ';
        } elseif (!empty($this->input->post('cost_cond')) == '2') {
            $cost_cond = ' OR ';
        } else {
            $cost_cond = '';
        }


        if ($this->input->post('start_dates') and ( $this->input->post('end_dates'))) {
            $start_date = $this->input->post('start_dates');
            $end_date = $this->input->post('end_dates');
            $dates = "(b.end_date>='$start_date' AND b.end_date <= '$end_date')";
        } else {
            $dates = '';
        }

        if (!empty($this->input->post('serviceinput'))) {
            $serviceinput = " g.id = " . $this->input->post('serviceinput');
            //$serviceinput = " g.service_name LIKE ".'"%'.$this->input->post('serviceinput').'%"';
        } else {
            $serviceinput = '';
        }

        if (!empty($this->input->post('clientnput'))) {
            $clientnput = " a.client LIKE " . '"%' . $this->input->post('clientnput') . '%"';
        } else {
            $clientnput = '';
        }

        if (!empty($this->input->post('yearinput'))) {
            $yearinput = " a.start_year LIKE " . '"%' . $this->input->post('yearinput') . '%"';
        } else {
            $yearinput = '';
        }

        if (!empty($this->input->post('sectorinput'))) {
            $sectorinput = " d.sectName LIKE " . '"%' . $this->input->post('sectorinput') . '%"';
        } else {
            $sectorinput = '';
        }

        if (!empty($this->input->post('stateinput'))) {
            $stateinput = " e.state_name LIKE " . '"%' . $this->input->post('stateinput') . '%"';
            //$this->db->where($where);
        } else {
            $stateinput = '';
        }

        if (!empty($this->input->post('projectcost'))) {
            $projectcost = " b.project_cost LIKE " . '"%' . $this->input->post('projectcost') . '%"';
        } else {
            $projectcost = '';
        }

        if (!empty($this->input->post('funding'))) {
            $funding = " a.funding LIKE " . '"%' . $this->input->post('funding') . '%"';
        } else {
            $funding = '';
        }

        if (!empty($state_cond) != '0' || !empty($cost_cond) != '0' || !empty($date_cond) != '0' || !empty($service_cond) != '0' || !empty($client_cond) != '0' || !empty($year_cond) != '0' || !empty($sector_cond) != '0' ||
                !empty($dates) || !empty($serviceinput) || !empty($clientnput) || !empty($yearinput) || !empty($sectorinput) || !empty($stateinput) || !empty($projectcost) || !empty($funding)) {
            $wheres = $dates . $date_cond . $serviceinput . $service_cond . $clientnput . $client_cond . $yearinput . $year_cond . $sectorinput . $sector_cond . $stateinput . $state_cond . $projectcost . $cost_cond . $funding;
            $this->db->where($wheres);
        }

        $this->db->select('a.project_id,c.TenderDetails,g.service_name,a.client,a.start_year,d.sectName,e.state_name,a.funding,b.project_numberid');
        $this->db->from($this->table);
        $this->db->join($this->table_datail, 'a.project_id = c.fld_id', 'left');
        $this->db->join($this->table_summery, 'a.project_id = b.project_id', 'left');
        $this->db->join($this->table_project_description, 'a.project_id = h.project_id', 'left');
        $this->db->join($this->table_main_services, 'g.id = h.service_id', 'left');
        $this->db->join($this->table_sector, 'h.sector_id = d.fld_id', 'left');
        $this->db->join($this->table_states, 'h.state_id = e.state_id', 'left');
        $this->db->join($this->table_bdproject_status, 'a.project_id = f.project_id', 'left');
        //$this->db->where(array('a.status' => '1', 'a.proj_status' => 'won'));
        //$this->db->select('a.*');
        // $this->db->from($this->table);
        // $this->db->join($this->table_summery ,'a.fld_id = b.cegexp_id','left');
        //$this->db->where(array('b.status' => '1'));
        $this->db->where(array('f.is_active' => '1', 'f.project_status' => '1'));
        $this->db->group_by('c.fld_id');
        
        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        if ($this->input->post()) {
            $recArr = $this->input->post();
        }
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        // $this->db->from($this->table);
        //  return $this->db->count_all_results();
        return '0';
    }

}
