<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manmonth_model extends CI_Model {

    var $table = 'tm_projectdata as a';
    var $bid_table = 'bid_project as b';
    //var $summery_table = 'main_employees_summary as b';
    //var $salery_table = 'main_empsalarydetails as c';
    //var $leave_table = 'main_employeeleaves as d';
    //var $education_table = 'main_empeducationdetails as e'; 
    //var $address_table = 'main_empcommunicationdetails as f'; 

    var $column_order = array(null, 'a.project_name', 'a.start_date', 'a.end_date', 'a.total_mm', 'a.construction_mm', 'a.development_mm', 'a.om_mm', 'a.client', 'a.services', 'a.project_code', 'b.project_id'); //set column field database for datatable orderable
    var $column_search = array('a.project_name', 'a.start_date', 'a.end_date', 'a.total_mm', 'a.construction_mm', 'a.development_mm', 'a.om_mm', 'a.client', 'a.services', 'a.project_code', 'b.project_id'); //set column field database for datatable searchable 
    var $order = array('id' => 'asc'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {


        if ($this->input->post('project_code')) {
            $this->db->where('a.project_code', $this->input->post('project_code'));
        }
        if ($this->input->post('project_name')) {
            $this->db->like('a.project_name', $this->input->post('project_name'));
        }
        if ($this->input->post('total_mm')) {
            $this->db->like('a.total_mm', $this->input->post('total_mm'));
        }


        //$this->db->from($this->table);
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join($this->bid_table, 'a.id = b.tmid', 'left');

        $i = 0;

        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_list_businessunit() {
        $this->db->select('*');
        $this->db->from('main_businessunits');
        $this->db->where('isactive', '1');
        $this->db->order_by('id', 'asc');
        $query = $this->db->get();
        $result = $query->result();
        $businessunit = array();
        foreach ($result as $row) {
            $businessunit[] = $row->unitname;
        }
        return $businessunit;
    }

    public function get_list_companyname() {
        $this->db->select('*');
        $this->db->from('tbl_companyname');
        $this->db->where('is_active', '1');
        $this->db->order_by('id', 'asc');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
        /* $arr = array('id'=>'1','is_active'=>'1');
          $this->db->select('*');
          $this->db->from('tbl_companyname');
          $this->db->where($arr);
          $this->db->order_by('id','asc');
          $query = $this->db->get();
          $result = $query->result();
          echo $result[0]->company_name; */
    }

    public function get_list_educationcode() {
        $this->db->select('*');
        $this->db->from('main_educationlevelcode');
        $this->db->where('isactive', '1');
        $this->db->order_by('id', 'asc');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function get_list_bycompanyid($id) {
        $arr = array('id' => $id, 'is_active' => '1');
        $this->db->select('*');
        $this->db->from('tbl_companyname');
        $this->db->where($arr);
        $this->db->order_by('id', 'asc');
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->company_name;
    }

}
