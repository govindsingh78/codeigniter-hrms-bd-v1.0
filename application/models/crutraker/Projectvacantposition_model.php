<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Projectvacantposition_model extends CI_Model {

    var $table_summary = 'bdcegexp_proj_summery as a';
    var $table_project = 'tm_projects as b';
    var $table_team = 'assign_finalteam as c';
    var $table_position = 'designation_master_requisition as d';
    var $table_vacant_date = 'invc_vacant_resign_date as e';
    var $table_vacant_master = 'invc_vacant_typemaster as f';
    var $table_status = 'bdcruvacant_status as g';

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    private function _get_datatables_query() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.bdcruvacant_crupanel.client_cvappr_yesorno,$db1.bdcruvacant_crupanel.final_cvappr_yesorno,$db1.bdcruvacant_crupanel.step_by_cru,$db1.bdcruvacant_crupanel.curr_status,$db1.invc_vacant_resign_date.designation_id,$db1.invc_vacant_resign_date.last_date_resign,$db1.bdcegexp_proj_summery.project_numberid,$db1.bdcegexp_proj_summery.project_id,$db2.tm_projects.project_name,$db1.designation_master_requisition.designation_name");
        $this->db->from("$db1.bdcegexp_proj_summery");
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id = $db1.bdcegexp_proj_summery.project_numberid", 'inner');
        $this->db->join("$db1.assign_finalteam", "$db1.assign_finalteam.project_id = $db1.bdcegexp_proj_summery.project_id", 'inner');
        $this->db->join("$db1.designation_master_requisition", "$db1.designation_master_requisition.fld_id = $db1.assign_finalteam.designation_id", 'inner');
        $this->db->join("$db1.invc_vacant_resign_date", "$db1.invc_vacant_resign_date.designation_id = $db1.designation_master_requisition.fld_id AND $db1.invc_vacant_resign_date.project_id = $db1.bdcegexp_proj_summery.project_id", 'inner');
        $this->db->join("$db1.invc_vacant_typemaster", "$db1.invc_vacant_typemaster.fld_id = $db1.invc_vacant_resign_date.vacant_typeid", 'inner');
        $this->db->join("$db1.bdcruvacant_crupanel", "($db1.assign_finalteam.designation_id=$db1.bdcruvacant_crupanel.designation_id AND $db1.assign_finalteam.project_id=$db1.bdcruvacant_crupanel.project_id AND $db1.bdcruvacant_crupanel.status='1')", "LEFT");
        $this->db->where(array("$db1.bdcegexp_proj_summery.status" => '1'));
        // $this->db->order_by("$db1.bdcegexp_proj_summery.project_id", 'ASC');
        //$this->db->group_by("$db1.bdcegexp_proj_summery.project_id");
        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        $ResultRec = $query->result();
        return $ResultRec;
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table_summary);
        return $this->db->count_all_results();
    }

}

?>