<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Projectlist_model extends CI_Model {

    var $table_mean = 'invc_vacant_resign_date as a';
    var $column_order = array(null, "invc_vacant_typemaster.type_name", "designation_master_requisition.designation_name", "tm_projects.project_name");
    var $column_search = array("invc_vacant_typemaster.type_name", "designation_master_requisition.designation_name", "tm_projects.project_name"); //set column field database for datatable searchable 
    var $order = array("invc_vacant_typemaster.fld_id" => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    private function _get_datatables_query() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.invc_vacant_resign_date.if_proj_neworold,$db1.invc_vacant_resign_date.fld_id as resignid,$db1.invc_vacant_typemaster.type_name,$db1.invc_vacant_resign_date.project_id,$db1.invc_vacant_resign_date.designation_id,$db1.invc_vacant_resign_date.last_date_resign,$db2.tm_projects.project_name,$db1.designation_master_requisition.designation_name,$db1.bdcruvacant_crupanel.curr_status");
        $this->db->from("$db1.invc_vacant_resign_date");
        $this->db->join("$db1.accountinfo", "$db1.accountinfo.project_id = $db1.invc_vacant_resign_date.project_id", 'INNER');
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id=$db1.accountinfo.project_numberid", 'LEFT');
        $this->db->join("$db1.designation_master_requisition", "$db1.designation_master_requisition.fld_id=$db1.invc_vacant_resign_date.designation_id", 'LEFT');
        $this->db->join("$db1.invc_vacant_typemaster", "$db1.invc_vacant_typemaster.fld_id=$db1.invc_vacant_resign_date.vacant_typeid", 'LEFT');
        $this->db->join("$db1.bdcruvacant_crupanel", "($db1.bdcruvacant_crupanel.project_id=$db1.invc_vacant_resign_date.project_id AND $db1.bdcruvacant_crupanel.designation_id=$db1.invc_vacant_resign_date.designation_id AND $db1.bdcruvacant_crupanel.curr_status!='3' AND $db1.bdcruvacant_crupanel.curr_status!='8' AND $db1.bdcruvacant_crupanel.status='1' )", 'LEFT');

        $this->db->where(array("$db1.invc_vacant_resign_date.status" => '1'));
        $this->db->group_by("$db1.invc_vacant_resign_date.fld_id");

        if ($this->input->post('typer_ids')) {
            $this->db->where(array("$db1.invc_vacant_resign_date.vacant_typeid" => $this->input->post('typer_ids')));
        } else {
            $this->db->where(array("$db1.invc_vacant_resign_date.vacant_typeid" => "1"));
        }
        if ($this->input->post('hrms_projids')) {
            $this->db->where(array("$db1.accountinfo.project_numberid" => $this->input->post('hrms_projids')));
        }
        if ($this->input->post('curr_status')) {
            $this->db->where(array("$db1.bdcruvacant_crupanel.curr_status" => $this->input->post('curr_status')));
        }


        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        $ResultRec = $query->result();
        return $ResultRec;
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table_mean);
        return $this->db->count_all_results();
    }

}

?>