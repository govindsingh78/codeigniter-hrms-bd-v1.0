<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class shortListedEoi_model extends CI_Model {

    var $table = 'bd_tenderdetail as a';
    var $genertedid_table = 'tender_generated_id as b';
    var $projectmanager_table = 'assign_project_manager as c';
    //var $users_table = 'main_users as d';
    var $scope_table = 'bdtender_scope as e';
    var $movedeoi_table = 'bd_moved_eoi as f';
    var $column_order = array(null, 'a.TenderDetails', 'a.Location', 'a.Organization', 'a.keyword_phase'); //set column field database for datatable orderable
    var $column_search = array('b.generated_tenderid', 'a.keyword_phase', 'a.TenderDetails', 'a.Location', 'a.Organization'); //set column field database for datatable searchable 
    var $order = array('a.Created_Date' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
         $this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db = $this->load->database('another_db', TRUE); 
    }

    private function _get_datatables_query() {
        if ($this->input->post('project_name')) {
            $this->db->where('b.generate_type', $this->input->post('project_name'));
        }
        if ($this->input->post('sectorinput')) {
            $this->db->like('a.Sector_IDs', $this->input->post('sectorinput'));
        }

        if ($this->input->post('financial_year')) {
            $financial_year = $this->input->post('financial_year');
            $start_date = $financial_year . '-04-01';
            $end_date = ($financial_year + 1) . '-03-31';
            $where_date = "(a.Expiry_Date>='$start_date' AND a.Expiry_Date <= '$end_date')";
            $this->db->where($where_date);
        } 
        // else {
            // $checkdate = date("d-m-Y");
            // $current_years = date("Y");
            // $current_years_chk = '01-04-' . $current_years;

            // if ($current_years_chk <= $checkdate) {
                // $start_date = $current_years . '-04-01';
                // $end_date = ($current_years + 1) . '-03-31';
                // $where_date = "(a.Expiry_Date>='$start_date' AND a.Expiry_Date <= '$end_date')";
                // $this->db->where($where_date);
            // } else {
                // $start_date = ($current_years - 1) . '-04-01';
                // $end_date = $current_years . '-03-31';
                // $where_date = "(a.Expiry_Date>='$start_date' AND a.Expiry_Date <= '$end_date')";
                // $this->db->where($where_date);
            // }
        // }

        $businessID = $this->session->userdata('businessunit_id');




        $this->db->select('a.*,b.generated_tenderid,f.*');
        $this->db->from($this->table);
        $this->db->join($this->genertedid_table, 'a.fld_id = b.project_id', 'left');
        //$this->db->join($this->projectmanager_table, 'a.fld_id = c.project_id', 'right');
        $this->db->join($this->scope_table, "a.fld_id = e.project_id", "right");
        $this->db->join($this->movedeoi_table, "a.fld_id = f.new_project_id", "right");
        //$this->db->where('e.visible_scope', 'To_Go_project');
        $this->db->where('e.businessunit_id', $businessID);
        //$this->db->join($this->users_table, 'd.fld_id = c.assign_to', 'left');
        //$this->db->where('a.is_active', '1');
        //$this->db->where('a.visible_scope', 'To_Go_project');
        $i = 0;

        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();


        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();

        
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        //$this->db->from($this->table);
        // return $this->db->count_all_results();
        return '0';
    }

}
