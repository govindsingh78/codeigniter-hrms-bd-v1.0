<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notimportantproject_model extends CI_Model {

    var $table = 'bdtenderdetails_dump as a';
    var $scope_table = 'bdtender_scopedump as b';
    var $column_order = array(null, 'a.TenderDetails', 'a.Location', 'a.Organization', 'a.keyword_phase'); //set column field database for datatable orderable
    var $column_search = array('a.keyword_phase', 'a.TenderDetails', 'a.Location', 'a.Organization'); //set column field database for datatable searchable 
    var $order = array('a.fld_id' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {
        if ($this->input->post('project_name')) {
            $this->db->like('a.keyword_phase', $this->input->post('project_name'));
        }

        if ($this->input->post('sectorinput')) {
            $this->db->like('a.Sector_IDs', $this->input->post('sectorinput'));
        }

        $businessID = $this->session->userdata('businessunit_id');

        $this->db->select('a.*,b.project_id,b.visible_scope');
        $this->db->from($this->table);
        $this->db->join($this->scope_table, "a.parent_tbl_id = b.project_id", "left");
        $this->db->where('b.visible_scope', 'Not_Important_project');
        $this->db->where('b.businessunit_id', $businessID);

        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        //$this->db->from($this->table);
        //return $this->db->count_all_results();
        return '0';
    }

}
