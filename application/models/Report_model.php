<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_model extends CI_Model {

    public $table = 'ceg_exp as a';
    public $tablegenerated = 'tender_generated_id as b';
    public $column_order = array(null, 'a.project_name', 'a.project_code', 'a.proj_status'); //set column field database for datatable orderable
    public $column_search = array('a.client', 'a.service', 'a.sector', 'a.state', 'a.project_code', 'a.project_name', 'a.project_code', 'a.proj_status'); //set column field database for datatable searchable 
    public  $order = array('a.fld_id' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db = $this->load->database('another_db', TRUE); 
    }




    
    public function getProjectDetailByUserID($userid) {
        //24-Code By Asheesh.. last evening
        $this->db->select('a.project_id, b.designation_name,c.project_status,d.TenderDetails');
        $this->db->from('team_assign_member a');
        $this->db->join('designation_master_requisition b', 'a.designation_id=b.fld_id', 'left');
        $this->db->join('bdproject_status c', 'a.project_id=c.project_id', 'left');
        $this->db->join('bd_tenderdetail d', 'a.project_id=d.fld_id', 'left');
        // $where = "(a.empname = '$userid' OR a.empnameother = '$userid')";
        $this->db->where('a.empname', $userid);
        $this->db->where('a.status', '1');
        $this->db->where('d.is_active', '1');
        // empnameother
        $result = $this->db->get()->result_array();

        return isset($result) ? $result : false;
    }

    public function getProjectDetailByUserIDother($userid) {
        //24-Code By Asheesh.. last evening
        $this->db->select('a.project_id, b.designation_name,c.project_status,d.TenderDetails');
        $this->db->from('team_assign_member a');
        $this->db->join('designation_master_requisition b', 'a.designation_id=b.fld_id', 'left');
        $this->db->join('bdproject_status c', 'a.project_id=c.project_id', 'left');
        $this->db->join('bd_tenderdetail d', 'a.project_id=d.fld_id', 'left');
        // $where = "(a.empname = '$userid' OR a.empnameother = '$userid')";
        $this->db->where('a.empnameother', $userid);
        $this->db->where('a.status', '1');
        $this->db->where('d.is_active', '1');
        // empnameother
        $result = $this->db->get()->result_array();
        return isset($result) ? $result : false;
    }

    private function _get_datatables_query($recArr) {


        //$srvinpt = $recArr['serviceinput'].$recArr['service_cond'].$recArr['clientnput'].$recArr['client_cond'].$recArr['yearinput'].$recArr['year_cond'].$recArr['sectorinput'].$recArr['sector_cond'].$recArr['stateinput'];
        

        //print_r($recArr); die;


        if (isset($recArr['service_cond']) && $recArr['service_cond'] == '1') {
            $service_cond = ' AND ';
        } elseif (isset($recArr['service_cond']) && $recArr['service_cond'] == '2') {
            $service_cond = ' OR ';
        } else {
            $service_cond = '';
        }

        if (isset($recArr['client_cond']) && $recArr['client_cond'] == '1') {
            $client_cond = ' AND ';
        } elseif (isset($recArr['client_cond']) &&  $recArr['client_cond'] == '2') {
            $client_cond = ' OR ';
        } else {
            $client_cond = '';
        }

        /* if($recArr['year_cond'] == '1'){
          $year_cond = ' AND ';
          }elseif($recArr['year_cond'] == '2'){
          $year_cond = ' OR ';
          }else{
          $year_cond = '';
          } */

        if (isset($recArr['sector_cond']) && $recArr['sector_cond'] == '1') {
            $sector_cond = ' AND ';
        } elseif (isset($recArr['sector_cond']) && $recArr['sector_cond'] == '2') {
            $sector_cond = ' OR ';
        } else {
            $sector_cond = '';
        }


        if (!empty($recArr['serviceinput'])) {
            $serviceinput = " a.service LIKE " . '"%' . $recArr['serviceinput'] . '%"';
        } else {
            $serviceinput = '';
        }

        if (!empty($recArr['clientnput'])) {
            $clientnput = " a.client LIKE " . '"%' . $recArr['clientnput'] . '%"';
        } else {
            $clientnput = '';
        }

        /* if(!empty($recArr['yearinput'])){
          $yearinput = " a.start_year LIKE ".'"%'.$recArr['yearinput'].'%"';
          }else{
          $yearinput = '';
          } */

          

        if (!empty($recArr['sectorinput'])) {
            $sectorinput = " a.sector LIKE " . '"%' . $recArr['sectorinput'] . '%"';
        } else {
            $sectorinput = '';
        }

        if (!empty($_POST['stateinput'])) {
            $stateinput = " a.state LIKE " . '"%' . $_POST['stateinput'] . '%"';
            //$this->db->where($where);
        } else {
            $stateinput = '';
        }




        // print_r($this->db->error()); die;
        if (!empty($recArr['service_cond']) != '0' || !empty($recArr['client_cond']) != '0' || !empty($recArr['sector_cond']) != '0' ||
                !empty($recArr['serviceinput']) || !empty($recArr['clientnput']) || !empty($recArr['sectorinput']) || !empty($recArr['stateinput'])
        ) {
            $where = $serviceinput . $service_cond . $clientnput . $client_cond . $sectorinput . $sector_cond . $stateinput;
            $this->db->where($where);
        }

        
       
        $this->db->select('a.*,b.generate_type');
        $this->db->from($this->table);
        $this->db->join($this->tablegenerated, 'a.project_id = b.project_id', 'left');
        $wheres = "(a.proj_status ='won' OR a.proj_status = 'loose')";
        $wheredata = "(b.generate_type ='P' OR b.generate_type = 'FQ')";
        $this->db->where($wheres);
        $this->db->where($wheredata);
        $this->db->where('status', '1');
        //$this->db->where('visible_scope', 'Active_project');


        $i = 0;

        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {



        if ($this->input->post()) {
            $recArr = $this->input->post();
 
        }

        $this->_get_datatables_query($recArr);
        // print_r($this->db->last_query()); die;

        if ($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        //$this->db->from($this->table);
        // return $this->db->count_all_results();
        return '0';
    }

    public function GetprojectNameById($tndrID) {
        $selectp = $this->db->query("SELECT * FROM ceg_exp WHERE fld_id=$tndrID ");
        $query = $selectp->result();
        return $query;
    }

}
