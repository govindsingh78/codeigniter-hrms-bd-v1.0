<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//error_reporting(E_ALL);

class Educationalreport_model extends CI_Model {

    // var $table = 'main_employees_summary as a';
    // var $table_client = 'tm_clients as b';
    // var $table_project_detail = 'bdcegexp_proj_summery as c';
    // var $columnorder = array(null,"$db2.main_employees_summary.userfullname.userfullname"); 
    //  var $column_search = array("userfullname");
    // var $order = array("$db2.main_employees_summary.id" => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
        $this->db = $this->load->database('online', TRUE);
    }

    private function _get_datatables_query($recArr) {
        $db1 = $this->db2->database;
        $db2 = $this->db1->database;

        if ($this->input->post('education_level')) {
            $education_level = $this->input->post('education_level');
//            if ($education_level == "1") {
//                $this->db->where("$db2.edu1.educationlevel", $education_level);
//            }
//            if ($education_level == "2") {
//                $this->db->where("$db2.edu2.educationlevel", $education_level);
//            }
            if ($education_level == "3") {
                $this->db->where("$db2.edu3.educationlevel", $education_level);
            }
            if ($education_level == "4") {
                $this->db->where("$db2.edu4.educationlevel", $education_level);
            }
            if ($education_level == "5") {
                $this->db->where("$db2.edu5.educationlevel", $education_level);
            }
            if ($education_level == "6") {
                $this->db->where("$db2.edu6.educationlevel", $education_level);
            }
            if ($education_level == "7") {
                $this->db->where("$db2.edu7.educationlevel", $education_level);
            }
        }
        //Name Filter..
        if ($this->input->post('empl_name')) {
            $keyword = $this->input->post('empl_name');
            $this->db->where("$db2.main_employees_summary.userfullname LIKE '%$keyword%'");
        }
        //Course Filter..
        if ($this->input->post('specialization')) {
            $specialization = $this->input->post('specialization');
            $this->db->where("($db2.edu1.specialization='" . $specialization . "' OR $db2.edu2.specialization='" . $specialization . "' OR $db2.edu3.specialization='" . $specialization . "' OR $db2.edu4.specialization='" . $specialization . "' OR $db2.edu5.specialization='" . $specialization . "' OR $db2.edu6.specialization='" . $specialization . "' OR $db2.edu7.specialization='" . $specialization . "')", NULL, FALSE);
        }

        //B-Unit..
        if ($this->input->post('bussn_unit')) {
            $bussn_unit = $this->input->post('bussn_unit');
            $this->db->where(array("$db2.main_employees_summary.businessunit_id" => $bussn_unit));
        } else {
            $this->db->where(array("$db2.main_employees_summary.businessunit_id" => '1'));
        }

        //Tag Filter...
        if ($this->input->post('tagsrec')) {
            @$tags = $this->input->post('tagsrec');
            $tagsArr = explode(',', $tags);
            //  $this->db->where_in("$db2.edu1.course", $tagsArr);
            //  $this->db->or_where_in("$db2.edu2.course", $tagsArr);
            $this->db->where_in("$db2.edu3.course", $tagsArr);
            $this->db->or_where_in("$db2.edu4.course", $tagsArr);
            $this->db->or_where_in("$db2.edu5.course", $tagsArr);
            $this->db->or_where_in("$db2.edu6.course", $tagsArr);
            $this->db->or_where_in("$db2.edu7.course", $tagsArr);
            // $this->db->or_where_in("$db2.edu1.spc_location", $tagsArr);
            // $this->db->or_where_in("$db2.edu2.spc_location", $tagsArr);
            $this->db->or_where_in("$db2.edu3.spc_location", $tagsArr);
            $this->db->or_where_in("$db2.edu4.spc_location", $tagsArr);
            $this->db->or_where_in("$db2.edu5.spc_location", $tagsArr);
            $this->db->or_where_in("$db2.edu6.spc_location", $tagsArr);
            $this->db->or_where_in("$db2.edu7.spc_location", $tagsArr);
            //$this->db->or_where_in("$db2.edu1.institution_name", $tagsArr);
            // $this->db->or_where_in("$db2.edu2.institution_name", $tagsArr);
            $this->db->or_where_in("$db2.edu3.institution_name", $tagsArr);
            $this->db->or_where_in("$db2.edu4.institution_name", $tagsArr);
            $this->db->or_where_in("$db2.edu5.institution_name", $tagsArr);
            $this->db->or_where_in("$db2.edu6.institution_name", $tagsArr);
            $this->db->or_where_in("$db2.edu7.institution_name", $tagsArr);
            // $this->db->or_where_in("$db2.edu1.specialization", $tagsArr);
            // $this->db->or_where_in("$db2.edu2.specialization", $tagsArr);
            $this->db->or_where_in("$db2.edu3.specialization", $tagsArr);
            $this->db->or_where_in("$db2.edu4.specialization", $tagsArr);
            $this->db->or_where_in("$db2.edu5.specialization", $tagsArr);
            $this->db->or_where_in("$db2.edu6.specialization", $tagsArr);
            $this->db->or_where_in("$db2.edu7.specialization", $tagsArr);
            //$this->db->or_where_in("$db2.edu1.from_date", $tagsArr);
            //$this->db->or_where_in("$db2.edu2.from_date", $tagsArr);
            $this->db->or_where_in("$db2.edu3.from_date", $tagsArr);
            $this->db->or_where_in("$db2.edu4.from_date", $tagsArr);
            $this->db->or_where_in("$db2.edu5.from_date", $tagsArr);
            $this->db->or_where_in("$db2.edu6.from_date", $tagsArr);
            $this->db->or_where_in("$db2.edu7.from_date", $tagsArr);
        }

        $this->db->select("$db2.main_employees_summary.prefix_name,$db2.main_employees_summary.years_exp,$db2.main_employees_summary.selecteddate,$db2.main_employees_summary.userfullname,$db2.main_employees_summary.employeeId,$db2.main_employees_summary.position_name,$db2.main_employees_summary.department_name,$db2.main_employees_summary.user_id,$db2.elv3.educationlevelcode as educationlevelcode3,$db2.edu3.institution_name as institution_name3,$db2.edu3.course as course3,$db2.edu3.spc_location as spc_location3,$db2.edu3.specialization as specialization3,$db2.edu3.from_date as from_date3,$db2.edu3.percentage as percentage3,$db2.elv4.educationlevelcode as educationlevelcode4,$db2.edu4.institution_name as institution_name4,$db2.edu4.course as course4,$db2.edu4.spc_location as spc_location4,$db2.edu4.specialization as specialization4,$db2.edu4.from_date as from_date4,$db2.edu4.percentage as percentage4,$db2.elv5.educationlevelcode as educationlevelcode5,$db2.edu5.institution_name as institution_name5,$db2.edu5.course as course5,$db2.edu5.spc_location as spc_location5,$db2.edu5.specialization as specialization5,$db2.edu5.from_date as from_date5,$db2.edu5.percentage as percentage5,$db2.elv6.educationlevelcode as educationlevelcode6,$db2.edu6.institution_name as institution_name6,$db2.edu6.course as course6,$db2.edu6.spc_location as spc_location6,$db2.edu6.specialization as specialization6,$db2.edu6.from_date as from_date6,$db2.edu6.percentage as percentage6,$db2.elv7.educationlevelcode as educationlevelcode7,$db2.edu7.institution_name as institution_name7,$db2.edu7.course as course7,$db2.edu7.spc_location as spc_location7,$db2.edu7.specialization as specialization7,$db2.edu7.from_date as from_date7,$db2.edu7.percentage as percentage7");
        $this->db->from("$db2.main_employees_summary");
        $this->db->join("$db2.main_users", "$db2.main_employees_summary.user_id=$db2.main_users.id", "INNER");
        //Lv 1..
        // $this->db->join("$db2.main_empeducationdetails as edu1", "($db2.edu1.user_id = $db2.main_employees_summary.user_id AND $db2.edu1.isactive='1' AND $db2.edu1.educationlevel='1')", 'LEFT');
        // $this->db->join("$db2.main_educationlevelcode as elv1", "$db2.edu1.educationlevel=$db2.elv1.id", 'LEFT');
        //Lv 2..
        // $this->db->join("$db2.main_empeducationdetails as edu2", "($db2.edu2.user_id = $db2.main_employees_summary.user_id AND $db2.edu2.isactive='1' AND $db2.edu2.educationlevel='2')", 'LEFT');
        // $this->db->join("$db2.main_educationlevelcode as elv2", "$db2.edu2.educationlevel=$db2.elv2.id", 'LEFT');
        //Lv 3..
        $this->db->join("$db2.main_empeducationdetails as edu3", "($db2.edu3.user_id = $db2.main_employees_summary.user_id AND $db2.edu3.isactive='1' AND $db2.edu3.educationlevel='3')", 'LEFT');
        $this->db->join("$db2.main_educationlevelcode as elv3", "$db2.edu3.educationlevel=$db2.elv3.id", 'LEFT');
        //Lv 4..
        $this->db->join("$db2.main_empeducationdetails as edu4", "($db2.edu4.user_id = $db2.main_employees_summary.user_id AND $db2.edu4.isactive='1' AND $db2.edu4.educationlevel='4')", 'LEFT');
        $this->db->join("$db2.main_educationlevelcode as elv4", "$db2.edu4.educationlevel=$db2.elv4.id", 'LEFT');
        //Lv 5..
        $this->db->join("$db2.main_empeducationdetails as edu5", "($db2.edu5.user_id = $db2.main_employees_summary.user_id AND $db2.edu5.isactive='1' AND $db2.edu5.educationlevel='5')", 'LEFT');
        $this->db->join("$db2.main_educationlevelcode as elv5", "$db2.edu5.educationlevel=$db2.elv5.id", 'LEFT');
        //Lv 6..
        $this->db->join("$db2.main_empeducationdetails as edu6", "($db2.edu6.user_id = $db2.main_employees_summary.user_id AND $db2.edu6.isactive='1' AND $db2.edu6.educationlevel='6')", 'LEFT');
        $this->db->join("$db2.main_educationlevelcode as elv6", "$db2.edu6.educationlevel=$db2.elv6.id", 'LEFT');
        //Lv 7..
        $this->db->join("$db2.main_empeducationdetails as edu7", "($db2.edu7.user_id = $db2.main_employees_summary.user_id AND $db2.edu7.isactive='1' AND $db2.edu7.educationlevel='7')", 'LEFT');
        $this->db->join("$db2.main_educationlevelcode as elv7", "$db2.edu7.educationlevel=$db2.elv7.id", 'LEFT');
        $this->db->where(array("$db2.main_employees_summary.isactive" => '1'));
        $this->db->where(array("$db2.main_users.isactive" => '1')); 
        $this->db->order_by("$db2.main_employees_summary.id", "ASC");

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) { // first loop
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        if ($this->input->post()) {
            $recArr = $this->input->post();
        }
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $db1 = $this->db2->database;
        $db2 = $this->db1->database;
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $db1 = $this->db2->database;
        $db2 = $this->db1->database;
        $this->db->from("$db2.main_employees_summary");
        return $this->db->count_all_results();
    }

}
