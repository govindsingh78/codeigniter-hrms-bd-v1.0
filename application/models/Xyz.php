<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Xyz extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        //$this->load->database();
		$this->db1 = $this->load->database('online',TRUE);
		$this->db2 = $this->load->database('another_db',TRUE);
	}
	
	public function InsertMasterData($recArr, $tablename) {
        if ($recArr && $tablename):
            $this->db->insert($tablename, $recArr);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        else:
            return false;
        endif;
    }
	
}

?>