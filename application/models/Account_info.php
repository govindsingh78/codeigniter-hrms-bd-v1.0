<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Account_info extends CI_Model {

    var $table = 'accountinfo as a';
    var $state_table = 'states as b';
    var $city_table = 'cities as c';
    var $tenderdetail_table = 'bd_tenderdetail as d';
    //var $column_order = array(null, 'company_name', 'company_details', 'contact_no', 'email_id'); //set column field database for datatable orderable
    //var $column_search = array('company_name', 'company_details', 'contact_no', 'email_id'); //set column field database for datatable searchable 
    var $order = array('id' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        //$this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    private function _get_datatables_query() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        // $this->db->select('a.id,a.registered_address,a.pan_no,a.gstn_no,a.project_id,b.state_name,c.city_name,d.TenderDetails');
        // $this->db->from($this->table);
        // $this->db->join($this->state_table ,'a.registered_state = b.state_id','left');
        // $this->db->join($this->city_table ,'a.registered_city = c.city_id','left');
        // $this->db->join($this->tenderdetail_table ,'a.project_id = d.fld_id','left');


        $this->db->select("$db2.tm_projects.project_name,$db1.accountinfo.*");
        $this->db->from("$db1.accountinfo");
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id = $db1.accountinfo.project_numberid", 'right');
        $this->db->where("$db2.tm_projects.is_active", '1');



        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        //$this->db->from($this->table);
        // return $this->db->count_all_results();
        return '0';
    }

}
