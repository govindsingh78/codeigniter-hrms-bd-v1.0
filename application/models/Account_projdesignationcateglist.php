<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Account_projdesignationcateglist extends CI_Model {

    var $table = 'acc2_designcateg_assign as a';
    var $column_order = array(null, 'ts_projid');
    var $column_search = array('ts_projid');
    var $order = array('reim_assign_id' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        //$this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    private function _get_datatables_query() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db1.acc2_designcateg_assign.bd_projid,$db1.acc2_designcateg_assign.desigcat_id,$db1.acc2_designcateg_assign.ts_projid,$db2.tm_projects.project_name,$db1.designationcat_master.ktype_name");
        $this->db->from("$db1.acc2_designcateg_assign");
        $this->db->join("$db2.tm_projects", "$db1.acc2_designcateg_assign.ts_projid=$db2.tm_projects.id", "LEFT");
        $this->db->join("$db1.designationcat_master", "$db1.acc2_designcateg_assign.desigcat_id=$db1.designationcat_master.k_id", "LEFT");
        $this->db->where("$db1.acc2_designcateg_assign.status", '1');

        // Ts Project ID
        if ($this->input->post('bdprojectid')) {
            $this->db->like("$db1.acc2_designcateg_assign.ts_projid", $this->input->post('bdprojectid'));
        }
        //Designation Cotegory
        if ($this->input->post('projdesigncategory')) {
            $this->db->like("$db1.acc2_designcateg_assign.desigcat_id", $this->input->post('projdesigncategory'));
        }

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        $redArr = $query->result();
        $returnArr = Array();
        if ($redArr) {
            foreach ($redArr as $reCdrow) {
                $reCdrow->count_assignondesign = $this->count_desigonprojcateg($reCdrow->bd_projid, $reCdrow->desigcat_id);
                $returnArr[] = $reCdrow;
            }
        }
        return $returnArr;
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function count_desigonprojcateg($projID, $designatin_categid) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.assign_finalteam.id");
        $this->db->from("$db1.assign_finalteam");
        $this->db->where("$db1.assign_finalteam.project_id", $projID);
        $this->db->where("$db1.assign_finalteam.designatin_categid", $designatin_categid);
        $this->db->where("$db1.assign_finalteam.status", "1");
        
         return $this->db->count_all_results();
    }
    
    

}
