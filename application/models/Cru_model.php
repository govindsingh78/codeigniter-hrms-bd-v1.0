<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cru_model extends CI_Model {

    var $table = 'team_assign as a';
    var $projectmanager_table = 'bd_tenderdetail as b';
    var $genertedid_table = 'tender_generated_id as c';
    var $bdtender_scope_table = 'bdtender_scope as d';
    var $bdproject_status_table = 'bdproject_status as e';
    var $column_order = array(null, 'b.TenderDetails', 'b.Location', 'b.Organization', 'b.keyword_phase'); //set column field database for datatable orderable
    var $column_search = array('c.generated_tenderid', 'b.keyword_phase', 'b.TenderDetails', 'b.Location', 'b.Organization'); //set column field database for datatable searchable 
    var $order = array('b.fld_id' => 'ASC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {
        if ($this->input->post('project_name')) {
            $this->db->like('b.keyword_phase', $this->input->post('project_name'));
        }
        if ($this->input->post('status')) {
            $this->db->like('e.project_status', $this->input->post('status'));
        }
        if ($this->input->post('sectorinput')) {
            $this->db->like('b.Sector_IDs', $this->input->post('sectorinput'));
        }
        if ($this->input->post('pro_status')) {
            $this->db->like('d.visible_scope', $this->input->post('pro_status'));
        }
        if ($this->input->post('financial_year')) {
            $financial_year = $this->input->post('financial_year');
            $start_date = $financial_year . '-04-01';
            $end_date = ($financial_year + 1) . '-03-31';
            $where_date = "(c.entrydate>='$start_date' AND c.entrydate <= '$end_date')";
            $this->db->where($where_date);
        } else {
            $checkdate = date("d-m-Y");
            $current_years = date("Y");
            $current_years_chk = '01-04-' . $current_years;
            if ($current_years_chk <= $checkdate) {
                $start_date = $current_years . '-04-01';
                $end_date = ($current_years + 1) . '-03-31';
                $where_date = "(c.entrydate>='$start_date' AND c.entrydate <= '$end_date')";
                $this->db->where($where_date);
            } else {
                $start_date = ($current_years - 1) . '-04-01';
                $end_date = $current_years . '-03-31';
                $where_date = "(c.entrydate>='$start_date' AND c.entrydate <= '$end_date')";
                $this->db->where($where_date);
            }
        }

        $this->db->select('a.project_id,e.project_status,b.TenderDetails,b.Location,b.Organization,b.keyword_phase,c.generated_tenderid');
        $this->db->from($this->table);
        $this->db->join($this->projectmanager_table, 'a.project_id = b.fld_id', 'LEFT');
        $this->db->join($this->bdtender_scope_table, 'b.fld_id = d.project_id', 'LEFT');
        $this->db->join($this->genertedid_table, 'a.project_id = c.project_id', 'LEFT');
        $this->db->join($this->bdproject_status_table, 'a.project_id = e.project_id', 'LEFT');
        $this->db->where("d.businessunit_id", "1");
       // $this->db->where("e.project_status!=", "3");
        $this->db->group_by('a.project_id');
        $i = 0;

        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
        //return '0';
    }

    public function checkempexist($projectID) {
        $query = $this->db->query("SELECT count(`empname`) as emp_id , count(`empnameother`) as otheremp_id FROM `team_assign_member` WHERE `project_id`=" . $projectID);
        $result = $query->result();
        $count = 0;
        if ($result) {
            $count = $result[0]->emp_id + $result[0]->otheremp_id;
        }
        return $count;
    }

}
