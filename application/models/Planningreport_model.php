<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//error_reporting(E_ALL);

class Planningreport_model extends CI_Model {

    var $table = 'tm_projects as a';
    var $table_client = 'tm_clients as b';
    var $table_project_detail = 'bdcegexp_proj_summery as c';
	var $table_assign_finalteam = 'assign_finalteam as d';

    // var $column_order = array(null, 'project_name', 'service', 'client', 'start_year', 'funding', 'sector', 'state'); //set column field database for datatable orderable
    //var $column_search = array("$db2.tm_projects.project_name","$db2.tm_clients.client_name"); //set column field database for datatable searchable 
    //var $order = array('a.id' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
        $this->db = $this->load->database('online', TRUE);
    }

    private function _get_datatables_query($recArr) {
        $db1 = $this->db2->database;
        $db2 = $this->db1->database;

        if (!empty($this->input->post('emp_cond')) == '1') {
            $emp_cond = ' AND ';
        } elseif (!empty($this->input->post('emp_cond')) == '2') {
            $emp_cond = ' OR ';
        } else {
            $emp_cond = '';
        }

        if (!empty($this->input->post('email_cond')) == '1') {
            $email_cond = ' AND ';
        } elseif (!empty($this->input->post('email_cond')) == '2') {
            $email_cond = ' OR ';
        } else {
            $email_cond = '';
        }

        if (!empty($this->input->post('designation_cond')) == '1') {
            $designation_cond = ' AND ';
        } elseif (!empty($this->input->post('designation_cond')) == '2') {
            $designation_cond = ' OR ';
        } else {
            $designation_cond = '';
        }

        if (!empty($this->input->post('contact_cond')) == '1') {
            $contact_cond = ' AND ';
        } elseif (!empty($this->input->post('contact_cond')) == '2') {
            $contact_cond = ' OR ';
        } else {
            $contact_cond = '';
        }

        if (!empty($this->input->post('balance_cond')) == '1') {
            $balance_cond = ' AND ';
        } elseif (!empty($this->input->post('balance_cond')) == '2') {
            $balance_cond = ' OR ';
        } else {
            $balance_cond = '';
        }

        if (!empty($this->input->post('project_cond')) == '1') {
            $project_cond = ' AND ';
        } elseif (!empty($this->input->post('project_cond')) == '2') {
            $project_cond = ' OR ';
        } else {
            $project_cond = '';
        }



        if (!empty($this->input->post('emp_id'))) {
            $emp_ids = $this->input->post('emp_id');
            $emp_id = "($db1.assign_finalteam.empname ='$emp_ids')";
        } else {
            $emp_id = '';
        }

        if (!empty($this->input->post('email_id'))) {
            $email_id = "$db2.main_users.emailaddress LIKE " . '"%' . $this->input->post('email_id') . '%"';
        } else {
            $email_id = '';
        }

        if (!empty($this->input->post('designation_id'))) {
            $designation = $this->input->post('designation_id');
            $designationId = implode(",", $designation);
            $designation_id = " $db1.designation_master_requisition.designation_id IN(" . $designationId . ")";
        } else {
            $designation_id = '';
        }

        if (!empty($this->input->post('contact_no'))) {
            $contact_no = "$db2.main_users.contactnumber LIKE " . '"%' . $this->input->post('contact_no') . '%"';
        } else {
            $contact_no = '';
        }

        if ($this->input->post('max_mm')) {
            $mm = $this->input->post('max_mm');
            $max_mm = "($db1.assign_finalteam.invtotalmm <='$mm')";
			//$max_mm = "$db1.assign_finalteam.invtotalmm >= ". $this->input->post('max_mm');
        } else {
            $max_mm = '';
        }

        // if ($this->input->post('balance_mm')) {
            // $balance_mmm = $this->input->post('balance_mm');
			//$balance_mm = $this->db1->where("d.invtotalconsmm", $balance_mmm);
			//$balance_mm = "($db1.assign_finalteam.invtotalconsmm>='$balance_mmm')";
            //$balance_mm = $this->db->where($where_mm);
            // $balance_mm = "($db1.assign_finalteam.invtotalconsmm >='$balance_mm')";
			//$balance_mm = "$db1.assign_finalteam.invtotalconsmm <= " . $this->input->post('balance_mm');
        // } 
		// else {
            // $balance_mm = '';
        // }
		
	    if(!empty($this->input->post('balance_mm'))){
			
          $balance_mm = "$db1.assign_finalteam.invtotalconsmm <= ". $this->input->post('balance_mm');
          
		} else{		  
            $balance_mm = '';
        }

        if ($this->input->post('projectinput')) {
            $projectinput = "$db2.tm_projects.id = " . $this->input->post('projectinput');
        } else {
            $projectinput = '';
        }

        /*
          if($this->input->post('min_marks') or $this->input->post('max_marks')){
          if(!empty($this->input->post('min_marks'))){
          $minMarks = $this->input->post('min_marks');
          }else{
          $minMarks = 0;
          }

          if(!empty($this->input->post('max_marks'))){
          $maxMarks = $this->input->post('max_marks');
          }else{
          $maxMarks = 100;
          }

          $marks = "($db1.a.marks >= '$minMarks' AND $db1.a.marks <= '$maxMarks')";
          }else{
          $marks = '';
          }

          if(!empty($this->input->post('balance_mm'))){
          $balance_mm = " $db1.a.balancemm <= ". $this->input->post('balance_mm');
          }else{
          $balance_mm = '';
          } */



        if (!empty($emp_cond) != '0' || !empty($email_cond) != '0' || !empty($designation_cond) != '0' || !empty($contact_cond) != '0' || !empty($balance_cond) != '0' || !empty($project_cond) != '0' ||
                !empty($emp_id) || !empty($email_id) || !empty($designation_id) || !empty($contact_no) || !empty($max_mm) || !empty($balance_mm) || !empty($projectinput)
        ) {
            $wheres = $emp_id . $emp_cond . $email_id . $email_cond . $designation_id . $designation_cond . $contact_no . $max_mm . $balance_cond . $balance_mm . $project_cond . $projectinput;
            $this->db->where($wheres);
        }

        /* if($this->input->post('projectinput')){
          $this->db->where("$db2.tm_projects.id",$this->input->post('projectinput'));
          }
          if($this->input->post('max_mm')){
          $mm = $this->input->post('max_mm');
          $where = "($db1.assign_finalteam.man_months <='$mm')";
          $this->db->where($where);
          }

          if($this->input->post('balance_mm')){
          $balance_mm = $this->input->post('balance_mm');
          $where = "($db1.assign_finalteam.balancemm <='$balance_mm')";
          $this->db->where($where);
          }

          if($this->input->post('emp_id')){
          $emp_id = $this->input->post('emp_id');
          $where = "($db1.assign_finalteam.empname ='$emp_id')";
          $this->db->where($where);
          } */

        $this->db->select("$db1.assign_finalteam.project_id,$db1.assign_finalteam.inventrydate,$db1.assign_finalteam.invtotalconsmm,$db1.assign_finalteam.cumulative_pre_mm,$db1.assign_finalteam.invtotalmm,$db1.assign_finalteam.invtotalbalmm,$db2.tm_projects.project_name,$db1.assign_finalteam.man_months,$db1.assign_finalteam.marks,$db1.assign_finalteam.firm,$db1.assign_finalteam.balancemm,$db2.main_users.userfullname,$db2.main_users.emailaddress,$db2.main_users.contactnumber,$db1.designation_master_requisition.designation_name,$db2.main_users.id,$db1.assign_finalteam.designation_id");
        $this->db->from("$db2.tm_projects");
        //$this->db->join("$db2.tm_clients" ,"$db2.tm_projects.client_id = $db2.tm_clients.id",'left');
        $this->db->join("$db1.bdcegexp_proj_summery", "$db2.tm_projects.id = $db1.bdcegexp_proj_summery.project_numberid", 'left');
        $this->db->join("$db1.assign_finalteam", "$db1.bdcegexp_proj_summery.project_id = $db1.assign_finalteam.project_id", 'right');
        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id = $db1.designation_master_requisition.fld_id", "left");
        $this->db->join("$db2.main_users", "$db2.main_users.id = $db1.assign_finalteam.empname", 'right');
        $this->db->where("$db2.tm_projects.is_active", '1');

        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        if ($this->input->post()) {
            $recArr = $this->input->post();
        }
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        // $this->db->from($this->table);
        //  return $this->db->count_all_results();
        return '0';
    }

    public function getuserInfo($userID) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;
        if ($userID) {
            //$this->db->select("$db2.a.user_id,$db2.a.date_of_joining,$db2.a.date_of_leaving,$db2.a.emp_status_name,$db2.a.businessunit_name,$db2.a.position_name,$db2.a.years_exp,$db2.a.office_number,$db2.a.userfullname,$db2.a.emailaddress,$db2.a.contactnumber,$db2.a.isactive,$db2.b.institution_name,$db2.b.course,$db2.b.spc_location,$db2.b.specialization,$db2.b.from_date,$db2.b.to_date,$db2.b.percentage,$db2.c.educationlevelcode");
            $data = array();
            $this->db->select("$db2.a.*,$db2.b.dob,$db2.d.country_name,$db2.e.state_name,$db2.f.city_name");
            $this->db->from("$db2.main_employees_summary as a");
            $this->db->join("$db2.main_emppersonaldetails as b", "$db2.a.user_id = $db2.b.user_id", "left");
            $this->db->join("$db2.main_empcommunicationdetails as c", "$db2.a.user_id = $db2.c.user_id", "left");
            $this->db->join("$db2.tbl_countries as d", "$db2.c.perm_country = $db2.d.id", "left");
            $this->db->join("$db2.tbl_states as e", "$db2.c.perm_state = $db2.e.id", "left");
            $this->db->join("$db2.tbl_cities as f", "$db2.c.perm_city = $db2.f.id", "left");

            $this->db->where("$db2.a.user_id", $userID);
            $res = $this->db->get()->result_object();

            $this->db->select("$db2.b.institution_name,$db2.b.course,$db2.b.spc_location,$db2.b.specialization,$db2.b.from_date,$db2.b.to_date,$db2.b.percentage,$db2.c.educationlevelcode");
            $this->db->from("$db2.main_empeducationdetails as b");
            $this->db->join("$db2.main_educationlevelcode as c", "$db2.b.educationlevel = $db2.c.id", "left");
            $this->db->where("$db2.b.user_id", $userID);
            $this->db->where("$db2.b.isactive","1");
            $res1 = $this->db->get()->result_object();


            $this->db->select("$db2.a.*");
            $this->db->from("$db2.main_empexperiancedetails as a");
            $this->db->where("$db2.a.user_id", $userID);
            $this->db->order_by("$db2.a.id", "ASC");
            $res2 = $this->db->get()->result_object();


            $this->db->select("$db1.a.*,$db2.b.userfullname,$db2.b.profileimg");
            $this->db->from("$db1.user_comment as a");
            $this->db->join("$db2.main_employees_summary as b", "$db1.a.user_id = $db2.b.user_id", "left");
            $this->db->where("$db1.a.status", 1);
            $this->db->where("$db1.a.comment_id", $userID);
            $res3 = $this->db->get()->result_object();


            $this->db->select("$db2.a.on_project");
            $this->db->from("$db2.emp_otherofficial_data as a");
            $this->db->where("$db2.a.user_id", $userID);
            $res4 = $this->db->get()->result_object();

            $res5 = '';
            if ($res4[0]->on_project) {
                $projectArr = explode(',', $res4[0]->on_project);
                $this->db->select("$db1.b.project_id,$db2.a.project_name");
                $this->db->from("$db2.tm_projects as a");
                $this->db->join("$db1.bdcegexp_proj_summery as b", "$db2.a.id = $db1.b.project_numberid", "left");
                $this->db->where_in("$db2.a.id", $projectArr);
				//$this->db->group_by("$db2.a.id");
                $res5 = $this->db->get()->result_object();
            }


            $this->db->select("$db1.b.fld_id,$db1.a.conusmedmm,$db1.a.cumulative_pre_mm,$db1.a.invtotalmm,$db1.a.invtotalbalmm,$db1.a.designation_id,$db1.a.empname,$db1.a.man_months,$db1.a.balancemm,$db1.b.TenderDetails,$db1.c.visible_scope,$db1.d.project_status,$db1.f.designation_name");
            //$this->db->select("$db1.a.conusmedmm,$db1.a.balancemm,$db1.b.TenderDetails");
            $this->db->from("$db1.assign_finalteam as a");
            $this->db->join("$db1.bd_tenderdetail as b", "$db1.a.project_id = $db1.b.fld_id", "left");
            $this->db->join("$db1.bdtender_scope as c", "$db1.a.project_id = $db1.c.project_id", "left");
            $this->db->join("$db1.bdproject_status as d", "$db1.a.project_id = $db1.d.project_id", "left");
            $this->db->join("$db1.designation_master_requisition as f", "$db1.a.designation_id = $db1.f.fld_id", "left");

            $this->db->where("$db1.a.empname", $userID);
            $this->db->where("$db1.c.businessunit_id", 1);
			$this->db->where("$db1.d.project_status", 1);
            $res6 = $this->db->get()->result_object();



            $data['userdetail'] = $res;
            $data['education'] = $res1;
            $data['experience'] = $res2;
            $data['comment'] = $res3;
            $data['onproject'] = isset($res5) ? $res5 : '';
            $data['bdproject'] = isset($res6) ? $res6 : '';

            return ($data) ? $data : false;
        }
    }

}
