<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SecondDB_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
        $this->db2 = $this->load->database('another_db', TRUE);
        $this->db1 = $this->load->database('online', TRUE);
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;
    }

    //Working Function..
    public function GetLoginDetailsArr() {
        $this->db->select(array('fld_id', 'emprole', 'firstname', 'lastname', 'userfullname', 'emailaddress', 'contactnumber', 'employeeId', 'selecteddate', 'thumbcode'));
        $this->db->from("main_users");
        $this->db->where('fld_id', $this->session->userdata('uid'));
        $query = $this->db->get()->row();
        return $query;
    }

    // Coded By Jitendra
    //Working Function.. comment by asheesh
//    public function getReqRecordbyUser() {
//        $pmid = $this->session->userdata('uid');
//        $selectp = $this->db->query("SELECT apm.*,td.TenderDetails,togo.projectgenid,mu.userfullname FROM  assign_project_manager as apm left join bd_tenderdetails as td ON apm.project_id=td.fld_id left join proj_togo_for_user as togo ON apm.project_id=togo.project_id left join main_users as mu ON mu.fld_id=apm.assign_to  WHERE apm.assign_to=" . $pmid . " AND apm.is_active='1' ");
//        $query = $selectp->result();
//        if (empty($query)) {
//            $selectp1 = $this->db->query("SELECT apm.*,td.TenderDetails,td.visible_scope,togo.projectgenid,mu.userfullname FROM assign_project_manager as apm left join bd_tenderdetails as td ON apm.project_id=td.fld_id left join proj_togo_for_user as togo ON apm.project_id=togo.project_id left join main_users as mu ON mu.fld_id=apm.assign_to WHERE apm.is_active='1' ");
//            $query2 = $selectp1->result();
//        }
//        return $query2;
//    }
//    


    public function getReqRecordbyUser() {
        $pmid = $this->session->userdata('uid');
        $selectp = $this->db->query("SELECT apm.*,td.TenderDetails,togo.projectgenid,mu.userfullname FROM assign_project_manager as apm left join bd_tenderdetails as td ON apm.project_id=td.fld_id left join proj_togo_for_user as togo ON apm.project_id=togo.project_id left join main_users as mu ON mu.fld_id=apm.assign_to WHERE apm.assign_to=" . $pmid . " AND apm.is_active='1'");
        $query = $selectp->result();
        if (empty($query)) {
            $selectp1 = $this->db->query("SELECT apm.*,td.TenderDetails,togo.projectgenid,mu.userfullname FROM assign_project_manager as apm left join bd_tenderdetails as td ON apm.project_id=td.fld_id left join proj_togo_for_user as togo ON apm.project_id=togo.project_id left join main_users as mu ON mu.fld_id=apm.assign_to WHERE apm.is_active='1'");
            $query = $selectp1->result();
        }
        //        $selectp = $this->db->query("SELECT bp.*,td.TenderDetails,togo.projectgenid,pm.proj_status,mu.userfullname FROM bid_project as bp left join bd_tenderdetails as td ON bp.project_id=td.fld_id left join proj_togo_for_user as togo ON bp.project_id=togo.project_id left join assign_project_manager as pm ON pm.project_id=bp.project_id left join main_users as mu ON mu.fld_id=bp.proposal_manager WHERE pm.assign_to=".$pmid." AND bp.is_active='1'");
//        $selectd = $this->db->query("SELECT * FROM designation_master_requisition WHERE 1");
//        $query['desi'] = $selectd->result();
        return $query;
    }

    //Working Function..
    public function getAllReqRecord() {
        $selectp = $this->db->query("SELECT td.TenderDetails,tr.*,dmr.fld_id,dmr.designation_name FROM bd_tenderdetails as td, team_reqbypm as tr,designation_master_requisition as dmr WHERE tr.project_id=td.fld_id AND tr.designation_id=dmr.fld_id AND tr.req_status='1' AND tr.is_active='1'");
        $query = $selectp->result();
        return $query;
    }

    //Get Tender Details By ID.
    public function GettenderNameById($tndrID) {
        $selectp = $this->db->query("SELECT * FROM bd_tenderdetails WHERE is_active='1' AND fld_id=$tndrID ");
        $query = $selectp->result();
        return $query;
    }

    //Working Function..
    public function getReqRecordbyProj($projid) {
        $selectp = $this->db->query("SELECT td.TenderDetails,tr.*,dmr.fld_id,dmr.designation_name FROM bd_tenderdetails as td,team_reqbypm as tr,designation_master_requisition as dmr WHERE tr.project_id=td.fld_id AND tr.designation_id=dmr.fld_id AND tr.project_id='" . $projid . "' AND tr.is_active='1'");
        $query['req'] = $selectp->result();
        $selectt = $this->db->query("SELECT tp.*,tm.userfullname,tm.emailaddress,tm.contactnumber,tm.is_locked FROM team_members as tm,team_projectwise as tp WHERE tm.fld_id=tp.tmid AND tp.project_id=" . $projid . " AND tp.is_active='1'");
        $query['team'] = $selectt->result();
        return $query;
    }

    //Working Function..By Punit...
//    public function getcruReqDataByPid($pid) {
//        $selectp = $this->db->query("SELECT tp.*,tm.userfullname,tm.emailaddress,tm.contactnumber FROM team_members as tm,team_projectwise as tp WHERE tm.fld_id=tp.tmid AND tp.project_id=".$pid." AND tp.is_active='1'");
//        $query = $selectp->result();
//        return $query;
//    }
    //Working Function..By Punit...
    public function getReqDataByPid($pid, $uid) {
        $selectp = $this->db->query("SELECT * FROM team_reqbypm WHERE project_id=" . $pid . " AND proposal_mngr_id=" . $uid);
        $query = $selectp->result();
        return $query;
    }

    //Get Login UserId..
//    public function GetLoginUserIdAndRole() {
//        $this->db->select(array('fld_id', 'emprole'));
//        $this->db->from("main_users");
//        $this->db->where('fld_id', $this->session->userdata('uid'));
//        $query = $this->db->get()->row();
//        return $query;
//    }
    //Get User Login Dept..
    public function loginUserDept() {
        $this->db->select(array('dept_id'));
        $this->db->from("main_users");
        $this->db->where('fld_id', $this->session->userdata('uid'));
        $query = $this->db->get()->row();
        return $query->dept_id;
    }

    //User Full Name By ID..
    public function UserNameById($empId) {
        /* $catnm = $this->selectRecord('main_users', array('userfullname'), array('fld_id' => $empId));
          if ($catnm) {
          $userdata = $catnm->first_row();
          $res = $userdata->userfullname;
          return $res;
          } else {
          return false;
          } */
        $this->db2->select('userfullname');
        $this->db2->from('main_users');
        $this->db2->where("id", $empId);
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec[0]->userfullname;
        } else {
            return false;
        }
    }

    //User Name By Id Other ..
    public function UserOtherNameById($empId) {
        $catnm = $this->selectRecord('team_req_otheremp', array('emp_name'), array('fld_id' => $empId));
        if ($catnm) {
            $userdata = $catnm->first_row();
            $res = $userdata->emp_name;
            return $res;
        } else {
            return false;
        }
    }

    //Designation Name By Id
    public function DesignationNameById($userId) {
        $catnm = $this->selectRecord('designation_master_requisition', array('designation_name'), array('fld_id' => $userId));
        if ($catnm) {
            $userdata = $catnm->first_row();
            $res = $userdata->designation_name;
            return $res;
        } else {
            return false;
        }
    }

    //Team Req Row By Id.
    public function Team_Req_RowById($Rwid) {
        $this->db->select('*');
        $this->db->from("team_reqbypm");
        $this->db->where('id', $Rwid);
        $Recdata = $this->db->get()->first_row();
        return ($Recdata) ? $Recdata : false;
    }

    //Get User Login Role..
    public function loginUserRole() {
        $this->db->select(array('emprole'));
        $this->db->from("main_users");
        $this->db->where('fld_id', $this->session->userdata('uid'));
        $query = $this->db->get()->row();
        return $query->emprole;
    }

    public function checkpwdexistance($oldpass) {
        $adminrec = $this->selectRecord('tbl_admin', array('*'), array('fld_password' => $oldpass));
        if ($adminrec) {
            $res = TRUE;
        } else {
            $res = FALSE;
        }
        return $res;
    }

    public function check_existance($tablename, $fieldArray) {
        $record = $this->selectRecord($tablename, array('fld_id'), $fieldArray);
        if ($record) {
            $res = TRUE;
        } else {
            $res = FALSE;
        }
        return $res;
    }

    public function checkcategoryexisting($parentid, $categname) {
        $adminrec = $this->selectRecord('tbl_category', array('*'), array('parent' => $parentid, 'name' => $categname));
        if ($adminrec) {
            $res = TRUE;
        } else {
            $res = FALSE;
        }
        return $res;
    }

    public function GetAllUserRec() {
        $this->db2->select('*');
        $this->db2->from('main_users');
        $this->db2->where('isactive', '1');
        $this->db2->order_by("userfullname", "ASC");
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function team_req_otherempAll() {
        $this->db->select("*");
        $this->db->from("team_req_otheremp");
        $this->db->where('status', '1');
        $this->db->order_by("emp_name", "ASC");
        $query = $this->db->get()->result();
        return $query;
    }

    //Get All User
    public function GetAllBdUserRec() {
        $this->db->select("*");
        $this->db->from("main_users");
        $this->db->where('isactive', '1');
        $query = $this->db->get()->result();

        $userofBd = array('296' => '296', '287' => '287', '190' => '190', '260' => '260', '175' => '175', '346' => '346', '257' => '257', '256' => '256', '191' => '191', '259' => '259', '211' => '211');
        $returnArr = array();
        foreach ($query as $keyid => $bduser) {
            if (in_array($bduser->fld_id, $userofBd)):
                array_push($returnArr, $bduser);
            endif;
        }

        return $returnArr;
    }

    //Get All Prefix Rec,,,
    public function GetAllBdPrefixRec() {
        $this->db->select("*");
        $this->db->from("projectno_prefx");
        $this->db->where('is_active', '1');
        $query = $this->db->get()->result();
        return $query;
    }

    public function GetAllProduct() {
        $this->db->select("*");
        $this->db->from("tbl_products");
        $this->db->where('fld_visibility', '1');
        $this->db->order_by("fld_id", "DESC");
        $query = $this->db->get()->result();
        return $query;
    }

    public function chk_user_existance($username) {
        $adminrec = $this->selectRecord('tbl_user', array('user_id'), array('user_id' => $username));
        if ($adminrec) {
            $res = TRUE;
        } else {
            $res = FALSE;
        }
        return $res;
    }

    public function getCategoryName($catID) {
        $catnm = $this->selectRecord('tbl_category', array('name'), array('fld_id' => $catID));
        if ($catnm) {
            $categdata = $catnm->first_row();
            $res = $categdata->name;
            return $res;
        } else {
            $res = FALSE;
        }
        return $res;
    }

    //Get Active Project By User...
    public function GetActiveProjByUser() {
        $this->db->select('*');
        $this->db->from('active_project_byuser a');
        $this->db->join('bd_tenderdetails b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('b.is_active', '1');
        $this->db->where('b.visible_scope', 'Active_project');
        //$this->db->order_by('c.track_title', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //To Go Active Project,,,
    public function GetToGoProjByUser() {
        $this->db->select('a.*, b.*, c.assign_to, u.userfullname');
        $this->db->from('proj_togo_for_user a');
        $this->db->join('bd_tenderdetails b', 'b.fld_id=a.project_id', 'left');
        $this->db->join('assign_project_manager c', 'b.fld_id=c.project_id', 'left');
        $this->db->join('main_users u', 'u.fld_id=c.assign_to', 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('b.visible_scope', 'To_Go_project');
        //$this->db->order_by('c.track_title', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //No Go Project By User...
    public function GetNoGoProjByUser() {
        $this->db->select('*');
        $this->db->from('proj_nogo_for_user a');
        $this->db->join('bd_tenderdetails b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('b.is_active', '1');
        $this->db->where('b.visible_scope', 'No_Go_project');
        //$this->db->order_by('c.track_title', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Get Inactive By User..
    public function GetinActiveProjByUser() {
        $this->db->select('*');
        $this->db->from('trash_for_user a');
        $this->db->join('bd_tenderdetails b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('b.is_active', '1');
        $this->db->where('b.visible_scope', 'InActive_project');
        //$this->db->order_by('c.track_title', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Get All Important Project..
    public function GetAllImportantProjByID() {
        $this->db->select('*');
        $this->db->from('import_for_user a');
        $this->db->join('bd_tenderdetails b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('b.is_active', '1');
        $this->db->where('b.visible_scope', 'Important_project');
        //$this->db->order_by('c.track_title', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Get All Review Project By ID..
    public function GetAllReviewProjByID() {
        $this->db->select('*');
        $this->db->from('proj_review_for_user a');
        $this->db->join('bd_tenderdetails b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('b.visible_scope', 'In_Review_project');
        //$this->db->order_by('c.track_title', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Get All marked as NotImportant Project..
    public function GetAllNotImportantProjByID() {
        $this->db->select('*');
        $this->db->from('notimport_for_user a');
        $this->db->join('bd_tenderdetails b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('b.visible_scope', 'Not_Important_project');
        //$this->db->order_by('c.track_title', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //All Bid Projects..
    public function GetAllbidprojects() {
        $this->db->select('a.*, b.*, d.generated_tenderid, c.assign_to, u.userfullname');
        $this->db->from('bid_project a');
        $this->db->join('bd_tenderdetails b', 'b.fld_id=a.project_id', 'left');
        $this->db->join('tender_generated_id d', 'b.fld_id=d.project_id', 'left');
        $this->db->join('assign_project_manager c', 'b.fld_id=c.project_id', 'left');
        $this->db->join('main_users u', 'u.fld_id=c.assign_to', 'left');
        $this->db->where('b.is_active', '1');
        $this->db->where('b.visible_scope', 'Bid_project');
//        $this->db->where('c.project_id', 'Bid_project');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Latest To Do List For Dashboard...
    public function GetlatestToGoListDashboard() {
        $this->db->select('*');
        $this->db->from('proj_togo_for_user a');
        $this->db->join('bd_tenderdetails b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('b.visible_scope', 'To_Go_project');
        $this->db->limit(4, 0);
        $this->db->order_by("a.fld_id", "DESC");

        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Latest Important Project..
    public function GetlatestImportantListDashboard() {
        $this->db->select('*');
        $this->db->from('import_for_user a');
        $this->db->join('bd_tenderdetails b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('b.visible_scope', 'Important_project');
        $this->db->limit(4, 0);
        $this->db->order_by("a.fld_id", "DESC");

        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Active Project,,,
    public function GetlatestActiveListDashboard() {
        $this->db->select('*');
        $this->db->from('active_project_byuser a');
        $this->db->join('bd_tenderdetails b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('b.visible_scope', 'Active_project');
        $this->db->limit(4, 0);
        $this->db->order_by("a.fld_id", "DESC");
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Latest Bid Project,,,
    public function GetlatestBidProjectDashboard() {
        $this->db->select('*');
        $this->db->from('bid_project a');
        $this->db->join('bd_tenderdetails b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('b.visible_scope', 'Bid_project');
        $this->db->limit(4, 0);
        $this->db->order_by("a.fld_id", "DESC");
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Select All Notification ..
    public function GetAllNotification() {
        $this->db->select('*');
        $this->db->from('notification');
        $this->db->order_by("fld_id", "DESC");
        $this->db->limit(5);

        $this->db->where(array('user_to_notify' => $this->session->userdata('uid'), 'seen_by_user' => 'no'));
        $query = $this->db->get()->result_array();
        if ($query):
            return $query;
        else:
            return false;
        endif;
    }

    //Count Notification...
    public function CountAllNotification() {

        $this->db->select('fld_id');
        $this->db->from('notification');
        $this->db->limit(5);

        $this->db->where(array('user_to_notify' => $this->session->userdata('uid'), 'seen_by_user' => 'no'));
        $query = $this->db->get()->num_rows();
        if ($query):
            return $query;
        else:
            return '0';
        endif;
    }

    //Get Tender Name By ID..
    public function GetProjectNameById($userID) {
        $adminrec = $this->selectRecord('bd_tenderdetails', array('TenderDetails'), array('fld_id' => $userID));
        if ($adminrec) {
            $RecArr = $adminrec->row();
            return $RecArr->TenderDetails;
        } else {
            return false;
        }
    }

    // Coded By jitendra
    //No Submitted Project By User...
    public function GetNoSubmitProjByUser() {
        /* $selectp = $this->db->query("SELECT * FROM bd_tenderdetails WHERE visible_scope IN ('No_Submit_project','Bid_project') AND is_active='1'");
          $query = $selectp->result();
          echo '<pre>';
          print_r($query); die; */
        $this->db->select('*');
        $this->db->from('bd_tenderdetails bd');
        $this->db->where('bd.is_active', '1');
        $this->db->where('bd.visible_scope', 'No_Submit_project');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Complete List Project By User...
    public function GetcompleteProjByUser() {
        $arr = array('No_Submit_project', 'Bid_project', 'To_Go_project');
        $this->db->select('bd.*, d.generated_tenderid');
        $this->db->from('bd_tenderdetails bd');
        $this->db->join('tender_generated_id d', 'bd.fld_id=d.project_id', 'left');
        //$this->db->from('bid_project a');
        //$this->db->join('bd_tenderdetails bd', 'bd.fld_id=a.project_id', 'left');
        $this->db->where('bd.is_active', '1');
        $this->db->where_in('bd.visible_scope', $arr);
        $query = $this->db->get();

        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //User Full Name By ID..
    public function BidProjectById($projectid) {
        $catnm = $this->selectRecord('bid_project', array('project_status'), array('project_id' => $projectid));
        if ($catnm) {
            $rows = $catnm->first_row();
            $res = $rows->project_status;
            return $res;
        } else {
            return false;
        }
    }

    public function bd_rolecheck() {
        $pmid = $this->session->userdata('uid');
        $query = $this->db->query('select role_id from bd_role where user_id= ' . $pmid);
        $result = $query->result();
        if (!empty($result)) {
            return $result[0]->role_id;
        } else {
            return false;
        }
    }

    public function allprojectProposalManager() {
        $query = $this->db->query('select a.*,b.userfullname from bd_role as a left join main_users as b on a.user_id = b.fld_id where a.role_id = 1');
        return $res = $query->result();
    }

    // All active Designation Master...
    function allActiveDesignation() {
        $Rec = $this->Front_model->selectRecordOrderByASC('designation_master_requisition', array('designation_name', 'fld_id'), array('is_active' => '1'));
        if ($Rec) {
            return $result = $Rec->result();
        }
        return false;
    }

    // All active Designation Sector...
    function allActiveDesignationSector() {
        $Rec = $this->db->query('select * from designation_sector');
        if ($Rec) {
            return $result = $Rec->result();
        }
        return false;
    }

    public function getCegScore($projID) {
        $query = $this->db->query('select * from cegexp_competitor_comp where projid= ' . $projID . ' and compt_comp_id= 74');
        $result = $query->result();
        if (!empty($result)) {
            return $result[0];
        } else {
            return false;
        }
    }

    public function getProjectstatus($status, $userid) {
        $data = array();
        if ($status == 1) {
            $this->db->select('a.*, b.*');
            $this->db->from('team_assign_member a');
            $this->db->join('designation_master_requisition b', 'a.designation_id=b.fld_id', 'left');
            $this->db->where('a.empname', $userid);
            $data['result'] = $this->db->get()->result_object();
        } else {
            if (!empty($userid)) {
                $this->db->select('a.*, b.*');
                $this->db->from('team_assign_member a');
                $this->db->join('designation_master_requisition b', 'a.designation_id=b.fld_id', 'left');
                $this->db->where('a.empnameother', $userid);
                $data['result'] = $this->db->get()->result_object();
            }
        }
        foreach ($data['result'] as $val) {
            $projID[] = $val->project_id;
        }
        $this->db->select('SUM(IF(`proj_status` = "won", 1,0)) AS `won`,SUM(IF(`proj_status` = "loose", 1,0)) AS `loose`,
SUM(IF(`proj_status` = "cancel", 1,0)) AS `cancel`,
SUM(IF(`proj_status` = "awaiting", 1,0)) AS `awaiting`');
        $this->db->from('ceg_exp');
        $this->db->where_in('project_id', $projID);

        $data['status'] = $this->db->get()->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return false;
        }
    }

    public function getProjectDetailByUserID($userid) {
        $this->db->select('a.project_id, b.designation_name,c.proj_status,c.project_name');
        $this->db->from('team_assign_member a');
        $this->db->join('designation_master_requisition b', 'a.designation_id=b.fld_id', 'left');
        $this->db->join('ceg_exp c', 'a.project_id=c.project_id', 'left');
        $where = "(a.empname = '$userid' OR a.empnameother = '$userid')";
        $this->db->where($where);
        $result = $this->db->get()->result_array();
        return isset($result) ? $result : false;
    }

    //Get Active Sector Find By Jitendra
    public function GetActiveSector() {
        $this->db->select("*");
        $this->db->from("sector");
        $this->db->where('is_active', '1');
        $this->db->order_by("sectName", "asc");
        $Rec = $this->db->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function GetActiveService1() {
        $this->db->select("*");
        $this->db->from("pds_service1");
        $this->db->where('is_active', '1');
        $this->db->order_by("service_name", "asc");
        $Rec = $this->db->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function GetActiveService2() {
        $this->db->select("*");
        $this->db->from("pds_service2");
        $this->db->where('is_active', '1');
        $this->db->order_by("service_name", "asc");
        $Rec = $this->db->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    /* public function getAllProject(){
      $this->db->select('id,project_name');
      $this->db->from('tm_projects');
      $this->db->where('is_active', '1');
      $Rec = $this->db->get()->result();
      if ($Rec) {
      return $Rec;
      } else {
      return false;
      }
      } */

    public function getAllProject() {
        $this->db2->select('id,project_name');
        $this->db2->from('tm_projects');
        $this->db2->where('is_active', '1');
        $this->db2->order_by('project_name');
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function mainServices() {
        $this->db->select('*');
        $this->db->from('main_services');
        $this->db->where('is_active', '1');
        $res = $this->db->get()->result_object();
        return isset($res) ? $res : false;
    }

    public function getProjectDetails($id) {
        $this->db->select('*');
        $this->db->from('project_description');
        $this->db->where('project_id', $id);
        $this->db->where('is_active', '1');
        $res = $this->db->get()->result_object();
        return isset($res) ? $res[0] : false;
    }

    public function getTeamassign() {
        $this->db->select('a.empname,a.empnameother,b.userfullname,c.emp_name');
        $this->db->from('team_assign_member as a');
        $this->db->join('main_users as b', 'a.empname = b.fld_id', 'left');
        $this->db->join('team_req_otheremp as c', 'a.empnameother = c.fld_id', 'left');
        $this->db->where('a.status', '1');
        $res = $this->db->get()->result_object();
        if (!empty($res)) {
            $userdata = array();
            foreach ($res as $val) {
                if (!empty($val->empname)) {
                    $userdata[] = array('id' => $val->empname, 'name' => $val->userfullname);
                } else if (!empty($val->empnameother)) {
                    $userdata[] = array('id' => $val->empnameother, 'name' => $val->emp_name);
                }
            }
        }
        return isset($userdata) ? $userdata : false;
    }

    public function getAlldesignation() {
        $this->db->select('fld_id,designation_name');
        $this->db->from('designation_master_requisition');
        $this->db->where('is_active', '1');
        $this->db->order_by('designation_name');
        $res = $this->db->get()->result_object();
        return isset($res) ? $res : false;
    }

    public function getAllemployees() {
        $this->db2->select('*');
        $this->db2->from('main_users');
        $this->db2->where('isactive', '1');
        $this->db2->order_by('userfullname');
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function getuserdetailByID($id) {
        $this->db2->select('*');
        $this->db2->from('main_users');
        $this->db2->where('isactive', '1');
        $this->db2->where('id', $id);
        $Rec = $this->db2->get()->result();
		$false = 'false';
        if ($Rec) {
            return $Rec;
        } else {
            return $false;
        }
    }

    public function getuserdetailByprojectID($id) {
        $this->db2->select('b.userfullname,b.emailaddress,b.contactnumber,b.id,c.position_id,c.position_name');
        $this->db2->from('emp_otherofficial_data as a');
        $this->db2->join('main_users as b', 'a.user_id = b.id', 'left');
        $this->db2->join('main_employees_summary as c', 'a.user_id = c.user_id', 'left');
        $this->db2->where('a.status', '1');
        $this->db2->where('b.isactive', '1');
        $this->db2->where('on_project', $id);
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    // Login Check by Jitendra 
    public function checkLogin($id, $password) {
        $this->db2->select('a.*,b.department_id,b.businessunit_id');
        $this->db2->from('main_users as a');
        $this->db2->join('main_employees as b', 'a.id = b.user_id', 'left');
        $this->db2->where('a.isactive', '1');
        $where = array('employeeId' => $id, 'emppassword' => md5($password));
        $this->db2->where($where);
        $Rec = $this->db2->get()->result();
        if ($Rec[0]) {
            return $Rec[0];
        } else {
            return false;
        }
    }

    // Login Check by Jitendra For Single Screen
    public function checkSingleScreenLogin($id, $password) {
        $this->db2->select('a.*,b.department_id,b.businessunit_id');
        $this->db2->from('main_users as a');
        $this->db2->join('main_employees as b', 'a.id = b.user_id', 'left');
        $this->db2->where('a.isactive', '1');
        $where = array('employeeId' => $id, 'emppassword' => $password);
        $this->db2->where($where);
        $Rec = $this->db2->get()->result();
        if ($Rec[0]) {
            return $Rec[0];
        } else {
            return false;
        }
    }

    public function getBusinessunit() {
        $this->db2->select('a.id,a.unitname');
        $this->db2->from('main_businessunits as a');
        $this->db2->where('a.isactive', '1');
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function getDept($id) {
        $this->db2->select('a.id,a.deptname');
        $this->db2->from('main_departments as a');
        $this->db2->where('a.isactive', '1');
        $this->db2->where('a.unitid', $id);
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function getUser($id) {
        $this->db2->select('a.user_id,a.userfullname,a.employeeId');
        $this->db2->from('main_employees_summary as a');
        $this->db2->where('a.isactive', '1');
        $this->db2->where('a.department_id', $id);
        $this->db2->order_by('a.userfullname');
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function getUserByID($id) {
        $this->db1->select('a.*');
        $this->db1->from('main_users as a');
        $this->db1->where('a.isactive', '1');
        $this->db1->where('a.id', $id);
        $Rec = $this->db1->get()->result();
        if ($Rec[0]) {
            return $Rec[0];
        } else {
            return false;
        }
    }

    public function getUserfullnameHrms($id) {
        $this->db2->select('a.prefix_name,a.userfullname');
        $this->db2->from('main_employees_summary as a');
        $this->db2->where('a.isactive', '1');
        $this->db2->where('a.user_id', $id);
        $Rec = $this->db2->get()->row();
        if ($Rec) {
            return $Rec->prefix_name . " " . $Rec->userfullname;
        } else {
            return false;
        }
    }

    public function getProject() {
        $this->db2->select('a.id,a.project_name');
        $this->db2->from('tm_projects as a');
        $this->db2->where('a.is_active', '1');
        $this->db2->order_by("a.project_name", "ASC");
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    

    public function getProjectBYId($id) {
        $this->db2->select('a.id,a.project_name');
        $this->db2->from('tm_projects as a');
        $this->db2->where('a.is_active', '1');
        $this->db2->where('a.id', $id);
        $Rec = $this->db2->get()->result();
        if ($Rec[0]) {
            return $Rec[0];
        } else {
            return false;
        }
    }

//    public function getProjectBYIdts($id) {
//        $this->db2->select('a.id,a.project_name');
//        $this->db2->from('tm_projects as a');
//        $this->db2->where('a.is_active', '1');
//       // $this->db2->where("(a.project_category='ie' OR a.project_category='ae')");
//        $this->db2->where('a.id', $id);
//        $Rec = $this->db2->get()->result();
//        if ($Rec[0]) {
//            return $Rec[0];
//        } else {
//            return false;
//        }
//    }
    //BD CRU mail List Code By Jitendra..
    public function GetBdEmailsList() {
        $emailArr = array();
        $this->db2->select('a.id,a.ofcialemail');
        $this->db2->from("main_users as a");
        $this->db2->join("main_employees_summary as b ", "a.id = b.user_id", 'left');
        $this->db2->where('b.department_id', '3');
        $this->db2->where('a.isactive', '1');
        $query = $this->db2->get()->result();
        //echo '<pre>'; print_r($query);
        if ($query):
            foreach ($query as $rowEml) {
                array_push($emailArr, $rowEml->ofcialemail);
            }
            return $emailArr;
        else:
            return false;
        endif;
    }

    //BD CRU mail List Code By Jitendra..
    public function GetCruEmailsList() {
        $emailArr = array();
        $this->db2->select('a.id,a.ofcialemail');
        $this->db2->from("main_users as a");
        $this->db2->join("main_employees_summary as b ", "a.id = b.user_id", 'left');
        $this->db2->where('b.department_id', '14');
        $this->db2->where('a.isactive', '1');
        $query = $this->db2->get()->result();
        //echo '<pre>'; print_r($query);
        if ($query):
            foreach ($query as $rowEml) {
                array_push($emailArr, $rowEml->ofcialemail);
            }
            return $emailArr;
        else:
            return false;
        endif;
    }

    public function GetAllUserRecByprojectID($projectid) {
        $this->db2->select('a.id,a.userfullname');
        $this->db2->from('main_users as a');
        $this->db2->join('tm_project_employees as b', 'b.emp_id = a.id', 'left');
        $this->db2->where('a.isactive', '1');
        $this->db2->where('b.project_id', $projectid);
        //$this->db2->where('b.is_active', '1');
        $this->db2->order_by("a.userfullname", "ASC");
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    //Get User Login Name..
    public function loginUserName() {
        $this->db2->select(array('userfullname'));
        $this->db2->from("main_users");
        $this->db2->where('id', $this->session->userdata('uid'));
        $query = $this->db2->get()->row();
        return $query->userfullname;
    }

}

?>
