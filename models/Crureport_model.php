<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Crureport_model extends CI_Model {

    var $table = 'team_assign_member as a';
    var $user_table = 'main_users as b';
    var $otheruser_table = 'team_req_otheremp as c';
    var $column_order = array(null, 'b.userfullname', 'c.emp_name', 'a.age_limit'); //set column field database for datatable orderable
    var $column_search = array('b.userfullname', 'c.emp_name', 'a.age_limit'); //set column field database for datatable searchable 

    //var $order = array('a.fld_id' => 'aesc'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE); 
    }

    private function _get_datatables_query() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.team_assign_member.empname,$db1.team_assign_member.age_limit,$db1.team_assign_member.empnameother,$db2.main_users.userfullname,$db1.team_req_otheremp.emp_name,count($db1.team_assign_member.id) AS 'total_project' ");
        $this->db->from("$db1.team_assign_member");
        $this->db->join("$db2.main_users", "$db1.team_assign_member.empname = $db2.main_users.id", "left");
        $this->db->join("$db1.team_req_otheremp", "$db1.team_assign_member.empnameother = $db1.team_req_otheremp.fld_id", "left");
        $this->db->where("$db1.team_assign_member.status", "1");
        $this->db->group_by(array("$db1.team_assign_member.empname", "$db1.team_assign_member.empnameother"));

        $i = 0;

        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        // $this->db->from($this->table);
        // return $this->db->count_all_results();
        return '0';
    }

}
