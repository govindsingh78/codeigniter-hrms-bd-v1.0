<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mastermodel extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->db1 = $this->load->database('online', TRUE);//sentrifugo
        $this->db2 = $this->load->database('another_db', TRUE);//bdtechno  ==default
    }

    public function UpdateRecords($cegth_table, $Where, $data) {
        $this->db->where($Where);
        return $this->db->update($cegth_table, $data);
        // $insert_id = $this->db->insert_id();
        // return $insert_id;
    }

//Get Company Name By ID ..
public function GetCompNameById($compID) {
    $this->db->select(array('company_name'));
    $this->db->from('main_company');
    $this->db->where(array('status' => '1', 'fld_id' => $compID));
    $RecArr = $this->db->get()->result();
    return ucwords(strtolower($RecArr[0]->company_name));
}

    //######### New Hrms 07-02-1992 ###########
    public function GetBasicRecLoginUser() {
		$db2 = $this->db2->database;
        $db1 = $this->db1->database;
        $id = $this->session->userdata('loginid');
        $this->db->select('user.*,b.thumbcode,b.payroll_with_name');
        $this->db->from("$db1.main_employees_summary as user");
        $this->db->join("$db1.emp_otherofficial_data b", "user.user_id=b.user_id", "LEFT");
        $this->db->where(array("user.user_id" => $id, "user.isactive" => "1"));
        $RecSingleRow = $this->db->get()->row();
        return ($RecSingleRow) ? $RecSingleRow : null;
    }

    //Get Official Rec By UserId.. 
    public function GetOfficialDataByUserId($userID) {
        $this->db->select('user.*,c.prefix_name as prefix_name_ro,c.userfullname as userfullname_ro,d.subdepartment,e.company_name,b.probation_period_no,b.noticeperiod');
        $this->db->from("$db1.main_employees_summary as user");
        $this->db->join("$db1.emp_otherofficial_data b", "user.user_id=b.user_id", "LEFT");
        $this->db->join("$db1.main_employees_summary c", "b.reviewing_officer_ro=c.user_id", "LEFT");
        $this->db->join("$db1.main_subdepartments d", "b.sub_department=d.id", "LEFT");
        $this->db->join("$db1.tbl_companyname e", "b.company_name=e.id", "LEFT");
        $this->db->where(array("user.user_id" => $userID, "user.isactive" => "1"));
        $RecSingleRow = $this->db->get()->row();
        return ($RecSingleRow) ? $RecSingleRow : null;
    }

    //Get Experiance Details..
    public function GetExperianceDetailByID($userID) {
        $this->db->select('a.*');
        $this->db->from('main_empexperiancedetails as a');
        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    //Skill..
    public function GetSkillRecordDetailByID($userID) {
        $this->db->select('a.*');
        $this->db->from('main_empskills as a');
        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    public function GetEducationDetailRecByID($userID) {
        $this->db->select('a.*,b.educationlevelcode');
        $this->db->from('main_empeducationdetails as a');
        $this->db->join('main_educationlevelcode as b', "a.educationlevel=b.id", "LEFT");
        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    //Family Details..
    public function GetFamilyDetailRecByID($userID) {
        $this->db->select('a.*');
        $this->db->from('main_empdependencydetails as a');
        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    //Employee Docs..
    public function GetEmployeeDocsDetailRecByID($userID) {
        $this->db->select('a.*');
        $this->db->from('main_employeedocuments as a');
        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

//Get Personnal Details..
    public function GetPersonalDetailsRecByID($userID) {
        $this->db->select('a.*,b.maritalstatusname,c.nationalitycode,d.languagename');
        $this->db->from('main_emppersonaldetails as a');
        $this->db->join('main_maritalstatus as b', "a.maritalstatusid=b.id", "LEFT");
        $this->db->join('main_nationality as c', "a.nationalityid=c.id", "LEFT");
        $this->db->join('main_language as d', "a.languageid=d.id", "LEFT");
        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->row();
        return ($RecRows) ? $RecRows : null;
    }

//Contact Details..
    public function GetContactDetailRecByID($userID) {
        $this->db->select('a.*,b.country_name as per_country_name,c.country_name as current_country_name,d.state_name as perm_state_name,e.state_name as current_state_name,f.city_name as perm_city_name,g.city_name as current_city_name');
        $this->db->from('main_empcommunicationdetails as a');
        $this->db->join('tbl_countries as b', "a.perm_country=b.id", "LEFT");
        $this->db->join('tbl_countries as c', "a.current_country=c.id", "LEFT");
        $this->db->join('tbl_states as d', "a.perm_state=d.id", "LEFT");
        $this->db->join('tbl_states as e', "a.current_state=e.id", "LEFT");
        $this->db->join('tbl_cities as f', "a.perm_city=f.id", "LEFT");
        $this->db->join('tbl_cities as g', "a.current_city=g.id", "LEFT");
        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->row();
        return ($RecRows) ? $RecRows : null;
    }

    //Get Previous Day In Time And Out Time..
    public function GetInOutTimeDetails($userThumbID) {
		$db2 = $this->db2->database;
        $db1 = $this->db1->database;
        $this->db->select('a.*');
        $this->db->from("$db1.thumb_attendance as a");
        $this->db->where(array("a.EmployeeID" => $userThumbID));
        $this->db->where(array("a.Status" => "1"));
        $this->db->order_by("a.thumbid", "DESC");
        $this->db->limit(1);
        $RecRows = $this->db->get()->row();
        return ($RecRows) ? $RecRows : null;
    }

    //Get Employee Salary Details..
    public function GetEmpSalaryDetailRecByID($userID) {
        $this->db->select('a.*,b.currencyname,c.freqtype');
        $this->db->from('main_empsalarydetails as a');
        $this->db->join('main_currency as b', "a.currencyid=b.id", "LEFT");
        $this->db->join('main_payfrequency as c', "a.salarytype=c.id", "LEFT");
        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->row();
        return ($RecRows) ? $RecRows : null;
    }

    //Pay Slips Details..
    public function GetEmpPayslipsRecByuserID($payroll_with_name, $year = '') {
        $this->db->select('a.*');
        $this->db->from('payslip_filepath as a');
        $this->db->where(array("a.payroll_with_name" => $payroll_with_name, "a.is_active" => "1"));
        if ($year) {
            $this->db->where("a.year", $year);
        }
        $this->db->order_by("a.id", "DESC");
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    //Get ITR Form Record By User Id..
    public function GetItrFormDetailsRecByID($userID) {
        $this->db->select('a.*');
        $this->db->from('main_itr as a');
        $this->db->where(array("a.user_id" => $userID, "a.is_active" => "1"));
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    //Get My Team Details..
    public function GetMyTeamDetailsRecByID($userID) {
        $this->db->select('a.*');
        $this->db->from('main_employees_summary as a');
        $this->db->where(array("a.reporting_manager" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    //Get All Applied Leave..
    public function GetMyAllAppliedLeaves($userID) {
        $this->db->select('a.*');
        $this->db->from('main_leaverequest_summary as a');
        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $this->db->order_by("a.id", "DESC");
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    //Get All Holidays List..
    public function GetAllHolidaysListRec($hgroupID = '', $hyear = '') {
        $this->db->select('a.*,b.groupname');
        $this->db->from('main_holidaydates as a');
        $this->db->join('main_holidaygroups as b', "a.groupid=b.id", "LEFT");
        $this->db->where(array("a.isactive" => "1"));
        if ($hgroupID) {
            $this->db->where(array("a.groupid" => $hgroupID));
        }
        if ($hyear) {
            $this->db->where(array("a.holidayyear" => $hyear));
        }
        $this->db->order_by("a.holidaydate", "ASC");
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    //Get All Tour List By UserID..
    public function GetAllAppliedTourListRec($userID) {
        $this->db->select('a.*,b.project_name,c.prefix_name,c.userfullname');
        $this->db->from('emp_tourapply as a');
        $this->db->join('tm_projects as b', "a.project_id=b.id", "LEFT");
        $this->db->join('main_employees_summary as c', "a.emp_id=c.user_id", "LEFT");
        $this->db->where(array("a.emp_id" => $userID, "a.is_active" => "1"));
        $this->db->order_by("a.start_date", "DESC");
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

//check login
    public function login_user_action($LoginCredentialArr) {
		$db2 = $this->db2->database;
        $db1 = $this->db1->database;
        $Where = array('a.employeeId' => $LoginCredentialArr['username'], 'a.emppassword' => md5($LoginCredentialArr['password']), 'a.isactive' => '1');
        $this->db->select('a.*,b.department_id,b.businessunit_id');
        $this->db->from("$db1.main_users as a");
        $this->db->join("$db1.main_employees as b", "a.id = b.user_id", "LEFT");
        $this->db->where($Where);
        $recArr = $this->db->get()->result();
        if ($recArr):
            return $recArr;
        else:
            return false;
        endif;
    }

    //code by govind
    //Edited By #GDSINGH 15012020

    public function getTotalActiveEmployeeCeg($id) {
        // $this->db1->database;
        // $db2 = $this->db2->database;
        $this->db->select('*');
        $this->db->from('main_employees_summary');
        $this->db->where(array('isactive' => '1', 'businessunit_id' => $id));
        $result = $this->db->get()->result();
        $resultCount = count($result);
        return isset($resultCount) ? $resultCount : 'N/A';
    }

    //code by gaurav
    //Get Table Data Record.
    public function GetTableData($cegth_table, $Where) {
        $this->db->select('*');
        $this->db->from($cegth_table);
        $this->db->order_by("id", "DESC");
        $this->db->where($Where);
        $recArr = $this->db->get()->result();
        return $recArr;
    }

    //Get All Applied Leave..
    public function GetAllActiveProjectList() {
        $this->db->select('a.id,a.project_name');
        $this->db->from('tm_projects as a');
        $this->db->where(array("a.is_active" => "1"));
        $this->db->order_by("a.project_name", "DESC");
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    public function set_prodesignation_byprojidbd_dropd_ajax($tsProjId) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        //JPG Tonk D..Static ID..
        $BDprojID = $this->GetProjIdTStoBDProjID($tsProjId);
        if ($BDprojID) {
            $this->db->select('a.designation_id,b.designation_name');
            $this->db->from("$db2.assign_finalteam as a");
            $this->db->join("$db2.designation_master_requisition as b", "a.designation_id=b.fld_id", "INNER");
            $this->db->where(array("a.status" => "1", "a.project_id" => $BDprojID));
            $this->db->group_by("a.designation_id");
            $this->db->order_by("b.designation_name", "ASC");
            $designationList = $this->db->get()->result();
        }
        return ($designationList) ? $designationList : null;
    }

    //Get Proj Id TS to BD Proj ID..
    public function GetProjIdTStoBDProjID($tsProjID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select('a.project_id');
        $this->db->from("$db2.accountinfo as a");
        $this->db->where(array("a.status" => "1", "a.project_numberid" => $tsProjID));
        $BdProjID = $this->db->get()->row();
        return ($BdProjID) ? $BdProjID->project_id : "";
    }

    //Get Employee Code..
    public function GetEmpIDCode() {
        $this->db->select('a.employee_code');
        $this->db->from("main_identitycodes as a");
        $EmpCode = $this->db->get()->row();
        return ($EmpCode) ? $EmpCode->employee_code : "";
    }

    //Get Last Employee Id By CompanyID..
    public function GetLastEmpIdByCompanyID($compID) {
        $this->db->select('a.employeeId');
        $this->db->from("main_users as a");
        $this->db->join("main_employees_summary as b", "a.id=b.user_id", "LEFT");
        $this->db->where("a.company_id", $compID);
        $this->db->where("b.businessunit_id!=", "3");
        $this->db->order_by("a.id", "DESC");
        $this->db->limit("1");
        $EmpCode = $this->db->get()->row();
        return ($EmpCode) ? $EmpCode->employeeId : "";
    }

    //Get Last Employee Id CEG Project..
    public function GetLastEmpIdCEGProj() {
        $this->db->select('a.employeeId');
        $this->db->from("main_users as a");
        $this->db->join("main_employees_summary as b", "a.id=b.user_id", "LEFT");
        $this->db->where("b.businessunit_id", "3");
        $this->db->order_by("a.id", "DESC");
        $this->db->limit("1");
        $EmpCode = $this->db->get()->row();
        return ($EmpCode) ? $EmpCode->employeeId : "";
    }

    //Get Employee Official Details By UserID..
    public function GetEmpOfficialDataByUserID($empID) {
        $this->db->select('a.*,b.date_of_joining,b.reporting_manager,b.emp_status_id,b.businessunit_id,b.department_id,b.ofc_locationid,b.company_nameid,b.ofcialemail,b.probation_period,b.probation_no_of_day,b.noticeperiod,b.jobtitle_id,b.position_id,b.years_exp,b.prefix_id,b.extension_number,b.office_number,b.thumbcode,b.reporting_manager_io,b.subdepartment,b.branch_id,c.reporting_manager_name,c.emp_status_name,c.businessunit_name,c.department_name,c.jobtitle_name,c.jobtitle_id,c.emp_status_id,c.position_name,c.holiday_group,c.holiday_group_name,c.prefix_name,c.emprole_name,c.referer_name,c.createdby_name');
        $this->db->from("main_users as a");
        $this->db->join("main_employees as b", "a.id=b.user_id", "LEFT");
        $this->db->join("main_employees_summary as c", "a.id=c.user_id", "LEFT");
        $this->db->where(array("a.isactive" => '1', "a.id" => $empID));
        $recSingleEmplDetails = $this->db->get()->row();
        return ($recSingleEmplDetails) ? $recSingleEmplDetails : null;
    }
	
	public function count_rows($cegth_table, $Where) {
        $this->db->select('*');
        $this->db->from($cegth_table);
        $this->db->where($Where);
        $rec = $this->db->count_all_results();
        return ($rec) ? $rec : '0';
    }
	
	//Select Recored From..
    public function SelectRecordC($cegth_table, $Where) {
        $this->db->select('*');
        $this->db->from($cegth_table);
        $this->db->order_by("country_name", "ASC");
        $this->db->where($Where);
        return $this->db->get()->result();
    }
	
	//Select Recored From..
    public function SelectRecord($cegth_table, $Where, $orderfield = '', $ordtype = '') {
        $this->db->select('*');
        $this->db->from($cegth_table);
        //$this->db->order_by($orderBy);
        $this->db->where($Where);
        $this->db->order_by($orderfield, $ordtype);
        return $this->db->get()->result();
    }
	
	    //Select All Country..
    public function allcountry() {
        $this->db->select('*');
        $this->db->from('countries');
        $this->db->order_by("country_name", "ASC");
        $this->db->where(array('status' => '1'));
        $recArr = $this->db->get()->result();
        return $recArr;
    }
	
	    //State Details By Id
    public function StateByCounID($cid) {
        $this->db->select(array('state_id', 'state_name'));
        $this->db->from('states');
        $this->db->order_by("state_name", "ASC");
        $this->db->where(array('status' => '1', 'country_id' => $cid));
        $recArr = $this->db->get()->result();
        return $recArr;
    }
	


}

?>