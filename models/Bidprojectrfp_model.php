<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bidprojectrfp_model extends CI_Model {

    var $table = 'bd_tenderdetail as a';
    var $genertedid_table = 'tender_generated_id as b';
    var $projectmanager_table = 'assign_project_manager as c';
    
    /*************code by durgsh(12-09-2019)*************/
    var $client_table = 'clientcontact as g';
    var $jv_table = 'jv_companyrecords as h';
    
    /*************code by durgsh(12-09-2019)*************/
    var $bidstatus_table = 'bdproject_status as e';
    var $scope_table = 'bdtender_scope as f';
    
    var $column_order = array(null, 'a.TenderDetails', 'a.Location', 'a.Organization', 'a.keyword_phase'); //set column field database for datatable orderable
    var $column_search = array('b.generated_tenderid', 'a.keyword_phase', 'a.TenderDetails', 'a.Location', 'a.Organization'); //set column field database for datatable searchable 
    var $order = array('a.fld_id' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {
        if ($this->input->post('sectorinput')) {
            $this->db->like('a.Sector_IDs', $this->input->post('sectorinput'));
        }
        if (($this->input->post('financial_status')) AND !is_null($this->input->post('financial_status'))) {
            $this->db->where('e.project_status', $this->input->post('financial_status'));
        }
	   
        if ($this->input->post('financial_year')) {
            $financial_year = $this->input->post('financial_year');
            $start_date = $financial_year . '-04-01';
            $end_date = ($financial_year + 1) . '-03-31';
            $where_date = "(a.Expiry_Date>='$start_date' AND a.Expiry_Date <= '$end_date')";
            $this->db->where($where_date);
         }
         
        $businessID = $this->session->userdata('businessunit_id');

        $this->db->select('a.*,b.generated_tenderid,b.generate_type,c.assign_to,e.*,,g.client_name,g.client_contact,g.client_position,h.lead_companes,h.joint_ventures,h.associate_company,c1.country_name,s1.state_name,sec1.sectName,serv.service_name');
        $this->db->from($this->table);
        $this->db->join($this->bidstatus_table, 'a.fld_id = e.project_id', 'left');
        $this->db->join($this->genertedid_table, 'a.fld_id = b.project_id', 'left');
        $this->db->join($this->projectmanager_table, 'a.fld_id = c.project_id', 'left');
        $this->db->join($this->scope_table, "a.fld_id = f.project_id", "left");
        
        /*************code by durgsh(12-09-2019)*************/
        $this->db->join($this->client_table, "a.fld_id = g.project_id", "left");
        $this->db->join($this->jv_table, "a.fld_id = h.project_id", "left");

        /*************Start code by asheesh (14-09-2019)*************/
        $this->db->join("project_description as prjdesc", "a.fld_id = prjdesc.project_id", "LEFT");
        $this->db->join("countries as c1", "c1.country_id = prjdesc.country_id", "LEFT");
        $this->db->join("states as s1", "s1.state_id = prjdesc.state_id", "LEFT");
        $this->db->join("sector as sec1", "sec1.fld_id = prjdesc.sector_id", "LEFT");
        $this->db->join("main_services as serv", "serv.id = prjdesc.service_id", "LEFT");

        /*************code by durgsh(12-09-2019)*************/
        $scope_arr = array('Bid_project','Manaul_project');
        $this->db->where_in('f.visible_scope', $scope_arr);
        $this->db->where('f.businessunit_id', $businessID);
        $this->db->where('b.generate_type', 'P');
        $this->db->order_by('b.fld_id', 'DESC');
        $this->db->group_by('a.fld_id');

        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $this->db->where('f.visible_scope', 'Bid_project');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        // $this->db->from($this->table);
        // $this->db->where('a.is_active', '1');
        // $this->db->where('a.visible_scope', 'Bid_project');
        //  return $this->db->count_all_results();
        return '0';
    }

    public function get_bidprojects($pidd = '') {
        $this->db->select('a.*,b.generated_tenderid,b.generate_type,c.assign_to,d.userfullname,e.*');
        $this->db->from($this->table);
        $this->db->join($this->bid_table, 'a.fld_id = e.project_id', 'left');
        $this->db->join($this->genertedid_table, 'a.fld_id = b.project_id', 'left');
        $this->db->join($this->projectmanager_table, 'a.fld_id = c.project_id', 'left');
        $this->db->join($this->users_table, 'd.fld_id = c.assign_to', 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('a.visible_scope', 'Bid_project');
        $this->db->where('a.fld_id', $pidd);
        $query = $this->db->get();
        return $query->first_row();
    }

}
