<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Doctroldocumentmodal extends CI_Model {

    var $table = 'dbo_pwd_doctrol_metadata as a';
    var $column_order = array(null, 'a.doc_id', ); //set column field database for datatable orderable
    var $column_search = array('a.doc_id'); //set column field database for datatable searchable 
   // var $order = array("a.fld_id" => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();

        $this->db1 = $this->load->database('online', TRUE);
        $this->db2 = $this->load->database('another_db', TRUE);
    }

    private function _get_datatables_query($projid) {

        $db1 = $this->db1->database; //BD
        $db2 = $this->db2->database; //HRMS
        if ($this->input->post('rep_cat')) {
            $this->db->where("$db1.dbo_doctrol_upload_report.rep_category_id", $this->input->post('rep_cat'));
        }
        if ($this->input->post('rep_duration')) {
            $this->db->where("$db1.dbo_doctrol_upload_report.rep_duration", $this->input->post('rep_duration'));
        }
        if ($this->input->post('rep_from') and $this->input->post('rep_to')) {
            $fromdate = date("Y-m-d", strtotime($this->input->post('rep_from')));
            $todate = date("Y-m-d", strtotime($this->input->post('rep_to')));
            $this->db->where("$db1.dbo_doctrol_upload_report.entry_date >=", $fromdate);
            $this->db->where("$db1.dbo_doctrol_upload_report.entry_date <=", $todate);
        }
        $this->db->select("$db1.dbo_duration.duration_name,$db1.tbl_doctype.short_name,$db1.tbl_doctype.report_typename,$db1.dbo_doctrol_upload_report.entry_date,$db2.tm_projects.project_name,$db1.dbo_doctrol_upload_report.file_name");
        $this->db->from("$db1.dbo_doctrol_upload_report");
        $this->db->join("$db1.tbl_doctype", "$db1.tbl_doctype.report_id=$db1.dbo_doctrol_upload_report.rep_category_id", 'left');
        $this->db->join("$db1.dbo_duration", "$db1.dbo_duration.duration_id=$db1.dbo_doctrol_upload_report.rep_duration", 'left');
        $this->db->join("$db2.tm_projects", "$db2.tm_projects.id = $db1.dbo_doctrol_upload_report.rep_project_id", 'left');
        $this->db->where("$db1.dbo_doctrol_upload_report.rep_project_id", $projid);
        $this->db->order_by("$db1.dbo_doctrol_upload_report.rep_id","DESC");


        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($projid) {
        $this->_get_datatables_query($projid);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($projid) {
        $this->_get_datatables_query($projid);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
         $this->db->from($this->table);
         return $this->db->count_all_results();

    }

}
