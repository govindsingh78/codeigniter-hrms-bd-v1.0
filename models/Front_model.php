<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Front_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        //$this->load->database();
        $this->db1 = $this->load->database('another_db', TRUE);
        $this->db2 = $this->load->database('online', TRUE);
    }

    //Working Function..
    public function GetLoginDetailsArr() {
        $this->db->select(array('fld_id', 'emprole', 'firstname', 'lastname', 'userfullname', 'emailaddress', 'contactnumber', 'employeeId', 'selecteddate', 'thumbcode'));
        $this->db->from("main_users");
        $this->db->where('fld_id', $this->session->userdata('loginid'));
        $query = $this->db->get()->row();
        return $query;
    }

    public function getReqRecordbyUser() {
        $pmid = $this->session->userdata('loginid');
        $selectp = $this->db->query("SELECT apm.*,td.TenderDetails,togo.projectgenid,mu.userfullname FROM assign_project_manager as apm left join bd_tenderdetail as td ON apm.project_id=td.fld_id left join proj_togo_for_user as togo ON apm.project_id=togo.project_id left join main_users as mu ON mu.fld_id=apm.assign_to WHERE apm.assign_to=" . $pmid . " AND apm.is_active='1'");
        $query = $selectp->result();
        if (empty($query)) {
            $selectp1 = $this->db->query("SELECT apm.*,td.TenderDetails,togo.projectgenid,mu.userfullname FROM assign_project_manager as apm left join bd_tenderdetail as td ON apm.project_id=td.fld_id left join proj_togo_for_user as togo ON apm.project_id=togo.project_id left join main_users as mu ON mu.fld_id=apm.assign_to WHERE apm.is_active='1'");
            $query = $selectp1->result();
        }
        //        $selectp = $this->db->query("SELECT bp.*,td.TenderDetails,togo.projectgenid,pm.proj_status,mu.userfullname FROM bid_project as bp left join bd_tenderdetails as td ON bp.project_id=td.fld_id left join proj_togo_for_user as togo ON bp.project_id=togo.project_id left join assign_project_manager as pm ON pm.project_id=bp.project_id left join main_users as mu ON mu.fld_id=bp.proposal_manager WHERE pm.assign_to=".$pmid." AND bp.is_active='1'");
//        $selectd = $this->db->query("SELECT * FROM designation_master_requisition WHERE 1");
//        $query['desi'] = $selectd->result();
        return $query;
    }

    //Working Function..
    public function getAllReqRecord() {
        $selectp = $this->db->query("SELECT td.TenderDetails,tr.*,dmr.fld_id,dmr.designation_name FROM bd_tenderdetail as td, team_reqbypm as tr,designation_master_requisition as dmr WHERE tr.project_id=td.fld_id AND tr.designation_id=dmr.fld_id AND tr.req_status='1' AND tr.is_active='1'");
        $query = $selectp->result();
        return $query;
    }

    //Get Tender Details By ID.
    public function GettenderNameById($tndrID) {
        $selectp = $this->db->query("SELECT * FROM bd_tenderdetail WHERE is_active='1' AND fld_id=$tndrID ");
        $query = $selectp->result();
        return $query;
    }

    //Working Function..
    public function getReqRecordbyProj($projid) {
        $selectp = $this->db->query("SELECT td.TenderDetails,tr.*,dmr.fld_id,dmr.designation_name FROM bd_tenderdetail as td,team_reqbypm as tr,designation_master_requisition as dmr WHERE tr.project_id=td.fld_id AND tr.designation_id=dmr.fld_id AND tr.project_id='" . $projid . "' AND tr.is_active='1'");
        $query['req'] = $selectp->result();
        $selectt = $this->db->query("SELECT tp.*,tm.userfullname,tm.emailaddress,tm.contactnumber,tm.is_locked FROM team_members as tm,team_projectwise as tp WHERE tm.fld_id=tp.tmid AND tp.project_id=" . $projid . " AND tp.is_active='1'");
        $query['team'] = $selectt->result();
        return $query;
    }

    //Working Function..By Punit...
    public function getReqDataByPid($pid, $uid) {
        $selectp = $this->db->query("SELECT * FROM team_reqbypm WHERE project_id=" . $pid . " AND proposal_mngr_id=" . $uid);
        $query = $selectp->result();
        return $query;
    }

    //Get User Login Name..
    public function loginUserName() {
        $this->db->select(array('userfullname'));
        $this->db->from("main_users");
        $this->db->where('fld_id', $this->session->userdata('loginid'));
        $query = $this->db->get()->row();
        return $query->userfullname;
    }

    //Get User Login Dept..
    public function loginUserDept() {
        $this->db->select(array('dept_id'));
        $this->db->from("main_users");
        $this->db->where('fld_id', $this->session->userdata('loginid'));
        $query = $this->db->get()->row();
        return $query->dept_id;
    }

    //BD CRU mail List Code By Asheesh..
    public function GetCruEmailsList() {
        $emailArr = array();
        $this->db->select('*');
        $this->db->from("main_users");
        $this->db->where('dept_id', '14');
        $query = $this->db->get()->result();
        if ($query):
            foreach ($query as $rowEml) {
                array_push($emailArr, $rowEml->ofcialemail);
            }
            return $emailArr;
        else:
            return false;
        endif;
    }

    //User Full Name By ID..
    public function UserNameById($empId) {
        /* $catnm = $this->selectRecord('main_users', array('userfullname'), array('fld_id' => $empId));
          if ($catnm) {
          $userdata = $catnm->first_row();
          $res = $userdata->userfullname;
          return $res;
          } else {
          return false;
          } */
        $this->db2->select('userfullname');
        $this->db2->from('main_users');
        $this->db2->where("id", $empId);
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec[0]->userfullname;
        } else {
            return false;
        }
    }

    //User Name By Id Other ..
    public function UserOtherNameById($empId) {
        $catnm = $this->selectRecord('team_req_otheremp', array('emp_name'), array('fld_id' => $empId));
        if ($catnm) {
            $userdata = $catnm->first_row();
            $res = $userdata->emp_name;
            return $res;
        } else {
            return false;
        }
    }

    //Team Req Row By Id.
    public function Team_Req_RowById($Rwid) {
        $this->db->select('*');
        $this->db->from("team_reqbypm");
        $this->db->where('id', $Rwid);
        $Recdata = $this->db->get()->first_row();
        return ($Recdata) ? $Recdata : false;
    }

    //Get User Login Role..
    public function loginUserRole() {
        $this->db->select(array('emprole'));
        $this->db->from("main_users");
        $this->db->where('fld_id', $this->session->userdata('loginid'));
        $query = $this->db->get()->row();
        return $query->emprole;
    }

    public function checkpwdexistance($oldpass) {
        $adminrec = $this->selectRecord('tbl_admin', array('*'), array('fld_password' => $oldpass));
        if ($adminrec) {
            $res = TRUE;
        } else {
            $res = FALSE;
        }
        return $res;
    }

    public function check_existance($tablename, $fieldArray) {
        $record = $this->selectRecord($tablename, array('fld_id'), $fieldArray);
        if ($record) {
            $res = TRUE;
        } else {
            $res = FALSE;
        }
        return $res;
    }

    public function checkcategoryexisting($parentid, $categname) {
        $adminrec = $this->selectRecord('tbl_category', array('*'), array('parent' => $parentid, 'name' => $categname));
        if ($adminrec) {
            $res = TRUE;
        } else {
            $res = FALSE;
        }
        return $res;
    }

    public function GetAllUserRec() {
        $this->db2->select('*');
        $this->db2->from('main_users');
        $this->db2->where('isactive', '1');
        $this->db2->order_by("userfullname", "ASC");
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function team_req_otherempAll() {
        $this->db->select("*");
        $this->db->from("team_req_otheremp");
        $this->db->where('status', '1');
        $this->db->order_by("emp_name", "ASC");
        $query = $this->db->get()->result();
        return $query;
    }

    //Get All User
    public function GetAllBdUserRec() {
        $this->db->select("*");
        $this->db->from("main_users");
        $this->db->where('isactive', '1');
        $query = $this->db->get()->result();

        $userofBd = array('296' => '296', '287' => '287', '190' => '190', '260' => '260', '175' => '175', '346' => '346', '257' => '257', '256' => '256', '191' => '191', '259' => '259', '211' => '211');
        $returnArr = array();
        foreach ($query as $keyid => $bduser) {
            if (in_array($bduser->fld_id, $userofBd)):
                array_push($returnArr, $bduser);
            endif;
        }

        return $returnArr;
    }

    //Get All Prefix Rec,,,
    public function GetAllBdPrefixRec() {
        $this->db->select("*");
        $this->db->from("projectno_prefx");
        $this->db->where('is_active', '1');
        $query = $this->db->get()->result();
        return $query;
    }

    public function GetAllProduct() {
        $this->db->select("*");
        $this->db->from("tbl_products");
        $this->db->where('fld_visibility', '1');
        $this->db->order_by("fld_id", "DESC");
        $query = $this->db->get()->result();
        return $query;
    }

    public function chk_user_existance($username) {
        $adminrec = $this->selectRecord('tbl_user', array('user_id'), array('user_id' => $username));
        if ($adminrec) {
            $res = TRUE;
        } else {
            $res = FALSE;
        }
        return $res;
    }

    public function getCategoryName($catID) {
        $catnm = $this->selectRecord('tbl_category', array('name'), array('fld_id' => $catID));
        if ($catnm) {
            $categdata = $catnm->first_row();
            $res = $categdata->name;
            return $res;
        } else {
            $res = FALSE;
        }
        return $res;
    }

    //Get Active Project By User...
    public function GetActiveProjByUser() {
        $this->db->select('*');
        $this->db->from('active_project_byuser a');
        $this->db->join('bd_tenderdetail b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('b.is_active', '1');
        $this->db->where('b.visible_scope', 'Active_project');
        //$this->db->order_by('c.track_title', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //To Go Active Project,,,
    public function GetToGoProjByUser() {
        $this->db->select('a.*, b.*, c.assign_to, u.userfullname');
        $this->db->from('proj_togo_for_user a');
        $this->db->join('bd_tenderdetail b', 'b.fld_id=a.project_id', 'left');
        $this->db->join('assign_project_manager c', 'b.fld_id=c.project_id', 'left');
        $this->db->join('main_users u', 'u.fld_id=c.assign_to', 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('b.visible_scope', 'To_Go_project');
        //$this->db->order_by('c.track_title', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //No Go Project By User...
    public function GetNoGoProjByUser() {
        $this->db->select('*');
        $this->db->from('proj_nogo_for_user a');
        $this->db->join('bd_tenderdetail b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('b.is_active', '1');
        $this->db->where('b.visible_scope', 'No_Go_project');
        //$this->db->order_by('c.track_title', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Get Inactive By User..
    public function GetinActiveProjByUser() {
        $this->db->select('*');
        $this->db->from('trash_for_user a');
        $this->db->join('bd_tenderdetail b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('b.is_active', '1');
        $this->db->where('b.visible_scope', 'InActive_project');
        //$this->db->order_by('c.track_title', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Get All Important Project..
    public function GetAllImportantProjByID() {
        $this->db->select('*');
        $this->db->from('import_for_user a');
        $this->db->join('bd_tenderdetail b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('b.is_active', '1');
        $this->db->where('b.visible_scope', 'Important_project');
        //$this->db->order_by('c.track_title', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Get All Review Project By ID..
    public function GetAllReviewProjByID() {
        $this->db->select('*');
        $this->db->from('proj_review_for_user a');
        $this->db->join('bd_tenderdetail b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('b.visible_scope', 'In_Review_project');
        //$this->db->order_by('c.track_title', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Get All marked as NotImportant Project..
    public function GetAllNotImportantProjByID() {
        $this->db->select('*');
        $this->db->from('notimport_for_user a');
        $this->db->join('bd_tenderdetail b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('b.visible_scope', 'Not_Important_project');
        //$this->db->order_by('c.track_title', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //All Bid Projects..
    public function GetAllbidprojects() {
        $this->db->select('a.*, b.*, d.generated_tenderid, c.assign_to, u.userfullname');
        $this->db->from('bid_project a');
        $this->db->join('bd_tenderdetail b', 'b.fld_id=a.project_id', 'left');
        $this->db->join('tender_generated_id d', 'b.fld_id=d.project_id', 'left');
        $this->db->join('assign_project_manager c', 'b.fld_id=c.project_id', 'left');
        $this->db->join('main_users u', 'u.fld_id=c.assign_to', 'left');
        $this->db->where('b.is_active', '1');
        $this->db->where('b.visible_scope', 'Bid_project');
//        $this->db->where('c.project_id', 'Bid_project');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Latest To Do List For Dashboard...
    public function GetlatestToGoListDashboard() {
        $this->db->select('*');
        $this->db->from('proj_togo_for_user a');
        $this->db->join('bd_tenderdetail b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('b.visible_scope', 'To_Go_project');
        $this->db->limit(4, 0);
        $this->db->order_by("a.fld_id", "DESC");

        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Latest Important Project..
    public function GetlatestImportantListDashboard() {
        $this->db->select('*');
        $this->db->from('import_for_user a');
        $this->db->join('bd_tenderdetail b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('b.visible_scope', 'Important_project');
        $this->db->limit(4, 0);
        $this->db->order_by("a.fld_id", "DESC");

        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Active Project,,,
    public function GetlatestActiveListDashboard() {
        $this->db->select('*');
        $this->db->from('active_project_byuser a');
        $this->db->join('bd_tenderdetail b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('b.visible_scope', 'Active_project');
        $this->db->limit(4, 0);
        $this->db->order_by("a.fld_id", "DESC");
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Latest Bid Project,,,
    public function GetlatestBidProjectDashboard() {
        $this->db->select('*');
        $this->db->from('bid_project a');
        $this->db->join('bd_tenderdetail b', 'b.fld_id=a.project_id', 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('b.visible_scope', 'Bid_project');
        $this->db->limit(4, 0);
        $this->db->order_by("a.fld_id", "DESC");
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //Select All Notification ..
    public function GetAllNotification() {
        $this->db->select('*');
        $this->db->from('notification');
        $this->db->order_by("fld_id", "DESC");
        $this->db->limit(5);

        $this->db->where(array('user_to_notify' => $this->session->userdata('loginid'), 'seen_by_user' => 'no'));
        $query = $this->db->get()->result_array();
        if ($query):
            return $query;
        else:
            return false;
        endif;
    }

    //Count Notification...
    public function CountAllNotification() {

        $this->db->select('fld_id');
        $this->db->from('notification');
        $this->db->limit(5);

        $this->db->where(array('user_to_notify' => $this->session->userdata('loginid'), 'seen_by_user' => 'no'));
        $query = $this->db->get()->num_rows();
        if ($query):
            return $query;
        else:
            return '0';
        endif;
    }

    //Get Tender Name By ID..
    public function GetProjectNameById($userID) {
        $adminrec = $this->selectRecord('bd_tenderdetail', array('TenderDetails'), array('fld_id' => $userID));
        if ($adminrec) {
            $RecArr = $adminrec->row();
            return $RecArr->TenderDetails;
        } else {
            return false;
        }
    }

    // Coded By jitendra
    //No Submitted Project By User...
    public function GetNoSubmitProjByUser() {
        /* $selectp = $this->db->query("SELECT * FROM bd_tenderdetails WHERE visible_scope IN ('No_Submit_project','Bid_project') AND is_active='1'");
          $query = $selectp->result();
          echo '<pre>';
          print_r($query); die; */
        /* $this->db->select('*');
          $this->db->from('bd_tenderdetails bd');
          $this->db->where('bd.is_active', '1');
          $this->db->where('bd.visible_scope', 'No_Submit_project');
          $query = $this->db->get();
          if ($query->num_rows() != 0) {
          $resulstArr = $query->result_array();
          //Set Aliance..
          $recArrData = array();
          foreach ($resulstArr as $rowVal) {
          $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
          array_push($recArrData, $rowVal);
          }
          return $recArrData;
          } else {
          return false;
          } */

        $this->db->select('a.*,b.*');
        $this->db->from('bd_tenderdetail a');
        $this->db->join('bdtender_scope as b', 'a.fld_id = b.project_id', 'left');
        $this->db->where('b.visible_scope', 'No_Submit_project');

        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            /* $recArrData = array();
              foreach ($resulstArr as $rowVal) {
              $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
              array_push($recArrData, $rowVal);
              }
              return $recArrData; */
            return $resulstArr;
        } else {
            return false;
        }
    }

    //Complete List Project By User...
    public function GetcompleteProjByUser() {
        $arr = array('No_Submit_project', 'Bid_project', 'To_Go_project');
        $this->db->select('bd.*, d.generated_tenderid');
        $this->db->from('bd_tenderdetail bd');
        $this->db->join('tender_generated_id d', 'bd.fld_id=d.project_id', 'left');
        //$this->db->from('bid_project a');
        //$this->db->join('bd_tenderdetail bd', 'bd.fld_id=a.project_id', 'left');
        $this->db->where('bd.is_active', '1');
        $this->db->where_in('bd.visible_scope', $arr);
        $query = $this->db->get();

        if ($query->num_rows() != 0) {
            $resulstArr = $query->result_array();
            //Set Aliance..
            $recArrData = array();
            foreach ($resulstArr as $rowVal) {
                $rowVal['activeBy'] = $this->UserNameById($rowVal['user_id']);
                array_push($recArrData, $rowVal);
            }
            return $recArrData;
        } else {
            return false;
        }
    }

    //User Full Name By ID..
    public function BidProjectById($projectid) {
        $catnm = $this->selectRecord('bid_project', array('project_status'), array('project_id' => $projectid));
        if ($catnm) {
            $rows = $catnm->first_row();
            $res = $rows->project_status;
            return $res;
        } else {
            return false;
        }
    }

    public function bd_rolecheck() {
        $pmid = $this->session->userdata('loginid');
        $query = $this->db->query('select role_id from bd_role where user_id= ' . $pmid);
        $result = $query->result();
        if (!empty($result)) {
            return $result[0]->role_id;
        } else {
            return false;
        }
    }

    /*  public function allprojectProposalManager() {
      $query = $this->db->query('select a.*,b.userfullname from bd_role as a left join main_users as b on a.user_id = b.fld_id where a.role_id = 1');
      return $res = $query->result();
      } */

    // All active Designation Master...
    function allActiveDesignation() {
        $Rec = $this->Front_model->selectRecordOrderByASC('designation_master_requisition', array('designation_name', 'fld_id'), array('is_active' => '1'));
        if ($Rec) {
            return $result = $Rec->result();
        }
        return false;
    }


    // All active Designation Sector...
    function allActiveDesignationSector() {
        $Rec = $this->db->query("SELECT * FROM designation_sector WHERE status='1' ORDER BY sector_name ASC ");
        if ($Rec) {
            return $result = $Rec->result();
        }
        return false;
    }

    public function getCegScore($projID) {
        $query = $this->db->query('select * from cegexp_competitor_comp where project_id= ' . $projID . ' and compt_comp_id= 74');
        $result = $query->result();
        if (!empty($result)) {
            return $result[0];
        } else {
            return false;
        }
    }

    public function getProjectstatus($status, $userid) {
        $data = array();
        if ($status == 1) {
            $this->db->select('a.*, b.*');
            $this->db->from('team_assign_member a');
            $this->db->join('designation_master_requisition b', 'a.designation_id=b.fld_id', 'left');
            $this->db->join('bd_tenderdetail d', 'a.project_id=d.fld_id', 'left');
            $this->db->where('a.empname', $userid);
            $this->db->where('a.status', '1');
            $this->db->where('d.is_active', '1');
            $data['result'] = $this->db->get()->result_object();
        } else {
            if (!empty($userid)) {
                $this->db->select('a.*, b.*');
                $this->db->from('team_assign_member a');
                $this->db->join('designation_master_requisition b', 'a.designation_id=b.fld_id', 'left');
                $this->db->join('bd_tenderdetail d', 'a.project_id=d.fld_id', 'left');
                $this->db->where('a.empnameother', $userid);
                $this->db->where('a.status', '1');
                $this->db->where('d.is_active', '1');
                $data['result'] = $this->db->get()->result_object();
            }
        }
        foreach ($data['result'] as $val) {
            $projID[] = $val->project_id;
        }
        $this->db->select('SUM(IF(`project_status` = "1", 1,0)) AS `won`,SUM(IF(`project_status` = "2", 1,0)) AS `lose`,
SUM(IF(`project_status` = "3", 1,0)) AS `cancel`,
SUM(IF(`project_status` = "0", 1,0)) AS `awaiting`');
        $this->db->from('bdproject_status');
        $this->db->where_in('project_id', $projID);

        $data['status'] = $this->db->get()->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return false;
        }
    }

    public function getProjectDetailByUserID($userid) {
        //24-Code By Asheesh.. last evening
        $this->db->select('a.project_id, b.designation_name,c.project_status,d.TenderDetails');
        $this->db->from('team_assign_member a');
        $this->db->join('designation_master_requisition b', 'a.designation_id=b.fld_id', 'left');
        $this->db->join('bdproject_status c', 'a.project_id=c.project_id', 'left');
        $this->db->join('bd_tenderdetail d', 'a.project_id=d.fld_id', 'left');
        // $where = "(a.empname = '$userid' OR a.empnameother = '$userid')";
        $this->db->where('a.empname', $userid);
        $this->db->where('a.status', '1');
        $this->db->where('d.is_active', '1');
        // empnameother
        $result = $this->db->get()->result_array();
        return isset($result) ? $result : false;
    }

    public function getProjectDetailByUserIDother($userid) {
        //24-Code By Asheesh.. last evening
        $this->db->select('a.project_id, b.designation_name,c.project_status,d.TenderDetails');
        $this->db->from('team_assign_member a');
        $this->db->join('designation_master_requisition b', 'a.designation_id=b.fld_id', 'left');
        $this->db->join('bdproject_status c', 'a.project_id=c.project_id', 'left');
        $this->db->join('bd_tenderdetail d', 'a.project_id=d.fld_id', 'left');
        // $where = "(a.empname = '$userid' OR a.empnameother = '$userid')";
        $this->db->where('a.empnameother', $userid);
        $this->db->where('a.status', '1');
        $this->db->where('d.is_active', '1');
        // empnameother
        $result = $this->db->get()->result_array();
        return isset($result) ? $result : false;
    }

    //BD CRU mail List Code By Asheesh..
    public function GetBdEmailsList() {
        $emailArr = array();
        $this->db->select('*');
        $this->db->from("main_users");
        $this->db->where('is_bd_dept', '1');
        $this->db->where('isactive', '1');
        $query = $this->db->get()->result();
        if ($query):
            foreach ($query as $rowEml) {
                array_push($emailArr, $rowEml->ofcialemail);
            }
            return $emailArr;
        else:
            return false;
        endif;
    }

    //Get Active Sector Find By Jitendra
    public function GetActiveSector() {
        $this->db->select("*");
        $this->db->from("sector");
        $this->db->where('is_active', '1');
        $this->db->order_by("sectName", "asc");
        $Rec = $this->db->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function GetActiveService1() {
        $this->db->select("*");
        $this->db->from("pds_service1");
        $this->db->where('is_active', '1');
        $this->db->order_by("service_name", "asc");
        $Rec = $this->db->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function GetActiveService2() {
        $this->db->select("*");
        $this->db->from("pds_service2");
        $this->db->where('is_active', '1');
        $this->db->order_by("service_name", "asc");
        $Rec = $this->db->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    /* public function getAllProject(){
      $this->db->select('id,project_name');
      $this->db->from('tm_projects');
      $this->db->where('is_active', '1');
      $Rec = $this->db->get()->result();
      if ($Rec) {
      return $Rec;
      } else {
      return false;
      }
      } */

    public function getAllProject() {
        $proj_categ = array('ae', 'IE', 'IE_O&M', 'pmc');
        $this->db2->select('id,project_name');
        $this->db2->from('tm_projects');
        $this->db2->where('is_active', '1');
        $this->db2->where_in('project_category', $proj_categ);
        $this->db2->order_by('project_name');
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function mainServices() {
        $this->db->select('*');
        $this->db->from('main_services');
        $this->db->where('is_active', '1');
        $res = $this->db->get()->result_object();
        return isset($res) ? $res : false;
    }

    public function getProjectDetails($id) {
        $this->db->select('*');
        $this->db->from('project_description');
        $this->db->where('project_id', $id);
        $this->db->where('is_active', '1');
        $res = $this->db->get()->result_object();
        return isset($res) ? $res[0] : false;
    }

    public function getTeamassign() {
        $this->db->select('a.empname,a.empnameother,b.userfullname,c.emp_name');
        $this->db->from('team_assign_member as a');
        $this->db->join('main_users as b', 'a.empname = b.fld_id', 'left');
        $this->db->join('team_req_otheremp as c', 'a.empnameother = c.fld_id', 'left');
        $this->db->where('a.status', '1');
        $res = $this->db->get()->result_object();
        if (!empty($res)) {
            $userdata = array();
            foreach ($res as $val) {
                if (!empty($val->empname)) {
                    $userdata[] = array('id' => $val->empname, 'name' => $val->userfullname);
                } else if (!empty($val->empnameother)) {
                    $userdata[] = array('id' => $val->empnameother, 'name' => $val->emp_name);
                }
            }
        }
        return isset($userdata) ? $userdata : false;
    }

    public function getAlldesignation() {
        $this->db->select('fld_id,designation_name');
        $this->db->from('designation_master_requisition');
        $this->db->where('is_active', '1');
        $this->db->order_by('designation_name');
        $res = $this->db->get()->result_object();
        return isset($res) ? $res : false;
    }

    public function getAllemployees() {
        $this->db2->select('*');
        $this->db2->from('main_users');
        $this->db2->where('isactive', '1');
        $this->db2->order_by('userfullname');
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function getuserdetailByID($id) {
        $this->db2->select('*');
        $this->db2->from('main_users');
        $this->db2->where('isactive', '1');
        $this->db2->where('id', $id);
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function getuserdetailByprojectID($id) {
        $this->db2->select('b.userfullname,b.emailaddress,b.contactnumber,b.id,c.position_id,c.position_name');
        $this->db2->from('emp_otherofficial_data as a');
        $this->db2->join('main_users as b', 'a.user_id = b.id', 'left');
        $this->db2->join('main_employees_summary as c', 'a.user_id = c.user_id', 'left');
        $this->db2->where('a.status', '1');
        $this->db2->where('b.isactive', '1');
        $this->db2->where('on_project', $id);
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function GetActiveKeyword() {
        $this->db->select("*");
        $this->db->from("bd_keyword");
        $this->db->where('status', '1');
        $this->db->order_by("keywordname", "asc");
        $Rec = $this->db->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function getsideMenu() {
        $this->db->select("*");
        $this->db->from("menu_table");
        $this->db->where('is_active', '1');
        $this->db->where('parent_id', '0');
        $this->db->where('menu_type', '1');
        $this->db->order_by("menuposition", "asc");
        $Rec = $this->db->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function gettopMenu() {
        $this->db->select("*");
        $this->db->from("menu_table");
        $this->db->where('is_active', '1');
        $this->db->where('parent_id', '0');
        $this->db->where('menu_type', '2');
        $this->db->order_by("menuposition", "asc");
        $Rec = $this->db->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function getsubMenu($id) {
        $this->db->select("id,menuname");
        $this->db->from("menu_table");
        $this->db->where('is_active', '1');
        $this->db->where('parent_id', $id);
        $this->db->order_by("submenuposition", "asc");
        $Rec = $this->db->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function getsideMenubyrole($roleid) {
        $this->db->select("a.*,b.menuname,b.menulink,b.icon,b.collapseid");
        $this->db->from("role_permission as a");
        $this->db->join("menu_table as b", "a.menu_id = b.id", 'left');
        $this->db->where('a.active', '1');
        $this->db->where('b.is_active', '1');
        $this->db->where('b.menu_type', '1');
        $this->db->where('a.role_id', $roleid);
        $this->db->order_by('b.menuposition', 'asc');
        $Rec = $this->db->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function getsidesubmenu($menuid, $roleid) {
        $this->db->select("b.*,a.menuname,a.menulink,a.icon,a.collapseid");
        $this->db->from("menu_table as a");
        $this->db->join("role_permission as b", "a.id = b.submenu_id", 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('b.active', '1');
        $this->db->where('a.menu_type', '1');
        $this->db->where('a.parent_id', $menuid);
        $this->db->where('b.role_id', $roleid);
        $this->db->order_by('a.submenuposition', 'asc');
        $Rec = $this->db->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function gettopMenubyrole($roleid) {
        $this->db->select("a.*,b.menuname,b.menulink,b.icon,b.collapseid");
        $this->db->from("role_permission as a");
        $this->db->join("menu_table as b", "a.menu_id = b.id", 'left');
        $this->db->where('a.active', '1');
        $this->db->where('b.is_active', '1');
        $this->db->where('b.menu_type', '2');
        $this->db->where('a.role_id', $roleid);
        $this->db->order_by('b.menuposition', 'asc');
        $Rec = $this->db->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function gettopsubmenu($menuid, $roleid) {
        $this->db->select("b.*,a.menuname,a.menulink,a.icon,a.collapseid");
        $this->db->from("menu_table as a");
        $this->db->join("role_permission as b", "a.id = b.submenu_id", 'left');
        $this->db->where('a.is_active', '1');
        $this->db->where('b.active', '1');
        $this->db->where('a.menu_type', '2');
        $this->db->where('a.parent_id', $menuid);
        $this->db->where('b.role_id', $roleid);
        $this->db->order_by('a.submenuposition', 'asc');
        $Rec = $this->db->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function getProjectDetail($id) {
        if ($id) {
            $businessID = $this->session->userdata('businessunit_id');
            $this->db->select('a.*,b.project_id,b.visible_scope,b.businessunit_id');
            $this->db->from('bd_tenderdetail as a');
            $this->db->join('bdtender_scope as b', 'a.fld_id = b.project_id', 'left');
            $this->db->where('a.fld_id', $id);
            $this->db->where('b.businessunit_id', $businessID);
            $Rec = $this->db->get()->result();
            if ($Rec[0]) {
                return $Rec[0];
            } else {
                return false;
            }
        }
    }

    public function allprojectProposalManager() {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;
        $this->db->select("$db2.main_users.id,$db2.main_users.userfullname");
        $this->db->from("$db1.bd_role");
        $this->db->join("$db2.main_users", "$db1.bd_role.user_id = $db2.main_users.id", "left");
        $this->db->where("$db2.main_users.isactive", "1");
        $this->db->where("$db1.bd_role.role_id", "1");
        //$query = $this->db->query('select a.*,b.userfullname from bd_role as a left join main_users as b on a.user_id = b.fld_id where a.role_id = 1');
        // return $res = $query->result();
        $Rec = $this->db->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function ReminderEmail() {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;
        $emailArr = array();
        $this->db->select("$db2.main_users.ofcialemail");
        $this->db->from("$db1.bd_role");
        $this->db->join("$db2.main_users", "$db1.bd_role.user_id = $db2.main_users.id", "left");
        $this->db->where("$db2.main_users.isactive", "1");
        $arr = array('1', '3');
        $this->db->where_in("$db1.bd_role.role_id", $arr);

        $Rec = $this->db->get()->result();
        if ($Rec) {
            foreach ($Rec as $rowEml) {
                array_push($emailArr, $rowEml->ofcialemail);
            }
            return $emailArr;
        } else {
            return false;
        }
    }

    public function getteambyprojectid($projid) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;


        /* $this->db->select("$db2.main_users.id,$db2.main_users.userfullname");
          $this->db->from("$db1.bd_role");
          $this->db->join("$db2.main_users","$db1.bd_role.user_id = $db2.main_users.id","left");
          $this->db->where("$db2.main_users.isactive","1");
          $this->db->where("$db1.bd_role.role_id","1");
          //$query = $this->db->query('select a.*,b.userfullname from bd_role as a left join main_users as b on a.user_id = b.fld_id where a.role_id = 1');
          // return $res = $query->result();
          $Rec = $this->db->get()->result();
          if ($Rec) {
          return $Rec;
          } else {
          return false;
          } */


        $this->db->select("$db1.a.project_id,$db1.a.marks,$db1.a.firm,$db2.b.userfullname,$db2.b.emailaddress,$db2.b.contactnumber,$db1.c.emp_name,$db1.c.emp_email,$db1.c.emp_contact,$db1.d.designation_name");
        $this->db->from("$db1.assign_finalteam as a");
        $this->db->join("$db2.main_users as b", "$db1.a.empname = $db2.b.id", "left");
        $this->db->join("$db1.team_req_otheremp as c", "$db1.a.empnameother = $db1.c.fld_id", "left");
        $this->db->join("$db1.designation_master_requisition as d", "$db1.a.designation_id = $db1.d.fld_id", "left");
        $this->db->where("$db1.a.status", '1');
        $this->db->where("$db1.a.project_id", $projid);
        $res = $this->db->get()->result_array();
        if ($res) {
            return $res;
        } else {
            return false;
        }
    }

    public function getprojectbyempid($userID, $empstatus) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        if ($empstatus == 1) {
            $this->db->select("$db2.d.userfullname");
            $this->db->join("$db2.main_users as d", "$db1.a.empname = $db2.d.id", "left");
            $this->db->where("$db1.a.empname", $userID);
        } else {
            $this->db->select("$db1.e.emp_name,$db1.a.empnameother");
            $this->db->join("$db1.team_req_otheremp as e", "$db1.a.empnameother = $db1.e.fld_id", "left");
            $this->db->where("$db1.a.empnameother", $userID);
        }

        $this->db->select("$db1.b.TenderDetails,$db1.c.designation_name,$db1.a.marks,$db1.f.project_status,$db1.f.project_id,$db1.a.man_months,$db1.a.balancemm");
        $this->db->from("$db1.assign_finalteam as a");
        $this->db->join("$db1.bd_tenderdetail as b", "$db1.a.project_id = $db1.b.fld_id", "left");
        $this->db->join("$db1.designation_master_requisition as c", "$db1.a.designation_id = $db1.c.fld_id", "left");
        $this->db->join("$db1.bdproject_status as f", "$db1.a.project_id = $db1.f.project_id", "left");
        $this->db->where("$db1.a.status", '1');
        $res = $this->db->get()->result_array();
        if ($res) {
            return $res;
        } else {
            return false;
        }
    }

    public function getuserinfoByprojectID($id) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $data = array();

        $this->db->select("$db2.c.userfullname,$db1.b.PositionName,$db1.b.id,$db1.b.designation_id,$db1.b.key_id");
        $this->db->from("$db1.bdcegexp_proj_summery as a");
        $this->db->join("$db1.assign_finalteam as b", "$db1.a.project_id = $db1.b.project_id", "left");
        $this->db->join("$db2.main_users as c", "$db1.b.empname = $db2.c.id", "right");
        $this->db->where("$db1.a.project_numberid", $id);
        $Rec = $this->db->get()->result();


        $this->db->select("$db1.a.sector_name,$db1.b.designation_name,$db1.b.fld_id");
        $this->db->from("$db1.designation_sector as a");
        $this->db->join("$db1.designation_master_requisition as b", "$db1.a.id = $db1.b.sector_id", "right");
        $this->db->where("$db1.a.status", '1');
        $Rec1 = $this->db->get()->result();


        $this->db->select("$db1.b.TenderDetails");
        $this->db->from("$db1.bdcegexp_proj_summery as a");
        $this->db->join("$db1.bd_tenderdetail as b", "$db1.a.project_id = $db1.b.fld_id", "left");
        $this->db->where("$db1.a.project_numberid", $id);
        $Rec2 = $this->db->get()->result();


        $data['users'] = $Rec;
        $data['designation'] = $Rec1;
        $data['project'] = $Rec2;

        if ($data) {
            return $data;
        } else {
            return false;
        }
    }

    public function getprojectdetailByProjectId($projectID) {
        $this->db->select('fld_id,TenderDetails,Expiry_Date');
        $this->db->from('bd_tenderdetail');
        $this->db->where('fld_id', $projectID);
        $result = $this->db->get()->result();


        $this->db->select('*');
        $this->db->from('comment_on_project');
        $this->db->where('project_id', $projectID);
        $this->db->where('is_active', '1');
        $result1 = $this->db->get()->result();


        $this->db->select('*');
        $this->db->from('project_concern_details');
        $this->db->where('project_id', $projectID);
        $this->db->where('status', '1');
        $result2 = $this->db->get()->result();


        $this->db->select('*');
        $this->db->from('projectdocument');
        $this->db->where('project_id', $projectID);
        $this->db->where('status', '1');
        $result3 = $this->db->get()->result();


        $this->db->select('a.project_id as projectid,a.user_id as activeuser,a.action_date as activedate,a.actionIP as activeip,
		b.user_id as impuser,b.action_date as impdate,b.actionIP as impip,
		c.user_id as notimpuser,c.action_date as notimpdate,c.actionIP as notimpip,
		d.user_id as reviewuser,d.action_date as reviewdate,d.actionIP as reviewip,
		e.user_id as nogouser,e.action_date as nogodate,e.actionIP as nogoip,
		f.user_id as inactiveuser,f.action_date as inactivedate,f.actionIP as inactiveip,
		g.user_id as biduser,g.action_date as biddate,g.actionIP as bidip,
		h.user_id as notsubmituser,h.action_date as notsubmitdate,h.actionIP as notsubmitip,
		k.user_id as togouser,k.action_date as togodate,k.actionIP as togoip,
		i.created_By as detailuser,i.Created_Date as detaildate
		');
        $this->db->from('bdactive_project_byuser as a');
        $this->db->join('bdimportant_project_byuser as b', 'a.project_id = b.project_id', 'left');
        $this->db->join('bdnotimp_project_byuser as c', 'a.project_id = c.project_id', 'left');
        $this->db->join('bdinreview_project_byuser as d', 'a.project_id = d.project_id', 'left');
        $this->db->join('bdnogo_project_byuser as e', 'a.project_id = e.project_id', 'left');
        $this->db->join('bdinactive_project_byuser as f', 'a.project_id = f.project_id', 'left');
        $this->db->join('bdbid_project_byuser as g', 'a.project_id = g.project_id', 'left');
        $this->db->join('bdnotsubmit_project_byuser as h', 'a.project_id = h.project_id', 'left');
        $this->db->join('bd_tenderdetail as i', 'a.project_id = i.fld_id', 'left');
        //$this->db->join('bdtenderdetails_dump as j','a.project_id = j.parent_tbl_id','left');
        $this->db->join('bdtogo_project_byuser as k', 'a.project_id = k.project_id', 'left');

        $this->db->where('a.project_id', $projectID);
        $result4 = $this->db->get()->result();

        $this->db->select('a.bid_security,a.tender_openingdate,a.amount,b.service_name,c.sectName,d.state_name,e.security_type');
        $this->db->from('project_description as a');
        $this->db->join('main_services as b', 'b.id = a.service_id', 'left');
        $this->db->join('sector as c', 'a.sector_id = c.fld_id', 'left');
        $this->db->join('states as d', 'a.state_id = d.state_id', 'left');
        $this->db->join('bidsecurity_type as e', 'a.bid_securitytype = e.id', 'left');
        $this->db->where('a.project_id', $projectID);

        $result5 = $this->db->get()->result();




        $data['tender'] = $result;
        $data['comment'] = $result1;
        $data['description'] = $result2;
        $data['document'] = $result3;
        $data['status'] = $result4;
        $data['info'] = $result5;

        if ($data) {
            return $data;
        } else {
            return false;
        }
    }

    public function getUserprojectExcel() {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        //$this->db->select("$db2.tm_projects.project_name");
        $this->db->select("$db2.tm_projects.project_name,$db1.assign_finalteam.project_id,$db1.assign_finalteam.man_months,$db1.bd_tenderdetail.TenderDetails,$db1.assign_finalteam.marks,$db1.assign_finalteam.firm,$db1.assign_finalteam.balancemm,$db2.main_users.employeeId,$db2.main_users.userfullname,$db2.main_users.emailaddress,$db2.main_users.contactnumber,$db1.assign_finalteam.PositionName,$db2.main_users.id,$db2.main_emppersonaldetails.aadhar_no_enrolment");
        $this->db->from("$db2.tm_projects");
        $this->db->join("$db1.bdcegexp_proj_summery", "$db2.tm_projects.id = $db1.bdcegexp_proj_summery.project_numberid", 'left');
        $this->db->join("$db1.bd_tenderdetail", "$db1.bdcegexp_proj_summery.project_id = $db1.bd_tenderdetail.fld_id", 'left');
        $this->db->join("$db1.assign_finalteam", "$db1.bdcegexp_proj_summery.project_id = $db1.assign_finalteam.project_id", 'right');
        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id = $db1.designation_master_requisition.fld_id", "left");
        $this->db->join("$db2.main_users", "$db2.main_users.id = $db1.assign_finalteam.empname", 'right');
        $this->db->join("$db2.main_emppersonaldetails", "$db2.main_emppersonaldetails.user_id = $db2.main_users.id", 'left');
        $this->db->where("$db2.tm_projects.is_active", '1');

        $result = $this->db->get()->result();
        return isset($result) ? $result : false;
    }

    public function getAssignuserinproject($projectID, $key_id) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        //$this->db->select("$db1.assign_finalteam.*,$db2.main_employees_summary.date_of_joining,$db2.main_employees_summary.userfullname");
        $this->db->select("$db1.assign_finalteam.srno,$db1.assign_finalteam.cumulative_pre_mm,$db1.assign_finalteam.cumulative_pre_amount,$db1.assign_finalteam.replacement_reducationrate,$db1.assign_finalteam.account_lock,$db1.assign_finalteam.man_months,$db1.assign_finalteam.balancemm,$db1.assign_finalteam.rate,$db1.assign_finalteam.id,$db1.assign_finalteam.designation_id,$db1.assign_finalteam.empname,$db2.main_employees_summary.date_of_joining,$db2.main_employees_summary.userfullname,$db1.designation_master_requisition.designation_name,$db1.assign_finalteam.maintenance_months,$db1.assign_finalteam.construction_months,$db1.assign_finalteam.maintenance_rate,$db1.assign_finalteam.construction_rate");
        $this->db->from("$db1.assign_finalteam");
        $this->db->join("$db2.main_employees_summary", "$db2.main_employees_summary.user_id = $db1.assign_finalteam.empname", 'left');
        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id = $db1.designation_master_requisition.fld_id", 'left');

        $this->db->where("$db1.assign_finalteam.project_id", $projectID);
        $this->db->where("$db1.assign_finalteam.key_id", $key_id);
        $this->db->order_by("$db1.assign_finalteam.srno", "asc");

        $result = $this->db->get()->result();
        return isset($result) ? $result : false;
    }

    public function geteotemployee($projectID, $eotid, $key_id) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        //$this->db->select("$db1.assign_finalteam.*,$db2.main_employees_summary.date_of_joining,$db2.main_employees_summary.userfullname");
        $this->db->select("$db1.saveeotteam.id,$db1.saveeotteam.designation_id,$db1.saveeotteam.emp_id,sum($db1.saveeotteam.eot_mm) as eot_mm,$db1.assign_finalteam.rate,$db1.assign_finalteam.man_months,$db2.main_employees_summary.userfullname,$db1.designation_master_requisition.designation_name");
        $this->db->from("$db1.saveeotteam");
        $this->db->join("$db2.main_employees_summary", "$db2.main_employees_summary.user_id = $db1.saveeotteam.emp_id", 'left');
        $this->db->join("$db1.designation_master_requisition", "$db1.saveeotteam.designation_id = $db1.designation_master_requisition.fld_id", 'left');
        $this->db->join("$db1.assign_finalteam", "$db1.saveeotteam.assign_id = $db1.assign_finalteam.id", 'left');

        $this->db->where("$db1.saveeotteam.project_id", $projectID);
        //$this->db->where("$db1.saveeotteam.eot_id",$eotid);
        $this->db->where("$db1.assign_finalteam.key_id", $key_id);
        $this->db->group_by("$db1.saveeotteam.emp_id");
        $this->db->group_by("$db1.saveeotteam.designation_id");
        $this->db->order_by("$db1.assign_finalteam.srno", "asc");

        $result = $this->db->get()->result();
        return isset($result) ? $result : false;
    }

    public function getcurrenteotemployee($projectID, $eotid, $designation_id) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        $this->db->select("sum($db1.saveeotteam.eot_mm) as eot_mm ");
        $this->db->from("$db1.saveeotteam");
        $this->db->where("$db1.saveeotteam.project_id", $projectID);
        //$this->db->where("$db1.saveeotteam.eot_id",$eotid);
        $this->db->where("$db1.saveeotteam.designation_id", $designation_id);
        $this->db->group_by("$db1.saveeotteam.emp_id");
        $this->db->group_by("$db1.saveeotteam.designation_id");
        $result = $this->db->get()->result();
        return isset($result) ? $result : false;
    }

    public function getemployeename($id) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        $this->db->select("$db2.main_employees_summary.userfullname");
        $this->db->from("$db1.assign_finalteam");
        $this->db->join("$db2.main_employees_summary", "$db1.assign_finalteam.empname = $db2.main_employees_summary.user_id", 'left');
        $this->db->where("$db1.assign_finalteam.id", $id);

        $result = $this->db->get()->result();
        return isset($result) ? $result : false;
    }

    /* function getreplacemendata($id){
      $db2 = $this->db2->database;
      $db1 = $this->db1->database;
      $data = array();

      $this->db->select("$db2.main_employees_summary.userfullname");
      $this->db->from("$db1.replacementteam");
      $this->db->join("$db2.main_employees_summary","$db1.replacementteam.replacement_userid = $db2.main_employees_summary.user_id",'left');
      $this->db->where("$db1.replacementteam.replacement_id",$id);
      $this->db->order_by("$db1.replacementteam.id",'desc');
      $result = $this->db->get()->result();

      $this->db->select("count($db1.replacementteam.replacement_id) as usercount");
      $this->db->from("$db1.replacementteam");
      $this->db->where("$db1.replacementteam.replacement_id",$id);
      $countresult = $this->db->get()->result();


      $username =  isset($result) ? $result : false;
      $countresult =  isset($countresult[0]) ? $countresult[0] : false;
      $data['username'] = $username;
      $data['countresult'] = $countresult;
      return $data;
      } */

    public function getexcelpreviousmonth($projectID, $invoiceID, $designaitioID) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        $this->db->select("$db1.acc_monthdetail.cumulative_pre_mm,$db1.acc_monthdetail.cumulative_pre_amount");
        $this->db->from("$db1.acc_monthdate");
        $this->db->join("$db1.acc_monthdetail", "$db1.acc_monthdate.id = $db1.acc_monthdetail.month_id", 'left');

        $previousmonth = date("Y-m-d", strtotime("first day of previous month"));
        $lastpreviousmonth = date("Y-m-d", strtotime("last day of previous month"));

        $this->db->where("$db1.acc_monthdate.project_id", $projectID);
        $this->db->where("$db1.acc_monthdate.invoice_id", $invoiceID);

        $where_date = "($db1.acc_monthdate.date_month >='$previousmonth' AND $db1.acc_monthdate.date_month <= '$lastpreviousmonth')";
        $this->db->where($where_date);
        $this->db->where("$db1.acc_monthdetail.designation_id", $designaitioID);

        $result = $this->db->get()->result();
        return isset($result[0]) ? $result[0] : false;
    }

    public function getpreviousmonth($projectID, $key_id) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        $this->db->select("$db1.assign_finalteam.empname,$db1.assign_finalteam.cumulative_pre_mm,$db1.assign_finalteam.cumulative_pre_amount,$db1.assign_finalteam.rate,$db2.main_employees_summary.userfullname,$db1.designation_master_requisition.designation_name");
        $this->db->from("$db1.assign_finalteam");
        $this->db->join("$db2.main_employees_summary", "$db2.main_employees_summary.user_id = $db1.assign_finalteam.empname", 'left');
        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id = $db1.designation_master_requisition.fld_id", 'left');
        $this->db->where("$db1.assign_finalteam.project_id", $projectID);
        $this->db->where("$db1.assign_finalteam.key_id", $key_id);
        $this->db->order_by("$db1.assign_finalteam.srno", "asc");
        $result = $this->db->get()->result();
        return isset($result) ? $result : false;
    }

    public function getotherpreviousmonth($projectID, $invoiceID, $masterdetail_id, $invoicedate) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        $this->db->select("$db1.acc_othermonthdate.id,$db1.acc_othermonthdate.date_month,$db1.acc_othermonthdetail.other_mm,$db1.acc_othermonthdetail.cumulative_pre_mm,$db1.acc_othermonthdetail.cumulative_pre_amount");
        $this->db->from("$db1.acc_othermonthdate");
        $this->db->join("$db1.acc_othermonthdetail", "$db1.acc_othermonthdate.id = $db1.acc_othermonthdetail.month_id", 'left');

        $previousmonth = date("Y-m-d", strtotime($invoicedate . "first day of -2 month"));
        $lastpreviousmonth = date("Y-m-d", strtotime($invoicedate . "last day of -2 month"));

        $this->db->where("$db1.acc_othermonthdate.project_id", $projectID);
        //$this->db->where("$db1.acc_othermonthdate.invoice_id",$invoiceID);

        $where_date = "($db1.acc_othermonthdate.date_month >='$previousmonth' AND $db1.acc_othermonthdate.date_month <= '$lastpreviousmonth')";
        $this->db->where($where_date);
        $this->db->where("$db1.acc_othermonthdetail.masterdetail_id", $masterdetail_id);

        $result = $this->db->get()->result();
        return isset($result) ? $result : false;
    }

    public function getothercurrentmonth($projectID, $invoiceID, $masterdetail_id, $invoicedate) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;
       // $masterdetail_id = '302';
        
        $this->db->select("$db1.acc_othermonthdate.id,$db1.acc_othermonthdate.date_month,$db1.acc_othermonthdetail.other_mm,$db1.acc_othermonthdetail.cumulative_pre_mm,$db1.acc_othermonthdetail.cumulative_pre_amount");
        $this->db->from("$db1.acc_othermonthdate");
        $this->db->join("$db1.acc_othermonthdetail", "$db1.acc_othermonthdate.id = $db1.acc_othermonthdetail.month_id", 'left');
        $previousmonth = date("Y-m-d", strtotime($invoicedate . "first day of previous month"));
        $lastpreviousmonth = date("Y-m-d", strtotime($invoicedate . "last day of previous month"));
        $this->db->where("$db1.acc_othermonthdate.project_id", $projectID);
        //$this->db->where("$db1.acc_othermonthdate.invoice_id",$invoiceID);
        $where_date = "($db1.acc_othermonthdate.date_month >='$previousmonth' AND $db1.acc_othermonthdate.date_month <= '$lastpreviousmonth')";
        $this->db->where($where_date);
        $this->db->where("$db1.acc_othermonthdetail.masterdetail_id", $masterdetail_id);
        $result = $this->db->get()->result();
        return isset($result) ? $result : false;
    }

    
    public function getaccountdetailforexcel($invoiceID) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;
        $this->db->select("$db1.accountinfo.*,$db1.invoicedetail.invoice_name,$db1.invoicedetail.invoice_no as invoiceno,$db1.invoicedetail.ra_invoice,$db1.invoicedetail.invoice_date,$db1.bd_tenderdetail.TenderDetails");
        $this->db->from("$db1.invoicedetail");
        $this->db->join("$db1.accountinfo", "$db1.invoicedetail.infoid = $db1.accountinfo.id", 'left');
        $this->db->join("$db1.bd_tenderdetail", "$db1.accountinfo.project_id = $db1.bd_tenderdetail.fld_id", 'left');
        $this->db->where("$db1.invoicedetail.id", $invoiceID);
        $result = $this->db->get()->result();
        return isset($result) ? $result[0] : false;
    }

    public function getexceldataproject($projectID, $projectno_id, $invoiceno_id, $key_id) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        $this->db->select("$db1.invoicesave.*,$db2.main_employees_summary.date_of_joining as dob,$db2.main_employees_summary.userfullname as username,$db1.designation_master_requisition.designation_name as designation");
        $this->db->from("$db1.invoicesave");
        $this->db->join("$db2.main_employees_summary", "$db2.main_employees_summary.user_id = $db1.invoicesave.emp_id", 'left');
        $this->db->join("$db1.designation_master_requisition", "$db1.invoicesave.designation_id = $db1.designation_master_requisition.fld_id", 'left');
        //$this->db->join("$db1.assign_finalteam","$db1.invoicesave.designation_id = $db1.assign_finalteam.designation_id",'left');

        $this->db->where("$db1.invoicesave.project_id", $projectID);
        $this->db->where("$db1.invoicesave.projectno_id", $projectno_id);
        $this->db->where("$db1.invoicesave.invoiceno_id", $invoiceno_id);
        $this->db->where("$db1.invoicesave.key_id", $key_id);
        $this->db->order_by("$db1.invoicesave.srno", "asc");

        $result = $this->db->get()->result();
        return isset($result) ? $result : false;
    }

    public function getproject() {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db2.tm_projects.project_name,$db1.bdcegexp_proj_summery.project_id");
        $this->db->from("$db2.tm_projects");
        $this->db->join("$db1.bdcegexp_proj_summery", "$db2.tm_projects.id = $db1.bdcegexp_proj_summery.project_numberid", 'right');
        $this->db->where("$db2.tm_projects.is_active", '1');
        $result = $this->db->get()->result();
        return isset($result) ? $result : false;
    }

    public function getprojectname($project_id) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db2.tm_projects.project_name,$db2.tm_projects.daycount");
        $this->db->from("$db1.bdcegexp_proj_summery");
        $this->db->join("$db2.tm_projects", "$db1.bdcegexp_proj_summery.project_numberid = $db2.tm_projects.id", 'left');
        $this->db->where("$db1.bdcegexp_proj_summery.project_id", $project_id);
        $this->db->where("$db2.tm_projects.is_active", '1');

        $result = $this->db->get()->result();
        return isset($result) ? $result : false;
    }

    public function getemployee($project_id, $keyid) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.assign_finalteam.id,$db1.assign_finalteam.srno,$db1.assign_finalteam.designation_id,$db1.assign_finalteam.empname,$db1.assign_finalteam.project_id,$db2.main_employees_summary.userfullname,$db1.designation_master_requisition.designation_name,$db1.assign_finalteam.replacement_effacted_date");
        $this->db->from("$db1.assign_finalteam");
        $this->db->join("$db2.main_employees_summary", "$db1.assign_finalteam.empname = $db2.main_employees_summary.user_id", 'left');
        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id = $db1.designation_master_requisition.fld_id", 'left');
        $this->db->where("$db1.assign_finalteam.project_id", $project_id);
        $this->db->where("$db1.assign_finalteam.key_id", $keyid);
        $this->db->order_by("$db1.assign_finalteam.srno", 'asc');
        $result = $this->db->get()->result();
        return isset($result) ? $result : false;
    }

    public function getdesignationdata($projectID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.assign_finalteam.id,$db1.assign_finalteam.designation_id,$db1.designation_master_requisition.designation_name");
        $this->db->from("$db1.assign_finalteam");
        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id = $db1.designation_master_requisition.fld_id", 'right');
        $this->db->where("$db1.assign_finalteam.project_id", $projectID);
        $result = $this->db->get()->result();
        return isset($result) ? $result : false;
    }

    public function getmonthattdence($month, $year, $project_id, $empid, $designation_id) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.timeshet_fill.*");
        $this->db->from("$db1.timeshet_fill");
        //Code Comment By Asheesh For Replacement bug fix.. 29-11-2019
        //  $this->db->where("$db1.timeshet_fill.emp_id", $empid);
        //24-04-2019 code for harish
        $this->db->where("$db1.timeshet_fill.designation_id", $designation_id);
        $this->db->where("$db1.timeshet_fill.project_id", $project_id);
        $this->db->where("$db1.timeshet_fill.year", $year);
        $this->db->where("$db1.timeshet_fill.month", $month);
        $this->db->where("$db1.timeshet_fill.is_active", "1");

        $recArr = $this->db->get()->result_array();
        return isset($recArr[0]) ? $recArr[0] : false;
    }

    public function getemptimesheet($emp_id, $designation_id, $project_id, $selectdate) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $year = date('Y', strtotime($selectdate));
        $month = date('n', strtotime($selectdate));
        $this->db->select("$db1.timeshet_fill.*");
        $this->db->from("$db1.timeshet_fill");
        $this->db->where("$db1.timeshet_fill.emp_id", $emp_id);
        $this->db->where("$db1.timeshet_fill.designation_id", $designation_id);
        $this->db->where("$db1.timeshet_fill.project_id", $project_id);
        $this->db->where("$db1.timeshet_fill.year", $year);
        $this->db->where("$db1.timeshet_fill.month", $month);
        $this->db->where("$db1.timeshet_fill.is_active", "1");

        $recArr = $this->db->get()->result_array();
        return isset($recArr[0]) ? $recArr[0] : false;
    }

    public function saveinfotime($Arr) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        if ($Arr) {
            $year = date('Y', strtotime($Arr['date_month']));
            $month = date('n', strtotime($Arr['date_month']));
            $dates = date('j', strtotime($Arr['date_month']));
            $length = count($Arr['emp_id']);
            for ($i = 0; $i < $length; $i++) {
                $Where = array("$db1.timeshet_fill.project_id" => $Arr['project_id'], "$db1.timeshet_fill.designation_id" => $Arr['designation_id'][$i], "$db1.timeshet_fill.emp_id" => $Arr['emp_id'][$i], "$db1.timeshet_fill.year" => $year, "$db1.timeshet_fill.month" => $month);
                $data = $this->mastermodel->count_rows("$db1.timeshet_fill", $Where);
                if ($data < 1) {
                    $arr = array(
                        "$db1.timeshet_fill.emp_id" => $Arr['emp_id'][$i],
                        "$db1.timeshet_fill.designation_id" => $Arr['designation_id'][$i],
                        "$db1.timeshet_fill.project_id" => $Arr['project_id'],
                        "$db1.timeshet_fill.fill_date" => $Arr['date_month'],
                        "$db1.timeshet_fill.year" => $year,
                        "$db1.timeshet_fill.month" => $month,
                        "$db1.timeshet_fill.$dates" => $Arr['attend'][$i],
                        "$db1.timeshet_fill.entryby" => $this->session->userdata('loginid'),
                        "$db1.timeshet_fill.created" => date('Y-m-d'),
                        "$db1.timeshet_fill.modified" => date('Y-m-d')
                    );
                    $res = $this->mastermodel->InsertMasterData($arr, "$db1.timeshet_fill");
                } else {
                    $arr = array(
                        "$db1.timeshet_fill.$dates" => $Arr['attend'][$i],
                        "$db1.timeshet_fill.entryby" => $this->session->userdata('loginid'),
                        "$db1.timeshet_fill.modified" => date('Y-m-d')
                    );
                    $Wheres = array("$db1.timeshet_fill.project_id" => $Arr['project_id'], "$db1.timeshet_fill.designation_id" => $Arr['designation_id'][$i], "$db1.timeshet_fill.emp_id" => $Arr['emp_id'][$i], "$db1.timeshet_fill.year" => $year, "$db1.timeshet_fill.month" => $month, "$db1.timeshet_fill.is_active" => "1");
                    $res = $this->mastermodel->UpdateRecords("$db1.timeshet_fill", $Wheres, $arr);
                }
            }
        }
        return $res;
    }

    public function getCumulativemm($projectID, $designation_id) {
        $this->db->select('project_numberid');
        $this->db->from('bdcegexp_proj_summery');
        $this->db->where('project_id', $projectID);
        $resprojID = $this->db->get()->row_array();
        if (!empty($resprojID['project_numberid'])) {
            $this->db->select('invoice_no');
            $this->db->from('invoicedetail');
            $this->db->where('project_numberid', $resprojID['project_numberid']);
            $this->db->order_by('id', 'desc');
            $resinvoicedata = $this->db->get()->row_array();
        }
        if (!empty($resinvoicedata)) {
            $wheredata = array(
                'project_id' => $projectID,
                'projectno_id' => $resprojID['project_numberid'],
                'invoiceno_id' => $resinvoicedata['invoice_no'],
                'designation_id' => $designation_id,
            );

            $this->db->select('totalcumulativemm');
            $this->db->from('invoicesave');
            $this->db->where($wheredata);
            $invoicecum = $this->db->get()->row_array();
        }
        return isset($invoicecum) ? $invoicecum : '';
    }

    public function getintermittent($projectID, $empID) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.bdcegexp_proj_summery.project_numberid");
        $this->db->from("$db1.bdcegexp_proj_summery");
        $this->db->where("$db1.bdcegexp_proj_summery.project_id", $projectID);
        $projectdata = $this->db->get()->row_array();

        $this->db->select("$db2.tm_project_employees.id");
        $this->db->from("$db2.tm_project_employees");
        $this->db->where("$db2.tm_project_employees.is_active", '1');
        $this->db->where("$db2.tm_project_employees.is_intermittent", '1');
        $this->db->where("$db2.tm_project_employees.project_id", $projectdata['project_numberid']);
        $this->db->where("$db2.tm_project_employees.emp_id", $empID);

        $result = $this->db->get()->row_array();
        return isset($result) ? $result : false;
    }

    public function getintermittentattd($project_id, $empid, $designation_id, $month, $year) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.timeshet_fill.emp_id , SUM(IF($db1.timeshet_fill.1 = 'p', 1,0) + IF($db1.timeshet_fill.2 = 'p', 1,0)
				+ IF($db1.timeshet_fill.3 = 'p', 1,0) + IF($db1.timeshet_fill.4 = 'p', 1,0) + IF($db1.timeshet_fill.5 = 'p', 1,0)
				+ IF($db1.timeshet_fill.6 = 'p', 1,0) + IF($db1.timeshet_fill.7 = 'p', 1,0) + IF($db1.timeshet_fill.8 = 'p', 1,0)
				+ IF($db1.timeshet_fill.9 = 'p', 1,0) + IF($db1.timeshet_fill.10 = 'p', 1,0) + IF($db1.timeshet_fill.11 = 'p', 1,0)
				+ IF($db1.timeshet_fill.12 = 'p', 1,0) + IF($db1.timeshet_fill.13 = 'p', 1,0) + IF($db1.timeshet_fill.14 = 'p', 1,0)
				+ IF($db1.timeshet_fill.15 = 'p', 1,0) + IF($db1.timeshet_fill.16 = 'p', 1,0) + IF($db1.timeshet_fill.17 = 'p', 1,0)
				+ IF($db1.timeshet_fill.18 = 'p', 1,0) + IF($db1.timeshet_fill.19 = 'p', 1,0) + IF($db1.timeshet_fill.20= 'p', 1,0)
				+ IF($db1.timeshet_fill.21 = 'p', 1,0) + IF($db1.timeshet_fill.22 = 'p', 1,0) + IF($db1.timeshet_fill.23 = 'p', 1,0)
				+ IF($db1.timeshet_fill.24 = 'p', 1,0) + IF($db1.timeshet_fill.25 = 'p', 1,0) + IF($db1.timeshet_fill.26 = 'p', 1,0)
				+ IF($db1.timeshet_fill.27 = 'p', 1,0) + IF($db1.timeshet_fill.28 = 'p', 1,0) + IF($db1.timeshet_fill.29 = 'p', 1,0)
				+ IF($db1.timeshet_fill.30 = 'p', 1,0) + IF($db1.timeshet_fill.31 = 'p', 1,0)
		)  AS 'present'");

        $this->db->from("$db1.timeshet_fill");
        $this->db->where("$db1.timeshet_fill.emp_id", $empid);
        //24-04-2019.. Harish
        $this->db->where("$db1.timeshet_fill.designation_id", $designation_id);
        $this->db->where("$db1.timeshet_fill.project_id", $project_id);
        $this->db->where("$db1.timeshet_fill.year", $year);
        $this->db->where("$db1.timeshet_fill.month", $month);
        $this->db->where("$db1.timeshet_fill.is_active", "1");

        $result = $this->db->get()->result();
        return isset($result[0]) ? $result[0] : false;
    }

    public function getemptimesheetattd($emp_id, $designation_id, $project_id, $selectdate) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $year = date('Y', strtotime($selectdate));
        $month = date('n', strtotime($selectdate));
        $dates = date('j', strtotime($selectdate));

        $this->db->select("$db1.timeshet_fill.$dates");
        $this->db->from("$db1.timeshet_fill");
        $this->db->where("$db1.timeshet_fill.emp_id", $emp_id);
        $this->db->where("$db1.timeshet_fill.designation_id", $designation_id);
        $this->db->where("$db1.timeshet_fill.project_id", $project_id);
        $this->db->where("$db1.timeshet_fill.year", $year);
        $this->db->where("$db1.timeshet_fill.month", $month);
        $this->db->where("$db1.timeshet_fill.is_active", "1");
        
        $recArr = $this->db->get()->result_array();
        return isset($recArr[0]) ? $recArr[0] : false;
    }

    // public function getmonthattdence($month,$year,$project_id,$empid,$designation_id){
    // $db1 = $this->db1->database;
    // $db2 = $this->db2->database;
    // $this->db->select("$db1.timeshet_fill.*");
    // $this->db->from("$db1.timeshet_fill");
    // $this->db->where("$db1.timeshet_fill.emp_id", $empid);
    // $this->db->where("$db1.timeshet_fill.designation_id", $designation_id);
    // $this->db->where("$db1.timeshet_fill.project_id", $project_id);
    // $this->db->where("$db1.timeshet_fill.year", $year);
    // $this->db->where("$db1.timeshet_fill.month", $month);
    // $recArr = $this->db->get()->result_array();
    // return isset($recArr[0]) ? $recArr[0] : false;
    // }


    public function getreimbursableeot($projectID, $masterdetail_id) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        $this->db->select("sum($db1.eot_detail.eot_mm) as eot_mm ");
        $this->db->from("$db1.eot_detail");
        $this->db->where("$db1.eot_detail.project_id", $projectID);
        $this->db->where("$db1.eot_detail.masterdetail_id", $masterdetail_id);
        $this->db->group_by("$db1.eot_detail.masterdetail_id");
        $result = $this->db->get()->result();
        return isset($result[0]) ? $result[0] : false;
    }

    public function checkinvoiceexists($invoice_no, $project_id) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        $this->db->select("$db1.invoicesave.*");
        $this->db->from("$db1.invoicesave");
        $this->db->where("$db1.invoicesave.project_id", $project_id);
        $this->db->where("$db1.invoicesave.invoiceno_id", $invoice_no);
        $this->db->group_by("$db1.invoicesave.invoiceno_id");
        $result = $this->db->get()->result();
        return isset($result) ? $result : false;
    }

    public function checktaxID($projectid, $invoiceid, $taxID) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        $Where = array('id' => $invoiceid);
        $infoid = $this->mastermodel->GetTableData('invoicedetail', $Where);

        $inID = $infoid[0]->infoid;
        $this->db->select('*');
        $this->db->from('invoicedetail');
        $wheres = "(infoid = $inID and id < $invoiceid)";
        $this->db->where($wheres);
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        $previnvoice = $this->db->get()->row_array();

        $this->db->select('*');
        $this->db->from('project_tax');
        $this->db->where('project_id', $projectid);
        $this->db->where('invoice_id', $invoiceid);
        $this->db->where('tax_id', $taxID);
        $curtaxres = $this->db->get()->result_array();

        $this->db->select('*');
        $this->db->from('project_tax');
        $this->db->where('project_id', $projectid);
        $this->db->where('invoice_id', $previnvoice['id']);
        $this->db->where('tax_id', $taxID);
        $prevtaxres = $this->db->get()->result_array();

        if (!empty($curtaxres)) {
            $taxres = $curtaxres;
        } else {
            $taxres = array();
            foreach ($prevtaxres as $val) {
                $val['amount'] = $val['totaltaxcum'];
                $taxres[] = $val;
            }
        }
        return isset($taxres) ? $taxres : false;
    }

    public function getescalation($projectid, $invoiceid) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;
        $this->db->select('*');
        $this->db->from('escalation_tax');
        $this->db->where('project_id', $projectid);
        $this->db->where('invoice_id', $invoiceid);
        $result = $this->db->get()->result_object();
        return isset($result) ? $result : false;
    }

    public function getprojecttax($projectid, $invoiceid) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;
        $this->db->select('a.*,b.tax_name');
        $this->db->from('project_tax as a');
        $this->db->join('tax_master as b', 'a.tax_id = b.id', 'left');
        $this->db->where('a.project_id', $projectid);
        $this->db->where('a.invoice_id', $invoiceid);
        $result = $this->db->get()->result_object();
        return isset($result) ? $result : false;
    }

    public function getexcelvalue($projid) {
        $this->db->select('invoice_after_xlsdownload.contract_value,SUM(total_cumulative) as total_cumulative');
        $this->db->from('invoice_after_xlsdownload');
        $this->db->where('project_id', $projid);
        $this->db->where('srpos_no', 'sub_total');
        $this->db->limit(1);
        $result = $this->db->get()->result_object();
        return isset($result) ? $result[0] : false;
    }

    public function getempbalancemm($projid, $emp_id, $designation_id) {
        $this->db->select('SUM(current_mm) as balance_mm');
        $this->db->from('siteofc_attend_invcrwn');
        $this->db->where('bd_projId', $projid);
        $this->db->where('emp_id', $emp_id);
        $result = $this->db->get()->result_object();
        return isset($result) ? $result[0] : false;
    }

    public function getempeot($projid, $emp_id, $designation_id) {
        $this->db->select('SUM(eot_mm) as eot_mm');
        $this->db->from('saveeotteam');
        $this->db->where('project_id', $projid);
        $this->db->where('emp_id', $emp_id);
        $this->db->where('designation_id', $designation_id);
        $result = $this->db->get()->result_object();
        return isset($result) ? $result[0] : false;
    }

    //New Code By Asheesh 20-Dec..
    public function getprojectid_tstobd($bdProjId) {
        // $db2 = $this->db2->database;
        // $db1 = $this->db1->database;
        $this->db->select('project_id');
        $this->db->from('accountinfo');
        $this->db->where('project_numberid', $bdProjId);
        $result = $this->db->get()->row();
        return isset($result) ? $result->project_id : false;
    }

    public function getsingleyearmonthAtten($bdProjId, $empId, $month, $year) {
        $this->db->select('a.billed_mm as current_mm');
        $this->db->from('monthwise_invc_saveupd_afterlock as a');
        $this->db->where(array('a.status' => '1', 'a.project_id' => $bdProjId, 'a.empl_id' => $empId, 'a.month' => $month, 'a.year' => $year));
        $RecArr = $this->db->get()->row();
        return ($RecArr) ? $RecArr : '';
    }

    //Coded by Jitendra 28122018
    public function checpercentage($projectid, $invoiceid, $typeid) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        $Where = array('id' => $invoiceid);
        $infoid = $this->mastermodel->GetTableData('invoicedetail', $Where);

        $inID = $infoid[0]->infoid;
        $this->db->select('*');
        $this->db->from('invoicedetail');
        $wheres = "(infoid = $inID and id < $invoiceid)";
        $this->db->where($wheres);
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        $previnvoice = $this->db->get()->row_array();


        $this->db->select('*');
        $this->db->from('acc_percentage');
        $this->db->where('project_id', $projectid);
        $this->db->where('invoice_id', $invoiceid);
        $this->db->where('type_id', $typeid);
        $curtaxres = $this->db->get()->result_array();

        $this->db->select('*');
        $this->db->from('acc_percentage');
        $this->db->where('project_id', $projectid);
        $this->db->where('invoice_id', $previnvoice['id']);
        $this->db->where('type_id', $typeid);
        $prevtaxres = $this->db->get()->result_array();

        if (!empty($curtaxres)) {
            $taxres = $curtaxres;
        } else {
            $taxres = array();
            foreach ($prevtaxres as $val) {
                $val['cumpremm'] = $val['totalmmcum'];
                $val['cumpreamt'] = $val['totalmmamount'];
                $taxres[] = $val;
            }
        }
        return isset($taxres) ? $taxres[0] : false;
    }

    function getreplacemendata($id) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;
        $data = array();

        $this->db->select("$db2.main_employees_summary.userfullname,$db2.main_employees_summary.date_of_joining,$db1.replacementteam.empname,$db1.replacementteam.designation_id,$db1.replacementteam.account_date,$db1.replacementteam.man_months");
        $this->db->from("$db1.replacementteam");
        $this->db->join("$db2.main_employees_summary", "$db1.replacementteam.empname = $db2.main_employees_summary.user_id", 'left');
        $this->db->where("$db1.replacementteam.replacement_id", $id);
        $this->db->order_by("$db1.replacementteam.id", 'desc');
        $result = $this->db->get()->result();

        $this->db->select("count($db1.replacementteam.replacement_id) as usercount");
        $this->db->from("$db1.replacementteam");
        $this->db->where("$db1.replacementteam.replacement_id", $id);
        $countresult = $this->db->get()->result();


        $username = isset($result) ? $result : false;
        $countresult = isset($countresult[0]) ? $countresult[0] : false;
        $data['username'] = $username;
        $data['countresult'] = $countresult;
        return $data;
    }

    ### New Function For ...

    public function ReplacementCountOrder($projID) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        $this->db->select("$db1.designation_master_requisition.designation_name,$db2.main_employees_summary.userfullname,$db1.replacementteam.account_date,$db1.replacementteam.empname,$db1.replacementteam.designation_id");
        $this->db->from("$db1.replacementteam");
        $this->db->join("$db2.main_employees_summary", "$db1.replacementteam.empname = $db2.main_employees_summary.user_id", 'left');
        $this->db->join("$db1.designation_master_requisition", "$db1.designation_master_requisition.fld_id = $db1.replacementteam.designation_id", 'LEFT');
        $this->db->where(array("$db1.replacementteam.project_id" => $projID, "$db1.replacementteam.status" => '1'));
        $this->db->order_by("$db1.replacementteam.id", "ASC");
        $recArr = $this->db->get()->result();


        return ($recArr) ? $recArr : false;
    }

    //Designation Name By Id
    public function DesignationNameById($userId) {
        $catnm = $this->selectRecord('designation_master_requisition', array('designation_name'), array('fld_id' => $userId));
        if ($catnm) {
            $userdata = $catnm->first_row();
            $res = $userdata->designation_name;
            return $res;
        } else {
            return false;
        }
    }

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Code By Asheesh !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //############################################## 2019 ##########################################################################
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 2019 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    //#############################################################################################################################
    public function GetAllEmplRecList() {
        $this->db2->SELECT('user_id,userfullname,employeeId');
        $this->db2->FROM('main_employees_summary');
        $this->db2->WHERE('isactive', '1');
        $this->db2->WHERE('businessunit_id!=', '2');
        $this->db2->ORDER_BY("userfullname", "ASC");
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function GetAllassignprojtoempl($emplID) {
        $this->db2->SELECT('user_id,userfullname,employeeId');
        $this->db2->FROM('main_employees_summary');
        $this->db2->WHERE('isactive', '1');
        $this->db2->WHERE('businessunit_id!=', '2');
        $this->db2->ORDER_BY("userfullname", "ASC");
        $Rec = $this->db2->get()->result();
        if ($Rec) {
            return $Rec;
        } else {
            return false;
        }
    }

    public function getProjNameByID($project_id) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.tm_projects.project_name");
        $this->db->from("$db2.tm_projects");
        $this->db->where("$db2.tm_projects.id", $project_id);
        $this->db->where("$db2.tm_projects.is_active", '1');
        $result = $this->db->get()->row();
        return isset($result) ? $result->project_name : false;
    }

    public function GetAllIntermittentProjectListEmpIdWise($Empid) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;
        $this->db->select("$db2.tm_project_employees.project_id");
        $this->db->from("$db2.tm_project_employees");
//        $this->db->join("$db.tm_project_employees");
        $this->db->where("$db2.tm_project_employees.emp_id", $Empid);
        $this->db->where("$db2.tm_project_employees.is_active", '1');
        $this->db->group_by("$db2.tm_project_employees.project_id");
        $result = $this->db->get()->result();
        return isset($result) ? $result : false;
    }

    //Code Replacement Code ... Asheesh 17-05-2018.
    public function getexceldataproject_withreplc($projectID, $projectno_id, $invoiceno_id, $key_id) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        $this->db->select("$db1.invoicesave.*,$db2.main_employees_summary.date_of_joining as dob,$db2.main_employees_summary.userfullname as username,$db1.designation_master_requisition.designation_name as designation");
        $this->db->from("$db1.invoicesave");
        $this->db->join("$db2.main_employees_summary", "$db2.main_employees_summary.user_id = $db1.invoicesave.emp_id", 'left');
        $this->db->join("$db1.designation_master_requisition", "$db1.invoicesave.designation_id = $db1.designation_master_requisition.fld_id", 'left');
        //$this->db->join("$db1.assign_finalteam","$db1.invoicesave.designation_id = $db1.assign_finalteam.designation_id",'left');
        $this->db->where("$db1.invoicesave.project_id", $projectID);
        $this->db->where("$db1.invoicesave.projectno_id", $projectno_id);
        $this->db->where("$db1.invoicesave.invoiceno_id", $invoiceno_id);
        $this->db->where("$db1.invoicesave.key_id", $key_id);
        // $this->db->where("$db1.invoicesave.id", "4489");
        $this->db->order_by("$db1.invoicesave.srno", "asc");
        $result = $this->db->get()->result();
        $resultArr = '';

        if ($result):
            foreach ($result as $recR) {
                //Check Replacement Multi Rec...
                $designation_id = $recR->designation_id;
                $infoid = $recR->info_id;
                $invoice_no = $recR->invoiceno_id;
                $returnRespon = $this->CheckReplacementorNot($projectID, $designation_id, $infoid, $invoice_no);
                $recR->username = ($returnRespon) ? $returnRespon : $recR->username;
                // $recR->cumulativeamount = $projectID . "==>" . $designation_id . "==>" . $infoid . "==>" . $invoice_no . "--" . $key_id;
                $resultArr[] = $recR;
            }
        endif;
        return isset($resultArr) ? $resultArr : false;
    }

    // Check Replacement or Not....
    public function CheckReplacementorNot($project_id, $designation_id, $infoid, $invoice_no) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;
        //Get Invoice Moth and Year..
        if ($infoid and $invoice_no) {
            $this->db2->SELECT("$db1.A1.*");
            $this->db2->FROM("$db1.invoicedetail as A1");
            $this->db2->WHERE(array("$db1.A1.infoid" => $infoid, "$db1.A1.invoice_no" => $invoice_no));
            $A1RecArr = $this->db2->get()->row();
            if ($A1RecArr) {
                $invcDate = strtotime($A1RecArr->invoice_date);
                $finalInvcMonth = date("m", strtotime("-1 month", $invcDate));
                $finalInvcYear = date("Y", strtotime("-1 month", $invcDate));
            }
            $this->db2->SELECT("$db2.main_employees_summary.userfullname");
            $this->db2->FROM("$db1.timeshet_fill");
            $this->db2->join("$db2.main_employees_summary", "$db1.timeshet_fill.emp_id=$db2.main_employees_summary.user_id", "LEFT");
            $this->db2->WHERE(array("$db1.timeshet_fill.year" => $finalInvcYear, "$db1.timeshet_fill.month" => $finalInvcMonth, "$db1.timeshet_fill.project_id" => $project_id, "$db1.timeshet_fill.designation_id" => $designation_id, "$db1.timeshet_fill.is_active" => "1"));
            $RecArr = $this->db2->get()->result();

            $finalRetR = '';
            if ($RecArr) {
                foreach ($RecArr as $nmRec) {
                    $finalRetR .= $nmRec->userfullname . "/";
                }
                return rtrim("$finalRetR", " / ");
            } else {
                return false;
            }
        }
    }

    public function getemployee_withreplcond($project_id, $keyid) {
        $ArrErec = $this->session->userdata('date_month');

        if ($ArrErec) {
            $StrValArr = explode("/", $ArrErec);
            $YeArsession = $StrValArr[1];
            $MontHsession = $StrValArr[0];
        }

        $db1 = $this->db2->database;
        $db2 = $this->db1->database;

        $this->db->select("$db2.assign_finalteam.replacement_effacted_date,$db2.assign_finalteam.id,$db2.assign_finalteam.srno,$db2.assign_finalteam.designation_id,$db2.assign_finalteam.empname,$db2.assign_finalteam.project_id,$db1.main_employees_summary.userfullname,$db2.designation_master_requisition.designation_name");
        $this->db->from("$db2.assign_finalteam");
        $this->db->join("$db1.main_employees_summary", "$db2.assign_finalteam.empname = $db1.main_employees_summary.user_id", 'left');
        $this->db->join("$db2.designation_master_requisition", "$db2.assign_finalteam.designation_id = $db2.designation_master_requisition.fld_id", 'left');
        $this->db->where("$db2.assign_finalteam.project_id", $project_id);
        $this->db->where("$db2.assign_finalteam.key_id", $keyid);
        $this->db->order_by("$db2.assign_finalteam.srno", 'asc');
        $result = $this->db->get()->result();

        $reTArr = array();
        if ($result) {
            foreach ($result as $reC):
                if (@$reC->replacement_effacted_date) {
                    // $project_id
                    $designation_id = $reC->designation_id;
                    $rEcArr2 = $this->ts_fillTableRecGet($project_id, $designation_id, $YeArsession, $MontHsession);
                    //if Single Rec..
                    if (count(@$rEcArr2) == "1"):
                        $reC->userfullname = '';
                        $reC->userfullname = ($rEcArr2[0]->userfullname) ? $rEcArr2[0]->userfullname : "";
                        $reC->empname = ($rEcArr2[0]->emp_id) ? $rEcArr2[0]->emp_id : "";
                    endif;
                    //If Greater..
                    if (count(@$rEcArr2) > 1):
                        $reC->userfullname = '';
                        foreach ($rEcArr2 as $detailRec) {
                            $reC->userfullname .= $detailRec->userfullname . " / ";
                            $reC->multirecReplc = @$rEcArr2;
                        }
                    endif;
                }
                $reTArr[] = $reC;
            endforeach;
        }
        return isset($reTArr) ? $reTArr : null;
    }

    public function ts_fillTableRecGet($projectID, $designation, $YeAr, $monTh) {
        $db2 = $this->db1->database;
        $db1 = $this->db2->database;

        $this->db->select("$db2.a.*,$db1.main_employees_summary.userfullname");
        $this->db->from("$db2.timeshet_fill as a");
        $this->db->join("$db1.main_employees_summary", "$db2.a.emp_id = $db1.main_employees_summary.user_id", 'LEFT');
        $this->db->where(array("$db2.a.designation_id" => $designation, "$db2.a.project_id" => $projectID, "$db2.a.year" => $YeAr, "$db2.a.month" => $monTh, "$db2.a.is_active" => "1"));
        $recResult = $this->db->get()->result();
        return ($recResult) ? $recResult : "";
    }

    public function getTotMMBymmyy($userID, $month, $year) {
        $db2 = $this->db1->database;
        $db1 = $this->db2->database;

        $this->db->select_sum("$db2.monthwise_invc_saveupd_afterlock.billed_mm");
        $this->db->from("$db2.monthwise_invc_saveupd_afterlock");
        $this->db->where(array("$db2.monthwise_invc_saveupd_afterlock.empl_id" => $userID, "$db2.monthwise_invc_saveupd_afterlock.year" => $year, "$db2.monthwise_invc_saveupd_afterlock.month" => $month, "$db2.monthwise_invc_saveupd_afterlock.status" => "1"));
        $recResult = $this->db->get()->result();
        return ($recResult) ? round($recResult[0]->billed_mm, 3) : "0";
    }

    public function getRecordInvcmm($BDprojID, $empId) {
        $db2 = $this->db1->database;
        $db1 = $this->db2->database;
        $this->db->select("$db2.monthwise_invc_saveupd_afterlock.*");
        $this->db->from("$db2.monthwise_invc_saveupd_afterlock");
        $this->db->where(array("$db2.monthwise_invc_saveupd_afterlock.project_id" => $BDprojID, "$db2.monthwise_invc_saveupd_afterlock.empl_id" => $empId, "$db2.monthwise_invc_saveupd_afterlock.status" => "1"));
        $this->db->order_by("$db2.monthwise_invc_saveupd_afterlock.fld_id", "DESC");
        $this->db->limit("1");
        $recResult = $this->db->get()->result();
        return ($recResult) ? $recResult[0] : null;
    }

    public function GetColorCodeWithCond($AT_project_id, $AT_designation_id, $AT_emp_id = "", $AT_year, $AT_month, $fullDate) {
        $db1 = $this->db2->database;
        $this->db->select("$db2.timeshet_fill_updhistory.color_code");
        $this->db->from("$db2.timeshet_fill_updhistory");
        $this->db->where(array(
            "$db2.timeshet_fill_updhistory.proj_id" => $AT_project_id,
            "$db2.timeshet_fill_updhistory.designation_id" => $AT_designation_id,
//            "$db2.timeshet_fill_updhistory.empl_id" => $AT_emp_id,
            "$db2.timeshet_fill_updhistory.year" => $AT_year,
            "$db2.timeshet_fill_updhistory.month" => $AT_month,
            "$db2.timeshet_fill_updhistory.atten_date" => date("Y-m-d", strtotime($fullDate))));
        $recResult = $this->db->get()->row();
        return ($recResult) ? $recResult->color_code : null;

        //  return "Red" . $AT_project_id . "<br>" . $AT_designation_id . "<br>" . $AT_emp_id . "<br>" . $AT_year . "<br>" . $AT_month . "<br>" . date("Y-m-d", strtotime($fullDate));
    }

    //New Code By Asheesh 30-07-2019-Dec..
    public function getprojectid_bdtots($bdProjId) {
        $db1 = $this->db1->database;

        $this->db->select("$db1.accountinfo.project_numberid");
        $this->db->from("$db1.accountinfo");
        $this->db->where("$db1.accountinfo.project_id", $bdProjId);
        $result = $this->db->get()->row();
        return isset($result) ? $result->project_numberid : false;
    }

    public function replacementdataGetNew($projID, $key) {
        $tSprojId = $this->getprojectid_bdtots($projID);
        $db2 = $this->db1->database;
        $db1 = $this->db2->database;

        $this->db->select("$db1.main_users.userfullname,$db2.replacementteam.account_date,$db2.designation_master_requisition.designation_name");
        $this->db->from("$db2.replacementteam");
        $this->db->join("$db2.designation_master_requisition", "$db2.replacementteam.designation_id=$db2.designation_master_requisition.fld_id", "LEFT");
        $this->db->join("$db1.main_users", "$db2.replacementteam.empname=$db1.main_users.id", "LEFT");
        $this->db->where(array("$db2.replacementteam.project_id" => $projID, "$db2.replacementteam.status" => "1", "$db2.replacementteam.key_id" => $key));
        $this->db->order_by("$db2.replacementteam.account_date", "ASC");
        $recResult1 = $this->db->get()->result();
        $recArrResp1 = array();
        if ($recResult1) {
            foreach ($recResult1 as $rEcreplc):
                $rEcreplc->resign_date = '';
                $recArrResp1[] = $rEcreplc;
            endforeach;
        }
        //Old Replacdement ...
        $this->db->select("$db2.team_req_otheremp.emp_name, GROUP_CONCAT($db1.inhouseempl.userfullname) as usertotal, GROUP_CONCAT($db2.old_replacement_emp_data.date_of_joining) as doj, GROUP_CONCAT($db2.old_replacement_emp_data.resign_date) as resig, GROUP_CONCAT($db2.old_replacement_emp_data.empl_type) as emp_type,$db1.inhouseempl.userfullname as inhouse_userfullname,$db2.designation_master_requisition.designation_name,$db2.old_replacement_emp_data.empl_type,$db2.old_replacement_emp_data.date_of_joining,$db2.old_replacement_emp_data.resign_date,$db2.old_replacement_emp_data.replacement_date");
        ///$this->db->distinct("$db1.inhouseempl.userfullname");
        $this->db->from("$db2.old_replacement_emp_data");
        $this->db->join("$db2.designation_master_requisition", "$db2.old_replacement_emp_data.designation_id=$db2.designation_master_requisition.fld_id", "LEFT");
        $this->db->join("$db1.main_users as inhouseempl", "$db2.old_replacement_emp_data.empl_id=$db1.inhouseempl.id", "LEFT");
        $this->db->join("$db2.team_req_otheremp", "$db2.old_replacement_emp_data.empl_id=$db2.team_req_otheremp.fld_id", "LEFT");
        $this->db->where(array("$db2.old_replacement_emp_data.project_id" => $tSprojId, "$db2.old_replacement_emp_data.status" => "1", "$db2.old_replacement_emp_data.key_id" => $key));
        $this->db->order_by("$db2.old_replacement_emp_data.date_of_joining", "desc");
        $this->db->group_by("$db2.old_replacement_emp_data.designation_id");
        $recResult2 = $this->db->get()->result();
        return array("newreplacementArr" => $recArrResp1, "oldreplacementArr" => $recResult2);
    }

    //Get All Sunday List ...
    public function GetAllSundayList($month, $year) {
        $list = array();
        for ($d = 1; $d <= 31; $d++) {
            $time = mktime(12, 0, 0, $month, $d, $year);
            if ((date('m', $time) == $month) and ( date('D', $time) == "Sun"))
                $list[] = date('d', $time);
        }
        return ($list) ? $list : "";
    }

    public function saveinfotimeattd($Arr) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $loginUser = $this->session->userdata('loginid');
        //Check if ofc Manager..
        $this->db->select("$db2.main_employees_summary.position_id,$db2.main_employees_summary.position_id");
        $this->db->from("$db2.main_employees_summary");
        $this->db->where(array("$db2.main_employees_summary.isactive" => '1', "$db2.main_employees_summary.user_id" => $loginUser));
        $EmpRow = $this->db->get()->row();

        $colorCode = "red";
        //if Not Office Manager 
        if (($EmpRow->position_id != "66") and ( $EmpRow->position_id != "10")) {
            $colorCode = "red";
        }
        //if Office Manager 
        if ($EmpRow->position_id == "66") {
            $colorCode = "green";
        }
        //if Account Department
        if ($EmpRow->position_id == "10") {
            $colorCode = "magenta";
        }

        if ($Arr) {
            $length = count($Arr['emp_id']);
            $arrchk = explode(',', $Arr['dates']);
            $dividearray = count($arrchk);
            $attdlength = count($Arr['attend']);
            //$dividearray = $length - 1;
            $datemonth = array_chunk($Arr['date_month'], $dividearray);
            $attendmonth = array_chunk($Arr['attend'], $dividearray);
            //echo '<pre>'; print_r($datemonth);
            for ($i = 0; $i < $length; $i++) {
                for ($j = 0; $j < $attdlength; $j++) {
                    $year = date('Y', strtotime($datemonth[$i][$j]));
                    $month = date('n', strtotime($datemonth[$i][$j]));
                    $datess = date('j', strtotime($datemonth[$i][$j]));
                    $filldates = date('Y-m-d', strtotime($datemonth[$i][$j]));

                    //24-04-2019 .. harish
                    $Where = array("$db1.timeshet_fill.project_id" => $Arr['project_id'], "$db1.timeshet_fill.designation_id" => $Arr['designation_id'][$i], "$db1.timeshet_fill.emp_id" => $Arr['emp_id'][$i], "$db1.timeshet_fill.year" => $year, "$db1.timeshet_fill.month" => $month);
                    // $Where = array("$db1.timeshet_fill.project_id" => $Arr['project_id'], "$db1.timeshet_fill.emp_id" => $Arr['emp_id'][$i], "$db1.timeshet_fill.year" => $year, "$db1.timeshet_fill.month" => $month);
                    $data = $this->mastermodel->count_rows("$db1.timeshet_fill", $Where);

                    // if (($data < 1) and ( $attendmonth[$i][$j])) {
                    if ($data < 1) {
                        $arr = array(
                            "$db1.timeshet_fill.emp_id" => $Arr['emp_id'][$i],
                            "$db1.timeshet_fill.designation_id" => $Arr['designation_id'][$i],
                            "$db1.timeshet_fill.project_id" => $Arr['project_id'],
                            "$db1.timeshet_fill.fill_date" => $filldates,
                            "$db1.timeshet_fill.year" => $year,
                            "$db1.timeshet_fill.month" => $month,
                            "$db1.timeshet_fill.$datess" => isset($attendmonth[$i][$j]) ? $attendmonth[$i][$j] : '',
                            "$db1.timeshet_fill.entryby" => $this->session->userdata('loginid'),
                            "$db1.timeshet_fill.created" => date('Y-m-d'),
                            "$db1.timeshet_fill.modified" => date('Y-m-d')
                        );
                        //21-08-2019 code for avoid Null entry..
                        if ($datemonth[$i][$j]):
                            $res = $this->mastermodel->InsertMasterData($arr, "$db1.timeshet_fill");
                        endif;
                    } else {
                        $arr = array(
                            "$db1.timeshet_fill.$datess" => $attendmonth[$i][$j],
                            "$db1.timeshet_fill.entryby" => $this->session->userdata('loginid'),
                            "$db1.timeshet_fill.modified" => date('Y-m-d')
                        );
                        //History For Color Set Atten.. Code Start Asheesh
                        $ts_fillHistoryInsrt2 = array("action_name" => "update", "designation_id" => $Arr['designation_id'][$i], "emp_id" => $Arr['emp_id'][$i], "proj_id" => $Arr['project_id'], "year" => $year, "month" => $month, "atten_date" => $filldates, "color_code" => $colorCode, "entry_by" => $loginUser);
                        if ($attendmonth[$i][$j]):
                            $Where = array("$db1.timeshet_fill.project_id" => $Arr['project_id'], "$db1.timeshet_fill.designation_id" => $Arr['designation_id'][$i], "$db1.timeshet_fill.emp_id" => $Arr['emp_id'][$i], "$db1.timeshet_fill.year" => $year, "$db1.timeshet_fill.month" => $month);
                            $this->db->select("$db1.timeshet_fill.*");
                            $this->db->from("$db1.timeshet_fill");
                            $this->db->where($Where);
                            $RdrecArr = $this->db->get()->result_array();
                            if ($RdrecArr[0][$datess] != $attendmonth[$i][$j]):
                                $this->mastermodel->InsertMasterData($ts_fillHistoryInsrt2, "$db1.timeshet_fill_updhistory");
                            endif;
                        endif;
                        //History For Color Set Atten.. Code Close Asheesh
                        //24-04-2019 .. harish
                        // $Wheres = array("$db1.timeshet_fill.project_id" => $Arr['project_id'], "$db1.timeshet_fill.emp_id" => $Arr['emp_id'][$i], "$db1.timeshet_fill.year" => $year, "$db1.timeshet_fill.month" => $month);
                        $Wheres = array("$db1.timeshet_fill.project_id" => $Arr['project_id'], "$db1.timeshet_fill.designation_id" => $Arr['designation_id'][$i], "$db1.timeshet_fill.emp_id" => $Arr['emp_id'][$i], "$db1.timeshet_fill.year" => $year, "$db1.timeshet_fill.month" => $month);
                        $res = $this->mastermodel->UpdateRecords("$db1.timeshet_fill", $Wheres, $arr);
                    }
                }
            }
        }
        return $res;
    }

    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //Comment Bug Function .. 05-11-2019 ....................................
//    public function getcurrentmonth($projectID, $key_id) {
//        $db2 = $this->db2->database;
//        $db1 = $this->db1->database;
//
//        $this->db->select("$db1.assign_finalteam.empname,$db1.assign_finalteam.man_months,$db1.assign_finalteam.designation_id,$db1.assign_finalteam.cumulative_pre_mm,$db1.assign_finalteam.cumulative_pre_amount,$db1.assign_finalteam.rate,$db1.assign_finalteam.replacement_reducationrate,$db2.main_employees_summary.userfullname,$db1.designation_master_requisition.designation_name");
//        $this->db->from("$db1.assign_finalteam");
//        $this->db->join("$db2.main_employees_summary", "$db2.main_employees_summary.user_id = $db1.assign_finalteam.empname", 'left');
//        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id = $db1.designation_master_requisition.fld_id", 'left');
//        $this->db->where("$db1.assign_finalteam.project_id", $projectID);
//        $this->db->where("$db1.assign_finalteam.key_id", $key_id);
//        $this->db->order_by("$db1.assign_finalteam.srno", "asc");
//        $result = $this->db->get()->result();
//        return isset($result) ? $result : false;
//    }
//    public function getuseattd($project_id, $empid, $designation_id, $month, $year) {
//        $db1 = $this->db1->database;
//        $db2 = $this->db2->database;
//
//        $this->db->select("$db1.timeshet_fill.emp_id , SUM(IF($db1.timeshet_fill.1 = 'l', 1,0) + IF($db1.timeshet_fill.2 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.3 = 'l', 1,0) + IF($db1.timeshet_fill.4 = 'l', 1,0) + IF($db1.timeshet_fill.5 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.6 = 'l', 1,0) + IF($db1.timeshet_fill.7 = 'l', 1,0) + IF($db1.timeshet_fill.8 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.9 = 'l', 1,0) + IF($db1.timeshet_fill.10 = 'l', 1,0) + IF($db1.timeshet_fill.11 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.12 = 'l', 1,0) + IF($db1.timeshet_fill.13 = 'l', 1,0) + IF($db1.timeshet_fill.14 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.15 = 'l', 1,0) + IF($db1.timeshet_fill.16 = 'l', 1,0) + IF($db1.timeshet_fill.17 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.18 = 'l', 1,0) + IF($db1.timeshet_fill.19 = 'l', 1,0) + IF($db1.timeshet_fill.20= 'l', 1,0)
//				+ IF($db1.timeshet_fill.21 = 'l', 1,0) + IF($db1.timeshet_fill.22 = 'l', 1,0) + IF($db1.timeshet_fill.23 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.24 = 'l', 1,0) + IF($db1.timeshet_fill.25 = 'l', 1,0) + IF($db1.timeshet_fill.26 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.27 = 'l', 1,0) + IF($db1.timeshet_fill.28 = 'l', 1,0) + IF($db1.timeshet_fill.29 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.30 = 'l', 1,0) + IF($db1.timeshet_fill.31 = 'l', 1,0)
//		)  AS 'leave' , SUM(IF($db1.timeshet_fill.1 = 'a', 1,0) + IF($db1.timeshet_fill.2 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.3 = 'a', 1,0) + IF($db1.timeshet_fill.4 = 'a', 1,0) + IF($db1.timeshet_fill.5 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.6 = 'a', 1,0) + IF($db1.timeshet_fill.7 = 'a', 1,0) + IF($db1.timeshet_fill.8 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.9 = 'a', 1,0) + IF($db1.timeshet_fill.10 = 'a', 1,0) + IF($db1.timeshet_fill.11 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.12 = 'a', 1,0) + IF($db1.timeshet_fill.13 = 'a', 1,0) + IF($db1.timeshet_fill.14 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.15 = 'a', 1,0) + IF($db1.timeshet_fill.16 = 'a', 1,0) + IF($db1.timeshet_fill.17 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.18 = 'a', 1,0) + IF($db1.timeshet_fill.19 = 'a', 1,0) + IF($db1.timeshet_fill.20= 'a', 1,0)
//				+ IF($db1.timeshet_fill.21 = 'a', 1,0) + IF($db1.timeshet_fill.22 = 'a', 1,0) + IF($db1.timeshet_fill.23 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.24 = 'a', 1,0) + IF($db1.timeshet_fill.25 = 'a', 1,0) + IF($db1.timeshet_fill.26 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.27 = 'a', 1,0) + IF($db1.timeshet_fill.28 = 'a', 1,0) + IF($db1.timeshet_fill.29 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.30 = 'a', 1,0) + IF($db1.timeshet_fill.31 = 'a', 1,0)
//		)  AS 'absent',
//		SUM(IF($db1.timeshet_fill.1 = 'p', 1,0) + IF($db1.timeshet_fill.2 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.3 = 'p', 1,0) + IF($db1.timeshet_fill.4 = 'p', 1,0) + IF($db1.timeshet_fill.5 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.6 = 'p', 1,0) + IF($db1.timeshet_fill.7 = 'p', 1,0) + IF($db1.timeshet_fill.8 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.9 = 'p', 1,0) + IF($db1.timeshet_fill.10 = 'p', 1,0) + IF($db1.timeshet_fill.11 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.12 = 'p', 1,0) + IF($db1.timeshet_fill.13 = 'p', 1,0) + IF($db1.timeshet_fill.14 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.15 = 'p', 1,0) + IF($db1.timeshet_fill.16 = 'p', 1,0) + IF($db1.timeshet_fill.17 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.18 = 'p', 1,0) + IF($db1.timeshet_fill.19 = 'p', 1,0) + IF($db1.timeshet_fill.20= 'p', 1,0)
//				+ IF($db1.timeshet_fill.21 = 'p', 1,0) + IF($db1.timeshet_fill.22 = 'p', 1,0) + IF($db1.timeshet_fill.23 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.24 = 'p', 1,0) + IF($db1.timeshet_fill.25 = 'p', 1,0) + IF($db1.timeshet_fill.26 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.27 = 'p', 1,0) + IF($db1.timeshet_fill.28 = 'p', 1,0) + IF($db1.timeshet_fill.29 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.30 = 'p', 1,0) + IF($db1.timeshet_fill.31 = 'p', 1,0)
//		)  AS 'present'
//	");
//
//        $this->db->from("$db1.timeshet_fill");
//        $this->db->where("$db1.timeshet_fill.emp_id", $empid);
//        //Comment 24-04-2019 for Harish - Attendance calculation
//        $this->db->where("$db1.timeshet_fill.designation_id", $designation_id);
//        $this->db->where("$db1.timeshet_fill.project_id", $project_id);
//        $this->db->where("$db1.timeshet_fill.year", $year);
//        $this->db->where("$db1.timeshet_fill.month", $month);
//
//        $result = $this->db->get()->result();
//        return isset($result[0]) ? $result[0] : false;
//    }
//    public function getexcelcurrentmonth($projectID, $invoiceID, $designaitioID) {
//        $db2 = $this->db2->database;
//        $db1 = $this->db1->database;
//
//        $this->db->select("$db1.acc_monthdetail.user_month");
//        $this->db->from("$db1.acc_monthdate");
//        $this->db->join("$db1.acc_monthdetail", "$db1.acc_monthdate.id = $db1.acc_monthdetail.month_id", 'left');
//
//        $previousmonth = date("Y-m-d", strtotime("first day of this month"));
//        $lastpreviousmonth = date("Y-m-d", strtotime("last day of this month"));
//
//        $this->db->where("$db1.acc_monthdate.project_id", $projectID);
//        $this->db->where("$db1.acc_monthdate.invoice_id", $invoiceID);
//
//        $where_date = "($db1.acc_monthdate.date_month >='$previousmonth' AND $db1.acc_monthdate.date_month <= '$lastpreviousmonth')";
//        $this->db->where($where_date);
//        $this->db->where("$db1.acc_monthdetail.designation_id", $designaitioID);
//
//        $result = $this->db->get()->result();
//        return isset($result[0]) ? $result[0] : false;
//    }
//    public function gettimesheetdetail($project_id, $designation_id, $empid, $month, $year) {
//        $db1 = $this->db1->database;
//        $db2 = $this->db2->database;
//
//        $this->db->select("$db1.timeshet_fill.emp_id , SUM(IF($db1.timeshet_fill.1 = 'l', 1,0) + IF($db1.timeshet_fill.2 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.3 = 'l', 1,0) + IF($db1.timeshet_fill.4 = 'l', 1,0) + IF($db1.timeshet_fill.5 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.6 = 'l', 1,0) + IF($db1.timeshet_fill.7 = 'l', 1,0) + IF($db1.timeshet_fill.8 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.9 = 'l', 1,0) + IF($db1.timeshet_fill.10 = 'l', 1,0) + IF($db1.timeshet_fill.11 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.12 = 'l', 1,0) + IF($db1.timeshet_fill.13 = 'l', 1,0) + IF($db1.timeshet_fill.14 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.15 = 'l', 1,0) + IF($db1.timeshet_fill.16 = 'l', 1,0) + IF($db1.timeshet_fill.17 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.18 = 'l', 1,0) + IF($db1.timeshet_fill.19 = 'l', 1,0) + IF($db1.timeshet_fill.20= 'l', 1,0)
//				+ IF($db1.timeshet_fill.21 = 'l', 1,0) + IF($db1.timeshet_fill.22 = 'l', 1,0) + IF($db1.timeshet_fill.23 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.24 = 'l', 1,0) + IF($db1.timeshet_fill.25 = 'l', 1,0) + IF($db1.timeshet_fill.26 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.27 = 'l', 1,0) + IF($db1.timeshet_fill.28 = 'l', 1,0) + IF($db1.timeshet_fill.29 = 'l', 1,0)
//				+ IF($db1.timeshet_fill.30 = 'l', 1,0) + IF($db1.timeshet_fill.31 = 'l', 1,0)
//		)  AS 'leave' , SUM(IF($db1.timeshet_fill.1 = 'a', 1,0) + IF($db1.timeshet_fill.2 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.3 = 'a', 1,0) + IF($db1.timeshet_fill.4 = 'a', 1,0) + IF($db1.timeshet_fill.5 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.6 = 'a', 1,0) + IF($db1.timeshet_fill.7 = 'a', 1,0) + IF($db1.timeshet_fill.8 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.9 = 'a', 1,0) + IF($db1.timeshet_fill.10 = 'a', 1,0) + IF($db1.timeshet_fill.11 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.12 = 'a', 1,0) + IF($db1.timeshet_fill.13 = 'a', 1,0) + IF($db1.timeshet_fill.14 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.15 = 'a', 1,0) + IF($db1.timeshet_fill.16 = 'a', 1,0) + IF($db1.timeshet_fill.17 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.18 = 'a', 1,0) + IF($db1.timeshet_fill.19 = 'a', 1,0) + IF($db1.timeshet_fill.20= 'a', 1,0)
//				+ IF($db1.timeshet_fill.21 = 'a', 1,0) + IF($db1.timeshet_fill.22 = 'a', 1,0) + IF($db1.timeshet_fill.23 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.24 = 'a', 1,0) + IF($db1.timeshet_fill.25 = 'a', 1,0) + IF($db1.timeshet_fill.26 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.27 = 'a', 1,0) + IF($db1.timeshet_fill.28 = 'a', 1,0) + IF($db1.timeshet_fill.29 = 'a', 1,0)
//				+ IF($db1.timeshet_fill.30 = 'a', 1,0) + IF($db1.timeshet_fill.31 = 'a', 1,0)
//		)  AS 'absent'
//		,
//		SUM(IF($db1.timeshet_fill.1 = 'p', 1,0) + IF($db1.timeshet_fill.2 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.3 = 'p', 1,0) + IF($db1.timeshet_fill.4 = 'p', 1,0) + IF($db1.timeshet_fill.5 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.6 = 'p', 1,0) + IF($db1.timeshet_fill.7 = 'p', 1,0) + IF($db1.timeshet_fill.8 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.9 = 'p', 1,0) + IF($db1.timeshet_fill.10 = 'p', 1,0) + IF($db1.timeshet_fill.11 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.12 = 'p', 1,0) + IF($db1.timeshet_fill.13 = 'p', 1,0) + IF($db1.timeshet_fill.14 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.15 = 'p', 1,0) + IF($db1.timeshet_fill.16 = 'p', 1,0) + IF($db1.timeshet_fill.17 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.18 = 'p', 1,0) + IF($db1.timeshet_fill.19 = 'p', 1,0) + IF($db1.timeshet_fill.20= 'p', 1,0)
//				+ IF($db1.timeshet_fill.21 = 'p', 1,0) + IF($db1.timeshet_fill.22 = 'p', 1,0) + IF($db1.timeshet_fill.23 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.24 = 'p', 1,0) + IF($db1.timeshet_fill.25 = 'p', 1,0) + IF($db1.timeshet_fill.26 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.27 = 'p', 1,0) + IF($db1.timeshet_fill.28 = 'p', 1,0) + IF($db1.timeshet_fill.29 = 'p', 1,0)
//				+ IF($db1.timeshet_fill.30 = 'p', 1,0) + IF($db1.timeshet_fill.31 = 'p', 1,0)
//		)  AS 'present'
//		");
//
//        $this->db->from("$db1.timeshet_fill");
//        $this->db->where("$db1.timeshet_fill.emp_id", $empid);
//        $this->db->where("$db1.timeshet_fill.project_id", $project_id);
//        //24-04-2019.. Harish
//        $this->db->where("$db1.timeshet_fill.designation_id", $designation_id);
//        $this->db->where("$db1.timeshet_fill.year", $year);
//        $this->db->where("$db1.timeshet_fill.month", $month);
//
//        $result = $this->db->get()->result();
//        return isset($result[0]) ? $result[0] : false;
//    }
    //Updated..
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    //################################################################

    public function getcurrentmonth($projectID, $key_id) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        $this->db->select("$db1.assign_finalteam.empname,$db1.assign_finalteam.man_months,$db1.assign_finalteam.designation_id,$db1.assign_finalteam.cumulative_pre_mm,$db1.assign_finalteam.cumulative_pre_amount,$db1.assign_finalteam.rate,$db1.assign_finalteam.replacement_reducationrate,$db2.main_employees_summary.userfullname,$db1.designation_master_requisition.designation_name");
        $this->db->from("$db1.assign_finalteam");
        $this->db->join("$db2.main_employees_summary", "$db2.main_employees_summary.user_id = $db1.assign_finalteam.empname", 'left');
        $this->db->join("$db1.designation_master_requisition", "$db1.assign_finalteam.designation_id = $db1.designation_master_requisition.fld_id", 'left');
        $this->db->where("$db1.assign_finalteam.project_id", $projectID);
        $this->db->where("$db1.assign_finalteam.key_id", $key_id);
        $this->db->order_by("$db1.assign_finalteam.srno", "asc");
        $result = $this->db->get()->result();
        return isset($result) ? $result : false;
    }

    public function getuseattd($project_id, $empid, $designation_id, $month, $year) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.timeshet_fill.emp_id , SUM(IF($db1.timeshet_fill.1 = 'l', 1,0) + IF($db1.timeshet_fill.2 = 'l', 1,0)
				+ IF($db1.timeshet_fill.3 = 'l', 1,0) + IF($db1.timeshet_fill.4 = 'l', 1,0) + IF($db1.timeshet_fill.5 = 'l', 1,0)
				+ IF($db1.timeshet_fill.6 = 'l', 1,0) + IF($db1.timeshet_fill.7 = 'l', 1,0) + IF($db1.timeshet_fill.8 = 'l', 1,0)
				+ IF($db1.timeshet_fill.9 = 'l', 1,0) + IF($db1.timeshet_fill.10 = 'l', 1,0) + IF($db1.timeshet_fill.11 = 'l', 1,0)
				+ IF($db1.timeshet_fill.12 = 'l', 1,0) + IF($db1.timeshet_fill.13 = 'l', 1,0) + IF($db1.timeshet_fill.14 = 'l', 1,0)
				+ IF($db1.timeshet_fill.15 = 'l', 1,0) + IF($db1.timeshet_fill.16 = 'l', 1,0) + IF($db1.timeshet_fill.17 = 'l', 1,0)
				+ IF($db1.timeshet_fill.18 = 'l', 1,0) + IF($db1.timeshet_fill.19 = 'l', 1,0) + IF($db1.timeshet_fill.20= 'l', 1,0)
				+ IF($db1.timeshet_fill.21 = 'l', 1,0) + IF($db1.timeshet_fill.22 = 'l', 1,0) + IF($db1.timeshet_fill.23 = 'l', 1,0)
				+ IF($db1.timeshet_fill.24 = 'l', 1,0) + IF($db1.timeshet_fill.25 = 'l', 1,0) + IF($db1.timeshet_fill.26 = 'l', 1,0)
				+ IF($db1.timeshet_fill.27 = 'l', 1,0) + IF($db1.timeshet_fill.28 = 'l', 1,0) + IF($db1.timeshet_fill.29 = 'l', 1,0)
				+ IF($db1.timeshet_fill.30 = 'l', 1,0) + IF($db1.timeshet_fill.31 = 'l', 1,0)
		)  AS 'leave' , SUM(IF($db1.timeshet_fill.1 = 'a', 1,0) + IF($db1.timeshet_fill.2 = 'a', 1,0)
				+ IF($db1.timeshet_fill.3 = 'a', 1,0) + IF($db1.timeshet_fill.4 = 'a', 1,0) + IF($db1.timeshet_fill.5 = 'a', 1,0)
				+ IF($db1.timeshet_fill.6 = 'a', 1,0) + IF($db1.timeshet_fill.7 = 'a', 1,0) + IF($db1.timeshet_fill.8 = 'a', 1,0)
				+ IF($db1.timeshet_fill.9 = 'a', 1,0) + IF($db1.timeshet_fill.10 = 'a', 1,0) + IF($db1.timeshet_fill.11 = 'a', 1,0)
				+ IF($db1.timeshet_fill.12 = 'a', 1,0) + IF($db1.timeshet_fill.13 = 'a', 1,0) + IF($db1.timeshet_fill.14 = 'a', 1,0)
				+ IF($db1.timeshet_fill.15 = 'a', 1,0) + IF($db1.timeshet_fill.16 = 'a', 1,0) + IF($db1.timeshet_fill.17 = 'a', 1,0)
				+ IF($db1.timeshet_fill.18 = 'a', 1,0) + IF($db1.timeshet_fill.19 = 'a', 1,0) + IF($db1.timeshet_fill.20= 'a', 1,0)
				+ IF($db1.timeshet_fill.21 = 'a', 1,0) + IF($db1.timeshet_fill.22 = 'a', 1,0) + IF($db1.timeshet_fill.23 = 'a', 1,0)
				+ IF($db1.timeshet_fill.24 = 'a', 1,0) + IF($db1.timeshet_fill.25 = 'a', 1,0) + IF($db1.timeshet_fill.26 = 'a', 1,0)
				+ IF($db1.timeshet_fill.27 = 'a', 1,0) + IF($db1.timeshet_fill.28 = 'a', 1,0) + IF($db1.timeshet_fill.29 = 'a', 1,0)
				+ IF($db1.timeshet_fill.30 = 'a', 1,0) + IF($db1.timeshet_fill.31 = 'a', 1,0)
		)  AS 'absent',
		SUM(IF($db1.timeshet_fill.1 = 'p', 1,0) + IF($db1.timeshet_fill.2 = 'p', 1,0)
				+ IF($db1.timeshet_fill.3 = 'p', 1,0) + IF($db1.timeshet_fill.4 = 'p', 1,0) + IF($db1.timeshet_fill.5 = 'p', 1,0)
				+ IF($db1.timeshet_fill.6 = 'p', 1,0) + IF($db1.timeshet_fill.7 = 'p', 1,0) + IF($db1.timeshet_fill.8 = 'p', 1,0)
				+ IF($db1.timeshet_fill.9 = 'p', 1,0) + IF($db1.timeshet_fill.10 = 'p', 1,0) + IF($db1.timeshet_fill.11 = 'p', 1,0)
				+ IF($db1.timeshet_fill.12 = 'p', 1,0) + IF($db1.timeshet_fill.13 = 'p', 1,0) + IF($db1.timeshet_fill.14 = 'p', 1,0)
				+ IF($db1.timeshet_fill.15 = 'p', 1,0) + IF($db1.timeshet_fill.16 = 'p', 1,0) + IF($db1.timeshet_fill.17 = 'p', 1,0)
				+ IF($db1.timeshet_fill.18 = 'p', 1,0) + IF($db1.timeshet_fill.19 = 'p', 1,0) + IF($db1.timeshet_fill.20= 'p', 1,0)
				+ IF($db1.timeshet_fill.21 = 'p', 1,0) + IF($db1.timeshet_fill.22 = 'p', 1,0) + IF($db1.timeshet_fill.23 = 'p', 1,0)
				+ IF($db1.timeshet_fill.24 = 'p', 1,0) + IF($db1.timeshet_fill.25 = 'p', 1,0) + IF($db1.timeshet_fill.26 = 'p', 1,0)
				+ IF($db1.timeshet_fill.27 = 'p', 1,0) + IF($db1.timeshet_fill.28 = 'p', 1,0) + IF($db1.timeshet_fill.29 = 'p', 1,0)
				+ IF($db1.timeshet_fill.30 = 'p', 1,0) + IF($db1.timeshet_fill.31 = 'p', 1,0)
		)  AS 'present'

	");

        $this->db->from("$db1.timeshet_fill");
        $this->db->where("$db1.timeshet_fill.emp_id", $empid);
        //Comment 24-04-2019 for Harish - Attendance calculation
        $this->db->where("$db1.timeshet_fill.designation_id", $designation_id);
        $this->db->where("$db1.timeshet_fill.project_id", $project_id);
        $this->db->where("$db1.timeshet_fill.year", $year);
        $this->db->where("$db1.timeshet_fill.month", $month);

        $result = $this->db->get()->result();
        return isset($result[0]) ? $result[0] : false;
    }

    public function getexcelcurrentmonth($projectID, $invoiceID, $designaitioID) {
        $db2 = $this->db2->database;
        $db1 = $this->db1->database;

        $this->db->select("$db1.acc_monthdetail.user_month");
        $this->db->from("$db1.acc_monthdate");
        $this->db->join("$db1.acc_monthdetail", "$db1.acc_monthdate.id = $db1.acc_monthdetail.month_id", 'left');

        $previousmonth = date("Y-m-d", strtotime("first day of this month"));
        $lastpreviousmonth = date("Y-m-d", strtotime("last day of this month"));

        $this->db->where("$db1.acc_monthdate.project_id", $projectID);
        $this->db->where("$db1.acc_monthdate.invoice_id", $invoiceID);

        $where_date = "($db1.acc_monthdate.date_month >='$previousmonth' AND $db1.acc_monthdate.date_month <= '$lastpreviousmonth')";
        $this->db->where($where_date);
        $this->db->where("$db1.acc_monthdetail.designation_id", $designaitioID);

        $result = $this->db->get()->result();
        return isset($result[0]) ? $result[0] : false;
    }

    public function gettimesheetdetail($project_id, $designation_id, $empid, $month, $year) {
        $db1 = $this->db1->database;
        $db2 = $this->db2->database;

        $this->db->select("$db1.timeshet_fill.emp_id , SUM(IF($db1.timeshet_fill.1 = 'l', 1,0) + IF($db1.timeshet_fill.2 = 'l', 1,0)
				+ IF($db1.timeshet_fill.3 = 'l', 1,0) + IF($db1.timeshet_fill.4 = 'l', 1,0) + IF($db1.timeshet_fill.5 = 'l', 1,0)
				+ IF($db1.timeshet_fill.6 = 'l', 1,0) + IF($db1.timeshet_fill.7 = 'l', 1,0) + IF($db1.timeshet_fill.8 = 'l', 1,0)
				+ IF($db1.timeshet_fill.9 = 'l', 1,0) + IF($db1.timeshet_fill.10 = 'l', 1,0) + IF($db1.timeshet_fill.11 = 'l', 1,0)
				+ IF($db1.timeshet_fill.12 = 'l', 1,0) + IF($db1.timeshet_fill.13 = 'l', 1,0) + IF($db1.timeshet_fill.14 = 'l', 1,0)
				+ IF($db1.timeshet_fill.15 = 'l', 1,0) + IF($db1.timeshet_fill.16 = 'l', 1,0) + IF($db1.timeshet_fill.17 = 'l', 1,0)
				+ IF($db1.timeshet_fill.18 = 'l', 1,0) + IF($db1.timeshet_fill.19 = 'l', 1,0) + IF($db1.timeshet_fill.20= 'l', 1,0)
				+ IF($db1.timeshet_fill.21 = 'l', 1,0) + IF($db1.timeshet_fill.22 = 'l', 1,0) + IF($db1.timeshet_fill.23 = 'l', 1,0)
				+ IF($db1.timeshet_fill.24 = 'l', 1,0) + IF($db1.timeshet_fill.25 = 'l', 1,0) + IF($db1.timeshet_fill.26 = 'l', 1,0)
				+ IF($db1.timeshet_fill.27 = 'l', 1,0) + IF($db1.timeshet_fill.28 = 'l', 1,0) + IF($db1.timeshet_fill.29 = 'l', 1,0)
				+ IF($db1.timeshet_fill.30 = 'l', 1,0) + IF($db1.timeshet_fill.31 = 'l', 1,0)
		)  AS 'leave' , SUM(IF($db1.timeshet_fill.1 = 'a', 1,0) + IF($db1.timeshet_fill.2 = 'a', 1,0)
				+ IF($db1.timeshet_fill.3 = 'a', 1,0) + IF($db1.timeshet_fill.4 = 'a', 1,0) + IF($db1.timeshet_fill.5 = 'a', 1,0)
				+ IF($db1.timeshet_fill.6 = 'a', 1,0) + IF($db1.timeshet_fill.7 = 'a', 1,0) + IF($db1.timeshet_fill.8 = 'a', 1,0)
				+ IF($db1.timeshet_fill.9 = 'a', 1,0) + IF($db1.timeshet_fill.10 = 'a', 1,0) + IF($db1.timeshet_fill.11 = 'a', 1,0)
				+ IF($db1.timeshet_fill.12 = 'a', 1,0) + IF($db1.timeshet_fill.13 = 'a', 1,0) + IF($db1.timeshet_fill.14 = 'a', 1,0)
				+ IF($db1.timeshet_fill.15 = 'a', 1,0) + IF($db1.timeshet_fill.16 = 'a', 1,0) + IF($db1.timeshet_fill.17 = 'a', 1,0)
				+ IF($db1.timeshet_fill.18 = 'a', 1,0) + IF($db1.timeshet_fill.19 = 'a', 1,0) + IF($db1.timeshet_fill.20= 'a', 1,0)
				+ IF($db1.timeshet_fill.21 = 'a', 1,0) + IF($db1.timeshet_fill.22 = 'a', 1,0) + IF($db1.timeshet_fill.23 = 'a', 1,0)
				+ IF($db1.timeshet_fill.24 = 'a', 1,0) + IF($db1.timeshet_fill.25 = 'a', 1,0) + IF($db1.timeshet_fill.26 = 'a', 1,0)
				+ IF($db1.timeshet_fill.27 = 'a', 1,0) + IF($db1.timeshet_fill.28 = 'a', 1,0) + IF($db1.timeshet_fill.29 = 'a', 1,0)
				+ IF($db1.timeshet_fill.30 = 'a', 1,0) + IF($db1.timeshet_fill.31 = 'a', 1,0)
		)  AS 'absent'
		,
		SUM(IF($db1.timeshet_fill.1 = 'p', 1,0) + IF($db1.timeshet_fill.2 = 'p', 1,0)
				+ IF($db1.timeshet_fill.3 = 'p', 1,0) + IF($db1.timeshet_fill.4 = 'p', 1,0) + IF($db1.timeshet_fill.5 = 'p', 1,0)
				+ IF($db1.timeshet_fill.6 = 'p', 1,0) + IF($db1.timeshet_fill.7 = 'p', 1,0) + IF($db1.timeshet_fill.8 = 'p', 1,0)
				+ IF($db1.timeshet_fill.9 = 'p', 1,0) + IF($db1.timeshet_fill.10 = 'p', 1,0) + IF($db1.timeshet_fill.11 = 'p', 1,0)
				+ IF($db1.timeshet_fill.12 = 'p', 1,0) + IF($db1.timeshet_fill.13 = 'p', 1,0) + IF($db1.timeshet_fill.14 = 'p', 1,0)
				+ IF($db1.timeshet_fill.15 = 'p', 1,0) + IF($db1.timeshet_fill.16 = 'p', 1,0) + IF($db1.timeshet_fill.17 = 'p', 1,0)
				+ IF($db1.timeshet_fill.18 = 'p', 1,0) + IF($db1.timeshet_fill.19 = 'p', 1,0) + IF($db1.timeshet_fill.20= 'p', 1,0)
				+ IF($db1.timeshet_fill.21 = 'p', 1,0) + IF($db1.timeshet_fill.22 = 'p', 1,0) + IF($db1.timeshet_fill.23 = 'p', 1,0)
				+ IF($db1.timeshet_fill.24 = 'p', 1,0) + IF($db1.timeshet_fill.25 = 'p', 1,0) + IF($db1.timeshet_fill.26 = 'p', 1,0)
				+ IF($db1.timeshet_fill.27 = 'p', 1,0) + IF($db1.timeshet_fill.28 = 'p', 1,0) + IF($db1.timeshet_fill.29 = 'p', 1,0)
				+ IF($db1.timeshet_fill.30 = 'p', 1,0) + IF($db1.timeshet_fill.31 = 'p', 1,0)
		)  AS 'present'
		");

        $this->db->from("$db1.timeshet_fill");
        //Code Comment By Asheesh in case of Replacement.. (29-11-2019)
        //$this->db->where("$db1.timeshet_fill.emp_id", $empid);
        $this->db->where("$db1.timeshet_fill.project_id", $project_id);
        //24-04-2019.. Harish
        $this->db->where("$db1.timeshet_fill.designation_id", $designation_id);
        $this->db->where("$db1.timeshet_fill.year", $year);
        $this->db->where("$db1.timeshet_fill.month", $month);
        $this->db->where("$db1.timeshet_fill.is_active", "1");

        $result = $this->db->get()->result();
        return isset($result[0]) ? $result[0] : false;
    }
	
	
	function insertRecord($table, $data) {
        $add = $this->db->insert($table, $data);

        print_r($this->db->last_query()); die;
        if ($add) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	
	//Select Recored From..
    public function selectRecord($cegth_table, $select, $Where) {
        $this->db->select($select);
        $this->db->from($cegth_table);
        $this->db->where($Where);
        return $this->db->get();
    }

}

?>
