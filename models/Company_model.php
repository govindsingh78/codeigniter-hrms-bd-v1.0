<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Company_model extends CI_Model {

    var $table = 'main_company as a';
    var $column_order = array(null, 'company_name', 'company_details', 'contact_no', 'email_id'); //set column field database for datatable orderable
    var $column_search = array('company_name', 'company_details', 'contact_no', 'email_id'); //set column field database for datatable searchable 
    var $order = array('a.company_website' => 'DESC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {

        if ($this->input->post('sectorinput')) {
            $secct = $this->input->post('sectorinput');
            foreach ($secct as $val) {
                $this->db->where("FIND_IN_SET($val, a.com_sector) !=", 0);
            }
        }
        if ($this->input->post('company_name_nature')) {
            $compnature = $this->input->post('company_name_nature');
            $this->db->where("FIND_IN_SET($compnature, a.comp_field_id) !=", 0);
        }
        if ($this->input->post('opcountry_name')) {
            $newcountId = $this->input->post('opcountry_name');
            $this->db->where("FIND_IN_SET($newcountId, a.operating_countries) !=", 0);
             //$this->db->like('a.operating_countries', $this->input->post('country_name'));
        }

        if ($this->input->post('country_name')) {
            $countIds = $this->input->post('country_name');
            $this->db->where("FIND_IN_SET($countIds, a.country) !=", 0);
             //$this->db->like('a.country', $this->input->post('country_name'));
        }
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('status', '1');
		$this->db->order_by('fld_id','DESC');
        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
         return $this->db->count_all_results();
        //return '0';
    }

}
